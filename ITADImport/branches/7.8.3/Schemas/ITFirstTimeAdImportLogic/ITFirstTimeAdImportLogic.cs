using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Text;
using System.Xml;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using System.Linq;
using System.IO;
using System.DirectoryServices;
using System.Security.Principal;
using Terrasoft.Core.LDAP;

namespace Terrasoft.Configuration.ITBase
{
	public class ContactObject
	{
		public Guid ContactId { get; set; }
		public SysAdminUnitItem SysAdminUnit { get; set; }
		public string Email { get; set; }
		public ContactObject(Guid contactId, SysAdminUnitItem sysAdminUnit)
		{
			ContactId = contactId;
			SysAdminUnit = sysAdminUnit;
		}
	}
	public class ITFirstTimeAdImportLogic : ITDBBase
	{
		public ITFirstTimeAdImportLogic(UserConnection userConnection) : base(userConnection)
		{

		}

		public void DoAdImport(UserConnection userConnection)
		{
			string adUnit = "OU=FGUP,DC=main,DC=russianpost,DC=ru";
			//получение всех пользователей Active Directory по пути OU=FGUP,DC=main,DC=russianpost,DC=ru
			List<AdUser> adUsers = GetAdUsers(adUnit, userConnection);
			//получение всех пользователей bpm'online c SysAdminUnitTypeValue = 4,
			//LEN(Ldapentryid) = 0, SynchronizeWithLDAP = 0
			List<ContactObject> sysAdminUnitList = GetSysAdminUnitList(userConnection);
			//получение всех адресов email --select Email from Contact where Id = '410006E1-CA4E-4502-A9EC-E54D922D2C00'
			GetEmailFromContactTable(ref sysAdminUnitList, userConnection);
			//обновление всех данных по Active Directory
			UpdateTables(adUsers, sysAdminUnitList, userConnection);
		}

		private void UpdateTables(List<AdUser> adUsers, List<ContactObject> sysAdminUnitList, UserConnection userConnection)
		{
			//try
			//{
				List<string> existedLDAPEntryDb = GetLDAPEntryDbUsers(userConnection);
				for (int i = 0; i < sysAdminUnitList.Count; i++)
				{
					var sysAdminUnit = sysAdminUnitList[i];
					var adUser = adUsers.Where(a => a.Email == sysAdminUnit.Email).FirstOrDefault();
					if (adUser != null)
					{
						var existedUser = existedLDAPEntryDb.Where(l => l == adUser.LDAPEntryId).FirstOrDefault();
						if (existedUser == null)
						{
							//Create LDAPElementId
							Guid lDAPElementId = Guid.NewGuid();
							//insert LDAPElement
							InsertNewLDAPElement(lDAPElementId, adUser, userConnection);
							//update SysAdminUnit set LDAPEntry, LDAPEntryId
							UpdateSysAdminUnit(sysAdminUnit.ContactId, adUser, lDAPElementId, userConnection);
							//throw new Exception("adUser.DistinguishedName: " + adUser.DistinguishedName);
						}
					}

				}
				//throw new Exception("adUsers.Count: " + adUsers.Count);
			//}
			//catch (Exception ex)
			//{
				//throw new Exception("UpdateTables(List<AdUser> adUsers, List<ContactObject> sysAdminUnitList, UserConnection userConnection)");
			//}
		}

		private List<string> GetLDAPEntryDbUsers(UserConnection userConnection)
		{
			List<string> result = new List<string>();
			var selectSysAdminUnits = new Select(userConnection).From("SysAdminUnit")
				.Column("LDAPEntryId").Where("SysAdminUnitTypeValue")
				.IsEqual(Column.Parameter(4)) as Select;
			using (DBExecutor executor = userConnection.EnsureDBConnection())
			{
				using (IDataReader reader = selectSysAdminUnits.ExecuteReader(executor))
				{
					while (reader.Read())
					{
						var lDAPEntryId = reader.GetColumnValue<string>("LDAPEntryId");
						if (!string.IsNullOrEmpty(lDAPEntryId))
							result.Add(lDAPEntryId);
					}
				}
			}
			return result;
		}

		private void UpdateSysAdminUnit(Guid contactId, AdUser adUser, Guid lDAPElementId, UserConnection userConnection)
		{
			try
			{
				var updateSysAdminUnit = new Update(userConnection, "SysAdminUnit")
					.Set("SynchronizeWithLDAP", Column.Parameter(true))
					.Set("LDAPEntry", Column.Parameter(adUser.SAMAccountName))
					.Set("LDAPEntryId", Column.Parameter(adUser.LDAPEntryId))
					.Set("LDAPElementId", Column.Parameter(lDAPElementId))
					.Where("ContactId")
					.IsEqual(Column.Parameter(contactId))
					;
				updateSysAdminUnit.Execute();
			}
			catch (Exception ex)
			{
				throw new Exception("UpdateSysAdminUnit(Guid contactId, AdUser adUser, UserConnection userConnection)");
			}
		}

		private void InsertNewLDAPElement(Guid lDAPElementId, AdUser adUser, UserConnection userConnection)
		{
			try
			{
				var insertLDAPElement = new Insert(userConnection).Into("LDAPElement")
					.Set("Id", Column.Parameter(lDAPElementId))
					.Set("Name", Column.Parameter(adUser.Cn))
					.Set("Description", Column.Parameter("ITAutoLDAPUsersLoading"))
					.Set("ProcessListeners", Column.Parameter(0))
					.Set("LDAPEntryId", Column.Parameter(adUser.LDAPEntryId))
					.Set("LDAPEntryDN", Column.Parameter(""))
					.Set("Type", Column.Parameter(4))
					.Set("FullName", Column.Parameter(adUser.SAMAccountName))
					.Set("Company", Column.Parameter(""))
					.Set("Email", Column.Parameter(""))
					.Set("Phone", Column.Parameter(""))
					.Set("JobTitle", Column.Parameter(""))
					.Set("IsActive", Column.Parameter(true))
					;
				insertLDAPElement.Execute();
			}
			catch (Exception ex)
			{
				throw new Exception("InsertNewLDAPElement(Guid lDAPElementId, AdUser adUser, UserConnection userConnection)");
			}
		}

		private void GetEmailFromContactTable(ref List<ContactObject> sysAdminUnitList, UserConnection userConnection)
		{
			try
			{
				int emailCounter = 0;
				if (sysAdminUnitList.Count > 0)
				{
					for (int i = 0; i < sysAdminUnitList.Count; i++)
					{
						var item = sysAdminUnitList[i];
						var selectContact = new Select(userConnection).From("Contact")
						.Column("Email")
						.Where("Id")
						.IsEqual(Column.Parameter(item.ContactId)) as Select
						;
						using (DBExecutor executor = userConnection.EnsureDBConnection())
						{
							using (IDataReader reader = selectContact.ExecuteReader(executor))
							{
								while (reader.Read())
								{
									string email = reader.GetColumnValue<string>("Email");
									sysAdminUnitList[i].Email = email;
									emailCounter++;
								}
							}
						}
					}
				}
				//throw new Exception("emailCounter: " + emailCounter);
			}
			catch (Exception ex)
			{
				throw new Exception("GetEmailFromContactTable(ref List<ContactObject> sysAdminUnitList, UserConnection userConnection)");
			}
		}

		private List<ContactObject> GetSysAdminUnitList(UserConnection userConnection)
		{
			List<ContactObject> result = new List<ContactObject>();
			try
			{
				var selectSysAdminUnit = new Select(userConnection).From("SysAdminUnit")
					.Column("Id")
					.Column("LDAPEntry")
					.Column("LDAPEntryId")
					.Column("LDAPEntryDN")
					.Column("LDAPElementId")
					.Column("SysAdminUnitTypeValue")
					.Column("SynchronizeWithLDAP")
					.Column("ContactId")
					;
				using (DBExecutor executor = userConnection.EnsureDBConnection())
				{
					using (IDataReader reader = selectSysAdminUnit.ExecuteReader(executor))
					{
						while (reader.Read())
						{
							var sysAdminUnitTypeValue = reader.GetColumnValue<int>("SysAdminUnitTypeValue");
							var ldapentryid = reader.GetColumnValue<string>("Ldapentryid");
							var synchronizeWithLDAP = reader.GetColumnValue<bool>("SynchronizeWithLDAP");

							if (sysAdminUnitTypeValue == 4
								&& string.IsNullOrEmpty(ldapentryid)
								&& synchronizeWithLDAP == false)
							{
								var id = reader.GetColumnValue<Guid>("Id");
								SysAdminUnitItem saui = new SysAdminUnitItem(id, "", "", "", null);
								var contactId = reader.GetColumnValue<Guid>("ContactId");
								if (contactId != null && contactId != Guid.Empty)
								{
									ContactObject co = new ContactObject(contactId, saui);
									result.Add(co);
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("List<ContactObject> GetSysAdminUnitList(UserConnection userConnection)");
			}
			//throw new Exception("GetSysAdminUnitList.result.Count" + result.Count);
			return result;
		}

		private List<AdUser> GetAdUsers(string oUnit, UserConnection userConnection)
		{
			List<AdUser> users = new List<AdUser>();
			try
			{
				DirectoryEntry searchRoot = CreateDirectoryEntry(oUnit);
				DirectorySearcher search = new DirectorySearcher(searchRoot);
				search.Filter = "(&(memberOf=CN=R00-CRM_users,OU=CRM_Users,OU=Groups,OU=R00,OU=FGUP,DC=main,DC=russianpost,DC=ru)(objectClass=person))";
				search.PropertiesToLoad.Add("cn");
				search.PropertiesToLoad.Add("userPrincipalName");
				search.PropertiesToLoad.Add("objectSid");
				search.PropertiesToLoad.Add("sAMAccountName");
				search.PropertiesToLoad.Add("distinguishedName");
				search.PropertiesToLoad.Add("mail");

				SearchResult result;
				SearchResultCollection resultCol = search.FindAll();

				if (resultCol != null)
				{
					for (int counter = 0; counter < resultCol.Count; counter++)
					{
						result = resultCol[counter];
						if (result.Properties.Contains("cn")
							&& result.Properties.Contains("userPrincipalName")
							&& result.Properties.Contains("objectSid")
							&& result.Properties.Contains("sAMAccountName")
							&& result.Properties.Contains("distinguishedName")
							&& result.Properties.Contains("mail"))
						{
							var sidInBytes = (byte[])result.Properties["objectSid"][0];
							LdapUtilities ldapUtils = new LdapUtilities(userConnection);
							DirectoryEntry user = result.GetDirectoryEntry();

							if (user != null)
							{
								try
								{
									string objectSid = ldapUtils.IdentityAttributeToString(sidInBytes);
									string userPrincipalName = (string)result.Properties["userPrincipalName"][0];
									string cn = (string)result.Properties["cn"][0];
									string sAMAccountName = (string)result.Properties["sAMAccountName"][0];
									string email = (string)result.Properties["mail"][0];
									string distinguishedName = user.Properties["distinguishedName"].Value.ToString();
									AdUser adUser = new AdUser(cn, distinguishedName, objectSid,
										sAMAccountName, userPrincipalName, email);
									users.Add(adUser);
								}
								catch (Exception ex)
								{
									throw new Exception("new AdUser(cn, distinguishedName, objectSid, sAMAccountName, userPrincipalName, email);");
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("List<AdUser> GetAdUsers(string oUnit, UserConnection userConnection);");
			}
			//throw new Exception("users.Count" + users.Count);
			return users;
		}

		private DirectoryEntry CreateDirectoryEntry(string oUnit)
		{
			DirectoryEntry ldapConnection = new DirectoryEntry();
			ldapConnection.Username = "CRM-ldap-R00";
			ldapConnection.Password = "IvuCE3GxJh";
			ldapConnection.Path = "LDAP://" + oUnit;
			ldapConnection.AuthenticationType = AuthenticationTypes.Secure;
			return ldapConnection;
		}
	}
}
