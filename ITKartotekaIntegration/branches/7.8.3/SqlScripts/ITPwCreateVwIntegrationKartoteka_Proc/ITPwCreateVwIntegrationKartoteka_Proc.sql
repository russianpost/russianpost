SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT OBJECT_ID('[dbo].[ITPwCreateVwIntegrationKartoteka_Proc]') IS NULL
BEGIN

DROP PROCEDURE [dbo].[ITPwCreateVwIntegrationKartoteka_Proc];
END;
GO

CREATE PROCEDURE [dbo].[ITPwCreateVwIntegrationKartoteka_Proc]

AS

declare @dbName nvarchar(100)
declare @localDummy nvarchar(max)
declare @localInsert nvarchar(max)
declare @localView nvarchar(max)
declare @prodView nvarchar(max)

BEGIN

SET NOCOUNT ON;

IF NOT OBJECT_ID('[dbo].[ITVwIntegrationKartoteka]') IS NULL

DROP VIEW [dbo].[ITVwIntegrationKartoteka];

SELECT @dbName = DB_NAME();

SET @localDummy = 'CREATE TABLE [dbo].[org_detail]
	(
	[hash] [varchar](32) NOT NULL,
	[inn] [varchar](10) NULL,
	[kpp] [varchar](9) NULL,
	[ogrn] [varchar](13) NULL,
	[okpo] [varchar](8) NULL,
	[full_name] [nvarchar](2048) NULL,
	[short_name] [nvarchar](2048) NULL
) ON [PRIMARY]
SET ANSI_PADDING ON
ALTER TABLE [dbo].[org_detail] ADD [phones] [nvarchar](1024) NULL
ALTER TABLE [dbo].[org_detail] ADD [address] [nvarchar](2048) NULL
ALTER TABLE [dbo].[org_detail] ADD [control_name] [nvarchar](1024) NULL
ALTER TABLE [dbo].[org_detail] ADD [control_post] [nvarchar](1024) NULL
ALTER TABLE [dbo].[org_detail] ADD [status_name] [varchar](50) NULL
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[org_detail] ADD [okved_code] [varchar](8) NULL
ALTER TABLE [dbo].[org_detail] ADD [okved_name] [nvarchar](2048) NULL
ALTER TABLE [dbo].[org_detail] ADD [okogu] [varchar](8) NULL
ALTER TABLE [dbo].[org_detail] ADD [okogu_name] [nvarchar](2048) NULL
ALTER TABLE [dbo].[org_detail] ADD [okopf] [varchar](5) NULL
ALTER TABLE [dbo].[org_detail] ADD [okopf_name] [nvarchar](2048) NULL

 CONSTRAINT [pk_org_detail] PRIMARY KEY CLUSTERED 
(
	[hash] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
'

SET @localInsert = '
INSERT INTO [dbo].[org_detail] ([hash],  [inn], [kpp], [ogrn], [okpo],[full_name], [short_name],  [phones],  [address], [control_name], [control_post], [status_name], [okved_code], [okved_name], [okogu], [okogu_name], [okopf], [okopf_name]) VALUES (N''000002949883ecaac2f23e545286d1a3'', N''1035004467'', N''502406021'', N''7040008912123'', N''50240101'', N''Общество с ограниченной ответственностью "Облконтракт"'', N''ООО "Облконтракт"'',  N''2794949'', N''143406, МОСКОВСКАЯ ОБЛАСТЬ, КРАСНОГОРСКИЙ РАЙОН, КРАСНОГОРСК ГОРОД, УЛИЦА ЦИОЛКОВСКОГО, 17'', N''Минаева Лариса Владимировна'', N''Генеральный директор'', N''Действующая'', N''51.70'', N''Прочая опотовая торговля'', N''4210014'', N''Организации, учрежденные юридическими лицами или гражданами, или юридическими лицами и гражданами совместно'', N''12300'', N''Общества с ограниченной ответственностью'')
INSERT INTO [dbo].[org_detail] ([hash],  [inn], [kpp], [ogrn], [okpo],[full_name], [short_name],  [phones],  [address], [control_name], [control_post], [status_name], [okved_code], [okved_name], [okogu], [okogu_name], [okopf], [okopf_name]) VALUES (N''000007a335696729b34b72b1c64b864b'', N''1063720006'', N''371600250'', N''9937421456899'', N''37160101'', N''Общество с ограниченной ответственностью "Жилсервис"'', N''ООО "Жилсервис"'',  N''49344-21091,2-10-91'', N''155270, ИВАНОВСКАЯ ОБЛАСТЬ, ЛУХСКИЙ РАЙОН, ЛУХ ПОСЕЛОК, 101, 4'', N''Лебедев Владимир Юрьевич'', N''ДИРЕКТОР'', N''Действующая'', N''51.70'', N''Прочая опотовая торговля'', N''4210014'', N''Организации, учрежденные юридическими лицами или гражданами, или юридическими лицами и гражданами совместно'', N''12300'', N''Общества с ограниченной ответственностью'')
INSERT INTO [dbo].[org_detail] ([hash],  [inn], [kpp], [ogrn], [okpo],[full_name], [short_name],  [phones],  [address], [control_name], [control_post], [status_name], [okved_code], [okved_name], [okogu], [okogu_name], [okopf], [okopf_name]) VALUES (N''000007c57a4ae0fe37f1f5fd2dc95a92'', N''1138603007'', N''860320040'', N''1251166854546'', N''86030101'', N''ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "СИБЭНЕРДЖИ"'', N''ООО "СИБЭНЕРДЖИ"'',  N''3466-56-15-16,+79222552037'', N''628616, ХАНТЫ-МАНСИЙСКИЙ АВТОНОМНЫЙ ОКРУГ - ЮГРА АВТОНОМНЫЙ ОКРУГ, НИЖНЕВАРТОВСК ГОРОД, УЛИЦА 4 ПС, 8'', N''Попов Владимир Викторович'', N''ДИРЕКТОР'', N''Действующая'', N''51.70'', N''Прочая опотовая торговля'', N''4210014'', N''Организации, учрежденные юридическими лицами или гражданами, или юридическими лицами и гражданами совместно'', N''12300'', N''Общества с ограниченной ответственностью'')
INSERT INTO [dbo].[org_detail] ([hash],  [inn], [kpp], [ogrn], [okpo],[full_name], [short_name],  [phones],  [address], [control_name], [control_post], [status_name], [okved_code], [okved_name], [okogu], [okogu_name], [okopf], [okopf_name]) VALUES (N''000008372c8ef23fcb2faf1cc3376cdd'', N''1155243001'', N''524303589'', N''5246621605455'', N''52430101'', N''ЖИЛИЩНО-СТРОИТЕЛЬНЫЙ ПОТРЕБИТЕЛЬСКИЙ КООПЕРАТИВ № 103'', N''ЖСПК № 103'',  N''98164,98444'', N''607220, НИЖЕГОРОДСКАЯ ОБЛАСТЬ, АРЗАМАС ГОРОД, УЛИЦА КАЛИНИНА, ДОМ 18-А'', N''Рынденкова Олеся Александровна'', N''ПРЕДСЕДАТЕЛЬ'', N''Действующая'', N''51.70'', N''Прочая опотовая торговля'', N''4210014'', N''Организации, учрежденные юридическими лицами или гражданами, или юридическими лицами и гражданами совместно'', N''12300'', N''Общества с ограниченной ответственностью'')
INSERT INTO [dbo].[org_detail] ([hash],  [inn], [kpp], [ogrn], [okpo],[full_name], [short_name],  [phones],  [address], [control_name], [control_post], [status_name], [okved_code], [okved_name], [okogu], [okogu_name], [okopf], [okopf_name]) VALUES (N''0000086338bae66c6525e19b01690966'', N''1035206157'', N''526304005'', N''1391451754566'', N''52630101'', N''ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ ТОРГОВО-ПРОМЫШЛЕННАЯ КОМПАНИЯ "БУШИР"'', N''ООО ТПК "БУШИР"'',   N''2238916'', N''603003, НИЖЕГОРОДСКАЯ ОБЛАСТЬ, НИЖНИЙ НОВГОРОД ГОРОД, УЛИЦА ЩЕРБАКОВА, 39'', N''Ширяев Владимир Станиславович'', N''ДИРЕКТОР'', N''Действующая'', N''51.70'', N''Прочая опотовая торговля'', N''4210014'', N''Организации, учрежденные юридическими лицами или гражданами, или юридическими лицами и гражданами совместно'', N''12300'', N''Общества с ограниченной ответственностью'')
INSERT INTO [dbo].[org_detail] ([hash],  [inn], [kpp], [ogrn], [okpo],[full_name], [short_name],  [phones],  [address], [control_name], [control_post], [status_name], [okved_code], [okved_name], [okogu], [okogu_name], [okopf], [okopf_name]) VALUES (N''00000963762df61587bf41b63db15162'', N''1055402028'', N''540245537'', N''7756061454888'', N''54020101'', N''Общество с ограниченной ответственностью "Сибирский парк"'', N''ООО "Сибирский парк"'',  NULL, N''630082, НОВОСИБИРСКАЯ ОБЛАСТЬ, НОВОСИБИРСК ГОРОД, УЛИЦА ВАВИЛОВА, 11, 7'', N''Надеева Юлия Юрьевна'', N''Директор'', N''Действующая'', N''51.70'', N''Прочая опотовая торговля'', N''4210014'', N''Организации, учрежденные юридическими лицами или гражданами, или юридическими лицами и гражданами совместно'', N''12300'', N''Общества с ограниченной ответственностью'')
INSERT INTO [dbo].[org_detail] ([hash],  [inn], [kpp], [ogrn], [okpo],[full_name], [short_name],  [phones],  [address], [control_name], [control_post], [status_name], [okved_code], [okved_name], [okogu], [okogu_name], [okopf], [okopf_name]) VALUES (N''000009d5a3a4e8dabbafb5c3920e97bb'', N''1172225020'', N''222518216'', N''1338757887855'', N''22250101'', N''ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "ГАРАНТ АВТО"'', N''ООО "ГАРАНТ АВТО"'',  NULL, N''656056, АЛТАЙСКИЙ КРАЙ, БАРНАУЛ ГОРОД, УЛИЦА ЛЬВА ТОЛСТОГО, ДОМ 9'', N''Романова Светлана Анатольевна'', N''ДИРЕКТОР'', N''Действующая'', N''51.70'', N''Прочая опотовая торговля'', N''4210014'', N''Организации, учрежденные юридическими лицами или гражданами, или юридическими лицами и гражданами совместно'', N''12300'', N''Общества с ограниченной ответственностью'')
INSERT INTO [dbo].[org_detail] ([hash],  [inn], [kpp], [ogrn], [okpo],[full_name], [short_name],  [phones],  [address], [control_name], [control_post], [status_name], [okved_code], [okved_name], [okogu], [okogu_name], [okopf], [okopf_name]) VALUES (N''00000a8dd3895f436705061878ecafac'', N''1147746428'', N''771897790'', N''2936462248779'', N''77200101'', N''ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МУФТА ПРО"'', N''ООО "МУФТА ПРО"'',  NULL, N''111024, МОСКВА ГОРОД, УЛИЦА ЭНТУЗИАСТОВ 2-Я, ДОМ 5, КОРПУС 1, ЭТАЖ 2 КОМН.9'', N''Сметанкин Алексей Валерьевич'', N''ГЕНЕРАЛЬНЫЙ ДИРЕКТОР'', N''Действующая'', N''51.70'', N''Прочая опотовая торговля'', N''4210014'', N''Организации, учрежденные юридическими лицами или гражданами, или юридическими лицами и гражданами совместно'', N''12300'', N''Общества с ограниченной ответственностью'')
INSERT INTO [dbo].[org_detail] ([hash],  [inn], [kpp], [ogrn], [okpo],[full_name], [short_name],  [phones],  [address], [control_name], [control_post], [status_name], [okved_code], [okved_name], [okogu], [okogu_name], [okopf], [okopf_name]) VALUES (N''00000ab63a86a6fef6f259be86bef59d'', N''1167847078'', N''781463896'', N''3438395224777'', N''78140101'', N''ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "ВОДИНВЕСТ"'', N''ООО "ВОДИНВЕСТ"'',  NULL, N''197373, САНКТ-ПЕТЕРБУРГ ГОРОД, ПРОСПЕКТ АВИАКОНСТРУКТОРОВ, ДОМ 25, КВАРТИРА 135'', N''Гатина Лиана Раудатовна'', N''ГЕНЕРАЛЬНЫЙ ДИРЕКТОР'', N''Действующая'', N''51.70'', N''Прочая опотовая торговля'', N''4210014'', N''Организации, учрежденные юридическими лицами или гражданами, или юридическими лицами и гражданами совместно'', N''12300'', N''Общества с ограниченной ответственностью'')
INSERT INTO [dbo].[org_detail] ([hash],  [inn], [kpp], [ogrn], [okpo],[full_name], [short_name],  [phones],  [address], [control_name], [control_post], [status_name], [okved_code], [okved_name], [okogu], [okogu_name], [okopf], [okopf_name]) VALUES (N''00000cc36fc9b17ce4de30fade3811d0'', N''1122651029'', N''263581544'', N''3886838534644'', N''26350101'', N''Общество с ограниченной ответственностью Управляющая Компания "ЛИДЕР"'', N''ООО УК "ЛИДЕР"'',  N''8652-57-15-36'',N''355042, СТАВРОПОЛЬСКИЙ КРАЙ, СТАВРОПОЛЬ ГОРОД, ПРОЕЗД 2 ЮГО-ЗАПАДНЫЙ, 2, Д'', N''Власенко Елена Васильевна'', N''Директор'', N''Действующая'', N''51.70'', N''Прочая опотовая торговля'', N''4210014'', N''Организации, учрежденные юридическими лицами или гражданами, или юридическими лицами и гражданами совместно'', N''12300'', N''Общества с ограниченной ответственностью'')
'

SET @localView = 'CREATE VIEW [dbo].[ITVwIntegrationKartoteka]
AS
SELECT         null as Id, null as CreatedById, null as CreatedOn, null as ModifiedById, null as ModifiedOn, ogrn AS ITOgrn, okpo AS ITOkpo, full_name AS ITFullName, short_name AS ITShortName, phones AS ITPhones, control_name AS ITControlName, 
               control_post AS ITControlPost, okogu AS ITOkogu, okopf as ITOkopf, okopf_name as ITOkopfName, inn AS ITInn, kpp AS ITKpp, [address] as ITAddress, [status_name] as ITStatus,
               okogu_name as ITOKOGUDescription, okved_code as ITOKVED, okved_name as ITOKVEDDescription
FROM            dbo.org_detail'

SET @prodView = 'CREATE VIEW [dbo].[ITVwIntegrationKartoteka]
AS
SELECT         null as Id, null as CreatedById, null as CreatedOn, null as ModifiedById, null as ModifiedOn, ogrn AS ITOgrn, okpo AS ITOkpo, full_name AS ITFullName, short_name AS ITShortName, phones AS ITPhones, control_name AS ITControlName, 
               control_post AS ITControlPost, okogu AS ITOkogu, okopf as ITOkopf, okopf_name as ITOkopfName, inn AS ITInn, kpp AS ITKpp, [address] as ITAddress, [status_name] as ITStatus,
               okogu_name as ITOKOGUDescription, okved_code as ITOKVED, okved_name as ITOKVEDDescription
FROM [10.7.121.225].[Kartoteka].[data].[org_detail]'


IF(@dbName = 'bpmonline783' or @dbName = 'TEST_PROD')
BEGIN

IF EXISTS(SELECT 1 FROM sys.servers WHERE name = '10.7.121.225')
Exec sp_dropserver '10.7.121.225', 'droplogins'

EXEC sp_addlinkedserver   
    @server='10.7.121.225', 
    @srvproduct='',
    @provider='SQLNCLI', 
    @datasrc='tcp:10.7.121.225'

EXEC sp_addlinkedsrvlogin
    @useself='FALSE',
    @rmtsrvname='10.7.121.225',
    @rmtuser='crm_usr',
    @rmtpassword='Crm&Usr@1' 

EXEC sp_executesql @prodView

END

ELSE

BEGIN

IF NOT OBJECT_ID('[dbo].[org_detail]') IS NULL

DROP TABLE [dbo].[org_detail];

SET LANGUAGE ENGLISH

EXEC sp_executesql @localDummy
EXEC sp_executesql @localInsert
EXEC sp_executesql @localView

END
END

--
--
