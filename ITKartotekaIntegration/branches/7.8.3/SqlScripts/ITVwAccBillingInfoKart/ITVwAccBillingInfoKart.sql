SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT OBJECT_ID('[dbo].[ITVwAccBillingInfoKart]') IS NULL
BEGIN

DROP VIEW [dbo].[ITVwAccBillingInfoKart];
END;
GO

CREATE VIEW [dbo].[ITVwAccBillingInfoKart]
AS
SELECT        null as Id, null as CreatedById, null as CreatedOn, null as ModifiedById, null as ModifiedOn, T1.AccountId as ITAccountId, T1.TsIdentificationNumber as ITINN, T1.TsKPP as ITKPP, T1.TsOKPO as ITOKPO, T1.TsOKVED as ITOKVED, T1.TsOGRN as ITOGRN, T2.Name AS ITAccountManagerName, T3.Name AS ITChiefAccountantName
FROM            dbo.AccountBillingInfo AS T1 LEFT OUTER JOIN
                         dbo.Contact AS T2 ON T1.AccountManagerId = T2.Id LEFT OUTER JOIN
                         dbo.Contact AS T3 ON T1.ChiefAccountantId = T3.Id
WHERE        (T1.AccountManagerId IS NOT NULL) OR
                         (T1.ChiefAccountantId IS NOT NULL)

GO
--