--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--IF NOT OBJECT_ID('[dbo].[ITVwIntegrationKartoteka]') IS NULL
--BEGIN

--DROP VIEW [dbo].[ITVwIntegrationKartoteka];
--END;
--GO

--CREATE VIEW [dbo].[ITVwIntegrationKartoteka]
--AS
--SELECT         null as Id, null as CreatedById, null as CreatedOn, null as ModifiedById, null as ModifiedOn, ogrn AS ITOgrn, okpo AS ITOkpo, full_name AS ITFullName, short_name AS ITShortName, phones AS ITPhones, control_name AS ITControlName, 
--               control_post AS ITControlPost, reg_date AS ITRegDate, end_date AS ITEndDate, okogu AS ITOkogu, okopf2015 as ITOkopf, okopf_name as ITOkopfName, bik AS ITBik, inn AS ITInn, kpp AS ITKpp, address as ITAddress, status as ITStatus,
--               okogu as ITOKOGUDescription, okfs as ITOKVED, okfs as ITOKVEDDescription
--FROM            dbo.org
----[10.7.121.225].[Kartoteka].[data].[org_detail]

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--IF NOT OBJECT_ID('[dbo].[ITVwIntegrationKartoteka]') IS NULL
--BEGIN

--DROP VIEW [dbo].[ITVwIntegrationKartoteka];
--END;
--GO

--CREATE VIEW [dbo].[ITVwIntegrationKartoteka]
--AS
--SELECT         null as Id, null as CreatedById, null as CreatedOn, null as ModifiedById, null as ModifiedOn, ogrn AS ITOgrn, okpo AS ITOkpo, full_name AS ITFullName, short_name AS ITShortName, phones AS ITPhones, control_name AS ITControlName, 
--               control_post AS ITControlPost, okogu AS ITOkogu, okopf as ITOkopf, okopf_name as ITOkopfName, inn AS ITInn, kpp AS ITKpp, [address] as ITAddress, [status_name] as ITStatus,
--               okogu_name as ITOKOGUDescription, okved_code as ITOKVED, okved_name as ITOKVEDDescription
--FROM            dbo.org_detail
--FROM [10.7.121.225].[Kartoteka].[data].[org_detail]
