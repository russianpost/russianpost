SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT OBJECT_ID('[dbo].[ITPwEnrichAccountFromKartoteka_Proc]') IS NULL
BEGIN

DROP PROCEDURE [dbo].[ITPwEnrichAccountFromKartoteka_Proc];
END;
GO

	CREATE PROCEDURE [dbo].[ITPwEnrichAccountFromKartoteka_Proc]

		@AccountId uniqueidentifier,
		@Address nvarchar(2048),
		@ControlName nvarchar(250),
		@ControlPost nvarchar(250),
		@OwnerId uniqueidentifier

	AS

	declare @itChiefId uniqueidentifier
	declare @countContact int
	declare @result int
	declare @jobId uniqueidentifier
	declare @insertedContactIdTable TABLE (ID uniqueidentifier)
	declare @insertedJobIdTable	TABLE (ID uniqueidentifier)
	declare @tempItChiefId uniqueidentifier
	declare @tempItChiefAccountantId uniqueidentifier

	BEGIN

	SET NOCOUNT ON;

	SET @result = 0;
	SET @countContact = 0;

		--Создаем/изменяем адрес, если его передали
	--IF(@Address IS NOT NULL AND @Address != '')
	--BEGIN

	--SET @result = (SELECT COUNT(1) FROM [AccountAddress] WHERE AccountId = @AccountId AND AddressTypeId = '770BF68C-4B6E-DF11-B988-001D60E938C6')
	
	--IF(@result = 0)
	--INSERT INTO [AccountAddress] (AccountId, AddressTypeId, [Address], [Primary]) VALUES (@AccountId, '770BF68C-4B6E-DF11-B988-001D60E938C6', @address, 0);

	--ELSE
	--UPDATE [AccountAddress] SET [Address] = @address WHERE [AccountId] = @AccountId

	--END

	--Проверяем наличие должности, если нет добавляем
	IF(@ControlPost IS NOT NULL AND @ControlPost != '' AND CHARINDEX('ухгалтер', @ControlPost) = 0) 
	BEGIN
	SELECT @jobId = Id FROM [Job] WHERE UPPER(Name) = UPPER(@ControlPost)
	
	IF(@jobId IS NULL)
	BEGIN
	INSERT INTO [Job] (CreatedById, ModifiedById, Name, ProcessListeners) OUTPUT INSERTED.Id INTO @insertedJobIdTable VALUES(@OwnerId, @OwnerId, CAST(UPPER(SUBSTRING(@ControlPost, 1, 1)) + LOWER(SUBSTRING(@ControlPost, 2, LEN(@ControlPost)-1)) AS nvarchar), 0)

	SELECT @jobId = (SELECT ID FROM @insertedJobIdTable)
	END
	END

	--Создаем контакт руководителя, если его нет
	IF(@ControlName IS NOT NULL AND @ControlName != '')
	BEGIN
	SELECT @countContact = 1, @itChiefId = Id FROM Contact WHERE Name = @ControlName
	
	--Если значение должности бухгалтер добавляем его Id, для добавления главным бухгалтером
	IF CHARINDEX('ухгалтер', @ControlPost) > 0
	SET @jobId = '2B9F3536-1395-4AE8-B978-491A7BDF543C'

	IF(@countContact = 0)
	INSERT INTO [Contact] (Name, AccountId, OwnerId, JobId, JobTitle, TypeId, Confirmed, Completeness) OUTPUT INSERTED.Id INTO @insertedContactIdTable
	 VALUES (@ControlName, @AccountId, @OwnerId, @jobId, @ControlPost, '806732EE-F36B-1410-A883-16D83CAB0980', 1, 30)
	--ELSE
	--UPDATE [Contact] SET JobId = @jobId WHERE Name = @ControlName

	IF(@countContact = 0)
		 select @itChiefId = (SELECT ID FROM @insertedContactIdTable)
	 END

	SELECT ITChiefId = @tempItChiefId, ITChiefAccountantId = @tempItChiefAccountantId from Account WHERE [Id] = @AccountId;

	IF(@tempItChiefId is null)
	BEGIN
	--Обновляем привязку руководителя, только если нам передали его
	IF(@ControlName IS NOT NULL AND @ControlName != '' and CHARINDEX('ухгалтер', @ControlPost) = 0)
	UPDATE Account SET ITChiefId = @itChiefId WHERE [Id] = @AccountId;
	END

	--Если должность для обогащения главный бухгалтер, добавляем привязку
	IF(@tempItChiefAccountantId is null and CHARINDEX('ухгалтер', @ControlPost) > 0)
	UPDATE Account SET ITChiefAccountantId = @itChiefId WHERE [Id] = @AccountId;

	END
	--