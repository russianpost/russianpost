SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT OBJECT_ID('[dbo].[ITPwEnrichNewAccountFromKartoteka_Proc]') IS NULL
BEGIN

DROP PROCEDURE [dbo].[ITPwEnrichNewAccountFromKartoteka_Proc];
END;
GO

CREATE PROCEDURE [dbo].[ITPwEnrichNewAccountFromKartoteka_Proc]
		@AccountId uniqueidentifier,
		@OwnerId uniqueidentifier,
		@Inn varchar(12),
		@Kpp varchar(9)
AS

declare @ogrn varchar(13)
declare @okpo varchar(8)
declare @full_name nvarchar(2048)
declare @short_name nvarchar(2048)
declare @address nvarchar(2048)
declare @phones nvarchar(1024)
declare @control_name nvarchar(1024)
declare @control_post nvarchar(1024)
declare @status_name varchar(50)
declare @okved_code varchar(50)
declare @okved_name nvarchar(2048)
declare @okogu varchar(8)
declare @okogu_name nvarchar(2048)
declare @okopf varchar(5)
declare @okopf_name nvarchar(2048)
declare @insertedContactIdTable TABLE (ID uniqueidentifier)
declare @insertedJobIdTable	TABLE (ID uniqueidentifier)
declare @insertedContactId uniqueidentifier
declare @itChiefId uniqueidentifier
declare @countContact int
declare @accountAddressCount int
declare @result int
declare @jobId uniqueidentifier
declare @tempItChiefId uniqueidentifier
declare @tempItChiefAccountantId uniqueidentifier
		
BEGIN

	SET NOCOUNT ON;
	SET @countContact = 0;
	SET @accountAddressCount = 0;
	SET @result = 0;

	--Получаем данные из картотеки по КПП и ИНН
	IF EXISTS(SELECT 1 FROM [ITVwIntegrationKartoteka] Where [ITKpp] = @Kpp and [ITInn] = @Inn)
	BEGIN
	SELECT @ogrn = [ITOgrn],@okpo = [ITOkpo],@full_name = [ITFullName],@short_name = [ITShortName],
	@address = [ITAddress],@phones = [ITPhones],@control_name = [ITControlName],
	@control_post = [ITControlPost],@okogu = [ITOkogu],@okogu_name = [ITOKOGUDescription],
	@okopf = [ITOkopf],@okopf_name = [ITOkopfName],@okved_code = [ITOKVED],
	@okved_name = [ITOKVEDDescription], @status_name = [ITStatus]
	FROM [ITVwIntegrationKartoteka] Where [ITKpp] = @Kpp and [ITInn] = @Inn;

	--Создаем/изменяем адрес, если его передали
	--IF(@Address IS NOT NULL AND @Address != '')
	--BEGIN

	--SET @result = (SELECT COUNT(1) FROM [AccountAddress] WHERE AccountId = @AccountId AND AddressTypeId = '770BF68C-4B6E-DF11-B988-001D60E938C6')
	
	--IF(@result = 0)
	--INSERT INTO [AccountAddress] (AccountId, AddressTypeId, [Address], [Primary]) VALUES (@AccountId, '770BF68C-4B6E-DF11-B988-001D60E938C6', @address, 0);

	--ELSE
	--UPDATE [AccountAddress] SET [Address] = @address WHERE [AccountId] = @AccountId

	--END

	--Проверяем наличие должности, если нет добавляем
	IF(@control_post IS NOT NULL AND @control_post != '' AND CHARINDEX('ухгалтер', @control_post) = 0) 
	BEGIN
	SELECT @jobId = Id FROM [Job] WHERE UPPER(Name) = UPPER(@control_post)
	
	IF(@jobId IS NULL)
	BEGIN
	INSERT INTO [Job] (CreatedById, ModifiedById, Name, ProcessListeners) OUTPUT INSERTED.Id INTO @insertedJobIdTable VALUES(@OwnerId, @OwnerId, CAST(UPPER(SUBSTRING(@control_post, 1, 1)) + LOWER(SUBSTRING(@control_post, 2, LEN(@control_post)-1)) AS nvarchar), 0)

	SELECT @jobId = (SELECT ID FROM @insertedJobIdTable)
	END
	END

	--Создаем контакт руководителя, если его нет
	IF(@control_name IS NOT NULL AND @control_name != '')
	BEGIN
	SELECT @countContact = 1, @itChiefId = Id FROM Contact WHERE Name = @control_name
	
	--Если значение должности бухгалтер добавляем его Id, для добавления главным бухгалтером
	IF CHARINDEX('ухгалтер', @control_post) > 0
	SET @jobId = '2B9F3536-1395-4AE8-B978-491A7BDF543C'

	IF(@countContact = 0)
	INSERT INTO [Contact] (Name, AccountId, OwnerId, JobId, JobTitle, TypeId, Confirmed, Completeness) OUTPUT INSERTED.Id INTO @insertedContactIdTable
	 VALUES (@control_name, @AccountId, @OwnerId, @jobId, @control_post, '806732EE-F36B-1410-A883-16D83CAB0980', 1, 30)
	--ELSE
	--UPDATE [Contact] SET JobId = @jobId WHERE Name = @ControlName

	IF(@countContact = 0)
		 select @itChiefId = (SELECT ID FROM @insertedContactIdTable)
	 END

	--Обновляем контрагента
	UPDATE Account SET ITOGRN = ISNULL(@ogrn, ''),ITOKPO = ISNULL(@okpo, ''), TsFullName = ISNULL(@full_name, ''), ITShortName = ISNULL(@short_name, ''),
						Phone = ISNULL(@phones, ''), ITOKOGU = ISNULL(@okogu, ''), ITOKOGUDEScription = ISNULL(@okogu_name, ''),
						ITOKOPF = ISNULL(@okopf, ''), ITOKOPFDescription = @okopf_name, ITOKVED = @okved_code, ITOKVEDDescription = ISNULL(@okved_name, ''),
						ITStatus = ISNULL(@status_name, '')
						WHERE [Id] = @AccountId;
	
	SELECT ITChiefId = @tempItChiefId, ITChiefAccountantId = @tempItChiefAccountantId from Account WHERE [Id] = @AccountId;

	IF(@tempItChiefId is null)
	BEGIN
	--Обновляем привязку руководителя, только если нам передали его
	IF(@control_name IS NOT NULL AND @control_name != '' and CHARINDEX('ухгалтер', @control_post) = 0)
	UPDATE Account SET ITChiefId = @itChiefId WHERE [Id] = @AccountId;
	END

	--Если должность для обогащения главный бухгалтер, добавляем привязку
	IF(@tempItChiefAccountantId is null and CHARINDEX('ухгалтер', @control_post) > 0)
	UPDATE Account SET ITChiefAccountantId = @itChiefId WHERE [Id] = @AccountId;

	END
END
--