namespace Terrasoft.Configuration.ITKartotekaIntegration
{
    using System.CodeDom.Compiler;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.ServiceModel.Activation;
    using System.Runtime.Serialization;
    using System.Web;
    using Terrasoft.Common;
    using Terrasoft.Core;
    using Terrasoft.Core.DB;
    using Terrasoft.Core.Entities;
    using Terrasoft.Core.Store;
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Newtonsoft.Json.Linq;

    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ITEnrichNewAccountFromKartoteka
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
        public string EnrichNewAccount(string accountId, string ownerId, string inn, string kpp)
        {

            try
            {
            	var userConnection = (UserConnection)HttpContext.Current.Session["UserConnection"];
            	
                var storedProcedure = new StoredProcedure(userConnection, "ITPwEnrichNewAccountFromKartoteka_Proc")
                .WithParameter("AccountId", accountId)
                .WithParameter("OwnerId", ownerId)
                .WithParameter("Inn", inn)
                .WithParameter("Kpp", kpp) as StoredProcedure;
				storedProcedure.PackageName = userConnection.DBEngine.SystemPackageName;
				storedProcedure.Execute();
            }
            
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Good";
        }
    }
}
