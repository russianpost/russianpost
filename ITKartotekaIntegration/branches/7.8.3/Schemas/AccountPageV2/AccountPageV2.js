define("AccountPageV2", ["ServiceHelper"], function(ServiceHelper) {
	return {
		entitySchemaName: "Account",

		attributes: {
			"EnrichButtonEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITOKOGUVisible": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITOkoguEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITOKOGUDescriptionEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},

			"ITOgrnEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITOkpoEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITFullNameEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITShortNameEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITControlNameEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITControlPostEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITOKVEDEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITOKVEDDescriptionEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITOkopfEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITOkopfNameEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITStatusEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITChiefEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			
			"ITChiefAccountantEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},
			"AccountPhoneEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": true
			},

			"TsKPP": {
				dependencies: [
					{
						columns: ["Ownership"],
						methodName: "onOwnershipChange"
					}
				]
			},

			"TsKPPisVisible": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: true
			}
		},

		methods: {

			onEntityInitialized: function() {
				this.callParent();
				this.on("change:ITSector", this.onITSectorChanged, this);
				this.onITSectorChanged();
				this.accountBillingInfoLoad();
				this.TsKppVisibility();
			},

			onITSectorChanged: function() {
				var sector = this.get("ITSector");
				if (!sector || sector && sector.displayValue !== "Государственный") {
					this.set("ITOKOGUVisible", false);
				} else {
					this.set("ITOKOGUVisible", true);
				}
			},
			
			accountBillingInfoLoad: function() {
				var cardStorage = this.Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "ITVwAccBillingInfoKart"
				});

				cardStorage.addColumn("ITAccountId");
				cardStorage.addColumn("ITINN");
				cardStorage.addColumn("ITKPP");
				cardStorage.addColumn("ITOKPO");
				cardStorage.addColumn("ITOKVED");
				cardStorage.addColumn("ITOGRN");
				cardStorage.addColumn("ITAccountManagerName");
				cardStorage.addColumn("ITChiefAccountantName");

				var mainContext = this;

				cardStorage.filters.add("filterId", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ITAccountId", this.get("Id")));

				cardStorage.getEntityCollection(function(result) {
					if (!result.success) {
						return;
					}
					if (result.success) {
						if (!result.collection.getCount() > 0) {
							return;
						}
					}

					result.collection.each(function(item) {
						if (!mainContext.get("TsKPP")) {
							mainContext.set("TsKPP", item.get("ITKPP"));
						}
						if (!mainContext.get("TsINN")) {
							mainContext.set("TsINN", item.get("ITINN"));
						}
						if (!mainContext.get("ITOKPO")) {
							mainContext.set("ITOKPO", item.get("ITOKPO"));
						}
						if (!mainContext.get("ITOGRN")) {
							mainContext.set("ITOGRN", item.get("ITOGRN"));
						}
						if (!mainContext.get("ITOKVED")) {
							mainContext.set("ITOKVED", item.get("ITOKVED"));
						}
						if (!mainContext.get("ITOGRN")) {
							mainContext.set("ITOGRN", item.get("ITOGRN"));
						}
						if (!mainContext.get("ITChief")) {
							mainContext.set("ITChief", item.get("ITAccountManagerName"));
						}
						if (!mainContext.get("ITChiefAccountant")) {
							mainContext.set("ITChiefAccountant", item.get("ITChiefAccountantName"));
						}
					});
				}, this);
			},


			onEnrichAccountButtonClick: function() {
				if (!this.get("TsKPP") || !this.get("TsINN")) {
					this.showInformationDialog("Поля ИНН и КПП должны быть заполнены");
					return;
				}

				var cfg = {
					style: Terrasoft.MessageBoxStyles.BLUE
				};
				this.showConfirmationDialog(
					"Будут загружены регистрационные данные для данного контрагента",
					function getSelectedButton(returnCode) {
						if (returnCode === Terrasoft.MessageBoxButtons.OK.returnCode) {
							var cardStorage = this.Ext.create("Terrasoft.EntitySchemaQuery", {
								rootSchemaName: "ITVwIntegrationKartoteka"
							});

							cardStorage.addColumn("ITOgrn");
							cardStorage.addColumn("ITOkpo");
							cardStorage.addColumn("ITFullName");
							cardStorage.addColumn("ITShortName");
							cardStorage.addColumn("ITPhones");
							cardStorage.addColumn("ITAddress");
							cardStorage.addColumn("ITControlName");
							cardStorage.addColumn("ITControlPost");
							cardStorage.addColumn("ITOkogu");
							cardStorage.addColumn("ITOKOGUDescription");
							cardStorage.addColumn("ITOkopf");
							cardStorage.addColumn("ITOkopfName");
							cardStorage.addColumn("ITOKVED");
							cardStorage.addColumn("ITOKVEDDescription");
							cardStorage.addColumn("ITStatus");

							var mainContext = this;

							cardStorage.filters.add("filterKpp", this.Terrasoft.createColumnFilterWithParameter(
								this.Terrasoft.ComparisonType.EQUAL, "ITKpp", this.get("TsKPP").toString()));

							cardStorage.filters.add("filterInn", this.Terrasoft.createColumnFilterWithParameter(
								this.Terrasoft.ComparisonType.EQUAL, "ITInn", this.get("TsINN").toString()));

							cardStorage.filters.logicalOperator = Terrasoft.LogicalOperatorType.AND;

							cardStorage.getEntityCollection(function(result) {
								mainContext.showBodyMask();
								if (!result.success) {
									this.showInformationDialog("Ошибка запроса данных");
									mainContext.hideBodyMask();
									return;
								}
								if (result.success) {
									if (!result.collection.getCount() > 0) {
										this.showInformationDialog("По данному клиенту не найдены данные для обогащения информации в карточке");
										mainContext.hideBodyMask();
										return;
									}
								}

								result.collection.each(function(item) {
									if (!mainContext.get("TsFullName")) {
										mainContext.set("TsFullName", item.get("ITFullName"));
										mainContext.set("ITFullNameEnabled", false);
									}
									if (!mainContext.get("ITOKPO")) {
										mainContext.set("ITOKPO", item.get("ITOkpo"));
										mainContext.set("ITOkpoEnabled", false);
									}
									if (!mainContext.get("ITOGRN")) {
										mainContext.set("ITOGRN", item.get("ITOgrn"));
										mainContext.set("ITOgrnEnabled", false);
									}
									if (!mainContext.get("ITShortName")) {
										mainContext.set("ITShortName", item.get("ITShortName"));
										mainContext.set("ITShortNameEnabled", false);
									}
									if (mainContext.get("ITOKOGUVisible") && !mainContext.get("ITOKOGU")) {
										mainContext.set("ITOKOGU", item.get("ITOkogu"));
										mainContext.set("ITOkoguEnabled", false);
									}
									if (mainContext.get("ITOKOGUVisible") && !mainContext.get("ITOKOGUDescription")) {
										mainContext.set("ITOKOGUDescription", item.get("ITOKOGUDescription"));
										mainContext.set("ITOKOGUDescriptionEnabled", false);
									}
									if (!mainContext.get("ITOKOPF")) {
										mainContext.set("ITOKOPF", item.get("ITOkopf"));
										mainContext.set("ITOkopfEnabled", false);
									}
									if (!mainContext.get("ITOKOPFDescription")) {
										mainContext.set("ITOKOPFDescription", item.get("ITOkopfName"));
										mainContext.set("ITOkopfNameEnabled", false);
									}
									if (!mainContext.get("ITOKVED")) {
										mainContext.set("ITOKVED", item.get("ITOKVED"));
										mainContext.set("ITOKVEDEnabled", false);
									}
									if (!mainContext.get("ITOKVEDDescription")) {
										mainContext.set("ITOKVEDDescription", item.get("ITOKVEDDescription"));
										mainContext.set("ITOKVEDDescriptionEnabled", false);
									}
									if (!mainContext.get("ITStatus")) {
										mainContext.set("ITStatus", item.get("ITStatus"));
										mainContext.set("ITStatusEnabled", false);
									}
									if (!mainContext.get("Phone")) {
										mainContext.set("Phone", item.get("ITPhones"));
										mainContext.set("AccountPhoneEnabled", false);
									}
									if (mainContext.get("PrimaryColumnValue")) {
										var accountId = mainContext.get("PrimaryColumnValue");
										var ownerId = mainContext.get("Owner").value;
										var controlName = item.get("ITControlName");
										var controlPost = item.get("ITControlPost");
										var address = item.get("ITAddress");
										var isControlChiefAccountant = item.get("ITControlPost").indexOf("ухгалтер") !== -1;

										var serviceData = {
											accountId: accountId,
											address: address,
											controlName: controlName,
											controlPost: controlPost,
											ownerId: ownerId
										};
										
										ServiceHelper.callService("ITEnrichAccountFromKartoteka", "EnrichAccount",
											function(response) {
												if (response.EnrichAccountResult === "Good") {
													if (address) {
														mainContext.updateDetail({detail: "AccountAddress"});
													}
													var contactStorage = mainContext.Ext.create("Terrasoft.EntitySchemaQuery", {
														rootSchemaName: "Contact"
													});

													contactStorage.addColumn("Id");
													contactStorage.addColumn("Name");

													contactStorage.filters.add("filterName", mainContext.Terrasoft.createColumnFilterWithParameter(
														mainContext.Terrasoft.ComparisonType.EQUAL, "Name", controlName));

													contactStorage.getEntityCollection(function(result) {
														if (result.success) {
															result.collection.each(function(item) {
																if (isControlChiefAccountant && !mainContext.get("ITChiefAccountant")) {
																	mainContext.set("ITChiefAccountant", {
																		value: item.get("Id"),
																		displayValue: item.get("Name")
																	});
																}
																else {
																	if (!mainContext.get("ITChief")) {
																		mainContext.set("ITChief", {
																			value: item.get("Id"),
																			displayValue: item.get("Name")
																		});
																	}
																}
															});
														}
													}, mainContext);
												}
											}, serviceData, this);
									}
									/*if (mainContext.get("PrimaryColumnValue") && item.get("ITControlName")) {
										var accountId1 = mainContext.get("PrimaryColumnValue");
										var ownerId = mainContext.get("Owner").value;
										var controlName = item.get("ITControlName");
										var jobTitle = item.get("ITControlPost");

										var serviceData1 = {
											accountId: accountId1,
											ownerId: ownerId,
											controlName: controlName,
											jobTitle: jobTitle
										};

										ServiceHelper.callService("ITUpdateAccountChiefFromKartoteka", "UpdateAccountChief",
											function(response) {
												if (response.UpdateAccountChiefResult === "Good") {
													var contactStorage = mainContext.Ext.create("Terrasoft.EntitySchemaQuery", {
														rootSchemaName: "Contact"
													});

													contactStorage.addColumn("Id");
													contactStorage.addColumn("Name");

													contactStorage.filters.add("filterName", mainContext.Terrasoft.createColumnFilterWithParameter(
														mainContext.Terrasoft.ComparisonType.EQUAL, "Name", controlName));

													contactStorage.getEntityCollection(function(result) {
														if (result.success) {
															result.collection.each(function(item) {
																mainContext.set("ITChief", {
																	value: item.get("Id"),
																	displayValue: item.get("Name")
																});
															});
														}
													}, mainContext);
												}
											}, serviceData1, this);
									}*/
									/*if (!mainContext.get("ITChief")) {
										mainContext.set("ITChief", item.get("ITControlName"));
										mainContext.set("ITChiefEnabled", false);
									}*/
									/*if (!mainContext.get("ITChiefAccountant")) {
										mainContext.set("ITChiefAccountant", item.get("ITControlPost"));
										mainContext.set("ITChiefAccountantEnabled", false);
									}*/
									/*if (mainContext.get("PrimaryColumnValue") && item.get("ITAddress")) {
										var accountId = mainContext.get("PrimaryColumnValue");
										var address = this.get("ITAddress");

										var serviceData = {
											accountId: accountId,
											address: address
										};

										ServiceHelper.callService("ITUpdateAccountAddressFromKartotekaWCFService", "UpdateAccountAddress",
											function(response) {
												if (response.UpdateAccountAddressResult === "Good") {
													mainContext.updateDetail({
														detail: "AccountAddress"
													});
												}
											}, serviceData, this);
									}*/
									mainContext.hideBodyMask();
								});
							}, this);
						}
					}, ["ok", "cancel"], cfg);



			},

			onOwnershipChange: function() {
				this.clearTsKpp();
				this.TsKppVisibility();
			},

			clearTsKpp: function() {
				if (this.get("Ownership") && this.get("Ownership").value === "350469c4-40f9-49c5-8298-7f8a38548050") {
					this.set("TsKPP", null);
				}
			},

			TsKppVisibility: function() {
				if (this.get("Ownership") && this.get("Ownership").value === "350469c4-40f9-49c5-8298-7f8a38548050") {
					this.set("TsKPPisVisible", false);
				} else {
					this.set("TsKPPisVisible", true);
				}
			}
		},

		//Настройка визуализации кнопки на странице редактирования.
		diff: [

			// Метаданные для добавления на страницу пользовательской кнопки.
			{
				// Указывает на то, что выполняется операция добавления элемента на страницу.
				"operation": "insert",
				// Мета-имя родительского элемента управления, в который добавляется кнопка.
				"parentName": "ActionDashboardContainer",
				// Указывает на то, что кнопка добавляется в коллекцию элементов управления
				// родительского элемента (мета-имя которого указано в parentName).
				"propertyName": "items",
				// Мета-имя добавляемой кнопки.
				"name": "EnrichButton",
				// Дополнительные свойства элемента.
				"values": {
					// Тип добавляемого элемента — кнопка.
					"itemType": Terrasoft.ViewItemType.BUTTON,
					// Привязка заголовка кнопки к локализуемой строке схемы.
					"caption": "Обогатить данными из картотеки",
					// Привязка метода-обработчика нажатия кнопки.
					"click": {
						bindTo: "onEnrichAccountButtonClick"
					},
					// Стиль отображения кнопки.
					"style": Terrasoft.controls.ButtonEnums.style.GREEN,
					// Привязка свойства доступности кнопки.
					"enabled": {
						bindTo: "EnrichButtonEnabled"
					}
				}
			},

			{
				"operation": "remove",
				"name": "TsINN"
			},

			{
				"operation": "remove",
				"name": "TsKPP"
			},

			{
				"operation": "remove",
				"name": "TsFullName"
			},
			
			{
				"operation": "merge",
				"name": "TsBrandName",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			
			{
				"operation": "merge",
				"name": "AccountPhone",
				"values": {
					"enabled": {bindTo: "AccountPhoneEnabled"}
				}
			},

			{
				"operation": "insert",
				"name": "RegisterInfoGroup",
				"parentName": "AccountPageGeneralTabContainer",
				"propertyName": "items",
				"index": 1,
				"values": {
					"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
					"caption": "Регистрационные данные",
					"items": []
				}
			},

			{
				"operation": "insert",
				"name": "RegDataGrid",
				"values": {
					"itemType": 0,
					"items": []
				},
				"parentName": "RegisterInfoGroup",
				"propertyName": "items",
				"index": 0
			},

			{
				"operation": "insert",
				"name": "RegDataNoOKOGUGrid",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 0,
						"column": 0,
						"row": 0,
						"layoutName": "RegDataGrid"
					},
					"itemType": 0,
					"items": []
				},
				"parentName": "RegDataGrid",
				"propertyName": "items",
				"index": 0
			},

			{
				"operation": "insert",
				"name": "RegDataOKOGUGrid",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 0,
						"column": 0,
						"row": 0,
						"layoutName": "RegisterInfoGroup"
					},
					"visible": {
						"bindTo": "ITOKOGUVisible"
					},
					"itemType": 0,
					"items": []
				},
				"parentName": "RegisterInfoGroup",
				"propertyName": "items",
				"index": 1
			},

			{
				"operation": "insert",
				"name": "RegDataAfterOKOGUGrid",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 0,
						"column": 0,
						"row": 0,
						"layoutName": "RegisterInfoGroup"
					},
					"itemType": 0,
					"items": []
				},
				"parentName": "RegisterInfoGroup",
				"propertyName": "items",
				"index": 2
			},


			{
				"operation": "insert",
				"name": "TsFullName",
				"values": {
					"caption": "Полное наименование контрагента",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "RegDataNoOKOGUGrid"
					},
					"bindTo": "TsFullName",
					"enabled": {bindTo: "ITFullNameEnabled"}
				},
				"parentName": "RegDataNoOKOGUGrid",
				"propertyName": "items",
				"index": 0
			},

			{
				"operation": "insert",
				"name": "ITShortName",
				"values": {
					"caption": "Сокращенное наименование",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "RegDataNoOKOGUGrid"
					},
					"bindTo": "ITShortName",
					"enabled": {bindTo: "ITShortNameEnabled"}
				},
				"parentName": "RegDataNoOKOGUGrid",
				"propertyName": "items",
				"index": 1
			},

			{
				"operation": "insert",
				"name": "ITChief",
				"values": {
					"caption": "Руководитель",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "RegDataNoOKOGUGrid"
					},
					"bindTo": "ITChief",
					"enabled": {bindTo: "ITChiefEnabled"}
				},
				"parentName": "RegDataNoOKOGUGrid",
				"propertyName": "items",
				"index": 2
			},

			{
				"operation": "insert",
				"name": "ITChiefAccountant",
				"values": {
					"caption": "Главный бухгалтер",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "RegDataNoOKOGUGrid"
					},
					"bindTo": "ITChiefAccountant",
					"enabled": {bindTo: "ITChiefAccountantEnabled"}
				},
				"parentName": "RegDataNoOKOGUGrid",
				"propertyName": "items",
				"index": 3
			},

			{
				"operation": "insert",
				"name": "TsINN",
				"values": {
					"caption": "ИНН",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "RegDataNoOKOGUGrid"
					},
					"bindTo": "TsINN"
				},
				"parentName": "RegDataNoOKOGUGrid",
				"propertyName": "items",
				"index": 4
			},

			{
				"operation": "insert",
				"name": "TsKPP",
				"values": {
					"visible": {"bindTo": "TsKPPisVisible"},
					"caption": "КПП",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "RegDataNoOKOGUGrid"
					},
					"bindTo": "TsKPP"
				},
				"parentName": "RegDataNoOKOGUGrid",
				"propertyName": "items",
				"index": 5
			},

			{
				"operation": "insert",
				"name": "ITOGRN",
				"values": {
					"caption": "ОГРН",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "RegDataNoOKOGUGrid"
					},
					"bindTo": "ITOGRN",
					"enabled": {bindTo: "ITOgrnEnabled"}
				},
				"parentName": "RegDataNoOKOGUGrid",
				"propertyName": "items",
				"index": 6
			},

			{
				"operation": "insert",
				"name": "ITOKPO",
				"values": {
					"caption": "ОКПО",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "RegDataNoOKOGUGrid"
					},
					"bindTo": "ITOKPO",
					"enabled": {bindTo: "ITOkpoEnabled"}
				},
				"parentName": "RegDataNoOKOGUGrid",
				"propertyName": "items",
				"index": 7
			},

			{
				"operation": "insert",
				"name": "ITOKVED",
				"values": {
					"caption": "ОКВЭД",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "RegDataNoOKOGUGrid"
					},
					"bindTo": "ITOKVED",
					"enabled": {bindTo: "ITOKVEDEnabled"}
				},
				"parentName": "RegDataNoOKOGUGrid",
				"propertyName": "items",
				"index": 8
			},

			{
				"operation": "insert",
				"name": "ITOKVEDDescription",
				"values": {
					"caption": "Расшифровка кода ОКВЭД",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "RegDataNoOKOGUGrid"
					},
					"bindTo": "ITOKVEDDescription",
					"enabled": {bindTo: "ITOKVEDDescriptionEnabled"}
				},
				"parentName": "RegDataNoOKOGUGrid",
				"propertyName": "items",
				"index": 9
			},

			{
				"operation": "insert",
				"name": "ITOKOGU",
				"values": {
					"caption": "ОКОГУ",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "RegDataOKOGUGrid"
					},
					"bindTo": "ITOKOGU",
					"enabled": {bindTo: "ITOkoguEnabled"}
				},
				"parentName": "RegDataOKOGUGrid",
				"propertyName": "items",
				"index": 0
			},

			{
				"operation": "insert",
				"name": "ITOKOGUDescription",
				"values": {
					"caption": "Расшифровка кода ОКОГУ",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "RegDataOKOGUGrid"
					},
					"bindTo": "ITOKOGUDescription",
					"enabled": {bindTo: "ITOKOGUDescriptionEnabled"}
				},
				"parentName": "RegDataOKOGUGrid",
				"propertyName": "items",
				"index": 1
			},

			{
				"operation": "insert",
				"name": "ITOKOPF",
				"values": {
					"caption": "ОКОПФ",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "RegDataAfterOKOGUGrid"
					},
					"bindTo": "ITOKOPF",
					"enabled": {bindTo: "ITOkopfEnabled"}
				},
				"parentName": "RegDataAfterOKOGUGrid",
				"propertyName": "items",
				"index": 0
			},

			{
				"operation": "insert",
				"name": "ITOKOPFDescription",
				"values": {
					"caption": "Расшифровка кода ОКОПФ",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "RegDataAfterOKOGUGrid"
					},
					"bindTo": "ITOKOPFDescription",
					"enabled": {bindTo: "ITOkopfNameEnabled"}
				},
				"parentName": "RegDataAfterOKOGUGrid",
				"propertyName": "items",
				"index": 1
			},

			{
				"operation": "insert",
				"name": "ITStatus",
				"values": {
					"caption": "Статус",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "RegDataAfterOKOGUGrid"
					},
					"bindTo": "ITStatus",
					"enabled": {bindTo: "ITStatusEnabled"}
				},
				"parentName": "RegDataAfterOKOGUGrid",
				"propertyName": "items",
				"index": 2
			}

		]

	};
});