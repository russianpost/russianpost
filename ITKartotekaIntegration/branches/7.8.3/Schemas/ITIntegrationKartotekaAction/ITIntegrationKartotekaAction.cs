using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using System.Linq;

namespace Terrasoft.Configuration.ITBase
{
    public class Job
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Job(Guid id, string name)
        {
            Id = id;
            Name = name;
        }
    }

    public class AccountItem
    {
        public Guid Id { get; set; }
        public Int64 TsKPP { get; set; }
        public string TsINN { get; set; }
        public Guid? OwnerId { get; set; }

        public AccountItem(Guid id, Int64 tsKPP, string tsINN, Guid? ownerId)
        {
            Id = id;
            TsKPP = tsKPP;
            TsINN = tsINN;
            OwnerId = ownerId;
        }
    }

    public class ITVwIntegrationKartoteka
    {
        public Int64 TsKPP { get; set; }
        public string TsINN { get; set; }
        public string ITOgrn { get; set; }
        public string ITOkpo { get; set; }
        public string ITFullName { get; set; }
        public string ITShortName { get; set; }
        public string ITPhones { get; set; }
        public string ITAddress { get; set; }
        public string ITControlName { get; set; }
        public string ITControlPost { get; set; }
        public string ITOkogu { get; set; }
        public string ITOKOGUDescription { get; set; }
        public string ITOkopf { get; set; }
        public string ITOkopfName { get; set; }
        public string ITOKVED { get; set; }
        public string ITOKVEDDescription { get; set; }
        public string ITStatus { get; set; }

        public ITVwIntegrationKartoteka(Int64 tsKPP, string tsINN, string itOgrn, string itOkpo, string itFullName, string itShortName,
            string itPhones, string itAddress, string itControlName, string itControlPost, string itOkogu, string itOKOGUDescription,
            string itOkopf, string itOkopfName, string itOKVED, string itOKVEDDescription, string itStatus)
        {
            TsKPP = tsKPP;
            TsINN = tsINN;
            ITOgrn = itOgrn;
            ITOkpo = itOkpo;
            ITFullName = itFullName;
            ITShortName = itShortName;
            ITPhones = itPhones;
            ITAddress = itAddress;
            ITControlName = itControlName;
            ITControlPost = itControlPost;
            ITOkogu = itOkogu;
            ITOKOGUDescription = itOKOGUDescription;
            ITOkopf = itOkopf;
            ITOkopfName = itOkopfName;
            ITOKVED = itOKVED;
            ITOKVEDDescription = itOKVEDDescription;
            ITStatus = itStatus;
        }
    }

    public class IntegrationKartotekaAction : ITDBBase
    {
        public IntegrationKartotekaAction(UserConnection userConnection) : base(userConnection)
        {
        }

        public void Update(UserConnection userConnection)
        {
            int line = 0;
            string accId = "";
            string address = "";
            string controlName = "";
            string controlPost = "";
            //string ownerId = "";
            //1 Получаем всех контрагентов
            line = 1;
            try
            {
                List<AccountItem> accounts = GetAllExistAccounts(userConnection);
                //throw new Exception("Test3");
                line = 2;
                foreach (var account in accounts)
                {
                    line = 3;
                    ITVwIntegrationKartoteka itVwIntegrationKartoteka = GetITVwIntegrationKartoteka(account, userConnection);
                    line = 4;
                    if (itVwIntegrationKartoteka != null)
                    {
                        //InsertAcademyUrl(account, userConnection);
                        line = 5;
                        UpdateAccount(account, itVwIntegrationKartoteka, userConnection);
                        line = 6;
                        string accountId = account.Id.ToString();
                        accId = accountId;
                        address = itVwIntegrationKartoteka.ITAddress;
                        controlName = itVwIntegrationKartoteka.ITControlName;
                        controlPost = itVwIntegrationKartoteka.ITControlPost;
                        //ownerId = account.OwnerId == null || account.OwnerId == Guid.Empty ? Guid.Empty.ToString() : account.OwnerId.ToString();
                        Guid? ownerId = account.OwnerId;
                        line = 7;
                        try
                        {
                            EnrichAccount(account.Id, address, controlName, controlPost, ownerId, userConnection);
                        }
                        catch(Exception ex)
                        {
                            /*
                            address = string.IsNullOrEmpty(address) ? "0" : address;
                            controlName = string.IsNullOrEmpty(controlName) ? "0" : controlName;
                            controlPost = string.IsNullOrEmpty(controlPost) ? "0" : controlPost;
                            var msg = "line: " + line + ","
                                //+ "address :" + "," + address + "controlName:" + controlName + "," + "controlPost:" + controlPost + ", ownerId" + ownerId
                                + ", accId = "
                                + accId + ", Ex: " + ex.Message;
                            var insert = new Insert(userConnection).Into("AcademyURL")
                            .Set("Id", Column.Parameter(Guid.NewGuid()))
                            .Set("Name", Column.Parameter("Error"))
                            .Set("Descr2", Column.Parameter(msg))
                            .Set("ProcessListeners", Column.Parameter(0))
                            ;

                            insert.Execute();
                            */
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                /*
                address = string.IsNullOrEmpty(address) ? "0" : address;
                controlName = string.IsNullOrEmpty(controlName) ? "0" : controlName;
                controlPost = string.IsNullOrEmpty(controlPost) ? "0" : controlPost;
                var msg = "line: " + line + ","
                    //+ "address :" + "," + address + "controlName:" + controlName + "," + "controlPost:" + controlPost + ", ownerId" + ownerId
                    + ", accId = "
                    + accId + ", Ex: " + ex.Message;
                var insert = new Insert(userConnection).Into("AcademyURL")
                .Set("Id", Column.Parameter(Guid.NewGuid()))
                .Set("Name", Column.Parameter("Error"))
                .Set("Descr2", Column.Parameter(msg))
                .Set("ProcessListeners", Column.Parameter(0))
                ;

                insert.Execute();
                */
            }
        }

        private void InsertAcademyUrl(AccountItem account, UserConnection userConnection)
        {
            string res = "account.Id : " + account.Id;
            var insert = new Insert(userConnection).Into("AcademyURL")
                .Set("Id", Column.Parameter(Guid.NewGuid()))
                .Set("Name", Column.Parameter("NewAccountUpdate"))
                .Set("Description", Column.Parameter(res))
                .Set("ProcessListeners", Column.Parameter(0))
                ;
            insert.Execute();
        }

        private void InsertNewContactAcademyUrl(Guid id, UserConnection userConnection)
        {
            string res = "Contact.Id : " + id;
            var insert = new Insert(userConnection).Into("AcademyURL")
                .Set("Id", Column.Parameter(Guid.NewGuid()))
                .Set("Name", Column.Parameter("NewContact"))
                .Set("Description", Column.Parameter(res))
                .Set("ProcessListeners", Column.Parameter(0))
                ;
            insert.Execute();
        }

        private void EnrichAccount(Guid accountId, string address, string controlName, string controlPost, Guid? ownerId, UserConnection userConnection)
        {
            List<Job> jobList = GetJobList(userConnection);
            bool isAccountPosition = CheckIsAccountPosition(controlPost);
            Guid? jobId = null;
            Guid? itChiefId = null;
            if (!string.IsNullOrEmpty(controlPost) && isAccountPosition == false)
            {
                Job job = jobList.Where(j => j.Name.ToUpper() == controlPost.ToUpper()).FirstOrDefault();
                if(job == null)
                {
                    jobId = Guid.NewGuid();
                    InsertNewJobItem((Guid)jobId, controlPost, userConnection);
                }
                else
                {
                    jobId = job.Id;
                }
            }
            else if(isAccountPosition == true)
            {
                jobId = Guid.Parse("2B9F3536-1395-4AE8-B978-491A7BDF543C");
            }
            //else
            //{
            //    jobId = Guid.NewGuid();
            //    InsertNewJobItem(jobId, controlPost, userConnection);
            //}
            
            //Создаем контакт руководителя, если его нет
            if(!string.IsNullOrEmpty(controlName))
            {
                itChiefId = GetChifIdFromContact(controlName, userConnection);
                if (itChiefId == null && jobId != null)
                    itChiefId = InsertNewContact(controlName, accountId, ownerId, (Guid)jobId, controlPost, userConnection);
            }

            //Обновляем привязку руководителя, только если нам передали его
            if(!string.IsNullOrEmpty(controlName) && isAccountPosition == false && itChiefId != null)
            {
                UpdateAccounITChiefId(itChiefId, accountId, userConnection);
            }

            //Если должность для обогащения главный бухгалтер, добавляем привязку
            if(isAccountPosition == true && itChiefId != null)
            {
                UpdateITChiefAccountantId(itChiefId, accountId, userConnection);
            }
        }

        private void UpdateITChiefAccountantId(Guid? itChiefId, Guid accountId, UserConnection userConnection)
        {
            var updateAccount = new Update(userConnection, "Account")
                .Set("ITChiefAccountantId", Column.Parameter(itChiefId))
                .Where("Id").IsEqual(Column.Parameter(accountId))
                ;
            updateAccount.Execute();
        }

        private void UpdateAccounITChiefId(Guid? itChiefId, Guid accountId, UserConnection userConnection)
        {
            var updateAccount = new Update(userConnection, "Account")
                .Set("ITChiefId", Column.Parameter(itChiefId))
                .Where("Id").IsEqual(Column.Parameter(accountId))
                ;
            updateAccount.Execute();
        }

        private Guid? InsertNewContact(string controlName, Guid accountId, Guid? ownerId, Guid jobId, string controlPost, UserConnection userConnection)
        {
            Guid? id = Guid.NewGuid();
            Guid newContactId = (Guid)id;
            Guid typeId = Guid.Parse("806732EE-F36B-1410-A883-16D83CAB0980");
            //ChangeContactIdentityInsert("ITPwKartotekaContactIdentityInsertOn_Proc", userConnection);
            var insertContact = new Insert(userConnection).Into("Contact")
                .Set("Id", Column.Parameter(newContactId))
                .Set("AccountId", Column.Parameter(accountId))
                //.Set("OwnerId", Column.Parameter(ownerId == null ? null : ownerId))
                .Set("JobId", Column.Parameter(jobId))
                .Set("JobTitle", Column.Parameter(controlPost))
                .Set("TypeId", Column.Parameter(typeId))
                .Set("Confirmed", Column.Parameter(true))
                .Set("Completeness", Column.Parameter(30))
                ;
            /*var insertContact = new Insert(userConnection).Into("Contact")
                .Set("Id", Column.Parameter(newContactId))
                .Set("ProcessListeners", Column.Parameter(0))
                .Set("Name", Column.Parameter(controlName))
                .Set("OwnerId", Column.Parameter(ownerId == null ? null : ownerId))
                .Set("JobId", Column.Parameter(jobId))
                .Set("JobTitle", Column.Parameter(controlPost))
                .Set("TypeId", Column.Parameter(typeId))
                .Set("Confirmed", Column.Parameter(true))
                .Set("Completeness", Column.Parameter(30))
                .Set("Description", Column.Parameter(""))
                .Set("Dear", Column.Parameter(""))
                .Set("Phone", Column.Parameter(""))
                .Set("MobilePhone", Column.Parameter(""))
                .Set("HomePhone", Column.Parameter(""))
                .Set("Skype", Column.Parameter(""))
                .Set("Email", Column.Parameter(""))
                .Set("Address", Column.Parameter(""))
                .Set("Zip", Column.Parameter(""))
                .Set("DoNotUseEmail", Column.Parameter(false))
                .Set("DoNotUseCall", Column.Parameter(false))
                .Set("DoNotUseFax", Column.Parameter(false))
                .Set("DoNotUseSms", Column.Parameter(false))
                .Set("DoNotUseMail", Column.Parameter(false))
                .Set("Notes", Column.Parameter(""))
                .Set("Facebook", Column.Parameter(""))
                .Set("LinkedIn", Column.Parameter(""))
                .Set("Twitter", Column.Parameter(""))
                .Set("FacebookId", Column.Parameter(""))
                .Set("LinkedInId", Column.Parameter(""))
                .Set("TwitterId", Column.Parameter(""))
                .Set("GPSN", Column.Parameter(""))
                .Set("GPSE", Column.Parameter(""))
                .Set("Surname", Column.Parameter(""))
                .Set("GivenName", Column.Parameter(""))
                .Set("MiddleName", Column.Parameter(""))
                .Set("IsNonActualEmail", Column.Parameter(false))
                .Set("RId", Column.Parameter(0))
                ;
                */
            insertContact.Execute();
            //InsertNewContactAcademyUrl((Guid)id, userConnection);
            //ChangeContactIdentityInsert("ITPwKartotekaContactIdentityInsertOff_Proc", userConnection);
            return id;
        }

        private Guid? GetChifIdFromContact(string controlName, UserConnection userConnection)
        {
            Guid? chifId = null;
            var selectContact = new Select(userConnection).From("Contact").Column("Id")
                .Where("Name").IsEqual(Column.Parameter(controlName)) as Select;
            using (DBExecutor executor = userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = selectContact.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        chifId = reader.GetColumnValue<Guid>("Id");
                    }
                }
            }
            return chifId;
        }

        private void InsertNewJobItem(Guid jobId, string controlPost, UserConnection userConnection)
        {
            var insertJob = new Insert(userConnection).Into("Job")
                .Set("Id", Column.Parameter(jobId))
                .Set("ProcessListeners", Column.Parameter(0))
                .Set("Name", Column.Parameter(controlPost))
                .Set("Description", Column.Parameter(""))
                ;
            insertJob.Execute();
        }

        private bool CheckIsAccountPosition(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;
            else
            {
                bool result = value.Contains("ухгалтер");
                return result;
            }
        }

        private List<Job> GetJobList(UserConnection userConnection)
        {
            List<Job> result = new List<Job>();
            var selectJob = new Select(userConnection).From("Job").Column("Id")
                .Column("Name");
            using (DBExecutor executor = userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = selectJob.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        Guid id = reader.GetColumnValue<Guid>("Id");
                        string name = reader.GetColumnValue<string>("Name");
                        Job job = new Job(id, name);
                        result.Add(job);
                    }
                }
            }
            return result;
        }

        //private void EnrichAccount(string accountId, string address, string controlName, string controlPost, string ownerId, UserConnection userConnection)
        //{
        //    var storedProcedure = new StoredProcedure(userConnection, "ITPwEnrichAccountFromKartoteka_Proc")
        //    .WithParameter("AccountId", accountId)
        //    .WithParameter("Address", GetCorrectAcoountField(address, 2048))
        //    .WithParameter("ControlName", GetCorrectAcoountField(controlName, 250))
        //    .WithParameter("ControlPost", GetCorrectAcoountField(controlPost, 250))
        //    .WithParameter("OwnerId", ownerId) as StoredProcedure;
        //    storedProcedure.PackageName = userConnection.DBEngine.SystemPackageName;
        //    storedProcedure.Execute();
        //}



        private void ChangeContactIdentityInsert(string procName, UserConnection userConnection)
        {
            var storedProcedure = new StoredProcedure(userConnection, procName)
                as StoredProcedure;
            storedProcedure.PackageName = userConnection.DBEngine.SystemPackageName;
            storedProcedure.Execute();
        }
        private void UpdateAccount(AccountItem account, ITVwIntegrationKartoteka itVwIntegrationKartoteka, UserConnection userConnection)
        {
            var updateAccount = new Update(userConnection, "Account")
                .Set("TsFullName", Column.Parameter(GetCorrectAcoountField(itVwIntegrationKartoteka.ITFullName, 250)/*itVwIntegrationKartoteka.ITFullName == null ? "" : itVwIntegrationKartoteka.ITFullName*/))
                .Set("ITOKPO", Column.Parameter(GetCorrectAcoountField(itVwIntegrationKartoteka.ITOkpo, 50)/*itVwIntegrationKartoteka.ITOkpo == null ? "" : itVwIntegrationKartoteka.ITOkpo*/))
                .Set("ITOGRN", Column.Parameter(GetCorrectAcoountField(itVwIntegrationKartoteka.ITOgrn, 50)/*itVwIntegrationKartoteka.ITOgrn == null ? "" : itVwIntegrationKartoteka.ITOgrn*/))
                .Set("ITShortName", Column.Parameter(itVwIntegrationKartoteka.ITShortName == null ? "" : itVwIntegrationKartoteka.ITShortName))
                .Set("ITOKOGU", Column.Parameter(GetCorrectAcoountField(itVwIntegrationKartoteka.ITOkogu, 50)/*itVwIntegrationKartoteka.ITOkogu == null ? "" : itVwIntegrationKartoteka.ITOkogu*/))
                .Set("ITOKOGUDescription", Column.Parameter(itVwIntegrationKartoteka.ITOKOGUDescription == null ? "" : itVwIntegrationKartoteka.ITOKOGUDescription))
                .Set("ITOKOPF", Column.Parameter(GetCorrectAcoountField(itVwIntegrationKartoteka.ITOkopf, 50)/*itVwIntegrationKartoteka.ITOkopf == null ? "" : itVwIntegrationKartoteka.ITOkopf*/))
                .Set("ITOKOPFDescription", Column.Parameter(itVwIntegrationKartoteka.ITOkopfName == null ? "" : itVwIntegrationKartoteka.ITOkopfName))
                .Set("ITOKVED", Column.Parameter(GetCorrectAcoountField(itVwIntegrationKartoteka.ITOKVED, 50)/*itVwIntegrationKartoteka.ITOKVED == null ? "" : itVwIntegrationKartoteka.ITOKVED*/))
                .Set("ITOKVEDDescription", Column.Parameter(itVwIntegrationKartoteka.ITOKVEDDescription == null ? "" : itVwIntegrationKartoteka.ITOKVEDDescription))
                .Set("ITStatus", Column.Parameter(GetCorrectAcoountField(itVwIntegrationKartoteka.ITStatus, 50)/*itVwIntegrationKartoteka.ITStatus == null ? "" : itVwIntegrationKartoteka.ITStatus*/))
                .Set("Phone", Column.Parameter(GetCorrectAcoountField(itVwIntegrationKartoteka.ITPhones, 250)/*itVwIntegrationKartoteka.ITPhones == null ? "" : itVwIntegrationKartoteka.ITPhones*/))
                .Where("Id").IsEqual(Column.Parameter(account.Id));
            updateAccount.Execute();
        }

        private string GetCorrectAcoountField(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value))
                return "";
            else
            {
                if (value.Length > maxLength)
                {
                    return value.Substring(0, maxLength).Trim();
                }
                else return value;
            }
        }

        private ITVwIntegrationKartoteka GetITVwIntegrationKartoteka(AccountItem account, UserConnection userConnection)
        {
            ITVwIntegrationKartoteka itVwIntegrationKartoteka = null;
            var selectITVwIntegrationKartoteka = new Select(userConnection)
                .Column("ITOgrn")
                .Column("ITOkpo")
                .Column("ITFullName")
                .Column("ITShortName")
                .Column("ITPhones")
                .Column("ITAddress")
                .Column("ITControlName")
                .Column("ITControlPost")
                .Column("ITOkogu")
                .Column("ITOKOGUDescription")
                .Column("ITOkopf")
                .Column("ITOkopfName")
                .Column("ITOKVED")
                .Column("ITOKVEDDescription")
                .Column("ITStatus")
                .From("ITVwIntegrationKartoteka")
                .Where("ITKpp").IsEqual(Column.Parameter(account.TsKPP.ToString()))
                .And("ITInn").IsEqual(Column.Parameter(account.TsINN))
                as Select;
            using (DBExecutor executor = userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = selectITVwIntegrationKartoteka.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        var itOgrn = reader.GetColumnValue<string>("ITOgrn");
                        var itOkpo = reader.GetColumnValue<string>("ITOkpo");
                        var itFullName = reader.GetColumnValue<string>("ITFullName");
                        var itShortName = reader.GetColumnValue<string>("ITShortName");
                        var itPhones = reader.GetColumnValue<string>("ITPhones");
                        var itAddress = reader.GetColumnValue<string>("ITAddress");
                        var itControlName = reader.GetColumnValue<string>("ITControlName");
                        var itControlPost = reader.GetColumnValue<string>("ITControlPost");
                        var itOkogu = reader.GetColumnValue<string>("ITOkogu");
                        var itOKOGUDescription = reader.GetColumnValue<string>("ITOKOGUDescription");
                        var itOkopf = reader.GetColumnValue<string>("ITOkopf");
                        var itOkopfName = reader.GetColumnValue<string>("ITOkopfName");
                        var itOKVED = reader.GetColumnValue<string>("ITOKVED");
                        var itOKVEDDescription = reader.GetColumnValue<string>("ITOKVEDDescription");
                        var itStatus = reader.GetColumnValue<string>("ITStatus");
                        itVwIntegrationKartoteka = new ITVwIntegrationKartoteka(account.TsKPP, account.TsINN, itOgrn, itOkpo,
                            itFullName, itShortName, itPhones, itAddress, itControlName, itControlPost, itOkogu, itOKOGUDescription,
                            itOkopf, itOkopfName, itOKVED, itOKVEDDescription, itStatus);
                    }
                }
            }
            return itVwIntegrationKartoteka;
        }

        private List<AccountItem> GetAllExistAccounts(UserConnection userConnection)
        {
            List<AccountItem> result = new List<AccountItem>();
            var selectAccount = new Select(userConnection).From("Account").Column("Id")
                .Column("TsKPP").Column("TsINN").Column("OwnerId");
            using (DBExecutor executor = userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = selectAccount.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        Guid? ids = null;
                        try
                        {
                            //throw new Exception("Test4");
                            Guid id = reader.GetColumnValue<Guid>("Id");
                            ids = id;
                            Int64? tsKPP = reader.GetColumnValue<Int64>("TsKPP");
                            string tsINN = reader.GetColumnValue<string>("TsINN");
                            var ownerId = reader.GetColumnValue<Guid>("OwnerId");
                            if (tsKPP != 0 && tsKPP != null && !string.IsNullOrEmpty(tsINN))
                            {
                                AccountItem a = new AccountItem(id, (Int64)tsKPP, tsINN, ownerId);
                                result.Add(a);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Test5, id = " + ((Guid)ids).ToString() +
                            ", Ex = " + ex.Message);
                        }
                    }
                }
            }
            return result;
        }
    }
}
