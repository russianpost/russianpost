namespace Terrasoft.Configuration.ITKartotekaIntegration
{
    using System.CodeDom.Compiler;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.ServiceModel.Activation;
    using System.Runtime.Serialization;
    using System.Web;
    using Terrasoft.Common;
    using Terrasoft.Core;
    using Terrasoft.Core.DB;
    using Terrasoft.Core.Entities;
    using Terrasoft.Core.Store;
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Newtonsoft.Json.Linq;

    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ITEnrichAccountFromKartoteka
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
        public string EnrichAccount(string accountId, string address, string controlName, string controlPost, string ownerId)
        {

            try
            {
            	var userConnection = (UserConnection)HttpContext.Current.Session["UserConnection"];
            	
                var storedProcedure = new StoredProcedure(userConnection, "ITPwEnrichAccountFromKartoteka_Proc")
                .WithParameter("AccountId", accountId)
                .WithParameter("Address", address)
                .WithParameter("ControlName", controlName)
                .WithParameter("ControlPost", controlPost)
                .WithParameter("OwnerId", ownerId) as StoredProcedure;
				storedProcedure.PackageName = userConnection.DBEngine.SystemPackageName;
				storedProcedure.Execute();
            }
            
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Good";
        }
    }
}
