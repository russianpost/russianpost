define("AccountMiniPage", ["ServiceHelper"], function(ServiceHelper) {
	return {

		methods: {

			onSaved: function() {
				this.callParent(arguments);
				var inn = this.get("TsINN");
				var kpp = this.get("TsKPP");
				var accountId = this.get("Id");
				var ownerId = this.get("Owner").value;

				var serviceData = {
					accountId: accountId,
					ownerId: ownerId,
					inn: inn,
					kpp: kpp
				};
				
				if (inn && kpp) {
					ServiceHelper.callService("ITEnrichNewAccountFromKartoteka", "EnrichNewAccount", function(response) {
						//if (!response.success) {
						//	this.showInformationDialog("Ошибка запроса");
						//}
						//if (response.success) {
						//	this.showInformationDialog("Запрос удачен");
						//}
						//var result = response;
						//this.showInformationDialog(result.toString());
					}, serviceData, this);
				}
			}
		},

		diff: [

		]


	};
});