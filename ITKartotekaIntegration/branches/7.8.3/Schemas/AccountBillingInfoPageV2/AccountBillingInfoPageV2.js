define("AccountBillingInfoPageV2", [],
		function() {
			return {
				
				attributes: {
					"ITIsActive": {
						"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
						"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
						"value": true
					}
				},

				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "remove",
						"name": "TsKPP"
					},
					{
						"operation": "remove",
						"name": "TsIdentificationNumber"
					},
					{
						"operation": "remove",
						"name": "Country"
					},
					{
						"operation": "remove",
						"name": "Description"
					},
					{
						"operation": "remove",
						"name": "LegalUnit"
					},
					{
						"operation": "remove",
						"name": "TsOKVED"
					},
					{
						"operation": "remove",
						"name": "TsOKPO"
					},
					{
						"operation": "remove",
						"name": "TsOGRN"
					},
					{
						"operation": "remove",
						"name": "AccountManager"
					},
					{
						"operation": "remove",
						"name": "ChiefAccountant"
					},
					
					{
						"operation": "merge",
						"name": "Name",
						"values": {
							"tip": {
								// Текст подсказки.
								"content": {"bindTo": "Resources.Strings.NameTipContent"},
								// Режим отображения подсказки.
								// По умолчанию режим WIDE - толщина зеленой полоски,
								// которая отображается в подсказке.
								"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
							},
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 12,
								"row": 0
							}
						}
					},
					
					{
						"operation": "merge",
						"name": "TsBankName",
						"values": {
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 0,
								"row": 1
							}
						}
					},
					
					{
						"operation": "merge",
						"name": "TsBIK",
						"values": {
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 12,
								"row": 1
							}
						}
					},
					
					{
						"operation": "merge",
						"name": "TsBankAccountNumber",
						"values": {
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 0,
								"row": 2
							}
						}
					},
					
					{
						"operation": "merge",
						"name": "TsBankCorrespondentAccount",
						"values": {
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 12,
								"row": 2
							}
						}
					},
					
					{
						"operation": "insert",
						"name": "ITIsActiveField",
						"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
						"values": {
							"caption": "Действующий",
							"bindTo": "ITIsActive",
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 0,
								"row": 3
							}
						},
						"parentName": "Header",
						"propertyName": "items",
						"index": 7
					}

				]/**SCHEMA_DIFF*/
			};
		});