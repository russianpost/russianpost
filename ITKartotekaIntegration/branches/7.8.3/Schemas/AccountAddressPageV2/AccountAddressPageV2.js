define("AccountAddressPageV2", ["ConfigurationConstants"],
	function(ConfigurationConstants) {
		return {

			attributes: {
				"Street": {
					"dataValueType": this.Terrasoft.DataValueType.TEXT,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": "Улица"
				},
				"House": {
					"dataValueType": this.Terrasoft.DataValueType.TEXT,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": "Дом"
				},
				"HouseBlock": {
					"dataValueType": this.Terrasoft.DataValueType.TEXT,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": "Корпус"
				},
				"Building": {
					"dataValueType": this.Terrasoft.DataValueType.TEXT,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": "Строение"
				}
			},

			methods: {

				onEntityInitialized: function() {
					this.callParent();
					this.on("change:Street", this.onAdressChanged, this);
					this.on("change:House", this.onAdressChanged, this);
					this.on("change:HouseBlock", this.onAdressChanged, this);
					this.on("change:Building", this.onAdressChanged, this);
					this.setAddressInfo();
				},

				onAdressChanged: function() {
					if (!this.get("Street")) {
						this.set("Address", "");
						return;
					}

					var currentHouse = "";
					var currentHouseBlock = "";
					var currentBuilding = "";
					var currentStreet = "";

					currentStreet = this.get("Street");

					if (this.get("House")) {
						currentHouse = this.get("House");
					}
					if (this.get("HouseBlock")) {
						currentHouseBlock = "к" + this.get("HouseBlock");
					}
					if (this.get("Building")) {
						currentBuilding = "с" + this.get("Building");
					}

					this.set("Address", currentStreet + " " + currentHouse + " " + currentHouseBlock + " " + currentBuilding);
				},
				
				isLetter: function(c) {
					return c.toLowerCase() != c.toUpperCase();
				},
				
				setAddressInfo: function() {
					if (this.get("Address")) {
						var address = this.get("Address");
						var result = address.split(" ");

						if (result[0]) {
							this.set("Street", result[0]);
						}
						if (result[1]) {
							var firstLetter = result[1].charAt(0);
							if (this.isLetter(firstLetter)) {
								this.set("Street", result[0] + " " + result[1]);
								if (result[2]) {
									this.set("House", result[2]);
								}
								if (result[3]) {
									this.set("HouseBlock", result[3].replace("к", ""));
								}
								if (result[4]) {
									this.set("Building", result[4].replace("с", ""));
								}
							} else {
								this.set("House", result[1]);
								if (result[2]) {
									this.set("HouseBlock", result[2].replace("к", ""));
								}
								if (result[3]) {
									this.set("Building", result[3].replace("с", ""));
								}
							}

						}

					}
				},
				
				setValidationConfig: function() {
					this.callParent(arguments);
					this.addColumnValidator("Zip", this.zipValidator);
				},
				
				zipValidator: function(value) {
					var invalidMessage = "Значение поля должно содержать ровно 6 цифр";
					var zip = value || this.get("Zip");
					
					if (zip && zip.length === 6) {
						invalidMessage = "";
					}
					
					return {
						fullInvalisMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
					
				}

			},

			diff: /**SCHEMA_DIFF*/ [
				
				{
					"operation": "remove",
					"name": "AddressType"
				},

				{
					"operation": "merge",
					"name": "Region",
					"values": {
						"caption": "Область",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						}
					}
				},
				
				{
					"operation": "merge",
					"name": "Country",
					"values": {
						"caption": "Страна",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						}
					}
				},

				{
					"operation": "merge",
					"name": "City",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2
						}
					}
				},

				{
					"operation": "merge",
					"name": "Address",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 5
						},
						"visible": false
					}
				},

				{
					"operation": "merge",
					"name": "Zip",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 4
						}
					}
				},

				{
					"operation": "insert",
					"name": "Street",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"bindTo": "Street",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0
						}
					}
				},

				{
					"operation": "insert",
					"name": "House",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"bindTo": "House",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1
						}
					}
				},

				{
					"operation": "insert",
					"name": "HouseBlock",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"bindTo": "HouseBlock",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 2
						}
					}
				},

				{
					"operation": "insert",
					"name": "Building",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"bindTo": "Building",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 3
						}
					}
				}

			] /**SCHEMA_DIFF*/
		};
	});