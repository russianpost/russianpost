IF OBJECT_ID('ITVwAccountOwner', 'V') IS NOT NULL
    DROP VIEW ITVwAccountOwner
GO
create view ITVwAccountOwner as
select coalesce(ac.Id, NEWID()) Id, a.Id ITAccountId, lt.Id ITLeadTypeId, 
ac.ITOwnerId, ac.ITOwnerSupplierId,
ac.CreatedById,
ac.ModifiedById,
ac.CreatedOn,
ac.ModifiedOn,
ac.ProcessListeners,
case when ac.id is null then 0 else 1 end ITSaved
from Account a
cross join LeadType lt 
left join ITAccountCateg ac on ac.ITAccountId = a.id and ac.ITLeadTypeId = lt.Id