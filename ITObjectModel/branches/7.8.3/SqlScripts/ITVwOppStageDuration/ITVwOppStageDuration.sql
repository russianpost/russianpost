IF OBJECT_ID('ITVwOppStageDuration', 'V') IS NOT NULL
    DROP VIEW ITVwOppStageDuration
GO
CREATE VIEW ITVwOppStageDuration AS
SELECT OppInStage.Id ITId,
OppInStage.OpportunityId ITOpportunityId,
OppInStage.StageId ITStageId,
DATEDIFF(DAY,OppInStage.StartDate,COALESCE(OppInStage.DueDate, GETDATE())) ITDuration
FROM OpportunityInStage OppInStage