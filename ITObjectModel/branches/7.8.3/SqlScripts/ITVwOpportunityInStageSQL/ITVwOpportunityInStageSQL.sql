IF OBJECT_ID('ITVwOpportunityInStage', 'V') is not null
  drop view ITVwOpportunityInStage
GO
create view ITVwOpportunityInStage as
select OiS.*,TS.TsNumber from OpportunityInStage OiS
inner join Opportunity O on O.Id=Ois.OpportunityId
inner join TsOppStageInLeadType TS on TS.TsLeadTypeId=O.LeadTypeId and OiS.StageId=TS.TsOpportunityStageId
where Ts.TsNumber<=(select top 1 TsNumber from TsOppStageInLeadType where TsLeadTypeId=O.LeadTypeId and TsOpportunityStageId=O.StageId)
 