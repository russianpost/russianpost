IF OBJECT_ID('ITVwContactAnniversaryForTom', 'V') IS NOT NULL
 DROP VIEW ITVwContactAnniversaryForTom
GO
create view ITVwContactAnniversaryForTom 
as
Select Id, CreatedById, CreatedOn, ModifiedById, ModifiedOn, [ProcessListeners],
[Date] ITDate, [ContactId] ITContactId, [AnniversaryTypeId] ITAnniversaryTypeId
 FROM ContactAnniversary 
Where (day(ContactAnniversary.Date)=day(DATEADD(Day,1,GETDATE()))
AND month(ContactAnniversary.Date)=month(DATEADD(Day,1,GETDATE())));