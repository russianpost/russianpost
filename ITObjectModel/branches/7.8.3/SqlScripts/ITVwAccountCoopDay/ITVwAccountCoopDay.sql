IF OBJECT_ID('ITVwAccountCoopDay', 'V') IS NOT NULL
    DROP VIEW ITVwAccountCoopDay
GO
create view ITVwAccountCoopDay as
select Account.Id ITAccountId, MAX(DATEDIFF(DAY,StartDate, GETDATE())) ITDurationOfCooperation
from Contract
join Account on Account.Id = AccountId
join ContractState on ContractState.Id = StateId
where ContractState.name = 'Подписан'
group by Account.Id