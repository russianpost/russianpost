IF OBJECT_ID('ITVwOpportunitySummary', 'V') IS NOT NULL
    DROP VIEW ITVwOpportunitySummary
GO
create view ITVwOpportunitySummary as
SELECT     o.Id AS ITOpportunity, DATEDIFF(DAY, MIN(os.CreatedOn), MAX(oc.CreatedOn)) AS ITContractingDuration, o.Id AS ITOpportunityLookupId
FROM         dbo.Opportunity AS o INNER JOIN
                      dbo.OpportunityInStage AS os ON os.OpportunityId = o.Id INNER JOIN
                      dbo.Contract AS oc ON oc.ITOpportunityInContractId = o.Id
GROUP BY o.Id