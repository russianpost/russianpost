IF OBJECT_ID('ITVwNoActionsOnClient', 'V') IS NOT NULL
 DROP VIEW ITVwNoActionsOnClient
GO
create view ITVwNoActionsOnClient as
SELECT 
own1.Id, own1.CreatedById, own1.CreatedOn, own1.ModifiedById, own1.ModifiedOn, own1.ProcessListeners,
own1.ITOwnerId ITResponsibleId, 
a1.Id ITClientId,
a1.Name,
act1.StartDate ITActDate
FROM ITVwAccountOwner own1
inner join Account a1 on a1.Id=own1.ITAccountId
inner join AccountType at1 on at1.Id=a1.TypeId 
inner join Activity act1 on (a1.Id=act1.AccountId) and (own1.ITOwnerId=act1.OwnerId)
WHERE (at1.Name='Клиент' AND
act1.StartDate<DATEADD(Day,-30,GETDATE()) AND
act1.StartDate=(SELECT MAX(Activity.StartDate) 
FROM Activity WHERE Activity.OwnerId=own1.ITOwnerId AND Activity.AccountId=own1.ITAccountId)
)
GROUP by
own1.Id, own1.CreatedById, own1.CreatedOn, own1.ModifiedById, own1.ModifiedOn, own1.ProcessListeners,
own1.ITOwnerId, 
a1.Id,
a1.Name,
act1.StartDate
 
UNION ALL

SELECT 
own1.Id, own1.CreatedById, own1.CreatedOn, own1.ModifiedById, own1.ModifiedOn, own1.ProcessListeners,
own1.ITOwnerId ITResponsibleId,
a1.Id ITClientId,
a1.Name,
StartDate=NULL

FROM ITVwAccountOwner own1
inner join Account a1 on a1.Id=own1.ITAccountId
inner join AccountType at1 on at1.Id=a1.TypeId 
WHERE (at1.Name='Клиент' AND
(SELECT COUNT (*) FROM Activity WHERE (Activity.AccountId=a1.Id))=0 AND
a1.CreatedOn<DATEADD(Day,-30,GETDATE()) AND
own1.ITOwnerId IS NOT NULL
)
GROUP by
own1.Id, own1.CreatedById, own1.CreatedOn, own1.ModifiedById, own1.ModifiedOn, own1.ProcessListeners,
own1.ITOwnerId, 
a1.Id,
a1.Name
;

