IF OBJECT_ID('ITVwRoleOfContact', 'V') IS NOT NULL
 DROP VIEW ITVwRoleOfContact
GO

create view ITVwRoleOfContact as
select sr.Id, c.Id ITContactId, sa.Name ITRoleName, c.Email ITEmail, sr.SysAdminUnitRoleId ITRoleSysAdminUnitId,
sr.CreatedById, sr.CreatedOn, sr.ModifiedById, sr.ModifiedOn, sr.ProcessListeners
from SysAdminUnitInRole sr
join SysAdminUnit sa on sa.Id = sr.SysAdminUnitId
join SysAdminUnit sar on sar.Id = sr.SysAdminUnitRoleId
join Contact c on c.Id = sa.ContactId
where sar.Name like '%Группа руководителей%'