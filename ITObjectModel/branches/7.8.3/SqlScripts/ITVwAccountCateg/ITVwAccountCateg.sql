IF OBJECT_ID('ITVwAccountCateg', 'V') IS NOT NULL
    DROP VIEW ITVwAccountCateg
GO
create view ITVwAccountCateg as
select coalesce(ac.Id, NEWID()) Id, a.Id ITAccountId, lt.Id ITLeadTypeId, 
coalesce(ac.ITB2B, 0) ITB2B, 
coalesce(ac.ITB2C, 0) ITB2C, 
coalesce(ac.ITG2B, 0) ITG2B, 
coalesce(ac.ITVIP, 0) ITVIP, 
ac.ITCategId, 
ac.ITAccountLevelId,
ac.CreatedById,
ac.ModifiedById,
ac.CreatedOn,
ac.ModifiedOn,
ac.ProcessListeners,
case when ac.id is null then 0 else 1 end ITSaved
from Account a
cross join LeadType lt 
left join ITAccountCateg ac on ac.ITAccountId = a.id and ac.ITLeadTypeId = lt.Id
GO