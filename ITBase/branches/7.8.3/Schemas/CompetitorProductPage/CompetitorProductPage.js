define("CompetitorProductPage", [],
		function() {
			return {
				entitySchemaName: "CompetitorProduct",
				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "remove",
						"parentName": "Header",
						"propertyName": "items",
						"name": "Weakness"
					},
					{
						"operation": "remove",
						"parentName": "Header",
						"propertyName": "items",
						"name": "Strengths"
					},
					{
						"operation": "insert",
						"parentName": "Header",
						"propertyName": "items",
						"name": "ITWeakSides",
						"values": {
							"bindTo": "ITWeakSides",
							"layout": { "column": 0, "row": 2, "colSpan": 12 },
							"contentType": Terrasoft.ContentType.ENUM
						}
					},
					{
						"operation": "insert",
						"parentName": "Header",
						"propertyName": "items",
						"name": "ITStrongSides",
						"values": {
							"bindTo": "ITStrongSides",
							"layout": { "column": 0, "row": 3, "colSpan": 12 },
							"contentType": Terrasoft.ContentType.ENUM
						}
					}
				]/**SCHEMA_DIFF*/
			};
		});