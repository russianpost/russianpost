define("ITJsLeadConsts", [], function() {
    var disqualifyReason = {
        AlreadyHasContract: "390ca40d-9868-44fa-8edc-296d77d28d3f",
        NoNeed: "cdff2546-724d-43b7-a32e-7e0a0d15c788",
        NoBudget: "5bd9f2fe-cbb6-4d60-bf10-9063fa82b469",
        NotSatisfyClientNeed: "271640da-9a76-44a9-b45f-155480e6dc10",
        ConditionsNotSuit: "03e7a457-095f-412c-972c-ec451fbf8945",
        Other: "c05b61cd-a71e-4415-8cf7-8927d2b854ba"
    };
    return {
        DisqualifyReason: disqualifyReason
    };
});
