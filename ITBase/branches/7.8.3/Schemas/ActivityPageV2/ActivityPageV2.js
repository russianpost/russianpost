define("ActivityPageV2", ["ConfigurationConstants"],
	function(ConfigurationConstants) {
	return {

		entitySchemaName: "Activity",

		attributes: {
			//Атрибут, отвечающий за доступность основных полей.
			"isEditable": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: true
			},
			//Атрибут, отвечающий за доступность поля "Состояние".
			"isStatusEditable": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			//Атрибут поля "Состояние".
			"Status": {
				//При изменении состояния задачи выполняется метод "onStatusChanged".
				onChange: "onStatusChanged"
			},
			//Атрибут для проверки состояния задачи в БД при сохранении изменений.
			"isStatusDone": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			"onOwnerChange": {
				"dependencies": [
					{
						"columns": ["Owner"],
						"methodName": "onOwnerChanged"
					}
				]
			}
		},

		messages: {
			//Сообщение для ActivityParticipantDetailV2, FileDetailV2, EmailDetailV2, CallDetail.
			"onSaveButtonClick": {
				mode: this.Terrasoft.MessageMode.BROADCAST,
				direction: this.Terrasoft.MessageDirectionType.PUBLISH
			},

			"onInitFileDetail": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},

		details: /**SCHEMA_DETAILS*/{
			ITSetOwnerDetail: {
				schemaName: "ITSetOwnerDetail",
				filter: {
					masterColumn: "Id",
					detailColumn: "ITActivity"
				}
			},
			ITChangeOfTimeDetail: {
				schemaName: "ITChangeOfTimeDetail",
				filter: {
					masterColumn: "Id",
					detailColumn: "ITActivity"
				}
			}
		}/**SCHEMA_DETAILS*/,

		methods: {
			//Метод, который получает Id текущего состояния задачи, затем, если Id равен
			//Id статуса "Завершена", делает поля недоступными (кроме поля "Состояние"). В противном случае,
			//оставляет поля доступными для редактирования.
			onStatusChanged: function() {
				var recordId = this.get("Status").value;
				if (recordId === ConfigurationConstants.Activity.Status.Done) {
					this.set("isEditable", false);
				} else {
					this.set("isEditable", true);
				}
			},

			//Метод осуществляет подписку на сообщение в методе init
			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("onInitFileDetail", this.onInitFileDetail, this, ["key1"]);
			},

			//Возвращает значение состояние задачи из БД (true - была завершена при загрузке страницы)
			onInitFileDetail: function(args) {
				return this.get("isStatusDone");
			},

			onEntityInitialized: function() {
				this.callParent(arguments);
				this.changeIsNotEditable();
				this.checkActivityDateOnPageInitialized();
			},

			//Метод запрашивает состояние задачи из БД и если состояние "Завершена", устанавливает
			//в атрибуты соответствующие значения.
			changeIsNotEditable: function() {
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", { rootSchemaName: "Activity"});
				esq.addColumn("Status", "Status");
				esq.getEntity(this.get("Id"), function(result) {
					if (result.success && result.entity &&
							result.entity.get("Status").value === ConfigurationConstants.Activity.Status.Done) {
						this.set("isEditable", false);
						this.set("isStatusEditable", false);
						this.set("isStatusDone", true);
					} else {
						this.set("isEditable", true);
						this.set("isStatusEditable", true);
						this.set("isStatusDone", false);
					}
				}, this);
			},

			//Если при загрузке страницы состояние задачи в БД было "Завершена", то всплывает ошибка, иначе происходит 
			//сохранение изменений.
			save: function() {
				//Если статус в БД !== Завершена
				if (!this.get("isStatusDone")) {
					this.callParent(arguments);
					//Если на странице установлено состояние "Завершена", то поле "Состояние" становится недоступно.
					if (!this.get("isEditable")) {
						this.set("isStatusEditable", false);
						//Устанавливаем атрибут (флаг) завершености активности в положение true
						//чтобы при инициализации деталей отрабатывало сообщение
						this.set("isStatusDone", true);
						this.sandbox.publish("onSaveButtonClick", null, [this.name]);
					}
				} else {
					this.showInformationDialog("Активность была завершена, ее изменение невозможно");
				}
			},

			onSaved: function() {
				this.callParent(arguments);
				this.completedBy();
				this.checkActivityDateOnSaved();
			},

			// Если активность завершили, то на вкладку "История" записывается, кто завершил и дата/время завершения.
			completedBy: function() {
				var activityId = this.get("Id");
				var completedDate = new Date();
				var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "Activity"
				});
				esq.addColumn("Status", "Status");
				esq.getEntity(activityId, function(result) {
					if (result.entity.get("Status").value === ConfigurationConstants.Activity.Status.Done) {
						this.set("ITCompletedBy", Terrasoft.SysValue.CURRENT_USER_CONTACT);
						this.set("ITCompletedDate", completedDate);
					}
				}, this);
			},

			// При изменении колонки "Ответственный", добавляется запись на вкладку "История",
			// кто и когда изменил ответственного.
			onOwnerChanged: function() {
				var activityId = this.get("Id");
				var ownerChangeDate = new Date();
				var insertQuery = this.Ext.create("Terrasoft.InsertQuery", {
					rootSchemaName: "ITSetOwner"
				});
				insertQuery.setParameterValue("ITActivity", activityId, Terrasoft.DataValueType.GUID);
				insertQuery.setParameterValue("ITContact", Terrasoft.SysValue.CURRENT_USER_CONTACT.value,
					Terrasoft.DataValueType.GUID);
				insertQuery.setParameterValue("ITDate", ownerChangeDate, Terrasoft.DataValueType.DATE_TIME);
	//			return insertQuery;
				insertQuery.execute();
				this.updateDetail({detail: "ITSetOwnerDetail", reloadAll: true});
			},

			// Записывает дату и время начала и завершения активности.
			checkActivityDateOnPageInitialized: function() {
				this.dateTime = [];
				this.dateTime[0] = this.get("StartDate");
				this.dateTime[1] = this.get("DueDate");
			},

			// Если дата/время начала или завершения активности были изменены, обновляет деталь "Изменение времени".
			checkActivityDateOnSaved: function() {
				var dt = [];
				dt[0] = this.get("StartDate");
				dt[1] = this.get("DueDate");
				if (dt[0] === this.dateTime[0] && dt[1] === this.dateTime[1]) {
					return;
				} else {
					this.updateDetail({detail: "ITChangeOfTimeDetail"});
				}
			},

			onDelegateActivityClick: function() {
				var status = this.get("Status").value;
				if (status && status === "4bdbb88f-58e6-df11-971b-001d60e938c6") {
					this.showInformationDialog("Нельзя делегировать задачу в состоянии: Завершена");
					return;
				}
				this.callParent();
			}
		},

		rules: {
			"Result": {
				"BindParameterEnabledResultToStatus": {
					"ruleType": 999
				},
				"BindParameterRequiredResultToStatus": {
					"ruleType": 999
				}
			},
			"DetailedResult": {
				"BindParameterEnabledDetailedResultToStatus": {
					"ruleType": 999
				}
			}
		},

		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "Title",
				"values": {
					"enabled": {bindTo: "isEditable"}
				}
			},
			{
				"operation": "merge",
				"name": "StartDate",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "StartDateTime",
				"values": {
					"enabled": {bindTo: "isEditable"}
				}
			},
			{
				"operation": "merge",
				"name": "Owner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "move",
				"name": "Owner",
				"parentName": "Header",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "merge",
				"name": "DueDate",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					}
				}
			},
			{
				"operation": "merge",
				"name": "DueDateTime",
				"values": {
					"enabled": {bindTo: "isEditable"}
				}
			},
			{
				"operation": "merge",
				"name": "Author",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2
					}
				}
			},
			{
				"operation": "merge",
				"name": "Status",
				"values": {
					"enabled": {bindTo: "isStatusEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3
					}
				}
			},
			{
				"operation": "move",
				"name": "Status",
				"parentName": "Header",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "merge",
				"name": "Priority",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3
					}
				}
			},
			{
				"operation": "merge",
				"name": "ShowInScheduler",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "ActivityCategory",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4
					}
				}
			},
			{
				"operation": "move",
				"name": "ActivityCategory",
				"parentName": "Header",
				"propertyName": "items",
				"index": 9
			},
			{
				"operation": "merge",
				"name": "TsBusinessTypes",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 20
					},
					"visible": false
				}
			},
			{
				"operation": "merge",
				"name": "CallDirection",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5
					}
				}
			},
			{
				"operation": "move",
				"name": "CallDirection",
				"parentName": "Header",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "merge",
				"name": "CustomActionSelectedResultControlGroup",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"parentName": "ResultControlBlock",
				"propertyName": "items",
				"name": "Result",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"parentName": "ResultControlBlock",
				"propertyName": "items",
				"name": "DetailedResult",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 24,
						"rowSpan": 3,
						"column": 0,
						"row": 1
					},
					"contentType": Terrasoft.ContentType.LONG_TEXT
				}
			},
			{
				"operation": "merge",
				"name": "RemindToOwner",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "RemindToOwnerDate",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "RemindToAuthor",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "RemindToAuthorDate",
				"values": {
					"enabled": {bindTo: "isEditable"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "remove",
				"name": "ConnectionWithProjectControlGroup"
			},
			{
				"operation": "remove",
				"name": "FullProjectName"
			},
			{
				"operation": "move",
				"name": "InformationOnStepButtonContainer",
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "move",
				"name": "ESNTab",
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 5
			},
			{
				"operation": "merge",
				"name": "Notes",
				"values": {
					"enabled": {bindTo: "isEditable"}
				}
			},
			{
				"operation": "insert",
				"parentName": "Tabs",
				"propertyName": "tabs",
				"name": "HistoryTab",
				"values": {
					"caption": {"bindTo": "Resources.Strings.HistoryTabCaption"},
					"items": []
				},
				"index": 2
			},
			{
				"operation": "insert",
				"parentName": "HistoryTab",
				"name": "HistoryControlGroup",
				"propertyName": "items",
				"values": {
					"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
					"items": []
				}
			},
			{
				"operation": "insert",
				"parentName": "HistoryControlGroup",
				"propertyName": "items",
				"name": "HistoryControlBlock",
				"values": {
					"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
					"items": []
				}
			},
			{
				"operation": "insert",
				"parentName": "HistoryControlBlock",
				"propertyName": "items",
				"name": "CreatedBy",
				"values": {
					"bindTo": "CreatedBy",
					"layout": {"column": 0, "row": 0, "colSpan": 12},
					"enabled": false,
					"contentType": Terrasoft.ContentType.ENUM
				}
			},
			{
				"operation": "insert",
				"parentName": "HistoryControlBlock",
				"propertyName": "items",
				"name": "CreatedOn",
				"values": {
					"bindTo": "CreatedOn",
					"layout": {"column": 13, "row": 0, "colSpan": 12},
					"enabled": false,
					"contentType": Terrasoft.ContentType.DATE_TIME
				}
			},
			{
				"operation": "insert",
				"parentName": "HistoryControlBlock",
				"propertyName": "items",
				"name": "ITCompletedBy",
				"values": {
					"bindTo": "ITCompletedBy",
					"layout": {"column": 0, "row": 1, "colSpan": 12},
					"enabled": false,
					"contentType": Terrasoft.ContentType.LOOKUP
				}
			},
			{
				"operation": "insert",
				"parentName": "HistoryControlBlock",
				"propertyName": "items",
				"name": "ITCompletedDate",
				"values": {
					"bindTo": "ITCompletedDate",
					"layout": {"column": 13, "row": 1, "colSpan": 12},
					"enabled": false,
					"contentType": Terrasoft.ContentType.DATE_TIME
				}
			},
			{
				"operation": "insert",
				"parentName": "HistoryTab",
				"propertyName": "items",
				"name": "ITSetOwnerDetail",
				"values": {
					"itemType": Terrasoft.ViewItemType.DETAIL
				},
				"index": 1
			},
			{
				"operation": "insert",
				"parentName": "HistoryTab",
				"propertyName": "items",
				"name": "ITChangeOfTimeDetail",
				"values": {
					"itemType": Terrasoft.ViewItemType.DETAIL
				},
				"index": 2
			}
		]/**SCHEMA_DIFF*/
	};
});
