define("ITJsActivityConsts", [], function() {

	//Добавлены результаты активности по задаче: https://it-bpm.atlassian.net/browse/RPCRM-800
	var activityResult = {
		Meeting: "9b4b4971-cf21-40dd-a60c-7389fb9e756c",
		NeedCall: "16a345c0-7319-4c3f-ac44-86e072f2a117",
		DontCall: "dd568373-9ec1-476a-bed7-72e8f2295a83",
		Other: "07250142-ef79-477a-8b52-c8ec62ef3255"
	};

	var activityPriority = {
		Low: "ac96fa02-7fe6-df11-971b-001d60e938c6",
		Middle: "ab96fa02-7fe6-df11-971b-001d60e938c6",
		High: "d625a9fc-7ee6-df11-971b-001d60e938c6"
	};

	var activityCategory = {
		Meeting: "42c74c49-58e6-df11-971b-001d60e938c6",
		Call: "e52bd583-7825-e011-8165-00155d043204"
	};

	var activityStatus = {
		NotStarted: "384d4b84-58e6-df11-971b-001d60e938c6",
		InProgress: "394d4b84-58e6-df11-971b-001d60e938c6",
		Finished: "4bdbb88f-58e6-df11-971b-001d60e938c6",
		Canceled: "201cfba8-58e6-df11-971b-001d60e938c6"
	};

	var callDirection = {
		Incoming: "1d96a65f-2131-4916-8825-2d142b1000b2",
		Outgoing: "53f71b5f-7e17-4cf5-bf14-6a59212db422",
		Indefined: "c072be2c-3d82-4468-9d4a-6db47d1f4cca"
	};

	return {
		ActivityResult: activityResult,
		ActivityPriority: activityPriority,
		ActivityCategory: activityCategory,
		ActivityStatus: activityStatus,
		CallDirection: callDirection
	};
});
