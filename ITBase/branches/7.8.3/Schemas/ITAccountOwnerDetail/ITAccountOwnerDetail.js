define("ITAccountOwnerDetail", ["ConfigurationGrid", "ConfigurationGridGenerator",
	"ConfigurationGridUtilities", "ProcessModuleUtilities"],
	function(ConfigurationGrid, ConfigurationGridGenerator, ConfigurationGridUtilities, ProcessModuleUtilities) {
	return {
		messages: {
		},
		// Название схемы объекта детали.
		entitySchemaName: "ITVwAccountOwner",
		// Перечень атрибутов схемы.
		attributes: {
			// Признак возможности редактирования.
			"IsEditable": {
				// Тип данных — логический.
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				// Тип атрибута — виртуальная колонка модели представления.
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				// Устанавливаемое значение.
				value: true
			}
		},
		diff: [
			{
				// Тип операции — слияние.
				"operation": "merge",
				// Название элемента схемы, над которым производится действие.
				"name": "DataGrid",
				// Объект, свойства которого будут объединены со свойствами элемента схемы.
				"values": {
					// Имя класса
					"className": "Terrasoft.ConfigurationGrid",
					// Генератор представления должен генерировать только часть представления.
					"generator": "ConfigurationGridGenerator.generatePartial",
					// Привязка события получения конфигурации элементов редактирования
					// активной строки к методу-обработчику.
					"generateControlsConfig": {"bindTo": "generatActiveRowControlsConfig"},
					// Привязка события смены активной записи к методу-обработчику.
					"changeRow": {"bindTo": "changeRow"},
					// Привязка события отмены выбора записи к методу-обработчику.
					"unSelectRow": {"bindTo": "unSelectRow"},
					// Привязка  события клика на реестре к методу-обработчику.
					"onGridClick": {"bindTo": "onGridClick"},
					// Действия, производимые с активной записью.
					"activeRowActions": [
						// Настройка действия [Сохранить].
						{
							// Имя класса элемента управления, с которым связано действие.
							"className": "Terrasoft.Button",
							// Стиль отображения — прозрачная кнопка.
							"style": this.Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
							// Тег.
							"tag": "save",
							// Значение маркера.
							"markerValue": "save",
							// Привязка к изображению кнопки.
							"imageConfig": {"bindTo": "Resources.Images.SaveIcon"}
						},
						// Настройка действия [Отменить].
						{
							"className": "Terrasoft.Button",
							"style": this.Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
							"tag": "cancel",
							"markerValue": "cancel",
							"imageConfig": {"bindTo": "Resources.Images.CancelIcon"}
						}
					],
					// Привязка к методу, который инициализирует подписку на события
					// нажатия кнопок в активной строке.
					"initActiveRowKeyMap": {"bindTo": "initActiveRowKeyMap"},
					// Привязка события выполнения действия активной записи к методу-обработчику.
					"activeRowAction": {"bindTo": "onActiveRowAction"},
					// Признак возможности выбора нескольких записей.
					"multiSelect": false,
					"isPageable" : false
				}
			}
		],
		// Используемые классы с дополнительной функциональностью.
		mixins: {
			ConfigurationGridUtilites: "Terrasoft.ConfigurationGridUtilities"
		},
		methods: {
			init: function() {
				this.callParent(arguments);
				this.systemColumns.push("ITLeadType");
			},
			fillParams: function(row, query) {
				if (row.changedValues.ITOwner !== undefined) {
					query.setParameterValue("ITOwner",
						row.changedValues.ITOwner ? row.changedValues.ITOwner.value: null,
						this.Terrasoft.DataValueType.GUID);
				}
				if (row.changedValues.ITOwnerSupplier !== undefined) {
					query.setParameterValue("ITOwnerSupplier",
						row.changedValues.ITOwnerSupplier ? row.changedValues.ITOwnerSupplier.value : null,
						this.Terrasoft.DataValueType.GUID);
				}
			},
			getEmptyGuid: function() {
				return "00000000-0000-0000-0000-000000000000";
			},
			runAccessRightsProcess: function(row) {
				var params = {};
				if (row.values.ITOwner) {
					params.OldResponsible = row.values.ITOwner.value;
					params.NewResponsible = params.OldResponsible;
				}
				if (row.changedValues.ITOwner) {
					params.NewResponsible = row.changedValues.ITOwner.value;
					params.Account = this.values.MasterRecordId;
					console.log(params);
					
					ProcessModuleUtilities.executeProcess({
						sysProcessName: "ITRightsForAccountInstallation",
						parameters: params
					});
				}
				params = {};
				if (row.values.ITOwnerSupplier) {
					params.OldResponsible = row.values.ITOwnerSupplier.value;
					params.NewResponsible = params.OldResponsible;
				}
				if (row.changedValues.ITOwnerSupplier) {
					params.NewResponsible = row.changedValues.ITOwnerSupplier.value;
					params.Account = this.values.MasterRecordId;
					console.log(params);
					ProcessModuleUtilities.executeProcess({
						sysProcessName: "ITRightsForAccountInstallation",
						parameters: params
					});
				}
			},
			insertRow: function(row) {
				var insert = this.Ext.create("Terrasoft.InsertQuery", {rootSchemaName: "ITAccountCateg"});
				insert.setParameterValue("Id", row.values.Id, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("ITAccount", row.values.ITAccount.value, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("ITLeadType", row.values.ITLeadType.value, this.Terrasoft.DataValueType.GUID);
				this.fillParams(row, insert);
                var detailRow = {
                    values: this.Terrasoft.deepClone(row.values),
                    changedValues: this.Terrasoft.deepClone(row.changedValues)
                };
				insert.execute(function(result) {
                    if (result.success) {
                        this.runAccessRightsProcess(detailRow);
                    }
                }, this);
			},
			updateRow: function(row) {
				var update = this.Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "ITAccountCateg"});
				var filterId = update.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Id", row.values.Id);
				update.filters.add("filterId", filterId);
				this.fillParams(row, update);
                var detailRow = {
                    values: this.Terrasoft.deepClone(row.values),
                    changedValues: this.Terrasoft.deepClone(row.changedValues)
                };
                update.execute(function(result) {
                    if (result.success) {
                        this.runAccessRightsProcess(detailRow);
                    }
                }, this);
			},
			defaultSaveRowChanges: function(row, callback, scope) {
				scope = scope || this;
				callback = callback || this.Terrasoft.emptyFn;
				if (row && this.getIsRowChanged(row)) {
					row.save({
						callback: callback,
						isSilent: true,
						scope: scope
					});
				} else {
					callback.call(scope);
				}
			},
			saveRowChanges: function(row, callback, scope) {
				scope = scope || this;
				callback = callback || this.Terrasoft.emptyFn;
				if (row && this.getIsRowChanged(row)) {
					if (row.values.ITSaved) {
						this.updateRow(row);
					} else {
						this.insertRow(row);
					}
					for (var key in row.changedValues) {
						row.values[key] = row.changedValues[key];
					}
					row.changedValues = {IsChanged: false};

					row.save({
						callback: callback,
						isSilent: true,
						scope: scope
					});
					row.values.ITSaved = true;
				} else {
					callback.call(scope);
				}
			},
			addToolsButtonMenuItems: function(toolsButtonMenu) {
				this.addGridOperationsMenuItems(toolsButtonMenu);
			}
		}
	};
});