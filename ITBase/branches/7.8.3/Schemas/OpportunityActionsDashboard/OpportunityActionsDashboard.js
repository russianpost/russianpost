define("OpportunityActionsDashboard", ["ITBaseDashboardItemViewConfig", "ITActivityDashboardItemViewModel",
	"ITProcessDashboardItemViewModel", "css!ITActionsDashboardCSS"], function() {
	return {
		messages: {

			"ContinueLoadingDashboard": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			}

		},
		methods: {

			initDashboardConfig: function() {
				this.callParent(arguments);
				var dashboardConfig = this.get("DashboardConfig");
				dashboardConfig.VwProcessDashboard.viewModelClassName = "Terrasoft.ITProcessDashboardItemViewModel";
				dashboardConfig.VwProcessDashboard.viewConfigClassName = "Terrasoft.ITBaseDashboardItemViewConfig";
				dashboardConfig.Activity.viewModelClassName = "Terrasoft.ITActivityDashboardItemViewModel";
				dashboardConfig.Activity.viewConfigClassName = "Terrasoft.ITBaseDashboardItemViewConfig";
				this.set("DashboardConfig", dashboardConfig);
			},

			getParentMethod: function() {
				var method,
					superMethod = (method = this.getParentMethod.caller) && (method.$previous ||
						((method = method.$owner ? method : method.caller) &&
						method.$owner.superclass[method.$name]));
				return superMethod;
			},

			init: function() {
				var parentSave = this.getParentMethod();
				var parentArguments = arguments;
				var leadType = this.getMasterEntityParameterValue("LeadType");
				if (leadType) {
					parentSave.apply(this, parentArguments);
				}
				else {
					var sandbox = this.sandbox;
					sandbox.subscribe("ContinueLoadingDashboard", function() {
						parentSave.apply(this, parentArguments);
					}, this, [sandbox.id]);
				}
			},
			setActiveActionId: function() {
				var columnName = this.get("ActionsColumnName");
				var oldActiveActionId = this.get("ActiveActionId");
				var currentAction = this.getMasterEntityParameterValue(columnName);
				if (currentAction) {
					this.set("ActiveActionId", currentAction.value);
					if (oldActiveActionId && (oldActiveActionId !== currentAction.value)) {
						this.setMasterEntityParameterValue(columnName, currentAction);
						this.saveMasterEntity();
					}
				}
			}
		},
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
	};
});
