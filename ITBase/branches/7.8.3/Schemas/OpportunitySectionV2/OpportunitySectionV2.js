define("OpportunitySectionV2", ["ConfigurationEnums"], function(ConfigurationEnums) {
	return {
		entitySchemaName: "Opportunity",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		methods: {
			init: function() {
				this.callParent(arguments);
				this.set("UseStaticFolders", true);
			},
			onRender: function() {
				var restored = this.get("Restored");
				var historyStateInfo = this.getHistoryStateInfo();
				if (!restored && historyStateInfo.workAreaMode !== ConfigurationEnums.WorkAreaMode.COMBINED) {
					this.showFolderTree();
				}
				this.set("IgnoreFilterUpdate", false);
				this.callParent(arguments);
			}
		},
		diff: /**SCHEMA_DIFF*/[
			//Удаление кнопки "Добавить заказ"
			{
				"operation": "remove",
				"name": "CreateOrderFromOpportunityButton"
			}
		]/**SCHEMA_DIFF*/
	};
});
