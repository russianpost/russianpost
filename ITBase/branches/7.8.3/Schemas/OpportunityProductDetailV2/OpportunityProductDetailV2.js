define("OpportunityProductDetailV2", [], function() {
	return {
		// Название схемы объекта детали.
		entitySchemaName: "OpportunityProductInterest",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		messages: {
			"OpportunityProductTotalsChanged": {
					mode: Terrasoft.MessageMode.BROADCAST,
					direction: Terrasoft.MessageDirectionType.PUBLISH
				}
			},
			methods: {
				updateTotals: function() {
		//			Подсчитываем Amount после добавления новой Услуги
					var sum = 0, sumNDS = 0;
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "OpportunityProductInterest"});
					esq.addColumn("ITAmount", "ITAmount");
					esq.addColumn("ITAmountNds", "ITAmountNds");
					esq.filters.add("esqFirstFilter", esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
						"Opportunity", this.get("MasterRecordId")));
					esq.getEntityCollection(function(result) {
						result.collection.each(function(item) {
							sum += item.values.ITAmount;
							sumNDS += item.values.ITAmountNds;
						});
			//			Создаем экземпляр UpdateQuery
						var update = this.Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "Opportunity"});
			//			Добавляем фильтр по Id текущей продажи
						update.filters.add("IdFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Id", this.get("MasterRecordId")));
			//			Записываем текущее значение колонки Amount в соответствующую ячейку в БД
						update.setParameterValue("Amount", sum, this.Terrasoft.DataValueType.MONEY);
						update.setParameterValue("ITAmountNDS", sumNDS, this.Terrasoft.DataValueType.MONEY);
						update.execute(function() {
							this.reloadGridData();
							this.sandbox.publish("OpportunityProductTotalsChanged", true, []);
						}, this);
					}, this);
				},
				updateDetail: function() {
					this.callParent(arguments);
					this.updateTotals();
				},
				onDeleted: function() {
					this.callParent(arguments);
					this.updateTotals();
				},
				onEntityInitilized: function()
				{
					this.callParent(arguments);
					this.updateTotals();
				}
			},
			diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
		};
});