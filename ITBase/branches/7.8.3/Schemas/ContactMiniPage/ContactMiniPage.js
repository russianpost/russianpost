define("ContactMiniPage", ["ConfigurationConstants", "ITJsConsts", "BusinessRuleModule", "ContactMiniPageResources"],
function(ConfigurationConstants, ITJsConsts, BusinessRuleModule, resources) {
	return {
		
		attributes: {
			//Фильтрация поля "Макрорегион" по типу оргструктуры АУП и МРЦ
			"ITMacroregion": {
				"dataValueType": Terrasoft.DataValueType.LOOKUP,
				"lookupListConfig": {
					"filters": [
						function() {
							var filterGroup = this.Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("ITMacroregionByOrgStructure",
							this.Terrasoft.createColumnInFilterWithParameters("ITOrgStructure",
								[ITJsConsts.OrgStructure.MRC.toLowerCase(), ITJsConsts.OrgStructure.AUP.toLowerCase()]
							));
							return filterGroup;
						}
					]
				}
			},
			//Фильтрация поля "Тип" в зависимости от того, является ли текущий пользователь Администратором
			"Type": {
				"isRequired": true,
				"dataValueType": Terrasoft.DataValueType.LOOKUP,
				"lookupListConfig": {
					"filters": [
						function() {
							var filterGroup = this.Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("TypeByIsAdministrator",
							this.Terrasoft.createColumnInFilterWithParameters("Id", this.array
							));
							return filterGroup;
						}
					]
				}
			}
		},
		
		rules: {
			//Правила для поля "Филиал"
			"TsAccount": {
				//Обязательность заполнения - удалено
				"RequireIfContactIsEmployee": {
					"ruleType": 999
				},
				
				//фильтруется по "Тип" = "Наша компания" - удалено
				"FiltrationByOurCompanyType": {
					"ruleType": 999
				},
				
				//фильтруется по Родительскому нотрагенту Филиала
				"FiltrationByUFPS": {
					"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
					"autocomplete": false,
					"baseAttributePatch": "Parent",
					"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
					"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
					"attribute": "ITMacroregion"
				}
			},
			
			//Правила для поля "Макрорегион"
			"ITMacroregion": {
				//Обязательность заполнения "Макрорегион"
				"RequireIfContactIsEmployee": {
					ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
					property: BusinessRuleModule.enums.Property.REQUIRED,
					conditions: [
						{
							"leftExpression": {
								"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								"attribute": "Type"
							},
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"rightExpression": {
								"type": BusinessRuleModule.enums.ValueType.CONSTANT,
								"value": ConfigurationConstants.ContactType.Employee
							}
						}
					]
				},
				//Видимость "Макрорегион"
				"VisibleIfContactIsEmployee": {
					ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
					property: BusinessRuleModule.enums.Property.VISIBLE,
					conditions: [
						{
							"leftExpression": {
								"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								"attribute": "Type"
							},
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"rightExpression": {
								"type": BusinessRuleModule.enums.ValueType.CONSTANT,
								"value": ConfigurationConstants.ContactType.Employee
							}
						}
					]
				}
			}
		},
		
		methods: {
			
			onEntityInitialized: function() {
				this.getUserSaveRights();
				//Задаем массив для фильтрации поля "Тип"
				this.array = [];
				this.callParent(arguments);
			},
			
			//Переопределяем метод Ts, чтобы избавиться от метода includes, который не поддерживается IE11-
			actualizeAttributes: function(columnNames) {
				if (Ext.isEmpty(columnNames) || columnNames === "Type") {
					this.set("IsContactEmployee", this.get("Type") &&
						ConfigurationConstants.ContactType.Employee === this.get("Type").value);
				}
				if (Ext.isEmpty(columnNames) || columnNames === "TsAccountOldValue") {
					this.set("TsAccountOldValue", this.get("TsAccount"));
				}
			},
			
			//Переопределен, чтобы в actualizeAttributes передавать значение, а не массив
			onTypeChanged: function() {
				this.actualizeAttributes("Type");
			},
			
			//Переопределен, чтобы в actualizeAttributes передавать значение, а не массив
			onTsAccountChanged: function() {
				var account = this.get("Account");
				var tsAccountOldValue = this.get("TsAccountOldValue");
				var tsAccount = this.get("TsAccount");

				if (account) {
					if (tsAccountOldValue && tsAccountOldValue.value === account.value) {
						this.set("Account", tsAccount);
					}
				} else {
					if (Ext.isEmpty(tsAccountOldValue)) {
						this.set("Account", tsAccount);
					}
				}
				this.actualizeAttributes("TsAccountOldValue");
			},
			
			//Метод для проверки является ли пользователь Администратором
			getUserSaveRights: function() {
				var employee = ITJsConsts.ContactType.Employee.value, currentUser = Terrasoft.SysValue.CURRENT_USER.value,
				sysAdmins = ConfigurationConstants.SysAdminUnit.Id.SysAdministrators, massive = [];
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "SysUserInRole"
				});
				esq.addColumn("SysRole");
				esq.addColumn("SysUser");
				esq.filters.add("SysUser", Terrasoft.createColumnFilterWithParameter(
					Terrasoft.ComparisonType.EQUAL, "SysUser", currentUser));
				esq.filters.add("SysRole", Terrasoft.createColumnFilterWithParameter(
					Terrasoft.ComparisonType.EQUAL, "SysRole", sysAdmins));
				esq.getEntityCollection(function(response) {
					if (response && response.success) {
						var result = response.collection;
						var isSysAdmin = (result.collection.length !== 0);
						
						//Записываем в массив Id всех строк справочника, за исключением "Сотрудника"
						var lookup = Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "ContactType"
						});
						lookup.addColumn("Id");
						lookup.filters.add("ById", Terrasoft.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.NOT_EQUAL, "Id", employee));
						lookup.getEntityCollection(function(respon) {
							respon.collection.each(function(item) {
								massive.push(item.get("Id"));
							});
							//Если пользователь является Администратором, то в массив добавляется его Id
							if (isSysAdmin) {
								massive.push(employee);
								this.array = massive;
							} else {
								this.array = massive;
							}
						}, this);
					}
				}, this);
			}
		},
		
		diff: [
			{
				"operation": "remove",
				"name": "TsLeadType"
			},
			{
				"operation": "remove",
				"name": "ProfileFlexibleContainer"
			},
			{
				"operation": "insert",
				"parentName": "MiniPage",
				"propertyName": "items",
				"name": "ITProfileContainer",
				"values": {
					"isMiniPageModelItem": true,
					"itemType": this.Terrasoft.ViewItemType.CONTAINER,
					"visible": {"bindTo": "isNotViewMode"},
					"layout": {
						"column": 0,
						"row": 5,
						"colSpan": 24
					},
					"items": []
				}
			},
			{
				"operation": "move",
				"name": "Department",
				"parentName": "ITProfileContainer",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "Department",
				"values": {
					"caption": resources.localizableStrings.DepartmentCaption,
					"layout": {
						"row": 0,
						"column": 0,
						"colSpan": 24
					}
				}
			},
			{
				"operation": "move",
				"name": "Job",
				"parentName": "ITProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "Job",
				"values": {
					"layout": {
						"row": 1,
						"column": 0,
						"colSpan": 24
					}
				}
			},
			{
				"operation": "move",
				"name": "JobTitle",
				"parentName": "ITProfileContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "merge",
				"name": "JobTitle",
				"values": {
					"layout": {
						"row": 2,
						"column": 0,
						"colSpan": 24
					}
				}
			},
			{
				"operation": "move",
				"name": "MobilePhone",
				"parentName": "ITProfileContainer",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "merge",
				"name": "MobilePhone",
				"values": {
					"layout": {
						"row": 3,
						"column": 0,
						"colSpan": 24
					}
				}
			},
			{
				"operation": "move",
				"name": "Email",
				"parentName": "ITProfileContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "merge",
				"name": "Email",
				"values": {
					"layout": {
						"row": 4,
						"column": 0,
						"colSpan": 24
					}
				}
			},
			{
				"operation": "move",
				"name": "TsTarifficatorRegion",
				"parentName": "ITProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "merge",
				"name": "TsTarifficatorRegion",
				"values": {
					"isMiniPageModelItem": true,
					"visible": {bindTo: "IsContactEmployee"},
					"contentType": this.Terrasoft.ContentType.LOOKUP,
					"layout": {
						"row": 5,
						"column": 0,
						"colSpan": 24
					}
				}
			},
			//Добавление поля "Макрорегион"
			{
				"operation": "insert",
				"parentName": "ITProfileContainer",
				"propertyName": "items",
				"name": "ITMacroregion",
				"values": {
					"isMiniPageModelItem": true,
					"bindTo": "ITMacroregion",
					"contentType": this.Terrasoft.ContentType.LOOKUP,
					"layout": {
						"row": 6,
						"column": 0,
						"colSpan": 24
					},
					"index": 6,
					"showValueAsLink": false
				}
			},
			{
				"operation": "move",
				"name": "TsAccount",
				"parentName": "ITProfileContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "merge",
				"name": "TsAccount",
				"values": {
					"visible": {bindTo: "IsContactEmployee"},
					"contentType": this.Terrasoft.ContentType.LOOKUP,
					"layout": {
						"row": 7,
						"column": 0,
						"colSpan": 24
					},
					"showValueAsLink": false
				}
			},
			{
				"operation": "merge",
				"parentName": "MiniPage",
				"propertyName": "items",
				"name": "Account",
				"values": {
					"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
					"contentType": this.Terrasoft.ContentType.LOOKUP,
					"isMiniPageModelItem": true,
					"visible": {"bindTo": "isNotViewMode"},
					"layout": {
						"column": 0,
						"row": 3,
						"colSpan": 24
					}
				}
			}
		]
	};
});
