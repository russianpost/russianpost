define("ActivitySectionV2", ["ProcessModuleUtilities", "ITScheduleItem"],
function(ProcessModuleUtilities) {
	return {
		mixins: {},

		attributes: {},

		messages: {
			"AddNewRecordOnCallEnd": {
				mode: this.Terrasoft.MessageMode.BROADCAST,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},

		methods: {

			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("AddNewRecordOnCallEnd", this.reloadGridOnSaved, this, ["callKey"]);
			},

			reloadGridOnSaved: function(args) {
				setTimeout(function(scope) {
					scope.reloadGridData();
				}, 2000, this);
			},

			scheduleItemTitleMouseOver: function(options, entityInfo) {
				entityInfo.referenceSchemaName = this.entitySchemaName;
				this.prepareMiniPageOpenParameters(options, entityInfo);
			}
		},

		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,

		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,

		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
	};
});
