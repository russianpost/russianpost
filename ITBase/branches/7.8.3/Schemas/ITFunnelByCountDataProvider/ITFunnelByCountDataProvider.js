define("ITFunnelByCountDataProvider", ["ext-base",
        "terrasoft", "ITFunnelByCountDataProviderResources",
        "FunnelByCountDataProvider"],
    function(Ext, Terrasoft, resources) {
        // Определение нового провайдера расчетов.
        Ext.define("Terrasoft.configuration.ITFunnelByCountDataProvider", {
            extend: "Terrasoft.FunnelByCountDataProvider",
            alternateClassName: "Terrasoft.ITFunnelByCountDataProvider",

            initOpportunityStageCollection: function(callback, scope) {
                var entitySchemaQuery = this.getStagesEsq();
                entitySchemaQuery.getEntityCollection(function(response) {
                    if (response && response.success) {
                        this.opportunityStageCollection = response.collection;
                        callback.call(scope);
                    }
                }, this);
            },

            getFunnelAllowedStagesFilters: function(parentSchemaName) {
                var filter = parentSchemaName ?
                    "[OpportunityInStage:Opportunity].Stage.[TsOppStageInLeadType:TsOpportunityStage].ITShowInFunnel" :
                    "[TsOppStageInLeadType:TsOpportunityStage].ITShowInFunnel";
                return Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
                    filter, true, Terrasoft.DataValueType.BOOLEAN);
            },

            getFunnelFixedFilters: function() {
                var esqFiltersGroup = this.callParent(arguments);
                if (this.historyState.leadType) {
                    esqFiltersGroup.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
                        "LeadType.Id", this.historyState.leadType.value, Terrasoft.DataValueType.GUID));
                }
                return esqFiltersGroup;
            },

            getStagesEsq: function() {
                var currentCultureId = Terrasoft.SysValue.CURRENT_USER_CULTURE.value;
                var cacheValue = this.historyState.leadType ? this.historyState.leadType.value : "";
                var leadTypeFilterValue = this.historyState.leadType ? this.historyState.leadType.value : Terrasoft.GUID_EMPTY;
                var entitySchemaQuery = Ext.create("Terrasoft.EntitySchemaQuery", {
                    rootSchemaName: "OpportunityStage",
                    clientESQCacheParameters: {
                        cacheItemName: "OpportunityStagesFunnelData_" + cacheValue
                    }
                });
                var allowedStageFiltersGroup = this.getFunnelAllowedStagesFilters();
                entitySchemaQuery.filters.addItem(allowedStageFiltersGroup);
                entitySchemaQuery.filters.add("LeadTypeFilter", Terrasoft.createColumnFilterWithParameter(
                    Terrasoft.ComparisonType.EQUAL, "[TsOppStageInLeadType:TsOpportunityStage].TsLeadType", leadTypeFilterValue));
                entitySchemaQuery.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_COLUMN, "Id");
                entitySchemaQuery.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_DISPLAY_COLUMN, "Name");
                entitySchemaQuery.addColumn("End", "End");
                var numberColumn = entitySchemaQuery.addColumn("[TsOppStageInLeadType:TsOpportunityStage].TsNumber", "Number");
                numberColumn.orderDirection = Terrasoft.OrderDirection.ASC;
                numberColumn.orderPosition = 0;
                return entitySchemaQuery;
            }

        });
    });