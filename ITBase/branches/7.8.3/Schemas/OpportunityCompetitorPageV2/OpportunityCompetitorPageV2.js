define("OpportunityCompetitorPageV2", ["BusinessRuleModule"], function(BusinessRuleModule) {
	return {
		entitySchemaName: "OpportunityCompetitor",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		attributes: {
			"CompetitorProduct": {
				lookupListConfig: {
					columns: ["ITWeakSides", "ITStrongSides"]
				}
			},
			"ITWeakSides": {
				dependencies: [
					{
						columns: ["CompetitorProduct"],
						methodName: "onCompetitorProductSelect"
					}
				]
			},
			"ITStrongSides": {
				dependencies: [
					{
						columns: ["Competitor"],
						methodName: "onCompetitorSelect"
					}
				]
			}
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "Opportunity",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Competitor",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					},
					"labelConfig": {},
					"enabled": true,
					"contentType": 5
				}
			},
			{
				"operation": "merge",
				"name": "CompetitorProduct",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "remove",
				"name": "Weakness"
			},
			{
				"operation": "remove",
				"name": "Strengths"
			},
			{
				"operation": "insert",
				"name": "ITWeakSides",
				"parentName": "Header",
				"propertyName": "items",
				"values": {
					"bindTo": "ITWeakSides",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					},
					"contentType": 3
				}
			},
			{
				"operation": "insert",
				"name": "ITStrongSides",
				"parentName": "Header",
				"propertyName": "items",
				"values": {
					"bindTo": "ITStrongSides",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2
					},
					"contentType": 3
				}
			},
			{
				"operation": "merge",
				"name": "IsWinner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "remove",
				"name": "Supplier"
			},
			{
				"operation": "insert",
				"name": "ITRate",
				"parentName": "Header",
				"propertyName": "items",
				"values": {
					"bindTo": "ITRate",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3
					}
				}
			},
			{
				"operation": "insert",
				"name": "ITComment",
				"parentName": "Header",
				"propertyName": "items",
				"values": {
					"bindTo": "ITComment",
					"layout": {
						"colSpan": 24,
	//					"rowSpan": 1,
						"column": 0,
						"row": 4
					},
					"contentType": 0
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {
			onCompetitorProductSelect: function() {
				var competitorProduct = this.get("CompetitorProduct");
				if (!competitorProduct) {
					this.set("ITWeakSides", null);
					this.set("ITStrongSides", null);
				} else {
					this.set("ITWeakSides", competitorProduct.ITWeakSides);
					this.set("ITStrongSides", competitorProduct.ITStrongSides);
				}
			}
		},
		rules: {
			"Competitor": {
				"FiltrationCompetitorByType": {
					"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
					"autocomplete": false,
					"autoClean": false,
					"baseAttributePatch": "Type",
					"comparisonType": Terrasoft.ComparisonType.EQUAL,
					"type": BusinessRuleModule.enums.ValueType.CONSTANT,
					"value": "d44b9da2-53e6-df11-971b-001d60e938c6"
				}
			}
		}
	};
});
