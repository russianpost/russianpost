define("DocumentPageV2", ["BaseFiltersGenerateModule", "VisaHelper", "BusinessRuleModule", "ConfigurationConstants"],
	function(BaseFiltersGenerateModule, VisaHelper, BusinessRuleModule, ConfigurationConstants) {
		return {
			entitySchemaName: "Document",
			attributes: {},
			details: /**SCHEMA_DETAILS*/{
				"ITMeetingsDetail": {
					"schemaName": "ITMeetingsDetailV2",
					"entitySchemaName": "Activity",
					"filter": {
						"detailColumn": "Account",
						"masterColumn": "Id"
					},
					filterMethod: "meetingsFilter"
				},
				"ITTasksDetailV2": {
					"schemaName": "ITTasksDetailV2",
					//"entitySchemaName": "Activity",
					"filter": {
						"detailColumn": "Account",
						"masterColumn": "Id"
					},
					filterMethod: "tasksActivityFilter"
				},
				"ITCallsDetail": {
					"schemaName": "ITCallsDetailV2",
					//"entitySchemaName": "Activity",
					"filter": {
						"detailColumn": "Account",
						"masterColumn": "Id"
					},
					filterMethod: "callsActivityFilter"
				}
			}/**SCHEMA_DETAILS*/,
			methods: {
				
				meetingsFilter: function() {
					var filterGroup = new this.Terrasoft.createFilterGroup();
					filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "42C74C49-58E6-DF11-971B-001D60E938C6"));
					// добавляете фильтр по типу активности
					filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Document", this.get("Id")));
					return filterGroup;
				},
	
	
	
				
				callsActivityFilter: function() {
					var filterGroup = new this.Terrasoft.createFilterGroup();
					filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "03DF85BF-6B19-4DEA-8463-D5D49B80BB28"));
					// добавляете фильтр по типу активности
					filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Document", this.get("Id")));
					return filterGroup;
				},
	
	
	
				
				tasksActivityFilter: function() {
					var filterGroup = new this.Terrasoft.createFilterGroup();
					filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "F51C4643-58E6-DF11-971B-001D60E938C6"));
					// добавляете фильтр по типу активности
					filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Document", this.get("Id")));
					return filterGroup;
				}
			},
			rules: {},
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "remove",
					"name": "Activities"
				},
				{
					"operation": "remove",
					"name": "Calls"
				},
				{
					"operation": "insert",
					"name": "ITMeetingsDetail",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 10
				},
				{
					"operation": "insert",
					"name": "ITTasksDetailV2",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 11
				},
				{
					"operation": "insert",
					"name": "ITCallsDetail",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 12
				}
			]/**SCHEMA_DIFF*/
		};
	});
