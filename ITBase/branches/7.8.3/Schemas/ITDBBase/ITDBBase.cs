using System;
using System.Collections.Generic;
using System.Data;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
namespace Terrasoft.Configuration.ITBase
{
	
	public class ITDBBase
    {
        #region Public: Properties

        public UserConnection UserConnection { get; private set; }

        public UserConnection SystemUserConnection
        {
            get { return this.UserConnection.AppConnection.SystemUserConnection; }
        }

        #endregion

        #region Public: Constructors

        public ITDBBase(UserConnection userConnection)
        {
            UserConnection = userConnection;
        }

        #endregion
    }
    
}