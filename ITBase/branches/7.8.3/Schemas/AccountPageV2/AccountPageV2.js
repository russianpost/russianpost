define("AccountPageV2", ["MaskHelper", "ProcessModuleUtilities", "BusinessRuleModule", "ConfigurationConstants"],
	function(MaskHelper, ProcessModuleUtilities, BusinessRuleModule, ConfigurationConstants) {
		return {
			//ITBase
			entitySchemaName: "Account",
			
			attributes: {
				"isNotOurCompanyType": {
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: true,
					dependencies: [{
						columns: ["Type"],
						methodName: "getIsNotOurCompanyType"
					}]
				},
				"DurationOfCooperation": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.INTEGER,
					value: "0"
				},
				"TsINN": {
					isRequired: true
				},
				"subsegmentVisibility": {
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: true
				},
				"TsAccountBusinessType": {
					dependencies: [
						{
							columns: ["TsAccountBusinessType"],
							methodName: "subsegmentIsVisible"
						}
					]
				},
				"Type": {
					dependencies: [
						{
							columns: ["Type"],
							methodName: "onAccountTypeChange"
						}
					]
				},

				"PrimaryContact": {
					"lookupListConfig": {
						filter: function() {
							var accountType = this.get("Type");
							var filterGroup = Terrasoft.createFilterGroup();
							if (accountType.value !== ConfigurationConstants.AccountType.OurCompany.toLowerCase()) {
								filterGroup.addItem(Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.NOT_EQUAL, "Type",
									"60733efc-f36b-1410-a883-16d83cab0980"));
							} else {
								filterGroup.add("isAssociate",
									Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Type",
										"60733efc-f36b-1410-a883-16d83cab0980",
										Terrasoft.DataValueType.GUID));
								filterGroup.add("isOurCompany",
									Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Account.Type",
										"57412fad-53e6-df11-971b-001d60e938c6",
										Terrasoft.DataValueType.GUID));
							}
							return filterGroup;
						}
					}
				},

				"TsTarifficatorRegion": {
					dependencies: [
						{
							columns: ["TsAccountRegionType"],
							methodName: "regionVisibility"
						}
					],
					"lookupListConfig": {
						filter: function() {
							var filterGroup = Terrasoft.createFilterGroup(),
								currentMacroRegionId = this.get("ITMacroregion").value;
							if (currentMacroRegionId) {
								filterGroup.add("byMacroRegion",
									Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "ITMacroregion",
										currentMacroRegionId,
										Terrasoft.DataValueType.GUID));
							}
							return filterGroup;
						}
					}
				},

				"ClearCategorizationFields": {
					dependencies: [
						{
							columns: ["ITSector", "Industry", "TsAccountBusinessType"],
							methodName: "clearCetegorizationFields"
						}
					]
				},

				"onRegionChange": {
					dependencies: [
						{
							columns: ["TsTarifficatorRegion"],
							methodName: "onRegionChange"
						}
					]
				},

				"setMacroregion": {
					dependencies: [
						{
							columns: ["Owner"],
							methodName: "setMacroregion2"
						}
					]
				},

				"ITTypeOfWorkWeek": {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					lookupListConfig: {
						orders: [
							{
								columnPath: "ITIndexNumber",
								orderDirection: Terrasoft.OrderDirection.ASC
							}
						]
					}
				},

				"ITTypeOfWorkTime": {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					lookupListConfig: {
						orders: [
							{
								columnPath: "ITIndexNumber",
								orderDirection: Terrasoft.OrderDirection.ASC
							}
						]
					}
				},

				"ITMethodsOfFormingForm103": {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					lookupListConfig: {
						orders: [
							{
								columnPath: "ITIndexNumber",
								orderDirection: Terrasoft.OrderDirection.ASC
							}
						]
					}
				},

				"IsNeedValid": {
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: true
				},

				"IsNeedSetTsTarifficatorRegion": {
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: true
				},

				"OldOwnerId": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.GUID
				},

				"editRightsProcessRunned": {
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: false
				}
				
			},
			messages: {
				"GetIsNotOurCompanyTypeForDetail": {
					mode: this.Terrasoft.MessageMode.PTP,
					direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
				},
				"GetIsNotOurCompanyTypeForSecondDetail": {
					mode: this.Terrasoft.MessageMode.PTP,
					direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
				},
				"SetIsNotOurCompanyTypeForDetail": {
					mode: this.Terrasoft.MessageMode.BROADCAST,
					direction: this.Terrasoft.MessageDirectionType.PUBLISH
				},
				//#RPCRM-898 Сообщение для передачи данных текущего контрагента в миникарточку Продажи
				"SetClientForOpportunityMiniPage": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},
				//#RPCRM-898 Сообщение для передачи данных текущего контрагента в миникарточку Лид
				"SetClientForLeadMiniPage": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				},
				//#RPCRM-987 Сообщение для передачи региона обслуживания контрагента в деталь "Отв-е по направлениям бизнеса"
				"SetTsTarifficatorRegion": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				}
			},
			
			details: /**SCHEMA_DETAILS*/{
				"ITAccountCategDetail1": {
					"schemaName": "ITAccountCategDetail",
					"entitySchemaName": "ITVwAccountCateg",
					"filter": {
						"detailColumn": "ITAccount",
						"masterColumn": "Id"
					}
				},
				"ITAccountOwnerDetail": {
					"schemaName": "ITAccountOwnerDetail",
					"entitySchemaName": "ITVwAccountOwner",
					"filter": {
						"detailColumn": "ITAccount",
						"masterColumn": "Id"
					}
				},
				"ITMeetingsDetail": {
					"schemaName": "ITMeetingsDetailV2",
					"entitySchemaName": "Activity",
					"filter": {
						"detailColumn": "Account",
						"masterColumn": "Id"
					},
					filterMethod: "meetingsFilter"
				},
				"ITTasksDetailV2": {
					"schemaName": "ITTasksDetailV2",
					//"entitySchemaName": "Activity",
					"filter": {
						"detailColumn": "Account",
						"masterColumn": "Id"
					},
					filterMethod: "tasksActivityFilter"
				},
				"ITCallsDetail": {
					"schemaName": "ITCallsDetailV2",
					//"entitySchemaName": "Activity",
					"filter": {
						"detailColumn": "Account",
						"masterColumn": "Id"
					},
					filterMethod: "callsActivityFilter"
				}
			}/**SCHEMA_DETAILS*/,
			modules: {},
			rules: {
				//Видимость поля Сектор (true, если тип НЕ наша компания)
				ITSector: {
					"VisibleIfNotOurCompany": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.VISIBLE,
						conditions: [
							{
								"leftExpression": {
									"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									"attribute": "Type"
								},
								"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
								"rightExpression": {
									"type": BusinessRuleModule.enums.ValueType.CONSTANT,
									"value": ConfigurationConstants.AccountType.OurCompany.toLowerCase()
								}
							}
						]
					}
				},

				Industry: {
					// Фильтрация поля "Отрасль" по значению в поле "Сектор".
					"FiltrationIndustryBySector": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": true,
						"autoClean": true,
						"baseAttributePatch": "ITSector",
						"comparisonType": Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
						"attribute": "ITSector"
					},
					// Поле доступно, если поле "Сектор" заполнено.
					"EnabledIfITSectorIsNotEmpty": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.ENABLED,
						conditions: [
							{
								"leftExpression": {
									"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									"attribute": "ITSector"
								},
								"comparisonType": this.Terrasoft.ComparisonType.IS_NOT_NULL
							}
						]
					}
				},

				TsAccountBusinessType: {
					// Фильтрация поля "Сегмент" по значению в поле "Отрасль".
					"FiltrationSegmentByIndustry": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": true,
						"autoClean": true,
						"baseAttributePatch": "ITIndustry",
						"comparisonType": Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
						"attribute": "Industry"
					},
					// Поле доступно, если поле "Отрасль" заполнено.
					"EnabledIfIndustryIsNotEmpty": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.ENABLED,
						conditions: [
							{
								"leftExpression": {
									"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									"attribute": "Industry"
								},
								"comparisonType": this.Terrasoft.ComparisonType.IS_NOT_NULL
							}
						]
					}
				},

				ITSubsegment: {
					// Фильтрация поля "Подсегмент" по значению в поле "Сегмент".
					"FiltrationSegmentByIndustry": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": true,
						"autoClean": true,
						"baseAttributePatch": "ITSegment",
						"comparisonType": Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
						"attribute": "TsAccountBusinessType"
					},
					// Поле доступно, если поле "Сегмент" заполнено.
					"EnabledIfIndustryIsNotEmpty": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.ENABLED,
						conditions: [
							{
								"leftExpression": {
									"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									"attribute": "TsAccountBusinessType"
								},
								"comparisonType": this.Terrasoft.ComparisonType.IS_NOT_NULL
							}
						]
					}
				},

				//Ограничения на поле "Организационная структура"
				ITOrgStructure: {
					"RequireIfOurCompany": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.REQUIRED,
						conditions: [
							{
								"leftExpression": {
									"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									"attribute": "Type"
								},
								"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
								"rightExpression": {
									"type": BusinessRuleModule.enums.ValueType.CONSTANT,
									"value": ConfigurationConstants.AccountType.OurCompany.toLowerCase()
								}
							}
						]
					},
					"VisibleIfOurCompany": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.VISIBLE,
						conditions: [
							{
								"leftExpression": {
									"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									"attribute": "Type"
								},
								"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
								"rightExpression": {
									"type": BusinessRuleModule.enums.ValueType.CONSTANT,
									"value": ConfigurationConstants.AccountType.OurCompany.toLowerCase()
								}
							}
						]
					}
				},
				//Обязательность заполнения поля КПП, если форма собственности НЕ ИП.
				TsKPP: {
					"RequireTsKPP": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.REQUIRED,
						conditions: [
							{
								"leftExpression": {
									"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									"attribute": "Ownership"
								},
								"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
								"rightExpression": {
									"type": BusinessRuleModule.enums.ValueType.CONSTANT,
									"value": "350469c4-40f9-49c5-8298-7f8a38548050"
								}
							}
						]
					}
				},
				// Обязательность заполнения поля "Комментарий", если в поле "Тип рабочей недели" или в поле
				// "Тип рабочего времени" установлено значение "Другое".
				"ITComment": {
					"BindParameterRequiredITComment": {
						"ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						"property": BusinessRuleModule.enums.Property.REQUIRED,
						"conditions": [
							{
								"leftExpression": {
									"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									"attribute": "ITTypeOfWorkWeek"
								},
								"comparisonType": Terrasoft.ComparisonType.EQUAL,
								"rightExpression": {
									"type": BusinessRuleModule.enums.ValueType.CONSTANT,
									"value": "9dbfea23-3bba-4c60-a665-e9c37aee946f"
								}
							},
							{
								"leftExpression": {
									"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									"attribute": "ITTypeOfWorkTime"
								},
								"comparisonType": Terrasoft.ComparisonType.EQUAL,
								"rightExpression": {
									"type": BusinessRuleModule.enums.ValueType.CONSTANT,
									"value": "5925b6fb-db7d-4f6b-8166-8772ca4f1ecb"
								}
							}
						]
					}
				},
				//Видимость поля "Способ формирования формы 103" (true, если тип = Клиент)
				ITMethodsOfFormingForm103: {
					"VisibleIfClient": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.VISIBLE,
						conditions: [
							{
								"leftExpression": {
									"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									"attribute": "Type"
								},
								"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
								"rightExpression": {
									"type": BusinessRuleModule.enums.ValueType.CONSTANT,
									"value": ConfigurationConstants.AccountType.Client.toLowerCase()
								}
							}
						]
					}
				}
			},
			
			methods: {



				
				meetingsFilter: function() {
					var filterGroup = new this.Terrasoft.createFilterGroup();
					filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "42C74C49-58E6-DF11-971B-001D60E938C6"));
					// добавляете фильтр по типу активности
					filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Account", this.get("Id")));
					return filterGroup;
				},



				
				callsActivityFilter: function() {
					var filterGroup = new this.Terrasoft.createFilterGroup();
					filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "03DF85BF-6B19-4DEA-8463-D5D49B80BB28"));
					// добавляете фильтр по типу активности
					filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Account", this.get("Id")));
					return filterGroup;
				},



				
				tasksActivityFilter: function() {
					var filterGroup = new this.Terrasoft.createFilterGroup();
					filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "F51C4643-58E6-DF11-971B-001D60E938C6"));
					// добавляете фильтр по типу активности
					filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Account", this.get("Id")));
					return filterGroup;
				},


				
				onEntityInitialized: function() {
					this.callParent(arguments);
					this.getIsNotOurCompanyType();
					this.fillDurationOfCooperation();
					this.on("change:Type", this.onTypeChangedIT, this);
					this.subsegmentIsVisible2();
					this.regionVisibility();
					this.setRegionForDetail();
					this.oldRegion = this.get("TsTarifficatorRegion");
					this.set("OldOwnerId", this.get("Owner").value);
				},

				//Обнуление поля "Оргструктура" при изменении Типа контрагента на НЕ "Наша компания"
				onTypeChangedIT: function() {
					var type = this.get("Type");
					if (type && type.value !== ConfigurationConstants.AccountType.OurCompany.toLowerCase()) {
						this.set("ITOrgStructure", "");
					}
				},

				getIsNotOurCompanyType: function() {
					var type = this.get("Type");
					if (type && type.value) {
						this.set("isNotOurCompanyType",
							type.value !== ConfigurationConstants.AccountType.OurCompany.toLowerCase());
					} else {
						this.set("isNotOurCompanyType", true);
					}
					this.sandbox.subscribe("GetIsNotOurCompanyTypeForDetail", function(args) {
						return this.get("isNotOurCompanyType");
					}, this, null);
					this.sandbox.subscribe("GetIsNotOurCompanyTypeForSecondDetail", function(args) {
						return this.get("isNotOurCompanyType");
					}, this, null);
					this.sandbox.publish("SetIsNotOurCompanyTypeForDetail", this.get("isNotOurCompanyType"), null);
					return this.get("isNotOurCompanyType");
				},

				fillDurationOfCooperation: function() {
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", { rootSchemaName: "ITVwAccountCoopDay"});
					esq.addColumn("ITDurationOfCooperation", "Duration");
					esq.getEntity(this.get("Id"), function(result) {
						if (!result.success || !result.entity) {
							this.set("DurationOfCooperation", 0);
							return;
						}
						this.set("DurationOfCooperation", result.entity.get("Duration"));
					}, this);
				},

				//Валидация поля ИНН.
				identificationNumberValidator: function(value) {
					var invalidMessage = "", code = "350469c4-40f9-49c5-8298-7f8a38548050";//id ИП
					var ownership = this.get("Ownership");
					var type;
					if (ownership) {
						type = ownership.value;
					}
					if (!this.Ext.isEmpty(this.get("TsINN"))) {
						var innValue = this.get("TsINN");
						var digitsPattern = /^\d+$/;
						if (!innValue.match(digitsPattern)) {
							invalidMessage = "Некорректно заполнен код ИНН";
						} else {
							var currentINNLength = innValue.toString().length;
							if (type && type === code && currentINNLength !== 12) {
								invalidMessage = "Длина ИНН для ИП должна составлять 12 символов";
							}
			/*				if (type !== code && (typeLength === 12 || typeLength === 0) && currentINNLength !== 12) {
								invalidMessage = "Длина ИНН должна составлять 12 символов";
							}*/
							if (type && type !== code && currentINNLength !== 10) {
								invalidMessage = "Длина ИНН должна составлять 10 символов";
							}
							if (!type && currentINNLength !== 10 && currentINNLength !== 12) {
								invalidMessage = "Некорректная длина ИНН";
							}
						}
					}
					return {
						fullInvalidMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
				},

				// Валидация поля КПП.
				kppValueValidator: function() {
					var invalidMessage = "", code = "350469c4-40f9-49c5-8298-7f8a38548050";//id ИП
					var ownership = this.get("Ownership");
					if (ownership && ownership.value === code) {
						return {
							fullInvalidMessage: invalidMessage,
							invalidMessage: invalidMessage
						};
					}
					if (!this.Ext.isEmpty(this.get("TsKPP"))) {
						var kppValue = this.get("TsKPP");
						var currentKppLength = kppValue.toString().length;
						if (currentKppLength !== 9) {
							invalidMessage = "Длина КПП должна составлять 9 символов";
						}
					}
					return {
						fullInvalidMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
				},

				setValidationConfig: function() {
					this.callParent(arguments);
					this.addColumnValidator("TsKPP", this.kppValueValidator);
					this.addColumnValidator("TsINN", this.identificationNumberValidator);
				},

				// Метод определяет видимость поля "Подсегмент". Срабатывает при изменении поля "Сегмент".
				// Если у выбранного сегмента не имеется подсегментов, то поле "Подсегмент" скрывается.
				subsegmentIsVisible: function() {
					var segment;
					if (this.get("TsAccountBusinessType")) {
						segment = this.get("TsAccountBusinessType").value;
					}
					if (segment) {
						var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "ITSubsegment"});
						esq.addColumn("ITSegment", "ITSegment");
						var esqFirstFilter = esq.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.EQUAL, "ITSegment", segment);
						esq.filters.add("esqFirstFilter", esqFirstFilter);
						esq.getEntityCollection(function(result) {
							if (result.success && result.collection.collection.items.length) {
								this.set("subsegmentVisibility", true);
							} else {
								this.set("subsegmentVisibility", false);
							}
						}, this);
					}
				},

				// Метод определяет видимость поля "Подсегмент". Срабатывает после инициализации страницы и при
				// изменении типа контрагента. Если выбран тип "наша компания", поле "Подсегмент" скрывается.
				subsegmentIsVisible2: function() {
					var accType = this.get("Type");
					if (accType && accType.value === ConfigurationConstants.AccountType.OurCompany.toLowerCase()) {
						this.set("subsegmentVisibility", false);
					} else {
						var segment;
						if (this.get("TsAccountBusinessType")) {
							segment = this.get("TsAccountBusinessType").value;
						}
						if (segment) {
							var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "ITSubsegment"});
							esq.addColumn("ITSegment", "ITSegment");
							var esqFirstFilter = esq.createColumnFilterWithParameter(
								Terrasoft.ComparisonType.EQUAL, "ITSegment", segment);
							esq.filters.add("esqFirstFilter", esqFirstFilter);
							esq.getEntityCollection(function(result) {
								if (result.success && result.collection.collection.items.length) {
									this.set("subsegmentVisibility", true);
								} else {
									this.set("subsegmentVisibility", false);
								}
							}, this);
						}
					}
				},

				// Метод очищает значения полей при изменении типа контрагента.
				onAccountTypeChange: function() {
					this.subsegmentIsVisible2();
					this.set("ITSector", null);
					this.set("Industry", null);
					this.set("TsAccountBusinessType", null);
					this.set("ITSubsegment", null);
					this.set("ITOrgStructure", null);
				},

				// Метод определяет видимость поля "Регион обслуживания" в зависимости от значения в поле "Масштаб
				// деятельности компании".
				regionVisibility: function() {
					var type = this.get("TsAccountRegionType");
					if (type && type.value === "dc9d6a89-220d-4ee8-af34-5d0dcb8ef5cd") {
						this.set("TsTarifficatorRegion", null);
						return false;
					} else {
						return true;
					}
				},

				// Метод очищает нижестоящие поля категоризации контрагента, если очистить вышестоящее поле.
				clearCetegorizationFields: function() {
					if (!this.get("ITSector")) {
						this.set("Industry", null);
						this.set("TsAccountBusinessType", null);
						this.set("ITSubsegment", null);
						return;
					}
					if (!this.get("Industry")) {
						this.set("TsAccountBusinessType", null);
						this.set("ITSubsegment", null);
						return;
					}
					if (!this.get("TsAccountBusinessType")) {
						this.set("ITSubsegment", null);
					}
				},



				
				runAccessRightsProcess: function() {
					var params = {};
					
					if (!this.get("editRightsProcessRunned") && this.get("OldOwnerId") !== this.get("Owner").value) {
						this.set("editRightsProcessRunned", true);
						params.OldResponsible = this.get("OldOwnerId");
						params.NewResponsible =  this.get("Owner").value;
						params.Account = this.get("Id");
						ProcessModuleUtilities.executeProcess({
							sysProcessName: "ITRightsForAccountInstallation",
							parameters: params
						});
					}
				},

				/* Простановка макрорегиона из ответственного при открытии карточки контрагента.
				setMacroregion: function() {
					var macroregion = this.get("ITMacroregion");
					if (this.Ext.isEmpty(macroregion)) {
						var owner = this.get("Owner");
						if (!this.Ext.isEmpty(owner)) {
							var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
								rootSchemaName: "Contact"
							});
							esq.addColumn("ITMacroregion", "ITMacroregion");
							esq.getEntity(owner.value, function(result) {
								if (result.success) {
									var macroreg = result.entity.get("ITMacroregion");
									if (!this.Ext.isEmpty(macroreg)) {
										this.set("ITMacroregion", macroreg);
										var update = this.Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "Account"});
										update.enablePrimaryColumnFilter(this.get("Id"));
										update.setParameterValue("ITMacroregion", macroreg.value,
											this.Terrasoft.DataValueType.GUID);
										update.execute();
										this.set("ShowSaveButton", false);
										this.set("ShowDiscardButton", false);
										this.set("ShowCloseButton", true);
									}
								}
							}, this);
						}
					}
				},*/

				// Простановка макрорегиона из ответственного при изменении ответственного.
				setMacroregion2: function() {
					var owner = this.get("Owner");
					if (!this.Ext.isEmpty(owner)) {
						var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "Contact"
						});
						esq.addColumn("ITMacroregion", "ITMacroregion");
						esq.getEntity(owner.value, function(result) {
							if (result.success) {
								var macroreg = result.entity.get("ITMacroregion");
								if (!this.Ext.isEmpty(macroreg)) {
									this.set("ITMacroregion", macroreg);
								} else {
									this.set("ITMacroregion", null);
								}
							}
						}, this);
					}
				},

				//#RPCRM-898 Открытие миникарточки Лид и передача данных текущего контрагента для привязки
				onAddLeadButtonClick: function() {
					this.openAddMiniPage({
						entitySchemaName: "Lead",
						valuePairs: []
					});
					this.sandbox.subscribe("SetClientForLeadMiniPage", function() {
						var currentAccount = {
							Id: this.get("Id"),
							Name: this.get("Name"),
							displayValue: this.get("Name"),
							value: this.get("Id")
						};
						return currentAccount;
					}, this, null);
				},

				//#RPCRM-898 Открытие миникарточки Продажи и передача данных текущего контрагента для привязки
				onAddOpportunityButtonClick: function() {
					this.openAddMiniPage({
						entitySchemaName: "Opportunity",
						valuePairs: []
					});
					this.sandbox.subscribe("SetClientForOpportunityMiniPage", function() {
						var currentAccount = {
							Id: this.get("Id"),
							Name: this.get("Name"),
							displayValue: this.get("Name"),
							value: this.get("Id")
						};
						return currentAccount;
					}, this, null);
				},

				//#RPCRM-898 Если контрагент является клиентом вовращает true, иначе false
				IsTypeAccountClient: function() {
					if (this.get("Type")) {
						if (this.get("Type").value === ConfigurationConstants.AccountType.Client.toLowerCase()) {
							return true;
						}
					}
					return false;
				},

				//#RPCRM-987 Проверяет есть ли ответсвенные, регион которого отличается от выбранного и запрещает изменение
				onRegionChange: function() {
					if (this.get("IsNeedValid")) {
						var filterGroup = Ext.create("Terrasoft.FilterGroup"),
							currentAccountId = this.get("Id"),
							currentRegion = this.get("TsTarifficatorRegion"),
							esq = Ext.create("Terrasoft.EntitySchemaQuery", {
								rootSchemaName: "ITVwAccountOwner"
							});
						esq.addColumn("ITOwner.TsTarifficatorRegion", "RegionOwner");
						esq.addColumn("ITOwnerSupplier.TsTarifficatorRegion", "RegionOwnerSupplier");
						filterGroup.logicalOperation = this.Terrasoft.LogicalOperatorType.OR;
						var esqFirstFilter = esq.createColumnIsNotNullFilter("ITOwner");
						var esqSecondFilter = esq.createColumnIsNotNullFilter("ITOwnerSupplier");
						filterGroup.addItem(esqFirstFilter);
						filterGroup.addItem(esqSecondFilter);
						var esqCurrentAccount = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
							"ITAccount", currentAccountId);
						esq.filters.add("filterGroup", filterGroup);
						esq.filters.add("esqCurrentAccount", esqCurrentAccount);
						esq.getEntityCollection(function(result) {
							var IsChangeRegion = true;
							if (!currentRegion && result.collection.collection.items.length) {
								IsChangeRegion = false;
							} else {
								result.collection.each(function(item) {
									if (item.get("RegionOwner")) {
										if (currentRegion.value !== item.get("RegionOwner").value) {
											IsChangeRegion = false;
										}
									}
									if (item.get("RegionOwnerSupplier")) {
										if (currentRegion.value !== item.get("RegionOwnerSupplier").value) {
											IsChangeRegion = false;
										}
									}
								});
							}
							if (!IsChangeRegion) {
								this.set("IsNeedValid", false);
								this.set("IsNeedSetTsTarifficatorRegion", false);
								this.set("TsTarifficatorRegion", this.oldRegion);
								if (currentRegion) {
									this.showInformationDialog("Внимание! По данному контрагенту есть ответственные в другом регионе");
								} else {
									this.showInformationDialog("Внимание! Вы не сможете удалить регион," +
									" пока по данному контрагенту есть ответственные");
								}
								
							} else {
								this.oldRegion = this.get("TsTarifficatorRegion");
							}
							this.setRegionForDetail();
							this.set("IsNeedValid", true);
						}, this);
					} else if (this.get("IsNeedSetTsTarifficatorRegion")) {
						this.setRegionForDetail();
					}
					
				},

				//Передача региона в деталь "Отв-е по направлениям бизнеса"
				setRegionForDetail: function() {
					var sandbox = this.sandbox,
						tag = [sandbox.id + "_detail_ITAccountOwnerDetailITVwAccountOwner"];
					sandbox.subscribe("SetTsTarifficatorRegion", function() {
						var currentRegion = this.get("TsTarifficatorRegion");
						return currentRegion;
					}, this, tag);
				},
				
			/*	copyFieldsProcess: function() {
					MaskHelper.ShowBodyMask({
						selector: "#AccountBillingInfoDetailV2DetailControlGroup"
					});
					var account = this.get("Id");
					var config = {
						scope: this
					};
					ProcessModuleUtilities.executeProcess({
						sysProcessName: "ITCopyingAccountBillingInfo",
						parameters: {
							CurrentAccountId: account
						},
						callback: this.updateAccountBillingInfo.bind(this, config)
					});
				},

				updateAccountBillingInfo: function(config) {
					var scope = config.scope || this;
					MaskHelper.HideBodyMask({
						selector: "#AccountBillingInfoDetailV2DetailControlGroup"
					});
					scope.updateDetail({ detail: "AccountBillingInfo" });
				},
				*/
				onSaved: function() {
					this.set("IsNeedValid", false);
					this.callParent(arguments);
					this.set("IsNeedValid", true);
					this.runAccessRightsProcess();
				}
			},
			diff: /**SCHEMA_DIFF*/[
				/**ProfileContainer block start**/
				{
					"operation": "merge",
					"name": "AccountName",
					"values": {
						"tip": {
							// Текст подсказки.
							"content": {"bindTo": "Resources.Strings.AccountNameTipContent"},
							// Режим отображения подсказки.
							// По умолчанию режим WIDE - толщина зеленой полоски,
							// которая отображается в подсказке.
							"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
						},
						"layout": {"colSpan": 24, "column": 0, "row": 0}
					},
					"index": 0
				},

				{
					"operation": "merge",
					"name": "AccountType",
					"values": {
						"layout": {"colSpan": 24, "column": 0, "row": 1},
						"caption": {
							"bindTo": "Resources.Strings.ITAlternativeAccountTypeCaption"
						}
					},
					"index": 1
				},

				{
					"operation": "insert",
					"name": "ITSector",
					"values": {
						"bindTo": "ITSector",
						"layout": {"colSpan": 24, "column": 0, "row": 2}
					},
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"index": 2
				},

				{
					"operation": "insert",
					"name": "ITOrgStructure",
					"values": {
						"bindTo": "ITOrgStructure",
						"layout": {"colSpan": 24, "column": 0, "row": 3},
						"labelConfig": {},
						"enabled": true,
						"contentType": 3
					},
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"index": 3
				},

				{
					"operation": "insert",
					"name": "ITInterationType",
					"values": {
						"layout": {"colSpan": 24, "column": 0, "row": 7, "layoutName": "ProfileContainer"},
						"bindTo": "ITInterationType",
						"contentType": 3
					},
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"index": 4
				},

				{
					"operation": "merge",
					"name": "AccountOwner",
					"values": {
						"layout": {"colSpan": 24, "column": 0, "row": 8}
					},
					"index": 5
				},

				{
					"operation": "merge",
					"name": "AccountWeb",
					"values": {
						"layout": {"colSpan": 24, "column": 0, "row": 10},
						"caption": "Сайт",
						"visible": {
							"bindTo": "getIsNotOurCompanyType"
						}
					},
					"index": 6
				},

				{
					"operation": "merge",
					"name": "AccountPhone",
					"values": {
						"tip": {
							// Текст подсказки.
							"content": {"bindTo": "Resources.Strings.AccountPhoneTipContent"},
							// Режим отображения подсказки.
							// По умолчанию режим WIDE - толщина зеленой полоски,
							// которая отображается в подсказке.
							"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
						},
						"layout": {"colSpan": 24, "column": 0, "row": 11}
					},
					"index": 7
				},

				{
					"operation": "merge",
					"name": "NewAccountCategory",
					"values": {
						"tip": {
							// Текст подсказки.
							"content": { "bindTo": "Resources.Strings.NewAccountCategoryTipContent"},
							// Режим отображения подсказки.
							// По умолчанию режим WIDE - толщина зеленой полоски,
							// которая отображается в подсказке.
							"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
						},
						"layout": {"colSpan": 24, "column": 0, "row": 12},
						"visible": {
							"bindTo": "getIsNotOurCompanyType"
						},
						"caption": "Категория по ABC"
					},
					"index": 8
				},

				{
					"operation": "merge",
					"name": "AccountIndustry",
					"values": {
						"tip": {
							// Текст подсказки.
							"content": { "bindTo": "Resources.Strings.AccountIndustryTipContent"},
							// Режим отображения подсказки.
							// По умолчанию режим WIDE - толщина зеленой полоски,
							// которая отображается в подсказке.
							"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
						},
						"layout": {"colSpan": 24, "column": 0, "row": 4},
						"visible": {
							"bindTo": "getIsNotOurCompanyType"
						}
					},
					"index": 9
				},

				{
					"operation": "merge",
					"name": "TsAccountBusinessType",
					"values": {
						"tip": {
							// Текст подсказки.
							"content": { "bindTo": "Resources.Strings.TsAccountBusinessTypeTipContent"},
							// Режим отображения подсказки.
							// По умолчанию режим WIDE - толщина зеленой полоски,
							// которая отображается в подсказке.
							"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
						},
						"layout": {"colSpan": 24, "column": 0, "row": 5},
						"contentType": Terrasoft.ContentType.LOOKUP,
						"visible": {
							"bindTo": "getIsNotOurCompanyType"
						},
						"caption": "Сегмент"
					},
					"index": 10
				},

				{
					"operation": "insert",
					"name": "ITSubsegment",
					"values": {
						"tip": {
							// Текст подсказки.
							"content": { "bindTo": "Resources.Strings.ITSubsegmentTipContent"},
							// Режим отображения подсказки.
							// По умолчанию режим WIDE - толщина зеленой полоски,
							// которая отображается в подсказке.
							"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
						},
						"visible": {
							"bindTo": "subsegmentVisibility"
						},
						"bindTo": "ITSubsegment",
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 6
						},
						"contentType": Terrasoft.ContentType.LOOKUP
					},
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"index": 13
				},

				{
					"operation": "insert",
					"name": "DurationOfCooperationField",
					"values": {
						"caption": "Длительность взаимодействия, дней",
						"bindTo": "DurationOfCooperation",
						"enabled": false,
						"layout": {"colSpan": 24, "column": 0, "row": 13}
					},
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"index": 11
				},

				{
					"operation": "merge",
					"name": "AccountCompletenessContainer",
					"values": {
						"layout": {"colSpan": 24, "column": 0, "row": 14},
						"visible": {
							"bindTo": "getIsNotOurCompanyType"
						}
					},
					"index": 12
				},
				{
					"operation": "insert",
					"name": "ITMacroregion",
					"values": {
						"enabled": false,
						"layout": {"colSpan": 24, "column": 0, "row": 9, "layoutName": "ProfileContainer"},
						"bindTo": "ITMacroregion",
						"contentType": 3
					},
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"index": 14
				},
				/**ProfileContainer block end**/

				{
					"operation": "merge",
					"name": "TsINN",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2
						}
	//					"isRequired": true
					}
				},
				{
					"operation": "move",
					"name": "TsINN",
					"parentName": "AccountPageGeneralInfoBlock",
					"propertyName": "items"
				},
				{
					"operation": "merge",
					"name": "TsKPP",
					"values": {
						"layout": {
							"colSpan": 12,
							"column": 12,
							"row": 1
						}
					}
				},
				{
					"operation": "move",
					"name": "TsKPP",
					"parentName": "AccountPageGeneralInfoBlock",
					"propertyName": "items"
				},
				{
					"operation": "merge",
					"name": "AlternativeName",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						}
					}
				},
				{
					"operation": "merge",
					"name": "Code",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 3
						},
						visible: false
					}
				},
				{
					"operation": "merge",
					"name": "TsFullName",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 3
						}
					}
				},
				{
					"operation": "merge",
					"name": "TsBrandName",
					"values": {
						"tip": {
							// Текст подсказки.
							"content": { "bindTo": "Resources.Strings.TsBrandNameTipContent"},
							// Режим отображения подсказки.
							// По умолчанию режим WIDE - толщина зеленой полоски,
							// которая отображается в подсказке.
							"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
						},
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 2
						},
						"visible": {
							"bindTo": "getIsNotOurCompanyType"
						}
					}
				},
				{
					"operation": "merge",
					"name": "TsTarifficatorRegion",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0,
							"layoutName": "AccountPageGeneralInfoBlock"
						},
						"caption": "Регион обслуживания",
						"visible": {"bindTo": "regionVisibility"}
					}
				},
				{
					"operation": "merge",
					"name": "TsAccountRegionType",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1
	//						"layoutName": "CategoriesControlGroupContainer"
						}
					}
				},
				{
					"operation": "move",
					"name": "TsAccountRegionType",
					"parentName": "AccountPageGeneralInfoBlock",
					"propertyName": "items"
				},
				{
					"operation": "merge",
					"name": "EmployeesNumber",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0
						}
					}
				},
				{
					"operation": "insert",
					"name": "ITBudgetingPrinciple",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesControlGroupContainer"
						},
						"bindTo": "ITBudgetingPrinciple"
					},
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "merge",
					"name": "AccountAnniversary",
					"values": {
						"visible": {
							"bindTo": "getIsNotOurCompanyType"
						}
					}
				},
				{
					"operation": "insert",
					"name": "ITAccountOwnerDetail",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "ContactsAndStructureTabContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "ITAccountCategDetail1",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "KindOfBusinessTab",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "remove",
					"name": "TsAccountLevel"
				},
				{
					"operation": "remove",
					"name": "KindOfBusinessGridContainer"
				},
				{
					"operation": "remove",
					"name": "KindOfBusinessContainer"
				},
				{
					"operation": "remove",
					"name": "KindOfBusinessGrid"
				},
				{
					"operation": "remove",
					"name": "Тип бизнеса"
				},
				{
					"operation": "remove",
					"name": "KindOfBusinessCategorizationGridContainer"
				},
				{
					"operation": "remove",
					"name": "CategoriesRetailControlGroupContainer"
				},
				{
					"operation": "remove",
					"name": "TsB2BRetail"
				},
				{
					"operation": "remove",
					"name": "TsB2CRetail"
				},
				{
					"operation": "remove",
					"name": "TsG2BRetail"
				},
				{
					"operation": "remove",
					"name": "TsVipRetail"
				},
				{
					"operation": "remove",
					"name": "TsAccCatInRetail"
				},
				{
					"operation": "remove",
					"name": "TsExecutiveRetail"
				},
				{
					"operation": "remove",
					"name": "TsExecSuppRetail"
				},
				{
					"operation": "remove",
					"name": "CategoriesPrintingServicesControlGroupContainer"
				},
				{
					"operation": "remove",
					"name": "TsB2BPrintingServices"
				},
				{
					"operation": "remove",
					"name": "TsB2CPrintingServices"
				},
				{
					"operation": "remove",
					"name": "TsG2BPrintingServices"
				},
				{
					"operation": "remove",
					"name": "TsVipPrintingServices"
				},
				{
					"operation": "remove",
					"name": "TsAccCatInPrintingServices"
				},
				{
					"operation": "remove",
					"name": "TsExecutivePrintingServices"
				},
				{
					"operation": "remove",
					"name": "TsExecSuppPrintingServices"
				},
				{
					"operation": "remove",
					"name": "CategoriesAdvertBusinessControlGroupContainer"
				},
				{
					"operation": "remove",
					"name": "TsB2BAdvertBusiness"
				},
				{
					"operation": "remove",
					"name": "TsB2CAdvertBusiness"
				},
				{
					"operation": "remove",
					"name": "TsG2BAdvertBusiness"
				},
				{
					"operation": "remove",
					"name": "TsVipAdvertBusiness"
				},
				{
					"operation": "remove",
					"name": "TsAccountAdvertCategory"
				},
				{
					"operation": "remove",
					"name": "TsExecutiveAdvertBusiness"
				},
				{
					"operation": "remove",
					"name": "TsExecSuppAdvertBusiness"
				},
				{
					"operation": "remove",
					"name": "CategoriesFinancialBusinessControlGroupContainer"
				},
				{
					"operation": "remove",
					"name": "TsB2BFinancialBusiness"
				},
				{
					"operation": "remove",
					"name": "TsB2CFinancialBusiness"
				},
				{
					"operation": "remove",
					"name": "TsG2BFinancialBusiness"
				},
				{
					"operation": "remove",
					"name": "TsVipFinancialBusiness"
				},
				{
					"operation": "remove",
					"name": "TsAccountFinancialCategory"
				},
				{
					"operation": "remove",
					"name": "TsExecutiveFinancialBusiness"
				},
				{
					"operation": "remove",
					"name": "TsExecSuppFinBusiness"
				},
				{
					"operation": "remove",
					"name": "CategoriesParcelBusinessControlGroupContainer"
				},
				{
					"operation": "remove",
					"name": "TsB2BParcelBusiness"
				},
				{
					"operation": "remove",
					"name": "TsB2CParcelBusiness"
				},
				{
					"operation": "remove",
					"name": "TsG2BParcelBusiness"
				},
				{
					"operation": "remove",
					"name": "TsVipParcelBusiness"
				},
				{
					"operation": "remove",
					"name": "TsAccountParcelCategory"
				},
				{
					"operation": "remove",
					"name": "TsExecutiveParcelBusiness"
				},
				{
					"operation": "remove",
					"name": "TsExecSuppParcelBusiness"
				},
				{
					"operation": "remove",
					"name": "CategoriesSubscribeServicesControlGroupContainer"
				},
				{
					"operation": "remove",
					"name": "TsB2BSubscribeServices"
				},
				{
					"operation": "remove",
					"name": "TsB2CSubscribeServices"
				},
				{
					"operation": "remove",
					"name": "TsG2BSubscribeServices"
				},
				{
					"operation": "remove",
					"name": "TsVipSubscribeServices"
				},
				{
					"operation": "remove",
					"name": "TsAccCatInSubscribeServices"
				},
				{
					"operation": "remove",
					"name": "TsExecutiveSubscribeServices"
				},
				{
					"operation": "remove",
					"name": "TsExecSuppSubscribeServices"
				},
				{
					"operation": "remove",
					"name": "Cases"
				},
				{
					"operation": "remove",
					"name": "TsDiscountInAccountDetailV2"
				},
				{
					"operation": "remove",
					"name": "Project"
				},
				{
					"operation": "remove",
					"name": "Order"
				},
				{
					"operation": "move",
					"name": "TsAccountExecHistoryDetailV2",
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 8
				},
				{
					"operation": "move",
					"name": "Leads",
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "move",
					"name": "Documents",
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "move",
					"name": "Opportunities",
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "remove",
					"name": "AlternativeName"
				},
				{
					"operation": "move",
					"name": "Ownership",
					"parentName": "AccountPageGeneralInfoBlock",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "merge",
					"name": "Ownership",
					"values": {
						"contentType": this.Terrasoft.ContentType.ENUM,
						"layout": {
							"column": 0,
							"row": 0,
							"colSpan": 12
						}
					}
				},
				{
					"operation": "merge",
					"name": "AnnualRevenue",
					"values": {
						"layout": {
							"column": 0,
							"row": 0,
							"colSpan": 12
						}
					}
				},

				// скрытие детали "Счета"
				{
					"operation": "merge",
					"name": "Invoice",
					"values": {
						"visible": false
					}
				},
				{
					"operation": "remove",
					"name": "Documents"
				},
				{
					"operation": "remove",
					"name": "Activities"
				},
				{
					"operation": "remove",
					"name": "Calls"
				},
				{
					"operation": "insert",
					"name": "ITMeetingsDetail",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 10
				},
				{
					"operation": "insert",
					"name": "ITTasksDetailV2",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 11
				},
				{
					"operation": "insert",
					"name": "ITCallsDetail",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 12
				},
				{
					"operation": "move",
					"name": "CategoriesControlGroup",
					"parentName": "KindOfBusinessTab",
					"propertyName": "items",
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
						"caption": {"bindTo": "Resources.Strings.CategoriesGroupCaption"},
						"items": []
					},
					"index": 0
				},
				{
					"operation": "move",
					"name": "CategoriesControlGroupContainer",
					"parentName": "CategoriesControlGroup",
					"propertyName": "items",
					"values": {
						"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
						"items": []
					}
				},
				{
					"operation": "move",
					"name": "EmployeesNumber",
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items"
				},
				{
					"operation": "move",
					"name": "AnnualRevenue",
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items",
					"values": {
						"bindTo": "AnnualRevenue",
						"layout": {
							"column": 0,
							"row": 0,
							"colSpan": 12
						},
						"contentType": Terrasoft.ContentType.ENUM
					}
				},
				{
					"operation": "insert",
					"name": "Schedule",
					"parentName": "ContactsAndStructureTabContainer",
					"propertyName": "items",
					"index": 3,
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
						"caption": {"bindTo": "Resources.Strings.ITScheduleCaption"},
						"items": []
					}
				},
				{
					"operation": "insert",
					"name": "ScheduleDataGrid",
					"values": {
						"itemType": 0,
						"items": []
					},
					"parentName": "Schedule",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "ITTypeOfWorkWeek",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "ScheduleDataGrid"
						},
						"bindTo": "ITTypeOfWorkWeek"
					},
					"parentName": "ScheduleDataGrid",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "ITTypeOfWorkTime",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 13,
							"row": 0,
							"layoutName": "ScheduleDataGrid"
						},
						"bindTo": "ITTypeOfWorkTime"
					},
					"parentName": "ScheduleDataGrid",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "ITComment",
					"values": {
						"contentType": Terrasoft.ContentType.LONG_TEXT,
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "ScheduleDataGrid"
						},
						"bindTo": "ITComment"
					},
					"parentName": "ScheduleDataGrid",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "F103DataGrid",
					"values": {
						"itemType": 0,
						"items": []
					},
					"parentName": "AccountPageGeneralTabContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "ITMethodsOfFormingForm103",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						},
						"contentType": 3,
						"bindTo": "ITMethodsOfFormingForm103",
						"caption": {"bindTo": "Resources.Strings.ITMethodsOfFormingForm103Caption"}
					},
					"parentName": "F103DataGrid",
					"propertyName": "items",
					"index": 0
				},
				//#RPCRM-898 Кнопка Добавить Лид
				{
					"operation": "insert",
					"name": "AddLeadButton",
					"parentName": "LeftContainer",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.BUTTON,
						"caption": "Добавить лид",
						"style": this.Terrasoft.controls.ButtonEnums.style.GREEN,
						"classes": {
							"textClass": ["actions-button-margin-right"],
							"wrapperClass": ["actions-button-margin-right"]
						},
						"click": {"bindTo": "onAddLeadButtonClick"},
						"visible": {"bindTo": "IsTypeAccountClient"},
						"enabled": true
					}
				},
				//#RPCRM-898 Кнопка Добавить Продажу
				{
					"operation": "insert",
					"name": "AddOpportunityButton",
					"parentName": "LeftContainer",
					"propertyName": "items",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"caption": "Добавить продажу",
						"style": this.Terrasoft.controls.ButtonEnums.style.BLUE,
						"classes": {
							"textClass": ["actions-button-margin-right"],
							"wrapperClass": ["actions-button-margin-right"]
						},
						"click": {"bindTo": "onAddOpportunityButtonClick"},
						"visible": {"bindTo": "IsTypeAccountClient"},
						"enabled": true
					}
				}
			]/**SCHEMA_DIFF*/
		};
		//ITBase
	});
