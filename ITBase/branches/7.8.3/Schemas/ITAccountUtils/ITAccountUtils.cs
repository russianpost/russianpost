using System;
using System.Collections.Generic;
using System.Data;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Scheduler;
using Terrasoft.Configuration.TsBase;


namespace Terrasoft.Configuration.ITBase
{
    public class ITAccountUtils : TsAccountUtils
    {
        public ITAccountUtils(UserConnection userConnection)
        	:base(userConnection)
        {
        }
        
		protected virtual ChangeExecutiveLog ChangeAccountCategOwnerAny(Guid oldOwnerId, Guid newOwnerId, 
			Guid leadTypeId, string ownerFieldName)
        {
            return ChangeOwnerInNotFinishedEntityTemplate("ITAccountCateg",
                configureEsq: (accountCateg) =>
                {
                    accountCateg.Filters.LogicalOperation = LogicalOperationStrict.And;// Or;
                    accountCateg.AddAllSchemaColumns();
                },
                addFiltersToEsq: (accountCateg) =>
                {
                    accountCateg.Filters.Add(accountCateg
                        .CreateFilterWithParameters(FilterComparisonType.Equal, ownerFieldName,
                            oldOwnerId));
                    if (leadTypeId != Guid.Empty)
	                    accountCateg.Filters.Add(accountCateg
	                        .CreateFilterWithParameters(FilterComparisonType.Equal, "ITLeadType",
	                            leadTypeId));
                },
                processEntity: (entity) =>
                {
                    var accountCateg = entity as ITAccountCateg;
                    if (accountCateg == null) return;
                    accountCateg.SetColumnValue(
                        ownerFieldName + "Id",
                        newOwnerId);
                    accountCateg.ModifiedById = UserConnection.CurrentUser.ContactId;
                }, 
                entityEnum: EntityEnum.Account);
        }
        
        public virtual List<ChangeExecutiveLog> ChangeAccountAndRelatedEntitiesOwner(Guid oldOwnerId,
            Guid newOwnerId, Guid leadTypeId, bool changeOwner, bool changeOwnerSupplier)
        {
            List<ChangeExecutiveLog> changeExecutiveLogList = new List<ChangeExecutiveLog>();

            if(!changeOwner && !changeOwnerSupplier)
            	return changeExecutiveLogList;
            if(changeOwner)
            {
	            ChangeExecutiveLog accountLog = ChangeAccountCategOwnerAny(oldOwnerId, newOwnerId, leadTypeId, "ITOwner");
	            if (accountLog != null)
	                changeExecutiveLogList.Add(accountLog);
            }
            if(changeOwnerSupplier)
            {
	            ChangeExecutiveLog accountLog = ChangeAccountCategOwnerAny(oldOwnerId, newOwnerId, leadTypeId, "ITOwnerSupplier");
	            if (accountLog != null)
	                changeExecutiveLogList.Add(accountLog);
            }

            //ChangeExecutiveLog activitiesLog = ChangeOwnerInNotFinishedActivities(oldOwnerId, newOwnerId,
            //    leadTypeId);
            //if (activitiesLog != null)
            //{
            //    changeExecutiveLogList.Add(activitiesLog);
            //}

            ChangeExecutiveLog contractsLog = ChangeOwnerInNotFinishedContracts(oldOwnerId, newOwnerId,
                leadTypeId);
            if (contractsLog != null)
            {
                changeExecutiveLogList.Add(contractsLog);
            }

            ChangeExecutiveLog opportunitiesLog = ChangeOwnerInNotFinishedOpportunities(oldOwnerId, newOwnerId,
                leadTypeId);
            if (opportunitiesLog != null)
            {
                changeExecutiveLogList.Add(opportunitiesLog);
            }

            ChangeExecutiveLog leadLog = ChangeOwnerInNotFinishedLeads(oldOwnerId, newOwnerId, leadTypeId);
            if (leadLog != null)
            {
                changeExecutiveLogList.Add(leadLog);
            }

            return changeExecutiveLogList;
        }
    }
}