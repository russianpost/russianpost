define("EmailDetailV2", ["ConfigurationConstants"], function(ConfigurationConstants) {
	return {

		messages: {
			//Подписка на сообщение от ActivityPageV2.
			"onSaveButtonClick": {
				mode: this.Terrasoft.MessageMode.BROADCAST,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			},

			"onInitFileDetail": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.PUBLISH
			}
		},

		attributes: {
			"isEditable": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			}
		},

		methods: {
			init: function() {
				this.callParent(arguments);
				this.getStatusValue();
			},

			//Отправляет сообщение чтобы определить доступность детали
			getStatusValue: function() {
				var mesValue = this.sandbox.publish("onInitFileDetail", null, ["key1"]);
				if (mesValue) {
					this.set("isEditable", false);
				} else {
					this.set("isEditable", true);
				}
			},

			//Подписка на сообщение со страницы Активности
			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("onSaveButtonClick", function() {
					this.onSaveButtonClick();
				}, this, ["ActivityPageV2"]);
			},

			//Выполняется при выполнении save на странице Активности
			onSaveButtonClick: function() {
				this.set("isEditable", false);
	//			this.getAddRecordButtonEnabled();
			},

			//Базовый метод определения видимости кнопки AddRecordButton
			getAddRecordButtonEnabled: function() {
				return this.get("isEditable");
			}
		},

		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "ToolsButton",
				"values": {
					"enabled": {bindTo: "isEditable"}
				}
			}
		]
		/**SCHEMA_DIFF*/
	};
});