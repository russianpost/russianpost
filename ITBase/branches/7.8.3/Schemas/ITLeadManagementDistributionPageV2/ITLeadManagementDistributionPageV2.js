define("ITLeadManagementDistributionPageV2", ["BaseFiltersGenerateModule", "BusinessRuleModule", "terrasoft",
		"ITLeadManagementDistributionPageV2Resources", "LookupUtilities", "ConfigurationConstants",
		"ConfigurationEnums", "CustomProcessPageV2Utilities", "css!InfoButtonStyles"],
	function(BaseFiltersGenerateModule, BusinessRuleModule, Terrasoft, resources, LookupUtilities,
			ConfigurationConstants, Enums) {
		return {

			entitySchemaName: "Lead",

			mixins: {
				BaseProcessViewModel: "Terrasoft.CustomProcessPageV2Utilities"
			},

			attributes: {
				"Owner": {
					dataValueType: Terrasoft.DataValueType.LOOKUP,
					isRequired: true,
					lookupListConfig: {
						filter: function() {
							var owner = this.get("Owner");
							var filterGroup = Ext.create("Terrasoft.FilterGroup");
							if (owner) {
								filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.OR;
								filterGroup.add("CurrentOwnerLead",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Id",
										owner.value));
								filterGroup.add("LeadNotEmpty",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Owner.Id",
										owner.value));
							}
							else {
								var leadType = this.get("LeadType");
								filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.AND;
								filterGroup.add("TypeFilter",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Type.Id",
										ConfigurationConstants.ContactType.Employee));
								if (leadType) {
									filterGroup.add("CurrentOwnerLead",
										Terrasoft.createColumnFilterWithParameter(
											Terrasoft.ComparisonType.EQUAL,
											"[ITContactLeadType:ITContact].ITLeadType.Id",
											leadType.value));
								}
							}
							return filterGroup;
						}

					}
				}
			},

			rules: {
				"Owner": {
					RequireOwner: {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.REQUIRED,
						conditions: [{
							leftExpression: {
								type: BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								attribute: "Owner"
							},
							comparisonType: this.Terrasoft.ComparisonType.IS_NULL
						}]
					}
				}
			},

			methods: {


				isNotEmptyValue: function(value) {
					return !Ext.isEmpty(value);
				},


				setInitialData: function() {
					var parameters = this.get("ProcessData").parameters;
					this.set("Operation",  Enums.CardStateV2.EDIT);
					this.set("PrimaryColumnValue", parameters.LeadPageId);
					this.processParameters.push({key: "Result", value: ""});
				},


				initEntity: function(callback) {
					this.setInitialData();
					this.callParent(arguments);
					//this.callParent(
					//	[function() {
					//		this.loadReferencedSchemas(callback);
					//	}, this]
					//);
				},


				loadReferencedSchemas: function(callback) {
					var contact = this.get("QualifiedContact") || {value: Terrasoft.GUID_EMPTY};
					var account = this.get("QualifiedAccount") || {value: Terrasoft.GUID_EMPTY};
					this.readContact(contact.value, function(entity) {
						if (entity) {
							this.set("ContactName", entity.get("Name"));
							this.set("ContactDepartment", entity.get("Department"));
							this.set("ContactDecisionRole", entity.get("DecisionRole"));
							this.set("ContactJobTitle", entity.get("JobTitle"));
							this.set("ContactGender", entity.get("Gender"));
							this.set("ContactJob", entity.get("Job"));
						}
						this.readAccount(account.value, function(entity) {
							if (entity) {
								this.set("AccountName", entity.get("Name"));
								this.set("Ownership", entity.get("Ownership"));
								this.set("AccountIndustry", entity.get("Industry"));
								this.set("Category", entity.get("AccountCategory"));
								this.set("AccountEmployeesNumber", entity.get("EmployeesNumber"));
								this.set("AccountAnnualRevenue", entity.get("AnnualRevenue"));
							}
							if (callback) {
								callback.call(this);
							}
						});
					});
				},


				readEntity: function(schemaName, recordId, columns, callback) {
					if (recordId === Terrasoft.GUID_EMPTY) {
						if (callback) {
							callback.call(this);
						}
						return;
					}
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: schemaName
					});
					Terrasoft.each(columns, function(columnName) {
						esq.addColumn(columnName);
					});
					esq.getEntity(recordId, function(result) {
						if (callback) {
							callback.call(this, result.entity);
						}
					}, this);
				},


				readContact: function(contactId, callback) {
					this.readEntity("Contact", contactId,
						["Name", "Department", "DecisionRole", "JobTitle", "Gender", "Job"], callback);
				},


				readAccount: function(accountId, callback) {
					this.readEntity("Account", accountId, ["Name", "Ownership", "Industry", "AccountCategory",
						"EmployeesNumber", "AnnualRevenue"], callback);
				},


				getLeadFilters: function() {
					var contact = this.get("QualifiedContact");
					var contactId = (contact && contact.value) ? contact.value : this.Terrasoft.GUID_EMPTY;
					var leadId = this.get("Id");
					var filters = this.Terrasoft.createFilterGroup();
					filters.logicalOperation = this.Terrasoft.LogicalOperatorType.AND;
					filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "QualifiedContact", contactId));
					filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.NOT_EQUAL, "Id", leadId));
					return filters;
				},


				getContactFolderFilters: function() {
					var contact = this.get("QualifiedContact");
					var contactId = this.Terrasoft.GUID_EMPTY;
					if (contact && contact.value) {
						contactId = contact.value;
					}
					var filters = this.Terrasoft.createFilterGroup();
					filters.logicalOperation = this.Terrasoft.LogicalOperatorType.AND;
					filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Contact", contactId));
					return filters;
				},


				setTransferToSale: function() {
					this.processParameters.push({
						key: "Result",
						value: "TransferToSale"
					});
					this.completeProcess(true);
				},


				setDistributeLater: function() {
					this.processParameters.push({
						key: "Result",
						value: "DistributeLater"
					});
					this.completeProcess(false);
				},


				setNotInteresting: function() {
					this.processParameters.push({
						key: "Result",
						value: "NotInteresting"
					});
					this.completeProcess(false);
				},


				subscribeSandboxEvents: function() {
					this.callParent(arguments);
					var subscribersIds = this.getSaveRecordMessagePublishers();
					this.sandbox.subscribe("GetCardState", function() {
						return {state: Enums.CardStateV2.EDIT};
					}, this, subscribersIds);
				},


				getHeader: function() {
					return this.get("Resources.Strings.DistributionPageCaption");
				},


				initHeaderCaption: Ext.emptyFn,


				initPrintButtonMenu: Ext.emptyFn,


				updateButtonsVisibility: function() {
					this.callParent(arguments);
					this.set("ShowCloseButton", true);
				},


				onCloseCardButtonClick: function() {
					this.sandbox.publish("BackHistoryState");
				},


				onNextButtonClick: function() {
					this.acceptProcessElement("NextButton");
				},


				completeProcess: function(validate) {
					if (validate) {
						var resultObject = {
							success: this.validate()
						};
						if (!resultObject.success) {
							resultObject.message = this.getValidationMessage();
							this.validateResponse(resultObject);
							return;
						}
					}
					var owner = this.get("Owner");
					this.processParameters.push(
						{
							key: "LeadPageOwnerId",
							value:  (owner && owner.value) ? owner.value : null
						}
					);
					this.completeExecution(arguments);
				},


				pushProcessParameter: function(name) {
					var parameter = this.get(name) || null;
					this.processParameters.push(
						{
							key: name,
							value: (parameter && parameter.value) ? parameter.value : parameter
						}
					);
				}
			},

			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,

			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "insert",
					"name": "Owner",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"contentType": Terrasoft.ContentType.LOOKUP,
						"layout": {"column": 0, "row": 0},
						"bindTo": "Owner"
					}
				},
				{
					"operation": "merge",
					"name": "actions",
					"values": {
						"visible": false
					}
				},
				{
					"operation": "insert",
					"parentName": "LeftContainer",
					"propertyName": "items",
					"name": "DistributionButton",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"caption": {"bindTo": "Resources.Strings.DistributeButtonCaption"},
						"click": {"bindTo": "setTransferToSale"},
						"classes": {
							textClass: "actions-button-margin-right",
							"wrapperClass": ["actions-button-margin-right"]
						},
						"style": Terrasoft.controls.ButtonEnums.style.GREEN,
						"controlConfig": {},
						"layout": {column: 0, row: 0, colSpan: 2}
					},
					"index": 0
				},
				{
					"operation": "merge",
					"name": "SaveButton",
					"values": {
						"visible": false
					}
				},
				{
					"operation": "merge",
					"name": "DelayExecutionButton",
					"values": {
						"visible": false
					}
				},
				{
					"operation": "merge",
					"name": "DiscardChangesButton",
					"values": {
						"visible": false
					}
				},
				{
					"operation": "merge",
					"name": "ViewOptionsButton",
					"values": {
						"visible": false
					}
				},
				{
					"operation": "insert",
					"parentName": "RightContainer",
					"propertyName": "items",
					"name": "InformationButton",
					"values": {
						"itemType": Terrasoft.ViewItemType.INFORMATION_BUTTON,
						"controlConfig": {
							"classes": {
								"wrapperClass": "info-button-auto-padding"
							}
						},
						"content": {"bindTo": "Resources.Strings.DistributionInfoTip"},
						"tools": []
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});
