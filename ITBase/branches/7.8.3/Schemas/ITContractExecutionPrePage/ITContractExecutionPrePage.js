define("ITContractExecutionPrePage", ["CustomProcessPageV2Utilities", "BusinessRuleModule", "MaskHelper"],
	function(CustomProcessPageV2Utilities, BusinessRuleModule, MaskHelper) {
		return {

			mixins: {
				BaseProcessViewModel: "Terrasoft.CustomProcessPageV2Utilities"
			},

			attributes: {
				isResponsibleAppointed: {
					"dependencies": [
						{
							"columns": ["isResponsibleAppointed"],
							"methodName": "onResponsibleAppointedChanged"
						}
					]
				},

				ResponsibleVisibility: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				//Определяет возможность редактирования поля Стадия процесса
				isRecordExists: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				//Атрибут с фильтрами: 1) Филиал текущего пользователя ()
				GetOwner: {
					"dataValueType": Terrasoft.DataValueType.LOOKUP,
					"type": Terrasoft.core.enums.ViewModelSchemaItem.ATTRIBUTE,
					"isCollection": true,
					"referenceSchemaName": "Contact",
					"lookupListConfig": {
						"filters": [
							function() {
								var filterGroup = this.Ext.create("Terrasoft.FilterGroup");
		//						filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.AND;
								filterGroup.add("ByAccount", this.Terrasoft.createColumnFilterWithParameter(
									this.Terrasoft.ComparisonType.EQUAL, "Account", this.filtersArray[0]));
		/*						filterGroup.add("ByUFPS", this.Terrasoft.createColumnFilterWithParameter(
									this.Terrasoft.ComparisonType.EQUAL, "TsAccount", this.filtersArray[1]));*/
								//нужно добавить оператор ИЛИ + как то брать руководителей
								return filterGroup;
							}
						]
					}
				}
			},

			rules: {},

			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,

			methods: {
				onEntityInitialized: function() {
					this.setContactFilters();
					this.callParent(arguments);
					this.onResponsibleAppointedChanged();
					this.setParametersOnInitialized();
				},

				//Определяет видимость поля с выбором ответственного
				onResponsibleAppointedChanged: function() {
					var responsible = this.get("isResponsibleAppointed");
					if (responsible) {
						return this.set("ResponsibleVisibility", true);
					} else {
						this.set("GetOwner", null);
						return this.set("ResponsibleVisibility", false);
					}
					
					return responsible ? this.set("ResponsibleVisibility", true) : this.set("ResponsibleVisibility", false);
				},

				//Формурует массив для фильтрации контактов по УФПС и МРЦ
				setContactFilters: function() {
					this.filtersArray = [];
					var recordId = this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value;
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Contact"
					});
	//				esq.addColumn("ITMacroregion");
					esq.addColumn("Account");
					esq.getEntity(recordId, function(result) {
						this.filtersArray.push(result.entity.get("Account").value);
	//					this.filtersArray.push(result.entity.get("TsAccount").value);
					}, this);
				},

				//Записывает значения параметров из таблицы ITContractExecutionObject
				setParametersOnInitialized: function() {
					var opportunityId = this.get("OpportunityId");
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "ITContractExecutionObject"
					});
					esq.addColumn("Id");
					esq.addColumn("ITGetOwner");
					esq.addColumn("ITisResponsibleAppointed");
					esq.addColumn("ITisMailSent");
					esq.addColumn("ITisContractTransferred");
					esq.addColumn("ITisOwnerFilled");
					esq.filters.add("ByProductId", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ITOpportunity.Id", opportunityId));
					esq.getEntityCollection(function(result) {
						if (!result.success || result.collection.collection.length === 0) {
							//Устанавливает флаг того, что запись в бд уже существует
							this.set("isRecordExists", false);
							return;
						}
						result.collection.each(function(item) {
							//Устанавливает флаг того, что запись в бд уже существует
							this.set("isRecordExists", true);
							this.detailRowId = item.get("Id");
							this.set("GetOwner", item.get("ITGetOwner"));
							this.set("isResponsibleAppointed", item.get("ITisResponsibleAppointed"));
							this.set("isMailSent", item.get("ITisMailSent"));
							this.set("isContractTransferred", item.get("ITisContractTransferred"));
							this.set("isOwnerFilled", item.get("ITisOwnerFilled"));
						}, this);
					
					}, this);
				},

				//При нажатии на кнопку сохранить либо переходит на следующую стадию процесса, либо возвращает
				onNextOrCloseButtonClick: function() {
					var recordExists = this.get("isRecordExists"),
						owner = this.get("GetOwner"),
						ownerValue = owner ? owner.value : null,
						responsibleAppointed = this.get("isResponsibleAppointed"),
						mailSent = this.get("isMailSent"),
						contractTransferred = this.get("isContractTransferred"),
						ownerFilled;
					this.set("isOwnerFilled", (ownerValue && ownerValue !== "00000000-0000-0000-0000-000000000000") ? true : false);
					ownerFilled = this.get("isOwnerFilled");
					if (ownerFilled && responsibleAppointed && mailSent && contractTransferred) {
						this.isNextStep = true;
						if (recordExists) {
							this.updateParametersInDB();
						} else {
							this.insertParametersInDB();
						}
					} else {
						this.isNextStep = false;
						if (recordExists) {
							this.updateParametersInDB();
						} else {
							this.insertParametersInDB();
						}
					}
				},

				//Update записи по данной продаже
				updateParametersInDB: function() {
					var opportunityId = this.get("OpportunityId"),
						owner = this.get("GetOwner"),
						ownerValue = owner ? owner.value : null,
						responsibleAppointed = this.get("isResponsibleAppointed"),
						mailSent = this.get("isMailSent"),
						contractTransferred = this.get("isContractTransferred"),
						ownerFilled;
					this.set("isOwnerFilled", (ownerValue && ownerValue !== "00000000-0000-0000-0000-000000000000") ? true : false);
					ownerFilled = this.get("isOwnerFilled");
					
					var update = this.Ext.create("Terrasoft.UpdateQuery", {
						rootSchemaName: "ITContractExecutionObject"
					});
					update.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Id", this.detailRowId));
					update.setParameterValue("ITGetOwner", ownerValue, this.Terrasoft.DataValueType.GUID);
					update.setParameterValue("ITisResponsibleAppointed", responsibleAppointed, this.Terrasoft.DataValueType.BOOLEAN);
					update.setParameterValue("ITisMailSent", mailSent, this.Terrasoft.DataValueType.BOOLEAN);
					update.setParameterValue("ITisContractTransferred", contractTransferred, this.Terrasoft.DataValueType.BOOLEAN);
					update.setParameterValue("ITisOwnerFilled", ownerFilled, this.Terrasoft.DataValueType.BOOLEAN);
					update.execute(function() {
						switch (this.isNextStep) {
							case true:
								var updateOpportunity = this.Ext.create("Terrasoft.UpdateQuery", {
										rootSchemaName: "Opportunity"
									});
								updateOpportunity.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
									this.Terrasoft.ComparisonType.EQUAL, "Id", opportunityId));
								updateOpportunity.setParameterValue("ITResponsible", ownerValue, this.Terrasoft.DataValueType.GUID);
								updateOpportunity.execute(function() {
									this.acceptProcessElement("NextButton");
								}, this);
								break;
							default:
								this.sandbox.publish("BackHistoryState");
						}
					}, this);
				},

				//Insert новой строки, если такой еще нет
				insertParametersInDB: function() {
					var opportunityId = this.get("OpportunityId"),
						owner = this.get("GetOwner"),
						ownerValue = owner ? owner.value : null,
						responsibleAppointed = this.get("isResponsibleAppointed"),
						mailSent = this.get("isMailSent"),
						contractTransferred = this.get("isContractTransferred"),
						ownerFilled;
					this.set("isOwnerFilled", (ownerValue && ownerValue !== "00000000-0000-0000-0000-000000000000") ? true : false);
					ownerFilled = this.get("isOwnerFilled");
					
					var insert = this.Ext.create("Terrasoft.InsertQuery", {
						rootSchemaName: "ITContractExecutionObject"
					});
					insert.setParameterValue("ITOpportunity", opportunityId, this.Terrasoft.DataValueType.GUID);
					insert.setParameterValue("ITGetOwner", ownerValue, this.Terrasoft.DataValueType.GUID);
					insert.setParameterValue("ITisResponsibleAppointed", responsibleAppointed, this.Terrasoft.DataValueType.BOOLEAN);
					insert.setParameterValue("ITisMailSent", mailSent, this.Terrasoft.DataValueType.BOOLEAN);
					insert.setParameterValue("ITisContractTransferred", contractTransferred, this.Terrasoft.DataValueType.BOOLEAN);
					insert.setParameterValue("ITisOwnerFilled", ownerFilled, this.Terrasoft.DataValueType.BOOLEAN);
					insert.execute(function() {
						switch (this.isNextStep) {
							case true:
								var updateOpportunity = this.Ext.create("Terrasoft.UpdateQuery", {
										rootSchemaName: "Opportunity"
									});
								updateOpportunity.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
									this.Terrasoft.ComparisonType.EQUAL, "Id", opportunityId));
								updateOpportunity.setParameterValue("ITResponsible", ownerValue, this.Terrasoft.DataValueType.GUID);
								updateOpportunity.execute(function() {
									this.acceptProcessElement("NextButton");
								}, this);
								break;
							default:
								this.sandbox.publish("BackHistoryState");
						}
					}, this);
				},

				//Возвращаем необходимое название для страницы
				getHeader: function() {
					return "Оформление договора внутри предприятия";
				},

				//Переопределение метода для прорисовки caption данной страницы
				getPageHeaderCaption: function() {
					var pageName = this.name;
					var header = (pageName === "ITContractExecutionPrePage") ? this.getHeader() : "";
					return header;
				},

				initHeaderCaption: Ext.emptyFn,

				initPrintButtonMenu: Ext.emptyFn,

				loadVocabulary: function(args, tag) {
					var column = this.getColumnByName(tag);
					args.schemaName = column.referenceSchemaName;
					this.callParent(arguments);
				},

				getLookupPageConfig: function(args, columnName) {
					var config = {
						entitySchemaName: args.schemaName,
						multiSelect: false,
						columnName: columnName,
						columnValue: this.get(columnName),
						searchValue: args.searchValue,
						filters: this.getLookupQueryFilters(columnName)
					};
					this.Ext.apply(config, this.getLookupListConfig(columnName));
					return config;
				},

				onCloseCardButtonClick: function() {
					this.sandbox.publish("BackHistoryState");
				}
			},

			diff: /**SCHEMA_DIFF*/[
				// Удаляем лишние кнопки
				{
					operation: "remove",
					name: "ActionButtonsContainer"
				},

				{
					operation: "remove",
					name: "DiscardChangesButton"
				},

				{
					operation: "remove",
					name: "CloseButton"
				},

	/*			{
					"operation": "remove",
					"name": "Tabs"
				},*/

				{
					operation: "remove",
					name: "DelayExecutionButton"
				},

				{
					operation: "remove",
					name: "ViewOptionsButton"
				},

				{
					operation: "remove",
					name: "SaveButton"
				},

				{
					"operation": "remove",
					"name": "actions"
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITCLoseButton",
					"values": {
						"caption": "Закрыть",
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"classes": {textClass: "actions-button-margin-right"},
						"style": Terrasoft.controls.ButtonEnums.style.RED,
						"click": {"bindTo": "onCloseClick"},
						"layout": {column: 8, row: 5, colSpan: 4}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "NextButton",
					"values": {
						"caption": "Сохранить",
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"classes": {textClass: "actions-button-margin-right"},
						"style": Terrasoft.controls.ButtonEnums.style.GREEN,
						"click": {bindTo: "onNextOrCloseButtonClick"},
						"layout": {column: 12, row: 5, colSpan: 4}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "isResponsibleAppointed",
					"values": {
						"caption": "Назначен ответственный поддержки",
						"bindTo": "isResponsibleAppointed",
						"dataValueType": Terrasoft.DataValueType.BOOLEAN,
						"layout": {column: 2, row: 0, colSpan: 3}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "GetOwner",
					"values": {
						"caption": "ФИО ответственного поддержки",
						"contentType": Terrasoft.ContentType.LOOKUP,
						"visible": {bindTo: "ResponsibleVisibility"},
						"bindTo": "GetOwner",
						"layout": {column: 9, row: 0, colSpan: 9}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "isMailSent",
					"values": {
						"caption": "В точку сдачи направлено пояснительное письмо по условиям договора",
						"bindTo": "isMailSent",
						"dataValueType": Terrasoft.DataValueType.BOOLEAN,
						"layout": {column: 2, row: 1, colSpan: 3}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "isContractTransferred",
					"values": {
						"caption": "В бухгалтерию передан договор",
						"bindTo": "isContractTransferred",
						"dataValueType": Terrasoft.DataValueType.BOOLEAN,
						"layout": {column: 2, row: 2, colSpan: 3}
					}
				}
				
			]/**SCHEMA_DIFF*/
		};
	});