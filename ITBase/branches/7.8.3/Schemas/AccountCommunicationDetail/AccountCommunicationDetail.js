define("AccountCommunicationDetail", [], function() {
	return {
		entitySchemaName: "AccountCommunication",
		methods: {
			init: function() {
				var isNotOurCompanyType = this.sandbox.publish("GetIsNotOurCompanyTypeForSecondDetail", null, null);
				this.set("isNotOurCompanyType", isNotOurCompanyType);
				this.callParent(arguments);
				this.sandbox.subscribe("SetIsNotOurCompanyTypeForDetail", function(isNotOurCompanyTypeArg) {
					this.set("isNotOurCompanyType", isNotOurCompanyTypeArg);
					this.initToolsMenuItems();
				}, this, null);
			},
			addSocialNetworkMenuItem: function() {
				if (this.get("isNotOurCompanyType")) {
					this.callParent(arguments);
				}
			},
			getSocialNetworkVisible: function() {
				return this.get("isNotOurCompanyType") && this.getToolsVisible();
			}
		},
		messages: {
			"GetIsNotOurCompanyTypeForSecondDetail": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.PUBLISH
			},
			"SetIsNotOurCompanyTypeForDetail": {
				mode: this.Terrasoft.MessageMode.BROADCAST,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		attribute: {
			"isNotOurCompanyType": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: true
			}
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "SocialNetworksContainer",
				"parentName": "Detail",
				"propertyName": "tools",
				"values": {
					"visible": {"bindTo": "getSocialNetworkVisible"}
				}
			}
		]/**SCHEMA_DIFF*/
	};
});