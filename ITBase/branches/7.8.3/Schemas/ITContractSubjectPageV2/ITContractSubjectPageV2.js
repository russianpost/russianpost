define("ITContractSubjectPageV2", [], function() {
	return {
		entitySchemaName: "ITContractSubject",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
	{
		"operation": "insert",
		"name": "ITProductId89accefd-c95a-493f-9de3-0ab30357798a",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 0,
				"row": 0,
				"layoutName": "Header"
			},
			"bindTo": "ITProductId"
		},
		"parentName": "Header",
		"propertyName": "items",
		"index": 0
	},
	{
		"operation": "insert",
		"name": "ITCostPerItemf2e6efbf-351d-4860-808f-64ae564868ac",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 12,
				"row": 0,
				"layoutName": "Header"
			},
			"bindTo": "ITCostPerItem"
		},
		"parentName": "Header",
		"propertyName": "items",
		"index": 1
	}
]/**SCHEMA_DIFF*/,
		methods: {},
		rules: {}
	};
});
