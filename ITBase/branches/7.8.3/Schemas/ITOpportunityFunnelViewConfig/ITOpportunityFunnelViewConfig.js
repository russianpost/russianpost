define("ITOpportunityFunnelViewConfig", ["ITOpportunityFunnelViewConfigResources", "sandbox", "LookupUtilities"], function(resources, sandbox, LookupUtilities) {


        Ext.define("Terrasoft.configuration.ITOpportunityFunnelViewConfig", {
            extend: "Terrasoft.OpportunityFunnelViewConfig",
            alternateClassName: "Terrasoft.ITOpportunityFunnelViewConfig",

            getPeriodFilterViewConfig: function(filterConfig) {
                var config = this.callParent(arguments);
                var lookupLeadTypeCaption = {
                    id: "leadTypeLabel",
                    className: "Terrasoft.Label",
                    labelClass: "t-label",
                    caption: resources.localizableStrings.LeadTypeFilterCaption
                };
                var lookupLeadType = {
                    className: "Terrasoft.ComboBoxEdit",
                    markerValue: "LeadTypeEdit",
                    classes: { wrapClass: "filter-simple-filter-edit" },
                    value: { bindTo: "LeadTypeValue" },
                    list: { bindTo: "LeadTypeList" },
                    prepareList: {bindTo: "getLeadTypeList"},
                    change: {bindTo: "onLeadTypeValueChange"}
                };
                config[0].items.push(lookupLeadTypeCaption);
                config[0].items.push(lookupLeadType);
                return config;
            }




        });
    }
);
