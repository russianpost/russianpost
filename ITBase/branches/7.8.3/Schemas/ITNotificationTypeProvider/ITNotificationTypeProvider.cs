namespace Terrasoft.Configuration
{
	using Terrasoft.Core;
	using Terrasoft.Core.DB;
	using Terrasoft.Core.Entities;
	using Terrasoft.Core.Configuration;
	using System.Collections.Generic;
	using Terrasoft.Core;
	using Terrasoft.Common;
	using System;

	#region Class: ITNotificationTypeProvider

	
	
	
	public class ITNotificationTypeProvider : BaseNotificationProvider, INotification
	{
		#region Constants: Private

		private const string GROUP_NAME = "Notification";
		private const string NAME = "Notification";

		#endregion

		#region Constructors: Public

		public ITNotificationTypeProvider(UserConnection userConnection)
			: base(userConnection) {
		}

		
		
		
		
		public ITNotificationTypeProvider(Dictionary<string, object> parameters)
			: base(parameters) {
		}

		#endregion

		#region Properties: Public

		
		
		
		public string Group {
			get {
				return GROUP_NAME;
			}
		}

		
		
		
		public string Name {
			get {
				return NAME;
			}
		}

		#endregion

		#region Methods: Protected

		protected override void AddColumns(Select select) {
			select
				.Column("Reminding", "Id").As("Id")
				.Column("Reminding", "RemindTime").As("RemindTime")
				.Column("Reminding", "ContactId").As("RemindingContactId")
				.Column("Reminding", "Description").As("Description")
				.Column("Reminding", "SubjectId").As("SubjectId")
				.Column("SysAdminUnit", "Id").As("SysAdminUnitId")
				.Column("Reminding", "SubjectCaption").As("Title")
				.Column("SysSchema", "Name").As("EntitySchemaName")
				.Column("NotificationsSettings", "SysImageId").As("ImageId");
		}

		protected override void JoinTables(Select select) {
			select
				.InnerJoin("SysAdminUnit").On("Reminding", "ContactId").IsEqual("SysAdminUnit", "ContactId")
				.LeftOuterJoin("SysSchema").On("Reminding", "SysEntitySchemaId").IsEqual("SysSchema", "UId")
				.LeftOuterJoin("NotificationsSettings")
						.On("Reminding", "SysEntitySchemaId").IsEqual("NotificationsSettings", "SysEntitySchemaUId");
		}

		protected override void AddConditions(Select select) {
			base.AddConditions(select);
			select.And("Reminding", "NotificationTypeId")
					.IsEqual(Column.Parameter(RemindingConsts.NotificationTypeNotificationId));
		}

		protected override INotificationInfo GetRecordNotificationInfo(
						Dictionary<string, string> dictionaryColumnValues) {
			Guid imageId;
			Guid.TryParse(dictionaryColumnValues["ImageId"], out imageId);
			return new NotificationInfo() {
				Title = dictionaryColumnValues["Title"],
				Body = dictionaryColumnValues["Description"],
				ImageId = imageId,
				EntityId = new Guid(dictionaryColumnValues["SubjectId"]),
				EntitySchemaName = dictionaryColumnValues["EntitySchemaName"],
				MessageId = new Guid(dictionaryColumnValues["Id"]),
				SysAdminUnit = new Guid(dictionaryColumnValues["SysAdminUnitId"]),
				GroupName = Group
			};
		}

		#endregion

		#region Methods: Public

		
		
		
		public new int GetCount() {
			//return 7;
			int result = 0;
			Guid sysAdminUnitId = new Guid();
			if (!Guid.TryParse(parameters["sysAdminUnitId"].ToString(), out sysAdminUnitId)) {
				return result;
			}
			DateTime date = new DateTime();
			if (!DateTime.TryParse(parameters["dueDate"].ToString(), out date)) {
				return result;
			}
			var countSelect = new Select(UserConnection)
				.Column(Func.Count("Reminding", "Id"))
					.Distinct()
				.From("Reminding")
					.LeftOuterJoin("SysAdminUnit")
						.On("SysAdminUnit", "ContactId").IsEqual("Reminding", "ContactId")
				.Where("RemindTime").IsLessOrEqual(Column.Const(date))
				.And("IsRead").IsEqual(Column.Parameter(0))
				.And(Column.SqlText("[SysAdminUnit].[Id]")).IsEqual(Column.Parameter(sysAdminUnitId)) as Select;
			result = countSelect.ExecuteScalar<int>();
			return result;
		}

		public override void SetColumns(List<string> columns) {
			columns.Add("Id");
			columns.Add("ContactId");
			columns.Add("RemindTime");
			columns.Add("SubjectId");
			columns.Add("Title");
			columns.Add("Description");
			columns.Add("EntitySchemaName");
			columns.Add("ImageId");
		}

		public override string GetRecordResult(Dictionary<string, string> dictionaryColumnValues) {
			var title = dictionaryColumnValues["Title"];
			var id = dictionaryColumnValues["Id"];
			var remindTime = dictionaryColumnValues["RemindTime"];
			var subjectId = dictionaryColumnValues["SubjectId"];
			string body = dictionaryColumnValues["Description"];
			var key = id + "_" + remindTime; 
			var schema = dictionaryColumnValues["EntitySchemaName"];
			var imageId = dictionaryColumnValues["ImageId"];
			var popup = new PopupData() {
				Title = title,
				Body = body, 
				ImageId = imageId,
				EntityId = subjectId,
				EntitySchemaName = schema
			}; 
			var serializePopup = popup.Serialize();
			return string.Format("\"{0}\": {1}", key, serializePopup);
		}

		
		
		
		
		public IEnumerable<INotificationInfo> GetNotifications() {
			return GetNotificationsInfos();
		}

		
		
		
		
		public override Select GetEntitiesSelect() {
			Guid sysAdminUnitId = (Guid)this.parameters["sysAdminUnitId"];
			DateTime date = (DateTime)this.parameters["dueDate"];
			var select = new Select(UserConnection)
				.Column("Reminding", "Id").As("Id")
				.Column("Reminding", "RemindTime").As("RemindTime")
				.Column("Reminding", "ContactId").As("ContactId")
				.Column("Reminding", "Description").As("Description")
				.Column("Reminding", "SubjectId").As("SubjectId")
				.Column("Reminding", "SubjectCaption").As("Title")
				.Column("Contact", "Name").As("ContactName")
				.Column("NotificationsSettings", "SysImageId").As("ImageId")
				.Column("SysSchema", "Name").As("EntitySchemaName")
				.From("Reminding")
				.InnerJoin("Contact").On("Reminding", "ContactId").IsEqual("Contact", "Id")
				.LeftOuterJoin("SysSchema").On("Reminding", "SysEntitySchemaId").IsEqual("SysSchema", "UId")
				.LeftOuterJoin("SysAdminUnit").On("Reminding", "ContactId").IsEqual("SysAdminUnit", "ContactId")
				.LeftOuterJoin("NotificationsSettings")
					.On("Reminding", "SysEntitySchemaId").IsEqual("NotificationsSettings", "SysEntitySchemaUId")
				.Where("RemindTime").IsLessOrEqual(Column.Parameter(date.ToUniversalTime()))
				.And("Reminding", "NotificationTypeId")
					.IsEqual(Column.Parameter(RemindingConsts.NotificationTypeNotificationId))
				.And("SysAdminUnit", "Id").IsEqual(Column.Parameter(sysAdminUnitId)) as Select;
			return select;
		}


		#endregion
	}

	#endregion
}