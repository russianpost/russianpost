define("RectProgressBar", ["ext-base", "css!RectProgressBar", "BaseProgressBarIndicator"],
		function(Ext) {

			
			Ext.define("Terrasoft.controls.RectProgressBar", {
				alternateClassName: "Terrasoft.RectProgressBar",
				extend: "Terrasoft.BaseProgressBarIndicator",

				

				
				horizontal: false,

				

				

				
				horizontalClass: "horizontal-progress-bar",

				
				valueCssAttibute: "height",

				
				captionAlignCssAttibute: "bottom",

				
				tpl: [
					
					
					'<div id="{id}-wrap" class="{wrapClass} ts-rect-progress-bar-wrap completeness-indicator-wrap {horizontalClass}" progress-data="{value}">',
					'<div id="{id}-rect" class="rect" style="{rectElStyle}"></div>',
					'<div id="{id}-overlay" class="overlay" style="{overlayElStyle}" data-item-marker="{id}">',
					'<span class="caption">{caption}',
					'<tpl if="captionSuffix">',
					'<span class="caption-suffix">{captionSuffix}</span>',
					'</tpl>',
					'</span>',
					'</div>',
					'<tpl if="menu">',
					'<span id="{id}-menuWrapEl" class="{menuWrapClass}"></span>',
					'</tpl>',
					'</div>'
					
					
				],

				

				

				
				init: function() {
					this.callParent(arguments);
					this.setDefaultCssAttributes();
					this.selectors = {
						wrapEl: "#" + this.id + "-wrap",
						rectEl: "#" + this.id + "-rect",
						overlayEl: "#" + this.id + "-overlay"
					};
				},

				
				setDefaultCssAttributes: function() {
					if (this.horizontal) {
						this.valueCssAttibute = "width";
						this.captionAlignCssAttibute = "left";
					}
				},

				
				getTplData: function() {
					var tplData = this.callParent(arguments);
					tplData.horizontalClass = this.horizontal ? this.horizontalClass : "";
					this.setSectorsProgressColor();
					return tplData;
				},

				
				onAfterRender: function() {
					this.callParent(arguments);
					this.setWrapClasses();
				},

				
				onAfterReRender: function() {
					this.callParent(arguments);
					this.setWrapClasses();
				},

				
				setWrapClasses: function() {
					if (this.rectEl) {
						var valueStyle = {
							"background-color": this.progressColor
						};
						valueStyle[this.valueCssAttibute] = this.value + "%";
						this.rectEl.setStyle(valueStyle);
					}
					if (this.overlayEl) {
						var captionStyle = {
							"color": this.progressColor
						};
						var captionOffset = this.horizontal ?
								(this.overlayEl.dom.clientWidth / 2) :
								(this.overlayEl.dom.clientHeight / 2);
						captionStyle[this.captionAlignCssAttibute] =
								"calc(" + this.value + "% - " + captionOffset + "px)";
						this.overlayEl.setStyle(captionStyle);
					}
				}

				
			});
			return Terrasoft.RectProgressBar;
		}
);
