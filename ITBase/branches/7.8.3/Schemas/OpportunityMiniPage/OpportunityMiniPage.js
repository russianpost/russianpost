define("OpportunityMiniPage", ["BusinessRuleModule", "ConfigurationConstants", "ProcessModuleUtilities"],
		function(BusinessRuleModule, ConfigurationConstants, ProcessModuleUtilities) {
			return {
				entitySchemaName: "Opportunity",

				attributes: {

					"Stage": {
						lookupListConfig: {
							columns: ["ITNumber"],
							orders: [
								{
									columnPath: "ITNumber",
									orderDirection: Terrasoft.OrderDirection.ASC
								}
							]
						}
					},

					"Client": {
						"multiLookupColumns": ["Contact"],
						"isRequired": false
					},

					"LeadType": {
						"dependencies": [
							{
								"columns": ["LeadType"],
								"methodName": "setStageEditable"
							}
						]
					},

					"StageEditable":
					{
						dataValueType: Terrasoft.DataValueType.BOOLEAN,
						value: false
					}
				},
				messages: {
					"SetClientForOpportunityMiniPage": {
						mode: Terrasoft.MessageMode.PTP,
						direction: Terrasoft.MessageDirectionType.PUBLISH
					}
				},
				rules: {
					"Account": {
						"FiltrationClientByNotOurCompany": {
							"ruleType": 999
						},
						"FiltrationOrderByNotCompetitor": {
							"ruleType": 999
						},
						"FiltrationAccountByType": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"baseAttributePatch": "Type",
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"type": BusinessRuleModule.enums.ValueType.CONSTANT,
							"value": ConfigurationConstants.AccountType.Client
						}
					},
					"Stage": {
						"FiltrationStageByEnd": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"baseAttributePatch": "End",
							"comparisonType": Terrasoft.ComparisonType.EQUAL,
							"type": BusinessRuleModule.enums.ValueType.CONSTANT,
							"value": false
						}
					/*	"FiltrationStageByTsLeadType": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"baseAttributePatch": "TsLeadType",
							"comparisonType": Terrasoft.ComparisonType.EQUAL,
							"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
							"attribute": "TsLeadType"
						}*/
					}
				},

				methods: {

					/**Методы для выполнения сортировки справочника через атрибут**/
					getLookupQuery: function(filterValue, columnName) {
						var esq = this.callParent(arguments);
						this.applyColumnsOrderToLookupQuery(esq, columnName);
						this.addRelatedColumnsToLookupQuery(esq, columnName);
						var filterGroup = this.getLookupQueryFilters(columnName);
						esq.filters.addItem(filterGroup);
						return esq;
					},

					applyColumnsOrderToLookupQuery: function(esq, columnName) {
						var lookupColumn = this.getColumnByName(columnName);
						var lookupListConfig = lookupColumn.lookupListConfig;
						if (!lookupListConfig || !lookupListConfig.orders) {
							return;
						}
						var columns = esq.columns;
						this.Terrasoft.each(lookupListConfig.orders, function(order) {
							var orderColumnPath = order.columnPath;
							if (!columns.contains(orderColumnPath)) {
								esq.addColumn(orderColumnPath);
							}
							var sortedColumn = columns.get(orderColumnPath);
							var direction = order.direction;
							sortedColumn.orderDirection = direction ? direction : Terrasoft.OrderDirection.ASC;
							var position = order.position;
							sortedColumn.orderPosition = position ? position : 1;
							this.shiftColumnsOrderPosition(columns, sortedColumn);
						}, this);
					},

					shiftColumnsOrderPosition: function(columns, sortedColumn) {
						var sortedColumnOrderPosition = sortedColumn.orderPosition;
						if (Ext.isNumber(sortedColumnOrderPosition)) {
							columns.each(function(column) {
								if (column !== sortedColumn && Ext.isNumber(column.orderPosition) &&
									column.orderPosition >= sortedColumnOrderPosition) {
									column.orderPosition += 1;
								}
							});
						}
					},
					/**Методы для выполнения сортировки справочника через атрибут**/
					onSaved: function() {
						this.callParent(arguments);
						var OppStage = this.get("Stage");
						if (OppStage) {
							var IsFinalStage = OppStage.End;
							if (this.isAddMode() && !IsFinalStage) {
								this.showConfirmationDialog(this.get("Resources.Strings.ConfirmRunOpportunityProcessMessage"),
								function(result) {
									if (result === Terrasoft.MessageBoxButtons.YES.returnCode) {
										ProcessModuleUtilities.executeProcess({
											sysProcessName: "ITTsOpportunityManagement3",
											parameters: {
												CurrentOpportunity: this.get("Id")
											}
										});
									}
								}, ["yes", "no"]);
							}
						}
					},
					setStageEditable: function() {
						var StageEditable = this.get("LeadType"),
							LeadTypeB2BId = "9513105a-63c4-4952-96bd-3700d05c2f23",
							LeadTypeB2CId = "410ad894-d2a9-406c-970c-669b4a653d07",
							currentLeadTypeId = this.get("LeadType") ? this.get("LeadType").value : null,
							IsNeedBlock = (currentLeadTypeId === LeadTypeB2BId || currentLeadTypeId === LeadTypeB2CId);
						if (StageEditable) {
							this.set("StageEditable", true);
						}
						else if (!StageEditable) {
							this.set("Stage", null);
							this.set("StageEditable", false);
						}
						//#RPCRM-1144 Если посылочный блок B2B или B2C, то заблочить поле "Стадия" и присвоить ей Презентация
						if (IsNeedBlock) {
							this.set("StageEditable", false);
							var presentationObj = { "ITNumber": 0,
								"[TsOppStageInLeadType:TsOpportunityStage].TsLeadType": this.get("LeadType"),
								"customHtml": "Презентация",
								"displayValue": "Презентация",
								"markerValue": "Презентация",
								"primaryImageVisible": false,
								"value": "325f0619-0ee0-df11-971b-001d60e938c6"};
							this.set("Stage", presentationObj);
						}
					},
					
					onEntityInitialized: function() {
						this.callParent(arguments);
						//#RPCRM-898 Если пришло сообщение с данными контрагента для привязки, то вставить его в поле
						var sandbox = this.sandbox,
							clientFromAccountPage = sandbox.publish("SetClientForOpportunityMiniPage", null);
						if (clientFromAccountPage) {
							this.set("Account", clientFromAccountPage);
						}
						
					},

					//#RPCRM-898 Если пришло сообщение с данными контрагента для привязки, то заблочить поле
					IsBlockClient: function() {
						var sandbox = this.sandbox,
							clientFromAccountPage = sandbox.publish("SetClientForOpportunityMiniPage", null);
						if (clientFromAccountPage) {
							return false;
						}
						return true;
					}
				},

				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,

				diff: [
					{
						"operation": "remove",
						"name": "Client"
					},
					{
						"operation": "insert",
						"parentName": "MiniPage",
						"propertyName": "items",
						"name": "ITAccount",
						"values": {
							"bindTo": "Account",
							"caption": {"bindTo": "Resources.Strings.Client"},
							"visible": {"bindTo": "isNotViewMode"},
							"showValueAsLink": false,
							"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
							"contentType": this.Terrasoft.ContentType.LOOKUP,
							"layout": {"column": 0, "row": 1, "colSpan": 24},
							"isMiniPageModelItem": true,
							"isRequired": true,
							"enabled": {"bindTo": "IsBlockClient"}
						}
					},
					/*{
						"operation": "merge",
						"parentName": "MiniPage",
						"propertyName": "items",
						"name": "Client",
						"values": {
							"visible": {"bindTo": "isNotViewMode"},
							"tip": {
								"content": {"bindTo": "Resources.Strings.ClientTip"}
							},
							"controlWrapConfig": {
								"classes": {"wrapClassName": ["client-edit-field"]}
							},
							"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
							"contentType": this.Terrasoft.ContentType.LOOKUP,
							"controlConfig": {
								"enableLeftIcon": true,
								"focused": true,
								"leftIconConfig": {"bindTo": "getMultiLookupIconConfig"}
							},
							"layout": {"column": 0, "row": 1, "colSpan": 24},
							"isMiniPageModelItem": true
						}
					},
					{
						"operation": "insert",
						"name": "Account",
						"parentName": "MiniPage",
						"propertyName": "items",
						"values": {
							"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
							"contentType": this.Terrasoft.ContentType.LOOKUP,
							"visible": {
								"bindTo": "Account",
								"bindConfig": {
									converter: "isNotEmptyColumnVisibleInViewMode"
								}
							},
							"layout": {"column": 0, "row": 5, "colSpan": 18},
							"labelConfig": {
								"caption": {"bindTo": "Resources.Strings.Client"}
							},
							"isMiniPageModelItem": true
						}
					}*/
					{
						"operation": "merge",
						"name": "Stage",
						"parentName": "MiniPage",
						"propertyName": "items",
						"values": {
							"enabled": {"bindTo": "StageEditable"},
							"layout": {
								"column": 0,
								"row": 4,
								"colSpan": 24
							}
						},
						"isMiniPageModelItem": true
					}
				]
			};
		});
