define("ITChooseMainContactPageV2", ["OpportunityConfigurationConstants","BaseFiltersGenerateModule", "BusinessRuleModule", "terrasoft",
        "LookupUtilities", "ConfigurationConstants",
        "ConfigurationEnums", "CustomProcessPageV2Utilities", "css!InfoButtonStyles"],
    function(OpportunityConfigurationConstants, BaseFiltersGenerateModule, BusinessRuleModule, Terrasoft, LookupUtilities,
             ConfigurationConstants, Enums) {
        return {
            entitySchemaName: "OpportunityContact",
            mixins: {
                BaseProcessViewModel: "Terrasoft.CustomProcessPageV2Utilities"
            },
			attributes: {
				"Role": {
					onChange: "setInfluence"
				}
			},
            details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
            methods: {

                subscribeSandboxEvents: function() {
                    this.callParent(arguments);
                    var subscribersIds = this.getSaveRecordMessagePublishers();
                    this.sandbox.subscribe("GetCardState", function() {
                        return {state: Enums.CardStateV2.EDIT};
                    }, this, subscribersIds);
                },

                initEntity: function(callback) {
                    this.setInitialData();
                    this.callParent(
                        [function() {
                            this.loadReferencedSchemas(callback);
                        }, this]
                    );
                },

                setInitialData: function() {
                    var parameters = this.get("ProcessData").parameters;
                    this.set("Id", Terrasoft.generateGUID());
                    this.set("Operation",  Enums.CardStateV2.ADD);
                    this.set("BaseOpportunity", parameters.OpportunityId);
                    this.processParameters.push({key: "Result", value: ""});
                },


                loadReferencedSchemas: function(callback) {
                    var opporunityId = this.get("BaseOpportunity") || {value: Terrasoft.GUID_EMPTY};
                    this.readOpporunity(opporunityId, function(entity) {
                        if (entity && entity.values) {
                            this.set("Opportunity", {value: entity.values.Id, displayValue: entity.values.Title});
                        }
                    });
                    var roleId = OpportunityConfigurationConstants.OppContactRole.DecisionMaker;
                    this.readRole(roleId, function(entity) {
                        if (entity && entity.values) {
                            this.set("Role", {value: entity.values.Id, displayValue: entity.values.Name});
                        }
                    });
                    this.hideBodyMask();
                },

                readOpporunity: function(opporunityId, callback) {
                    this.readEntity("Opportunity", opporunityId,
                        ["Title"], callback);
                },

                readRole: function(roleId, callback) {
                    this.readEntity("OppContactRole", roleId,
                        ["Name"], callback);
                },

                readEntity: function(schemaName, recordId, columns, callback) {
                    if (recordId === Terrasoft.GUID_EMPTY) {
                        if (callback) {
                            callback.call(this);
                        }
                        return;
                    }
                    var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
                        rootSchemaName: schemaName
                    });
                    Terrasoft.each(columns, function(columnName) {
                        esq.addColumn(columnName);
                    });
                    esq.getEntity(recordId, function(result) {
                        if (callback) {
                            callback.call(this, result.entity);
                        }
                    }, this);
                },

                onCloseCardButtonClick: function() {
                    this.sandbox.publish("BackHistoryState");
                },


                onNextButtonClick: function() {
                    this.acceptProcessElement("NextButton");
                },

                completeProcess: function(validate) {
                    if (validate) {
                        var resultObject = {
                            success: this.validate()
                        };
                        if (!resultObject.success) {
                            resultObject.message = this.getValidationMessage();
                            this.validateResponse(resultObject);
                            return;
                        }
                    }
                    this.processParameters.push({
                        key: "Result",
                        value: ""
                    });
                    this.completeExecution(arguments);
                },

                onSaved: function() {
                    this.callParent(arguments);
                    this.completeProcess(true);
                },

				// Выставляет по умолчанию высокую степень влияния для ЛПР.
				setInfluence: function() {
					var role = this.get("Role");
					var influence = this.get("Influence");
					var infHigh = {value: "cb35dec5-59f5-465c-a985-99f980079853", primaryImageVisible: false,
						displayValue: "Высокая", customHtml: "Высокая", markerValue: "Высокая"};
					if (role && role.value === "82ecb430-8d6b-40ea-8c01-741fe82cdcbd" && !influence) {
						this.set("Influence", infHigh);
					}
				}

            },
            diff: /**SCHEMA_DIFF*/[
                {
                    "operation": "insert",
                    "parentName": "Header",
                    "propertyName": "items",
                    "name": "Opportunity",
                    "values": {
                        "layout": {"column": 0, "row": 0},
                        "controlConfig": {"enabled": false}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "Header",
                    "propertyName": "items",
                    "name": "Contact",
                    "values": {
                        "layout": {"column": 0, "row": 1}
                        //"controlConfig": {"enabled": false}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "Header",
                    "propertyName": "items",
                    "name": "Role",
                    "values": {
                        "layout": {"column": 0, "row": 2},
                        "controlConfig": {"enabled": false}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "Header",
                    "propertyName": "items",
                    "name": "IsMainContact",
                    "values": {
                        "layout": {"column": 0, "row": 3}
                        //"controlConfig": {"enabled": false}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "Header",
                    "propertyName": "items",
                    "name": "Influence",
                    "values": {
                        "layout": {"column": 0, "row": 4}
                        //"controlConfig": {"enabled": false}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "Header",
                    "propertyName": "items",
                    "name": "ContactLoyality",
                    "values": {
                        "layout": {"column": 0, "row": 5}
                        //"controlConfig": {"enabled": false}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "Header",
                    "propertyName": "items",
                    "name": "Description",
                    "values": {
                        "layout": {"column": 0, "row": 6}
                        //"controlConfig": {"enabled": false}
                    }
                },
                {
                    "operation": "insert",
                    "parentName": "Header",
                    "propertyName": "items",
                    "name": "TsResponsibilityField",
                    "values": {
                        "layout": {"column": 0, "row": 7}
                        //"controlConfig": {"enabled": false}
                    }
                }

            ]/**SCHEMA_DIFF*/
        };
    });
