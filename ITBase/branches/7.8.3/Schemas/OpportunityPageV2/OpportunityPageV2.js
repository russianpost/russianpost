define("OpportunityPageV2", ["ConfigurationConstants", "ProcessModuleUtilities", "ITJsConsts",
		"ITJsContractConstants", "OpportunityPageV2Resources"],
	function(ConfigurationConstants, ProcessModuleUtilities, ITJsConsts,
			ITJsContractConstants, resources) {
	return {
		entitySchemaName: "Opportunity",

		messages: {
			"OpportunityProductTotalsChanged": {
				mode: Terrasoft.MessageMode.BROADCAST,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},

			"ContinueLoadingDashboard": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			}
		},

		attributes: {
			"ShipmentVisibility": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			//Атрибут доступности поля "Сумма продажи"
			"AmountEnabled": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"Owner": {
				"lookupListConfig": {
					"columns": ["TsAccount", "TsTarifficatorRegion", "ITMacroregion"],
					"filters": [
						function() {
							var filterGroupMain = Ext.create("Terrasoft.FilterGroup"),
								filterGroup = Ext.create("Terrasoft.FilterGroup"),
								filterGroupByLeadType = Ext.create("Terrasoft.FilterGroup");
							filterGroup.logicalOperation = this.Terrasoft.LogicalOperatorType.OR;
							filterGroup.add("IsCurrentUser",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Name",
										Terrasoft.SysValue.CURRENT_USER_CONTACT.displayValue));
							filterGroup.add("IsOwner",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Owner",
										Terrasoft.SysValue.CURRENT_USER_CONTACT.value));
							if (this.get("isRoleOfManager")) {
								var arrayDependents = [];
								this.get("Dependents").each(function(item) {
									arrayDependents.push(item.get("RelatedAccount.Id"));
								});
								filterGroup.add("IsDependent",
										Terrasoft.createColumnInFilterWithParameters(
											"Account",
											arrayDependents));
							}
							var arrayContactsByLeadType = [];
							this.get("ContactsByLeadType").each(function(item) {
									arrayContactsByLeadType.push(item.get("ITContact.Id"));
								});
							filterGroupByLeadType.add("validLeadType",
									Terrasoft.createColumnInFilterWithParameters(
										"Id",
										arrayContactsByLeadType));
							filterGroupMain.logicalOperation = this.Terrasoft.LogicalOperatorType.AND;
							filterGroupMain.addItem(filterGroup);
							filterGroupMain.addItem(filterGroupByLeadType);
							return filterGroupMain;
						}
					]
				},
				dependencies: [
					{
						columns: ["Owner"],
						methodName: "onOwnerChanged"
					}
				]
			},

			//Атрибут поля "Маркорегион"
			"OwnerITMacroregion": {
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				dataValueType: this.Terrasoft.DataValueType.LOOKUP
			},

			"ContractingDuration": {
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				dataValueType: this.Terrasoft.DataValueType.TEXT,
				value: "длительность"
			},

			"IsNotDeliveryBillType": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				value: true,
				dependencies: [{
					columns: ["LeadType"],
					methodName: "onLeadTypeChange"
				}]
			},

			//Флаг, показывающий есть ли в продаже Доп услуги (true - есть)
			"EmptyServicesFlag": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				onChange: "onEmptyServicesFlagChange"
			},

			//Коллекция, в котором хранятся Id нижестоящих подразделений по отношению к контрагенту тек. пользователя
			"Dependents": {
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				dataValueType: this.Terrasoft.DataValueType.COLLECTION
			},

			//Атрибут, показывающий есть ли роль руководителя у текущего пользователя (true - есть)
			"isRoleOfManager": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: false
			},

			//Коллекция, в котором хранятся Id всех контактов, где присутствует соответсвующий тип потребности
			"ContactsByLeadType": {
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				dataValueType: this.Terrasoft.DataValueType.COLLECTION
			},

			//Получаем значения колонки Successful справочника OpportunityStage
			"Stage": {
				lookupListConfig: {
					"orders": [{columnPath: "Number"}],
					"columns": ["Successful"]
				}
	/*			dependencies: [
					{
						"columns": ["Stage"],
						"methodName": "onStageChanged"
					}
				]*/
			},

			"PaymentFormVisibility": {
				"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
				"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			//Флаг для валидации поля "Срок 100% погашения"
			"isAfterInitialized": {
				"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
				"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			"ITPaymentForm": {
				"onChange": "onITPaymentFormChanged"
			}
		},

		methods: {

			onEntityInitialized: function() {
				this.isCorDeliveryLeadType();
				this.updateAmountEnable();
				this.callParent(arguments);
				this.fillContractingDuration();
				this.onLeadTypeChange();
				this.subscribeSandboxEvents();
				var dashboardModuleId = this.getOpportunityDashboardModuleId();
				this.sandbox.publish("ContinueLoadingDashboard", null, [dashboardModuleId]);
				this.getRoleOfCurrentUser();
				this.getDependents();
				this.getContactsByLeadType();
				this.afterInitialized();
			},

			meetingsFilter: function() {
				var filterGroup = new this.Terrasoft.createFilterGroup();
				filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "42C74C49-58E6-DF11-971B-001D60E938C6"));
				// добавляете фильтр по типу активности
				filterGroup.add("OpportunityFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Opportunity", this.get("Id")));
				return filterGroup;
			},



			
			callsActivityFilter: function() {
				var filterGroup = new this.Terrasoft.createFilterGroup();
				filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "03DF85BF-6B19-4DEA-8463-D5D49B80BB28"));
				// добавляете фильтр по типу активности
				filterGroup.add("OpportunityFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Opportunity", this.get("Id")));
				return filterGroup;
			},



			
			tasksActivityFilter: function() {
				var filterGroup = new this.Terrasoft.createFilterGroup();
				filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "F51C4643-58E6-DF11-971B-001D60E938C6"));
				// добавляете фильтр по типу активности
				filterGroup.add("OpportunityFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Opportunity", this.get("Id")));
				return filterGroup;
			},

			getOpportunityDashboardModuleId: function() {
				return this.sandbox.id + "_module_ActionsDashboardModule";
			},

			//Флаги для валидации поля "Срок 100% погашения"
			afterInitialized: function() {
				this.onInitDate = this.get("ITFullPaymentDate");
				this.set("isAfterInitialized", true);
			},

			//Валидация полей формы оплаты
			setValidationConfig: function() {
				this.callParent(arguments);
				this.addColumnValidator("ITPrepaymentPercent", this.ITPrepaymentPercentageValidation);
				this.addColumnValidator("ITFullPaymentDate", this.ITFullPaymentDateValidation);
			},

			//Выполняет проверку валидации поля "% авансирования"
			ITPrepaymentPercentageValidation: function() {
				var percent = this.get("ITPrepaymentPercent"),
					visible = this.get("PaymentFormVisibility"),
					invalidMessage = "";
				if (visible) {
					if (percent == null || percent < 5 || percent > 100) {
						invalidMessage = resources.localizableStrings.PercentageValidationMessage;
					}
				}
				return {
					fullInvalidMessage: invalidMessage,
					invalidMessage: invalidMessage
				};
			},

			//Выполняет проверку валидации поля "Срок 100% погашения"
			ITFullPaymentDateValidation: function() {
				var nowDate = new Date(),
					isAfterInitialized = this.get("isAfterInitialized"),
					userDate = this.get("ITFullPaymentDate"),
					visible = this.get("PaymentFormVisibility"),
					invalidMessage = "";
				
				if (isAfterInitialized && visible) {
					if (userDate === null) {
						invalidMessage = resources.localizableStrings.PaymentDateValidationMessage;
					} else if (userDate !== this.onInitDate) {
						//Проверка даты на правило "Дата должна быть в будущем"
						switch (userDate.getFullYear() > nowDate.getFullYear()) {
							case true:
								break;
							default:
								switch (userDate.getFullYear() === nowDate.getFullYear()) {
									case true:
										switch (userDate.getMonth() > nowDate.getMonth()) {
											case true:
												break;
											default:
												switch (userDate.getMonth() === nowDate.getMonth()) {
													case true:
														switch (userDate.getDate() < nowDate.getDate()) {
															case true:
																invalidMessage = resources.localizableStrings.PaymentDateValidationMessage;
																break;
															default:
														}
														break;
													default:
														invalidMessage = resources.localizableStrings.PaymentDateValidationMessage;
												}
										}
										break;
									default:
										invalidMessage = resources.localizableStrings.PaymentDateValidationMessage;
								}
						}
					}
				}
				return {
					fullInvalidMessage: invalidMessage,
					invalidMessage: invalidMessage
				};
			},

			//Если тип доставка корреспонденции, то поля в ShipmentContainer видимы
			isCorDeliveryLeadType: function() {
				var leadType = this.get("LeadType"),
					corDelivery = ITJsConsts.LeadType.corDelivery.value;
				if (!leadType) {return; }
				switch (leadType.value === corDelivery) {
					case true:
						this.set("ShipmentVisibility", true);
						break;
					default:
						this.set("ShipmentVisibility", false);
				}
			},

			//При изменении стадии (писался под процесс Сопровождения доставки письменной корреспонденции)
	/*		onStageChanged: function() {
				var recordId = this.get("Id"),
					stage = this.get("Stage").value,
					endStage = ITJsConsts.OpportunityStage.finishedSuccess.value,
					leadType = this.get("LeadType").value,
					corDelivery = ITJsConsts.LeadType.corDelivery.value;
				if (leadType === corDelivery && stage === endStage) {
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: this.entitySchemaName
					});
					esq.addColumn("ITResponsible");
					esq.addColumn("ITShipmentDate");
					esq.getEntity(recordId, function(result) {
						if (!result.success) {return; }
						this.set("ITResponsible", result.entity.get("ITResponsible"));
						this.set("ITShipmentDate", result.entity.get("ITShipmentDate"));
						this.set("ShipmentVisibility", true);
					}, this);
				}
			},*/

			//Добавление пункта меню действия
			getActions: function() {
				var actionMenuItems = this.callParent(arguments);
				//actionMenuItems.get("Item4").values.Click.bindTo = "runOpportunityManagementProcessManual";
				actionMenuItems.get("Item4").values.Tag = "runOpportunityManagementProcessManual2";
				return actionMenuItems;
				
	/*			var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": "Обслуживание: Запустить процесс корп. продажи",
					"Tag": "runOpportunityManagementProcessManual",
					"Click": {bindTo: "runOpportunityManagementProcessManual"}
				}));
				return actionMenuItems;*/
				
			},

			//Запуск процесса корпоративной продажи
			runOpportunityManagementProcessManual2: function() {
				var id = this.get("Id");
				
				
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "VwSysProcessEntity"
				});
				
				
				var esqSchemaIdFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
					"SysProcess.SysSchema.Id", "22a2b25b-eb8e-4d05-bbc6-bf62af91ccab");
				esq.filters.add("esqSchemaIdFilter", esqSchemaIdFilter);
				
				
				var esqOpportunityFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
					"EntityId", id);
				esq.filters.add("esqOpportunityFilter", esqOpportunityFilter);
				
				
				esq.getEntityCollection(function(result) {
					if (result.success) {
						if (result.collection.collection.items.length === 0) {
							ProcessModuleUtilities.executeProcess({
								sysProcessName: "ITTsOpportunityManagement3", // указать правильное название процесса
								parameters: {
									CurrentOpportunity: id
								},
								callback: function() {
									this.hideBodyMask.call(this);
									this.sandbox.publish("EntityInitialized", this.onGetEntityInfo(), this.getEntityInitializedSubscribers());
								},
								scope: this
							});
						} else {
							this.showInformationDialog("Процесс продажи уже запущен!");
						}
					}
				}, this);
			},

			//Получение всех контактов, у которых есть соответствующий тип потребности
			getContactsByLeadType: function() {
				var esqContactLeadType = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "ITContactLeadType"
				});
				esqContactLeadType.addColumn("ITContact.Id");
				// Создание   экземпляра первого фильтра.  
				var esqFirstFilter = esqContactLeadType.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"ITLeadType.Id", this.get("LeadType").value);
				esqContactLeadType.filters.add("esqFirstFilter", esqFirstFilter);
				esqContactLeadType.getEntityCollection(function(result) {
					if (result.success) {
						this.set("ContactsByLeadType", result.collection);
					}
				}, this);
			},

			//Метод для определения подчиненных
			getDependents: function() {
				var esqRelationShip = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "VwAccountRelationship"
				});
				esqRelationShip.addColumn("RelatedAccount.Id");
				// Создание экземпляра первого фильтра.
				var esqFirstFilter = esqRelationShip.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"ReverseRelationType.Id", ConfigurationConstants.RelationType.Subsidiary);
				// Создание экземпляра второго фильтра.
				var esqSecondFilter = esqRelationShip.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"Account.Id", Terrasoft.SysValue.CURRENT_USER_ACCOUNT.value);
				esqRelationShip.filters.add("esqFirstFilter", esqFirstFilter);
				esqRelationShip.filters.add("esqSecondFilter", esqSecondFilter);
				// В данную коллекцию попадут объекты - результаты запроса, отфильтрованные по двум фильтрам.
				esqRelationShip.getEntityCollection(function(result) {
					if (result.success) {
						this.set("Dependents", result.collection);
					}
				}, this);
			},

			//Если у текущего пользователя есть роль руководителя, то "isRoleOfManager" - true
			getRoleOfCurrentUser: function() {
				var esqRoleOfContact = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "ITVwRoleOfContact"
				});
				// Создание экземпляра первого фильтра.
				var esqFirstFilter = esqRoleOfContact.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"ITContact.Id", Terrasoft.SysValue.CURRENT_USER_CONTACT.value);
				esqRoleOfContact.filters.add("esqFirstFilter", esqFirstFilter);
				esqRoleOfContact.getEntityCollection(function(result) {
					if (result.success && result.collection.collection.items.length) {
						this.set("isRoleOfManager", true);
					}
				}, this);
			},

			//Метод для подписки на сообщения
			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.sandbox.subscribe("OpportunityProductTotalsChanged", this.getAmount, this, []);
				
			},

			//Срабатывает от сообщения и обновляет отображаемое значение Amount и AmoundNDS из БД
			getAmount: function() {
				var recordId = this.get("Id");
				var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Opportunity"});
				esq.addColumn("Amount", "onAmount");
				esq.addColumn("ITAmountNDS", "ITAmountNDS");
				esq.getEntity(recordId, function(result) {
					this.set("Amount", result.entity.get("onAmount"));
					this.set("ITAmountNDS", result.entity.get("ITAmountNDS"));
					this.save({
						scope: this,
						isSilent: true
					});
					this.updateAmountEnable();
				}, this);
			},

			//При инициализации страницы устанавливает доступность "Amount"
			updateAmountEnable: function() {
				var recordId = this.get("Id");
				var count = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "OpportunityProductInterest"});
				count.addColumn("ITAmount", "ITAmount");
				count.filters.add("filterById", count.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
					"Opportunity", recordId));
				count.getEntityCollection(function(result) {
					if (result.collection.getCount() > 0) {
	//					this.showInformationDialog(result.collection.getCount());
						this.set("AmountEnabled", false);
						this.set("EmptyServicesFlag", false);
					} else {
						this.set("AmountEnabled", true);
						this.set("EmptyServicesFlag", true);
						this.updateAmountNDS();
					}
				}, this);
			},

			//Если флаг true, то запускает метод расчета НДС
			onEmptyServicesFlagChange: function() {
				var value = this.get("EmptyServicesFlag");
				if (value) {
					this.updateAmountNDS();
				}
			},

			//Расчитывает и записывает Сумму с НДС
			updateAmountNDS: function() {
				var amount = this.get("Amount"), amountNDS;
				if (this.Ext.isEmpty(amount) || amount <= 0) {
					return;
				} else {
					amountNDS = amount * 1.18;
					this.set("ITAmountNDS", amountNDS);
				}
			},

			//Срабатывает при смене ответственного
			onOwnerChanged: function() {
				var newOwner = this.get("Owner");
				if (Ext.isEmpty(newOwner)) {
					this.set("OwnerTsAccount", null);
					//this.set("OwnerTsTarifficatorRegion", null);
					this.set("OwnerITMacroregion", null);
					return;
				}
				this.set("OwnerTsAccount", newOwner.TsAccount);
				//this.set("OwnerTsTarifficatorRegion", newOwner.TsTarifficatorRegion);
				this.set("OwnerITMacroregion", newOwner.ITMacroregion);
			},

			entityColumnChanged: function(columnName, columnValue) {
				this.callParent(arguments);
				if ((columnName === "Amount") && (columnValue !== null) &&
				(columnValue !== 0) && (columnValue > 0)) {
					this.updateAmountEnable();
					//Создаем экземпляр UpdateQuery
					var update = this.Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "Opportunity"});
					//Добавляем фильтра по Id текущей продажи
					update.filters.add("IdFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Id", this.get("Id")));
					//Записываем текущее значение колонки Amount в соответствующую ячейку в БД
					update.setParameterValue("Amount", this.get("Amount"), this.Terrasoft.DataValueType.MONEY);
					update.execute();
				}
			},

			fillContractingDuration: function() {
				var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", { rootSchemaName: "ITVwOpportunitySummary" });
				esq.addColumn("ITContractingDuration", "ITContractingDuration");
				esq.getEntity(this.get("Id"), function(result) {
					if (result.success && result.entity) {
						this.set("ContractingDuration", result.entity.values.ITContractingDuration.toString());
					}
					else {
						this.set("ContractingDuration", "");
					}
				}, this);
			},

			//При смене Типа потребности
			onLeadTypeChange: function() {
				var type = this.get("LeadType");
				switch (this.Ext.isEmpty(type)) {
					case true:
						this.set("IsNotDeliveryBillType", false);
						break;
					default:
						switch (type.value === ITJsConsts.LeadType.invoiceDelivery.value) {
							case false:
								this.set("IsNotDeliveryBillType", false);
								break;
							default:
								this.set("IsNotDeliveryBillType", true);
						}
				}
			},

			isMetricsContainerVisible: function() {
				return false;
			},

			//Усановка видимости полей формы оплаты и их обнуление
			onITPaymentFormChanged: function() {
				var paymentForm = this.get("ITPaymentForm");
				if (paymentForm && paymentForm.value !== ITJsContractConstants.PaymentForm.PostPay) {
					this.set("PaymentFormVisibility", false);
					this.set("ITPrepaymentPercent", null);
					this.set("ITFullPaymentDate", null);
				} else {this.set("PaymentFormVisibility", true); }
			},

			//Определение видимости группы полей "Информация о завершении продажи"
			getCompleteDataGroupFieldVisible: function() {
				var stageEnd = this.get("Stage");
				return (stageEnd) && stageEnd.End && (!stageEnd.Successful);
			},
			init: function() {
				if (this.sandbox.moduleName === "ProcessCardModuleV2") {
					var pageId = this.values.PrimaryColumnValue;
					var requestUrl = "CardModuleV2/OpportunityPageV2/edit/" + pageId;
					this.sandbox.publish("PushHistoryState", {
						hash: requestUrl
					});
				}
				else {
					this.callParent(arguments);
				}
			},
			onCloseCardButtonClick: function() {
				if (this.tryShowNextPrcElCard()) {
					return;
				}
				var isLastProcessElement = this.get("IsProcessMode") && !this.get("NextPrcElReady");
				if ((this.get("IsInChain") || this.get("IsSeparateMode")) || isLastProcessElement) {
					if (!this.destroyed) {
						var requestUrl = "SectionModuleV2/OpportunitySectionV2/";
						this.sandbox.publish("PushHistoryState", {
							hash: requestUrl
						});
					}
					return;
				}
				this.sandbox.publish("CloseCard", null, [this.sandbox.id]);
			}
		},

		details: /**SCHEMA_DETAILS*/{
			"OpportunityProductDetailV2": {
				"schemaName": "OpportunityProductDetailV2",
				"entitySchemaName": "OpportunityProductInterest",
				"filter": {
					"detailColumn": "Opportunity",
					"masterColumn": "Id"
				}
			},
			"ContractDetailV2": {
				"schemaName": "ContractDetailV2",
				"entitySchemaName": "Contract",
				"filter": {
					"detailColumn": "ITOpportunityInContract",
					"masterColumn": "Id"
				},
				defaultValues: {
					Account: { masterColumn: "Account"},
					ITOpportunityOwner: { masterColumn: "Owner"},
					TsLeadType: { masterColumn: "LeadType"},
					Amount: { masterColumn: "Amount"},
					ITAmountNDS: { masterColumn: "ITAmountNDS"},
					ITPaymentForm: {masterColumn: "ITPaymentForm"},
					ITPrepaymentPercentage: {masterColumn: "ITPrepaymentPercent"},
					ITFullPaymentDate: {masterColumn: "ITFullPaymentDate"}
				}
			},
			"ITTownCategoryDetail": {
				"schemaName": "ITOppTownCategoryDetail",
				"entitySchemaName": "ITTownCategoryInOpp",
				"filter": {
					"detailColumn": "ITOpportunity",
					"masterColumn": "Id"
				}
			},
			"ITMeetingsDetail": {
				"schemaName": "ITMeetingsDetailV2",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				},
				filterMethod: "meetingsFilter"
			},
			"ITTasksDetailV2": {
				"schemaName": "ITTasksDetailV2",
				//"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				},
				filterMethod: "tasksActivityFilter"
			},
			"ITCallsDetail": {
				"schemaName": "ITCallsDetailV2",
				//"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				},
				filterMethod: "callsActivityFilter"
			}
		},/**SCHEMA_DETAILS*/

		diff: /**SCHEMA_DIFF*/[

	/*		{
				"operation": "insert",
				"name": "startProcessOpportunity",
				"parentName": "ActionDashboardContainer",
				"propertyName": "items",
				"index": 7,
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.RED,
					"Align": this.Terrasoft.Align.RIGHT,
					"caption": "Запустить корпоративную продажу",
					"click": {"bindTo": "runOpportunityManagementProcessManual"},
	//				"enabled": {"bindTo": "canEntityBeOperated"},
					"classes": {
						"textClass": ["cloudintegration-refresh-button"]
					}
				}
			},*/

			//Удаление кнопки Добавить заказ
			{
				"operation": "remove",
				"name": "CreateOrderFromOpportunityButton"
			},

			//Сделать поля радио группы недоступными для редактирования
			{
				"operation": "merge",
				"name": "FirstOpportunity",
				"values": {
					"enabled": false
				}
			},

			{
				"operation": "merge",
				"name": "SecondOpportunity",
				"values": {
					"enabled": false
				}
			},
			//Удаление поля "Регион" из вкладки "Данные о продаже"
			{
				"operation": "remove",
				"name": "TsTarifficatorRegion"
			},
			{
				"operation": "insert",
				"name": "ITBankBookCount",
				"values": {
					"visible": {
						"bindTo": "IsNotDeliveryBillType"
					},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6
					},
					"bindTo": "ITBankBookCount"
				},
				"parentName": "OpportunityPageGeneralBlock",
				"propertyName": "items"
			},
			{
				"operation": "merge",
				"name": "MetricsContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "OpportunityLeadType",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "move",
				"name": "OpportunityLeadType",
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "remove",
				"name": "OwnerTsTarifficatorRegion"
			},
			{
				"operation": "merge",
				"name": "OwnerTsAccount",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "OpportunityDueDate",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5
					}
				}
			},
			{
				"operation": "merge",
				"name": "LablelMetricsContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6
					}
				}
			},
			{
				"operation": "move",
				"name": "LablelMetricsContainer",
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "OwnerITMacroregion",
				"values": {
					"caption": "Макрорегион",
					"bindTo": "OwnerITMacroregion",
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3
					},
					"enabled": false,
					"contentType": 3
				},
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "merge",
				"name": "OpportunityTitle",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					}
				}
			},
			{
				"operation": "move",
				"name": "OpportunityTitle",
				"parentName": "OpportunityPageGeneralBlock",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "Amount",
				"values": {
					"enabled": {bindTo: "AmountEnabled"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3
					},
					"tip": {
						"content": {
							"bindTo": "Resources.Strings.AmountTip"
						}
					}
				}
			},

			{
				"operation": "insert",
				"name": "ITAmountNDS",
				"parentName": "TermsOfPaymentBlock1",
				"values": {
					"enabled": false,
					"layout": {"colSpan": 12, "rowSpan": 1, "column": 12, "row": 0
					},
					"bindTo": "ITAmountNDS"
				},
				"propertyName": "items"
			},

			{
				"operation": "merge",
				"name": "ResponsibleDepartment",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "Probability",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "Owner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5
					}
				}
			},
			{
				"operation": "merge",
				"name": "Category",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5
					}
				}
			},
			{
				"operation": "move",
				"name": "Category",
				"parentName": "OpportunityPageGeneralBlock",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "merge",
				"name": "Source",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6
					}
				}
			},
			{
				"operation": "merge",
				"name": "CreatedOn",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6
					}
				}
			},
			{
				"operation": "merge",
				"name": "Partner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 7
					}
				}
			},
			{
				"operation": "merge",
				"name": "CloseReason",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 7
					}
				}
			},
			{
				"operation": "insert",
				"name": "ITTownCategoryDetail",
				"values": {
					"visible": {
						"bindTo": "IsNotDeliveryBillType"
					},
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "merge",
				"name": "Description",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "insert",
				"name": "ContractDetailV2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "merge",
				"name": "TsOurStrongSides",
				"values": {
					"layout": {
						"colSpan": 11,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "OpportunityTacticAndCompetitorBlock"
					}
				}
			},
			{
				"operation": "move",
				"name": "TsOurStrongSides",
				"parentName": "OpportunityTacticAndCompetitorBlock",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "merge",
				"name": "CheckDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2
					}
				}
			},
			{
				"operation": "insert",
				"name": "OpportunityProductDetailV2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ProductsTab",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"parentName": "HistoryTab",
				"name": "OpportunityCompleteDataGroup",
				"propertyName": "items",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.CONTROL_GROUP,
					"caption": {"bindTo": "Resources.Strings.OpportunityCompleteDataGroupCaption"},
					"visible": {"bindTo": "getCompleteDataGroupFieldVisible"},
					"controlConfig": {
						"collapsed": false
					},
					"items": []
				},
				"index": 0
			},
			{
				"operation": "insert",
				"parentName": "OpportunityCompleteDataGroup",
				"propertyName": "items",
				"name": "OpportunityCompleteDataBlock",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.GRID_LAYOUT,
					"items": []
				}
			},
			{
				"operation": "insert",
				"name": "ITLooseReason",
				"values": {
					"caption": "Причина проигрыша",
					"bindTo": "ITLooseReason",
					"contentType": Terrasoft.ContentType.LONG_TEXT,
					"layout": {
						"column": 0,
						"row": 0,
						"colSpan": 24,
						"rowSpan": 1,
						"layoutName": "OpportunityCompleteDataBlock"
					}
				},
				"parentName": "OpportunityCompleteDataBlock",
				"propertyName": "items"
			},
			{
				"operation": "insert",
				"name": "ITNextAction",
				"values": {
					"caption": "Возможные дальнейшие действия",
					"bindTo": "ITNextAction",
					"contentType": Terrasoft.ContentType.LONG_TEXT,
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1,
						"layoutName": "OpportunityCompleteDataBlock"
					}
				},
				"parentName": "OpportunityCompleteDataBlock",
				"propertyName": "items"
			},

			//Контейнер для полей "Ответственный поддержки" и "Дата первой отгрузки"
			{
				"operation": "insert",
				"name": "ShipmentContainer",
				"values": {
					"itemType": 0,
					"items": [],
					"layout": {
						"rowSpan": 2
					}
				},
				"parentName": "HistoryTab",
				"propertyName": "items",
				"index": 3
			},

			{
				"operation": "insert",
				"name": "ITResponsible",
				"values": {
					"bindTo": "ITResponsible",
					"enabled": false,
					"visible": {bindTo: "ShipmentVisibility"},
					"layout": {"colSpan": 12, "column": 0, "row": 0}
				},
				"parentName": "ShipmentContainer",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITShipmentDate",
				"values": {
					"bindTo": "ITShipmentDate",
					"enabled": false,
					"visible": {bindTo: "ShipmentVisibility"},
					"layout": {"colSpan": 12, "column": 12, "row": 0}
				},
				"parentName": "ShipmentContainer",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "StageAndDurationContainer",
				"values": {
					"itemType": 0,
					"items": [],
					"layout": {
						"rowSpan": 2
					}
				},
				"parentName": "HistoryTab",
				"propertyName": "items",
				"index": 1
			},

	/*		{
				"operation": "merge",
				"name": "StageInOpportunity",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6
					}
				}
			},
			{
				"operation": "move",
				"name": "StageInOpportunity",
				"parentName": "StageAndDurationContainer",
				"propertyName": "items",
				"index": 2
			},*/
			{
				"operation": "insert",
				"name": "ContractingDurationField",
				"values": {
					"caption": "Длительность контрактации, дней",
					"bindTo": "ContractingDuration",
					"enabled": false,
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				},
				"parentName": "StageAndDurationContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "remove",
				"name": "Type"
			},
			{
				"operation": "remove",
				"name": "OpportunityProduct"
			},
			{
				"operation": "remove",
				"name": "Order"
			},
			{
				"operation": "move",
				"name": "ESNTab",
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 7
			},
			{
				"operation": "move",
				"name": "OpportunityContacts",
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "move",
				"name": "TsTarifficatorRegion",
				"parentName": "OpportunityPageGeneralBlock",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "move",
				"name": "TsOurWeakSides",
				"parentName": "OpportunityTacticAndCompetitorBlock",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "remove",
				"name": "Project"
			},

			{
				"operation": "merge",
				"name": "Activities",
				"values": {
					"layout": {
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "Calls",
				"values": {
					"layout": {
						"row": 2
					}
				}
			},
			{
				"operation": "merge",
				"name": "EmailDetailV2",
				"values": {
					"layout": {
						"row": 3
					}
				}
			},
			{
				"operation": "merge",
				"name": "InvoiceDetailV2",
				"values": {
					"layout": {
						"row": 4
					}
				}
			},
			{
				"operation": "merge",
				"name": "DocumentDetailV2",
				"values": {
					"layout": {
						"row": 5
					}
				}
			},
			{
				"operation": "move",
				"name": "StageInOpportunity",
				"parentName": "HistoryTab",
				"propertyName": "items"
		//		"index": 2
			},
			{
				"operation": "merge",
				"name": "StageInOpportunity",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6
					}
				}
			},
			{
				"operation": "insert",
				"parentName": "GeneralInfoTab",
				"name": "TermsOfPaymentGroup",
				"propertyName": "items",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.CONTROL_GROUP,
					"caption": {"bindTo": "Resources.Strings.TermsOfPaymentGroupCaption"},
					"controlConfig": {"collapsed": false},
					"items": []
				},
				"index": 2
			},
			{
				"operation": "insert",
				"name": "TermsOfPaymentBlock1",
				"parentName": "TermsOfPaymentGroup",
				"propertyName": "items",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.GRID_LAYOUT,
					"items": []
				}
			},
			{
				"operation": "insert",
				"name": "TermsOfPaymentBlock2",
				"parentName": "TermsOfPaymentGroup",
				"propertyName": "items",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.GRID_LAYOUT,
					"visible": {"bindTo": "PaymentFormVisibility"},
					"items": []
				}
			},
			{
				"operation": "move",
				"name": "Amount",
				"parentName": "TermsOfPaymentBlock1",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "Amount",
				"parentName": "TermsOfPaymentBlock1",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "insert",
				"name": "ITPaymentForm",
				"values": {
					"layout": {"colSpan": 12, "rowSpan": 1, "column": 0, "row": 1},
					"bindTo": "ITPaymentForm",
					"contentType": 3
				},
				"parentName": "TermsOfPaymentBlock1",
				"propertyName": "items"
			},
			{
				"operation": "insert",
				"name": "ITPrepaymentPercent",
				"values": {
					"layout": {"colSpan": 12, "rowSpan": 1, "column": 0, "row": 0},
					"bindTo": "ITPrepaymentPercent",
					"visible": true,
					"isRequired": true
				},
				"parentName": "TermsOfPaymentBlock2",
				"propertyName": "items"
			},
			{
				"operation": "insert",
				"name": "ITFullPaymentDate",
				"values": {
					"layout": {"colSpan": 12, "rowSpan": 1, "column": 12, "row": 0},
					"bindTo": "ITFullPaymentDate",
					"visible": true,
					"isRequired": true
				},
				"parentName": "TermsOfPaymentBlock2",
				"propertyName": "items"
			},
			// скрытие детали "Счета".
			{
				"operation": "merge",
				"name": "Invoice",
				"values": {
					"visible": false
				}
			},
			//Замена Детали Активности
			{
				"operation": "remove",
				"name": "Document"
			},
			{
				"operation": "remove",
				"name": "Activities"
			},
			{
				"operation": "remove",
				"name": "Calls"
			},
			{
				"operation": "insert",
				"name": "ITMeetingsDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTab",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "ITTasksDetailV2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTab",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "ITCallsDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTab",
				"propertyName": "items",
				"index": 12
			}
			/*{
				"operation": "insert",
				"parentName": "SaveButton",
				"name": "SaveButtonHyperlink",
				"values": {
					"itemType": Terrasoft.ViewItemType.HYPERLINK,
					"caption": "Ссылка",
					"tag": {
						//Идентификатор текущего пользователя.
						"recordId": "64A6B39C-7F59-4B73-AF39-0001B0608756",
						// Название схемы объекта.
						"referenceSchemaName": "Account"
					}
				}
			}*/
		]/**SCHEMA_DIFF*/
	};
});