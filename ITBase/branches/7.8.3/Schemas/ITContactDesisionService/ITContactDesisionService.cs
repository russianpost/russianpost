using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using Terrasoft.Core;
using System.Web;
using Terrasoft.Configuration.ITBase;

namespace Terrasoft.Configuration
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ITContactDesisionService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
        public string GetDecisionRoleId(string contactId)
        {
            string result = "";
            try
            {
                var _userConnection = (UserConnection)HttpContext.Current.Session["UserConnection"];
                ITDelegateActivityAction itDAA = new ITDelegateActivityAction();
                result = itDAA.GetContactDecisionRoleId(contactId, _userConnection);
            }
            catch(Exception ex)
            {
            }
            return result;
        }
    }
}
