namespace Terrasoft.Configuration
{
    using System.CodeDom.Compiler;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.ServiceModel.Activation;
    using System.Runtime.Serialization;
    using System.Web;
    using Terrasoft.Common;
    using Terrasoft.Core;
    using Terrasoft.Core.DB;
    using Terrasoft.Core.Entities;
    using Terrasoft.Core.Store;
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Newtonsoft.Json.Linq;

    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ITDuplicationSearch
    {
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
        public string StartSearchDuplicationProcedure(string accountName, string primaryContact, string phone, string web, 
        string ownership, string inn, int kpp, string currentContactId)
        {

            try
            {
            	var userConnection = (UserConnection)HttpContext.Current.Session["UserConnection"];
            	
                var storedProcedure = new StoredProcedure(userConnection, "tsp_FindNewAccountDuplicate");
                storedProcedure.WithParameter("accountName", accountName);
                storedProcedure.WithParameter("primaryContact", primaryContact);
                storedProcedure.WithParameter("phone", phone);
                storedProcedure.WithParameter("web", web);
                storedProcedure.WithParameter("ownership", ownership);
                storedProcedure.WithParameter("inn", inn);
                storedProcedure.WithParameter("kpp", kpp);
                storedProcedure.WithParameter("currentContactId", currentContactId);
				storedProcedure.PackageName = userConnection.DBEngine.SystemPackageName;
				storedProcedure.Execute();
            }
            
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Good";
        }
        
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
        public string ClearTableDuplicationProcedure(string currentContactId)
        {

            try
            {
            	var userConnection = (UserConnection)HttpContext.Current.Session["UserConnection"];
            	
                var storedProcedure = new StoredProcedure(userConnection, "tsp_FindNewAccountDuplicateDelete");
                storedProcedure.WithParameter("currentContactId", currentContactId);
				storedProcedure.PackageName = userConnection.DBEngine.SystemPackageName;
				storedProcedure.Execute();
            }
            
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Good";
        }
        
    }
    
}