define("LeadSectionV2", ["LeadConfigurationConst", "ITLeadManagementUtilities"], function(LeadConfigurationConst) {
	return {
		entitySchemaName: "Lead",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: [
			{
				"operation": "merge",
				"name": "DataGridActiveRowQualificationProcessAction",
				"values": {
					"visible": false
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {

			getGridDataColumns: function() {
				var baseGridDataColumns = this.callParent(arguments);
				var gridDataColumns = {
					"QualifyStatus": {path: "QualifyStatus"},
					"QualifyStatus.IsFinal": {path: "QualifyStatus.IsFinal"}
				};
				return Ext.apply(baseGridDataColumns, gridDataColumns);
			},


			executeCardMethod: function(config) {
				this.sandbox.publish("OnCardAction", config, [this.getCardModuleSandboxId()]);
			},

			onDisqualifyModalBoxButtonClick: function() {
				this.executeCardMethod({
					methodName: "onDisqualifyModalBoxButtonClick"
				});
			},

			DisqalificationButtonVisible: function() {
				return false;
			},

			isDisqalifyFinalStatus: function() {
				var activeRow = this.get("ActiveRow");
				if (activeRow) {
					var status = this.get("GridData").get(activeRow).get("QualifyStatus");
					var isFinal = this.get("GridData").get(activeRow).get("QualifyStatus.IsFinal");
					return (activeRow && status && status.value !==
						LeadConfigurationConst.LeadConst.QualifyStatus.Disqualified && !isFinal);
				}
				return false;
			}

		}
	};
});
