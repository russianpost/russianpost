define("ITActivityDashboardItemViewModel", ["ITActivityDashboardItemViewModelResources", "terrasoft", "ProcessHelper", "MaskHelper", "ServiceHelper", "ConfigurationConstants", "ActivityDashboardItemViewModel", "CustomProcessPageV2Utilities"],
    function(resources, Terrasoft, ProcessHelper, MaskHelper, ServiceHelper, ConfigurationConstants) {
        Ext.define("Terrasoft.configuration.ITActivityDashboardItemViewModel", {
            extend: "Terrasoft.ActivityDashboardItemViewModel",
            alternateClassName: "Terrasoft.ITActivityDashboardItemViewModel",

            mixins: {
                CustomProcessPageV2Utilities: "Terrasoft.CustomProcessPageV2Utilities"
            },

            columns: {

                "DetailedResultVisible": {
                    dataValueType: Terrasoft.DataValueType.BOOLEAN,
                    value: false
                },

                "DetailedResultValue": {
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                    name: "DetailedResultValue"
                }
            },

            // @override
            initializeProperties: function() {
                this.callParent(arguments);
                this.initFirstButton();
                this.initSecondButton();
                this.initThirdButton();
                this.initDetailedResultVisible();
            },

            init: function() {
                this.callParent(arguments);
                this.setDashboardMask(true);
            },

            // @override
            onGetExecutionData: function(request, success, response) {
                if (success) {
                    var executionData = Ext.decode(Ext.decode(response.responseText));
                    this.set("ExecutionData", executionData);
                    this.parseExecutionData(executionData);
                }
            },

            initDetailedResultVisible: function() {
                this.set("DetailedResultVisible", false);
            },

            // @override
            initExecuteButtonVisibility: function() {
                this.set("ExecuteButtonVisible", false);
            },

            // @override
            initExecuteButtonCaption: function() {
                this.set("ExecuteButtonCaption", resources.localizableStrings.NextButtonCaption);
            },

            initFirstButton: function() {
                this.set("FirstButtonVisible", false);
                this.set("FirstButtonCaption", resources.localizableStrings.FirstButtonCaption);
                this.set("FirstButtonStyle", Terrasoft.controls.ButtonEnums.style.BLUE);
            },

            initSecondButton: function() {
                this.set("SecondButtonVisible", false);
                this.set("SecondButtonCaption", resources.localizableStrings.SecondButtonCaption);
                this.set("SecondButtonStyle", Terrasoft.controls.ButtonEnums.style.BLUE);
            },

            initThirdButton: function() {
                this.set("ThirdButtonVisible", false);
                this.set("ThirdButtonCaption", resources.localizableStrings.ThirdButtonCaption);
                this.set("ThirdButtonStyle", Terrasoft.controls.ButtonEnums.style.BLUE);
            },

            onFirstButtonClick: function() {
                this.onProcessButtonClick(0);
            },

            onSecondButtonClick: function() {
                this.onProcessButtonClick(1);
            },

            onThirdButtonClick: function() {
                this.onProcessButtonClick(2);
            },

            onExecuteButtonClick: function() {
                var executionData = this.get("ExecutionData");
                if (executionData) {
                    this.updateActivity(executionData.activityRecordId, function(result) {
                        this.onCompleteExecution();
                    });
                }
            },

            setDashboardMask: function(value) {
                if (value) {
                    MaskHelper.ShowBodyMask({
                        selector: ".action-dashboard-container"
                    });
                }
                else MaskHelper.HideBodyMask({
                    selector: ".action-dashboard-container"
                });
            },

            updateActivity: function(activityId, callback) {
                var detailedResult = this.get("DetailedResultValue");
                this.setDashboardMask(true);
                var recordId = this.get("Id");
                var update = Ext.create("Terrasoft.UpdateQuery", {
                    rootSchemaName: "Activity"
                });
                var filters = update.filters;
                var activityIdFilter = update.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
                    "Id", activityId);
                filters.add("activityIdFilter", activityIdFilter);
                update.setParameterValue("Status", ConfigurationConstants.Activity.Status.Done, Terrasoft.DataValueType.GUID);
                update.setParameterValue("Result", ConfigurationConstants.Activity.ActivityResult.Completed, Terrasoft.DataValueType.GUID);
                if (detailedResult) update.setParameterValue("DetailedResult", detailedResult, Terrasoft.DataValueType.TEXT);
                update.execute(function(result) {
                    callback.call(this, result);
                }, this);
            },

            saveProcessParameters: function() {
                var processData = this.get("ProcessData");
                var parameters = this.processParameters = [];
                if (Ext.isEmpty(processData)) {
                    return;
                }
                Terrasoft.each(processData.parameters, function(parameterValue, name) {
                    var value = Terrasoft.deepClone(this.get(name));
                    if (Ext.isDate(value)) {
                        value = Terrasoft.encodeDate(value);
                    } else if (value) {
                        value = ProcessHelper.getServerValueByDataValueType(value, value.dataValueType);
                    }
                    parameters.push({
                        key: name,
                        value: (!Ext.isEmpty(value) && !Ext.isEmpty(value.value)) ? value.value : value
                    });
                }, this);
            },

            onCompleteExecution: function() {
                var sandbox = this.sandbox;
                sandbox.publish("ReloadCard", null, [sandbox.id]);
                this.setDashboardMask(false);
            },


            completeExecution: function(code) {
                this.setDashboardMask(true);
                if (!Ext.isEmpty(code)) {
                    this.processParameters.push({
                        key: "PressedButtonCode",
                        value: code
                    });
                }
                var dataSend = {
                    procElUId: this.get("ProcessData").procElUId,
                    parameters: this.processParameters
                };
                var config = {
                    serviceName: "ProcessEngineService",
                    methodName: "CompleteExecution",
                    data: dataSend,
                    timeout: 99999,
                    callback: this.onCompleteExecution,
                    scope: this
                };
                ServiceHelper.callService(config);
            },

            onProcessButtonClick: function(buttonNum) {
                var processData = this.get("ExecutionData") || {};
                var resultDecisions = [];
                resultDecisions.push(processData.decisionOptions[buttonNum].Id);
                processData.parameters = processData.parameters || {};
                resultDecisions = Ext.encode(resultDecisions);
                this.set("ResultDecisions", resultDecisions);
                Ext.apply(processData.parameters, {
                    ResultDecisions: resultDecisions
                });
                this.set("ProcessData", processData);
                this.saveProcessParameters();
                this.completeExecution();
            },

            parseExecutionData: function(executionData) {
                if (executionData.decisionOptions) {
                    if (executionData.decisionOptions.length === 2) {
                        this.set("FirstButtonVisible", true);
                        this.set("SecondButtonVisible", true);
                        this.set("FirstButtonCaption", executionData.decisionOptions[0].Caption);
                        this.set("SecondButtonCaption", executionData.decisionOptions[1].Caption);
                        if (executionData.decisionOptions[0].Caption.toLowerCase() === "да" && executionData.decisionOptions[1].Caption.toLowerCase() === "нет") {
                            this.set("FirstButtonStyle", Terrasoft.controls.ButtonEnums.style.GREEN);
                            this.set("SecondButtonStyle", Terrasoft.controls.ButtonEnums.style.RED);
                        }
                    }
                    else if (executionData.decisionOptions.length === 3) {
                        this.set("FirstButtonVisible", true);
                        this.set("SecondButtonVisible", true);
                        this.set("ThirdButtonVisible", true);
                        this.set("FirstButtonCaption", executionData.decisionOptions[0].Caption);
                        this.set("SecondButtonCaption", executionData.decisionOptions[1].Caption);
                        this.set("ThirdButtonCaption", executionData.decisionOptions[2].Caption);
                    }
                    this.set("DetailedResultVisible", false);
                }
                else {
                    this.set("DetailedResultVisible", true);
                    this.set("ExecuteButtonVisible", true);
                }
                this.setDashboardMask(false);
            },


            onCancelButtonClick: function() {
                //this.callParent(arguments);
            }

        });
    });