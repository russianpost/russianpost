define("ITJsConsts", [], function() {
	//Создана на всякий случай
	var communicationConsts = {
		Web: "Сайт"
	};

	var opportunityStage = {
		//Завершена с победой типа "Доставка письменной корреспонденции"
		finishedSuccess: {
			value: "60d5310c-5be6-df11-971b-001d60e938c6",
			displayValue: "Завершена с победой"
		}
	};

	//Типы потребности
	var leadType = {
		corDelivery: {
			value: "013830c6-a34a-4ab4-832b-00f69e6b3f5d",
			displayValue: "Доставка письменной корреспонденции"
		},
		invoiceDelivery: {
			value: "2f2a26a4-fddb-44b7-b5c1-aaf2cbd1e633",
			displayValue: "Доставка счетов"
		},
		directMarketing: {
			value: "3998457d-79e0-4af4-8377-95979e87f300",
			displayValue: "Блок директ-маркетинга"
		},
		subscriptionServices: {
			value: "7ce973b9-7a99-46da-b14e-430d9c7ee66b",
			displayValue: "Блок подписных сервисов"
		},
		printingServices: {
			value: "68e280d1-186c-4cb9-9429-e066d201f9b6",
			displayValue: "Блок печатных сервисов"
		},
		retail: {
			value: "ffed6c57-6ddc-47c0-9904-a01d31e13b5b",
			displayValue: "Блок розничной торговли"
		},
		parcel: {
			value: "9513105a-63c4-4952-96bd-3700d05c2f23",
			displayValue: "Посылочный блок"
		},
		financial: {
			value: "e98c7297-50a5-4c2b-952f-7a06df400f45",
			displayValue: "Финансовый блок"
		}
	};

	var leadStages = {
		//Посылочный блок
		parcelBlock: {
			//Презентация
			presentation: "325f0619-0ee0-df11-971b-001d60e938c6",
			//Согласование КП
			agreement: "0d4ce7ea-ac1a-49b0-bc4b-afb40fa52e12",
			//Подписание договора
			signing: "8e27f01c-808e-4845-96b5-ac7685908b71",
			//Сопровождение
			escort: "4a9f18a0-8ee1-40e3-bc1e-e138e375e731",
			//Старт отправок
			shipments: "0eebbb4a-0ce4-41ed-b948-d9776c677218",
			//Завершена с победой
			victory: "60d5310c-5be6-df11-971b-001d60e938c6",
			//Завершена с проигрышем
			defeat: "a9aafdfe-2242-4f42-8cd5-2ae3b9556d79",
			//Отложена
			hold: "5e9c9567-d6a5-4e56-92ee-a6d89f8cdc4a"
		}
	};

	//Значения справочника "Организационная структура"
	var orgStructure = {
		AUP: "7905DBB3-16A0-4B72-B4C0-CA70F43EA80C",
		MRC: "443D47E6-C610-4D45-9155-F16EA6324F26",
		UFPS: "B588E227-103A-42F5-82FF-583698F4CDEB"
	};

	//Значения справочника "Функциональные роли"
	var funcStructure = {
		HeadOfSalesDepartment: "3d484921-dfe8-4baf-ae0c-b51d627d2a1f"
	};

	//displayValue могут отличаться от значений в БД
	var contactType = {
		Contact: {
			value: "806732ee-f36b-1410-a883-16d83cab0980",
			displayValue: "Контактное лицо"
		},
		Client: {
			value: "00783ef6-f36b-1410-a883-16d83cab0980",
			displayValue: "Клиент"
		},
		Employee: {
			value: "60733efc-f36b-1410-a883-16d83cab0980",
			displayValue: "Сотрудник"
		},
		Provider: {
			value: "ac278ef3-e63f-48d9-ba34-7c52e92fecfe",
			displayValue: "Поставщик"
		}
	};
	return {
		CommunicationConsts: communicationConsts,
		OpportunityStage: opportunityStage,
		OrgStructure: orgStructure,
		FuncStructure: funcStructure,
		ContactType: contactType,
		LeadStages: leadStages,
		LeadType: leadType
	};
});