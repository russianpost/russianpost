define("FunnelChartSchema", ["ITOpportunityFunnelViewConfig", "ITFunnelByCountDataProvider", "ITFunnelToPreviousStageDataProvider", "ITFunnelToFirstStageDataProvider"],
    function(ITOpportunityFunnelViewConfig) {
        return {
            entitySchemaName: "Opportunity",
            attributes: {
                LeadTypeValue: {
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                    isLookup: true,
                    name: "LeadTypeValue"
                },

                LeadTypeList: {
                    type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
                    name: "LeadTypeList"
                }
            },
            methods: {

                init: function() {
                    this.callParent(arguments);
                    this.set("LeadTypeList", null);
                },

                initProvider: function(provider) {
                    provider.pushHistory({
                        entitySchemaName: this.get("EntitySchemaName"),
                        moduleSchemaName: this.get("ModuleSchemaName"),
                        sectionBindingColumn: this.get("sectionBindingColumn"),
                        dateTimeFormat: this.get("DateTimeFormat") || "Year;Month",
                        xAxisColumn: this.get("XAxisColumn"),
                        orderBy: this.get("OrderByAxis"),
                        orderDirection: this.get("OrderDirection"),
                        func: this.get("Func"),
                        seriesKind: this.get("SeriesKind"),
                        yAxisColumn: this.get("YAxisColumn"),
                        displayMode: this.get("displayMode"),
                        seriesConfig: this.get("SeriesConfig"),
                        yAxis: this.get("yAxis"),
                        YAxisCaption: this.get("YAxisCaption"),
                        yAxisConfig: this.get("yAxisConfig"),
                        styleColor: this.get("styleColor"),
                        startDate: this.get("StartDate"),
                        dueDate: this.get("DueDate"),
                        leadType: this.get("LeadTypeValue")
                    });
                },

                getProvidersCollectionConfig: function() {
                    var config = this.callParent();
                    var byCount = Terrasoft.findItem(config, {tag: "byNumberConversion"});
                    byCount.item.className = "Terrasoft.ITFunnelByCountDataProvider";
                    var byPreviousStage = Terrasoft.findItem(config, {tag: "stageConversion"});
                    byPreviousStage.item.className = "Terrasoft.ITFunnelToPreviousStageDataProvider";
                    var byFirstStage = Terrasoft.findItem(config, {tag: "toFirstConversion"});
                    byFirstStage.item.className = "Terrasoft.ITFunnelToFirstStageDataProvider";
                    return config;
                },

                getViewConfigClassName: function() {
                    return "Terrasoft.ITOpportunityFunnelViewConfig";
                },


                getLeadTypeList: function() {
                    var items = [];
                    var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
                        rootSchemaName: "LeadType",
                        clientESQCacheParameters: {
                            cacheItemName: "LeadTypeCache"
                        }
                    });
                    esq.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_COLUMN, "Id");
                    esq.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_DISPLAY_COLUMN, "Name");
                    esq.getEntityCollection(function(result) {
                        var collection = this.Ext.create("Terrasoft.Collection");
                        if (result.success) {
                            var queryCollection = result.collection;
                            queryCollection.each(function(leadType) {
                                var item = {
                                    value: leadType.get("Id"),
                                    displayValue: leadType.get("Name")
                                };
                                collection.add(leadType.get("Id"), item);
                            }, this);
                            this.set("LeadTypeList", collection);
                        }
                    }, this);
                },


                onLeadTypeValueChange: function(newValue) {
                    this.set("LeadTypeValue", newValue);
                    this.providersCollection.each(function(provider) {
                        provider.changeCurrentHistory({
                            leadType: newValue
                        });
                    });
                    this.refreshData();
                }

            }
        };
    });
