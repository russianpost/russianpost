define("LeadPageV2", ["ConfigurationConstants", "LeadConfigurationConst", "ITJsLeadConsts",
	"BusinessRuleModule", "ITLeadManagementUtilities"],
	function(ConfigurationConstants, LeadConfigurationConst, ITJsLeadConsts, BusinessRuleModule) {
	return {

		entitySchemaName: "Lead",

		rules: {
			"QualifiedContact": {
				"FiltrationContactByAccount": {
					"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
					"autocomplete": true,
					"autoClean": true,
					"baseAttributePatch": "Account",
					"comparisonType": Terrasoft.ComparisonType.EQUAL,
					"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
					"attribute": "QualifiedAccount"
				}
			}
		},

		messages: {
			"GetModuleInfo": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"SaveDisqalificationModalBox": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			"UpdateSection": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			}
		},

		attributes: {
			"ITCompetitor" : {
				dataValueType: Terrasoft.DataValueType.LOOKUP,
				lookupListConfig: {
					filters: [
						function() {
							var filterGroup = Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("CompetitorFilter",
								Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL,
									"Type.Id",
									ConfigurationConstants.AccountType.Competitor));
							return filterGroup;
						}
					]
				}
			},

			"ITCompetitorInBlock": {
				dataValueType: Terrasoft.DataValueType.LOOKUP,
				lookupListConfig: {
					filters: [
						function() {
							var filterGroup = Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("CompetitorFilter",
								Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL,
									"Type.Id",
									ConfigurationConstants.AccountType.Competitor));
							return filterGroup;
						}
					]
				}
			},

			"OwnerEditable": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				value: true
			},

			"IsNotDeliveryBillType": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				value: true,
				dependencies: [{
					columns: ["LeadType"],
					methodName: "onLeadTypeChange"
				}]
			},
			
			"Owner": {
				"dataValueType": Terrasoft.DataValueType.LOOKUP,
				"lookupListConfig": {
					"filters": [
						function() {
							var filterGroup = Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("isAssociate",
								Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Type",
									ConfigurationConstants.ContactType.Employee,
									Terrasoft.DataValueType.GUID));
							filterGroup.add("isOurCompany",
								Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Account.Type",
									ConfigurationConstants.AccountType.OurCompany,
									Terrasoft.DataValueType.GUID));
							filterGroup.add("IsContactsByLeadType",
										Terrasoft.createColumnFilterWithParameter(
											Terrasoft.ComparisonType.EQUAL,
											"[ITContactLeadType:ITContact].ITLeadType.Id",
											this.get("LeadType").value));
							return filterGroup;
						}
					]
				}
			},

			"QualifyStatus": {
				lookupListConfig: {
					columns: ["IsFinal"]
				}
			},

			"QualifiedAccount": {
				"lookupListConfig": {
					"filters": [
						function() {
							var filterGroup = Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("IsClientFilter",
								Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL,
									"Type.Id",
									ConfigurationConstants.AccountType.Client));
							return filterGroup;
						}
					]
				}
			},

			// Делаем поле "Зрелость потребности" необязательным для заполнения.
			"LeadTypeStatus": {
				isRequired: false
			},

			"TsTarifficatorRegion": {
				// Фильтрация справочника по макрорегиону у ответственного в лиде.
				"lookupListConfig": {
					"filters": [
						function() {
							var filterGroup = Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("FilterRegionsByMacroregion",
								Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL, "ITMacroregion", this.macroregionId));
							return filterGroup;
						}
					]
				}
			}
		},

		methods: {
				
			meetingsFilter: function() {
				var filterGroup = new this.Terrasoft.createFilterGroup();
				filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "42C74C49-58E6-DF11-971B-001D60E938C6"));
				// добавляете фильтр по типу активности
				filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Lead", this.get("Id")));
				return filterGroup;
			},



			
			callsActivityFilter: function() {
				var filterGroup = new this.Terrasoft.createFilterGroup();
				filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "03DF85BF-6B19-4DEA-8463-D5D49B80BB28"));
				// добавляете фильтр по типу активности
				filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Lead", this.get("Id")));
				return filterGroup;
			},



			
			tasksActivityFilter: function() {
				var filterGroup = new this.Terrasoft.createFilterGroup();
				filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "F51C4643-58E6-DF11-971B-001D60E938C6"));
				// добавляете фильтр по типу активности
				filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Lead", this.get("Id")));
				return filterGroup;
			},



				
			onEntityInitialized: function() {
				this.callParent(arguments);
				this.getOwnerEditable();
				this.onLeadTypeChange();
				this.setRegionFromAccount();
				this.identifyMacroregion();
			},
			
			DisqalificationButtonVisible: function() {
				return false;
			},

			subscribeDisqalificationModalEvents: function() {
				this.sandbox.subscribe("GetModuleInfo", function() {
					return {
						schemaName: "ITDisqalificationModalBox"
					};
				}, this, [this.getDisqalificationModalBoxId("ITDisqalificationModalBox")]);
			},

			subscribeSaveDisqalificationModalEvents: function() {
				this.sandbox.subscribe("SaveDisqalificationModalBox", function(data) {
					this.disqualifySave(data);
				}, this, [this.getDisqalificationModalBoxId("ITDisqalificationModalBox")]);
			},

			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				this.subscribeDisqalificationModalEvents();
				this.subscribeSaveDisqalificationModalEvents();
			},

			getOwnerEditable: function() {
				var currentUserContactId = this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value;
				var ownerId = this.get("Owner");
				if (ownerId && ownerId.value) {
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Contact"});
					esq.addColumn("Owner", "Owner");
					esq.getEntity(ownerId.value, function(result) {
						var editable = true;
						if (result.success &&
							result.entity &&
							result.entity.values.Owner !== "" &&
							currentUserContactId !== result.entity.values.Owner.value) {
							editable = false;
						}
						this.set("OwnerEditable", editable);
					}, this);
				} else {
					this.set("OwnerEditable", true);
				}
			},

			onLeadTypeChange: function() {
				if (this.get("LeadType").displayValue === "Доставка счетов") {
					this.set("IsNotDeliveryBillType", true);
				} else {
					this.set("IsNotDeliveryBillType", false);
				}
			},

			getActions: function() {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": {"bindTo": "Resources.Strings.DisqalificationButtonCaption" },
					"Click": {"bindTo": "onDisqualifyModalBoxButtonClick"},
					"Visible": {"bindTo": "isDisqalifyFinalStatus"}
				}));
				return actionMenuItems;
			},

			getDisqalificationModalBoxId: function(schemaName) {
				return this.sandbox.id + "_" + schemaName;
			},

			onDisqualifyModalBoxButtonClick: function() {
				var config = {
					id: this.getDisqalificationModalBoxId("ITDisqalificationModalBox")
				};
				this.sandbox.loadModule("ModalBoxSchemaModule", config);
			},

			isDisqalificationStatus: function() {
				var status = this.get("QualifyStatus");
				return (status && status.value === LeadConfigurationConst.LeadConst.QualifyStatus.Disqualified);
			},

			isDisqalifyFinalStatus: function() {
				var status = this.get("QualifyStatus");
				return (status && status.value !== LeadConfigurationConst.LeadConst.QualifyStatus.Disqualified && !status.IsFinal);
			},

			isOtherDisqalificationReason: function() {
				var reason = this.get("LeadDisqualifyReason");
				return (reason && reason.value === ITJsLeadConsts.DisqualifyReason.Other);
			},

			isCompetitorDisqalificationReason: function() {
				var reason = this.get("LeadDisqualifyReason");
				return (reason && reason.value === ITJsLeadConsts.DisqualifyReason.AlreadyHasContract);
			},

			getAccountCompetitorFilter: function() {
				var filterGroup = Ext.create("Terrasoft.FilterGroup");
				filterGroup.add("CompetitorFilter",
					Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL,
						"Type.Id",
						ConfigurationConstants.AccountType.Competitor));
				return filterGroup;
			},

			getDisqualifyUpdateQuery: function(leadId, data) {
				var update = Ext.create("Terrasoft.UpdateQuery", {
					rootSchemaName: "Lead"
				});
				var idFilter = update.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Id", leadId);
				update.filters.add("IdFilter", idFilter);
				update.setParameterValue("LeadDisqualifyReason", data.reason.value, this.Terrasoft.DataValueType.GUID);
				update.setParameterValue("QualifyStatus", LeadConfigurationConst.LeadConst.QualifyStatus.Disqualified,
					this.Terrasoft.DataValueType.GUID);
				this.set("QualifyStatus", {value: LeadConfigurationConst.LeadConst.QualifyStatus.Disqualified});
				this.set("LeadDisqualifyReason", data.reason);
				if (data.description) {
					update.setParameterValue("ITDisqualifyReasonDescription", data.description, this.Terrasoft.DataValueType.TEXT);
					this.set("ITDisqualifyReasonDescription", data.description);
				}
				if (data.account) {
					update.setParameterValue("ITCompetitor", data.account.value, this.Terrasoft.DataValueType.GUID);
					this.set("ITCompetitor", data.account);
				}
				return update;
			},

			disqualifySave: function(data) {
				var leadId = this.get("Id");
				var saveCallbackConfig = {
					callback: this.leadManagementProcessCallback,
					isSilent: true
				};
				var bq = Ext.create("Terrasoft.BatchQuery");
				var updateLead = this.getDisqualifyUpdateQuery(leadId, data);
				bq.add(updateLead);
				bq.execute(this.save.bind(this, saveCallbackConfig), this);
			},

			leadManagementProcessCallback: function() {
				this.callParent(arguments);
				this.sandbox.publish("UpdateSection", null, ["LeadSectionV2_UpdateSection"]);
			},

			// Если Контрагент не выбран, показывает сообщение и не даёт возможность сохранить.
			save: function() {
				var qualifiedAccount = this.get("QualifiedAccount");
				if (this.Ext.isEmpty(qualifiedAccount)) {
					this.showInformationDialog("Необходимо добавить контрагента.");
				} else {
					this.callParent(arguments);
				}
			},

			// Если Контрагент не выбран, показывает сообщение и не даёт возможность закрыть карточку.
			onCloseClick: function() {
				var qAccount = this.get("QualifiedAccount");
				if (this.Ext.isEmpty(qAccount)) {
					this.showInformationDialog("Необходимо добавить контрагента.");
				} else {
					this.callParent(arguments);
				}
			},

			// Метод автоматически заполняет поле "Регион" значением региона связанного контрагента.
			setRegionFromAccount: function() {
				var region = this.get("TsTarifficatorRegion");
				if (!region) {
					var account = this.get("QualifiedAccount");
					if (!account) {
						return;
					} else {
						var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "Account"
						});
						esq.addColumn("TsTarifficatorRegion", "TsTarifficatorRegion");
						esq.getEntity(account.value, function(result) {
							if (result.success) {
								var reg = result.entity.get("TsTarifficatorRegion");
								if (reg) {
									this.set("TsTarifficatorRegion", reg);
									this.save();
								/*	var update = this.Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "Lead"});
									update.enablePrimaryColumnFilter(this.get("Id"));
									update.setParameterValue("TsTarifficatorRegion", reg.value,
										this.Terrasoft.DataValueType.GUID);
									update.execute();*/
								}
							}
						}, this);
					}
				}
			},

			// Достаёт Id макрорегиона у ответствееного в лиде (для фильтрации справочника).
			identifyMacroregion: function() {
				var owner = this.get("Owner");
				if (!this.Ext.isEmpty(owner)) {
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Contact"
					});
					esq.addColumn("ITMacroregion", "ITMacroregion");
					esq.getEntity(owner.value, function(result) {
						if (result.success) {
							var macroregion = result.entity.get("ITMacroregion");
							if (!this.Ext.isEmpty(macroregion)) {
								this.macroregionId = macroregion.value;
							}
						}
					}, this);
				}
			}

		},

		details: /**SCHEMA_DETAILS*/{
			"ITTownCategoryDetail": {
				"schemaName": "ITLeadTownCategoryDetail",
				"entitySchemaName": "ITTownCategoryInLead",
				"filter": {
					"detailColumn": "ITLead",
					"masterColumn": "Id"
				}
			},
			"ITMeetingsDetail": {
				"schemaName": "ITMeetingsDetailV2",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				},
				filterMethod: "meetingsFilter"
			},
			"ITTasksDetailV2": {
				"schemaName": "ITTasksDetailV2",
				//"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				},
				filterMethod: "tasksActivityFilter"
			},
			"ITCallsDetail": {
				"schemaName": "ITCallsDetailV2",
				//"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				},
				filterMethod: "callsActivityFilter"
			}
		}/**SCHEMA_DETAILS*/,

		diff: /**SCHEMA_DIFF*/[
			//Удаление детали "Заказы"
			{
				"operation": "remove",
				"name": "Order"
			},

			{
				"operation": "merge",
				"name": "NewLeadType",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},

			{
				"operation": "merge",
				"name": "LeadRegisterMethodInProfile",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 4
					}
				}
			},

			{
				"operation": "merge",
				"name": "LeadWebFormInProfile",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 5
					}
				}
			},
			{
				"operation": "merge",
				"name": "LeadCreatedOn",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 6
					}
				}
			},
			{
				"operation" : "insert",
				"name" : "DisqualifyReasonBlockContainer",
				"values" : {
					itemType : Terrasoft.ViewItemType.CONTAINER,
					"items" : [],
					"layout": {"column": 12, "row": 0, "colSpan": 12}
				},
				"parentName" : "LeadTypeBlock",
				"propertyName" : "items",
				"index" : 0
			},
			{
				"operation": "insert",
				"name": "ITLeadDisqualifyReasonInBlock",
				"parentName": "DisqualifyReasonBlockContainer",
				"propertyName": "items",
				"values": {
					"caption": {"bindTo": "Resources.Strings.LeadDisqualifyReasonDescriptionCaption" },
					"bindTo": "ITDisqualifyReasonDescription",
					"visible": {"bindTo": "isOtherDisqalificationReason"},
					"contentType": Terrasoft.ContentType.LONG_TEXT,
					"enabled": false
				},
				"index" : 0
			},
			{
				"operation": "insert",
				"name": "ITCompetitorInBlock",
				"parentName": "DisqualifyReasonBlockContainer",
				"propertyName": "items",
				"values": {
					//"layout": {"column": 12, "row": 0, "colSpan": 12},
					"bindTo": "ITCompetitor",
					"visible": {"bindTo": "isCompetitorDisqalificationReason"},
					"enabled": false
				},
				"index" : 0
			},
			{
				"operation": "move",
				"name": "NewLeadDisqualifyReason",
				"parentName": "LeadTypeBlock",
				"propertyName": "items",
				"values": {
					"bindTo": "LeadDisqualifyReason",
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					},
					"enabled": false,
					"visible": {"bindTo": "isDisqalificationStatus"}
				}
			},
			{
				"operation": "insert",
				"name": "ITPotential",
				"values": {
					"tip": {
						// Текст подсказки.
						"content": {"bindTo": "Resources.Strings.ITPotentialTipContent"},
						// Режим отображения подсказки.
						// По умолчанию режим WIDE - толщина зеленой полоски,
						// которая отображается в подсказке.
						"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
					},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "LeadPageRegisterInfoBlock"
					},
					"bindTo": "ITPotential",
					"labelConfig": {},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "LeadPageRegisterInfoBlock",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "ITPriority",
				"values": {
					"tip": {
						// Текст подсказки.
						"content": {"bindTo": "Resources.Strings.ITPriorityTipContent"},
						// Режим отображения подсказки.
						// По умолчанию режим WIDE - толщина зеленой полоски,
						// которая отображается в подсказке.
						"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
					},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "LeadPageRegisterInfoBlock"
					},
					"bindTo": "ITPriority",
					"labelConfig": {},
					"enabled": true,
					"contentType": 3
				},
				"parentName": "LeadPageRegisterInfoBlock",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "TsTarifficatorRegion",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "insert",
				"name": "ITLeadDescription",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "LeadPageRegisterInfoBlock"
					},
					"contentType": 0,
					"bindTo": "ITLeadDescription"
				},
				"parentName": "LeadPageRegisterInfoBlock",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "ITBankBookCount",
				"values": {
					"visible": {"bindTo": "IsNotDeliveryBillType"},
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "LeadPageRegisterInfoBlock"
					},
					"bindTo": "ITBankBookCount"
				},
				"parentName": "LeadPageRegisterInfoBlock",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "ITTownCategoryDetail",
				"values": {
					"visible": {"bindTo": "IsNotDeliveryBillType"},
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "LeadPageGeneralTabContainer",
				"propertyName": "items",
				"index": 2
			},
			{
				"operation": "merge",
				"name": "CountryStr",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "RegionStr",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "CityStr",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "LeadTypeStatus",
				"values": {
					"tip": {
						// Текст подсказки.
						"content": {"bindTo": "Resources.Strings.LeadTypeStatusTipContent"},
						// Режим отображения подсказки.
						// По умолчанию режим WIDE - толщина зеленой полоски,
						// которая отображается в подсказке.
						"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
					},
					"contentType": this.Terrasoft.ContentType.ENUM,
					"isRequired": false,
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "LeadMedium",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "LeadSource",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "RegisterMethod",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "Owner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					},
					"enabled": {
						"bindTo": "OwnerEditable"
					}
				}
			},
			{
				"operation": "merge",
				"name": "OpportunityDepartment",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "SalesOwner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "MeetingDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Budget",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "DecisionDate",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "Opportunity",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					}
				}
			},
			{
				"operation": "move",
				"name": "Opportunity",
				"parentName": "LeadPageTransferToSaleInfoBlock",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "remove",
				"name": "Contact"
			},
			{
				"operation": "remove",
				"name": "Account"
			},
			{
				"operation": "remove",
				"name": "Website"
			},
			{
				"operation": "remove",
				"name": "MobilePhone"
			},
			{
				"operation": "remove",
				"name": "Job"
			},
			{
				"operation": "remove",
				"name": "Email"
			},
			{
				"operation": "remove",
				"name": "Country"
			},
			{
				"operation": "remove",
				"name": "SpecificationInLead"
			},
			{
				"operation": "remove",
				"name": "SiteEvent"
			},
			{
				"operation": "remove",
				"name": "WebForm"
			},
			{
				"operation": "remove",
				"name": "BpmRef"
			},
			{
				"operation": "remove",
				"name": "Campaign"
			},
			{
				"operation": "remove",
				"name": "BulkEmail"
			},
			{
				"operation": "remove",
				"name": "Activities"
			},
			{
				"operation": "remove",
				"name": "Calls"
			},
			{
				"operation": "insert",
				"name": "ITMeetingsDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "LeadHistoryTabContainer",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "ITTasksDetailV2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "LeadHistoryTabContainer",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "ITCallsDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "LeadHistoryTabContainer",
				"propertyName": "items",
				"index": 12
			},
			// Скрыли деталь "Похожие лиды".
			{
				"operation": "merge",
				"name": "LeadsSimilarSearchResult",
				"values": {
					"visible": false,
					"itemType": Terrasoft.ViewItemType.DETAIL
				}
			}
		]/**SCHEMA_DIFF*/
	};
});