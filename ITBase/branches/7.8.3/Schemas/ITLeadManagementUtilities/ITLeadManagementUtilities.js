define("ITLeadManagementUtilities", ["ConfigurationConstants", "ServiceHelper", "LeadManagementUtilitiesResources",
		"LeadConfigurationConst", "ProcessModuleUtilities", "DesktopPopupNotification", "LeadManagementUtilities"],
	function(ConfigurationConstants, ServiceHelper, resources, LeadConfigurationConst,
			ProcessModuleUtilities, DesktopPopupNotification) {

		Ext.define("Terrasoft.ITLeadManagementUtilities", {
			alternateClassName: "Terrasoft.ITLeadManagementUtilities",
			override: "Terrasoft.LeadManagementUtilities",

			defaultProcessName: "LeadManagement78",

			columns: {

				"DisqualificationMenuItems": {
					dataValueType: Terrasoft.DataValueType.COLLECTION
				},

				"DisqualificationReasonsMenuItems": {
					dataValueType: Terrasoft.DataValueType.COLLECTION
				}
			},


			getEntity: function() {
				var entity = this;
				if (Ext.isFunction(this.getActiveRow)) {
					entity = this.getActiveRow();
				}
				return entity;
			},


			getLeadManagementProcessId: function() {
				var entity = this.getEntity();
				return entity.get("QualificationProcessId");
			},

			updateActivity: function(activityId, callback) {
				var detailedResult = this.get("DetailedResultValue");
				this.setDashboardMask(true);
	//			var recordId = this.get("Id");
				var update = Ext.create("Terrasoft.UpdateQuery", {
					rootSchemaName: "Activity"
				});
				var filters = update.filters;
				var activityIdFilter = update.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
					"Id", activityId);
				filters.add("activityIdFilter", activityIdFilter);
				update.setParameterValue("Status", ConfigurationConstants.Activity.Status.Done, Terrasoft.DataValueType.GUID);
				update.setParameterValue("Result", ConfigurationConstants.Activity.ActivityResult.Completed,
					Terrasoft.DataValueType.GUID);
				if (detailedResult) {update.setParameterValue("DetailedResult", detailedResult, Terrasoft.DataValueType.TEXT); }
				update.execute(function(result) {
					callback.call(this, result);
				}, this);
			},

			updateStatusLead: function(leadId, statusId) {
				var update = this.Ext.create("Terrasoft.UpdateQuery", {
					rootSchemaName: "Lead"
				});
				var filters = update.filters;
				var idFilter = update.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
					"Id", leadId);
				filters.add("IdFilter", idFilter);
				update.setParameterValue("QualifyStatus", statusId, this.Terrasoft.DataValueType.GUID);
				return update;
			},

			setTransferForSaleStatus: function() {
				var leadId = this.getPrimaryColumnValue();
				var batchQuery = Ext.create("Terrasoft.BatchQuery");
				var update = this.updateStatusLead(leadId, LeadConfigurationConst.LeadConst.QualifyStatus.WaitingForSale);
				batchQuery.add(update);
				batchQuery.execute(function() {
					this.hideBodyMask();
					if (this.isSection()) {this.updateSection(); }
					else {this.reloadEntity(); }
				}, this);
			},

			executeLeadManagementProcess: function(config) {
				this.set("LockBodyMask", false);
				var leadManagementProcessId = this.getLeadManagementProcessId();
				if (leadManagementProcessId === "00000000-0000-0000-0000-000000000000") {
					leadManagementProcessId = undefined;
				}
				
				var continueExecuting = !(config && config.continueExecuting === false);
				if (leadManagementProcessId &&
					continueExecuting) {
					var qualifyStatus = this.getQualifyStatus();
					if (qualifyStatus && qualifyStatus === LeadConfigurationConst.LeadConst.QualifyStatus.TransferForSale) {
						this.setTransferForSaleStatus();
					}
					else {ProcessModuleUtilities.continueExecuting(leadManagementProcessId, this); }
				} else if (!leadManagementProcessId) {
					Terrasoft.SysSettings.querySysSettingsItem("LeadManagementProcess", function(sysSetting) {
						var processConfig = this.getLeadManagementProcessConfig(config);
						var extendedConfig = {};
						if (sysSetting) {
							extendedConfig.sysProcessName = "";
							extendedConfig.sysProcessId = sysSetting.value;
						}
						Ext.apply(processConfig, extendedConfig);
						ProcessModuleUtilities.executeProcess(processConfig);
					}, this);
				} else {
					this.leadManagementProcessCallback(config);
				}
			},


			getLeadManagementProcessConfig: function(callbackConfig) {
				return {
					"sysProcessName": this.defaultProcessName,
					"sysProcessId": "",
					"parameters": this.getLeadManagementParameters(),
					"callback": this.leadManagementProcessCallback.bind(this, callbackConfig)
				};
			},


			leadManagementProcessCallback: function(config) {
				var qualifyStatus = this.getQualifyStatus();
				if (qualifyStatus === LeadConfigurationConst.LeadConst.QualifyStatus.Qualification) {
					this.showNotification();
				}
				var callback = this.reloadEntity;
				var scope = this;
				if (!Ext.isEmpty(config)) {
					callback = this.getMethodByName(config.callbackMethodName) || callback;
					scope = config.scope || this;
				}
				if (callback) {
					callback.call(scope);
				}
				this.set("LockBodyMask", false);
			},


			getMethodByName: function(name) {
				var method = this[name];
				if (Ext.isFunction(method)) {
					return method;
				}
				return null;
			},


			showNotification: function() {
				var config = this.getNotificationConfig();
				DesktopPopupNotification.showNotification(config);
			},


			getNotificationConfig: function() {
				return {
					id: Terrasoft.generateGUID(),
					title: resources.localizableStrings.QuialifiedNotificationTitle,
					body: resources.localizableStrings.QuialifiedNotificationBody,
					icon: this.getModuleIconUrl(),
					onShow: this.onShowNotification,
					onClick: this.onNotificationClick,
					scope: this,
					ignorePageVisibility: true
				};
			},


			onNotificationClick: function() {
				this.sandbox.publish("PushHistoryState", {hash: "CardModuleV2/LeadPageV2/edit/" +
				this.getPrimaryColumnValue()});
			},


			getModuleIconUrl: function() {
				var module = Terrasoft.configuration.ModuleStructure.Lead;
				return Terrasoft.ImageUrlBuilder.getUrl({
					source: Terrasoft.ImageSources.SYS_IMAGE,
					params: {
						primaryColumnValue: module.logoId
					}
				});
			},


			onShowNotification: function(event) {
				setTimeout(function() {
					DesktopPopupNotification.closeNotification(event.target);
				}, DesktopPopupNotification.defaultDisplayTimeout);
			},


			getLeadManagementParameters: function() {
				return {
					"LeadId": this.getPrimaryColumnValue(),
					"ShowDistributionPage": this.isSection()
				};
			},


			executeCardMethod: function(config) {
				this.sandbox.publish("OnCardAction", config, [this.getCardModuleSandboxId()]);
			},


			onLeadManagementSectionButtonClick: function() {
				if (this.get("IsCardVisible")) {
					this.executeCardMethod("onLeadManagementButtonClick");
				} else {
					this.continueLeadManagementExecuting();
				}
			},


			continueLeadManagementExecuting: function() {
				var leadManagementProcessId = this.getLeadManagementProcessId();
				if (!leadManagementProcessId) {
					var entity = this.getEntity();
					this.refreshColumns(["QualificationProcessId"], this.executeLeadManagementProcess, entity);
				} else {
					this.executeLeadManagementProcess();
				}
			},


			onLeadManagementButtonClick: function() {
				var config = {
					callback: this.continueLeadManagementExecuting,
					isSilent: true,
					lockBodyMask: true
				};
				var btnCaption = this.get("QualificationProcessButtonCaption");
				if (btnCaption === resources.localizableStrings.QualifyStatusTranslationForSaleCaption) {
					var recordId = this.get("Id");
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "LeadProduct"
					});
					esq.addColumn("ITClientInterest", "ITClientInterest");
					esq.filters.add("esqFilter", esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
						"Lead", recordId));
					var filterGroup = Ext.create("Terrasoft.FilterGroup");
					filterGroup.logicalOperation = this.Terrasoft.LogicalOperatorType.OR;
					filterGroup.add("IsNull", Terrasoft.createColumnIsNullFilter("ITClientInterest"));
					filterGroup.add("IsNotInterested", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "ITClientInterest", "c4d45b73-9a86-456a-b182-bd49bd801f0b"));
					esq.filters.add("filterGroup", filterGroup);
					esq.getEntityCollection(function(result) {
						if (result.success) {
							if (result.collection.collection.items.length) {
								var message = "Внимание! Вы создаете продажу. В продажу будут скопированы только те услуги, " +
									"которые помечены как заинтересовавшие клиента."
								this.showConfirmationDialog(message,
									function(returnCode) {
										if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
											this.save(config);
										}
									}, ["yes", "cancel"]);
							} else {
								this.save(config);
							}
						}
					}, this);
				} else {
					this.save(config);
				}
			},


			getIsQualificationOnActiveStage: function(qualifyStatusId) {
				var qualifyStatus = LeadConfigurationConst.LeadConst.QualifyStatus;
				var activeStageStatuses = [qualifyStatus.Qualification, qualifyStatus.Distribution,
					qualifyStatus.TransferForSale, qualifyStatus.WaitingForSale];
				return Terrasoft.contains(activeStageStatuses, qualifyStatusId);
			},


			getQualifyStatus: function() {
				var qualifyStatus = this.get("QualifyStatus");
				return (qualifyStatus) ? qualifyStatus.value : null;
			},


			initLeadManagement: function(callback, scope) {
				Terrasoft.chain(
					this.initParameters,
					this.initDisqualifyButtonItems,
					callback,
					scope || this
				);
			},


			initParameters: function(callback, scope) {
				Terrasoft.SysSettings.querySysSettingsItem("UseProcessLeadManagement", function(value) {
					this.set("UseProcessLeadManagement", value);
					callback.call(scope || this);
				}, this);
			},


			initLeadManagementControls: function(entity, callback, scope) {
				this.initLeadManagementButtonCaption(entity);
				this.checkIsProcessNotAttachedToEntity(entity, this.initLeadManagementButtonVisibility, this);
				this.initDisqalificationButtonVisibility();
				Ext.callback(callback, scope || this);
			},


			initLeadManagementButtonCaption: function(entity) {
				entity = entity || this.getEntity();
				if (Ext.isEmpty(entity)) {
					return;
				}
				var primaryColumnValue = entity.get(entity.primaryColumnName);
				var qualifyStatusId = this.getQualifyStatus(primaryColumnValue);
				var qualifyStatus = LeadConfigurationConst.LeadConst.QualifyStatus;
				var caption = "";
				switch (qualifyStatusId) {
					case qualifyStatus.Qualification:
						caption = resources.localizableStrings.QualifyStatusQualificationCaption;
						break;
					case qualifyStatus.Distribution:
						caption = resources.localizableStrings.QualifyStatusDistributionCaption;
						break;
					case qualifyStatus.TransferForSale:
						caption = resources.localizableStrings.QualifyStatusTranslationForSaleCaption;
						break;
				}
				this.set("QualificationProcessButtonCaption", caption);
			},


			addProcessItemQueryColumns: function(esq) {
				esq.addColumn("Id");
			},


			addProcessItemQueryFilters: function(esq, primaryColumnValue) {
				var filters = esq.filters;
				filters.addItem(esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
					"EntityId", primaryColumnValue));
			},


			getProcessItemQuery: function(primaryColumnValue) {
				var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "SysProcessEntity"
				});
				this.addProcessItemQueryColumns(esq);
				this.addProcessItemQueryFilters(esq, primaryColumnValue);
				return esq;
			},


			addQualificationProcessQueryColumns: function(esq) {
				esq.addColumn("QualificationProcessId");
			},


			addQualificationProcessQueryFilters: function(esq, primaryColumnValue) {
				esq.enablePrimaryColumnFilter(primaryColumnValue);
			},


			getQualificationProcessQuery: function(primaryColumnValue) {
				var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "Lead"
				});
				this.addQualificationProcessQueryColumns(esq);
				this.addQualificationProcessQueryFilters(esq, primaryColumnValue);
				return esq;
			},


			getQualificationProcessIdFromQueryResult: function(rows) {
				var qualProcessId = "";
				if (!this.Ext.isEmpty(rows)) {
					qualProcessId = rows[0].QualificationProcessId;
				}
				return qualProcessId;
			},


			getProcessNotAttachedToEntityQuery: function(primaryColumnValue) {
				var processItemEsq = this.getProcessItemQuery(primaryColumnValue);
				var qualificationProcessEsq = this.getQualificationProcessQuery(primaryColumnValue);
				var batchQuery = this.Ext.create("Terrasoft.BatchQuery");
				batchQuery.add(processItemEsq);
				batchQuery.add(qualificationProcessEsq);
				return batchQuery;
			},


			checkIsProcessNotAttachedToEntity: function(entity, callback, scope) {
				entity = entity || this.getEntity();
				if (Ext.isEmpty(entity)) {
					return;
				}
				var primaryColumnValue = entity.get(entity.primaryColumnName);
				this.set("IsProcessNotAttachedToEntity", true);
				var batchQuery = this.getProcessNotAttachedToEntityQuery(primaryColumnValue);
				batchQuery.execute(function(response) {
					if (response.success) {
						var queryResults = response.queryResults;
						var sysProcessEntities = queryResults[0].rows;
						var qualProcessId =
							this.getQualificationProcessIdFromQueryResult(queryResults[1] && queryResults[1].rows);
						if (!this.Ext.isEmpty(sysProcessEntities) || !qualProcessId) {
							this.set("IsProcessNotAttachedToEntity", false);
						}
					}
					this.Ext.callback(callback, scope || this, [entity]);
				}, this);
			},


			initLeadManagementButtonVisibility: function(entity) {
				entity = entity || this.getEntity();
				if (Ext.isEmpty(entity)) {
					return;
				}
				var primaryColumnValue = entity.get(entity.primaryColumnName);
				var isNewMode = ((this.isNewMode && this.isNewMode()) === true);
				var visible = false;
				if (!isNewMode) {
					var isCardVisible = this.get("IsCardVisible");
					var qualifyStatusId = this.getQualifyStatus(primaryColumnValue);
					var qualifyStatus = LeadConfigurationConst.LeadConst.QualifyStatus;
					var isProcessNotAttachedToEntity = this.get("IsProcessNotAttachedToEntity");
					var isQualification = (qualifyStatusId === qualifyStatus.Qualification);
					var isTransferForSale = (qualifyStatusId === qualifyStatus.TransferForSale);
					visible = isQualification || isTransferForSale;
					if (isCardVisible !== false && !visible) {
						visible = (isProcessNotAttachedToEntity && (
						qualifyStatusId === qualifyStatus.Distribution ||
						qualifyStatusId === qualifyStatus.TransferForSale
						));
					}
				}
				this.set("LeadManagementButtonVisible", visible);
			},


			initDisqalificationButtonVisibility: function() {
				var isNewMode = ((this.isNewMode && this.isNewMode()) === true);
				var visible = false;
				if (isNewMode === false) {
					var qualifyStatusId = this.getQualifyStatus();
					visible = this.getIsQualificationOnActiveStage(qualifyStatusId);
				}
				this.set("DisqalificationButtonVisible", visible);
			},


			onDisqualifyClick: function(reasonId) {
				if (this.isSection()) {
					this.executeCardMethod({
						methodName: "onDisqualifyClick",
						args: [reasonId]
					});
					return;
				}
				var saveCallbackConfig = {
					callback: this.leadManagementProcessCallback,
					isSilent: true,
					lockBodyMask: true
				};
				this.updateValues([
					{
						schemaName: "LeadDisqualifyReason",
						columnName: "LeadDisqualifyReason",
						recordId: reasonId
					},
					{
						schemaName: "QualifyStatus",
						columnName: "QualifyStatus",
						recordId: LeadConfigurationConst.LeadConst.QualifyStatus.Disqualified
					}
				], this.save.bind(this, saveCallbackConfig), this);
			},


			updateValues: function(config, callback, scope) {
				var bq = this.Ext.create("Terrasoft.BatchQuery");
				Terrasoft.each(config, function(item) {
					bq.add(this.getEntitySelectQuery(item.schemaName, item.recordId));
				}, this);
				bq.execute(function(response) {
					if (response.success) {
						Terrasoft.each(config, function(item, index) {
							var result = response.queryResults[index];
							if (Ext.isEmpty(result)) {
								return;
							}
							var resultItem = result.rows[0];
							if (Ext.isEmpty(resultItem)) {
								return;
							}
							var columnValue = {
								value: resultItem.Id,
								displayValue: resultItem.Name
							};
							this.set(item.columnName, columnValue);
						}, this);
					}
					callback.call(scope || this);
				}, this);
			},


			getEntitySelectQuery: function(schemaName, recordId) {
				var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: schemaName
				});
				esq.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_COLUMN, "Id");
				esq.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_DISPLAY_COLUMN, "Name");
				esq.enablePrimaryColumnFilter(recordId);
				return esq;
			},


			onNotInterestedClick: function() {
				if (this.isSection()) {
					this.executeCardMethod("onNotInterestedClick");
					return;
				}
				this.updateValues([{
					schemaName: "QualifyStatus",
					columnName: "QualifyStatus",
					recordId: LeadConfigurationConst.LeadConst.QualifyStatus.NotInterested
				}], this.save, this);
			},


			isSection: function() {
				var isSectionVisible = this.get("IsSectionVisible");
				return (isSectionVisible === true || isSectionVisible === false);
			},


			initDisqualifyButtonItems: function(callback, scope) {
				var disqual = "Дисквалифицировать";
				var menuItems = this.get("DisqualificationMenuItems");
				if (Ext.isEmpty(menuItems)) {
					menuItems = Ext.create("Terrasoft.BaseViewModelCollection");
					this.set("DisqualificationMenuItems", menuItems);
				}
				menuItems.clear();
				menuItems.addItem(this.getButtonMenuItem({
					"Caption": disqual,//resources.localizableStrings.DisqualificationReasonsButtonCaption,
					"Items": {"bindTo": "DisqualificationReasonsMenuItems"},
					"Visible": {"bindTo": "DisqalificationButtonVisible"}
				}));
				menuItems.addItem(this.getButtonMenuItem({
					"Caption": resources.localizableStrings.NotInterestedButtonCaption,
					"Click": {"bindTo": "onNotInterestedClick"},
					"Visible": {"bindTo": "DisqalificationButtonVisible"}
				}));
				this.getDisqualifyReasons(function(collection) {
					this.initDisqualifyReasonsButtonItems(collection, callback, scope);
				}, this);
			},


			initDisqualifyReasonsButtonItems: function(collection, callback, scope) {
				var menuItems = this.getDisqualifyButtonItems(collection);
				var disqualificationMenuItems = this.get("DisqualificationReasonsMenuItems");
				if (Ext.isEmpty(disqualificationMenuItems)) {
					disqualificationMenuItems = Ext.create("Terrasoft.BaseViewModelCollection");
					this.set("DisqualificationReasonsMenuItems", disqualificationMenuItems);
				}
				disqualificationMenuItems.loadAll(menuItems);
				callback.call(scope || this);
			},


			getDisqualifyButtonItems: function(collection) {
				var menuItems = this.Ext.create("Terrasoft.BaseViewModelCollection");
				if (collection instanceof Terrasoft.BaseViewModelCollection) {
					collection.each(function(item) {
						menuItems.addItem(this.getButtonMenuItem({
							"Caption": item.get("Name"),
							"Click": {"bindTo": "onDisqualifyClick"},
							"Tag": item.get("Id"),
							"Visible": {"bindTo": "DisqalificationButtonVisible"}
						}));
					}, this);
				}
				return menuItems;
			},


			getDisqualifyReasons: function(callback, scope) {
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					"rootSchemaName": "LeadDisqualifyReason"
				});
				esq.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_COLUMN, "Id");
				esq.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_DISPLAY_COLUMN, "Name");
				esq.getEntityCollection(function(response) {
					callback.call(scope, response.collection);
				}, this);
			}
		});
	});
