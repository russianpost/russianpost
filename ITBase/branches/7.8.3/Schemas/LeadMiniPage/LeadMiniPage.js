define("LeadMiniPage", ["ConfigurationConstants", "MiniPageResourceUtilities",
	"EmailHelper", "BusinessRuleModule", "BaseProgressBarModule", "css!BaseProgressBarModule", "css!LeadMiniPageCSS"],
	function(ConfigurationConstants, miniPageResources, EmailHelper, BusinessRuleModule) {
		return {
			entitySchemaName: "Lead",
			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
			attributes: {
				"QualifiedAccount": {
					"lookupListConfig": {
						"filters": [
							function() {
								var filterGroup = Ext.create("Terrasoft.FilterGroup");
								filterGroup.add("IsClientFilter",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Type.Id",
										ConfigurationConstants.AccountType.Client));
								return filterGroup;
							}
						]
					}
				}
			},
			messages: {
				"SetClientForLeadMiniPage": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.PUBLISH
				}
			},
			methods: {
				// Если поле "Клиент" не заполнено, показывает сообщение и не даёт возможность сохранить.
				save: function() {
					var qualifiedAccount = this.get("QualifiedAccount");
					if (this.Ext.isEmpty(qualifiedAccount)) {
						this.showInformationDialog("Поле 'Клиент' должно быть заполнено.");
					} else {
						this.callParent(arguments);
					}
				},
				
				getContactAccountVisibility: function(columnNames) {
					var sandbox = this.sandbox,
						clientFromAccountPage = sandbox.publish("SetClientForLeadMiniPage", null);
					if (clientFromAccountPage || this.get("IsFromSection") || this.get("IsFromQuickAddMenu")) {
						return true;
					}
					return !this.get("IsQualifiedLookupVisible") && this.isViewMode(columnNames);
				},
				
				onEntityInitialized: function() {
					this.callParent(arguments);
					//#RPCRM-898 Если пришло сообщение с данными контрагента для привязки, то вставить его в поле
					var sandbox = this.sandbox,
						clientFromAccountPage = sandbox.publish("SetClientForLeadMiniPage", null);
					if (clientFromAccountPage) {
						this.set("QualifiedAccount", clientFromAccountPage);
					}
				},

				//#RPCRM-898 Если пришло сообщение с данными контрагента для привязки, то заблочить поле
				IsBlockClient: function() {
					var sandbox = this.sandbox,
						clientFromAccountPage = sandbox.publish("SetClientForLeadMiniPage", null);
					if (clientFromAccountPage) {
						return false;
					}
					return true;
				}
			},
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "remove",
					"name": "Account",
					"parentName": "MiniPage",
					"propertyName": "items"
				},
				{
					"operation": "remove",
					"name": "Contact",
					"parentName": "MiniPage",
					"propertyName": "items"
				},
				{
					"operation": "insert",
					"name": "QualifiedAccount",
					"parentName": "MiniPage",
					"propertyName": "items",
					"values": {
						"bindTo": "QualifiedAccount",
						"caption": "Клиент",
						"layout": {"column": 0, "row": 4, "colSpan": 24},
						"isMiniPageModelItem": true,
						"showValueAsLink": false,
						"visible": {"bindTo": "getContactAccountVisibility"},
						"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
						"contentType": this.Terrasoft.ContentType.LOOKUP,
						"linkMouseOver": null,
						"isRequired": true,
						"enabled": {"bindTo": "IsBlockClient"}
					}
				},
				{
					"operation": "remove",
					"name": "QualifiedContact",
					"parentName": "MiniPage",
					"propertyName": "items"
				},
				{
					"operation": "insert",
					"name": "QualifiedContact",
					"parentName": "MiniPage",
					"propertyName": "items",
					"values": {
						"bindTo": "QualifiedContact",
						"layout": {"column": 0, "row": 5, "colSpan": 24},
						"isMiniPageModelItem": false,
						"showValueAsLink": false,
						"visible": {"bindTo": "getContactAccountVisibility"},
						"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
						"contentType": this.Terrasoft.ContentType.LOOKUP
					}
				},
				{
					"operation": "remove",
					"name": "Email",
					"parentName": "MiniPage",
					"propertyName": "items"
				},
				{
					"operation": "remove",
					"name": "QualifiedAccountContainer",
					"parentName": "MiniPage",
					"propertyName": "items"
				},
				{
					"operation": "remove",
					"name": "MobilePhone",
					"parentName": "MiniPage",
					"propertyName": "items"
				}
			]/**SCHEMA_DIFF*/,
			rules: {
				"QualifiedContact": {
					"FiltrationContactByAccount": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": true,
						"autoClean": true,
						"baseAttributePatch": "Account",
						"comparisonType": Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
						"attribute": "QualifiedAccount"
					}
				}
			}
		};
	}
);
