define("ITLeadProductDetailV2_Page", ["BusinessRuleModule"], function(BusinessRuleModule) {
	return {
		entitySchemaName: "LeadProduct",
		details:/**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		messages: {},
		attributes: {},
		methods: {},
		rules: {
			"Product": {
				"EnabledProduct": {
					"ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
					"property": BusinessRuleModule.enums.Property.ENABLED,
					"conditions": [
						{
							"leftExpression": {
								"type": BusinessRuleModule.enums.ValueType.CONSTANT,
								"value": true
							},
							comparisonType: Terrasoft.ComparisonType.NOT_EQUAL,
							rightExpression: {
								type: BusinessRuleModule.enums.ValueType.CONSTANT,
								value: true
							}
						}
					]
				}
			}
		},
		diff:/**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
	};
});
