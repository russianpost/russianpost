using System;
using System.Collections.Generic;
using System.Data;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Scheduler;
using Terrasoft.Configuration.TsBase;


namespace Terrasoft.Configuration.ITBase
{
    public class ITChangeAccountOwnerUtils
    {
        public bool ITGetChangeOwnerLog(UserConnection userConnection)
        {
            List<ITChangeOwnerLog> changeOwnerLog = GetChangeOwnerLog(userConnection);
            foreach (ITChangeOwnerLog item in changeOwnerLog) {
            	try{
                if (item.ITChanged != true && item.ITChangeFrom.Date <= DateTime.Now.Date)
                {
                	var itAccountUtils = new ITAccountUtils(userConnection);
                    List<ChangeExecutiveLog> result = itAccountUtils.ChangeAccountAndRelatedEntitiesOwner(item.ITNewOwnerId, item.ITOldOwnerId,
                        item.ITLeadTypeId, item.ITChangeOpportunityOwnerFlag, item.ITChangeOwnerSupplierFlag);
                    ITContact newOwner = GetContactName(item.ITNewOwnerId, userConnection);
                    ITContact oldOwner = GetContactName(item.ITOldOwnerId, userConnection);
                    item.ITChanged = true;
                    UpdateChangeOwnerLog(item, "ITChanged", userConnection);
                    string message = "Вы назначены замещающим по клиентам вместо " + oldOwner.Name + " до " + item.ITChangeUntilDate + ".";
                    InsertReminding(newOwner.Id, message, userConnection);
                }
            	}
            	catch(Exception ex)
            	{
            		var insert = new Insert(userConnection).Into("AcademyURL")
                .Set("Id", Column.Parameter(Guid.NewGuid()))
                .Set("Name", Column.Parameter("Error"))
                .Set("Description", Column.Parameter(ex.Message))
                .Set("ProcessListeners", Column.Parameter(0))
                ;

                insert.Execute();
            	}
            }
            foreach (ITChangeOwnerLog item in changeOwnerLog)
            {
                if (item.ITComeback != true && item.ITChangeUntilDate.Date <= DateTime.Now.Date)
                {
                    var itAccountUtils = new ITAccountUtils(userConnection);
                    List<ChangeExecutiveLog> result = itAccountUtils.ChangeAccountAndRelatedEntitiesOwner(item.ITOldOwnerId, item.ITNewOwnerId,
                        item.ITLeadTypeId, item.ITChangeOpportunityOwnerFlag, item.ITChangeOwnerSupplierFlag);
                    ITContact newOwner = GetContactName(item.ITOldOwnerId, userConnection);
                    ITContact oldOwner = GetContactName(item.ITNewOwnerId, userConnection);
                    item.ITComeback = true;
                    UpdateChangeOwnerLog(item, "ITComeback", userConnection);
                    string message = "Срок замещения сотрудника" + newOwner.Name + " истек. Работа с соответствующими клиентами передана данному сотруднику.";
                    InsertReminding(oldOwner.Id, message, userConnection);
                }
            }
            return changeOwnerLog.Count>0;
        }
	    private List<ITChangeOwnerLog> GetChangeOwnerLog( UserConnection userConnection)
	    {
	        List<ITChangeOwnerLog> result = new List<ITChangeOwnerLog>();
	        try
	        {
	            var selectITChangedOwnerLog = new Select(userConnection)
	                .Column("Id")
	                .Column("ITOldOwnerId")
	                .Column("ITNewOwnerId")
	                .Column("ITChangeOpportunityOwnerFlag")
	                .Column("ITChangeOwnerSupplierFlag")
	                .Column("ITLeadTypeId")
	                .Column("ITChangeUntilDate")
	                .Column("ITChangeFrom")
	                .Column("ITChanged")
	                .Column("ITComeback")
	                .From("ITChangeOwnerLog")
                    .Where("ITChanged").IsEqual(Column.Parameter((bool)false))
                    .Or("ITComeback").IsEqual(Column.Parameter((bool)false))
                    as Select;
	            using (DBExecutor executor = userConnection.EnsureDBConnection())
	            {
	                using (IDataReader reader = selectITChangedOwnerLog.ExecuteReader(executor))
	                {
	                    while (reader.Read())
	                    {
	                        var id = reader.GetColumnValue<Guid>("Id");
	                        var itOldOwner = reader.GetColumnValue<Guid>("ITOldOwnerId");
	                        var itNewOwner = reader.GetColumnValue<Guid>("ITNewOwnerId");
	                        var itChangeOpportunityOwnerFlag = reader.GetColumnValue<bool>("ITChangeOpportunityOwnerFlag");
	                        var itChangeOwnerSupplierFlag = reader.GetColumnValue<bool>("ITChangeOwnerSupplierFlag");
	                        var itLeadTypeId = reader.GetColumnValue<Guid>("ITLeadTypeId");
	                        var itChangeUntilDate = reader.GetColumnValue<DateTime>("ITChangeUntilDate");
                            //var itChangeUntilDate = Convert.ToDateTime(reader["ITChangeUntilDate"]);
                                //reader.GetColumnValue<DateTime>("ITChangeUntilDate");
	                        var itChangeFrom = reader.GetColumnValue<DateTime>("ITChangeFrom");
                            var itChanged = reader.GetColumnValue<bool>("ITChanged");
	                        var itComeback = reader.GetColumnValue<bool>("ITComeback");
	
	                        ITChangeOwnerLog item = new ITChangeOwnerLog(
	                            id, itOldOwner, itNewOwner, itChangeOpportunityOwnerFlag, itChangeOwnerSupplierFlag, itLeadTypeId,
	                            itChangeUntilDate, itChangeFrom, itChanged, itComeback);
	                        result.Add(item);
	                    }
	                }
	            }
	        }
	        catch (Exception ex)
	        {
                //Trace.WriteLine("Exception - [{0}]", ex);
	            throw new Exception("Error: private List<ITChangeOwnerLog> GetChangeOwnerLog(UserConnection userConnection)");
	        }
	        return result;
	    }
        private ITContact GetContactName(Guid contactId, UserConnection userConnection)
        {
            try
            {
            	ITContact contact = null;
                var selectITChangedOwnerLog = new Select(userConnection)
                    .Column("Id")
                    .Column("Name")
                    .From("Contact")
                    .Where("Id").IsEqual(Column.Parameter(contactId))
                    as Select;
                using (DBExecutor executor = userConnection.EnsureDBConnection())
                {
                    using (IDataReader reader = selectITChangedOwnerLog.ExecuteReader(executor))
                    {
                        while (reader.Read())
                        {
                            var id = reader.GetColumnValue<Guid>("Id");
                            var name = reader.GetColumnValue<string>("Name");

                            contact = new ITContact(id, name);
                        }
                    }
                }
                return contact;
            }
            catch (Exception ex)
            {
                //Trace.WriteLine("Exception - [{0}]", ex);
                throw new Exception("Error: private List<ITName> GetContactName(UserConnection userConnection)");
            }
        }
        private void UpdateChangeOwnerLog(ITChangeOwnerLog itChangedLogRow, string changedColumn, UserConnection userConnection)
        {
            if (changedColumn == "ITChanged")
            { 
            	var updateChangeOwnerLog = new Update(userConnection, "ITChangeOwnerLog")
	                .Set("ITChanged", Column.Parameter(itChangedLogRow.ITChanged))
	                .Where("Id")
	                .IsEqual(Column.Parameter(itChangedLogRow.Id));
	                updateChangeOwnerLog.Execute();
            }
            if (changedColumn == "ITComeback")
            {
                var updateChangeOwnerLog = new Update(userConnection, "ITChangeOwnerLog")
                    .Set("ITComeback", Column.Parameter(itChangedLogRow.ITChanged))
                    .Where("Id")
                    .IsEqual(Column.Parameter(itChangedLogRow.Id));
                    updateChangeOwnerLog.Execute();
            }
            //updateChangeOwnerLog.Execute();
        }
        private void InsertReminding(Guid SubjectId,string message, UserConnection userConnection)
        {
            Guid SourceId = Guid.Parse("A66D08E1-2E2D-E011-AC0A-00155D043205");
            var id = Guid.NewGuid();
            var idstr = id.ToString();
            var insertReminding = new Insert(userConnection).Into("Reminding")
                .Set("Id", Column.Parameter((Guid)id))
                .Set("AuthorId", Column.Parameter(userConnection.CurrentUser.ContactId))
                .Set("ContactId", Column.Parameter((Guid)SubjectId))
                .Set("SourceId", Column.Parameter((Guid)SourceId))
                .Set("RemindTime", Column.Parameter((DateTime)DateTime.Now))
                .Set("Description", Column.Parameter((string)"CheckTest1"))
                .Set("SubjectCaption", Column.Parameter((string)message))
                //.Set("SysEntitySchemaId", Column.Parameter((Guid)" "))
                .Set("SubjectId", Column.Parameter((Guid)SubjectId));
            try
            {
                insertReminding.Execute();
            }
            catch (Exception ex)
            {
                throw new Exception("MessageId");
            }
        }
    }
    

    public class ITChangeOwnerLog
    {
        public Guid Id { get; set; }
        public Guid ITOldOwnerId { get; set; }
        public Guid ITNewOwnerId { get; set; }
        public bool ITChangeOpportunityOwnerFlag { get; set; }
        public bool ITChangeOwnerSupplierFlag { get; set; }
        public Guid ITLeadTypeId { get; set; }
        public DateTime ITChangeUntilDate { get; set; }
        public DateTime ITChangeFrom { get; set; }
        public bool ITChanged { get; set; }
        public bool ITComeback { get; set; }

        public ITChangeOwnerLog(Guid id, Guid itOldOwner, Guid itNewOwner, bool itChangeOpportunityOwnerFlag,
            bool itChangeOwnerSupplierFlag, Guid itLeadTypeId, DateTime itChangeUntilDate, DateTime itChangeFrom,
            bool itChanged, bool itComeback)
        {
            this.Id = id;
            this.ITOldOwnerId = itOldOwner;
            this.ITNewOwnerId = itNewOwner;
            this.ITChangeOpportunityOwnerFlag = itChangeOpportunityOwnerFlag;
            this.ITChangeOwnerSupplierFlag = itChangeOwnerSupplierFlag;
            this.ITLeadTypeId = itLeadTypeId;
            this.ITChangeUntilDate = itChangeUntilDate;
            this.ITChangeFrom = itChangeFrom;
            this.ITChanged = itChanged;
            this.ITComeback = itComeback;
        }
    }
    public class ITContact
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public ITContact(Guid id, string name)
        {
            this.Id = id;
            this.Name = name;
        }
    }
}

