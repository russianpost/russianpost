define("ITClientPrePage", ["CustomProcessPageV2Utilities", "TsJsConsts", "ITJsConsts"],
	function(CustomProcessPageV2Utilities, TsJsConsts, ITJsConsts) {
		return {
			mixins: {
				BaseProcessViewModel: "Terrasoft.CustomProcessPageV2Utilities"
			},

			attributes: {
				//Атрибут с фильтрами: 1) Филиал текущего пользователя; 2) Контакты из Блока печатных сервисов
				"GetOwner": {
					"dataValueType": Terrasoft.DataValueType.LOOKUP,
					"isRequired": true,
					"referenceSchemaName": "Contact",
					"lookupListConfig": {
						"filters": [
							function() {
								var filterGroup = this.Ext.create("Terrasoft.FilterGroup");
								filterGroup.logicalOperation = Terrasoft.core.enums.LogicalOperatorType.AND;
								var TsAccount = this.Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "TsAccount",
									this.currentTsAccount);
								var ITMacroregion =
								this.Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "ITMacroregion",
									this.currentMacroregion);
								var ByTSLeadType = this.Terrasoft.createColumnInFilterWithParameters("Id", this.mass);
								filterGroup.add("TsAccount", TsAccount);
								filterGroup.add("ITMacroregion", ITMacroregion);
								filterGroup.add("ByTSLeadType", ByTSLeadType);
								//Фильтры включаются в зависимости от результата запроса
								TsAccount.isEnabled = this.TsAccountEnabled;
								ITMacroregion.isEnabled = this.ITMacroregionEnabled;
								ByTSLeadType.isEnabled = true;
								return filterGroup;
							}
						]
					}
				}
			},

			rules: {},

			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,

			mass: [],

			methods: {
				onEntityInitialized: function() {
					this.callParent(arguments);
					//Запросы для получения значений для фильтрации атрибута
					var massive = [], currentUserContactId = Terrasoft.SysValue.CURRENT_USER_CONTACT.value;
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "ITContactLeadType"
					});
					esq.addColumn("ITContact", "ITContact");
					esq.addColumn("ITLeadType", "ITLeadType");
					esq.filters.logicalOperation = Terrasoft.LogicalOperatorType.AND;
					//Фильтр по Id Блока печатных сервисов
					esq.filters.add("esqFirstFilter", esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
						"ITLeadType", "68e280d1-186c-4cb9-9429-e066d201f9b6"));
					esq.getEntityCollection(function(result1) {
						if (!result1.success) {
							return;
						}
						result1.collection.each(function(item) {
							massive.push(item.get("ITContact").value);
						});
						this.mass = massive;
						
						//Фильтр по Филиалу текущего пользователя или по Макрорегиону, если Филиала у пользователя нет
						var primary = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Contact"});
						primary.addColumn("TsAccount", "TsAccount");
						primary.getEntity(currentUserContactId, function(result2) {
							if (result2.entity.get("TsAccount")) {
								this.currentTsAccount = result2.entity.get("TsAccount").value;
								this.TsAccountEnabled = true;
								this.ITMacroregionEnabled = false;
							} else {
								this.TsAccountEnabled = false;
								this.ITMacroregionEnabled = true;
								var macro = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Contact"});
								macro.addColumn("ITMacroregion", "ITMacroregion");
								macro.getEntity(currentUserContactId, function(result3) {
									this.currentMacroregion = result3.entity.get("ITMacroregion").value;
								}, this);
							}
						}, this);
					}, this);
				},

				//Возвращаем заголовок преднастроенной страницы
				getHeader: function() {
					return this.get("Resources.Strings.PageHeaderCaption");
				},

				//Переопределение метода для прорисовки caption страницы
				getPageHeaderCaption: function() {
					var pageName = this.name;
					var header = pageName === "ITClientPrePage" ? this.getHeader() : "";
					return header;
				},

				initHeaderCaption: Ext.emptyFn,

				initPrintButtonMenu: Ext.emptyFn,

				loadVocabulary: function(args, tag) {
					var column = this.getColumnByName(tag);
					args.schemaName = column.referenceSchemaName;
					this.callParent(arguments);
				},

				getLookupPageConfig: function(args, columnName) {
					var config = {
						entitySchemaName: args.schemaName,
						multiSelect: false,
						columnName: columnName,
						columnValue: this.get(columnName),
						searchValue: args.searchValue,
						filters: this.getLookupQueryFilters(columnName)
					};
					this.Ext.apply(config, this.getLookupListConfig(columnName));
					return config;
				},

				onCloseCardButtonClick: function() {
					this.sandbox.publish("BackHistoryState");
				},

				//При клике на кнопку "Выбор"
				onNextButtonClick: function() {
					var value = this.get("GetOwner"),
						error = "Сначала выберите ответственного по продаже";
					switch (!value || value === this.Terrasoft.GUID_EMPTY) {
						case false:
							this.acceptProcessElement("NextButton");
							break;
						default:
							this.showInformationDialog(error);
					}
					
				},

	/*			onLookupDataLoaded: function(config) {
					return;
					this.callParent(arguments);
					config.isLookupEdit = true;
					this.mixins.LookupQuickAddMixin.onLookupDataLoaded.call(this, config);
				}*/

				getNewListItemConfig: function() {
					return;
				}
			},

			diff: /**SCHEMA_DIFF*/[
				// Удалить лишние кнопки
	/*			{
					operation: "remove",
					name: "ActionButtonsContainer"
				},*/
				{
					operation: "remove",
					name: "DiscardChangesButton"
				},
				{
					operation: "remove",
					name: "CloseButton"
				},
	/*			{
					"operation": "remove",
					"name": "Tabs"
				},*/
				{
					operation: "remove",
					name: "DelayExecutionButton"
				},
				{
					operation: "remove",
					name: "ViewOptionsButton"
				},
				{
					operation: "remove",
					name: "SaveButton"
				},
				{
					"operation": "remove",
					"name": "actions"
				},
				{
					"operation": "insert",
					"parentName": "LeftContainer",
					"propertyName": "items",
					"name": "NextButton",
					"values": {
						"caption": {bindTo: "Resources.Strings.NextButtonCaption"},
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"classes": {textClass: "actions-button-margin-right"},
						"style": Terrasoft.controls.ButtonEnums.style.GREEN,
						"click": {bindTo: "onNextButtonClick"}
	//					"layout": {column: 11, row: 1, colSpan: 5}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "GetOwner",
					"values": {
						"caption": {bindTo: "Resources.Strings.GetOwnerCaption"},
						"contentType": Terrasoft.ContentType.LOOKUP,
						"bindTo": "GetOwner",
						"layout": {column: 0, row: 1, colSpan: 9}
	/*					"controlConfig": {
							"keydown": {
								"bindTo": "onKeyDownAction"
							}
						}*/
					}
				}
			]/**SCHEMA_DIFF*/,

			userCode: {}
		};
	});