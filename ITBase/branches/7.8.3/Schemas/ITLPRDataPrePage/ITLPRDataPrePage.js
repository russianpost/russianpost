define("ITLPRDataPrePage", ["BusinessRuleModule", "ConfigurationEnums", "CustomProcessPageV2Utilities"],
	function(BusinessRuleModule, ConfigurationEnums, CustomProcessPageV2Utilities) {
		return {

			entitySchemaName: "OpportunityContact",

			mixins: {
				BaseProcessViewModel: "Terrasoft.CustomProcessPageV2Utilities"
			},
	
			attributes: {
				/*"Contact": {
					"lookupListConfig": {
						"hideActions" : true
					}
				},*/

				"ContactLoyality": {
					lookupListConfig: {
						orders: [
							{
								columnPath: "Position",
								orderDirection: Terrasoft.OrderDirection.ASC
							}
						]
					}
				},

				"Role": {
					"onChange": "setInfluence"
				},

				"IsSaveButtonPressed": {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				}
			},

			rules: {
				"Contact": {
					"ContactByAccount": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": true,
						"autoClean": true,
						"baseAttributePatch": "Account",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
						"attribute": "ITAccountId"
					}
				}
			},

			methods: {
				onEntityInitialized: function() {
					this.setDataOnInitialized();
					this.callParent(arguments);
					this.console.log("OpportunityId " + this.get("ITOpportunityId"));
					this.console.log("OpportunityContactID " + this.get("Id"));
				},

				setDataOnInitialized: function() {
					this.setOpportunityOnInitialized();
					this.setRoleOnInitialized();
				},

				setOpportunityOnInitialized: function() {
					var recordId = this.get("ITOpportunityId");
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Opportunity"
					});
					esq.addColumn("Title");
					esq.getEntity(recordId, function(response) {
						if (response.success && response.entity) {
							var oppConfig = {
								displayValue: response.entity.get("Title"),
								value: recordId
							};
							this.set("Opportunity", oppConfig);
						}
					}, this);
				},

				//Установка ЛПР роли по умолчанию
				setRoleOnInitialized: function() {
					var roleConfig = {
						value: "82ecb430-8d6b-40ea-8c01-741fe82cdcbd",
						displayValue: "Лицо, принимающее решения"
					};
					this.set("Role", roleConfig);
				},

				// Выставляет по умолчанию высокую степень влияния для ЛПР.
				setInfluence: function() {
					var role = this.get("Role");
					var influence = this.get("Influence");
					var infHigh = {value: "cb35dec5-59f5-465c-a985-99f980079853", primaryImageVisible: false,
						displayValue: "Высокая", customHtml: "Высокая", markerValue: "Высокая"};
					if (role && role.value === "82ecb430-8d6b-40ea-8c01-741fe82cdcbd" && !influence) {
						this.set("Influence", infHigh);
					}
				},

				onCloseCardButtonClick: function() {
					this.sandbox.publish("BackHistoryState");
				},

				onNextButtonClick: function() {
					this.acceptProcessElement("NextButton");
				},

				//Флажок нажатия на кнопку сохранить
				onSaveButtonPressed: function() {
					this.set("IsSaveButtonPressed", true);
					this.save();
				},

				//Переопределен для проверки события нажатия кнопки Сохранить
				onSaved: function(response, config) {
					this.hideBodyMask();
					if (!this.get("NextPrcElReady")) {
						this.set("NextPrcElReady", response.nextPrcElReady);
					}
					if (config && config.isSilent) {
						this.onSilentSaved(response, config);
					} else {
						var updateConfig = this.getUpdateDetailOnSavedConfig();
						this.sandbox.publish("UpdateDetail", updateConfig, [this.sandbox.id]);
						this.sendSaveCardModuleResponse(response.success);
						if (this.get("IsInChain")) {
							this.onProcessCardSaved();
							return;
						}
						if (this.get("IsSaveButtonPressed")) {
							this.updateIsMainContact();
							this.onNextButtonClick();
						}
					}
					this.set("Operation", Terrasoft.ConfigurationEnums.CardOperation.EDIT);
					if (!this.destroyed) {
						this.updateButtonsVisibility(false, {force: true});
					}
					this.set("IsChanged", this.isChanged());
				},

				//Почему то после сохранения карточки значение колонки "Основной контакт"
				//устанавливалось как false, хз почему, так что заплатка
				updateIsMainContact: function() {
					var update = this.Ext.create("Terrasoft.UpdateQuery", {
						rootSchemaName: "OpportunityContact"
					});
					update.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Id", this.get("Id")));
					update.setParameterValue("IsMainContact", this.get("IsMainContact"), this.Terrasoft.DataValueType.BOOLEAN);
					update.execute();
				}

			},

			details: /**SCHEMA_DETAILS*/{
				OppContactMotivatorsDetailV2: {
					schemaName: "OppContactMotivatorsDetailV2",
					"entitySchemaName": "OppContactMotivator",
					filter: {
						masterColumn: "Id",
						detailColumn: "OpportunityContact"
					}
				}
			}/**SCHEMA_DETAILS*/,

			diff: /**SCHEMA_DIFF*/[
				/*{
					"operation": "insert",
					"name": "GeneralInfoTab",
					"parentName": "Tabs",
					"propertyName": "tabs",
					"values": {
						"caption": "Мотиваторы контакта в продаже",
						"items": []
					}
				},*/

				{
					"operation": "insert",
					"name": "OppContactMotivatorsDetailV2",
					"parentName": "DetailContainer",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.DETAIL,
						"markerValue": "added-detail"
					},
					"index": 1
				},

				//CONTAINER
				{
					"operation": "insert",
					"name": "DetailContainer",
					"values": {
						"layout": {"column": 0, "row": 0, "colSpan": 24},
		//				"caption": {"bindTo": "Resources.Strings.form7pCaption"},
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"markerValue": "added-group",
						"items": []
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 0
				},

				//GRID_LAYOUT
				{
					"operation": "insert",
					"name": "DetailGroupContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
						"markerValue": "added-group",
						"items": []
					},
					"parentName": "DetailContainer",
					"propertyName": "items",
					"index": 0
				},

				{
					"operation": "insert",
					"parentName": "DetailGroupContainer",
					"propertyName": "items",
					"name": "Opportunity",
					"values": {
						"layout": {"column": 0, "row": 0, "colSpan": 10},
						"controlConfig": {"enabled": false}
					}
				},
				{
					"operation": "insert",
					"parentName": "DetailGroupContainer",
					"propertyName": "items",
					"name": "Contact",
					"values": {
						"layout": {"column": 0, "row": 1, "colSpan": 10}
					}
				},

				{
					"operation": "insert",
					"parentName": "DetailGroupContainer",
					"propertyName": "items",
					"name": "IsMainContact",
					"values": {
						"layout": {"column": 10, "row": 1, "colSpan": 10}
					}
				},

				{
					"operation": "insert",
					"parentName": "DetailGroupContainer",
					"propertyName": "items",
					"name": "Role",
					"values": {
						"layout": {"column": 0, "row": 2, "colSpan": 10},
						"contentType": this.Terrasoft.ContentType.ENUM,
						"controlConfig": {"enabled": false}
					}
				},

				{
					"operation": "insert",
					"parentName": "DetailGroupContainer",
					"propertyName": "items",
					"name": "Influence",
					"values": {
						"layout": {"column": 10, "row": 2, "colSpan": 10},
						"contentType": this.Terrasoft.ContentType.ENUM
					}
				},
				{
					"operation": "insert",
					"parentName": "DetailGroupContainer",
					"propertyName": "items",
					"name": "ContactLoyality",
					"values": {
						"layout": {"column": 0, "row": 3, "colSpan": 10},
						"contentType": this.Terrasoft.ContentType.ENUM
					}
				},
				{
					"operation": "insert",
					"parentName": "DetailGroupContainer",
					"propertyName": "items",
					"name": "Description",
					"values": {
						"layout": {"column": 10, "row": 3, "colSpan": 10}
					}
				},
				{
					"operation": "insert",
					"parentName": "DetailGroupContainer",
					"propertyName": "items",
					"name": "TsResponsibilityField",
					"values": {
						"layout": {"column": 0, "row": 4, "colSpan": 10},
						"contentType": this.Terrasoft.ContentType.ENUM
					}
				},

				{
					"operation": "merge",
					"name": "CloseButton",
					"values": {
						"style": Terrasoft.controls.ButtonEnums.style.RED,
						"click": {"bindTo": "onCloseCardButtonClick"},
						"visible": true
					}
				},

				{
					"operation": "merge",
					"name": "SaveButton",
					"values": {
						"click": {"bindTo": "onSaveButtonPressed"},
						"visible": true
					}
				},

				//Удаление лишних элементов
				{
					"operation": "remove",
					"name": "DiscardChangesButton"
				},

				{
					"operation": "remove",
					"name": "DelayExecutionButton"
				},

				{
					"operation": "remove",
					"name": "ViewOptionsButton"
				},

				{
					"operation": "remove",
					"name": "actions"
				}
			]/**SCHEMA_DIFF*/
		};
	});
