define("UserPageV2", [],
	function() {
		return {
			methods: {
				onEntityInitialized: function() {
					this.callParent(arguments);
					if (this.isAddMode() || this.isCopyMode()) {
						this.setDefaultHomePage();
					}
				},
				setDefaultHomePage: function() {
					this.set("HomePage", this.Terrasoft.SysSettings.cachedSettings.DefaultHomePage);
				}
			}
		};
	});