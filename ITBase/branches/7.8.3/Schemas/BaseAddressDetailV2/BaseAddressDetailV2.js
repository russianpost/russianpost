define("BaseAddressDetailV2", [],
function() {
	return {
		messages: {
		},
		attributes: {
			
		},
		methods: {
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "AddTypedRecordButton",
				"values": {
					"click": {"bindTo": "addRecord"},
					"menu": {}
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
