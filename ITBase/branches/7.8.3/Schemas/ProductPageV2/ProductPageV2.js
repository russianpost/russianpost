define("ProductPageV2", [], function() {
	return {
		entitySchemaName: "Product",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			//Удаление детали "Единицы измерения"
			{
				"operation": "remove",
				"name": "ProductUnitDetail"
			},
			//Удаление вкладки "Характеристики"
			{
				"operation": "remove",
				"name": "ProductSpecificationTab"
			},
			//Удаление детали "Характеристики" из вкладки "Характеристики"
			{
				"operation": "remove",
				"name": "ProductSpecificationDetail"
			},
			
			{
				"operation": "merge",
				"name": "Name",
				"values": {
					"layout": {
						"colSpan": 21,
						"rowSpan": 1,
						"column": 3,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "TradeMark",
				"values": {
					"labelConfig": {},
					"enabled": true,
					"contentType": 5
				}
			},
			{
				"operation": "move",
				"name": "TsOwnerDivision",
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "move",
				"name": "TsKAU",
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "move",
				"name": "TsProductActivityType",
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "move",
				"name": "TsIncludedInPacket",
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 13
			},
			{
				"operation": "move",
				"name": "TsIsObjectWithTariffication",
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 12
			},
			{
				"operation": "insert",
				"name": "ITTariffCode65db561f-501a-4b61-90de-042aabdaffd3",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 7,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "ITTariffCode"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 15
			}
			
		]/**SCHEMA_DIFF*/,
		methods: {},
		rules: {}
	};
});
