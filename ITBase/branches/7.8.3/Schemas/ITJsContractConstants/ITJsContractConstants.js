define("ITJsContractConstants", [], function() {

	//Способ закупки
	var purchaseMethod = {
		SingleSupplier: "20bdf777-8962-4130-8382-b6f257e1aef0",
		Auction: "e01f3d59-1d67-4af5-b1ea-f0147b1a5ccc",
		Other: "b91bc40e-bc7a-4e1a-9cee-05b450c76020"
	};

	//ITDocumentType
	var documentType = {
		Contract: "7cbaae95-4428-4a00-8255-3b5e4c514045",
		Agreement: "98141efa-4cab-4f61-8fd7-fa0dea03fd7e",
		Protocol: "a64fce93-2407-4312-a9bd-b1df40fa4c2e"
	};

	//ITContractType
	var contractType = {
		//Срочный, с указанием суммы
		ExpressTypedId: "e648b7cb-3a6e-47e2-a733-387afa25adcb",
		//С автоматической пролонгацией
		WithAutoProlongationTypeId: "fbf4b486-cf9b-4171-b694-fa7eddfaa07e",
		//Бессрочный
		UnlimitedId: "963a924e-d707-4c49-995b-0ed592be2db1",
		//С лимитом по сумме
		WithAmountLimit: "b62e749c-1200-4aad-b294-d061e7acd23b"
	};

	var paymentForm = {
		PrePayment: "bfc1f2a9-f6a0-46c5-bf96-4dd9a2b1bc6f",
		PostPay: "e9c9b181-a76e-43fc-9c32-74a8b0959c45"
	};

	return {
		PurchaseMethod: purchaseMethod,
		DocumentType: documentType,
		ContractType: contractType,
		PaymentForm: paymentForm
	};
});
