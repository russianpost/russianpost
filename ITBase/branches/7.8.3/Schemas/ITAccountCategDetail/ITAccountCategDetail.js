define("ITAccountCategDetail", ["ConfigurationGrid", "ConfigurationGridGenerator", "ConfigurationGridUtilities"],
	function() {
	return {
		messages: {
		},
		// Название схемы объекта детали.
		entitySchemaName: "ITVwAccountCateg",
		// Перечень атрибутов схемы.
		attributes: {
			// Признак возможности редактирования.
			"IsEditable": {
				// Тип данных — логический.
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				// Тип атрибута — виртуальная колонка модели представления.
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				// Устанавливаемое значение.
				value: true
			}
		},
		diff: [
			{
				// Тип операции — слияние.
				"operation": "merge",
				// Название элемента схемы, над которым производится действие.
				"name": "DataGrid",
				// Объект, свойства которого будут объединены со свойствами элемента схемы.
				"values": {
					// Имя класса
					"className": "Terrasoft.ConfigurationGrid",
					// Генератор представления должен генерировать только часть представления.
					"generator": "ConfigurationGridGenerator.generatePartial",
					// Привязка события получения конфигурации элементов редактирования
					// активной строки к методу-обработчику.
					"generateControlsConfig": {"bindTo": "generatActiveRowControlsConfig"},
					// Привязка события смены активной записи к методу-обработчику.
					"changeRow": {"bindTo": "changeRow"},
					// Привязка события отмены выбора записи к методу-обработчику.
					"unSelectRow": {"bindTo": "unSelectRow"},
					// Привязка  события клика на реестре к методу-обработчику.
					"onGridClick": {"bindTo": "onGridClick"},
					// Действия, производимые с активной записью.
					"activeRowActions": [
						// Настройка действия [Сохранить].
						{
							// Имя класса элемента управления, с которым связано действие.
							"className": "Terrasoft.Button",
							// Стиль отображения — прозрачная кнопка.
							"style": this.Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
							// Тег.
							"tag": "save",
							// Значение маркера.
							"markerValue": "save",
							// Привязка к изображению кнопки.
							"imageConfig": {"bindTo": "Resources.Images.SaveIcon"}
						},
						// Настройка действия [Отменить].
						{
							"className": "Terrasoft.Button",
							"style": this.Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
							"tag": "cancel",
							"markerValue": "cancel",
							"imageConfig": {"bindTo": "Resources.Images.CancelIcon"}
						}
					],
					// Привязка к методу, который инициализирует подписку на события
					// нажатия кнопок в активной строке.
					"initActiveRowKeyMap": {"bindTo": "initActiveRowKeyMap"},
					// Привязка события выполнения действия активной записи к методу-обработчику.
					"activeRowAction": {"bindTo": "onActiveRowAction"},
					// Признак возможности выбора нескольких записей.
					"multiSelect": false,
					"isPageable" : false
				}
			}
		],
		// Используемые классы с дополнительной функциональностью.
		mixins: {
			ConfigurationGridUtilites: "Terrasoft.ConfigurationGridUtilities"
		},
		methods: {
			init: function() {
				this.callParent(arguments);
				this.systemColumns.push("ITLeadType");
			},
			fillParams: function(row, query) {
				if (row.changedValues.ITAccountLevel) {
					query.setParameterValue("ITAccountLevel", row.changedValues.ITAccountLevel.value,
						this.Terrasoft.DataValueType.GUID);
				}
				if (row.changedValues.ITCateg) {
					query.setParameterValue("ITCateg", row.changedValues.ITCateg.value, this.Terrasoft.DataValueType.GUID);
				}
				if (row.changedValues.ITB2B) {
					query.setParameterValue("ITB2B", row.changedValues.ITB2B ? 1:0, this.Terrasoft.DataValueType.BOOLEAN);
				}
				if (row.changedValues.ITB2C) {
					query.setParameterValue("ITB2C", row.changedValues.ITB2C ? 1:0, this.Terrasoft.DataValueType.BOOLEAN);
				}
				if (row.changedValues.ITG2B) {
					query.setParameterValue("ITG2B", row.changedValues.ITG2B ? 1:0, this.Terrasoft.DataValueType.BOOLEAN);
				}
				if (row.changedValues.ITVIP) {
					query.setParameterValue("ITVIP", row.changedValues.ITVIP ? 1:0, this.Terrasoft.DataValueType.BOOLEAN);
				}
			},
			insertRow: function(row) {
				var insert = this.Ext.create("Terrasoft.InsertQuery", {rootSchemaName: "ITAccountCateg"});
				insert.setParameterValue("Id", row.values.Id, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("ITAccount", row.values.ITAccount.value, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("ITLeadType", row.values.ITLeadType.value, this.Terrasoft.DataValueType.GUID);
				this.fillParams(row, insert);
				//row.set("ITSaved", true);
				insert.execute();
			},
			updateRow: function(row) {
				var update = this.Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "ITAccountCateg"});
				var filterId = update.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Id", row.values.Id);
				update.filters.add("filterId", filterId);
				this.fillParams(row, update);
				update.execute();
			},
			defaultSaveRowChanges: function(row, callback, scope) {
				scope = scope || this;
				callback = callback || this.Terrasoft.emptyFn;
				if (row && this.getIsRowChanged(row)) {
					row.save({
						callback: callback,
						isSilent: true,
						scope: scope
					});
				} else {
					callback.call(scope);
				}
			},
			saveRowChanges: function(row, callback, scope) {
				scope = scope || this;
				callback = callback || this.Terrasoft.emptyFn;
				if (row && this.getIsRowChanged(row)) {
					if (row.values.ITSaved) {
						this.updateRow(row);
					} else {
						this.insertRow(row);
					}
					row.changedValues = {IsChanged: false};
					
					row.save({
						callback: callback,
						isSilent: true,
						scope: scope
					});
					row.values.ITSaved = true;
				}
				callback.call(scope);
			},
			addToolsButtonMenuItems: function(toolsButtonMenu) {
				this.addGridOperationsMenuItems(toolsButtonMenu);
			}
		}
	};
});