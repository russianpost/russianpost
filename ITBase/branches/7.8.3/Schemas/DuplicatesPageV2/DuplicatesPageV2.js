//#RPCRM-735 добавлена страница дублей в пакет ITBase
define("DuplicatesPageV2", [
	"AcademyUtilities", "SchemaBuilderV2", "ViewGeneratorV2", "ServiceHelper",
	"DuplicatesPageV2Resources", "DuplicatesMergeHelper", "GridUtilitiesV2", "MultilineLabel",
	"DuplicatesDetailModuleV2", "ImageCustomGeneratorV2", "css!MultilineLabel", "css!DeduplicationModuleCSS",
	"css!SectionModuleV2"
],
function(AcademyUtilities, SchemaBuilder, ViewGenerator, ServiceHelper) {
	return {
		entitySchemaName: "DuplicatesRule",
		mixins: {},
		attributes: {
			"NewAccount": {
				"value": false,
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			"NewAccountData": {
				"dataValueType": this.Terrasoft.DataValueType.COLLECTION
			},
			"ParsedResult": {
				"dataValueType": this.Terrasoft.DataValueType.COLLECTION
			},
			"ActionsInNewAccount": {
				"value": true,
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			}
		},
		messages: {
			"NewAccountToMiniPage": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		methods: {
			init: function() {
				if (this.values.Operation === "Account") {
					this.searchNewAccount();
				}
				this.callParent(arguments);
			},
			searchNewAccount: function() {
				var currentContactId = Terrasoft.core.enums.SysValue.CURRENT_USER_CONTACT.value;
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "ITAccountNew"
				});
				esq.addColumn("ITName", "Name");
				esq.addColumn("ITPrimaryContact", "PrimaryContact");
				esq.addColumn("ITPhone", "Phone");
				esq.addColumn("ITWeb", "Web");
				esq.addColumn("ITOwnership", "Ownership");
				esq.addColumn("ITInn", "TsINN");
				esq.addColumn("ITKpp", "TsKPP");
				esq.addColumn("ITCurrentContactId.TsTarifficatorRegion", "TsTarifficatorRegion");
				var esqFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"ITCurrentContactId", currentContactId);
				esq.filters.add("esqFilter", esqFilter);
				esq.getEntityCollection(function(result) {
					if (!result.success) {
						this.showInformationDialog("Ошибка запроса данных");
						return;
					}
					if (result.collection.collection.length === 1) {
						this.set("NewAccount", true);
						this.set("ActionsInNewAccount", false);
						var accountData = result.collection.collection.items[0].values;
						this.set("NewAccountData", accountData);
						this.getDeduplicationResultsNewAccount();
						this.sandbox.subscribe("NewAccountToMiniPage", function() {
							return accountData;
						}, this, []);
					}
				}, this);
			},
			getDeduplicationResultsCallback: function(response) {
				response = response || {};
				var responseResult = response.GetDeduplicationResultsResult;
				var parsedResult = JSON.parse(responseResult);
				if (this.get("NewAccount")) {
					var rows = this.get("ParsedResult");
					parsedResult.groups[0].rows.length = 0;
					parsedResult.groups[0].rows = rows;
					parsedResult.groups.length = 1;
					parsedResult.nextOffset = -1;
				}
				this.set("NextDuplicatesGroupOffset", parsedResult.nextOffset);
				var groups = parsedResult.groups || [];
				if (Ext.isEmpty(groups) === false) {
					this.prepareRowConfig(parsedResult.rowConfig);
					this.loadDuplicatesDetails(groups, parsedResult.rowConfig);
				}
				var duplicatesContainerItems = this.get("DuplicatesContainerItems");
				var isEmpty = duplicatesContainerItems.getCount() === 0;
				this.set("IsDuplicatesContainerItemsEmpty", isEmpty);
			},
			getDeduplicationResultsNewAccount: function() {
				var currentContact = Terrasoft.core.enums.SysValue.CURRENT_USER_CONTACT.value;
				var esqGroups = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "ITSearchNewAccount"
				});
				esqGroups.addColumn("ITAccount.Id", "AccountId");
				esqGroups.addColumn("ITName", "Name");
				esqGroups.addColumn("ITInn", "TsINN");
				esqGroups.addColumn("ITKpp", "TsKPP");
				esqGroups.addColumn("ITAccount.TsTarifficatorRegion", "TsTarifficatorRegion");
				esqGroups.addColumn("ITAccount.Type", "Type");
				var esqFilter = esqGroups.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
				"ITCurrentContact", currentContact);
				esqGroups.filters.add("esqFilter", esqFilter);
				esqGroups.getEntityCollection(function(result) {
					if (!result.success) {
						this.showInformationDialog("Ошибка запроса данных");
						return;
					}
					else {
						var rows = [];
						for (var i = 0; i < result.collection.collection.length; i++) {
							rows.push(result.collection.collection.items[i].values);
							rows[i].Id = result.collection.collection.items[i].values.AccountId;
							delete rows[i].AccountId;
						}
						this.set("ParsedResult", rows);
					}
				}, this);
			},
			onOpenMiniPage: function() {
				this.openAddMiniPage({
					entitySchemaName: "Account",
					valuePairs: []
				});
			},
			loadDuplicatesDetailModule: function(id, rootItem, rows) {
				var primaryDisplayColumnName = rootItem.entitySchema.primaryDisplayColumnName;
				var itemName = rootItem.get(primaryDisplayColumnName),
					newAccount;
				if (this.get("NewAccount")) {
					var newAccountData = this.get("NewAccountData");
					itemName = newAccountData.Name;
					newAccount = true;
				}
				var detailId = "DuplicatesDetailModule" + id;
				var containerId = "DuplicatesPageV2DuplicateContainerContainer-" + id + "-listItem";
				var sandbox = this.sandbox;
				sandbox.loadModule("DuplicatesDetailModuleV2", {
					renderTo: containerId,
					id: detailId
				});
				sandbox.subscribe("GetConfig", function() {
					var profile = this.get("GridProfile");
					return {
						newAccount: newAccount,
						caption: itemName,
						groupId: id,
						gridConfig: profile.listedConfig,
						entitySchemaName: this.getDuplicatesSchemaName()
					};
				}, this, [detailId]);
				sandbox.subscribe("GetGridData", function() {
					return rows;
				}, this, [detailId]);
				sandbox.subscribe("HideDetail", function() {
					this.hideItem(id);
				}, this, [detailId]);
			}
		},

		diff: /**SCHEMA_DIFF*/
			[
				{
					"operation": "insert",
					"parentName": "LeftContainer",
					"propertyName": "items",
					"name": "openMiniPage",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"caption": "ДОБАВИТЬ КОНТРАГЕНТА",
						"visible": {"bindTo": "NewAccount"},
						"click": {"bindTo": "onOpenMiniPage"},
						"style": Terrasoft.controls.ButtonEnums.style.GREEN,
						"index": 10
					}
				},
				{
					"operation": "merge",
					"parentName": "LeftContainer",
					"propertyName": "items",
					"name": "actions",
					"values": {
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"caption": {"bindTo": "Resources.Strings.ActionButtonCaption"},
						"classes": {
							"textClass": ["actions-button-margin-right"],
							"wrapperClass": ["actions-button-margin-right"]
						},
						"menu": {
							"items": {"bindTo": "ActionsButtonMenuItems"}
						},
						"visible": {"bindTo": "ActionsInNewAccount"}
					}
				}
			]/**SCHEMA_DIFF*/
		};
});