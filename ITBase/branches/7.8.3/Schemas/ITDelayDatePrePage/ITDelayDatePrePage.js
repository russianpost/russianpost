define("ITDelayDatePrePage", ["CustomProcessPageV2Utilities", "BusinessRuleModule", "ITJsConsts"],
	function(CustomProcessPageV2Utilities, BusinessRuleModule, ITJsConsts) {
		return {

			mixins: {
				BaseProcessViewModel: "Terrasoft.CustomProcessPageV2Utilities"
			},

			attributes: {
				"FiltredStages": {
					"dataValueType": Terrasoft.DataValueType.COLLECTION,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				
				"StageList": {
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
		//			"isCollection": true,
					"value": this.Ext.create("Terrasoft.Collection")
				},

				//Определяет возможность редактирования поля Стадия процесса
				"isEnabledStage": {
					dataValueType: Terrasoft.DataValueType.BOOLEAN,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				//Атрибут с фильтрами: 1) Филиал текущего пользователя; 2) Контакты из Блока печатных сервисов
				"GetStage": {
	//				"dataValueType": Terrasoft.DataValueType.ENUM,
					type: Terrasoft.core.enums.ViewModelSchemaItem.ATTRIBUTE,
					"isCollection": true
					/*"referenceSchemaName": "OpportunityStage",
					"lookupListConfig": {
						"filters": [
							function() {
								var filterGroup = this.Ext.create("Terrasoft.FilterGroup");
								var ByTSLeadType = this.Terrasoft.createColumnInFilterWithParameters("Id", this.stages);
								filterGroup.add("ByTSLeadType", ByTSLeadType);
								return filterGroup;
							}
						]
					}*/
				},

				"GetDate": {
					"dataValueType": Terrasoft.DataValueType.DATE,
					"isRequired": true
				},

				"GetCurrentLeadType": {
					"dataValueType": Terrasoft.DataValueType.GUID,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				}
			},

			rules: {},

			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,

			stages: [],

			methods: {				
				onEntityInitialized: function() {
					this.callParent(arguments);
					this.prepareFilter();
				},

				prepareStageList: function(filter, list) {
					var stages = {};
					if (list) {
						list.clear();
						
						var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "OpportunityStage"
						});
						esq.addColumn("Id");
						esq.addColumn("Name");
						esq.filters.add("esqFirstFilter", this.Terrasoft.createColumnInFilterWithParameters("Id", this.stages));
						esq.getEntityCollection(function (result) {
							if (result.success && result.collection.collection.items) {
								/*result.collection.collection.items.each(function (item) {
									stages.push({value: item.get("Id"), displayValue: item.get("Name")});
								}, this);*/
								
								result.collection.collection.items.forEach(function(item, i, list) {
									stages[i] = {id: item.get("Id"),
												 value: item.get("Id"),
												 displayValue: item.get("Name")};
								}, this);
								
								list.loadAll(stages);
							}							
						}, this);
					}
					
				},

				//Фильтрует справочник в зависимости от стадии продажи
				prepareFilter: function() {
					var configPresentation = {};
					var array = [], currentStage,
						presentation = ITJsConsts.LeadStages.parcelBlock.presentation,
						agreement = ITJsConsts.LeadStages.parcelBlock.agreement,
						signing = ITJsConsts.LeadStages.parcelBlock.signing,
						escort = ITJsConsts.LeadStages.parcelBlock.escort,
						shipments = ITJsConsts.LeadStages.parcelBlock.shipments,
						victory = ITJsConsts.LeadStages.parcelBlock.victory,
						defeat = ITJsConsts.LeadStages.parcelBlock.defeat,
						hold = ITJsConsts.LeadStages.parcelBlock.hold;
						
					currentStage = this.get("CurrentStage");
					//В зависимости от стадии, на которой находится на данный момент
					switch (currentStage) {
						case presentation:
							array.push(presentation);
							this.stages = array;
							configPresentation.Id = "325f0619-0ee0-df11-971b-001d60e938c6";
							configPresentation.Name = "Презентация";
							configPresentation.displayValue = "Презентация";
							configPresentation.value = "325f0619-0ee0-df11-971b-001d60e938c6";
							this.set("GetStage", configPresentation);
							this.set("isEnabledStage", false);
							break;
						case agreement:
							array.push(presentation, agreement);
							this.stages = array;
							this.set("isEnabledStage", true);
							break;
						case signing:
							array.push(presentation, agreement, signing);
							this.stages = array;
							this.set("isEnabledStage", true);
							break;
						
						default:
							array.push(presentation, agreement, signing, escort, shipments, victory, defeat, hold);
							this.stages = array;
							this.set("isEnabledStage", true);
					}
				},

				//Возвращаем необходимое название для страницы
				getHeader: function() {
					return "Перенос даты исполнения продажи";
				},

				//Переопределение метода для прорисовки caption данной страницы
				getPageHeaderCaption: function() {
					var pageName = this.name;
					var header = pageName === "ITDelayDatePrePage" ? this.getHeader() : "";
					return header;
				},

				initHeaderCaption: Ext.emptyFn,

				initPrintButtonMenu: Ext.emptyFn,
				
				loadVocabulary: function(args, tag) {
					var column = this.getColumnByName(tag);
					args.schemaName = column.referenceSchemaName;
					this.callParent(arguments);
				},

				getLookupPageConfig: function(args, columnName) {
					var config = {
						entitySchemaName: args.schemaName,
						multiSelect: false,
						columnName: columnName,
						columnValue: this.get(columnName),
						searchValue: args.searchValue,
						filters: this.getLookupQueryFilters(columnName)
					};
					this.Ext.apply(config, this.getLookupListConfig(columnName));
					return config;
				},

				onCloseCardButtonClick: function() {
					this.sandbox.publish("BackHistoryState");
				},

				//Срабатывает при нажатии на кнопку "Продолжить" + валидация заполнения полей
				onNextButtonClick: function() {
					var errorMessage = "Перенести продажу можно только на дату в будущем";
					var stage = this.get("GetStage"), date = this.get("GetDate"), nowDate = new Date();
					if (((this.Ext.isEmpty(stage) || stage === "00000000-0000-0000-0000-000000000000") ||
					this.Ext.isEmpty(date))) {
						this.showInformationDialog("Перед продолжением процесса необходимо заполнить все поля");
						return;
					} else {
						//Проверка даты на правило "Дата должна быть в будущем"
						switch (date.getFullYear() > nowDate.getFullYear()) {
							case true:
								this.acceptProcessElement("NextButton");
								break;
							default:
								switch (date.getFullYear() === nowDate.getFullYear()) {
									case true:
										switch (date.getMonth() > nowDate.getMonth()) {
											case true:
												this.acceptProcessElement("NextButton");
												break;
											default:
												switch (date.getMonth() === nowDate.getMonth()) {
													case true:
														switch (date.getDate() <= nowDate.getDate()) {
															case true:
																this.showInformationDialog(errorMessage);
																break;
															default:
																this.acceptProcessElement("NextButton");
														}
														break;
													default:
														this.showInformationDialog(errorMessage);
												}
										}
										break;
									default:
										this.showInformationDialog(errorMessage);
								}
						}
					}
					
				}
			},

			diff: /**SCHEMA_DIFF*/[
				// Удаляем лишние кнопки
				{
					operation: "remove",
					name: "ActionButtonsContainer"
				},

				{
					operation: "remove",
					name: "DiscardChangesButton"
				},

				{
					operation: "remove",
					name: "CloseButton"
				},

	/*			{
					"operation": "remove",
					"name": "Tabs"
				},*/

				{
					operation: "remove",
					name: "DelayExecutionButton"
				},

				{
					operation: "remove",
					name: "ViewOptionsButton"
				},

				{
					operation: "remove",
					name: "SaveButton"
				},

				{
					"operation": "remove",
					"name": "actions"
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "NextButton",
					"values": {
						caption: "Продолжить",
						itemType: Terrasoft.ViewItemType.BUTTON,
						classes: {textClass: "actions-button-margin-right"},
						style: Terrasoft.controls.ButtonEnums.style.GREEN,
						click: {bindTo: "onNextButtonClick"},
						layout: {column: 5, row: 4, colSpan: 7}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "GetStage",
					"values": {
						"caption": "Перенести на стадию",
						"dataValueType": Terrasoft.DataValueType.ENUM,
						"controlConfig": {
							"prepareList": {
								"bindTo": "prepareStageList"
							},
							"list": {
								"bindTo": "StageList"
							}
						},
						"enabled": {bindTo: "isEnabledStage"},
						"isRequired": true,
		//				"bindTo": "GetStage",
						"layout": {column: 0, row: 1, colSpan: 10}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "GetDate",
					"values": {
						"caption": "Перенести на дату: ",
						"bindTo": "GetDate",
						"dataValueType": Terrasoft.DataValueType.DATE,
						"layout": {column: 0, row: 2, colSpan: 10},
						"isRequired": true,
						"tip": {
							"content": "Дата должна быть в будущем"
						}
					}
				}
			]/**SCHEMA_DIFF*/,

			userCode: {}
		};
	});