define("ITBaseDashboardItemViewConfig", ["ImageCustomGeneratorV2", "ViewGeneratorV2", "BaseDashboardItemViewConfig"], function(ImageCustomGeneratorV2) {
    Ext.define("Terrasoft.configuration.ITBaseDashboardItemViewConfig", {
        extend: "Terrasoft.BaseDashboardItemViewConfig",
        alternateClassName: "Terrasoft.ITBaseDashboardItemViewConfig",

        createViewGenerator: function() {
            var viewGenerator = this.Ext.create(this.viewGeneratorClass, {
                customGenerators: {
                    "IconContainer": "ImageCustomGeneratorV2.generateSimpleCustomImage"
                },
                "viewModelClass": this.Ext.ClassManager.get("Terrasoft.ITActivityDashboardItemViewModel")
            });
            viewGenerator.setGeneratorsByModule("ImageCustomGeneratorV2", ImageCustomGeneratorV2);
            return viewGenerator;
        },

        getViewConfig: function() {
            return [
                {
                    "name": "Item",
                    "itemType": Terrasoft.ViewItemType.CONTAINER,
                    "items": [
                        {
                            "name": "Content",
                            "itemType": Terrasoft.ViewItemType.CONTAINER,
                            "classes": {wrapClassName: ["dashboard-item-content"]},
                            "items": [
                                {
                                    "name": "Caption",
                                    "itemType": Terrasoft.ViewItemType.LABEL,
                                    "caption": {"bindTo": "Caption"},
                                    "markerValue": {"bindTo": "Caption"},
                                    "click": {"bindTo": "onCaptionClick"},
                                    "classes": {
                                        "labelClass": ["dashboard-item-clickable"]
                                    },
                                    "tips": [{
                                        "content": {"bindTo": "Caption"},
                                        "markerValue": {"bindTo": "Caption"}
                                    }]
                                },
                                {
                                    "name": "DetailedResultCaption",
                                    "itemType": Terrasoft.ViewItemType.LABEL,
                                    "caption": "Введите результат подробно",
                                    "visible": { "bindTo": "DetailedResultVisible"},
                                    "classes": {"labelClass": ["detailed-result-label"]}
                                },
                                {
                                    "name": "DetailedResult",
                                    "dataValueType": Terrasoft.DataValueType.TEXT,
                                    "className": "Terrasoft.TextEdit",
                                    "labelConfig": {
                                        "visible": false
                                    },
                                    "value": {bindTo: 'DetailedResultValue'},
                                    "visible": { "bindTo": "DetailedResultVisible"}
                                }
                            ]
                        },
                        {
                            "name": "Footer",
                            "itemType": Terrasoft.ViewItemType.CONTAINER,
                            "classes": {wrapClassName: ["dashboard-item-footer"]},
                            "items": [
                                {
                                    "name": "Date",
                                    "itemType": Terrasoft.ViewItemType.LABEL,
                                    "caption": {"bindTo": "DateText"}
                                },
                                {
                                    "name": "Owner",
                                    "itemType": Terrasoft.ViewItemType.LABEL,
                                    "caption": {"bindTo": "Owner"},
                                    "classes": {
                                        "labelClass": ["dashboard-footer-item-border-left"]
                                    }
                                },
                                {
                                    "name": "IconContainer",
                                    "generator": "ImageCustomGeneratorV2.generateSimpleCustomImage",
                                    "classes": {
                                        "wrapClass": ["dashboard-item-icon dashboard-item-right on-hover-hidden"]
                                    },
                                    "onPhotoChange": Terrasoft.emptyFn,
                                    "getSrcMethod": "getIconSrc",
                                    "items": []
                                }
                            ]
                        },
                        {
                            name: "Actions",
                            "itemType": Terrasoft.ViewItemType.CONTAINER,
                            "classes": {wrapClassName: ["dashboard-item-actions"]},
                            "items": [
                                {
                                    "name": "ThirdButton",
                                    "itemType": Terrasoft.ViewItemType.BUTTON,
                                    "style": {"bindTo": "ThirdButtonStyle"},
                                    "caption": {"bindTo": "ThirdButtonCaption"},
                                    "click": {"bindTo": "onThirdButtonClick"},
                                    "classes": {
                                        "textClass": "dashboard-item-right"
                                    },
                                    "visible": {"bindTo": "ThirdButtonVisible"}
                                },
                                {
                                    "name": "SecondButton",
                                    "itemType": Terrasoft.ViewItemType.BUTTON,
                                    "style": {"bindTo": "SecondButtonStyle"},
                                    "caption": {"bindTo": "SecondButtonCaption"},
                                    "click": {"bindTo": "onSecondButtonClick"},
                                    "classes": {
                                        "textClass": "dashboard-item-right"
                                    },
                                    "visible": {"bindTo": "SecondButtonVisible"}
                                },
                                {
                                    "name": "FirstButton",
                                    "itemType": Terrasoft.ViewItemType.BUTTON,
                                    "style": {"bindTo": "FirstButtonStyle"},
                                    "caption": {"bindTo": "FirstButtonCaption"},
                                    "click": {"bindTo": "onFirstButtonClick"},
                                    "classes": {
                                        "textClass": "dashboard-item-right"
                                    },
                                    "visible": {"bindTo": "FirstButtonVisible"}
                                },
                                {
                                    "name": "Cancel",
                                    "itemType": Terrasoft.ViewItemType.BUTTON,
                                    "style": Terrasoft.controls.ButtonEnums.style.BLUE,
                                    "caption": {"bindTo": "CancelButtonCaption"},
                                    "click": {"bindTo": "onCancelButtonClick"},
                                    "classes": {
                                        "textClass": "dashboard-item-right"
                                    },
                                    "visible": {"bindTo": "CancelButtonVisible"}
                                },
                                {
                                    "name": "Execute",
                                    "itemType": Terrasoft.ViewItemType.BUTTON,
                                    "style": Terrasoft.controls.ButtonEnums.style.GREEN,
                                    "caption": {"bindTo": "ExecuteButtonCaption"},
                                    "click": {"bindTo": "onExecuteButtonClick"},
                                    "classes": {
                                        "textClass": "dashboard-item-right"
                                    },
                                    "visible": {"bindTo": "ExecuteButtonVisible"}
                                }
                            ]
                        }
                    ]
                }
            ];
        }

    });
});