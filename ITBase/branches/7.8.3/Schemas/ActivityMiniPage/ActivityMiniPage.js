define("ActivityMiniPage", ["ActivityMiniPageResources", "ITJsActivityConsts",
		"ConfigurationConstants", "css!ITActivityMiniPageCSS"],/*, "ProcessModuleUtilities"*/
	function(resources, ITJsActivityConsts,
			ConfigurationConstants) {
		return {

			entitySchemaName: "Activity",

			details: /**SCHEMA_DETAILS*/ {} /**SCHEMA_DETAILS*/ ,

			mixins: {},

			messages: {
				"AddNewRecordOnCallEnd": {
					mode: this.Terrasoft.MessageMode.BROADCAST,
					direction: this.Terrasoft.MessageDirectionType.PUBLISH
				}
			},

			attributes: {
				"isNotEditable": {
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: false
				},

				"ActivityDateVisibility": {
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dependencies: [{
						columns: ["ActivityCategory", "Result"],
						methodName: "newActivityDateVisibility"
					}]
				},

				"ActivityTitle": {
					dataValueType: this.Terrasoft.DataValueType.TEXT,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				"OpportunityStage": {
					dataValueType: this.Terrasoft.DataValueType.TEXT,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				}
			},
			methods: {

				onEntityInitialized: function() {
					this.callParent(arguments);
					this.changeIsNotEditable();
					this.setOpportunityStage();
				},

				//Определяет видимость колонки "Перенести на"
				newActivityDateVisibility: function() {
					var type = this.get("ActivityCategory"),
						result = this.get("Result"),
						visible = type && type.value === ConfigurationConstants.Activity.ActivityCategory.CallAsCategory &&
							result &&
							(result.value === ITJsActivityConsts.ActivityResult.Meeting ||
							result.value === ITJsActivityConsts.ActivityResult.NeedCall);
					if (!visible) {this.set("ITNewActivityDate", null); }
					this.set("ActivityDateVisibility", visible);
				},


				changeIsNotEditable: function() {
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", { rootSchemaName: "Activity"});
					esq.addColumn("Status", "Status");
					esq.getEntity(this.get("Id"), function(result) {
						if (!result.success || !result.entity) {
							this.set("isNotEditable", false);
							return;
						}
						if (result.entity.get("Status").value === ConfigurationConstants.Activity.Status.Done ||
							result.entity.get("Status").value === ConfigurationConstants.Activity.Status.Cancel) {
							this.set("isNotEditable", true);
						}
					}, this);
				},

				setOpportunityStage: function() {
					var opportunityStageText = "";
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Activity"
					});
					var activityitem = this;
					activityitem.set("isActivityTitleVisible", false);
					activityitem.set("isOpportunityStageVisible", false);

					var activityId = this.get("Id");
					var test = this.isNotViewMode;
					esq.addColumn("Opportunity.Stage.Name", "OpportunityStageName");
					esq.addColumn("Title", "Title");
					esq.getEntity(activityId, function(result) {
						if (result.success || result.entity) {
							activityitem.set("ActivityDateVisibility", false);
							opportunityStageText = result.entity.get("OpportunityStageName");
							if (opportunityStageText.length > 0) {
									/*var activityTitle = result.entity.get("Title");
									var aTitle = "Название активности: " + activityTitle;
									activityitem.set("isActivityTitleVisible", true);
									activityitem.set("ActivityTitle", {
										value: aTitle,
										displayValue: aTitle
									});*/

								var str = "Стадия продажи: " + opportunityStageText;
								activityitem.set("isOpportunityStageVisible", true);
								activityitem.set("OpportunityStage", {
									value: str,
									displayValue: str
								});
							}
						}
					});
				},


				//Проверка на заполненность полей перед сохранение завершенного звонка
				validationOnSave: function() {
					var date = this.get("ITNewActivityDate"),
						title = this.get("ITNewTitle");
					return date && title;
				},

				save: function() {
					var type = this.get("ActivityCategory"),
						status = this.get("Status"),
						result = this.get("Result"),
						meeting = ITJsActivityConsts.ActivityResult.Meeting,
						needCall = ITJsActivityConsts.ActivityResult.NeedCall,
						meetingCategory = ITJsActivityConsts.ActivityCategory.Meeting,
						callCategory = ITJsActivityConsts.ActivityCategory.Call,
						validationMessage = resources.localizableStrings.VadlidationMessage,
						finishedActivityMessage = resources.localizableStrings.FinishedActivityMessage,
						callDirection = ITJsActivityConsts.CallDirection.Outgoing;
					if (this.get("isNotEditable")) {
						this.showInformationDialog(finishedActivityMessage);
					} else {
						if (type && type.value !== ConfigurationConstants.Activity.ActivityCategory.CallAsCategory) {
							this.callParent(arguments);
						} else {
							if (status && status.value !== ConfigurationConstants.Activity.Status.Done) {
								this.callParent(arguments);
								//this.changeIsNotEditable();
							} else {
								if (result && result.value === meeting) {
									if (!this.validationOnSave()) {
										this.showInformationDialog(validationMessage);
										return;
									}
									this.createNewActivity(meetingCategory, null);
									this.callParent(arguments);
									this.sandbox.publish("AddNewRecordOnCallEnd", {}, ["callKey"]);
								} else if (result && result.value === needCall) {
									if (!this.validationOnSave()) {
										this.showInformationDialog(validationMessage);
										return;
									}
									this.createNewActivity(callCategory, callDirection);
									this.callParent(arguments);
									this.sandbox.publish("AddNewRecordOnCallEnd", {}, ["callKey"]);
								} else {
									this.callParent(arguments);
								}
							}
						}
					}
				},

				//Создание новой встречи или звонка
				createNewActivity: function(activityCategory, callDirection) {
					var configObject = {
						title: this.get("ITNewTitle"),
						startDateDate: this.get("ITNewActivityDate") || null,
						dueDate: this.get("ITNewActivityDate") || null,
						statusActivity: ITJsActivityConsts.ActivityStatus.NotStarted,
						owner: this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value,
						author: this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value,
						priority: ITJsActivityConsts.ActivityPriority.Middle,
						category: activityCategory,
						showInScheduler: true,
						callDirection: callDirection
					};
					var insert = this.Ext.create("Terrasoft.InsertQuery", {rootSchemaName: "Activity"});

					insert.setParameterValue("Title", configObject.title,
						this.Terrasoft.DataValueType.TEXT);
					insert.setParameterValue("StartDate", configObject.startDateDate,
						this.Terrasoft.DataValueType.DATE_TIME);
					insert.setParameterValue("DueDate", configObject.dueDate,
						this.Terrasoft.DataValueType.DATE_TIME);
					insert.setParameterValue("Status", configObject.statusActivity,
						this.Terrasoft.DataValueType.GUID);
					insert.setParameterValue("Owner", configObject.owner,
						this.Terrasoft.DataValueType.GUID);
					insert.setParameterValue("Author", configObject.author,
						this.Terrasoft.DataValueType.GUID);
					insert.setParameterValue("Priority", configObject.priority,
						this.Terrasoft.DataValueType.GUID);
					insert.setParameterValue("ActivityCategory", configObject.category,
						this.Terrasoft.DataValueType.GUID);
					insert.setParameterValue("ShowInScheduler", configObject.showInScheduler,
						this.Terrasoft.DataValueType.BOOLEAN);
					insert.setParameterValue("CallDirection", configObject.callDirection,
						this.Terrasoft.DataValueType.GUID);
					insert.execute();
				},
				updateDetail: function() {
					var updateConfig = this.getUpdateDetailConfig();
					var string = this.sandbox.id;
					if ((string.indexOf("ITMeetingsDetailActivityActivity") + 1) ||
					(string.indexOf("ITCallsDetailActivity") + 1) || (string.indexOf("ITTasksDetailV2Activity") + 1)) {
						updateConfig.reloadAll = true;
						var numberBegin = string.indexOf("IT");
						var numberEnd = string.lastIndexOf("Activity") + 8;
						var stringFirst = string.substring(0, numberBegin);
						var stringThird = string.substring(numberEnd);
						var finishStringMeeting = stringFirst + "ITMeetingsDetailActivityActivity" + stringThird;
						var finishStringCall = stringFirst + "ITCallsDetailActivity" + stringThird;
						var finishStringTasks = stringFirst + "ITTasksDetailV2Activity" + stringThird;
						this.sandbox.publish("UpdateDetail", updateConfig, finishStringMeeting);
						this.sandbox.publish("UpdateDetail", updateConfig, finishStringCall);
						this.sandbox.publish("UpdateDetail", updateConfig, finishStringTasks);
					}
					else {
						this.sandbox.publish("UpdateDetail", updateConfig, [this.sandbox.id]);
					}
				}

			},

			diff: /**SCHEMA_DIFF*/ [
				{
					"operation": "insert",
					"name": "RemindToOwner",
					"parentName": "MiniPage",
					"propertyName": "items",
					"values": {
						"bindTo": "RemindToOwner",
						"isMiniPageModelItem": true,
						"layout": { "column": 0, "row": 11, "colSpan": 12},
						"visible": { "bindTo": "isNotViewMode" }
					}
				},

				{
					"operation": "insert",
					"name": "RemindToOwnerDate",
					"parentName": "MiniPage",
					"propertyName": "items",
					"values": {
						"bindTo": "RemindToOwnerDate",
						"isMiniPageModelItem": true,
						"layout": { "column": 12, "row": 11, "colSpan": 12 },
						"visible": { "bindTo": "isNotViewMode" }
					}
				},
				{
					"operation": "insert",
					"name": "ActivityGeneralInfoContainer2",
					"parentName": "MiniPage",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.CONTAINER,
						"items": [],
						//"classes": {
							//"wrapClassName": ["activity-general-info-container"]
						//},
						"layout": {
							"column": 0,
							"row": 3,
							"colSpan": 24
						}
					}
				},
				{
					"operation": "insert",
					"parentName": "ActivityGeneralInfoContainer",
					"propertyName": "items",
					"name": "ActivityTitle",
					"values": {
						"labelConfig": {
							"visible": true
						},
						"value": {"bindTo": "ActivityTitle"},
						"wrapClass": ["activity-status-wrapper"],
						"classes": {
							"labelClass": ["activity-status-label-caption2"]
						},
						"isMiniPageModelItem": true,
						"visible": {
							"bindTo": "isActivityTitleVisible"
						}
					}
				},

				{
					"operation": "insert",
					"name": "ActivityGeneralInfoContainer2",
					"parentName": "MiniPage",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.CONTAINER,
						"items": [],
						"classes": {
							"wrapClassName": ["activity-general-info-container"]
						},
						"layout": {
							"column": 0,
							"row": 0,
							"colSpan": 24
						}
					}
				},
				{
					"operation": "merge",
					"name": "CommunicationsContainer",
					"parentName": "ActivityGeneralInfoContainer2",
					"values": {
						"visible": true
					}
				},

				{
					"operation": "insert",
					"parentName": "ActivityGeneralInfoContainer",
					"propertyName": "items",
					"name": "OpportunityStage",
					"values": {
						"labelConfig": {
							"visible": false
						},
						"value": {"bindTo": "OpportunityStage"},
						"isMiniPageModelItem": true,
						"wrapClass": ["activity-status-wrapper"],
						"classes": {
							"labelClass": ["activity-status-label-caption2"]
						},
						"visible": {
							"bindTo": "isOpportunityStageVisible"
						}
					}
				},

				{
					"operation": "insert",
					"name": "ITNewActivityDate",
					"parentName": "MiniPage",
					"propertyName": "items",
					"values": {
						"bindTo": "ITNewActivityDate",
						"isMiniPageModelItem": true,
						"layout": { "column": 0, "row": 6, "colSpan": 12},
						"visible": { "bindTo": "ActivityDateVisibility" },
						"isRequired": true
					}
				},

				{
					"operation": "insert",
					"name": "ITNewTitle",
					"parentName": "MiniPage",
					"propertyName": "items",
					"values": {
						"bindTo": "ITNewTitle",
						"isMiniPageModelItem": true,
						"layout": { "column": 0, "row": 7, "colSpan": 24},
						"visible": { "bindTo": "ActivityDateVisibility" },
						"isRequired": true
						/*				"controlConfig": {
						"placeholder": {"bindTo": "Resources.Strings.NewTitleCaption"}
						},
						"labelConfig": {
						"visible": false,
						"markerValue": ""
						}*/
					}
				},

				{
					"operation": "merge",
					"name": "DetailedResult",
					"values": {
						"layout": {"column": 0, "row": 8, "colSpan": 24}
					}
				},

				{
					"operation": "merge",
					"name": "CommunicationsContainer",
					"parentName": "MiniPage",
					"propertyName": "items",
					"values": {
						"layout": {
							"column": 0,
							"row": 13,
							"colSpan": 24
						}
					}
				}
			] /**SCHEMA_DIFF*/ ,
			rules: {}
		};
	});