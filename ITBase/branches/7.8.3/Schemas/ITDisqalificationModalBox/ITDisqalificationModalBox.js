define("ITDisqalificationModalBox", ["ConfigurationConstants", "LookupUtilities", "ITJsLeadConsts",
	"ITDisqalificationModalBoxResources", "ServiceHelper", "MaskHelper", "css!ITDisqalificationModalBoxCSS"],
	function(ConfigurationConstants, LookupUtilities, ITJsLeadConsts, resources, ServiceHelper, MaskHelper) {
		return {
			mixins: { },
			messages: {
				"SaveDisqalificationModalBox": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.PUBLISH
				}
			},
			attributes: {
				MinWidth: {
					dataValueType: Terrasoft.DataValueType.INTEGER,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: 450
				},
				DisqualifyReasonValue: {
					"dataValueType": Terrasoft.DataValueType.LOOKUP,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				"OldDisqualifyReasonValue": {
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: Terrasoft.DataValueType.GUID
				},
				DisqualifyDescriptionValue: {
					"dataValueType": Terrasoft.DataValueType.LOOKUP,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				DisqualifyDescriptionVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				DisqualifyAccountValue: {
					"dataValueType": Terrasoft.DataValueType.LOOKUP,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				DisqualifyAccountVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				}

			},
			methods: {

				onChangeDisqualifyReason: function() {
					var disqualifyReason = this.get("DisqualifyReasonValue");
					this.set("DisqualifyDescriptionValue", "");
					this.set("DisqualifyAccountValue", null);
					this.set("DisqualifyDescriptionVisible", (disqualifyReason) ?
						disqualifyReason.value === ITJsLeadConsts.DisqualifyReason.Other :
						false);
					this.set("DisqualifyAccountVisible", (disqualifyReason) ?
						disqualifyReason.value === ITJsLeadConsts.DisqualifyReason.AlreadyHasContract :
						false);
					this.onRender();
				},

				getAccountLookupFilters: function() {
					var filterGroup = new this.Terrasoft.createFilterGroup();
					filterGroup.logicalOperation = this.Terrasoft.LogicalOperatorType.AND;
					filterGroup.add("AccountTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Type.Id", ConfigurationConstants.AccountType.Competitor));
					return filterGroup;
				},

				/**
				 * Инициализация справочников
				 * @param callback
				 * @param scope
				 */
				initData: function(callback, scope) {
					this.initializeLookupList("LeadDisqualifyReason", null, null, true);
					var filters = this.getAccountLookupFilters();
					this.initializeLookupList("DisqualifyAccount", "Account", filters);
					callback.call(scope);
				},

				init: function(callback, scope) {
					MaskHelper.ShowBodyMask();
					this.callParent([function() {
						this.initData(function() {
							callback.call(scope);
						}, this);
					}, this]);
					this.on("change:DisqualifyReasonValue", this.onChangeDisqualifyReason, this);
				},

				saveChanges: function() {
					if (!this.hasErrorBeforeSave()) {
						var disqualifyReason = this.get("DisqualifyReasonValue");
						var description = this.get("DisqualifyDescriptionValue");
						var account = this.get("DisqualifyAccountValue");
						var sandbox = this.sandbox;
						var data = {
							reason: disqualifyReason,
							description: description,
							account: account
						};
						this.sandbox.publish("SaveDisqalificationModalBox", data, [sandbox.id]);
						this.closeWindow();
					}
					else {
						this.showInformationDialog(resources.localizableStrings.InvalidBlockReasonCaption);
					}
				},

				/**
				* Проверка на заполнения причины блокировки, при условии выбора статуса "Блокировка"
				* @returns {boolean|*}
				*/
				hasErrorBeforeSave: function() {
					var disqualifyReason = this.get("DisqualifyReasonValue");
					var description = this.get("DisqualifyDescriptionValue");
					var account = this.get("DisqualifyAccountValue");
					var isOther = (disqualifyReason) && disqualifyReason.value === ITJsLeadConsts.DisqualifyReason.Other;
					var isAlreadyContract = (disqualifyReason) &&
						disqualifyReason.value === ITJsLeadConsts.DisqualifyReason.AlreadyHasContract;
					return (isOther && !description) || (isAlreadyContract && !account) || (!disqualifyReason);
				},

				getModalWindowSize: function(domWindowEl) {
					var gridContainerEl = this.Ext.get("windowAreaContainer");
					if (!domWindowEl) {
						return null;
					}
					return {
						width: this.get("MinWidth"),
						height: Math.max(gridContainerEl.dom.clientHeight + 50, domWindowEl.dom.clientHeight + 10)
					};
				},

				onRender: function() {
					MaskHelper.HideBodyMask();
					var modalBoxInnerBoxEl = this.Ext.get(this.renderTo);
					if (modalBoxInnerBoxEl && this.Ext.isFunction(modalBoxInnerBoxEl.parent)) {
						var modalBoxEl = modalBoxInnerBoxEl.parent();
						if (modalBoxEl) {
							modalBoxEl.addCls("order-report-filter-modal-box");
						}
						modalBoxInnerBoxEl.addCls("container-padding");
					}
					var modalWindowSize = this.getModalWindowSize(modalBoxInnerBoxEl);
					if (modalWindowSize) {
						this.updateSize(modalWindowSize.width, modalWindowSize.height);
					}
				},

				closeWindow: function() {
					this.destroyModule();
				},

				getListValue: function(value, displayValue) {
					return {
						value: value,
						displayValue: displayValue
					};
				},

				convertArrayToObject: function(array) {
					var object = {};
					Terrasoft.each(array, function(element) {
						object[element.value] = element;
					}, this);
					return object;
				},

				prepareList: function(filter, list, entity) {
					if (list === null) {
						return;
					}
					list.clear();
					var listName =  entity + "List";
					var items = this.get(listName);
					list.loadAll(items);
				},

				prepareLeadDisqualifyReasonList: function(filter, list) {
					this.prepareList(filter, list, "LeadDisqualifyReason");
				},

				prepareDisqualifyAccountList: function(filter, list) {
					this.prepareList(filter, list, "Account");
				},


				/**
				 * Универсальный метод формирования справочного поля
				 * @param schemaEntityName
				 */
				initializeLookupList: function(schemaEntityName, listname, filters, sorted) {
					var list = (listname) ? listname : schemaEntityName;
					list += "List";
					var items = [];
					this.set(name, {});
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
						"rootSchemaName": listname || schemaEntityName
					});
					esq.addColumn("Id");
					esq.addColumn("Name");
					if (filters) {esq.filters = filters; }
					esq.getEntityCollection(function(response) {
						if (response && response.success) {
							Terrasoft.each(response.collection.collection.items, function(item) {
								var id = item.get("Id");
								var name = item.get("Name");
								items.push(this.getListValue(id, name));
							}, this);
							if (sorted) {
								//Изменена сортировка для задачи https://it-bpm.atlassian.net/browse/RPCRM-910
								if (schemaEntityName === "LeadDisqualifyReason") {
									items.sort(function(a, b) {
										//2 почти одинаковых метода сортировки массива
			/*							if (a.displayValue === "Другое") {return 1; }
										else if (b.displayValue === "Другое") {return -1; }
										else {return 0; }*/
										return a.displayValue === "Другое" ? 1 : -1;
									});
								} else {
									items.sort(function(a, b) {
										return a.displayValue > b.displayValue ? 1 : a.displayValue < b.displayValue ? -1 : 1;
									});
								}

							}
							this.set(list, this.convertArrayToObject(items));
						}
					}, this);
				}

			},
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "insert",
					"name": "windowAreaContainer",
					"index": 0,
					"values": {
						"id": "windowAreaContainer",
						"selectors": {"wrapEl": "#windowAreaContainer"},
						"wrapClass": ["fixed-area-container"],
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"items": []
					}
				},
				/* Header container + cross-close button */
				{
					"operation": "insert",
					"name": "headContainer",
					"parentName": "windowAreaContainer",
					"propertyName": "items",
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"wrapClass": ["header"],
						"items": []
					},
					"index": 0
				},
				{
					"operation": "insert",
					"name": "headerNameContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"wrapClass": ["header-name-container", "header-caption", "header-name-container-full"],
						"items": []
					},
					"parentName": "headContainer",
					"propertyName": "items"
				},
				{
					"operation": "insert",
					"name": "HeaderLabel",
					"values": {
						"itemType": Terrasoft.ViewItemType.LABEL,
						"caption": {"bindTo": "Resources.Strings.HeaderCaption"}
					},
					"parentName": "headerNameContainer",
					"propertyName": "items"
				},
				{
					"operation": "insert",
					"name": "closeIconContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"wrapClass": ["header-name-container", "header-name-container-full"],
						"items": []
					},
					"parentName": "headContainer",
					"propertyName": "items"
				},
				{
					"operation": "insert",
					"name": "closeIconButton",
					"parentName": "closeIconContainer",
					"propertyName": "items",
					"values": {
						"click": {"bindTo": "closeWindow"},
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
						"hint": {"bindTo": "Resources.Strings.CloseButtonHint"},
						"imageConfig": {"bindTo": "Resources.Images.CloseIcon"},
						"markerValue": "CloseIconButton",
						"classes": {"wrapperClass": ["close-btn-user-class"]},
						"selectors": {"wrapEl": "#headContainer"}
					}
				},
				{
					"operation": "insert",
					"name": "contentAreaContainer",
					"values": {
						"id": "contentAreaContainer",
						"selectors": {"wrapEl": "#contentAreaContainer"},
						"wrapClass": ["box-content"],
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"items": []
					},
					"parentName": "windowAreaContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "contentContainer",
					"values": {
						"id": "contentContainer",
						"selectors": {"wrapEl": "#contentContainer"},
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"items": []
					},
					"parentName": "contentAreaContainer",
					"propertyName": "items"
				},
				{
					"name": "LeadDisqualifyReasonLookup",
					"operation": "insert",
					"values": {
						"bindTo": "DisqualifyReasonValue",
						"caption": {"bindTo": "Resources.Strings.LeadDisqualifyReasonCaption"},
						//"layout": {"column": 0, "row": 0, "colSpan": 24},
						"dataValueType": Terrasoft.DataValueType.ENUM,
						"controlConfig": {
							"className": "Terrasoft.ComboBoxEdit",
							"prepareList": {"bindTo": "prepareLeadDisqualifyReasonList"}
						},
						"isRequired": true
					},
					"parentName": "contentContainer",
					"propertyName": "items"
				},
				{
					"name": "LeadDisqualifyReasonDescription",
					"operation": "insert",
					"values": {
						"bindTo": "DisqualifyDescriptionValue",
						"caption": {"bindTo": "Resources.Strings.LeadDisqualifyReasonDescription"},
						"dataValueType": Terrasoft.DataValueType.TEXT,
						"visible": {"bindTo": "DisqualifyDescriptionVisible"},
						"contentType": Terrasoft.ContentType.LONG_TEXT,
						"isRequired": true
					},
					"parentName": "contentContainer",
					"propertyName": "items"
				},

				{
					"name": "DisqualifyAccountLookup",
					"operation": "insert",
					"values": {
						"bindTo": "DisqualifyAccountValue",
						"caption": {"bindTo": "Resources.Strings.DisqualifyAccountCaption"},
						"dataValueType": Terrasoft.DataValueType.ENUM,
						"controlConfig": {
							"className": "Terrasoft.ComboBoxEdit",
							"prepareList": {"bindTo": "prepareDisqualifyAccountList"}
						},
						"visible": {"bindTo": "DisqualifyAccountVisible"},
						"isRequired": true
					},
					"parentName": "contentContainer",
					"propertyName": "items"
				},
				{
					"operation": "insert",
					"name": "buttonsContainer",
					"values": {
						"id": "buttonsContainer",
						"selectors": {"wrapEl": "#buttonsContainer"},
						"wrapClass": ["bottom-buttons-container"],
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"items": []
					},
					"parentName": "windowAreaContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "okButton",
					"parentName": "buttonsContainer",
					"propertyName": "items",
					"values": {
						"caption": {"bindTo": "Resources.Strings.OKButtonCaption"},
						"click": {"bindTo": "saveChanges"},
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.BLUE,
						"maskVisible": {"bindTo": "okButtonMaskVisible"},
						"markerValue": "ButtonOK"
					}
				},
				{
					"operation": "insert",
					"name": "cancelButton",
					"parentName": "buttonsContainer",
					"propertyName": "items",
					"values": {
						"caption": {"bindTo": "Resources.Strings.CancelButtonCaption"},
						"click": {"bindTo": "closeWindow"},
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"style": Terrasoft.controls.ButtonEnums.style.DEFAULT,
						"markerValue": "ButtonCancel"
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});