define("LeftPanelTopMenuModule", ["LeftPanelTopMenuModuleResources", "MaskHelper", "LookupUtilities",
		"ConfigurationConstants", "ProcessModuleUtilities", "LeftPanelUtilitiesV2", "ServiceHelper", "HoverMenuButton",
		"CheckModuleDestroyMixin", "css!ITLeftPanelTopMenuCSS"],
	function(resources, MaskHelper, LookupUtilities, ConfigurationConstants, ProcessModuleUtilities, LeftPanelUtilities,
		ServiceHelper) {
		function createConstructor(context) {
			var Ext = context.Ext;
			var sandbox = context.sandbox;
			var Terrasoft = context.Terrasoft;
			Ext.define("Terrasoft.LeftPanelTopMenuModuleViewModel", {
				extend: "Terrasoft.BaseViewModel",
				mixins: [
					"Terrasoft.CheckModuleDestroyMixin"
				],
				Ext: null,
				Terrasoft: null,
				sandbox: null,
				
				init: function(callback, scope) {
					this.loadMenu();
					this.setSystemDesignerVisible();
					LeftPanelUtilities.on("collapsedChanged", this.onSideBarCollapsedChanged, this);
					this.set("Collapsed", LeftPanelUtilities.getCollapsed());
					callback.call(scope);
				},

				
				setSystemDesignerVisible: function() {
					var isSystemDesignerVisible = !this.get("IsSSP");
					this.Terrasoft.SysSettings.querySysSettings(["BuildType"], function(sysSettings) {
						var buildType = sysSettings.BuildType;
						if (buildType && (buildType.value === ConfigurationConstants.BuildType.Public)) {
							isSystemDesignerVisible = false;
						}
						this.set("IsSystemDesignerVisible", isSystemDesignerVisible);
					}, this);
				},

				
				getConfigMenuItem: function(entity) {
					var uId = entity.get("IntroPageUId");
					var name = entity.get("Name");
					var tag = entity.get("Tag");
					return {
						Id: uId,
						Caption: name,
						Tag: tag,
						Class: "menu-item",
						Click: {bindTo: "goToIntroPageFromMenu"},
						canExecute: {bindTo: "canBeDestroyed"}
					};
				},

				
				loadItemsMainMenu: function() {
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "ApplicationMainMenu",
						isDistinct: true
					});
					esq.addColumn("Id");
					esq.addColumn("IntroPageUId");
					esq.addColumn("Name");
					esq.addColumn("[SysSchema:UId:IntroPageUId].Name", "Tag");
					esq.getEntityCollection(function(result) {
						if (!result.success) {
							return;
						}
						var menuCollection = this.Ext.create("Terrasoft.BaseViewModelCollection");
						var entities = result.collection;
						var mainMenuConfig = {
							Id: "menu-menu-item",
							Tag: "MainMenu",
							Caption: resources.localizableStrings.mainManuMenuItemCaption,
							Visible: {
								bindTo: "IsSSP",
								bindConfig: {
									converter: function(value) {
										return !value;
									}
								}
							}
						};
						var entitiesCount = entities.getCount();
						if (entitiesCount === 0) {
							mainMenuConfig.Class = "menu-item";
							mainMenuConfig.Click = {bindTo: "goToFromMenu"};
							menuCollection.add(mainMenuConfig.Id, this.Ext.create("Terrasoft.BaseViewModel", {
								values: mainMenuConfig
							}));
						} else if (entitiesCount === 1) {
							entities.each(function(entity) {
								var menuItem = this.getConfigMenuItem(entity);
								menuItem.Caption = mainMenuConfig.Caption;
								menuCollection.add(menuItem.Id, this.Ext.create("Terrasoft.BaseViewModel", {
									values: menuItem
								}));
							}, this);
						} else {
							mainMenuConfig.Type = "Terrasoft.MenuSeparator";
							menuCollection.add(mainMenuConfig.Id, this.Ext.create("Terrasoft.BaseViewModel", {
								values: mainMenuConfig
							}));
							entities.each(function(entity) {
								var menuItem = this.getConfigMenuItem(entity);
								menuCollection.add(menuItem.Id, this.Ext.create("Terrasoft.BaseViewModel", {
									values: menuItem
								}));
							}, this);
							var id = "end-menu-menu-item";
							menuCollection.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
								values: {
									Id: id,
									Type: "Terrasoft.MenuSeparator"
								}
							}));
						}
						var mainMenuItems = this.get("MainMenuItems");
						menuCollection.loadAll(mainMenuItems);
						mainMenuItems.clear();
						mainMenuItems.loadAll(menuCollection);
					}, this);
				},

				
				loadItemsQuickAddMenu: function() {
					var collection = this.get("quickAddMenu");
					collection.clear();
					var quickItems = this.Terrasoft.configuration.QuickAddMenu.QuickAddMenu;
					this.Terrasoft.each(quickItems, function(item) {
						var id = item.itemId;
						collection.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
							values: this.getItemQuickAddMenuConfig(id, item)
						}));
					}, this);
				},

				
				getItemQuickAddMenuConfig: function(id, item) {
					var config = item.hasAddMiniPage ? {} : {
						canExecute: {"bindTo": "canBeDestroyed"}
					};
					this.Ext.apply(config, {
						Id: id,
						Caption: item.name,
						Click: {bindTo: "processQuickMenuClick"},
						ModuleName: item.ModuleName,
						Tag: id,
						TypeColumnName: item.TypeColumnName,
						TypeColumnValue: item.TypeColumnValue,
						EditPageName: item.EditPageName,
						EntitySchemaName: item.EntitySchemaName,
						MiniPage: {
							schemaName: item.miniPageSchema,
							hasAddMiniPage: item.hasAddMiniPage
						}
					});
					return config;
				},

				
				processQuickMenuClick: function(tag) {
					var collection = this.get("quickAddMenu");
					var quickMenuItem = collection.get(tag);
					var moduleName = quickMenuItem.get("ModuleName") || "SysModuleEditManageModule";
					require([moduleName], function(module) {
						if (module) {
							module.Run({
								sandbox: sandbox,
								item: quickMenuItem
							});
						}
					});
				},

				
				loadItemsStartProcessMenu: function() {
					var filters = [];
					filters.push(
						this.Terrasoft.createExistsFilter("[RunButtonProcessList:SysSchemaUId:UId].Id"));
					var select = ProcessModuleUtilities.createRunProcessSelect(filters);
					select.getEntityCollection(function(result) {
						if (result.success) {
							var startProcessMenuItems = this.get("startProcessMenu");
							startProcessMenuItems.clear();
							var entities = result.collection;
							if (entities.getCount() > 0) {
								var id = "caption-runprocess-menu-item";
								startProcessMenuItems.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
									values: {
										Id: id,
										Type: "Terrasoft.MenuSeparator",
										Caption: resources.localizableStrings.RunProcessButtonMenuCaption
									}
								}));
								var idColumnName = "Id";
								var captionColumnName = "Caption";
								entities.each(function(entity) {
									id = entity.get(idColumnName);
									var caption = entity.get(captionColumnName);
									startProcessMenuItems.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
										values: {
											Id: id,
											Caption: caption,
											Click: {bindTo: "runProcess"},
											canExecute: {bindTo: "canBeDestroyed"},
											Tag: id,
											MarkerValue: caption
										}
									}));
								}, this);
								id = "separator-runprocess-menu-item";
								startProcessMenuItems.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
									values: {
										Id: id,
										Type: "Terrasoft.MenuSeparator",
										Caption: resources.localizableStrings.mainManuMenuItemCaption
									}
								}));
								id = "open-process-page";
								startProcessMenuItems.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
									values: {
										Id: id,
										Caption: resources.localizableStrings.AnotherProcessMenuItemCaption,
										Click: {bindTo: "openProcessPage"},
										Tag: id
									}
								}));
							}
							this.sandbox.publish("LoadedItemsStartProcessMenu");
						} else {
							throw new this.Terrasoft.QueryExecutionException();
						}
					}, this);
				},

				
				getViewConfig: function() {
					var view = {
						id: "side-bar-top-menu-module-container",
						selectors: {
							wrapEl: "#side-bar-top-menu-module-container"
						},
						classes: {
							wrapClassName: ["top-menu-module-container"]
						},
						items: this.getTopMenuConfig()
					};
					return view;
				},

				
				loadMenu: function() {
					this.loadItemsStartProcessMenu();
					this.sandbox.subscribe("ResetStartProcessMenuItems", function() {
						this.loadItemsStartProcessMenu();
					}, this);
					this.loadItemsQuickAddMenu();
					var menuCollection = this.Ext.create("Terrasoft.BaseViewModelCollection");
					var id = "process-menu-item";
					menuCollection.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
						values: {
							Id: id,
							Tag: "ProcessExecute",
							Caption: resources.localizableStrings.processMenuItemCaption,
							Click: {bindTo: "openProcessPage"},
							Visible: {
								bindTo: "IsSSP",
								bindConfig: {
									converter: function(value) {
										return !value;
									}
								}
							}
						}
					}));
					id = "collapse-menu-item";
					menuCollection.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
						values: {
							Id: id,
							Tag: "CollapseMenu",
							Caption: this.getCollapseSideBarMenuItemCaptionConfig(),
							Click: {bindTo: "collapseSideBar"}
						}
					}));
					var workplaceMenu = this.getWorkplaceMenu();
					if (workplaceMenu.getCount() > 0) {
						menuCollection.loadAll(workplaceMenu);
					}
					id = "system-designer-menu-item";
					menuCollection.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
						values: {
							Id: id,
							Tag: "IntroPage/SystemDesigner",
							Caption: resources.localizableStrings.systemDesignerMenuItemCaption,
							Click: {bindTo: "goToFromMenu"},
							Visible: {bindTo: "IsSystemDesignerVisible"},
							canExecute: {bindTo: "canBeDestroyed"}
						}
					}));
					id = "profile-menu-item";
					menuCollection.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
						values: {
							Id: id,
							Tag: "UserProfile",
							Caption: resources.localizableStrings.userProfileMenuItemCaption,
							Click: {bindTo: "goToFromMenu"},
							canExecute: {bindTo: "canBeDestroyed"}
						}
					}));
					id = "exit-menu-item";
					menuCollection.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
						values: {
							Id: id,
							Tag: "Exit",
							ClassName: "Terrasoft.MenuItem",
							Caption: resources.localizableStrings.exitMenuItemCaption,
							Click: {bindTo: "exitClick"},
							canExecute: {bindTo: "canBeDestroyed"}
						}
					}));
					var mainMenuItems = this.get("MainMenuItems");
					mainMenuItems.loadAll(menuCollection);
					this.loadItemsMainMenu();
				},

				
				getWorkplaceMenu: function() {
					var workplaceMenuItems = this.Ext.create("Terrasoft.BaseViewModelCollection");
					var workplaces = this.Terrasoft.deepClone(
						this.Terrasoft.configuration.WorkplacesStructure.Workplaces);
					if (workplaces.length > 0) {
						var id = "separator-top-menu-item";
						workplaceMenuItems.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
							values: {
								Id: id,
								Type: "Terrasoft.MenuSeparator",
								Caption: resources.localizableStrings.workPlaceMenuItemCaption
							}
						}));
						workplaces.sort(function(a, b) {
							if (a.name < b.name) {
								return -1;
							}
							if (a.name > b.name) {
								return 1;
							}
							return 0;
						});
						this.Terrasoft.each(workplaces, function(item) {
							if (item.hide) {
								return;
							}
							var menuItemConfig = {
								Caption: item.name,
								Tag: item.workplaceId,
								Click: {
									bindTo: "workPlaceMenuItemClick"
								}
							};
							workplaceMenuItems.add(this.Ext.create("Terrasoft.BaseViewModel", {
								values: menuItemConfig
							}));
						}, this);
						id = "separator-botom-menu-item";
						workplaceMenuItems.add(id, this.Ext.create("Terrasoft.BaseViewModel", {
							values: {
								Id: id,
								Type: "Terrasoft.MenuSeparator"
							}
						}));
					}
					return workplaceMenuItems;
				},

				
				getTopMenuConfig: function() {
					var menuConfig = [
						{
							id: "collapse-button",
							tag: "CollapseMenu",
							className: "Terrasoft.HoverMenuButton",
							style: this.Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
							classes: {
								imageClass: ["button-image-size"],
								wrapperClass: ["collapse-button-wrapperEl"]
							},
							imageConfig: resources.localizableImages.collapseIconSvg,
							click: {
								bindTo: "collapseSideBar"
							},
							hint: this.getCollapseSideBarMenuItemCaptionConfig(),
							markerValue: this.getCollapseSideBarMenuItemCaptionConfig()
						},/*
						{
							id: "menu-button",
							tag: "MainMenu",
							className: "Terrasoft.HoverMenuButton",
							style: this.Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
							hint: {bindTo: "getMenuButtonHint"},
							markerValue: resources.localizableStrings.MenuButtonHint,
							classes: {
								imageClass: ["button-image-size"],
								wrapperClass: ["menu-button-wrapperEl"]
							},
							imageConfig: resources.localizableImages.menuIconSvg,
							menu: {
								items: {bindTo: "MainMenuItems"},
								"alignType": "tr?"
							},
							delayedShowEnabled: {
								bindTo: "Collapsed"
							},
							showDelay: this.get("ShowDelay"),
							hideDelay: this.get("HideDelay")
						},
						{
							id: "menu-startprocess-button",
							tag: "StartProcessMenu",
							className: "Terrasoft.HoverMenuButton",
							style: this.Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
							hint: {bindTo: "getStartProcessMenuButtonHint"},
							markerValue: resources.localizableStrings.StartProcessButtonHint,
							classes: {
								imageClass: ["button-image-size"],
								wrapperClass: ["menu-startprocess-button-wrapperEl"]
							},
							imageConfig: resources.localizableImages.processIconSvg,
							menu: {
								items: {bindTo: "startProcessMenu"},
								"alignType": "tr?"
							},
							click: {
								bindTo: "startProcessMenuButtonClick"
							},
							visible: {
								bindTo: "IsSSP",
								bindConfig: {
									converter: function(value) {
										return !value;
									}
								}
							},
							delayedShowEnabled: {
								bindTo: "Collapsed"
							},
							showDelay: this.get("ShowDelay"),
							hideDelay: this.get("HideDelay")
						},*/
						{
							id: "menu-quickadd-button",
							tag: "quickAddMenu",
							className: "Terrasoft.HoverMenuButton",
							style: this.Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
							classes: {
								imageClass: ["button-image-size"],
								wrapperClass: ["menu-quickadd-button-wrapperEl"]
							},
							hint: {
								bindTo: "getQuickAddHint"
							},
							markerValue: resources.localizableStrings.AddButtonHint,
							imageConfig: resources.localizableImages.quickaddIconSvg,
							menu: {
								items: {bindTo: "quickAddMenu"},
								"alignType": "tr?"
							},
							visible: {
								bindTo: "IsSSP",
								bindConfig: {
									converter: function(value) {
										return !value;
									}
								}
							},
							delayedShowEnabled: {
								bindTo: "Collapsed"
							},
							showDelay: this.get("ShowDelay"),
							hideDelay: this.get("HideDelay")
						}
					];
					return menuConfig;
				},

				
				startProcessMenuButtonClick: function() {
					var startProcessMenu = this.get("startProcessMenu");
					if (startProcessMenu.getCount() > 0) {
						return false;
					}
					this.openProcessPage();
				},

				
				getQuickAddHint: function() {
					return this.getHint(resources.localizableStrings.AddButtonHint);
				},

				
				getStartProcessMenuButtonHint: function() {
					return this.getHint(resources.localizableStrings.StartProcessButtonHint);
				},

				
				getMenuButtonHint: function() {
					return this.getHint(resources.localizableStrings.MenuButtonHint);
				},

				
				getHint: function(hint) {
					var collapsed = this.get("Collapsed");
					if (!collapsed) {
						return hint;
					}
					return null;
				},

				
				getCollapseSideBarMenuItemCaptionConfig: function() {
					return {
						bindTo: "Collapsed",
						bindConfig: {
							converter: this.getCollapseSideBarMenuItemCaption
						}
					};
				},

				
				runProcess: function(tag) {
					ProcessModuleUtilities.executeProcess({
						sysProcessId: tag
					});
				},

				goTo: function() {
					var tag = arguments[3];
					var currentModule = this.sandbox.publish("GetHistoryState").hash.historyState;
					if (currentModule !== tag) {
						MaskHelper.ShowBodyMask();
						this.sandbox.publish("PushHistoryState", {hash: tag});
					}
				},

				exitClick: function() {
					ServiceHelper.callService("MainMenuService", "Logout", function() {
						window.logout = true;
						window.location.replace(this.Terrasoft.loaderBaseUrl + "?simpleLogin");
					}, {}, this);
				},

				goToFromMenu: function(tag) {
					var currentHistoryState = this.sandbox.publish("GetHistoryState").hash.historyState;
					if (currentHistoryState !== tag) {
						this.sandbox.publish("PushHistoryState", {hash: tag});
					}
				},

				goToIntroPageFromMenu: function(tag) {
					var currentHistoryState = this.sandbox.publish("GetHistoryState").hash.historyState;
					if (currentHistoryState !== tag) {
						var hash = "IntroPage/" + tag;
						this.sandbox.publish("PushHistoryState", {hash: hash});
					}
				},

				openProcessPage: function() {
					var vwSysProcessFilters = this.Terrasoft.createFilterGroup();
					vwSysProcessFilters.name = "vwSysProcessFiler";
					vwSysProcessFilters.logicalComparisonTypes = this.Terrasoft.LogicalOperatorType.AND;
					var sysWorkspaceFilter = this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "SysWorkspace",
						this.Terrasoft.SysValue.CURRENT_WORKSPACE.value);
					vwSysProcessFilters.addItem(sysWorkspaceFilter);
					var businessProcessTagFilter = this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "TagProperty",
						ConfigurationConstants.SysProcess.BusinessProcessTag);
					vwSysProcessFilters.addItem(businessProcessTagFilter);
					var isMaxVersionFilter = this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "IsMaxVersion", true);
					vwSysProcessFilters.addItem(isMaxVersionFilter);
					var config = {
						entitySchemaName: "VwSysProcess",
						isRunProcessPage: true,
						captionLookup: resources.localizableStrings.processLookupCaption,
						multiSelect: false,
						columnName: "Caption",
						filters: vwSysProcessFilters,
						hideActions: true
					};
					var handler = function(args) {
						var activeItems = args.selectedRows.getItems();
						if (!this.Ext.isEmpty(activeItems)) {
							ProcessModuleUtilities.executeProcess({
								sysProcessId: activeItems[0].Id
							});
						}
					};
					LookupUtilities.Open(this.sandbox, config, handler, this, null, false, false);
				},

				collapseSideBar: function() {
					LeftPanelUtilities.changeCollapsed();
				},

				showESN: function() {
					var esnHash = "SectionModuleV2/ESNFeedSectionV2/";
					var currentModule = this.sandbox.publish("GetHistoryState").hash.historyState;
					if (currentModule !== esnHash) {
						MaskHelper.ShowBodyMask();
						this.sandbox.publish("PushHistoryState", {hash: esnHash});
					}
				},

				
				getCollapseSideBarMenuItemCaption: function(isCollapsed) {
					if (this.Ext.isEmpty(isCollapsed)) {
						isCollapsed = LeftPanelUtilities.getDefaultCollapsed();
					}
					if (isCollapsed) {
						return resources.localizableStrings.expandSideBarMenuItemCaption;
					} else {
						return resources.localizableStrings.collapseSideBarMenuItemCaption;
					}
				},

				workPlaceMenuItemClick: function(tag) {
					var workplaceItem = this.getWorkplaceData(tag);
					if (workplaceItem) {
						this.sandbox.publish("ChangeCurrentWorkplace", tag);
					}
				},

				getWorkplaceData: function(workplaceId) {
					var workplaces = this.Terrasoft.configuration.WorkplacesStructure.Workplaces;
					var workplaceItem = null;
					if (workplaces.length > 0) {
						this.Terrasoft.each(workplaces, function(item) {
							if (item.workplaceId === workplaceId) {
								workplaceItem = item;
							}
						}, this);
					}
					return workplaceItem;
				},

				
				onSideBarCollapsedChanged: function(isCollapsed) {
					this.sandbox.publish("ChangeSideBarCollapsed", isCollapsed);
					this.set("Collapsed", isCollapsed);
				}

			});

			Ext.define("Terrasoft.configuration.LeftPanelTopMenuModule", {
				extend: "Terrasoft.BaseModule",
				isAsync: false,
				viewModel: null,
				viewModelClassName: "Terrasoft.LeftPanelTopMenuModuleViewModel",

				render: function(renderTo) {
					this.generate(renderTo);
				},

				init: function(callback, scope) {
					var viewModel = this.viewModel = this.getViewModel();
					callback = callback || Ext.emptyFn;
					scope = scope || this;
					viewModel.init(function() {
						callback.call(scope);
					}, this);
				},

				getViewModel: function() {
					return Ext.create(this.viewModelClassName, {
						Terrasoft: Terrasoft,
						Ext: Ext,
						sandbox: sandbox,
						values: {
							Collapsed: false,
							quickAddMenu: Ext.create("Terrasoft.BaseViewModelCollection"),
							startProcessMenu: Ext.create("Terrasoft.BaseViewModelCollection"),
							MainMenuItems: Ext.create("Terrasoft.BaseViewModelCollection"),
							IsSystemDesignerVisible: true,
							IsSSP: (Terrasoft.CurrentUser.userType === Terrasoft.UserType.SSP),
							ShowDelay: 0,
							HideDelay: 20
						}
					});
				},

				generate: function(container) {
					var viewModel = this.viewModel;
					var view = this.view;
					if (!Ext.isEmpty(viewModel) && !Ext.isEmpty(view)) {
						view.destroy();
					}
					var viewConfig = viewModel.getViewConfig();
					view = Ext.create("Terrasoft.Container", Terrasoft.deepClone(viewConfig));
					view.bind(viewModel);
					view.render(container);
					MaskHelper.HideBodyMask();
				}
			});

			return Terrasoft.configuration.LeftPanelTopMenuModule;
		}

		return createConstructor;
	});
