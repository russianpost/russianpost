define("ContactAddressPageV2", ["ConfigurationConstants"],
	function(ConfigurationConstants) {
		return {

			attributes: {},

			methods: {
				setValidationConfig: function() {
					this.callParent(arguments);
					this.addColumnValidator("ITHomeAddress", this.adressValidator);
					this.addColumnValidator("ITDeliveryAddress", this.adressValidator);
					this.addColumnValidator("ITOther", this.adressValidator);
					this.addColumnValidator("ITWorkAddress", this.adressValidator);
				},
				adressValidator: function() {
					var invalidMessage = "";
					
					var homeAddress = this.get("ITHomeAddress");
					if (homeAddress === undefined) {
						homeAddress = false;
					}
					var deliveryAdress = this.get("ITDeliveryAddress");
					if (deliveryAdress === undefined) {
						deliveryAdress = false;
					}
					var otherAddress = this.get("ITOther");
					if (otherAddress === undefined) {
						otherAddress = false;
					}
					var workAddress = this.get("ITWorkAddress");
					if (workAddress === undefined) {
						workAddress = false;
					}
					
					if (homeAddress === true && deliveryAdress === false && otherAddress === false && workAddress === false) {
						this.set("AddressType", {value: "4F8B2D67-71D0-45FB-897E-CD4A308A97C0", displayValue: "Домашний"});
					}
					if (homeAddress === false && deliveryAdress === true && otherAddress === false && workAddress === false) {
						this.set("AddressType", {value: "760BF68C-4B6E-DF11-B988-001D60E938C6", displayValue: "Доставки"});
					}
					if (homeAddress === false && deliveryAdress === false && otherAddress === true && workAddress === false) {
						this.set("AddressType", {value: "CE157F11-F425-4811-BA0E-E3816E8ECED5", displayValue: "Другой"});
					}
					if (homeAddress === false && deliveryAdress === false && otherAddress === false && workAddress === true) {
						this.set("AddressType", {value: "FB7A3F6A-F36B-1410-6F81-1C6F65E50343", displayValue: "Рабочий"});
					}


					//Для двух адресов
					if (homeAddress === true && deliveryAdress === true && otherAddress === false && workAddress === false) {
						this.set("AddressType", {value: "9031C86B-E38D-4B37-9204-9A0CC01BC97A", displayValue: "Доставки, Домашний"});
					}
					if (homeAddress === true && deliveryAdress === false && otherAddress === true && workAddress === false) {
						this.set("AddressType", {value: "FC91F98B-3383-41AB-BBD3-CB4E8A681ED6", displayValue: "Домашний, Другой"});
					}
					if (homeAddress === true && deliveryAdress === false && otherAddress === false && workAddress === true) {
						this.set("AddressType", {value: "0CADB44B-0CD5-4A16-86AE-1BEB23EE7CE5", displayValue: "Рабочий, Домашний"});
					}
					if (homeAddress === false && deliveryAdress === true && otherAddress === true && workAddress === false) {
						this.set("AddressType", {value: "5D197110-4E5D-46D8-9822-BBF1D5A48F0C", displayValue: "Доставки, Другой"});
					}
					if (homeAddress === false && deliveryAdress === true && otherAddress === false && workAddress === true) {
						this.set("AddressType", {value: "9DCE6E82-C6CC-4667-90B2-1A9C3644A2DB", displayValue: "Доставки, Рабочий"});
					}
					if (homeAddress === false && deliveryAdress === false && otherAddress === true && workAddress === true) {
						this.set("AddressType", {value: "C37EF1F3-0EE2-4872-9FA9-A48AE1C81E41", displayValue: "Рабочий, Другой"});
					}


					if (homeAddress === true && deliveryAdress === false && otherAddress === true && workAddress === true) {
						this.set("AddressType", {value: "209E001A-DC35-42FE-A0CB-7E624FEE8CC0", displayValue: "Рабочий, Домашний, Другой"});
					}
					if (homeAddress === true && deliveryAdress === true && otherAddress === false && workAddress === true) {
						this.set("AddressType", {value: "E68FFF13-F820-4FC3-A8F6-EC51813AF1E8", displayValue: "Доставки, Рабочий, Домашний"});
					}
					if (homeAddress === true && deliveryAdress === true && otherAddress === true && workAddress === false) {
						this.set("AddressType", {value: "CB614E64-E103-4B2B-BB37-31B58C5E56F1", displayValue: "Доставки, Домашний, Другой"});
					}
					if (homeAddress === false && deliveryAdress === true && otherAddress === true && workAddress === true) {
						this.set("AddressType", {value: "457EF119-6C7B-4F7D-B62F-EF2537D91551", displayValue: "Доставки, Рабочий, Другой"});
					}
					if (homeAddress === true && deliveryAdress === true && otherAddress === true && workAddress === true) {
						this.set("AddressType", {value: "EFADBC31-7A68-4E4B-AC1C-C7B9300095C2", displayValue: "Доставки, Рабочий, Домашний, Другой"});
					}


					if (homeAddress === false && deliveryAdress === false && otherAddress === false && workAddress === false) {
						this.set("AddressType", {value: null, displayValue: null});
						invalidMessage = "Необходимо указать хотя бы один тип адреса!";
					}
					return {
						fullInvalidMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
				}
			},

			diff: /**SCHEMA_DIFF*/ [

				{
					"operation": "insert",
					"name": "ITHomeAddress",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"bindTo": "ITHomeAddress",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 3
						}
					}
				},
				{
					"operation": "insert",
					"name": "ITDeliveryAddress",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"bindTo": "ITDeliveryAddress",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 6
						}
					}
				},
				{
					"operation": "insert",
					"name": "ITOther",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"bindTo": "ITOther",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 4
						}
					}
				},
				{
					"operation": "insert",
					"name": "ITWorkAddress",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"bindTo": "ITWorkAddress",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 5
						}
					}
				}
			] /**SCHEMA_DIFF*/
		};
	});