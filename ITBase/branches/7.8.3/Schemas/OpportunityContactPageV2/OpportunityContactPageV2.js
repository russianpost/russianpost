define("OpportunityContactPageV2", [], function() {
	return {

		entitySchemaName: "OpportunityContact",

		attributes: {
			"Contact": {
				lookupListConfig: {
					filter: function() {
						var opportunity = this.get("Opportunity");
						if (opportunity && opportunity.Account) {
							var filterGroup = new this.Terrasoft.createFilterGroup();
							filterGroup.add("FilterContactByAccount",
								Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
									"Account", opportunity.Account.value));
							return filterGroup;
						}
					},
					"hideActions" : true
				}
			},
			"Influence": {
				dependencies: [
					{
						columns: ["Role"],
						methodName: "setInfluence"
					}
				]
			}
		},

		methods: {
			onEntityInitialized: function() {
				this.callParent(arguments);
				this.setInfluence();
			},

			setInfluence: function() {
				var role = this.get("Role");
				var influence = this.get("Influence");
				var infHigh = {value: "cb35dec5-59f5-465c-a985-99f980079853", primaryImageVisible: false,
					displayValue: "Высокая", customHtml: "Высокая", markerValue: "Высокая"};
				if (role && role.value === "82ecb430-8d6b-40ea-8c01-741fe82cdcbd" && !influence) {
					this.set("Influence", infHigh);
				}
			}
		}
	};
});