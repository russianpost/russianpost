define("AccountAddressPageV2", ["ConfigurationConstants"],
	function(ConfigurationConstants) {
		return {

			attributes: {},

			methods: {
				setValidationConfig: function() {
					this.callParent(arguments);
					this.addColumnValidator("ITDeliveryAddress", this.adressValidator);
					this.addColumnValidator("ITActualAddress", this.adressValidator);
					this.addColumnValidator("ITLegalAddress", this.adressValidator);
				},
				adressValidator: function() {
					var invalidMessage = "";
					var deliveryAdress = this.get("ITDeliveryAddress");
					var actualAdress = this.get("ITActualAddress");
					var legalAdress = this.get("ITLegalAddress");
					if (deliveryAdress === true && actualAdress === false && legalAdress === false) {
						this.set("AddressType", {value: "760bf68c-4b6e-df11-b988-001d60e938c6", displayValue: "Доставки"});
					}
					if (actualAdress === true && deliveryAdress === false && legalAdress === false) {
						this.set("AddressType", {value: "780BF68C-4B6E-DF11-B988-001D60E938C6", displayValue: "Фактический"});
					}
					if (legalAdress === true && actualAdress === false && deliveryAdress === false) {
						this.set("AddressType", {value: "770BF68C-4B6E-DF11-B988-001D60E938C6", displayValue: "Юридический"});
					}
					
					
					if (legalAdress === true && actualAdress === false && deliveryAdress === true) {
						this.set("AddressType", {value: "42C6AD11-FC3D-4120-A9E3-4BFE76766F1B", displayValue: "Доставки, Юридеческий"});
					}
					if (legalAdress === true && actualAdress === true && deliveryAdress === false) {
						this.set("AddressType", {value: "5D17A9A4-80A6-47B7-9422-DB844E455C2E", displayValue: "Фактический, Юридический"});
					}
					if (legalAdress === false && actualAdress === true && deliveryAdress === true) {
						this.set("AddressType", {value: "A07CE182-DF3B-4D17-BAEA-B018CEF9C5B4", displayValue: "Доставки, Фактический"});
					}
					if (legalAdress === true && actualAdress === true && deliveryAdress === true) {
						this.set("AddressType", {value: "5E4AE0B0-4CB5-47B4-BE75-7925AD9F4459", displayValue: "Доставки, Юридический, Фактический"});
					}
					if (legalAdress === false && actualAdress === false && deliveryAdress === false) {
						invalidMessage = "Необходимо указать хотя бы один тип Адреса";
						this.set("AddressType", {value: null, displayValue: null});
					}
					return {
						fullInvalidMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
				}
			},

			diff: /**SCHEMA_DIFF*/ [

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITDeliveryAddress",
					"values": {
						bindTo: "ITDeliveryAddress",
						layout: { column: 0, row: 3, colSpan: 12 }
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITActualAddress",
					"values": {
						bindTo: "ITActualAddress",
						layout: { column: 0, row: 4, colSpan: 12 }
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITLegalAddress",
					"values": {
						bindTo: "ITLegalAddress",
						layout: { column: 0, row: 5, colSpan: 12 }
					}
				}
			] /**SCHEMA_DIFF*/
		};
	});