define("ITShipmentStartPrePage", ["CustomProcessPageV2Utilities"],
	function(CustomProcessPageV2Utilities) {
		return {

			mixins: {
				BaseProcessViewModel: "Terrasoft.CustomProcessPageV2Utilities"
			},

			attributes: {},

			rules: {},

			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,

			stages: [],

			methods: {
	/*			onEntityInitialized: function() {
					this.callParent(arguments);
				},*/

				//Возвращаем необходимое название для страницы
				getHeader: function() {
					return "Контроль начала отгрузок";
				},

				//Переопределение метода для прорисовки caption данной страницы
				getPageHeaderCaption: function() {
					var pageName = this.name;
					var header = pageName === "ITShipmentStartPrePage" ? this.getHeader() : "";
					return header;
				},

				initHeaderCaption: Ext.emptyFn,

				initPrintButtonMenu: Ext.emptyFn,
				
				loadVocabulary: function(args, tag) {
					var column = this.getColumnByName(tag);
					args.schemaName = column.referenceSchemaName;
					this.callParent(arguments);
				},

				getLookupPageConfig: function(args, columnName) {
					var config = {
						entitySchemaName: args.schemaName,
						multiSelect: false,
						columnName: columnName,
						columnValue: this.get(columnName),
						searchValue: args.searchValue,
						filters: this.getLookupQueryFilters(columnName)
					};
					this.Ext.apply(config, this.getLookupListConfig(columnName));
					return config;
				},

				//При нажатии на кнопку "Продолжить"
				onNextButtonClick: function() {
					var opportunityId = this.get("OppId"),
						date = this.get("GetShipmentDate");
					switch (this.Ext.isEmpty(date)) {
						case true:
							this.showInformationDialog("Сначала укажите дату первой отправки");
							break;
						default:
							var update = this.Ext.create("Terrasoft.UpdateQuery", {
								rootSchemaName: "Opportunity"
							});
							update.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
								this.Terrasoft.ComparisonType.EQUAL, "Id", opportunityId));
							update.setParameterValue("ITShipmentDate", date, this.Terrasoft.DataValueType.DATE);
							update.execute(function() {
								this.acceptProcessElement("NextButton");
							}, this);
					}
				},

				onCloseCardButtonClick: function() {
					this.sandbox.publish("BackHistoryState");
				}
			},

			diff: /**SCHEMA_DIFF*/[
				// Удаляем лишние кнопки
				{
					operation: "remove",
					name: "ActionButtonsContainer"
				},

				{
					operation: "remove",
					name: "DiscardChangesButton"
				},

				{
					operation: "remove",
					name: "CloseButton"
				},

	/*			{
					"operation": "remove",
					"name": "Tabs"
				},*/

				{
					operation: "remove",
					name: "DelayExecutionButton"
				},

				{
					operation: "remove",
					name: "ViewOptionsButton"
				},

				{
					operation: "remove",
					name: "SaveButton"
				},

				{
					"operation": "remove",
					"name": "actions"
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "DelayButton",
					"values": {
						"caption": "Закрыть",
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"classes": {textClass: "actions-button-margin-right"},
						"style": Terrasoft.controls.ButtonEnums.style.RED,
						"click": {bindTo: "onCloseCardButtonClick"},
						"layout": {column: 0, row: 4, colSpan: 4}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "NextButton",
					"values": {
						"caption": "Продолжить",
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"classes": {textClass: "actions-button-margin-right"},
						"style": Terrasoft.controls.ButtonEnums.style.GREEN,
						"click": {bindTo: "onNextButtonClick"},
						"layout": {column: 4, row: 4, colSpan: 4}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "GetShipmentDate",
					"values": {
						"caption": "Дата первой отправки",
						"bindTo": "GetShipmentDate",
						"dataValueType": Terrasoft.DataValueType.DATE,
						"layout": {column: 0, row: 0, colSpan: 6}
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});