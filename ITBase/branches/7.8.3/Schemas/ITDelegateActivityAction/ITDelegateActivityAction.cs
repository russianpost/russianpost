using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using System.Linq;

namespace Terrasoft.Configuration.ITBase
{
    public class ITDelegateActivityAction
    {
        public string GetContactDecisionRoleId(string contactId, UserConnection userConnection)
        {
            string result = "";
            try
            {
	            Guid id = new Guid(contactId);
	            var selectContact = new Select(userConnection).Column("DecisionRoleId").From("Contact")
	                .Where("Id").IsEqual(Column.Parameter(id)) as Select;
	            using (DBExecutor executor = userConnection.EnsureDBConnection())
	            {
	                using (IDataReader reader = selectContact.ExecuteReader(executor))
	                {
	                    while (reader.Read())
	                    {
	                        var r = reader.GetColumnValue<Guid>("DecisionRoleId");
	                        if (r != null && r != Guid.Empty) result = r.ToString();
	                    }
	                }
	            }
            }
            catch(Exception ex)
            {
            	var insert = new Insert(userConnection).Into("AcademyURL")
            	.Set("Id", Column.Parameter(Guid.NewGuid()))
            	.Set("Name", Column.Parameter("Error"))
            	.Set("Description", Column.Parameter(String.Format(Environment.NewLine + "InnerException - [{0}]" + Environment.NewLine + "StackTrace - [{1}]" + Environment.NewLine + "Message - [{2}]" , ex.InnerException , ex.StackTrace , ex.Message)))
            	.Set("ProcessListeners", Column.Parameter(0))
            	;
                
                insert.Execute();
                throw new Exception(ex.Message);
            }
            return result;
        }
    }
}
