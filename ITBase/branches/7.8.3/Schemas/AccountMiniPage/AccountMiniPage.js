define("AccountMiniPage", ["ServiceHelper", "MiniPageResourceUtilities", "ConfigurationConstants",
	"AccountMiniPageResources", "css!AccountMiniPageCSS"],
	function(ServiceHelper, miniPageResources, ConfigurationConstants, resources) {
		return {

			entitySchemaName: "Account",

			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,

			attributes: {
	/*			"Name": {
					"onChange": "onNameChanged"
				},
				"TsINN": {
					"onChange": "onINNChanged"
				},
				"TsKPP": {
					"onChange": "onKPPChange"
				},*/

				// Фильтрация поля "Основной контакт", показываются только те контакты, у которых тип контрагента "Наша компания"
				"PrimaryContact": {
					"lookupListConfig": {
						filter: function() {
							return Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.NOT_EQUAL, "Account.Type",
									ConfigurationConstants.AccountType.OurCompany);
						}
					}
				},

				"InnAndKppAvailability": {
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				"onChangeOwnership": {
					dependencies: [
						{
							columns: ["Ownership"],
							methodName: "enableInnAndKpp"
						}
					]
				},

				"TsKPPvisibility": {
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: true
				},
				
				"onChangeInnKpp": {
					dependencies: [
						{
							columns: ["TsINN", "TsKPP"],
							methodName: "searchDuplicates"
						}
					]
				}
			},
			messages: {
				//#RPCRM-735 получаем данные от страницы дублей по новому контрагенту
				"NewAccountToMiniPage": {
					mode: this.Terrasoft.MessageMode.PTP,
					direction: this.Terrasoft.MessageDirectionType.PUBLISH
				}
			},
			methods: {
				// Поиск дублей по ИНН и КПП.
				searchDuplicates: function() {
					if (!this.isAddMode()) {return; }
					if (this.get("Ownership") && this.get("Ownership").value === "350469c4-40f9-49c5-8298-7f8a38548050" &&
						this.get("TsINN")) {
						var arr = [], tsinn = this.get("TsINN"),
						msg = "Контрагент с таким ИНН уже существует. Перейти на страницу редактирования?",
						msgDoubles = "Найдено более одной записи с таким ИНН. Перейти к поиску дублей контрагентов?",
						url1 = "CardModuleV2/AccountPageV2/edit/";
						var name = Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "Account"
						});
						name.addColumn("Id");
						name.filters.add("ByINN", Terrasoft.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.EQUAL, "TsINN", tsinn));
						name.getEntityCollection(function(response) {
							if (response.success && response.collection.collection.length) {
								var length = response.collection.collection.length;
								var isExist = (length !== 0);
								if (isExist && length === 1) {
									response.collection.each(function(item) {
										arr.push(item.get("Id"));
									});
									url1 += arr[0];
									this.showConfirmationDialog(msg,
										function(returnCode) {
											if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
												this.sandbox.publish("PushHistoryState", {
													hash: url1,
													silent: false
												});
												this.close();
											}
										}, ["yes", "cancel"]);
								} else if (isExist && length > 1) {
									this.showConfirmationDialog(msgDoubles,
										function(returnCode) {
											if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
												this.sandbox.publish("PushHistoryState", {hash: "DuplicatesModule/Account"});
												this.close();
											}
										}, ["yes", "cancel"]);
								} else {return; }
							}
						}, this);
					}
					if (this.get("TsINN") && this.get("TsKPP")) {
						var array = [], inn = this.get("TsINN"), kpp = this.get("TsKPP"),
						message = "Контрагент с таким ИНН и КПП уже существует. Перейти на страницу редактирования?",
						messageDoubles = "Найдено более одной записи с таким ИНН и КПП. Перейти к поиску дублей контрагентов?",
						url = "CardModuleV2/AccountPageV2/edit/";
	//					if (this.Ext.isEmpty(inn) || this.Ext.isEmpty(kpp)) {return; }
						var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "Account"
						});
						esq.addColumn("Id");
						esq.filters.add("ByINN", Terrasoft.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.EQUAL, "TsINN", inn));
						esq.filters.add("ByKPP", Terrasoft.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.EQUAL, "TsKPP", kpp));
						esq.getEntityCollection(function(response) {
							if (response.success && response.collection.collection.length) {
								var length = response.collection.collection.length;
								var isExist = (length !== 0);
								if (isExist && length === 1) {
									response.collection.each(function(item) {
										array.push(item.get("Id"));
									});
									url += array[0];
									this.showConfirmationDialog(message,
										function(returnCode) {
											if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
												this.sandbox.publish("PushHistoryState", {
													hash: url,
													silent: false
												});
												this.close();
											}
										}, ["yes", "cancel"]);
								} else if (isExist && length > 1) {
									this.showConfirmationDialog(messageDoubles,
										function(returnCode) {
											if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
												this.sandbox.publish("PushHistoryState", {hash: "DuplicatesModule/Account"});
												this.close();
											}
										}, ["yes", "cancel"]);
								} else {return; }
							}
						}, this);
					}
				},
				
				onEntityInitialized: function() {
					this.callParent(arguments);
					this.regionByContact();
					this.setRegionAndMacroregion();
				},
				
				close: function() {
					this.callParent(arguments);
				},

	/*			//Срабатывает при изменении колонки "Название"
				onNameChanged: function() {
	//				Если не режим добвления новой записи, то проверки на дубли не происходит
					if (!this.isAddMode()) {return; }
					var array = [], txt = this.get("Name"),
					message = "Контрагент с таким названием уже существует. Перейти на страницу редактирования?",
					messageDoubles = "Найдено более одной записи с таким названием. Перейти к поиску дублей контрагентов?",
					url = "CardModuleV2/AccountPageV2/edit/";
					if (this.Ext.isEmpty(txt)) {return; }
					var name = Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Account"
					});
					name.addColumn("Id");
					name.filters.add("ByName", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "Name", txt));
					name.getEntityCollection(function(response) {
						var length = response.collection.collection.length;
						var isExist = (length !== 0);
						if (isExist && length === 1) {
							response.collection.each(function(item) {
								array.push(item.get("Id"));
							});
							url += array[0];
							this.showConfirmationDialog(message,
								function(returnCode) {
									if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
										this.sandbox.publish("PushHistoryState", {
											hash: url,
											silent: false
										});
										this.close();
									}/* else if (returnCode === Terrasoft.MessageBoxButtons.CANCEL.returnCode) {
										this.set("Name", "");
									}
								}, ["yes", "cancel"]);
						} else if (isExist && length > 1) {
							this.showConfirmationDialog(messageDoubles,
								function(returnCode) {
									if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
										this.sandbox.publish("PushHistoryState", {hash: "DuplicatesModule/Account"});
										this.close();
									}/* else if (returnCode === Terrasoft.MessageBoxButtons.CANCEL.returnCode) {
										this.set("Name", "");
									}
								}, ["yes", "cancel"]);
						} else {return; }
					}, this);
				},*/

	/*			//Метод срабатывает при изменении колонки "ИНН"
				onINNChanged: function() {
	//				Если не режим добвления новой записи, то проверки на дубли не происходит
					if (!this.isAddMode()) {return; }
					var array = [], txt = this.get("TsINN"),
					message = "Контрагент с таким ИНН уже существует. Перейти на страницу редактирования?",
					messageDoubles = "Найдено более одной записи с таким ИНН. Перейти к поиску дублей контрагентов?",
					url = "CardModuleV2/AccountPageV2/edit/";
					if (this.Ext.isEmpty(txt)) {return; }
					var name = Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Account"
					});
					name.addColumn("Id");
					name.filters.add("ByINN", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "TsINN", txt));
					name.getEntityCollection(function(response) {
						var length = response.collection.collection.length;
						var isExist = (length !== 0);
						if (isExist && length === 1) {
							response.collection.each(function(item) {
								array.push(item.get("Id"));
							});
							url += array[0];
							this.showConfirmationDialog(message,
								function(returnCode) {
									if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
										this.sandbox.publish("PushHistoryState", {
											hash: url,
											silent: false
										});
										this.close();
									}/* else if (returnCode === Terrasoft.MessageBoxButtons.CANCEL.returnCode) {
										this.set("TsINN", "");
									}
								}, ["yes", "cancel"]);
						} else if (isExist && length > 1) {
							this.showConfirmationDialog(messageDoubles,
								function(returnCode) {
									if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
										this.sandbox.publish("PushHistoryState", {hash: "DuplicatesModule/Account"});
										this.close();
									}/* else if (returnCode === Terrasoft.MessageBoxButtons.CANCEL.returnCode) {
										this.set("TsINN", "");
									}
								}, ["yes", "cancel"]);
						} else {return; }
					}, this);
				},*/

	/*			//Метод срабатывает при изменении колонки "КПП"
				onKPPChange: function() {
	//				Если не режим добвления новой записи, то проверки на дубли не происходит
					if (!this.isAddMode()) {return; }
					var array = [], txt = this.get("TsKPP"),
					message = "Контрагент с таким КПП уже существует. Перейти на страницу редактирования?",
					messageDoubles = "Найдено более одной записи с таким КПП. Перейти к поиску дублей контрагентов?",
					url = "CardModuleV2/AccountPageV2/edit/";
					if (this.Ext.isEmpty(txt)) {return; }
					var name = Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Account"
					});
					name.addColumn("Id");
					name.filters.add("ByKPP", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "TsKPP", txt));
					name.getEntityCollection(function(response) {
						var length = response.collection.collection.length;
						var isExist = (length !== 0);
						if (isExist && length === 1) {
							response.collection.each(function(item) {
								array.push(item.get("Id"));
							});
							url += array[0];
							this.showConfirmationDialog(message,
								function(returnCode) {
									if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
										this.sandbox.publish("PushHistoryState", {
											hash: url,
											silent: false
										});
										this.close();
									}/* else if (returnCode === Terrasoft.MessageBoxButtons.CANCEL.returnCode) {
										this.set("TsKPP", "");
									}
								}, ["yes", "cancel"]);
						} else if (isExist && length > 1) {
							this.showConfirmationDialog(messageDoubles,
								function(returnCode) {
									if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
										this.sandbox.publish("PushHistoryState", {hash: "DuplicatesModule/Account"});
										this.close();
									}/* else if (returnCode === Terrasoft.MessageBoxButtons.CANCEL.returnCode) {
										this.set("TsKPP", "");
									}
								}, ["yes", "cancel"]);
						} else {return; }
					}, this);
				},*/

				// Переопределение метода. Если не заполнено поле 'Форма собственности', не даёт сохранить мини-карточку.
				save: function() {
					var ownership = this.get("Ownership");
					if (ownership) {
						//#RPCRM-735 очистка таблицы дублей нового контрагента в случае сохранения
						var serviceData = {
							currentContactId: Terrasoft.core.enums.SysValue.CURRENT_USER_CONTACT.value
						};
						ServiceHelper.callService("ITDuplicationSearch", "ClearTableDuplicationProcedure", function(response) {
							var result = response.ClearTableDuplicationProcedureResult;
							if (result !== "Good") {
								this.showInformationDialog(result);
							}
						}, serviceData, this);
						// конец #RPCRM-735
						this.callParent(arguments);
					} else {
						this.showInformationDialog("Необходимо заполнить поле 'Форма собственности'");
					}
				},
				
				// #RPCRM-987 Если поля Регион или макрорегион контрагента пустые, то они проставляются по пользователю,
				// создавшему данную карточку
				setRegionAndMacroregion: function() {
					if (this.isAddMode()) {
						var currentUserContact = Terrasoft.SysValue.CURRENT_USER_CONTACT;
						var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
								rootSchemaName: "Contact"
							});
						esq.addColumn("ITMacroregion", "ITMacroregion");
						esq.addColumn("TsTarifficatorRegion", "TsTarifficatorRegion");
						esq.getEntity(currentUserContact.value, function(result) {
							if (result.success) {
								var macroregion = result.entity.get("ITMacroregion"),
									region = result.entity.get("TsTarifficatorRegion");
								if (!this.Ext.isEmpty(macroregion) || !this.Ext.isEmpty(region)) {
									var batchQuery = this.Ext.create("Terrasoft.BatchQuery");
									if (!this.Ext.isEmpty(macroregion)) {
										this.set("ITMacroregion", macroregion);
										var updateMacroregion = this.Ext.create("Terrasoft.UpdateQuery", { rootSchemaName: "Account" });
										updateMacroregion.enablePrimaryColumnFilter(this.get("Id"));
										updateMacroregion.setParameterValue("ITMacroregion", macroregion.value,
											this.Terrasoft.DataValueType.GUID);
										batchQuery.add(updateMacroregion);
									}
									if (!this.Ext.isEmpty(region)) {
										this.set("TsTarifficatorRegion", region);
										var updateRegion = this.Ext.create("Terrasoft.UpdateQuery", { rootSchemaName: "Account" });
										updateRegion.enablePrimaryColumnFilter(this.get("Id"));
										updateRegion.setParameterValue("TsTarifficatorRegion", region.value,
											this.Terrasoft.DataValueType.GUID);
										batchQuery.add(updateRegion);
									}
									batchQuery.execute();
								}
							}
						}, this);
					}
				},
				
				// Валидация поля ИНН.
				identificationNumberValidator: function() {
					var invalidMessage = "", ownership = this.get("Ownership"), ipId = "350469c4-40f9-49c5-8298-7f8a38548050";
					if (!this.Ext.isEmpty(this.get("TsINN"))) {
						var innValue = this.get("TsINN");
						var digitsPattern = /^\d+$/;
						if (!innValue.match(digitsPattern)) {
							invalidMessage = resources.localizableStrings.InnDigitsException;
						} else {
							var currentINNLength = innValue.toString().length;
							if (ownership && ownership.value === ipId && currentINNLength !== 12) {
								invalidMessage = "Длина ИНН для ИП должна составлять 12 символов";
							}
							if (ownership && ownership.value !== ipId && currentINNLength !== 10) {
								invalidMessage = "Длина ИНН должна составлять 10 символов";
							}
						}
					}
					return {
						fullInvalidMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
				},

				// Валидация поля КПП.
				kppValueValidator: function() {
					var invalidMessage = "";
					if (!this.Ext.isEmpty(this.get("TsKPP"))) {
						var kppValue = this.get("TsKPP");
						var currentKppLength = kppValue.toString().length;
						if (currentKppLength !== 9) {
							invalidMessage = "Длина КПП должна составлять 9 символов";
						}
					}
					return {
						fullInvalidMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
				},

				setValidationConfig: function() {
					this.callParent(arguments);
					this.addColumnValidator("TsKPP", this.kppValueValidator);
					this.addColumnValidator("TsINN", this.identificationNumberValidator);
				},

				// Регулирует доступность полей "ИНН" и "КПП" для редактирования.
				enableInnAndKpp: function() {
					var ownership = this.get("Ownership");
					if (ownership) {
						this.set("InnAndKppAvailability", true);
					} else {
						this.set("InnAndKppAvailability", false);
					}
					if (ownership && ownership.value === "350469c4-40f9-49c5-8298-7f8a38548050") {
						this.set("TsKPPvisibility", false);
					} else {
						this.set("TsKPPvisibility", true);
					}
				},
				
				// Автозаполнение поля "Регион обслуживания" при создании контрагента. Значение берётся из контакта,
				// которым был создан контрагент.
				regionByContact: function() {
					if (!this.isAddMode()) {return; }
					var currentContactId = Terrasoft.core.enums.SysValue.CURRENT_USER_CONTACT.value;
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Contact"
					});
					esq.addColumn("TsTarifficatorRegion", "TsTarifficatorRegion");
					esq.getEntity(currentContactId, function(result) {
						if (result.success) {
							this.set("TsTarifficatorRegion", result.entity.get("TsTarifficatorRegion"));
						}
					}, this);
				},
				//#RPCRM-735 до сохранения проверяем на дубли
				checkForDuplicates: function() {
					var name = this.get("Name"),
						primaryContact = this.get("PrimaryContact"),
						phone = this.get("Phone"),
						web = this.get("Web"),
						ownership = this.get("Ownership"),
						inn = this.get("TsINN"),
						kpp = this.get("TsKPP"),
						currentContactId = Terrasoft.core.enums.SysValue.CURRENT_USER_CONTACT.value;
					if (ownership) {
						ownership = ownership.Id;
					}
					if (primaryContact) {
						primaryContact = primaryContact.Id;
					}
					var serviceData = {
						accountName: name,
						primaryContact: primaryContact,
						phone: phone,
						web: web,
						ownership: ownership,
						inn: inn,
						kpp: kpp,
						currentContactId: currentContactId
					};
					for (var key in serviceData) {
						var value = serviceData[key];
						if (value === undefined || value === null) {
							if (key === "primaryContact" || key === "ownership") {
								value = "00000000-0000-0000-0000-000000000000";
							}
							if (key === "kpp") {
								value = 0;
							}
							if (key === "phone" || key === "web" || key === "inn") {
								value = " ";
							}
						}
						serviceData[key] = value;
					}
					ServiceHelper.callService("ITDuplicationSearch", "StartSearchDuplicationProcedure", function(response) {
						var result = response.StartSearchDuplicationProcedureResult;
						if (result === "Good") {
							var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
								rootSchemaName: "ITSearchNewAccount"
							});
							esq.getEntityCollection(function(result) {
								if (!result.success) {
									this.showInformationDialog("Ошибка запроса данных");
									return;
								}
								if (result.collection.collection.length === 0) {
									var newAccountData = this.sandbox.publish("NewAccountToMiniPage", null);
									this.save();
									if (newAccountData) {
										window.location.reload();
									}
								}
								else {
									var str,
										message;
									if (inn === undefined || inn === "" || kpp === undefined || kpp === null) {
										str = "ИНН";
										if ((inn === undefined  || inn === "") && (kpp === undefined || kpp === null)) {
											str = "ИНН и КПП";
										}
										if ((inn !== undefined  && inn !== "") && (kpp === undefined || kpp === null)) {
											str = "КПП";
										}
										message = "Обнаружены дубли! Заполнить " + str + " для более точного поиска?";
										this.showConfirmationDialog(message,
											function(returnCode) {
												if (returnCode === Terrasoft.MessageBoxButtons.NO.returnCode) {
													message = "Перейти на страницу дублей?";
													this.messageToClient(message);
												}
											}, ["yes", "no"]);
									}
									else {
										message = "Обнаружены дубли! Перейти на страницу дублей?";
										this.messageToClient(message);
									}
								}
							}, this);
						}
						else {
							this.showInformationDialog(result);
						}
					}, serviceData, this);
				},
				//#RPCRM-735 сообщение выведено в отдельный метод, чтобы избежать повторений
				messageToClient: function(message) {
					this.showConfirmationDialog(message,
						function(returnCode) {
							var newAccountData = this.sandbox.publish("NewAccountToMiniPage", null);
							if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
								if (newAccountData) {
									window.location.reload();
								}
								else {
									var url = "CardModuleV2/DuplicatesPageV2/Account";
									this.sandbox.publish("PushHistoryState", {
										hash: url,
										silent: false
									});
									this.close();
								}
							}
							else {
								this.save();
								if (newAccountData) {
									window.location.reload();
								}
							}
						}, ["yes", "no"]);
				},
				init: function() {
					//#RPCRM-735 добавляем проверку сообщения от страницы дублей
					this.publishNewAccountToMiniPage();
					this.callParent(arguments);
				},
				//#RPCRM-735 если миникарточка вызвана из страницы дублей, она будет заполнятся ранее введенными данными
				publishNewAccountToMiniPage: function() {
					var newAccountData = this.sandbox.publish("NewAccountToMiniPage", null);
					if (newAccountData) {
						for (var values in newAccountData) {
							if (values !== "Id" && values !== "TsTarifficatorRegion") {
								if (newAccountData[values] !== "" && newAccountData[values] !== " " && newAccountData[values] !== 0) {
									this.set(values, newAccountData[values]);
								}
							}
						}
					}
				}
			},

			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "insert",
					"name": "TsINNEdit",
					"parentName": "MiniPageEditModeContainer",
					"propertyName": "items",
					"values": {
						"tip": {
							"content": { "bindTo": "Resources.Strings.InnAndKppTipContent"},
							"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
						},
						"visible": {"bindTo": "isNotViewMode"},
						"enabled": {"bindTo": "InnAndKppAvailability"},
						"bindTo": "TsINN",
						"layout": {
							"column": 0,
							"row": 4,
							"colSpan": 24
						},
						"isMiniPageModelItem": true
					},
					"index": 6
				},
				{
					"operation": "insert",
					"name": "TsKPPEdit",
					"parentName": "MiniPageEditModeContainer",
					"propertyName": "items",
					"values": {
						"tip": {
							"content": { "bindTo": "Resources.Strings.InnAndKppTipContent"},
							"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
						},
	//					"visible": {"bindTo": "isNotViewMode"},
						"visible": {"bindTo": "TsKPPvisibility"},
						"enabled": {"bindTo": "InnAndKppAvailability"},
						"bindTo": "TsKPP",
						"layout": {
							"column": 0,
							"row": 5,
							"colSpan": 24
						},
						"isMiniPageModelItem": true
					},
					"index": 7
				},
				{
					"operation": "merge",
					"name": "PrimaryContactEdit",
					"parentName": "MiniPageEditModeContainer",
					"propertyName": "items",
					"values": {
						"visible": {"bindTo": "isNotViewMode"},
						"bindTo": "PrimaryContact",
						"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
						"contentType": this.Terrasoft.ContentType.LOOKUP,
						"layout": {
							"column": 0,
							"row": 1,
							"colSpan": 24
						},
						"isMiniPageModelItem": true,
						"showValueAsLink": false
					}
				},
				{
					"operation": "merge",
					"name": "WebInEditMode",
					"values": {
						"caption": "Сайт",
						"layout": {
							"column": 0,
							"row": 3,
							"colSpan": 24
						}
					}
				},
				{
					"operation": "insert",
					"name": "Ownership",
					"parentName": "MiniPageEditModeContainer",
					"propertyName": "items",
					"values": {
						"visible": {"bindTo": "isNotViewMode"},
						"bindTo": "Ownership",
						"isRequired": true,
						"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
						"contentType": this.Terrasoft.ContentType.LOOKUP,
						"isMiniPageModelItem": true,
						"layout": {
							"column": 0,
							"row": 6,
							"colSpan": 24
						}
					},
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsTarifficatorRegion",
					"parentName": "MiniPageEditModeContainer",
					"propertyName": "items",
					"values": {
						"visible": false,
						"bindTo": "TsTarifficatorRegion",
						"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
						"contentType": this.Terrasoft.ContentType.LOOKUP,
						"isMiniPageModelItem": true,
						"layout": {
							"column": 0,
							"row": 7,
							"colSpan": 24
						}
					},
					"index": 8
				},
				{
					"operation": "merge",
					"name": "SaveEditButton",
					"values": {
						"click": {"bindTo": "checkForDuplicates"}
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});
