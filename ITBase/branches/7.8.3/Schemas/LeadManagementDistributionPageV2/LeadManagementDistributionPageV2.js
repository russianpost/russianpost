define("LeadManagementDistributionPageV2", ["ConfigurationConstants","BusinessRuleModule"], function(ConfigurationConstants, BusinessRuleModule) {
    return {
        details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
        attributes: {
            "Owner": {
                dataValueType: Terrasoft.DataValueType.LOOKUP,
                isRequired: true,
                lookupListConfig: {
                    filter: function () {
                        var owner = this.get("Owner");
                        var filterGroup = Ext.create("Terrasoft.FilterGroup");
                        if (owner) {
                            filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.OR;
                            filterGroup.add("CurrentOwnerLead",
                                Terrasoft.createColumnFilterWithParameter(
                                    Terrasoft.ComparisonType.EQUAL,
                                    "Id",
                                    owner.value));
                            filterGroup.add("LeadNotEmpty",
                                Terrasoft.createColumnFilterWithParameter(
                                    Terrasoft.ComparisonType.EQUAL,
                                    "Owner.Id",
                                    owner.value));
                        }
                        else {
                            var leadType = this.get("LeadType");
                            filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.AND;
                            filterGroup.add("TypeFilter",
                                Terrasoft.createColumnFilterWithParameter(
                                    Terrasoft.ComparisonType.EQUAL,
                                    "Type.Id",
                                    ConfigurationConstants.ContactType.Employee));
                            if (leadType) {
                                filterGroup.add("CurrentOwnerLead",
                                    Terrasoft.createColumnFilterWithParameter(
                                        Terrasoft.ComparisonType.EQUAL,
                                        "[ITContactLeadType:ITContact].ITLeadType.Id",
                                        leadType.value));
                            }
                        }
                        return filterGroup;
                    }

                }
            }
        },
        messages: {},
        modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
        diff: /**SCHEMA_DIFF*/[
            {
                "operation": "merge",
                "parentName": "LeftContainer",
                "propertyName": "items",
                "name": "DistributionButton",
                "values": {
                    "itemType": Terrasoft.ViewItemType.BUTTON,
                    "caption": {"bindTo": "Resources.Strings.DistributeButtonCaption"},
                    "click": {"bindTo": "setTransferToSale"},
                    "classes": {
                        textClass: "actions-button-margin-right",
                        "wrapperClass": ["actions-button-margin-right"]
                    },
                    "style": Terrasoft.controls.ButtonEnums.style.GREEN,
                    "controlConfig": {},
                    "layout": {column: 0, row: 0, colSpan: 2}
                },
                "index": 0
            },
            {
                "operation": "move",
                "name": "Owner",
                "parentName": "Header",
                "propertyName": "items",
                "values": {
                    "layout": {"column": 0, "row": 0}
                }
            },
            {
                "operation": "remove",
                "name": "QualifiedContact"
            },
            {
                "operation": "remove",
                "name": "QualifiedAccount"
            },
            {
                "operation": "remove",
                "name": "LeadType"
            },
            {
                "operation": "remove",
                "name": "TransferToSaleTab"
            },
            {
                "operation": "remove",
                "name": "LeadTab"
            },
            {
                "operation": "remove",
                "name": "Leads"
            },
            {
                "operation": "remove",
                "name": "ContactInFolderTab"
            },
            {
                "operation": "remove",
                "name": "ContactsInFolder"
            },
            {
                "operation": "remove",
                "name": "TransferToSaleDataContainer"
            },
            {
                "operation": "remove",
                "name": "TransferToSaleBlock"
            },
            {
                "operation": "remove",
                "name": "ContactDataBlock"
            },
            {
                "operation": "remove",
                "name": "AccountDataBlock"
            },
            {
                "operation": "remove",
                "name": "ContactDataBlockGridLayout"
            },
            {
                "operation": "remove",
                "name": "AccountDataBlockGridLayout"
            },
            {
                "operation": "remove",
                "name": "TransferToSaleBlockGridLayout"
            },

            {
                "operation": "remove",
                "name": "RemindToOwner"
            },
            {
                "operation": "remove",
                "name": "OpportunityDepartment"
            },
            {
                "operation": "remove",
                "name": "ContactName"
            },
            {
                "operation": "remove",
                "name": "ContactDepartment"
            },
            {
                "operation": "remove",
                "name": "ContactJob"
            },
            {
                "operation": "remove",
                "name": "ContactGender"
            },
            {
                "operation": "remove",
                "name": "ContactJobTitle"
            },
            {
                "operation": "remove",
                "name": "ContactDecisionRole"
            },
            {
                "operation": "remove",
                "name": "AccountName"
            },
            {
                "operation": "remove",
                "name": "Category"
            },
            {
                "operation": "remove",
                "name": "Ownership"
            },
            {
                "operation": "remove",
                "name": "AccountEmployeesNumber"
            },
            {
                "operation": "remove",
                "name": "AccountIndustry"
            },
            {
                "operation": "remove",
                "name": "AccountAnnualRevenue"
            }
        ]/**SCHEMA_DIFF*/,
        methods: {
            onEntityInitialized: function() {
                this.callParent(arguments);
            }
        },
        rules: {
            "Owner": {
                RequireOwner: {
                    ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
                    property: BusinessRuleModule.enums.Property.REQUIRED,
                    conditions: [{
                        leftExpression: {
                            type: BusinessRuleModule.enums.ValueType.ATTRIBUTE,
                            attribute: "Owner"
                        },
                        comparisonType: this.Terrasoft.ComparisonType.IS_NULL
                    }]
                }
            }
        }
    };
});
