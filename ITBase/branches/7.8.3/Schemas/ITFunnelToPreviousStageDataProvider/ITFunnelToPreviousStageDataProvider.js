define("ITFunnelToPreviousStageDataProvider", ["ext-base", "FunnelToPreviousStageDataProvider", "FunnelConversionDataProvider" ],
    function(Ext) {

        Ext.define("Terrasoft.configuration.ITFunnelToPreviousStageDataProvider", {
            extend: "Terrasoft.FunnelToPreviousStageDataProvider",
            alternateClassName: "Terrasoft.ITFunnelToPreviousStageDataProvider",

            prepareConversionResponse: function(responseCollection) {
                this.callParent(arguments);
                var countStore = {};
                var thereIsSomeData;
                responseCollection.each(function(currentItem, itemNumber) {
                    var itemValue = currentItem.get("yAxis") || 0;
                    if (thereIsSomeData === undefined && itemValue === 0) {
                        return;
                    }
                    thereIsSomeData = true;
                    var bottomValue = 0;
                    var prevValue;
                    if (itemNumber !== 0 && (prevValue = countStore[itemNumber - 1]) > 0) {
                        bottomValue = prevValue;
                    } else {
                        bottomValue = itemValue;
                    }
                    var stage = this.getItemStage(currentItem);
                    var storeValue = itemValue;
                    if (stage.end) {
                        storeValue = prevValue;
                    }
                    countStore[itemNumber] = storeValue;
                    currentItem.set("bottomConversionValue", bottomValue);
                }, this);
            },

            initOpportunityStageCollection: function(callback, scope) {
                var entitySchemaQuery = this.getStagesEsq();
                entitySchemaQuery.getEntityCollection(function(response) {
                    if (response && response.success) {
                        this.opportunityStageCollection = response.collection;
                        callback.call(scope);
                    }
                }, this);
            },

            getFunnelAllowedStagesFilters: function(parentSchemaName) {
                var filter = parentSchemaName ?
                    "[OpportunityInStage:Opportunity].Stage.[TsOppStageInLeadType:TsOpportunityStage].ITShowInFunnel" :
                    "[TsOppStageInLeadType:TsOpportunityStage].ITShowInFunnel";
                return Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
                    filter, true, Terrasoft.DataValueType.BOOLEAN);
            },

            getFunnelFixedFilters: function() {
                var esqFiltersGroup = this.callParent(arguments);
                if (this.historyState.leadType) {
                    esqFiltersGroup.addItem(Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
                        "LeadType.Id", this.historyState.leadType.value, Terrasoft.DataValueType.GUID));
                }
                return esqFiltersGroup;
            },

            getStagesEsq: function() {
                var currentCultureId = Terrasoft.SysValue.CURRENT_USER_CULTURE.value;
                var cacheValue = this.historyState.leadType ? this.historyState.leadType.value : "";
                var leadTypeFilterValue = this.historyState.leadType ? this.historyState.leadType.value : Terrasoft.GUID_EMPTY;
                var entitySchemaQuery = Ext.create("Terrasoft.EntitySchemaQuery", {
                    rootSchemaName: "OpportunityStage",
                    clientESQCacheParameters: {
                        cacheItemName: "OpportunityStagesFunnelData_" + cacheValue
                    }
                });
                var allowedStageFiltersGroup = this.getFunnelAllowedStagesFilters();
                entitySchemaQuery.filters.addItem(allowedStageFiltersGroup);
                entitySchemaQuery.filters.add("LeadTypeFilter", Terrasoft.createColumnFilterWithParameter(
                    Terrasoft.ComparisonType.EQUAL, "[TsOppStageInLeadType:TsOpportunityStage].TsLeadType", leadTypeFilterValue));
                entitySchemaQuery.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_COLUMN, "Id");
                entitySchemaQuery.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_DISPLAY_COLUMN, "Name");
                entitySchemaQuery.addColumn("End", "End");
                var numberColumn = entitySchemaQuery.addColumn("[TsOppStageInLeadType:TsOpportunityStage].TsNumber", "Number");
                numberColumn.orderDirection = Terrasoft.OrderDirection.ASC;
                numberColumn.orderPosition = 0;
                return entitySchemaQuery;
            },


            //sortByStageNumberPredicate: function(item1, item2) {
            //    var stage1 = this.getItemStage(item1);
            //    var stage2 = this.getItemStage(item2);
            //    return stage1.Number > stage2.Number;
            //},
            //
            //
            //getSeriesItemYAxisValue: function(topValue, bottomValue) {
            //    var y = Math.round(100 * (topValue * 100 / bottomValue)) / 100;
            //    return Ext.isNumber(y) ? y : 0;
            //},
            //
            //
            //getSeriesDataConfigByItem: function(responseRow) {
            //    var lcz = Terrasoft.FunnelBaseDataProvider.Resources.Strings;
            //    var dataConfig = this.callParent(arguments);
            //    var bottomValue = responseRow.get("bottomConversionValue");
            //    var isDataPresent = Ext.isNumber(bottomValue);
            //    var topValue = responseRow.get("yAxis") || 0;
            //    var conversionValue;
            //    var y = 0;
            //    if (isDataPresent) {
            //        y = this.getSeriesItemYAxisValue(topValue, bottomValue);
            //        conversionValue =  Terrasoft.getFormattedNumberValue(y, {decimalPrecision: 2}) + "%";
            //    } else {
            //        conversionValue = lcz.NoData;
            //    }
            //    var conversionDisplayValue = (isDataPresent) ? lcz.Conversion + ": " + conversionValue : conversionValue;
            //    var name = Ext.String.format("{0}<br/>{1}", dataConfig.menuHeaderValue, conversionDisplayValue);
            //    var displayValue = Ext.String.format("<br/>{0}: {1}", lcz.Conversion, conversionValue);
            //    return Ext.apply(dataConfig, {
            //        name: name,
            //        displayValue: displayValue,
            //        y: y
            //    });
            //}


        });
    });