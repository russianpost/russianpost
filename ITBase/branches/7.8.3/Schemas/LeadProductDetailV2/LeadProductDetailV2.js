define("LeadProductDetailV2", ["ConfigurationGrid", "ConfigurationGridGenerator",
"ConfigurationGridUtilities"], function() {
			return {
				entitySchemaName: "LeadProduct",

				attributes: {
					// Признак возможности редактирования.
					"IsEditable": {
						// Тип данных — логический.
						dataValueType: Terrasoft.DataValueType.BOOLEAN,
						// Тип атрибута — виртуальная колонка модели представления.
						type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
						// Устанавливаемое значение.
						value: true
					}
				},

				// Используемые миксины.
				mixins: {
					ConfigurationGridUtilites: "Terrasoft.ConfigurationGridUtilities"
				},

				messages: {
					"GetLeadAccountInfo": {
						mode: this.Terrasoft.MessageMode.PTP,
						direction: this.Terrasoft.MessageDirectionType.PUBLISH
					}
				},
				methods: {
					// Убрана фильтрация, реализованная Террасофтом.
					openProductLookup: function() {
		//				var accountInfo = this.sandbox.publish("GetLeadAccountInfo", null, [this.sandbox.id]);

						var config = {
							entitySchemaName: "Product",
							multiSelect: true,
							columns: ["Name", "Price", "Currency"]
						};
						var leadId = this.get("MasterRecordId");

						if (this.Ext.isEmpty(leadId)) {
							return;
						}
						var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: this.entitySchemaName
						});
						esq.addColumn("Id");
						esq.addColumn("Product.Id", "ProductId");
						esq.filters.add("filterLead", this.Terrasoft.createColumnFilterWithParameter(
								this.Terrasoft.ComparisonType.EQUAL, "Lead", leadId));
						esq.getEntityCollection(function(result) {
							var existsProductsCollection = [];
							if (result.success) {
								result.collection.each(function(item) {
									existsProductsCollection.push(item.get("ProductId"));
								});
							}
							var filterGroup = this.Terrasoft.createFilterGroup();
							if (existsProductsCollection.length > 0) {
								var existsFilter = this.Terrasoft.createColumnInFilterWithParameters("Id",
										existsProductsCollection);
								existsFilter.comparisonType = this.Terrasoft.ComparisonType.NOT_EQUAL;
								existsFilter.Name = "existsFilter";
								filterGroup.addItem(existsFilter);

							}
		/*					var typesFilters = this.Terrasoft.createFilterGroup();
							typesFilters.logicalOperation = this.Terrasoft.LogicalOperatorType.OR;
							if (accountInfo.BusinessType) {
								var businessTypeFilter = this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
										"[TsBusinessTypeInProduct:TsProduct:Id].TsBusinessTypes", accountInfo.BusinessType.value);
								typesFilters.addItem(businessTypeFilter);
							}
							if (accountInfo.Industry) {
								var industryFilter = this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
										"[TsIndustryInProduct:TsProduct:Id].TsAccountIndustry", accountInfo.Industry.value);
								typesFilters.addItem(industryFilter);
							}

							filterGroup.addItem(typesFilters);*/

							config.filters = filterGroup;
							this.openLookup(config, this.addCallBack, this);
						}, this);
					},

					onProductInsert: function(response) {
						this.hideBodyMask.call(this);
						this.beforeLoadGridData();
						var filterCollection = [];
						response.queryResults.forEach(function(item) {
							filterCollection.push(item.id);
						});
						var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: this.entitySchemaName
						});
						this.initQueryColumns(esq);
						esq.filters.add("recordId", Terrasoft.createColumnInFilterWithParameters("Id", filterCollection));
						esq.getEntityCollection(function(response) {
							this.afterLoadGridData();
							if (response.success) {
								var responseCollection = response.collection;
								this.prepareResponseCollection(responseCollection);
								this.getGridData().loadAll(responseCollection);
								this.updateDetail({
									detail: "LeadProductDetailV2",
									reloadAll: true
								});
							}
						}, this);
					}
				},
				diff: /**SCHEMA_DIFF*/[
					{
						// Тип операции — слияние.
						"operation": "merge",
						// Название элемента схемы, над которым производится действие.
						"name": "DataGrid",
						// Объект, свойства которого будут объединены со свойствами элемента схемы.
						"values": {
							// Имя класса
							"className": "Terrasoft.ConfigurationGrid",
							// Генератор представления должен генерировать только часть представления.
							"generator": "ConfigurationGridGenerator.generatePartial",
							// Привязка события получения конфигурации элементов редактирования
							// активной строки к методу-обработчику.
							"generateControlsConfig": {"bindTo":
							"generatActiveRowControlsConfig"},
							// Привязка события смены активной записи к методу-обработчику.
							"changeRow": {"bindTo": "changeRow"},
							// Привязка события отмены выбора записи к методу-обработчику.
							"unSelectRow": {"bindTo": "unSelectRow"},
							// Привязка события клика на реестре к методу-обработчику.
							"onGridClick": {"bindTo": "onGridClick"},
							// Действия, производимые с активной записью.
							"activeRowActions": [
								{
									// Имя класса элемента управления, с которым связано действие.
									"className": "Terrasoft.Button",
									// Стиль отображения — прозрачная кнопка.
									"style":
									this.Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
									// Тег.
									"tag": "save",
									// Значение маркера.
									"markerValue": "save",
									// Привязка к изображению кнопки.
									"imageConfig": {"bindTo": "Resources.Images.SaveIcon"}
								},
								{
									// Настройка действия [Удалить].
									"className": "Terrasoft.Button",
									"style":
									this.Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
									"tag": "remove",
									"markerValue": "remove",
									"imageConfig": {"bindTo": "Resources.Images.RemoveIcon"}
								}
							],
							// Привязка к методу, который инициализирует подписку на события
							// нажатия кнопок в активной строке.
							"initActiveRowKeyMap": {"bindTo": "initActiveRowKeyMap"},
							// Привязка события выполнения действия активной записи к методу-обработчику.
							"activeRowAction": {"bindTo": "onActiveRowAction"},
							// Признак возможности выбора нескольких записей.
							"multiSelect": {"bindTo": "MultiSelect"}
						}
					}
				]/**SCHEMA_DIFF*/
			};
		}
);
