define("EmailPageV2", ["ConfigurationConstants", "ITExtendedHtmlEditModule"], function(ConfigurationConstants) {
	return {
		entitySchemaName: "Activity",
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "Body",
				"parentName": "MessageControlGroup",
				"propertyName": "items",
				"values": {
					"markerValue": "EmailBody",
					"className": "Terrasoft.ITExtendedHtmlEdit",
					"itemType": this.Terrasoft.ViewItemType.MODEL_ITEM,
					"dataValueType": this.Terrasoft.DataValueType.TEXT
				}
			},
			{
				"operation": "merge",
				"parentName": "Header",
				"propertyName": "items",
				"name": "Recepient",
				"values": {
					"layout": {
						"column": 0,
						"row": 1,
						"colSpan": 18
					}
				}
			},
			{
				"operation": "merge",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ToggleCopyRecipientVisible",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"caption": {"bindTo": "Resources.Strings.AlternativeCC"},
					"style": Terrasoft.controls.ButtonEnums.style.BLUE,
					"layout": {
						"column": 19,
						"row": 1,
						"colSpan": 2
					}
				}
			},
			{
				"operation": "merge",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ToggleBlindCopyRecipientVisible",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"caption": {"bindTo": "Resources.Strings.AlternativeBCC"},
					"style": Terrasoft.controls.ButtonEnums.style.BLUE,
					"layout": {
						"column": 21,
						"row": 1,
						"colSpan": 3
					}
				}
			}
		]/**SCHEMA_DIFF*/,
		methods: {
			onEntityInitialized: function() {
				this.callParent(arguments);
				this.setDefaultTemplate();
			},
			setDefaultTemplate: function() {
				var templateId;
				this.Terrasoft.SysSettings.querySysSettingsItem("EmailDefaultTemplate",
					function(value) {
						templateId = value.value;
					},
				this);

				if (this.Ext.isEmpty(this.get("Body")) &&
					this.get("EmailSendStatus") !== ConfigurationConstants.Activity.EmailSendStatus.Sended) {
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", { rootSchemaName: "EmailTemplate"});
					esq.addColumn("Body", "Body");
					esq.getEntity(templateId, function(result) {
						this.set("Body", result.entity.get("Body"));
					}, this);
				}
			}
		}
	};
});