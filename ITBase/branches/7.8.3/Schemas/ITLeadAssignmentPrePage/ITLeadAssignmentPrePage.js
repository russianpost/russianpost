/**Создано в рамках задачи https://it-bpm.atlassian.net/browse/RPCRM-1074**/

define("ITLeadAssignmentPrePage", ["CustomProcessPageV2Utilities", "BusinessRuleModule", "ConfigurationConstants",
		"TsJsConsts"],
	function(CustomProcessPageV2Utilities, BusinessRuleModule, ConfigurationConstants, TsJsConsts) {
		return {
			mixins: {
				BaseProcessViewModel: "Terrasoft.CustomProcessPageV2Utilities"
			},

			attributes: {
				"GetOwner": {
					dataValueType: Terrasoft.DataValueType.LOOKUP,
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					referenceSchemaName: "Contact"
	/*				lookupListConfig: {
						filter: function() {
							var owner = this.get("GetOwner");
							var filterGroup = Ext.create("Terrasoft.FilterGroup");
							if (owner) {
								filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.OR;
								filterGroup.add("CurrentOwnerLead",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Id",
										owner.value));
								filterGroup.add("LeadNotEmpty",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Owner.Id",
										owner.value));
							}
							else {
								var leadType = this.get("LeadType");
								filterGroup.logicalOperation = Terrasoft.LogicalOperatorType.AND;
								filterGroup.add("TypeFilter",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Type.Id",
										ConfigurationConstants.ContactType.Employee));
								if (leadType) {
									filterGroup.add("CurrentOwnerLead",
										Terrasoft.createColumnFilterWithParameter(
											Terrasoft.ComparisonType.EQUAL,
											"[ITContactLeadType:ITContact].ITLeadType.Id",
											leadType.value));
								}
							}
							return filterGroup;
						}
					}*/
				}
			},

			rules: {
				"GetOwner": {
					"RequireOwner": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.REQUIRED,
						conditions: [{
							leftExpression: {
								type: BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								attribute: "GetOwner"
							},
							comparisonType: this.Terrasoft.ComparisonType.IS_NULL
						}]
					},

					"FiltrationContactByType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": true,
						"autoClean": true,
						"baseAttributePatch": "Type",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": TsJsConsts.ContactType.Employee
					}
				}
			},

			methods: {
				onEntityInitialized: function() {
					this.callParent(arguments);
					this.setOwnerOnInit();
				},

				//Заполняем поле "Ответственный" по умолчанию текущим контактом
				setOwnerOnInit: function() {
					var contact = this.Terrasoft.SysValue.CURRENT_USER_CONTACT;
					this.set("GetOwner", contact);
				},

				//Возвращаем заголовок преднастроенной страницы
				getHeader: function() {
					return this.get("Resources.Strings.ITLeadAssignmentHeader");
				},

				//Переопределение метода для прорисовки caption страницы
				getPageHeaderCaption: function() {
					var pageName = this.name;
					var header = pageName === "ITLeadAssignmentPrePage" ? this.getHeader() : "";
					return header;
				},

				initHeaderCaption: Ext.emptyFn,

				initPrintButtonMenu: Ext.emptyFn,

				loadVocabulary: function(args, tag) {
					var column = this.getColumnByName(tag);
					args.schemaName = column.referenceSchemaName;
					this.callParent(arguments);
				},

				getLookupPageConfig: function(args, columnName) {
					var config = {
						entitySchemaName: args.schemaName,
						multiSelect: false,
						columnName: columnName,
						columnValue: this.get(columnName),
						searchValue: args.searchValue,
						filters: this.getLookupQueryFilters(columnName)
					};
					this.Ext.apply(config, this.getLookupListConfig(columnName));
					return config;
				},

				onCloseCardButtonClick: function() {
					this.sandbox.publish("BackHistoryState");
				},

				//При клике на кнопку "Выбор"
				onNextButtonClick: function() {
					var owner = this.get("GetOwner"),
						error = this.get("Resources.Strings.OwnerNotSelectedError");
					switch (!owner || owner === this.Terrasoft.GUID_EMPTY) {
						case false:
							this.set("OwnerContactId", owner.value);
							this.acceptProcessElement("NextButton");
							break;
						default:
							this.showInformationDialog(error);
					}
					
				},

				//Удаляем создание записи из всплывающей
				//подсказки справочных полей
				getNewListItemConfig: function() {
					return;
				}
			},

			diff: /**SCHEMA_DIFF*/[
				// Удалить лишние кнопки
	/*			{
					"operation": "remove",
					"name": "ActionButtonsContainer"
				},*/
				{
					"operation": "remove",
					"name": "DiscardChangesButton"
				},
	/*			{
					"operation": "remove",
					"name": "Tabs"
				},*/
				{
					"operation": "remove",
					"name": "DelayExecutionButton"
				},
				{
					"operation": "remove",
					"name": "ViewOptionsButton"
				},
				{
					"operation": "remove",
					"name": "SaveButton"
				},
				{
					"operation": "remove",
					"name": "actions"
				},

				{
					"operation": "merge",
					"name": "CloseButton",
					"values": {
						"style": Terrasoft.controls.ButtonEnums.style.RED,
						"click": {"bindTo": "onCloseCardButtonClick"},
						"visible": true
					}
				},

				{
					"operation": "insert",
					"parentName": "LeftContainer",
					"propertyName": "items",
					"name": "NextButton",
					"values": {
						"caption": {"bindTo": "Resources.Strings.AssignmentButtonCaption"},
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"classes": {textClass: "actions-button-margin-right"},
						"style": Terrasoft.controls.ButtonEnums.style.GREEN,
						"click": {bindTo: "onNextButtonClick"}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "GetOwner",
					"values": {
						"caption": {"bindTo": "Resources.Strings.OwnerCaption"},
						"bindTo": "GetOwner",
						"layout": {column: 0, row: 0, colSpan: 9}
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});