define("AccountSectionV2", ["ServiceHelper", "ConfigurationEnums", "ProcessModuleUtilities", "ConfigurationConstants"],
	function(ServiceHelper, ConfigurationEnums, ProcessModuleUtilities, ConfigurationConstants) {

	//
	//
	// -- ITBase --
	//
	//

	return {
		entitySchemaName: "Account",
		attributes: {
			"ChangeOwnerFlag": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: false
			},
			"ChangeOwnerSupplierFlag": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: false
			},
			"LeadType": {
				dataValueType: this.Terrasoft.DataValueType.LOOKUP,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				referenceSchemaName: "LeadType",
				lookupListConfig: {
					hideActions: true
				}
			},
			"FormValid": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: false
			},
			"IndefinitelyValid": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: true
			},
			"ActiveRow": {
				"onChange": "setNameAccount"
			},
			"NameAccount": {
				dataValueType: this.Terrasoft.DataValueType.TEXT,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			"IsClientAccount": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: false
			}
		},
		methods: {
			init: function() {
				this.callParent(arguments);
				this.setOnChangeRoutine();
				//this.set("UseStaticFolders", true);
			},
			setOnChangeRoutine: function() {
				this.on("change:ChangeOwnerFlag", this.updFormValid, this);
				this.on("change:ChangeOwnerSupplierFlag", this.updFormValid, this);
				this.on("change:OldExecutive", this.updFormValid, this);
				this.on("change:NewExecutive", this.updFormValid, this);
				this.on("change:ChangeUntilDate", this.updFormValid, this);
				this.on("change:ChangeFrom", this.updFormValid, this);
				this.on("change:Indefinitely", this.updIndefinitelyValid, this);
				this.on("change:Indefinitely", this.updFormValid, this);
				
			},
			/*
			onRender: function() {
				var restored = this.get("Restored");
				var historyStateInfo = this.getHistoryStateInfo();
				if (!restored && historyStateInfo.workAreaMode !== ConfigurationEnums.WorkAreaMode.COMBINED) {
					this.showFolderTree();
				}
				this.set("IgnoreFilterUpdate", false);
				this.callParent(arguments);
			},*/
			getChoosingModuleConfig: function() {
				var res = this.callParent(arguments);
				res.onButtonClick = "anyButtonClick";
				res.windowHeight = 450;
				res.windowWidth = 500;
				res.buttons.ok.enabled = {bindTo: "FormValid"};
				return res;
			},
			updFormValid: function() {
				this.set("FormValid",
					this.get("OldExecutive") !== undefined &&
					this.get("NewExecutive") !== undefined &&
					(this.get("ChangeOwnerSupplierFlag") || this.get("ChangeOwnerFlag")) &&
					(this.get("Indefinitely") ||
						(this.get("ChangeUntilDate") !== undefined && this.get("ChangeUntilDate") !== null) &&
						(this.get("ChangeFrom") !== undefined && this.get("ChangeFrom") !== null)
						)
					);
			},
			isValidForm: function() {
				if (this.get("OldExecutive") && this.get("NewExecutive")) {
					this.Terrasoft.showInformationDialog("Необходимо заполнить старого и нового ответственного.");
					return false;
				}
				if (!this.get("ChangeOwner") && !this.get("ChangeOwnerFlag"))
				{
					this.Terrasoft.showInformationDialog(
						"Необходимо выбрать смену ответственного и/или смену ответственного поддержки.");
					return false;
				}
				return true;
			},
			anyButtonClick: function(tag) {
				if (tag === "ok") {
					this.runBPChangeOwner();
				}
				
			},
			runBPChangeOwner: function() {
				var params = {};
				params.OldOwnerId = this.get("OldExecutive").value;
				params.NewOwnerId = this.get("NewExecutive").value;
				params.OldOwnerName = this.get("OldExecutive").Name;
				params.NewOwnerName = this.get("NewExecutive").Name;
				if (this.get("Indefinitely") === undefined) {
					this.set("Indefinitely", false);
				}
				params.Indefinitely = this.get("Indefinitely");
				if (this.get("ChangeUntilDate") !== null && this.get("ChangeUntilDate") !== undefined) {
					
					var date = this.get("ChangeUntilDate");
					var day = date.getDate();       // yields date
					var month = date.getMonth() + 1;    // yields month (add one as '.getMonth()' is zero indexed)
					var year = date.getFullYear();  // yields year
					var hour = date.getHours();     // yields hours 
					var minute = date.getMinutes(); // yields minutes
					var second = date.getSeconds(); // yields seconds
					
					// After this construct a string with the above results as below
					var time = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;

					params.ChangeUntilDate = time;
				}
				if (this.get("ChangeFrom") !== null && this.get("ChangeFrom") !== undefined) {
					
					var date = this.get("ChangeFrom");
					var day = date.getDate();       // yields date
					var month = date.getMonth() + 1;    // yields month (add one as '.getMonth()' is zero indexed)
					var year = date.getFullYear();  // yields year
					var hour = date.getHours();     // yields hours 
					var minute = date.getMinutes(); // yields minutes
					var second = date.getSeconds(); // yields seconds
					
					// After this construct a string with the above results as below
					var time = day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;

					params.ChangeFrom = time;
				}
				var leadType = this.get("LeadType");
				if (leadType) {
					params.LeadType = leadType.value;
				}
				params.ChangeOwnerFlag = this.get("ChangeOwnerFlag");
				params.ChangeOwnerSupplierFlag = this.get("ChangeOwnerSupplierFlag");
				ProcessModuleUtilities.executeProcess(
				{
					sysProcessName: "ITChangeAccountOwnerProcess",
					parameters: params
					//	this.Terrasoft.showInformation(this.get("Resources.Strings.ChangeAccountExecutiveIsRun"));
					//	this.Ext.callback();
					//},
				//	scope: this
				});
				this.Terrasoft.showInformation(this.get("Resources.Strings.ChangeAccountExecutiveIsRun"));
			},
			getControlsConfig: function() {
				return [{
						"name": "NewExecutive",
						"bindTo": "NewExecutive",
						"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
						"contentType": this.Terrasoft.ContentType.LOOKUP,
						caption: this.get("Resources.Strings.NewAccountExecutiveCaption"),
						"layout": {
							row: 1,
							rowSpan: 1,
							column: 0,
							colSpan: 24
						}
					},
					{
						"name": "OldExecutive",
						"bindTo": "OldExecutive",
						"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
						"contentType": this.Terrasoft.ContentType.LOOKUP,
						caption: this.get("Resources.Strings.OldAccountExecutiveCaption"),
						"layout": {
							row: 0,
							rowSpan: 1,
							column: 0,
							colSpan: 24
						}
					},
					{
						"name": "ChangeOwnerFlag",
						"bindTo": "ChangeOwnerFlag",
						"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
						caption: this.get("Resources.Strings.ChangeOwnerFlagCaption"),
						"layout": {
							row: 2,
							rowSpan: 1,
							column: 0,
							colSpan: 24
						}
					},
					{
						"name": "ChangeOwnerSupplierFlag",
						"bindTo": "ChangeOwnerSupplierFlag",
						"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
						caption: this.get("Resources.Strings.ChangeOwnerSupplierFlagCaption"),
						"layout": {
							row: 3,
							rowSpan: 1,
							column: 0,
							colSpan: 24
						}
					},
					{
						"name": "LeadType",
						"bindTo": "LeadType",
						"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
						"contentType": this.Terrasoft.ContentType.LOOKUP,
						caption: this.get("Resources.Strings.LeadTypeCaption"),
						"layout": {
							row: 4,
							rowSpan: 1,
							column: 0,
							colSpan: 24
						}
					},
					{
						"name": "ChangeFrom",
						"bindTo": "ChangeFrom",
						"dataValueType": this.Terrasoft.DataValueType.DATE,
						caption: "Заменить с",
						isRequired: {bindTo: "IndefinitelyValid"},
						enabled: {bindTo: "IndefinitelyValid"},
						"layout": {
							row: 6,
							rowSpan: 1,
							column: 0,
							colSpan: 24
						}
					},
					{
						"name": "ChangeUntilDate",
						"bindTo": "ChangeUntilDate",
						"dataValueType": this.Terrasoft.DataValueType.DATE,
						caption: "Заменить до",
						isRequired: {bindTo: "IndefinitelyValid"},
						enabled: {bindTo: "IndefinitelyValid"},
						"layout": {
							row: 7,
							rowSpan: 1,
							column: 0,
							colSpan: 24
						}
					},
					{
						"name": "Indefinitely",
						"bindTo": "Indefinitely",
						"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
						caption: "Бессрочно",
						//"enabled": {bindTo: "FormValid"},
						"layout": {
							row: 5,
							rowSpan: 1,
							column: 0,
							colSpan: 24
						}
					}
				];
			},

			
			updIndefinitelyValid: function() {
				var indefinitelyValue = this.get("Indefinitely");
				if (indefinitelyValue === true) {
					this.set("IndefinitelyValid", false);
					this.set("ChangeUntilDate", undefined);
					this.set("ChangeFrom", undefined);
				} else {
					this.set("IndefinitelyValid", true);
				}
			},

			//#RPCRM-898 Открытие миникарточки Лид и передача данных текущего контрагента для привязки
			onAddLeadButtonClick: function() {
				this.openAddMiniPage({
					entitySchemaName: "Lead",
					//moduleId: this.getMiniPageSandboxId("Lead"),
					valuePairs: []
				});
				this.sandbox.subscribe("SetClientForLeadMiniPage", function() {
					var currentAccount = {
						Id: this.get("ActiveRow"),
						Name: this.get("NameAccount"),
						displayValue: this.get("NameAccount"),
						value: this.get("ActiveRow")
					};
					return currentAccount;
				}, this, null);
			},

			//#RPCRM-898 Открытие миникарточки Продажи и передача данных текущего контрагента для привязки
			onAddOpportunityButtonClick: function() {
				this.openAddMiniPage({
					entitySchemaName: "Opportunity",
					//moduleId: this.getMiniPageSandboxId("Opportunity"),
					valuePairs: []
				});
				this.sandbox.subscribe("SetClientForOpportunityMiniPage", function() {
					var currentAccount = {
						Id: this.get("ActiveRow"),
						Name: this.get("NameAccount"),
						displayValue: this.get("NameAccount"),
						value: this.get("ActiveRow")
					};
					return currentAccount;
				}, this, null);
			},

			/* #RPCRM-898 При изменении текущей записи заносить в атрибут имя контрагента
			и если контрагент является клиентом заносить в атрибут true, иначе false */
			setNameAccount: function() {
				if (this.get("ActiveRow")) {
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Account"
					});
					esq.addColumn("Name");
					esq.addColumn("Type");
					esq.getEntity(this.get("ActiveRow"), function(result) {
						if (!result.success) {
							return;
						}
						this.set("NameAccount", result.entity.get("Name"));
						if (result.entity.get("Type").value === ConfigurationConstants.AccountType.Client.toLowerCase()) {
							this.set("IsClientAccount", true);
						} else {
							this.set("IsClientAccount", false);
						}
					}, this);
				}
			},

			//#RPCRM-898 Если контрагент является клиентом вовращает true, иначе false
			IsTypeAccountClient: function() {
				if (this.get("IsClientAccount")) {
					return true;
				}
				return false;
			},
			openDuplicatesModule: function() {
				//#RPCRM-735 Очистка таблицы дублей нового контрагента
				var serviceData = {
					currentContactId: Terrasoft.core.enums.SysValue.CURRENT_USER_CONTACT.value
				};
				ServiceHelper.callService("ITDuplicationSearch", "ClearTableDuplicationProcedure", function(response) {
					var result = response.ClearTableDuplicationProcedureResult;
					if (result !== "Good") {
						this.showInformationDialog(result);
					}
				}, serviceData, this);
				//конец #RPCRM-735
				this.callParent(arguments);
			}
		},
		messages: {
			//#RPCRM-898 Сообщение для передачи данных текущего контрагента в миникарточку Продажи
			"SetClientForOpportunityMiniPage": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			},
			//#RPCRM-898 Сообщение для передачи данных текущего контрагента в миникарточку Лид
			"SetClientForLeadMiniPage": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			//#RPCRM-898 Кнопка Добавить Лид
			{
				"operation": "insert",
				"name": "AddLeadButton",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.BUTTON,
					"caption": "Добавить лид",
					"style": this.Terrasoft.controls.ButtonEnums.style.GREEN,
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
					"click": {"bindTo": "onAddLeadButtonClick"},
					"visible": {"bindTo": "IsTypeAccountClient"},
					"enabled": true
				}
			},
			//#RPCRM-898 Кнопка Добавить Продажу
			{
				"operation": "insert",
				"name": "AddOpportunityButton",
				"parentName": "CombinedModeActionButtonsCardLeftContainer",
				"propertyName": "items",
				"values": {
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"caption": "Добавить продажу",
					"style": this.Terrasoft.controls.ButtonEnums.style.BLUE,
					"classes": {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
					"click": {"bindTo": "onAddOpportunityButtonClick"},
					"visible": {"bindTo": "IsTypeAccountClient"},
					"enabled": true
				}
			}
		]/**SCHEMA_DIFF*/
	};
});