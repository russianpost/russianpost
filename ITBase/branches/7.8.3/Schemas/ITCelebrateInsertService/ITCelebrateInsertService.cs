using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Web;
using System.Linq;
using System.IO;
using Terrasoft.Configuration.ITBase;


namespace Terrasoft.Configuration.ITBase
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ITCelebrateInsertService : ITDBBase
    {
        List<Account> accounts = new List<Account>();
        List<ITAnniversary> itAnniversaries = new List<ITAnniversary>();
        List<AccountAnniversary> accountAnniversaries = new List<AccountAnniversary>();

        UserConnection userConnection1;

        public ITCelebrateInsertService(UserConnection userConnection):base(userConnection)
        {
            userConnection1 = userConnection;
            accounts = GetAllAccounts();
            itAnniversaries = GetITAnniversaries();
            accountAnniversaries = GetAnniversaryAccounts();
        }
        
        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
        public string ITUpdate()
        {
        	SearchAccountAnniversaryForIndustry();
        	SearchAccountAnniversaryForSegment();
        	SearchAccountAnniversaryForSubsegment();
            var insert = new Insert(userConnection1).Into("AcademyURL")
            .Set("Id", Column.Parameter(Guid.NewGuid()))
            .Set("Name", Column.Parameter("Error"))
            .Set("Description", Column.Parameter("Test1"))
            .Set("ProcessListeners", Column.Parameter(0));

            insert.Execute();
            return "Completed";
        }

        public List<ITAnniversary> GetITAnniversaries()
        {
            List<ITAnniversary> returnItAnniversaryList = new List<ITAnniversary>();

            var selectITAnniversaryList = new Select(userConnection1).From("ITAnniversaryList")
                .Column("Id")
                .Column("Description")
                .Column("ITAccountIndustryId")
                .Column("ITSubsegmentId")
                .Column("ITDate")
                .Column("ITBusinessTypesId")
                .Column("ITDayOfMonth")
                .Column("ITMonthNumber");
            try
            {
                using (DBExecutor executor = userConnection1.EnsureDBConnection())
                {
                    using (IDataReader reader = selectITAnniversaryList.ExecuteReader(executor))
                    {
                        while (reader.Read())
                        {
                            var id = reader.GetColumnValue<Guid>("Id");
                            var itDescription = reader.GetColumnValue<string>("Description");
                            var iTAccountIndustryId = reader.GetColumnValue<Guid>("ITAccountIndustryId");
                            var iTSubsegmentId = reader.GetColumnValue<Guid>("ITSubsegmentId");
                            var itDate = reader.GetColumnValue<DateTime>("ITDate");
                            var iTBusinessTypesId = reader.GetColumnValue<Guid>("ITBusinessTypesId");
                            var iTDayOfMonth = reader.GetColumnValue<int>("ITDayOfMonth");
                            var iTMonthNumber = reader.GetColumnValue<int>("ITMonthNumber");

                            var iTAnniversaryList = new ITAnniversary(id, itDescription, iTAccountIndustryId, iTSubsegmentId, itDate, iTBusinessTypesId, iTDayOfMonth, iTMonthNumber);
                            returnItAnniversaryList.Add(iTAnniversaryList);
                        }
                    }
                }
                return returnItAnniversaryList;
            }
            catch (Exception ex)
            {
                return new List<ITAnniversary>();
            }
        }

        public List<Account> GetAllAccounts()
        {
            List<Account> returnAccountListForIndustry = new List<Account>();

            var selectAccounts = new Select(userConnection1).From("Account")
                .Column("Id")
                .Column("IndustryId")
                .Column("ITSubsegmentId")
                .Column("TsAccountBusinessTypeId");
            try
            {
                using (DBExecutor executor = userConnection1.EnsureDBConnection())
                {
                    using (IDataReader reader = selectAccounts.ExecuteReader(executor))
                    {
                        while (reader.Read())
                        {
                            var id = reader.GetColumnValue<Guid>("Id");
                            var iTAccountIndustryId = reader.GetColumnValue<Guid>("IndustryId");
                            var iTSubsegmentId = reader.GetColumnValue<Guid>("ITSubsegmentId");
                            var tsAccountBusinessTypesId = reader.GetColumnValue<Guid>("TsAccountBusinessTypeId");

                            var account = new Account(id, iTAccountIndustryId, tsAccountBusinessTypesId, iTSubsegmentId);
                            returnAccountListForIndustry.Add(account);
                        }
                    }
                }
                return returnAccountListForIndustry;
            }
            catch (Exception ex)
            {
                return  new List<Account>();
            }
        }

        public List<AccountAnniversary> GetAnniversaryAccounts()
        {
            List<AccountAnniversary> returnAnniversaryAccountsList = new List<AccountAnniversary>();

            var selectAnniversaryAccounts = new Select(userConnection1).From("AccountAnniversary")
                .Column("Id")
                .Column("Date")
                .Column("AccountId")
                .Column("Description")
                .Column("ITAnniversaryListId");
                //.Where("ITAnniversaryListId")
                //.IsNotEqual(DBNull.Value);
            try
            {
                using (DBExecutor executor = userConnection1.EnsureDBConnection())
                {
                    using (IDataReader reader = selectAnniversaryAccounts.ExecuteReader(executor))
                    {
                        while (reader.Read())
                        {
                            var id = reader.GetColumnValue<Guid>("Id");
                            var date = reader.GetColumnValue<DateTime>("Date");
                            var accountId = reader.GetColumnValue<Guid>("AccountId");
                            var description = reader.GetColumnValue<string>("Description");
                            var iTAnniversaryId = reader.GetColumnValue<Guid>("ITAnniversaryListId");

                            var accountAnniversary = new AccountAnniversary(id, date, accountId, description, iTAnniversaryId);
                            returnAnniversaryAccountsList.Add(accountAnniversary);
                        }
                    }
                }
                return returnAnniversaryAccountsList;
            }
            catch (Exception ex)
            {
                return  new List<AccountAnniversary>();
            }
        }

        public void SearchAccountAnniversaryForIndustry()
        {
            foreach (var itAnniversary in itAnniversaries)
            {
                if (itAnniversary.ITAccountIndustryId != Guid.Empty)
                {
                    foreach (var account in accounts)
                    {
                        if (account.IndustryId != Guid.Empty && account.IndustryId == itAnniversary.ITAccountIndustryId)
                        {
                            bool isAccountAniversaryExists = false;
                            foreach (var accountAnniversary in accountAnniversaries)
                            {
                                if (accountAnniversary.AccountId == account.Id && accountAnniversary.ITAnniversaryId == itAnniversary.Id)
                                {
                                    isAccountAniversaryExists = true;
                                    UpdateAccountAnniversary(itAnniversary, accountAnniversary.Id);
                                }
                            }
                            if (!isAccountAniversaryExists)
                            {
                                InsertAccountAnniversary(itAnniversary, account.Id);
                            }
                        }
                    }
                }
            }
        }
        
        public void SearchAccountAnniversaryForSegment()
        {
            foreach (var itAnniversary in itAnniversaries)
            {
                if (itAnniversary.ITBusinessTypesId != Guid.Empty)
                {
                    foreach (var account in accounts)
                    {
                        if (account.TsAccountBusinessTypeId != Guid.Empty && account.TsAccountBusinessTypeId == itAnniversary.ITBusinessTypesId)
                        {
                            bool isAccountAniversaryExists = false;
                            foreach (var accountAnniversary in accountAnniversaries)
                            {
                                if (accountAnniversary.AccountId == account.Id && accountAnniversary.ITAnniversaryId == itAnniversary.Id)
                                {
                                    isAccountAniversaryExists = true;
                                    UpdateAccountAnniversary(itAnniversary, accountAnniversary.Id);
                                }
                            }
                            if (!isAccountAniversaryExists)
                            {
                                InsertAccountAnniversary(itAnniversary, account.Id);
                            }
                        }
                    }
                }
            }
        }

        public void SearchAccountAnniversaryForSubsegment()
        {
            foreach (var itAnniversary in itAnniversaries)
            {
                if (itAnniversary.ITSubsegmentId != Guid.Empty)
                {
                    foreach (var account in accounts)
                    {
                        if (account.ITSubsegmentId != Guid.Empty && account.ITSubsegmentId == itAnniversary.ITSubsegmentId)
                        {
                            bool isAccountAniversaryExists = false;
                            foreach (var accountAnniversary in accountAnniversaries)
                            {
                                if (accountAnniversary.AccountId == account.Id && accountAnniversary.ITAnniversaryId == itAnniversary.Id)
                                {
                                    isAccountAniversaryExists = true;
                                    UpdateAccountAnniversary(itAnniversary, accountAnniversary.Id);
                                }
                            }
                            if (!isAccountAniversaryExists)
                            {
                                InsertAccountAnniversary(itAnniversary, account.Id);
                            }
                        }
                    }
                }
            }
        }

        public bool UpdateAccountAnniversary(ITAnniversary itAnniversary, Guid? accountAnniversaryId)
        {
            try
            {
            	DateTime? celebrateDate = null;
                if (itAnniversary.ITDate != DateTime.MinValue)
                {
                    celebrateDate = new DateTime(DateTime.Now.Year, itAnniversary.ITDate.Value.Month, itAnniversary.ITDate.Value.Day);
                }
                else
                {
                    celebrateDate = GetHolidayDateTime(itAnniversary.ITMonthNumber, itAnniversary.ITDayOfMonth);
                }
                Guid anniversaryType = Guid.Parse("6A8A98C8-5831-4B81-BBA7-05CBFD412725");
                var updateAccountAnniversary = new Update(userConnection1, "AccountAnniversary")
                    .Set("Date", Column.Parameter(celebrateDate))
                    .Set("AnniversaryTypeId", Column.Parameter(anniversaryType))
                    .Where("Id")
                    .IsEqual(Column.Parameter(accountAnniversaryId));
                updateAccountAnniversary.Execute();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool InsertAccountAnniversary(ITAnniversary itAnniversary, Guid accountId)
        {
            try
            {
            	DateTime? celebrateDate = null;
                if (itAnniversary.ITDate != DateTime.MinValue)
                {
                    celebrateDate = new DateTime(DateTime.Now.Year, itAnniversary.ITDate.Value.Month, itAnniversary.ITDate.Value.Day);
                }
                else
                {
                    celebrateDate = GetHolidayDateTime(itAnniversary.ITMonthNumber, itAnniversary.ITDayOfMonth);
                }
                Guid anniversaryType = Guid.Parse("6A8A98C8-5831-4B81-BBA7-05CBFD412725");
                var insertAccountAnniversary = new Insert(userConnection1).Into("AccountAnniversary")
                    .Set("Id", Column.Parameter(Guid.NewGuid()))
                    .Set("Date", Column.Parameter(celebrateDate))
                    .Set("Description", Column.Parameter(itAnniversary.ITDescription))
                    .Set("AccountId", Column.Parameter(accountId))
                    .Set("AnniversaryTypeId", Column.Parameter(anniversaryType))
                    .Set("ITAnniversaryListId", Column.Parameter(itAnniversary.Id));
                insertAccountAnniversary.Execute();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public DateTime GetHolidayDateTime(int monthOfHoliday, int numberOfHolidaysWeek)
        {
            if (numberOfHolidaysWeek > 0)
            {
                numberOfHolidaysWeek = numberOfHolidaysWeek - 1;
            }

            DateTime firstDayOfMonth = new DateTime(DateTime.Now.Year, monthOfHoliday, 1);
            var firstDayNumber = (int)firstDayOfMonth.DayOfWeek; 

            DateTime lastOfThisMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            var lastDayCode = (int)lastOfThisMonth.DayOfWeek;
            var lastSundayHoliday = lastOfThisMonth;

            DateTime holidayDate = DateTime.Now;

            if (numberOfHolidaysWeek == -1)
            {
                if (lastDayCode == 0)
                {
                    holidayDate = lastSundayHoliday;
                }
                else
                {
                    for (int i = 0; i < 6; i++)
                    {
                        lastSundayHoliday = lastSundayHoliday.AddDays(-1);
                        if ((int)lastSundayHoliday.DayOfWeek == 0)
                        {
                            holidayDate = lastSundayHoliday;
                        }
                    }
                }
            }
            else
            {
                    int sumOfDays = numberOfHolidaysWeek * 7;
                    if (firstDayNumber != 0)
                    {
                        sumOfDays = sumOfDays + (7 - firstDayNumber);
                    }
                holidayDate = firstDayOfMonth.AddDays(sumOfDays);
            }
            return holidayDate;
        }
        
    }
    
    public class ITAnniversary
    {
        public Guid Id { get; set; }
        public string ITDescription { get; set; }

        public Guid? ITAccountIndustryId { get; set; }

        public Guid? ITSubsegmentId { get; set; }

        public DateTime? ITDate { get; set; }

        public Guid? ITBusinessTypesId { get; set; }

        public int ITDayOfMonth { get; set; }

        public int ITMonthNumber { get; set; }

        public ITAnniversary(Guid id, string itDescription, Guid? itAccountIndustryId, Guid? itSubsegmentId,
            DateTime? itDate, Guid? itBusinessTypesId, int itDayOfMonth, int itMonthNumber)
        {
            Id = id;
            ITDescription = itDescription;
            ITAccountIndustryId = itAccountIndustryId;
            ITSubsegmentId = itSubsegmentId;
            ITDate = itDate;
            ITBusinessTypesId = itBusinessTypesId;
            ITDayOfMonth = itDayOfMonth;
            ITMonthNumber = itMonthNumber;
        }
    }

    public class Account
    {
        public Guid Id { get; set; }
        public Guid? IndustryId { get; set; }

        public Guid? TsAccountBusinessTypeId { get; set; }

        public Guid? ITSubsegmentId { get; set; }

        public Account(Guid id, Guid? industryId, Guid? tsAccountBusinessTypeId, Guid? itSubsegmentId)
        {
            Id = id;
            IndustryId = industryId;
            TsAccountBusinessTypeId = tsAccountBusinessTypeId;
            ITSubsegmentId = itSubsegmentId;
        }
    }

    public class AccountAnniversary
    {
        public Guid Id { get; set; }

        public DateTime? Date { get; set; }

        public Guid? AccountId { get; set; }

        public string Description { get; set; }

        public Guid? ITAnniversaryId { get; set; }

        public AccountAnniversary(Guid id, DateTime? date, Guid? accountId, string description, Guid? itAnniversaryId)
        {
            Id = id;
            Date = date;
            AccountId = accountId;
            Description = description;
            ITAnniversaryId = itAnniversaryId;
        }
    }

}