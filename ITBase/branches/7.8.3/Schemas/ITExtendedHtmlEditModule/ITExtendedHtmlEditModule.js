define("ITExtendedHtmlEditModule", ["ext-base", "terrasoft", "ExtendedHtmlEditModule"],
	function(Ext, Terrasoft) {
		Ext.define("Terrasoft.controls.ITExtendedHtmlEdit", {
			extend: "Terrasoft.ExtendedHtmlEdit",
			alternateClassName: "Terrasoft.ITExtendedHtmlEdit",
			
			initItemsConfig: function() {
				var tag = "addTemplate";
				var isPresent = false;
				this.itemsConfig.forEach(function(item) {
					if (item.tag === tag) {
						isPresent = true;
					}
				});

				if (isPresent) {
					this.itemsConfig.splice(this.itemsConfig.length - 1);
				}

				var self = this;
				var item = {
					className: "Terrasoft.Button",
					iconAlign: Terrasoft.controls.ButtonEnums.iconAlign.TOP,
					imageConfig: {
						source: Terrasoft.ImageSources.URL,
						url: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABZUlEQVR42qVTsU7CY" +
						"BA+J5+AmPgAJr6Bg7PPoZvxEXRS4+JkdDEOSFtKGFRWhcEYdYBRQiTSRlKw/YuYKlAohXLeXwVDAlXwki9/L//d1+/u/" +
						"gM4LMxCRFsDmWUgVnkhmAFgFKeAZIhwoi6Ab2J5FeTKG8SrOBFipgnhQgj8P0+a3EdU3wJf9rQEMkuAXxs5yykLBbWFZ" +
						"5oTiLDSxKWk9a3ASA4ISo0uRtQmKrUOjrPHd5dibExX3b6C1ICgbHt+ULzYxPRreyix1+vhrdnGc63l+5kgAm5Zy8XjJ" +
						"xs9SuwS9nONIWW/EvDgnYc6bmRquJ7+wN1sHZ/rfyTgMrlcLtv1POwQ+PcNczBRCijhq4k25qlR4yxHd8KIJjLurFxbR" +
						"NDCi5ITiFOaFB/5zxhlU/3fQ5L02NQEEtukZSouTrVMMl8mI8T3cQaO8vMg6HtU0xVE2R2d96NBd5JxCaKxDQfZOZ78C" +
						"alt4kRGWulAAAAAAElFTkSuQmCC"
					},
					handler: function itemHandler() {
						self.fireEvent("openEmailTemplate", {control: self});
					},
					tag: tag
				};
				this.itemsConfig.push(item);
			}
			
		});
	});

