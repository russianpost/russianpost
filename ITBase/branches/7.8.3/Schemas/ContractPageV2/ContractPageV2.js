define("ContractPageV2", ["ITJsConsts", "BusinessRuleModule", "ITJsContractConstants",
		"ProcessModuleUtilities", "ContractPageV2Resources", "ConfigurationConstants", "CompletenessMixin",
		"ConfigurationRectProgressBarGenerator", "CompletenessIndicator", "css!CompletenessCSSV2", "TooltipUtilities"],
	function(ITJsConsts, BusinessRuleModule, ITJsContractConstants,
			ProcessModuleUtilities, resources, ConfigurationConstants) {
		return {

			entitySchemaName: "Contract",

			messages: {
				"OpportunityProductTotalsChanged": {
					mode: Terrasoft.MessageMode.BROADCAST,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				}
			},

			attributes: {

				"ITDocumentType": {
					"onChange": "onDocumentTypeChanged"
				},

				//	Атрибут был переопределен из базовой схемы для доступности поля "Сумма" для записи в него данных
				"CanAmountEdit": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
					"value": true
				},

				"TsSupportEmployeeEnable": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
					"value": true
				},

				"ITOpportunityInContractEnable": {
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
					"value": true
				},

				"ForRulesAtt": {
//				"dataValueType": this.Terrasoft.DataValueType.TEXT,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				
				"Owner": {
					"lookupListConfig": {
						"columns": ["TsLeadType"],
						"filters": [
							function() {
								var filterGroup = Ext.create("Terrasoft.FilterGroup");
								var contact = Terrasoft.SysValue.CURRENT_USER_CONTACT;
								if (contact) {
									filterGroup.add("OwnerFilter",
										Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "Owner.Id", contact.value));
									filterGroup.add("IsEmployee",
										Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Type",
											ConfigurationConstants.ContactType.Employee,
											Terrasoft.DataValueType.GUID));
								}
								
								return filterGroup;
							}
						]
					}
				},

				"IsOwnerEnable": {
					"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},

				"Amount": {
					"onChange": "onAmountChanged"
				},

				"IsAmountEnable": {
					"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},

				"PaymentFormVisibility": {
					"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},

				"ITSignatory": {
					"lookupListConfig": {
						"columns": ["ITSignatory"],
						"filters": [
							function() {
								var filterGroup = this.Ext.create("Terrasoft.FilterGroup");
								var account = this.get("OurCompany");
								if (account) {
									filterGroup.add("ITSignatoryByLeadType",
										this.Terrasoft.createColumnFilterWithParameter(
											this.Terrasoft.ComparisonType.EQUAL,
											"TsAccount.Id", account.value));
								}
								return filterGroup;
							}
						]
					}
				},

				"EqualToSigningDate": {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"dependencies": [
						{
							columns: ["ITEqualWhithSigningDate"],
							methodName: "onEqualToSigningDateChanged"
						}
					]
				},

				"Account": {
					"dependencies": [
						{
							columns: ["Account"],
							methodName: "onAccountChange"
						}
					]
				},
				
				"Contact": {
					"lookupListConfig": {
						"filters": [
							function() {
								var filterGroup = Ext.create("Terrasoft.FilterGroup"),
									currentClient = this.get("Account");
								if (currentClient) {
									filterGroup.add("filterByClient",
										Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Account.Id",
											currentClient.id,
											Terrasoft.DataValueType.GUID));
								} else {
									filterGroup.add("filterByAccountType",
										Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Account.Type",
											"03A75490-53E6-DF11-971B-001D60E938C6",
											Terrasoft.DataValueType.GUID));
								}
								return filterGroup;
							}
						]
					}
				},
				"ITOpportunityInContract": {
					"lookupListConfig": {
						"columns": ["Amount", "ITAmountNDS", "Owner", "LeadType"],
						"filters": [
							function() {
								var filterGroup = Ext.create("Terrasoft.FilterGroup");
								filterGroup.logicalOperation = this.Terrasoft.LogicalOperatorType.OR;
								var account = this.get("Account");
								var ourCompany = this.get("OurCompany");
								var owner = this.get("Owner");
								if (account) {
									filterGroup.add("AccountFilter",
										Terrasoft.createColumnFilterWithParameter(
											this.Terrasoft.ComparisonType.EQUAL,
											"Account.Id",
											account.value));
								}
								if (ourCompany) {
									filterGroup.add("OurCompanyFilter",
										Terrasoft.createColumnFilterWithParameter(
											this.Terrasoft.ComparisonType.EQUAL,
											"Owner.TsAccount.Id",
											ourCompany.value));
								}
								if (owner && owner.TsLeadType) {
									filterGroup.add("ByTsLeadTypeFilter",
										Terrasoft.createColumnFilterWithParameter(
											this.Terrasoft.ComparisonType.EQUAL,
											"LeadType.Id",
											owner.TsLeadType.value));
								}
								return filterGroup;
							}
						]
					},
					"dependencies": [
						{
							columns: ["ITOpportunityInContract"],
							methodName: "onChangeOpportunity"
						}
					]
				},

				"State": {
					"dependencies": [
						{
							columns: ["State"],
							methodName: "onStateChange"
						}
					]
				},

				"OurCompany": {
					"dependencies": [
						{
							columns: ["OurCompany"],
							methodName: "onOurCompanyChange"
						}
					]
				},

				"ITContractTypeIT": {
					"dependencies": [
						{
							columns: ["ITContractTypeIT"],
							methodName: "onITContractTypeITChange"
						}
					]
	//				"onChange": "onITContractTypeITChange"
				},

				"ITContractTypeITVisibility": {
					"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},

				"ITPaymentForm": {
					"onChange": "onITPaymentFormChanged"
				},

				"Currency": {
					lookupListConfig: {
						columns: ["Rate"]
					}
				},

				//Флаг для валидации поля "Срок 100% погашения"
				"isAfterInitialized": {
					"dataValueType": this.Terrasoft.DataValueType.BOOLEAN,
					"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},

				"ITPurchaseMethod": {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					dependencies: [
						{
							columns: ["ITPurchaseMethod"],
							methodName: "onITPurchaseMethodChange"
						}
					],
					lookupListConfig: {
						orders: [
							{
								columnPath: "ITIndexNumber",
								orderDirection: Terrasoft.OrderDirection.ASC
							}
						]
					}
				},

				"OnChangeStartDate": {
					"dependencies": [
						{
							columns: ["StartDate"],
							methodName: "onChangeStartDate"
						}
					]
				},

				"OnChangeSignedDate": {
					"dependencies": [
						{
							columns: ["ITSignedDate"],
							methodName: "onChangeSignedDate"
						}
					]
				},

				// RPCRM-968
				CompletenessValue: {
					dataValueType: Terrasoft.DataValueType.INTEGER,
					value: 0
				},
				MissingParametersCollection: {
					dataValueType: Terrasoft.DataValueType.COLLECTION
				}
			},

			mixins: {
				CompletenessMixin: "Terrasoft.CompletenessMixin", // RPCRM-968
				TooltipUtilitiesMixin: "Terrasoft.TooltipUtilities" // RPCRM-968
			},

			rules: {

				//Видимость поля "Комментарий"
				"ITComment": {
					DurationBulkEmailVisible: {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.VISIBLE,
						logical: Terrasoft.LogicalOperatorType.AND,
						conditions: [
							{
								leftExpression: {
									type: BusinessRuleModule.enums.ValueType.ATTRIBUTE,
									attribute: "ITPurchaseMethod"
								},
								comparisonType: Terrasoft.core.enums.ComparisonType.EQUAL,
								rightExpression: {
									type: BusinessRuleModule.enums.ValueType.CONSTANT,
									value: ITJsContractConstants.PurchaseMethod.Other
								}
							}
						]
					}
				},

				//		Фильтрация Подписантов по Блоку бизнеса
				"ITSignatory": {
					"FiltrationITSignatoryByOwnerBusiness": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": true,
						"autoClean": true,
						"baseAttributePatch": "TsLeadType",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
						"attribute": "ForRulesAtt"
					}
				},
				"Account": {
					"FiltrationAccountByType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": true,
						"autoClean": true,
						"baseAttributePatch": "Type",
						"comparisonType": Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": "03a75490-53e6-df11-971b-001d60e938c6"
					}
				}
			},

			methods: {


				init: function() {
					this.set("MissingParametersCollection", this.Ext.create("Terrasoft.BaseViewModelCollection")); //RPCRM-968
					this.callParent(arguments);
				},

				// RPCRM-968
				onDetailChanged: function() {
					this.callParent(arguments);
					this.calculateCompleteness();
				},

				meetingsFilter: function() {
					var filterGroup = new this.Terrasoft.createFilterGroup();
					filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "42C74C49-58E6-DF11-971B-001D60E938C6"));
					// добавляете фильтр по типу активности
					filterGroup.add("ContractFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Contract", this.get("Id")));
					return filterGroup;
				},
	
	
	
				
				callsActivityFilter: function() {
					var filterGroup = new this.Terrasoft.createFilterGroup();
					filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "03DF85BF-6B19-4DEA-8463-D5D49B80BB28"));
					// добавляете фильтр по типу активности
					filterGroup.add("ContractFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Contract", this.get("Id")));
					return filterGroup;
				},
	
	
	
				
				tasksActivityFilter: function() {
					var filterGroup = new this.Terrasoft.createFilterGroup();
					filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "F51C4643-58E6-DF11-971B-001D60E938C6"));
					// добавляете фильтр по типу активности
					filterGroup.add("ContractFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Contract", this.get("Id")));
					return filterGroup;
				},

				onEntityInitialized: function() {
					this.callParent(arguments);
					if (this.isEditMode()) { // RPCRM-968
						var collection = this.get("MissingParametersCollection");
						collection.clear();
						this.set("CompletenessValue", 0);
						this.calculateCompleteness();
					}
					this.setContractDefaultValues();
					this.setAmountFromOpportunity();
					this.afterInitialized();
					this.contact = this.get("Contact");
				},
				
				//Подписка на сообщения
				subscribeSandboxEvents: function() {
					this.callParent(arguments);
					this.sandbox.subscribe("OpportunityProductTotalsChanged", this.onOpportunityAmount, this, []);
				},

				setContractDefaultValues: function() {
					this.onSignatoryLoad();
					this.setOpportunityEnable();
					this.setOwnerEnabled();
					this.onOurCompanyChange();
					this.setForRulesAtt();
					if (this.isNew || this.isCopyMode()) {
						this.setOurCompanyValue();
						this.onOpportunityAmount();
						this.onAccountChange();
					}
				},

				getColumnValueFromContract: function(columnName) {
					var column = this.get(columnName);
					if (column && column.value) {
						return column.value;
					} else {return null; }
				},

				//Срабатывает при изменении поля "Способ закупки"
				onITPurchaseMethodChange: function() {
					var type = this.get("ITPurchaseMethod"),
						typeOther = ITJsContractConstants.PurchaseMethod.Other;
					if (type && type.value !== typeOther) {this.set("ITComment", null); }
					
				},

				//Флаги для валидации поля "Срок 100% погашения"
				afterInitialized: function() {
					this.onInitDate = this.get("ITFullPaymentDate");
					this.set("isAfterInitialized", true);
				},

				//Валидация полей формы оплаты
				setValidationConfig: function() {
					this.callParent(arguments);
					this.addColumnValidator("ITPrepaymentPercentage", this.ITPrepaymentPercentageValidation);
					this.addColumnValidator("ITFullPaymentDate", this.ITFullPaymentDateValidation);
				},

				//Выполняет проверку валидации поля "% авансирования"
				ITPrepaymentPercentageValidation: function() {
					var percent = this.get("ITPrepaymentPercentage"),
						visible = this.get("PaymentFormVisibility"),
						invalidMessage = "";
					if (visible) {
						if (percent == null || percent < 5 || percent > 100) {
							invalidMessage = resources.localizableStrings.PercentageValidationMessage;
						}
					}
					return {
						fullInvalidMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
				},

				//Выполняет проверку валидации поля "Срок 100% погашения"
				ITFullPaymentDateValidation: function() {
					var nowDate = new Date(),
						isAfterInitialized = this.get("isAfterInitialized"),
						userDate = this.get("ITFullPaymentDate"),
						visible = this.get("PaymentFormVisibility"),
						invalidMessage = "";
					
					if (isAfterInitialized && visible) {
						if (userDate === null) {
							invalidMessage = resources.localizableStrings.PaymentDateValidationMessage;
						} else if (userDate !== this.onInitDate) {
							//Проверка даты на правило "Дата должна быть в будущем"
							switch (userDate.getFullYear() > nowDate.getFullYear()) {
								case true:
									break;
								default:
									switch (userDate.getFullYear() === nowDate.getFullYear()) {
										case true:
											switch (userDate.getMonth() > nowDate.getMonth()) {
												case true:
													break;
												default:
													switch (userDate.getMonth() === nowDate.getMonth()) {
														case true:
															switch (userDate.getDate() < nowDate.getDate()) {
																case true:
																	invalidMessage = resources.localizableStrings.PaymentDateValidationMessage;
																	break;
																default:
															}
															break;
														default:
															invalidMessage = resources.localizableStrings.PaymentDateValidationMessage;
													}
											}
											break;
										default:
											invalidMessage = resources.localizableStrings.PaymentDateValidationMessage;
									}
							}
						}
					}
					return {
						fullInvalidMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
				},

				onDocumentTypeChanged: function() {
					var docType = this.get("ITDocumentType");
					if (!docType) {
						this.set("ITContractTypeITVisibility", false);
						this.set("ITContractTypeIT ", null);
						return;
					} else {
						switch (docType.value === ITJsContractConstants.DocumentType.Contract) {
							case true:
								this.set("ITContractTypeITVisibility", true);
								break;
							default:
								this.set("ITContractTypeITVisibility", false);
								this.set("ITContractTypeIT ", null);
						}
					}
				},

				setForRulesAtt: function() {
					var owner = this.get("Owner");
					if (owner) {
						this.set("ForRulesAtt", owner.TsLeadType);
					}
				},

				//Устанавливает значение поля "Подразделение ФГУП "Почта России""
				setOurCompanyValue: function() {
					var currentContact = Terrasoft.SysValue.CURRENT_USER_CONTACT;
					if (currentContact) {
						var select = this.Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "Contact"
	//Кэширование значения - сначала достается из БД, а потом используется из кэша, запросы в БД не идут						
	/*						clientESQCacheParameters: {
								cacheItemName: "SetOurCompanyValue"
							}*/
						});
						select.addColumn("TsAccount");
						select.addColumn("ITMacroregion");
						select.getEntity(currentContact.value, function(result) {
							if (result.success) {
								var ufps = result.entity.get("TsAccount"),
									mrc = result.entity.get("ITMacroregion");
								if (ufps && ufps !== "") {
									this.set("OurCompany", ufps);
								} else {
									this.set("OurCompany", mrc);
								}
							}
						}, this);
					}
				},

				setOwnerEnabled: function() {
					var currentContact = Terrasoft.SysValue.CURRENT_USER_CONTACT;
					if (currentContact) {
						var select = this.Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "SysUserInRole",
							clientESQCacheParameters: {
								cacheItemName: "GetOwnerEnabled"
							}
						});
						select.addColumn("Id");
						select.filters.add("ContactFilter", this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "SysUser.Contact.Id", currentContact.value));
						select.filters.add("RoleFilter", this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "SysRole.Id", ITJsConsts.FuncStructure.HeadOfSalesDepartment));
						select.getEntityCollection(function(result) {
							if (result.success && result.collection.getCount() > 0) {
								this.set("IsOwnerEnable", true);
							}
						}, this);
					}
				},

				onOurCompanyChange: function() {
					var account = this.get("OurCompany");
					this.getAccountBillingInfo(account, function(result) {
						this.set("SupplierBillingInfo", result);
					}, this);
				},

				onAccountChange: function() {
					var account = this.get("Account");
					this.getAccountBillingInfo(account, function(result) {
						this.set("CustomerBillingInfo", result);
					}, this);
					this.setTsSupportEmployee();
				},

				/**
				* Получить единственный реквизит
				* @param account
				* @param callback
				* @param scope
				*/
				getAccountBillingInfo: function(account, callback, scope) {
					if (account) {
						var select = this.Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "AccountBillingInfo"
						});
						select.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_COLUMN, "Id");
						select.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_DISPLAY_COLUMN, "Name");
						select.filters.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "Account.Id", account.value));
						select.getEntityCollection(function(result) {
							if (result.success && result.collection.getCount() === 1) {
								var entities = result.collection.getItems();
								var config = {
									displayValue: entities[0].get("Name"),
									value: entities[0].get("Id")
								};
								if (callback) {
									callback.call(scope, config);
								}
							}
						}, this);
					}
				},

				setOpportunityEnable: function() {
					var changedValues = this.changedValues;
					if (changedValues && changedValues.DefaultValues && changedValues.DefaultValues.length > 0 &&
						Terrasoft.utils.array.findWhere(changedValues.DefaultValues, {name: "ITOpportunityInContract"}))
					{
						this.set("ITOpportunityInContractEnable", false);
					}
				},

				setTsSupportEmployee: function() {
					var account = this.get("Account");
					var leadType = this.get("TsLeadType");
					if (account && leadType) {
						var select = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "ITVwAccountOwner"});
						select.addColumn("ITOwnerSupplier");
						select.filters.logicalOperation = Terrasoft.LogicalOperatorType.AND;
						select.filters.add("ITAccountFilter", this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "ITAccount.Id", account.value));
						select.filters.add("ITLeadTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "ITLeadType.Id", leadType.value));
						select.getEntityCollection(function(result) {
							var tsSupportEmployeeEnable = true;
							if (result.success && result.collection.getCount() === 1) {
								var entities = result.collection.getItems();
								var ownerSupplier = entities[0].get("ITOwnerSupplier");
								this.set("TsSupportEmployee", ownerSupplier);
								if (ownerSupplier && !Ext.isEmpty(ownerSupplier)) {
									tsSupportEmployeeEnable = false;
								}
							}
							else {
								this.set("TsSupportEmployee", null);
							}
							this.set("TsSupportEmployeeEnable", tsSupportEmployeeEnable);
						}, this);
					}

				},

				// Заполнение полей "Сумма", "Сумма (в т.ч. НДС)", "Ответственный по продаже",
				//"Направление продажи" по полю "Продажа"
				onOpportunityAmount: function() {
					var opportunity = this.get("ITOpportunityInContract");
					if (opportunity) {
						this.set("Amount", opportunity.Amount || null);
						this.set("ITAmountNDS", opportunity.ITAmountNDS || null);
						this.set("ITOpportunityOwner", opportunity.Owner || null);
						this.set("TsLeadType", opportunity.LeadType || null);
						this.setTsSupportEmployee();
					}
				},

				copyInvoiceProducts: function(sourceEntityItems, callback) {
					var batchQuery = this.Ext.create("Terrasoft.BatchQuery");
					sourceEntityItems.each(function(entity) {
						var insertQuery = this.Ext.create("Terrasoft.InsertQuery", {
							rootSchemaName: "InvoiceProduct"
						});
						delete entity.columns.Id;
						this.Terrasoft.each(entity.columns, function(column) {
							if (column.columnPath === "Invoice") {
								insertQuery.setParameterValue(column.columnPath, this.get("Id"),
									column.dataValueType);
							} else {
								if (!this.Ext.isEmpty(entity.get(column.columnPath))) {
									insertQuery.setParameterValue(column.columnPath,
										entity.get(column.columnPath), column.dataValueType);
								}
							}
						}, this);
						batchQuery.add(insertQuery);
					}, this);
					batchQuery.execute(callback, this);
				},

				//Срабатывает при изменении/заполнении Amount
				onAmountChanged: function() {
					this.countNDS();
				},

				//Подсчитывает значение колонки Сумма с НДС
				countNDS: function() {
					var amount = this.get("Amount"), amountNDS;
					amountNDS = amount * 1.18;
					this.set("ITAmountNDS", amountNDS);
				},

				//При смене стадии
				onStateChange: function() {
					var state = this.get("State");
					if (state) {
						if (state.displayValue === "Подписан") {
							this.set("ITSignedDate", new Date());
						} else {
							this.set("ITSignedDate", null);
						}
					}
				},

				//Заполнение поля "Подписант"
				onSignatoryLoad: function() {
					var ifOwner = this.get("Owner");
					var ifSign = this.get("ITSignatory");
					if (ifOwner && ifSign) {
						var maxSign;
						var maxMod = 0;
						var select = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Contract"});
						select.addColumn("Id", "IdSignatory");
						select.addColumn("ModifiedOn", "ModifiedOn");
						select.filters.logicalOperation = Terrasoft.LogicalOperatorType.AND;
						select.filters.add("ByOwnerId", this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "Owner.Id", this.get("Owner").value));
						select.filters.add("ByNotThisContractId", this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.NOT_EQUAL, "Id", this.get("Id")));
						select.getEntityCollection(function(result) {
							if (result.success && result.collection.getCount() > 0) {
								result.collection.each(function(item) {
									if (item.get("ModifiedOn") > maxMod) {
										maxMod = item.get("ModifiedOn");
										maxSign = item.get("IdSignatory");
									}
								});
								var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Contract"});
								esq.addColumn("ITSignatory", "Signatory");
								esq.getEntity(maxSign, function(result) {
									if (!this.Ext.isEmpty(result.entity.get("Signatory"))) {
										this.set("ITSignatory", result.entity.get("Signatory"));
									}
								}, this);
							}
						}, this);
					}
				},

				onEqualToSigningDateChanged: function() {
					var remindToOwner = this.get("ITEqualWhithSigningDate");
					if (remindToOwner) {
						var startDate = this.Terrasoft.deepClone(this.get("StartDate"));
						if (!Ext.isDate(startDate)) {
							return;
						}
						startDate = this.Terrasoft.clearSeconds(startDate);
						this.set("ITSignedDate", startDate);
						this.set("ITEqualWhithSigningDate", true);
					} else {
						this.set("ITSignedDate", null);
						this.set("ITEqualWhithSigningDate", false);
					}
				},

				getEndDateVisible: function() {
					var contractType = this.get("ITContractTypeIT");
					return (contractType) && (
						contractType.value === ITJsContractConstants.ContractType.ExpressTypedId ||
						contractType.value === ITJsContractConstants.ContractType.WithAutoProlongationTypeId);
				},

				getAmountEnable: function() {
					var contractType = this.get("ITContractTypeIT");
					var enable = (contractType &&
					(
					contractType.value === ITJsContractConstants.ContractType.ExpressTypedId ||
					contractType.value === ITJsContractConstants.ContractType.WithAmountLimit)
					);
					return enable;
				},

				onITContractTypeITChange: function() {
					var amountEnable = true;
					if (!this.getAmountEnable()) {
						amountEnable = false;
						this.set("Amount", null);
					}
					this.set("IsAmountEnable", amountEnable);
				},

				//Усановка видимости полей формы оплаты и их обнуление
				onITPaymentFormChanged: function() {
					var paymentForm = this.get("ITPaymentForm");
					if (paymentForm && paymentForm.value !== ITJsContractConstants.PaymentForm.PostPay) {
						this.set("PaymentFormVisibility", false);
						this.set("ITPrepaymentPercentage", null);
						this.set("ITFullPaymentDate", null);
					} else {this.set("PaymentFormVisibility", true); }
				},

				setAmountFromOpportunity: function() {
					Terrasoft.SysSettings.querySysSettings(["PrimaryCurrency"], function(sysSettings) {
						var primaryCurrency = sysSettings.PrimaryCurrency;
						var currentopp = this.get("ITOpportunityInContract");
						if ((this.isNewMode()) && (currentopp)) {
							this.set("Amount", currentopp.Amount);
							this.set("PrimaryAmount", currentopp.Amount);
							this.set("Currency", primaryCurrency);
		//					this.set("CurrencyRate", Currency.Rate);
						}
					}, this);
				},

				callCustomProcess: function() {
					var contract = this.get("Id");
					var args = {
						sysProcessName: "ITAddProductsToContract",
						parameters: {
							ProcessSchemaContractParameter: contract
						}
					};
					ProcessModuleUtilities.executeProcess(args);
				},

				onChangeOpportunity: function() {
					this.onOpportunityAmount();
					this.updateProducts();
					this.clearITOpportunityFields();
				},

	/*			// Метод обновляет значение колонки "ITOpportunityInContract" в БД при изменении поля "Продажа", затем
				// запускает бизнес-процесс, который заполняет деталь "Предмет договора" услугами из выбранной продажи,
				// предварительно очищая список услуг в детали "Предмет договора".
				updateProducts: function() {
					var opportunity = this.get("ITOpportunityInContract");
					if (opportunity) {
						var update = this.Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "Contract"});
						update.enablePrimaryColumnFilter(this.get("Id"));
						update.setParameterValue("ITOpportunityInContract", opportunity.value,
							this.Terrasoft.DataValueType.GUID);
						update.execute(function(result) {
							if (result.success) {
								this.callCustomProcess();
							}
						}, this);
						this.updateITContractSubjectDetail();
					}
				},*/

				// Переопределен базовый метод валидации обязательных полей при сохранении карточки.
				// Если есть незаполненные поля, карточка не сохраняется и поле "Продажа" очищается.
				validateResponse: function() {
					var isSuccess = this.mixins.EntityResponseValidationMixin.validateResponse.apply(this, arguments);
					if (!isSuccess) {
						this.set("ITOpportunityInContract", null);
						this.hideBodyMask();
					}
					return isSuccess;
				},

				//Проверяет режим добавления новой записи и запускает процесс добавления услуг из продажи
				onCreatingContractFromOpportunity: function() {
					if (this.isNewMode()) {this.callProcess(); }
				},

				callProcess: function() {
					var opportunity = this.get("ITOpportunityInContract");
					if (opportunity) {
						this.callCustomProcess();
						this.updateITContractSubjectDetail();
					}
				},

				// Если карточка договора новая (её Id нет в БД), то при добавлении продажи пользователю будет
				// предложено сохранить карточку договора. Если пользователь выберет "ДА", то добавится продажа,
				// карточка договора сохранится и услуги из продажи добавятся в деталь "Предмет договора". Если
				// пользователь выберет "НЕТ", то связанные с продажей поля очистятся. Если карточка не новая, то,
				// при добавлении продажи, Id продажи записывается в БД, затем запускается процесс, который
				// добавляет в договор услуги из продажи.
				updateProducts: function() {
					var opportunity = this.get("ITOpportunityInContract");
					if (this.isNewMode() && opportunity) {
						var confirmationMessage = "Перед добавлением продажи необходимо сохранить карточку договора. Сохранить сейчас?";
						this.showConfirmationDialog(confirmationMessage, function(result) {
							if (result === this.Terrasoft.MessageBoxButtons.YES.returnCode) {
								var config = {
									callback: function(result) {
										this.callProcess();
									},
									isSilent: true
								};
								return this.save(config);
							} else {
								this.set("ITOpportunityInContract", null);
								this.set("ITOpportunityOwner", null);
								this.set("TsLeadType", null);
								this.set("TsSupportEmployee", null);
								this.set("Amount", null);
								return false;
							}
						}, ["yes", "no"]);
					}
					if (!this.isNewMode() && opportunity) {
						var update = this.Ext.create("Terrasoft.UpdateQuery", {rootSchemaName: "Contract"});
						update.enablePrimaryColumnFilter(this.get("Id"));
						update.setParameterValue("ITOpportunityInContract", opportunity.value,
							this.Terrasoft.DataValueType.GUID);
						update.execute(function(result) {
							if (result.success) {
								this.callCustomProcess();
							}
						}, this);
						this.updateITContractSubjectDetail();
					}
				},

				updateITContractSubjectDetail: function() {
					setTimeout(function(scope) {
						scope.updateDetail({detail: "ITContractSubjectDetail", reloadAll: true});
					}, 2000, this);
				},

				// Метод очищает список услуг в детали "Предмет договора" в случае, если поле "Продажа" пустое.
				clearITContractSubjectDetail: function() {
					var opportunity = this.get("ITOpportunityInContract");
					if (!opportunity) {
						var deleteQuery = Ext.create("Terrasoft.DeleteQuery", {
							rootSchemaName: "ITContractSubject"
						});
						var contractIdFilter = Terrasoft.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.EQUAL, "ITContract", this.get("Id"));
						deleteQuery.filters.add("contractIdFilter", contractIdFilter);
						deleteQuery.execute(function(result) {
							if (result.success) {
								this.updateDetail({detail: "ITContractSubjectDetail", reloadAll: true});
							}
						}, this);
					}
				},

				// Если поле "Продажа" не заполнено, очищает нижеперечисленные поля.
				clearITOpportunityFields: function() {
					var opportunity = this.get("ITOpportunityInContract");
					if (!opportunity) {
						this.set("ITOpportunityOwner", null);
						this.set("TsLeadType", null);
						this.set("TsSupportEmployee", null);
						this.set("Amount", null);
					}
				},

				onSaved: function() {
					var contractId = this.get("Id");
					this.callParent(arguments);
					if (!this.isNewMode() && !this.get("IsProcessMode")) { // RPCRM-968
						this.calculateCompleteness();
					}
					this.clearITContractSubjectDetail();
					this.contractInContact(contractId);
				},

				// RPCRM-968
				calculateCompleteness: function() {
					var config = {
						recordId: this.get("Id"),
						schemaName: this.entitySchemaName
					};
					this.mixins.CompletenessMixin.getCompleteness.call(this, config, this.calculateCompletenessResponce, this);
				},

				// RPCRM-968
				calculateCompletenessResponce: function(completenessResponce) {
					if (this.Ext.isEmpty(completenessResponce)) {
						return;
					}
					var missingParametersCollection = completenessResponce.missingParametersCollection;
					var completeness = completenessResponce.completenessValue;
					var scale = completenessResponce.scale;
					if (!this.Ext.isEmpty(missingParametersCollection)) {
						var collection = this.get("MissingParametersCollection");
						collection.clear();
						collection.loadAll(missingParametersCollection);
					}
					if (this.Ext.isObject(scale) && this.Ext.isArray(scale.sectorsBounds)) {
						this.set("CompletenessSectorsBounds", scale.sectorsBounds);
					}
					if (this.Ext.isNumber(completeness)) {
						this.set("CompletenessValue", completeness);
					}
				},

				// Удаление действия "Отправить на визирование" из меню действий.
				getActions: function() {
					var actions = this.callParent(arguments);
					actions.each(function(obj, index) {
						if (obj && obj.values.Tag === "sendToVisa") {
							actions.removeByIndex(index);
						}
					});
					return actions;
				},

				// Метод отрабатывает при добавлении нового договора из карточки контакта. Записывает Id контакта в договор
				// (деталь "Договоры" в контакте).
				contractInContact: function(contractId) {
					if (this.contact && this.contact.value) {
						if (this.isNewMode && this.isAddMode) {
							var update = this.Ext.create("Terrasoft.UpdateQuery", { rootSchemaName: "Contract" });
							update.filters.addItem(Terrasoft.createColumnFilterWithParameter(
								Terrasoft.ComparisonType.EQUAL, "Id", contractId));
							update.setParameterValue("Contact", this.contact.value, this.Terrasoft.DataValueType.GUID);
							update.execute();
						}
					}
				},

				// Если проставлена галка в чекбоксе "Совпадает с датой подписания",
				// устанавливает дату подписания равную дате начала.
				onChangeStartDate: function() {
					var checkbox = this.get("ITEqualWhithSigningDate");
					if (checkbox) {
						this.set("ITSignedDate", this.get("StartDate"));
					}
				},

				// Если изменить дату подписания при проставленной галке в чекбоксе "Совпадает с датой подписания",
				// снимает галку, если дата подписания не совпадает с датой начала.
				onChangeSignedDate: function() {
					var checkbox = this.get("ITEqualWhithSigningDate");
					if (checkbox) {
			//			var startDate = this.get("StartDate");
			//			var signedDate = this.get("SignedDate");
						if (this.get("StartDate") !== this.get("ITSignedDate")) {
							this.set("ITEqualWhithSigningDate", false);
						}
					}
				}
			},
			details: {
				/*		"OrderDetailV2": {
				"schemaName": "OrderDetailV2",
				"entitySchemaName": "Order",
				"filter": {
				"detailColumn": "Number",
				"masterColumn": "Id"
				}
				},*/
				/*"OpportunityProductDetailV2": {
				"schemaName": "OpportunityProductDetailV2",
				"entitySchemaName": "OpportunityProductInterest",
				"filter": {
				"detailColumn": "Opportunity",
				"masterColumn": "ITOpportunityInContract"
				}
				}*/

				"ITContractSubjectDetail": {
					"schemaName": "ITContractSubjectDetail",
					"entitySchemaName": "ITContractSubject",
					"filter": {
						"detailColumn": "ITContract",
						"masterColumn": "Id"
					}
				},

				"ITMeetingsDetail": {
					"schemaName": "ITMeetingsDetailV2",
					"entitySchemaName": "Activity",
					"filter": {
						"detailColumn": "Account",
						"masterColumn": "Id"
					},
					filterMethod: "meetingsFilter"
				},

				"ITTasksDetailV2": {
					"schemaName": "ITTasksDetailV2",
					//"entitySchemaName": "Activity",
					"filter": {
						"detailColumn": "Account",
						"masterColumn": "Id"
					},
					filterMethod: "tasksActivityFilter"
				},

				"ITCallsDetail": {
					"schemaName": "ITCallsDetailV2",
					//"entitySchemaName": "Activity",
					"filter": {
						"detailColumn": "Account",
						"masterColumn": "Id"
					},
					filterMethod: "callsActivityFilter"
				}
			},

			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "insert",
					"name": "ITAmountNDS",
					"values": {
						"layout": {"colSpan": 12, "column": 12, "row": 0},
						"bindTo": "ITAmountNDS",
						"enabled": false
					},
					"parentName": "ContractSumBlock",
					"propertyName": "items"
				},

				{
					"operation": "merge",
					"name": "Owner",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1
						},
						"enabled": {"bindTo": "IsOwnerEnable"}
					}
				},

				{
					"operation": "remove",
					"name": "Type"
				},

				{
					"operation": "insert",
					"name": "ITContractPayment",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 2,
							"layoutName": "Header"
						},
						"bindTo": "ITContractPayment",
						"labelConfig": {},
						"enabled": true,
						"contentType": 3
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 8
				},

				{
					"operation": "merge",
					"name": "OurCompany",
					"values": {
						"labelConfig": {}
					}
				},

				//Удаление вкладки "Паспорт Договора"
				{
					"operation": "remove",
					"name": "ContractPassportTab"
				},
				//Удаление детали "Услуга в заказе" с вкладки "Паспорт Договора"
				{
					"operation": "remove",
					"name": "Product"
				},
				{
					"operation": "move",
					"name": "OurCompany",
					"parentName": "SideInfoBlock",
					"propertyName": "items",
					"index": 3,
					"values": {
						"layout": {
							"column": 12,
							"row": 0,
							"colSpan": 12,
							"rowSpan": 1
						}
					}
				},

				{
					"operation": "merge",
					"name": "Amount",
					"values": {
						"enabled": {"bindTo": "IsAmountEnable"}
					}
				},

				{
					"operation": "move",
					"name": "Account",
					"parentName": "SideInfoBlock",
					"propertyName": "items",
					"index": 0
				},

				{
					"operation": "merge",
					"name": "Account",
					"values": {
						"layout": {
							"column": 0,
							"row": 0,
							"colSpan": 12,
							"rowSpan": 1
						}
					}
				},

				{
					"operation": "move",
					"name": "Contact",
					"parentName": "SideInfoBlock",
					"propertyName": "items",
					"index": 2
				},

				{
					"operation": "merge",
					"name": "Contact",
					"parentName": "SideInfoBlock",
					"values": {
						"layout": {
							"column": 0,
							"row": 1,
							"colSpan": 12,
							"rowSpan": 1
						}
					}
				},

				{
					"operation": "move",
					"name": "CustomerBillingInfo",
					"parentName": "SideInfoBlock",
					"propertyName": "items",
					"index": 3
				},

				{
					"operation": "merge",
					"name": "CustomerBillingInfo",
					"values": {
						"layout": {
							"column": 0,
							"row": 2,
							"colSpan": 12,
							"rowSpan": 1
						}
					}
				},

				{
					"operation": "move",
					"name": "SupplierBillingInfo",
					"parentName": "SideInfoBlock",
					"propertyName": "items",
					"index": 5
				},

				{
					"operation": "merge",
					"name": "SupplierBillingInfo",
					"values": {
						"layout": {
							"column": 12,
							"row": 2,
							"colSpan": 12,
							"rowSpan": 1
						}
					}
				},

				{
					"operation": "insert",
					"name": "ITSignatory",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1
						}
					},
					"parentName": "SideInfoBlock",
					"propertyName": "items",
					"index": 4
				},

				{
					"operation": "insert",
					"name": "ITOpportunityInContract",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						},
						"labelConfig": {},
						"enabled": {"bindTo": "ITOpportunityInContractEnable"},
						"contentType": 5,
						"bindTo": "ITOpportunityInContract"
					},
					"parentName": "OpportunityInfoBlock",
					"propertyName": "items",
					"index": 0
				},

				{
					"operation": "insert",
					"name": "ITOpportunityOwner",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						},
						"enabled": false
					},
					"parentName": "OpportunityInfoBlock",
					"propertyName": "items",
					"index": 7
				},

				/*			{
				"operation": "insert",
				"name": "OrderDetailV2",
				"values": {
				"itemType": 2,
				"markerValue": "added-detail"
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 2
				},*/

				{
					"operation": "merge",
					"name": "TsLinkOnSEDContract",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						}
					}
				},

				{
					"operation": "move",
					"name": "TsLinkOnSEDContract",
					"parentName": "RelationsInfoBlock",
					"propertyName": "items",
					"index": 0
				},

				/*{
				"operation": "merge",
				"name": "TsLinkOnSEDContractButton",
				"values": {
				"layout": {
				"colSpan": 1,
				"rowSpan": 1,
				"column": 0,
				"row": 1
				}
				}
				},*/
				/*{
				"operation": "insert",
				"name": "OpportunityProductDetailV2",
				"values": {
				"itemType": 2,
				"markerValue": "added-detail"
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 5
				},*/

				{
					"operation": "remove",
					"name": "TsTarifficatorRegion"
				},

				{
					"operation": "remove",
					"name": "Order"
				},

				//добавление вкладки "Информация о продаже"
				{
					"operation": "insert",
					"name": "OpportunityInfoTab",
					"parentName": "Tabs",
					"propertyName": "tabs",
					"values": {
						"caption": {"bindTo": "Resources.Strings.OpportunityInfoTabCaption"},
						"items": []
					},
					"index": 1
				},

				{
					"operation": "insert",
					"parentName": "OpportunityInfoTab",
					"name": "OpportunityInfoGroup",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.CONTROL_GROUP,
						"controlConfig": {
							"collapsed": false
						},
						"items": []
					},
					"index": 0
				},

				{
					"operation": "insert",
					"parentName": "OpportunityInfoGroup",
					"propertyName": "items",
					"name": "OpportunityInfoBlock",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.GRID_LAYOUT,
						"items": [],
						"index": 0
					}
				},

				{
					"operation": "move",
					"name": "TsLeadType",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0
						},
						"enabled": false,
						"caption": {"bindTo": "Resources.Strings.TsLeadTypeCaption"}
					},
					"parentName": "OpportunityInfoBlock",
					"propertyName": "items"
				},

				{
					"operation": "move",
					"name": "TsSupportEmployee",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1
						},
						"tip": {
							"content": {
								"bindTo": "Resources.Strings.TsSupportEmployeeTip"
							}
						},
						"enabled": {"bindTo": "TsSupportEmployeeEnable"}
					},
					"parentName": "OpportunityInfoBlock",
					"propertyName": "items"
				},

				//добавление вкладки "Связи"
				{
					"operation": "insert",
					"name": "RelationsTab",
					"parentName": "Tabs",
					"propertyName": "tabs",
					"values": {
						"caption": {"bindTo": "Resources.Strings.RelationsTabCaption"},
						"items": []
					},
					"index": 2
				},

				{
					"operation": "insert",
					"parentName": "RelationsTab",
					"name": "RelationsInfoGroup",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.CONTROL_GROUP,
						"controlConfig": {
							"collapsed": false
						},
						"items": []
					},
					"index": 0
				},

				{
					"operation": "remove",
					"name": "TsLinkOnSEDContractButton"
				},

				{
					"operation": "insert",
					"parentName": "RelationsInfoGroup",
					"propertyName": "items",
					"name": "RelationsInfoBlock",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.GRID_LAYOUT,
						"items": [],
						"index": 0
					}
				},

				{
					"operation": "merge",
					"name": "Parent",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						}
					}
				},

				{
					"operation": "move",
					"name": "Parent",
					"parentName": "RelationsInfoBlock",
					"propertyName": "items",
					"index": 1
				},

				{
					"operation": "move",
					"name": "SubordinateContracts",
					"parentName": "RelationsTab",
					"propertyName": "items",
					"index": 2
				},

				//изменение структуры блока полей общей информации
				{
					"operation": "merge",
					"name": "Number",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0
						}
					}
				},

				{
					"operation": "remove",
					"name": "StartDate"
				},

				{
					"operation": "insert",
					"name": "ITDocumentType",
					"values": {
						"contentType": 3,
						"layout": {
							"colSpan": 11,
							"rowSpan": 1,
							"column": 1,
							"row": 0,
							"layoutName": "Header"
						}
					},
					"parentName": "Header",
					"propertyName": "items"
				},

				{
					"operation": "merge",
					"name": "State",
					"parentName": "Header",
					"values": {
						"layout": {
							"colSpan": 11,
							"rowSpan": 1,
							"column": 1,
							"row": 1
						}
					}
				},

				//переопределение позиций вкладок
				{
					"operation": "remove",
					"name": "ContractVisaTab"
				},

				{
					"operation": "merge",
					"name": "GeneralInfoTab",
					"values": {},
					"index": "0"
				},

				{
					"operation": "merge",
					"name": "HistoryTab",
					"values": {},
					"index": "3"
				},

				{
					"operation": "merge",
					"name": "NotesAndFilesTab",
					"values": {},
					"index": "4"
				},

				//изменение вкладки Основная Информация
				{
					"operation": "insert",
					"parentName": "GeneralInfoTab",
					"name": "ValidityInfoGroup",
					"propertyName": "items",
					"values": {
						"caption": {"bindTo": "Resources.Strings.ValidityInfoGroupCaption"},
						"itemType": this.Terrasoft.ViewItemType.CONTROL_GROUP,
						"controlConfig": {
							"collapsed": false
						},
						"items": []
					},
					"index": 0
				},

				{
					"operation": "insert",
					"parentName": "ValidityInfoGroup",
					"propertyName": "items",
					"name": "ValidityInfoBlock",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.GRID_LAYOUT,
						"items": [],
						"index": 0
					}
				},

				{
					"operation": "insert",
					"name": "StartDate",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						}
					},
					"parentName": "ValidityInfoBlock",
					"propertyName": "items"
				},

				{
					"operation": "insert",
					"name": "ITSignedDate",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						}
					},
					"parentName": "ValidityInfoBlock",
					"propertyName": "items"
				},

				{
					"operation": "move",
					"name": "EndDate",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2
						},
						"visible": {"bindTo": "getEndDateVisible"}
					},
					"parentName": "ValidityInfoBlock",
					"propertyName": "items"
				},

				{
					"operation": "insert",
					"name": "ITEqualWhithSigningDate",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0
						}
					},
					"parentName": "ValidityInfoBlock",
					"propertyName": "items"
				},

				{
					"operation": "insert",
					"parentName": "GeneralInfoTab",
					"name": "SideInfoGroup",
					"propertyName": "items",
					"values": {
						"caption": {"bindTo": "Resources.Strings.SideInfoGroupCaption"},
						"itemType": this.Terrasoft.ViewItemType.CONTROL_GROUP,
						"controlConfig": {
							"collapsed": false
						},
						"items": []
					},
					"index": 1
				},

				{
					"operation": "insert",
					"parentName": "SideInfoGroup",
					"propertyName": "items",
					"name": "SideInfoBlock",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.GRID_LAYOUT,
						"items": [],
						"index": 0
					}
				},

				{
					"operation": "remove",
					"name": "ContractConnectionsGroup"
				},

				//добавление полей в группу полей Сумма
				{
					"operation": "insert",
					"name": "ITPaymentForm",
					"values": {
						"layout": {"colSpan": 12, "rowSpan": 1, "column": 0, "row": 1},
						"bindTo": "ITPaymentForm",
						"contentType": 3
					},
					"parentName": "ContractSumBlock",
					"propertyName": "items"
				},

				{
					"operation": "insert",
					"name": "ITPrepaymentPercentage",
					"values": {
						"layout": {"colSpan": 12, "rowSpan": 1, "column": 0, "row": 2},
						"bindTo": "ITPrepaymentPercentage",
						"caption": {"bindTo": "Resources.Strings.ITPrepaymentPercentageCaption"},
						"visible": {"bindTo": "PaymentFormVisibility"},
						"isRequired": true
					},
					"parentName": "ContractSumBlock",
					"propertyName": "items"
				},

				{
					"operation": "insert",
					"name": "ITFullPaymentDate",
					"values": {
						"layout": {"colSpan": 12, "rowSpan": 1, "column": 12, "row": 2},
						"bindTo": "ITFullPaymentDate",
						"visible": {"bindTo": "PaymentFormVisibility"},
						"isRequired": true
					},
					"parentName": "ContractSumBlock",
					"propertyName": "items"
				},

				{
					"operation": "insert",
					"name": "ITContractTypeIT",
					"values": {
						"layout": {"colSpan": 11, "rowSpan": 1, "column": 1, "row": 2},
						"bindTo": "ITContractTypeIT",
						"contentType": 3,
						"visible": {"bindTo": "ITContractTypeITVisibility"}
					},
					"parentName": "Header",
					"propertyName": "items"
				},

				{
					"operation": "insert",
					"name": "ITPurchaseMethod",
					"values": {
						"layout": {"colSpan": 11, "column": 1, "row": 3},
						"bindTo": "ITPurchaseMethod",
						"contentType": 3,
						"visible": {"bindTo": "ITContractTypeITVisibility"}
					},
					"parentName": "Header",
					"propertyName": "items"
				},

				{
					"operation": "insert",
					"name": "ITComment",
					"values": {
						"layout": {"colSpan": 12, "column": 12, "row": 3},
						"bindTo": "ITComment",
						"contentType": Terrasoft.ContentType.LONG_TEXT,
						"visible": true//{"bindTo": "ITContractTypeITVisibility"}
					},
					"parentName": "Header",
					"propertyName": "items"
				},

				{
					"operation": "insert",
					"name": "ITContractSubjectDetail",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "GeneralInfoTab",
					"propertyName": "items",
					"index": 4
				},

				// скрытие детали "Счета".
				{
					"operation": "merge",
					"name": "Invoice",
					"values": {
						"visible": false
					}
				},
				//Замена Детали Активности
				{
					"operation": "remove",
					"name": "Document"
				},
				{
					"operation": "remove",
					"name": "Activities"
				},
				{
					"operation": "remove",
					"name": "Calls"
				},
				{
					"operation": "insert",
					"name": "ITMeetingsDetail",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "HistoryTab",
					"propertyName": "items",
					"index": 10
				},
				{
					"operation": "insert",
					"name": "ITTasksDetailV2",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "HistoryTab",
					"propertyName": "items",
					"index": 11
				},
				{
					"operation": "insert",
					"name": "ITCallsDetail",
					"values": {
						"itemType": 2,
						"markerValue": "added-detail"
					},
					"parentName": "HistoryTab",
					"propertyName": "items",
					"index": 12
				},



				// RPCRM-968 Добавление индикатора заполненности данными.
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "IndicatorContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"layout": {"column": 0, "row": 0, "rowSpan": 4, "colSpan": 1},
						"items": []
					}
				},
				{
					"operation": "insert",
					"parentName": "IndicatorContainer",
					"propertyName": "items",
					"name": "CompletenessContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"classes": {wrapClassName: ["completeness-container"]},
						"items": [/*
							{
								"name": "CompletenessCaption",
								"itemType": Terrasoft.ViewItemType.LABEL,
								"classes": {
									"labelClass": ["completeness-caption"]
								},
								"caption": {
									"bindTo": "Resources.Strings.CompletenessCaption"
								}
							}*/
						]
					}/*,
					"alias": {
						"name": "CompletenessContainer",
						"excludeProperties": ["layout"],
						"excludeOperations": ["remove", "move"]
					}*/
				},
				{
					"operation": "insert",
					"parentName": "CompletenessContainer",
					"propertyName": "items",
					"name": "CompletenessValue",
					"values": {
						"classes": {wrapClass: ["indicator-container"]},
						"generator": "ConfigurationRectProgressBarGenerator.generateProgressBar",
						"controlConfig": {
				//			"horizontal": true,
							"value": {
								"bindTo": "CompletenessValue"
							},
							"menu": {
								"items": {
									"bindTo": "MissingParametersCollection"
								}
							},
							"sectorsBounds": {
								"bindTo": "CompletenessSectorsBounds"
							}
						},
						"tips": []
					}
				},
				{
					"operation": "insert",
					"parentName": "CompletenessValue",
					"propertyName": "tips",
					"name": "CompletenessTip",
					"values": {
						"content": {"bindTo": "Resources.Strings.CompletenessHint"}
					}
				}



			]/**SCHEMA_DIFF*/

		};
	});
