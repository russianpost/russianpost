define("AccountBillingInfoPageV2", [],
		function() {
			return {
				entitySchemaName: "AccountBillingInfo",
				methods: {
					// Валидация поля ИНН.
					identificationNumberValidator: function() {
						var invalidMessage = "", code = "350469c4-40f9-49c5-8298-7f8a38548050";//id ИП
						var acc = this.get("Account");
						var type = acc.Ownership.value, typeLength = this.get("Account")["Ownership.TsINNLength"];
						if (!this.Ext.isEmpty(this.get("TsIdentificationNumber"))) {
							var innValue = this.get("TsIdentificationNumber");
							var digitsPattern = /^\d+$/;
							if (!innValue.match(digitsPattern)) {
								invalidMessage = this.get("Resources.Strings.InnDigitsException");
							} else {
								var currentINNLength = innValue.toString().length;
								if (type === code && currentINNLength !== 12) {
									invalidMessage = "Длина ИНН для ИП должна составлять 12 символов";
								}
								if (type !== code && (typeLength === 12 || typeLength === 0) && currentINNLength !== 12) {
									invalidMessage = "Длина ИНН должна составлять 12 символов";
								}
								if (type !== code && currentINNLength !== 10 && typeLength === 10) {
									invalidMessage = "Длина ИНН должна составлять 10 символов";
								}
							}
						}
						return {
							fullInvalidMessage: invalidMessage,
							invalidMessage: invalidMessage
						};

					},
					
					// Валидация поля КПП.
					kppValueValidator: function() {
						var invalidMessage = "";
						if (!this.Ext.isEmpty(this.get("TsKPP"))) {
							var kppValue = this.get("TsKPP");
							var digitsPattern = /^\d+$/;
							if (!kppValue.match(digitsPattern)) {
								invalidMessage = this.get("Resources.Strings.KppDigitsException");
							} else {
								var currentKppLength = kppValue.toString().length;
								if (currentKppLength !== 9) {
									invalidMessage = "Длина КПП должна составлять 9 символов";
								}
							}
						}
						return {
							fullInvalidMessage: invalidMessage,
							invalidMessage: invalidMessage
						};
					}
				}
			};
		});