define("OrderPageV2", [], function() {
	return {
		entitySchemaName: "Order",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
	{
		"operation": "merge",
		"name": "Client",
		"values": {
			"layout": {
				"colSpan": 24,
				"rowSpan": 1,
				"column": 0,
				"row": 0
			}
		}
	},
	{
		"operation": "move",
		"name": "Client",
		"parentName": "Header",
		"propertyName": "items",
		"index": 0
	},
	{
		"operation": "merge",
		"name": "TsLeadType",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 0,
				"row": 1,
				"columnSpan": 12
			}
		}
	},
	{
		"operation": "move",
		"name": "TsLeadType",
		"parentName": "Header",
		"propertyName": "items",
		"index": 1
	},
	{
		"operation": "merge",
		"name": "Amount",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 12,
				"row": 1
			}
		}
	},
	{
		"operation": "merge",
		"name": "Status",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 0,
				"row": 2,
				"columnSpan": 12
			}
		}
	},
	{
		"operation": "move",
		"name": "Status",
		"parentName": "Header",
		"propertyName": "items",
		"index": 3
	},
	{
		"operation": "merge",
		"name": "PaymentAmount",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 12,
				"row": 2
			}
		}
	},
	{
		"operation": "insert",
		"name": "OrderPassportTabGroup57dca3da",
		"values": {
			"caption": {
				"bindTo": "Resources.Strings.OrderPassportTabGroup57dca3daGroupCaption"
			},
			"itemType": 15,
			"markerValue": "added-group",
			"items": []
		},
		"parentName": "OrderPassportTab",
		"propertyName": "items",
		"index": 0
	},
	{
		"operation": "insert",
		"name": "OrderPassportTabGridLayout602fcf20",
		"values": {
			"itemType": 0,
			"items": []
		},
		"parentName": "OrderPassportTabGroup57dca3da",
		"propertyName": "items",
		"index": 0
	},
	{
		"operation": "insert",
		"name": "Numberb43e8a37-b423-44e1-b9e3-48ba70bbe6a9",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 0,
				"row": 0,
				"layoutName": "OrderPassportTabGridLayout602fcf20"
			},
			"bindTo": "Number"
		},
		"parentName": "OrderPassportTabGridLayout602fcf20",
		"propertyName": "items",
		"index": 0
	},
	{
		"operation": "insert",
		"name": "Date3fa581f9-f14b-47ca-b227-600c1f48aaa9",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 12,
				"row": 0,
				"layoutName": "OrderPassportTabGridLayout602fcf20"
			},
			"bindTo": "Date"
		},
		"parentName": "OrderPassportTabGridLayout602fcf20",
		"propertyName": "items",
		"index": 1
	},
	{
		"operation": "insert",
		"name": "Opportunityaf5bc679-11f5-4b38-b9c7-b1fd3faf3730",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 0,
				"row": 1,
				"layoutName": "OrderPassportTabGridLayout602fcf20"
			},
			"bindTo": "Opportunity"
		},
		"parentName": "OrderPassportTabGridLayout602fcf20",
		"propertyName": "items",
		"index": 2
	},
	{
		"operation": "insert",
		"name": "SourceOrderf3af5b23-ffb9-431f-b0ab-eccb81ab2b23",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 12,
				"row": 1,
				"layoutName": "OrderPassportTabGridLayout602fcf20"
			},
			"bindTo": "SourceOrder"
		},
		"parentName": "OrderPassportTabGridLayout602fcf20",
		"propertyName": "items",
		"index": 3
	},
	{
		"operation": "insert",
		"name": "DueDate2367be51-924e-45bc-acaf-501989806a82",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 0,
				"row": 2,
				"layoutName": "OrderPassportTabGridLayout602fcf20"
			},
			"bindTo": "DueDate"
		},
		"parentName": "OrderPassportTabGridLayout602fcf20",
		"propertyName": "items",
		"index": 4
	},
	{
		"operation": "insert",
		"name": "ActualDate3237c7c2-97d2-4c9b-975f-b5d027e481fa",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 12,
				"row": 2,
				"layoutName": "OrderPassportTabGridLayout602fcf20"
			},
			"bindTo": "ActualDate"
		},
		"parentName": "OrderPassportTabGridLayout602fcf20",
		"propertyName": "items",
		"index": 5
	},
	{
		"operation": "insert",
		"name": "PaymentStatus1a99d47b-1d5d-4b4f-8083-50a683de0e51",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 0,
				"row": 3,
				"layoutName": "OrderPassportTabGridLayout602fcf20"
			},
			"bindTo": "PaymentStatus"
		},
		"parentName": "OrderPassportTabGridLayout602fcf20",
		"propertyName": "items",
		"index": 6
	},
	{
		"operation": "insert",
		"name": "DeliveryStatus016f967a-2eb5-43d5-8273-1f3826c07c25",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 12,
				"row": 3,
				"layoutName": "OrderPassportTabGridLayout602fcf20"
			},
			"bindTo": "DeliveryStatus"
		},
		"parentName": "OrderPassportTabGridLayout602fcf20",
		"propertyName": "items",
		"index": 7
	},
	{
		"operation": "insert",
		"name": "Ownerb3d588b9-1347-415f-9bc7-d81c0505e3ec",
		"values": {
			"layout": {
				"colSpan": 12,
				"rowSpan": 1,
				"column": 0,
				"row": 4,
				"layoutName": "OrderPassportTabGridLayout602fcf20"
			},
			"bindTo": "Owner"
		},
		"parentName": "OrderPassportTabGridLayout602fcf20",
		"propertyName": "items",
		"index": 8
	},
	{
		"operation": "remove",
		"name": "OrderProductTab"
	},
	{
		"operation": "remove",
		"name": "ProductInProductsTab"
	},
	{
		"operation": "remove",
		"name": "OrderResultsTab"
	},
	{
		"operation": "remove",
		"name": "ProductInResultsTab"
	},
	{
		"operation": "remove",
		"name": "SupplyPaymentResults"
	},
	{
		"operation": "remove",
		"name": "AddressSelectionDetailResultsTab"
	},
	{
		"operation": "remove",
		"name": "OrderGeneralInformationTab"
	},
	{
		"operation": "remove",
		"name": "OrderPageGeneralInformationBlock"
	},
	{
		"operation": "remove",
		"name": "Number"
	},
	{
		"operation": "remove",
		"name": "Date"
	},
	{
		"operation": "remove",
		"name": "SourceOrder"
	},
	{
		"operation": "remove",
		"name": "DueDate"
	},
	{
		"operation": "remove",
		"name": "ActualDate"
	},
	{
		"operation": "remove",
		"name": "PaymentStatus"
	},
	{
		"operation": "remove",
		"name": "DeliveryStatus"
	},
	{
		"operation": "remove",
		"name": "Owner"
	},
	{
		"operation": "remove",
		"name": "Opportunity"
	},
	{
		"operation": "remove",
		"name": "OrderVisaTab"
	},
	{
		"operation": "remove",
		"name": "OrderPageVisaTabContainer"
	},
	{
		"operation": "remove",
		"name": "OrderPageVisaBlock"
	},
	{
		"operation": "remove",
		"name": "Visa"
	},
	{
		"operation": "move",
		"name": "ESNTab",
		"parentName": "Tabs",
		"propertyName": "tabs",
		"index": 3
	}
]/**SCHEMA_DIFF*/,
		methods: {},
		rules: {}
	};
});
