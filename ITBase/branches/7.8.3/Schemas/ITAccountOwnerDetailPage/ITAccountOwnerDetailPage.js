define("ITAccountOwnerDetailPage", ["BusinessRuleModule", "ConfigurationConstants"],
function(BusinessRuleModule, ConfigurationConstants) {
	return {
		entitySchemaName: "ITVwAccountOwner",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		messages: {
			"SetTsTarifficatorRegion": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.PUBLISH
			}
		},
		attributes: {
			"ITOwner": {
				"lookupListConfig": {
					"filters": [
						function() {
							var sandbox = this.sandbox,
								regionFromAccount = sandbox.publish("SetTsTarifficatorRegion", null, [sandbox.id]),
								filterGroup = Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("isAssociate",
								Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Type",
									ConfigurationConstants.ContactType.Employee,
									Terrasoft.DataValueType.GUID));
							filterGroup.add("isOurCompany",
								Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Account.Type",
									ConfigurationConstants.AccountType.OurCompany,
									Terrasoft.DataValueType.GUID));
							filterGroup.add("IsContactsByLeadType",
										Terrasoft.createColumnFilterWithParameter(
											Terrasoft.ComparisonType.EQUAL,
											"[ITContactLeadType:ITContact].ITLeadType.Id",
											this.get("ITLeadType").value));
							if (regionFromAccount) {
								filterGroup.add("byRegionAccount",
									Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "TsTarifficatorRegion",
										regionFromAccount.value,
										Terrasoft.DataValueType.GUID));
							}
							return filterGroup;
						}
					]
				}
			},
			"ITOwnerSupplier": {
				"lookupListConfig": {
					"filters": [
						function() {
							var sandbox = this.sandbox,
								regionFromAccount = sandbox.publish("SetTsTarifficatorRegion", null, [sandbox.id]),
								filterGroup = Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("isAssociate",
								Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Type",
									ConfigurationConstants.ContactType.Employee,
									Terrasoft.DataValueType.GUID));
							filterGroup.add("isOurCompany",
								Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Account.Type",
									ConfigurationConstants.AccountType.OurCompany,
									Terrasoft.DataValueType.GUID));
							filterGroup.add("IsContactsByLeadType",
										Terrasoft.createColumnFilterWithParameter(
											Terrasoft.ComparisonType.EQUAL,
											"[ITContactLeadType:ITContact].ITLeadType.Id",
											this.get("ITLeadType").value));
							if (regionFromAccount) {
								filterGroup.add("byRegionAccount",
									Terrasoft.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "TsTarifficatorRegion",
										regionFromAccount.value,
										Terrasoft.DataValueType.GUID));
							}
							return filterGroup;
						}
					]
				}
			}
		},
		methods: {},
		rules: {},
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
	};
});
