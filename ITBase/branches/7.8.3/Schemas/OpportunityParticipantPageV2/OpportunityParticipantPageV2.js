define("OpportunityParticipantPageV2", ["BusinessRuleModule"],
	function(BusinessRuleModule) {
		return {
			entitySchemaName: "OpportunityParticipant",
			attributes: {
				"Contact": {
					"dataValueType": Terrasoft.DataValueType.LOOKUP,
					"lookupListConfig": {
						"filters": [
							function() {
								var filterGroup = Ext.create("Terrasoft.FilterGroup");
								filterGroup.add("OurCompany",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Account.Type.Name",
										"Наша компания"));
								return filterGroup;
							}
						]
					}
				},
				"Account": {
					"dataValueType": Terrasoft.DataValueType.LOOKUP,
					"lookupListConfig": {
						"filters": [
							function() {
								var filterGroup = Ext.create("Terrasoft.FilterGroup");
								filterGroup.add("OurCompany",
									Terrasoft.createColumnFilterWithParameter(
										Terrasoft.ComparisonType.EQUAL,
										"Type.Name",
										"Наша компания"));
								return filterGroup;
							}
						]
					}
				}
			},
			details: /**SCHEMA_DETAILS*/{
			}/**SCHEMA_DETAILS*/,
			diff: [],
			rules: {}
		};
	});