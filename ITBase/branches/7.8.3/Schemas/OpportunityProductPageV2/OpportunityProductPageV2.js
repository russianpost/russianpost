define("OpportunityProductPageV2", ["BusinessRuleModule"],
	function(BusinessRuleModule) {
		return {
			entitySchemaName: "OpportunityProductInterest",
			attributes: {

				//Видимость кнопки расчета
				BtnVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				PricePerUnitVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				
	//			Группа полей доп. услуг
				//Видимость группы полей Дополнительные услуги
				Group1Visible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				//Видимость группы полей Дополнительные параметры
				Group2Visible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				//Атрибуты для расчета доп. услуг (caption, value, visible)
				//Value
				ITServiceAtr0: {
					"caption": {bindTo: "Serv0"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				//Caption
				Serv0: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				//Visible
				SerVis0: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled0: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr1: {
					"caption": {bindTo: "Serv1"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv1: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis1: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled1: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr2: {
					"caption": {bindTo: "Serv2"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv2: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis2: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled2: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr3: {
					"caption": {bindTo: "Serv3"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv3: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis3: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled3: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr4: {
					"caption": {bindTo: "Serv4"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv4: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis4: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled4: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr5: {
					"caption": {bindTo: "Serv5"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv5: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis5: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled5: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr6: {
					"caption": {bindTo: "Serv6"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv6: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis6: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled6: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr7: {
					"caption": {bindTo: "Serv7"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv7: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis7: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled7: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr8: {
					"caption": {bindTo: "Serv8"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv8: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis8: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled8: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr9: {
					"caption": {bindTo: "Serv9"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv9: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis9: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled9: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr10: {
					"caption": {bindTo: "Serv10"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv10: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis10: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled10: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr11: {
					"caption": {bindTo: "Serv11"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv11: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis11: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled11: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr12: {
					"caption": {bindTo: "Serv12"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv12: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis12: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled12: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				ITServiceAtr13: {
					"caption": {bindTo: "Serv13"},
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": false
				},
				Serv13: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SerVis13: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
	//				"value": "false"
				},
				ServEnabled13: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": true
				},
				
				MonthCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				MonthVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				
				//Атрибут справочной колонки ITMonthEnum
				MonthEnum: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": {bindTo: "MonthEnumCaption"}
				},
				//Заголовок колонки ITMonthEnum
				MonthEnumCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				//Видимость колонки ITMonthEnum
				MonthEnumVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				MonthCollection: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"isCollection": true
				},
				
				//Заголовок ITSum
				SumCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				SumVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				
				//Атрибут справочной колонки ITSumEnum
				SumEnum: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": {bindTo: "SumEnumCaption"}
				},
				//Заголовок колонки ITSumEnum
				SumEnumCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				//Видимость колонки ITSumEnum
				SumEnumVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				SumCollection: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"isCollection": true
				},
				
				//Заголовок колонки ITWeight
				WeightCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				//Видимость колонки ITWeight
				WeightVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				
				//Заголовок колонки ITCount
				CountCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				//Видимость колонки ITCount
				CountVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				
				//Заголовок колонки ITCountinPack
				CountInPackCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				
				//Видимость колонки ITCountinPack
				CountInPackVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				
				//Атрибут справочной колонки ITCountinPackEnum
				CountInEnum: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": {bindTo: "CountInCaption"}
				},
				//Заголовок колонки ITCountinPackEnum
				CountInCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				//Видимость колонки ITCountinPackEnum
				CountInVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				CountInCollection: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"isCollection": true
				},
				
	//			Атрибуты для колонок Дополнительных параметров
				//Справочная колонка для параметра P1
				P1Enum: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": {bindTo: "P1EnumCaption"}
				},
				//Заголовок колонки P1
				P1EnumCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				P1EnumVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"visible": false
				},
				P1EnumCollection: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"isCollection": true
				},
				//Справочная колонка для параметра P2
				P2Enum: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": {bindTo: "P2EnumCaption"}
				},
				//Заголовок колонки P2
				P2EnumCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				P2EnumVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"visible": false
				},
				P2EnumCollection: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"isCollection": true
				},
				//Справочная колонка для параметра P3
				P3Enum: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": {bindTo: "P3EnumCaption"}
				},
				P3EnumCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				P3EnumVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"visible": false
				},
				P3EnumCollection: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"isCollection": true
				},
				//Справочная колонка для параметра P4
				P4Enum: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": {bindTo: "P4EnumCaption"}
				},
				P4EnumCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				P4EnumVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"visible": false
				},
				P4EnumCollection: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"isCollection": true
				},
				//Справочная колонка для параметра P5
				P5Enum: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": {bindTo: "P5EnumCaption"}
				},
				P5EnumCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				P5EnumVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"visible": false
				},
				P5EnumCollection: {
					"dataValueType": Terrasoft.DataValueType.ENUM,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"isCollection": true
				},
				//Атрибут для выбора расчета срока доставки
				DeliveryAtr: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": {bindTo: "DeliveryCaption"}
				},
				DeliveryCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": ""
				},
				DeliveryVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				},
				//Атрибут для текстового отображения "Срока доставки"
	/*			DeliveryTxtAtr: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"caption": {bindTo: "DeliveryTxtCaption"}
				},
				DeliveryTxtCaption: {
					"dataValueType": Terrasoft.DataValueType.TEXT,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					"value": "Срок доставки"
				},*/
				DeliveryTxtVisible: {
					"dataValueType": Terrasoft.DataValueType.BOOLEAN,
					"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
				}
			},
			
	//		Бизнес правила
			rules: {
				"ITRegion": {
					"FiltrationRegionWithCode": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": true,
						"autoClean": true,
						"baseAttributePatch": "ITCode",
						"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": 0
					}
				}
			},
			
			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
			
			methods: {
				//Для текущего значения Итого без НДС
				currentAmount: 0,
				
				//Для текущего значения поля Количество
				currentQuantity: 1,
				
				//Сюда передается массив объектов "service"
				activeServices: [],
				currentCount: "",
				currentMonth: "",
				currentSum: "",
				//Текущий массив объектов доп.услуги P1
				currentP1: "",
				//Текущий массив объектов доп.услуги P2
				currentP2: "",
				//Текущий массив объектов доп.услуги P3
				currentP3: "",
				//Текущий массив объектов доп.услуги P4
				currentP4: "",
				//Текущий массив объектов доп.услуги P5
				currentP5: "",
				
				//Метод, срабатывающий при инициализации страницы
				onEntityInitialized: function() {
					this.callParent(arguments);
					this.productCode = "";
					this.IsaviaCode = "";
					this.packCode = "";
					this.countryCode = 0;
					this.regionCode = 0;
					this.updateTarifficatorServer();
					this.onServicesChange();
					this.CountInValue = "";
					this.MonthValue = "";
					this.SumValue = "";
					this.p1Value = "";//для записи значения доп.параметра p1 или client
					this.p2Value = "";
					this.p3Value = "";
					this.p4Value = "";
					this.p5Value = "";
					this.p1ForMassive = "";//для записи члена массива
					this.currentDelivery = "";//Для текущего значения "Срока доставки"
				},
				//Для справочника-атрибутов ITP1 - ITP5
				onPageInitialized: function(callback, scope) {
					if (!this.get("CountInCollection")) {
						this.set("CountInCollection", this.Ext.create("Terrasoft.Collection"));
					}
					if (!this.get("MonthCollection")) {
						this.set("MonthCollection", this.Ext.create("Terrasoft.Collection"));
					}
					if (!this.get("SumCollection")) {
						this.set("SumCollection", this.Ext.create("Terrasoft.Collection"));
					}
					for (var i = 1; i < 6; i++) {
						var collect = "P" + i + "EnumCollection";
						if (!this.get(collect)) {
							this.set(collect, this.Ext.create("Terrasoft.Collection"));
						}
					}
					if (callback) {
						callback.call(scope || this);
					}
				},
				
				//Срабатывает при изменении значений атрибутов доп.услуг
				onServicesChange: function() {
					var leng = 14;
					for (var i = 0; i < leng; i++) {
						var atrName = "ITServiceAtr" + i;
						this.on("change:" + atrName, this.onServicesChangeFunction, this);
					}
				},
				
				//Метод обработчик onServicesChange
				onServicesChangeFunction: function(model) {
					var i, arr = [], leng;
					arr = this.activeServices;
					for (var y = 0; y < arr.length; y++) {
						var txt = "ITServiceAtr" + y;
						for (var key in model.changed) {
							if (model.changed[key]) {
								if (key === txt) {
									i = y;
								}
							} else {return; }
						}
					}
					if (arr) {
						leng = arr.length;
						if (arr[i].off && arr[i].off.length > 0) {
							for (var j = 0; j < arr[i].off.length; j++) {
								for (var z = 0; z < leng; z++) {
									if (arr[i].off[j] === arr[z].id) {
										var atrNameFalse = "ITServiceAtr" + z;
										this.set(atrNameFalse, false);
									}
								}
							}
						}
					}
				},
				
				//Метод для установки caption, visible атрибутов доп.услуг
				trololo: function(itemService) {
					var arr = [];
					arr = itemService;
					var leng = arr.length;
					for (var i = 0; i < leng; i++) {
						var capt = arr[i].caption;
						var atrName = "Serv" + i;
						this.set(atrName, capt);
						this.set(("SerVis" + i), true);
					}
					this.set("Group1Visible", true);
				},
				
				//Метод записывает id выбранных доп. услуг в поле "ITCodeProduct"
				setServices: function() {
					var arr = [], i, text = "";
					arr = this.activeServices;
					var leng = arr.length;
					for (i = 0; i < leng; i++) {
						var atrName = "ITServiceAtr" + i;
						if (this.get(atrName)) {
							if (i !== leng - 1) {
								text += arr[i].id + ",";
							} else {
								text += arr[i].id;
							}
						}
					}
					this.set("ITCodeProduct", text);
					this.calculateTarif();
				},
				
	//			Для заполнения справочника ITCountinPackEnum
				//Срабатывает при изменении значения в справочной колонке
				CountInChange: function(val) {
					if (val && val.displayValue) {
						//Записывает в переменную значение value выбранной строки справочника
						this.CountInValue = val.value;
					}
				},
				//Формирование списка справочника
				CountInList: function(filter, list) {
					var itemList = [], columns = {};
					itemList = this.currentCount;
					if (list === null) {
						return;
					}
					list.clear();
					for (var i = 0; i < itemList.length; i++) {
						var obj = "obj" + i;
						obj = {
							value: itemList[i].id,
							displayValue: itemList[i].name
						};
						columns[1 + i] = obj;
					}
					list.loadAll(columns);
				},
				
	//			Для заполнения справочника ITMonthEnum
				//Срабатывает при изменении значения в справочной колонке
				MonthChange: function(val) {
					if (val && val.displayValue) {
						//Записывает в переменную значение value выбранной строки справочника
						this.MonthValue = val.value;
					}
				},
				//Формирование списка справочника
				MonthList: function(filter, list) {
					if (list === null) {
						return;
					}
					var itemList = [], columns = {};
					itemList = this.currentMonth;
					list.clear();
					for (var i = 0; i < itemList.length; i++) {
						var obj = "obj" + i;
						obj = {
							value: itemList[i].id,
							displayValue: itemList[i].name
						};
						columns[1 + i] = obj;
					}
					list.loadAll(columns);
				},
				
	//			Для заполнения справочника ITSum
				//Срабатывает при изменении значения в справочной колонке
				SumChange: function(val) {
					if (val && val.displayValue) {
						//Записывает в переменную значение value выбранной строки справочника
						this.SumValue = val.value;
					}
				},
				//Формирование списка справочника
				SumList: function(filter, list) {
					if (list === null) {
						return;
					}
					var itemList = [], columns = {};
					itemList = this.currentSum;
					list.clear();
					for (var i = 0; i < itemList.length; i++) {
						var obj = "obj" + i;
						obj = {
							value: itemList[i].id,
							displayValue: itemList[i].name
						};
						columns[1 + i] = obj;
					}
					list.loadAll(columns);
				},
				
	//			Для заполнения справочником ITP1
				//Срабатывает при изменении значения в справочной колонке P1
				P1ValueChange: function(val1) {
					if (val1 && val1.displayValue) {
	//					this.console.log("you pick: ", val.displayValue);
						//Записывает в переменную value выбранной строки справочника
						this.p1Value = val1.value;
					}
				},
				
				P1prepareList: function(filter1, list1) {
					var itemList = [], columns = {};
					itemList = this.currentP1;
					if (list1 === null) {
						return;
					}
					list1.clear();
					for (var i = 0; i < itemList.length; i++) {
						var obj = "obj" + i;
						obj = {
							value: itemList[i].id,
							displayValue: itemList[i].name
						};
						columns[1 + i] = obj;
					}
					list1.loadAll(columns);
				},
				
	//			Для заполнения справочником ITP2
				//Срабатывает при изменении значения в справочной колонке P2
				P2ValueChange: function(val2) {
					if (val2 && val2.displayValue) {
						//Записывает в переменную value выбранной строки справочника
						this.p2Value = val2.value;
					}
				},
				
				P2prepareList: function(filter2, list2) {
					var itemList = [], columns = {};
					itemList = this.currentP2;
					if (list2 === null) {
						return;
					}
					list2.clear();
					for (var i = 0; i < itemList.length; i++) {
						var obj = "obj" + i;
						obj = {
							value: itemList[i].id,
							displayValue: itemList[i].name
						};
						columns[1 + i] = obj;
					}
					list2.loadAll(columns);
				},
				
	//			Для заполнения справочником ITP3
				//Срабатывает при изменении значения в справочной колонке P3
				P3ValueChange: function(val3) {
					if (val3 && val3.displayValue) {
						//Записывает в переменную value выбранной строки справочника
						this.p3Value = val3.value;
					}
				},
				
				P3prepareList: function(filter3, list3) {
					var itemList = [], columns = {};
					itemList = this.currentP3;
					if (list3 === null) {
						return;
					}
					list3.clear();
					for (var i = 0; i < itemList.length; i++) {
						var obj = "obj" + i;
						obj = {
							value: itemList[i].id,
							displayValue: itemList[i].name
						};
						columns[1 + i] = obj;
					}
					list3.loadAll(columns);
				},
				
	//			Для заполнения справочником ITP4
				//Срабатывает при изменении значения в справочной колонке P4
				P4ValueChange: function(val4) {
					if (val4 && val4.displayValue) {
						//Записывает в переменную value выбранной строки справочника
						this.p4Value = val4.value;
					}
				},
				
				P4prepareList: function(filter4, list4) {
					var itemList = [], columns = {};
					itemList = this.currentP4;
					if (list4 === null) {
						return;
					}
					list4.clear();
					for (var i = 0; i < itemList.length; i++) {
						var obj = "obj" + i;
						obj = {
							value: itemList[i].id,
							displayValue: itemList[i].name
						};
						columns[1 + i] = obj;
					}
					list4.loadAll(columns);
				},
				
	//			Для заполнения справочником ITP5
				//Срабатывает при изменении значения в справочной колонке P5
				P5ValueChange: function(val5) {
					if (val5 && val5.displayValue) {
						//Записывает в переменную value выбранной строки справочника
						this.p5Value = val5.value;
					}
				},
				
				P5prepareList: function(filter5, list5) {
					var itemList = [], columns = {};
					itemList = this.currentP5;
					if (list5 === null) {
						return;
					}
					list5.clear();
					for (var i = 0; i < itemList.length; i++) {
						var obj = "obj" + i;
						obj = {
							value: itemList[i].id,
							displayValue: itemList[i].name
						};
						columns[1 + i] = obj;
					}
					list5.loadAll(columns);
				},
				
	//			Методы для управления видимостью основных полей
				ToVisible: function() {
					var txt = this.get("ITAnswerFor");
					if (txt) {
						var arr = [];
						arr = txt.split(",");
						for (var i = 0; i < arr.length; i++) {
							if (arr[i] === "to") {
								return true;
							}
						}
					} else {
						return false;
					}
				},
				FromVisible: function() {
					var txt = this.get("ITAnswerFor");
					if (txt) {
						var arr = [];
						arr = txt.split(",");
						for (var i = 0; i < arr.length; i++) {
							if (arr[i] === "from") {
								return true;
							}
						}
					} else {
						return false;
					}
				},
				CountryVisible: function() {
					var txt = this.get("ITAnswerFor");
					if (txt) {
						var arr = [];
						arr = txt.split(",");
						for (var i = 0; i < arr.length; i++) {
							if (arr[i] === "country") {
								return true;
							}
						}
					} else {
						return false;
					}
				},
				RegionVisible: function() {
					var txt = this.get("ITAnswerFor");
					if (txt) {
						var arr = [];
						arr = txt.split(",");
						for (var i = 0; i < arr.length; i++) {
							if (arr[i] === "region") {
								return true;
							}
						}
					} else {
						return false;
					}
				},
				SumocVisible: function() {
					var txt = this.get("ITAnswerFor");
					if (txt) {
						var arr = [];
						arr = txt.split(",");
						for (var i = 0; i < arr.length; i++) {
							if (arr[i] === "sumoc") { //arr[i].Name === ""
								return true;
							}
						}
					} else {
						return false;
					}
				},
				SumnpVisible: function() {
					var txt = this.get("ITAnswerFor");
					if (txt) {
						var arr = [];
						arr = txt.split(",");
						for (var i = 0; i < arr.length; i++) {
							if (arr[i] === "sumnp") { //arr[i].Name === ""
								return true;
							}
						}
					} else {
						return false;
					}
				},
				SumgsVisible: function() {
					var txt = this.get("ITAnswerFor");
					if (txt) {
						var arr = [];
						arr = txt.split(",");
						for (var i = 0; i < arr.length; i++) {
							if (arr[i] === "sumgs") { //arr[i].Name === ""
								return true;
							}
						}
					} else {
						return false;
					}
				},
				IsaviaVisible: function() {
					var txt = this.get("ITAnswerFor");
					if (txt) {
						var arr = [];
						arr = txt.split(",");
						for (var i = 0; i < arr.length; i++) {
							if (arr[i] === "isavia") {
								return true;
							}
						}
					} else {
						return false;
					}
				},
				PackVisible: function() {
					var txt = this.get("ITAnswerFor");
					if (txt) {
						var arr = [];
						arr = txt.split(",");
						for (var i = 0; i < arr.length; i++) {
							if (arr[i] === "pack") {
								return true;
							}
						}
					} else {
						return false;
					}
				},
				
				//Метод берет код тарификации выбранного продукта из БД
				getProductCode: function() {
					var recordId = this.get("Product").Id;
					if (recordId === undefined) {
						recordId = this.get("Product").value;
					}
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Product"});
					esq.addColumn("ITTariffCode", "TariffCode");
					esq.getEntity(recordId, function(result) {
						if (result.success && result.entity.get("TariffCode")) {
							this.productCode = result.entity.get("TariffCode");
							this.set("PricePerUnitVisible", false);
							this.set("BtnVisible", true);
							this.parseMethod();
						} else {
							this.productCode = "";
							this.set("ITFrom", null);
							this.set("ITTo", null);
							this.set("ITCountry", null);
							this.set("ITRegion", null);
							this.set("ITWeight", null);
							this.set("ITSumoc", null);
							this.set("ITSumnp", null);
							this.set("ITSumgs", null);
							this.set("ITSum", null);
							this.set("ITMonth", null);
							this.set("ITPack", null);
							this.set("ITCount", null);
							this.set("ITCountinPackEnum", null);
							this.set("ITIsavia", null);
							this.set("ITCodeProduct", null);
							this.set("ITP1", null);
							this.set("ITP2", null);
							this.set("ITP3", null);
							this.set("ITP4", null);
							this.set("ITP5", null);
							this.set("ITAnswerFor", null);
							this.set("ITAmount", null);
							this.set("ITAmountNds", null);
							//Видимость колонок при нетарифицируемой услуге
							this.set("BtnVisible", false);
							this.set("PricePerUnitVisible", true);
	//						this.showInformationDialog("Выбранная услуга не рассчитывается по тарификатору ФГУП \"Почта России\"!");
						}
					}, this);
				},
				
				//Метод берет код указанной упаковки из БД
				getPackCode: function() {
					var recordId = this.get("ITPack").Id;
					if (recordId === undefined) {
						recordId = this.get("ITPack").value;
					}
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "ITCodeTypePack"});
					esq.addColumn("ITCode", "ITCode");
					esq.getEntity(recordId, function(result) {
						this.packCode = result.entity.get("ITCode");
					}, this);
				},
				
				//Метод берет код указанного направления доставки из БД
				getIsaviaCode: function() {
					var recordId = this.get("ITIsavia").Id, arr = [];
					arr = this.activeServices;
					if (recordId === undefined) {
						recordId = this.get("ITIsavia").value;
					}
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "ITIsAvia"});
					esq.addColumn("ITCode", "ITCode");
					esq.getEntity(recordId, function(result) {
						this.IsaviaCode = result.entity.get("ITCode");
						for (var i = 0; i < arr.length; i++) {
							if (arr[i].avia.indexOf(this.IsaviaCode) === -1) {
								this.set("ServEnabled" + i, false);
							}
						}
					}, this);
				},
				
				//Метод берет код указанной страны из БД
				getCountryCode: function() {
					var country = this.get("ITCountry");
					if (!country) {
						this.countryCode = 0;
						return;
					}
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Country"});
					esq.addColumn("ITCode", "ITCode");
					esq.getEntity(country.Id, function(result) {
						this.countryCode = result.entity.values.ITCode;
					}, this);
				},
				
				//Метод берет код указанного региона из БД
				getRegionCode: function() {
					var region = this.get("ITRegion");
					if (!region) {
						this.regionCode = 0;
						return;
					}
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Region"});
					esq.addColumn("ITCode", "ITCode");
					esq.getEntity(region.Id, function(result) {
						this.regionCode = result.entity.values.ITCode;
					}, this);
				},
				
				//Метод отправляет выбранные данные на сервер и получает итоговый расчет по выбранной услуге
				calculateTarif: function() {
					var requestJson;
					var deliveryTxt = "";
					var finalSum = 0;
					var finalSumNds = 0;
					var request = new XMLHttpRequest();
					var massive;
					var servicesSplit = this.get("ITCodeProduct");
					var requestText = this.tarifficatorServer + "/v1/calculate?json&errorcode=1";
					var dateString;
					var RegEx = /\s/g;
					
					//В данной версии не используется
	/*				function clearDisplay(_this, name) {
						try {
							return _this.get(name).displayValue;
						}
						catch (e) {
							return "";
						}
					}*/
					function clearString(_this, name) {
						try {
							return _this.get(name).toString();
						}
						catch (e) {
							return "";
						}
					}
					function sumClearString(_this, name) {
						try {
							var val = 100 * parseFloat(_this.get(name).toString());
							return val.toString();
						}
						catch (e) {
							return "";
						}
					}
					
					//Для получения значения параметра 
					function getMonthValue(_this, name) {
						try {
							var val = _this.get(name);
							return val.toString();
						}
						catch (e) {
							return _this.MonthValue;
						}
					}
					function getCountInValue(_this, name) {
						try {
							var val = _this.get(name);
							return val.toString();
						}
						catch (e) {
							return _this.CountInValue;
						}
					}
					function getSumValue(_this, name) {
						try {
							var val = _this.get(name);
							return val.toString();
						}
						catch (e) {
							return _this.SumValue;
						}
					}
					massive = [
						[
							this.productCode,
							"",
							"",
							"",
							this.get("OfferDate"),
							clearString(this, "ITFrom"),
							clearString(this, "ITTo"),
							this.countryCode,
							this.regionCode,
							clearString(this, "ITWeight"),
							sumClearString(this, "ITSumoc"),
							sumClearString(this, "ITSumnp"),
							sumClearString(this, "ITSumgs"),
							getSumValue(this, "ITSum"),//&sum=
							getMonthValue(this, "ITMonth"),//&month=
							clearString(this, "ITCount"),//&count=
							this.packCode,//packFunc(this, "ITPack"),
							getCountInValue(this, "ITCountinPack"),//&countinpack=
							this.IsaviaCode,
							servicesSplit,//&service=
							this.p1Value,//&p1=
							this.p2Value,//&p2=
							this.p3Value,//&p3=
							this.p4Value,//&p4=
							this.p5Value//&p5=
						],
						[
							"&object=", "&typ=", "&cat=", "&dir=", "&date=", "&from=", "&to=", "&country=",
							"&region=", "&weight=", "&sumoc=", "&sumnp=",
							"&sumgs=", "&sum=", "&month=", "&count=", "&pack=", "&countinpack=", "&isavia=",
							"&service=", this.p1ForMassive, "&p2=", "&p3=", "&p4=", "&p5="
						],
						[
							0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0
						]
					];
					for (var i = 0; i < 25; i++) {
						if (massive[2][i] === 0) {
							if (massive[0][i] !== "") {
								requestText += massive[1][i] + massive[0][i];
							}
						} else {
							if (massive[2][i] === 1) {
								if (massive[0][i] !== "") {
									dateString = new Date(massive[0][i]);
									var month = dateString.getMonth() + 1;
									month += "";
									var day = "" + dateString.getDate();
									if (month.length === 1) {
										month = "0" + month;
									}
									if (day.length === 1) {
										day = "0" + day;
										
									}
									requestText += massive[1][i] + dateString.getFullYear() + month + day;
								}
							} else {
								if (massive[2][i] === 2 && massive[0][i] !== "") {
									requestText += massive[1][i] + massive[0][i];
								}
							}
						}
					}
					
					//Если есть параметр "Срок доставки", то он будет добавлен в GET запрос
					if (this.currentDelivery !== "") {
						requestText += "&delivery=" + this.currentDelivery;
					}
					//Формируется запрос и получаем Итоговый расчет по Услугам
					request.open("GET", (requestText).replace(RegEx, ""), false);
					request.send();
					var countSum = this.get("ITQuantity");
					if (request.status === 200) {
						requestJson = JSON.parse(request.responseText);
						if (requestJson.delivery) {
							if (this.get("DeliveryAtr")) {
								deliveryTxt = "от " + requestJson.delivery.min + " до " + requestJson.delivery.max + " дней";
								this.set("ITEstimatedDeliveryTime", deliveryTxt);
								this.set("DeliveryTxtVisible", true);
							} else if (!this.get("DeliveryAtr")) {
								this.set("ITEstimatedDeliveryTime", "");
								this.set("DeliveryTxtVisible", false);
							}
						}
						finalSum += (requestJson.pay) / 100;
						finalSumNds += (requestJson.paynds) / 100;
						var countFinalSum = finalSum * countSum;
						var countFinalSumNds = finalSumNds * countSum;
						this.set("ITAmount", countFinalSum ? countFinalSum : 0);
						this.set("ITAmountNds", countFinalSumNds ? countFinalSumNds : 0);
					} else {
						var errorText = "";
						try {
							requestJson = JSON.parse(request.responseText);
							errorText = requestJson.error;
						}
						catch (e) {}
						this.showInformationDialog("Ошибка тарификации. " + errorText);
					}
				},
				
				//Метод, запускающий другие методы при изменении определенных колонок
				entityColumnChanged: function(columnName, columnValue) {
					if (!columnValue) {
						return;
					}
					if (columnName === "Product") {
						this.onProductChange();
					} else if (columnName === "ITIsavia") {
						this.getIsaviaCode();
					} else if (columnName === "ITPack") {
						this.getPackCode();
					} else if (columnName === "ITCountry") {
						this.getCountryCode();
					} else if (columnName === "ITRegion") {
						this.getRegionCode();
					} else if (columnName === "ITPricePerUnit") {
						this.countPerUnit();
					} else if (columnName === "ITAmount" && !this.Ext.isEmpty(this.get("Product")) &&
					this.productCode === "") {
						this.countAmounts();
					} else if (columnName === "ITQuantity" && !this.Ext.isEmpty(this.get("Product")) &&
					this.productCode === "" && columnValue < 1) {
						this.showInformationDialog("Количество не может быть меньше 1");
					} else if (columnName === "ITQuantity" && !this.Ext.isEmpty(this.get("Product")) &&
					this.productCode === "") {
						this.countAmounts();
					}
				},
				
				onProductChange: function() {
					//Обнуление справочных полей атрибутов
					this.set("MonthEnum", "");
					this.set("SumEnum", "");
					this.set("CountInEnum", "");
					this.set("P1Enum", "");
					this.set("P2Enum", "");
					this.set("P3Enum", "");
					this.set("P4Enum", "");
					this.set("P4Enum", "");
					
					//Атрибуты "Срок доставки"
					this.set("DeliveryCaption", "");
					this.set("DeliveryVisible", false);
					this.set("DeliveryTxtVisible", false);
					this.set("ITEstimatedDeliveryTime", "");
					this.set("DeliveryAtr", false);
					this.set("ITEstimatedDeliveryTime", "");
					
					//Обнуление переменных, содержащих массивы записей справочников
					this.currentSum = "";
					this.currentCount = "";
					this.currentMonth = "";
					this.currentP1 = "";
					this.currentP2 = "";
					this.currentP3 = "";
					this.currentP4 = "";
					this.currentP5 = "";
					
					//Обнуление значения переменных для массива [0]
					this.CountInValue = "";
					this.MonthValue = "";
					this.SumValue = "";
					
					//Обнуление значения переменных p1Value - p5Value
					this.p1Value = "";
					this.p2Value = "";
					this.p3Value = "";
					this.p4Value = "";
					this.p5Value = "";
					
					//Обнуление значения переменной p1ForMassive
					this.p1ForMassive = "";
					//Обнуление текущего значения "Срока доставки"
					this.currentDelivery = "";
					
					this.set("Group1Visible", false);
					this.set("Group2Visible", false);
					this.set("ITPricePerUnit", 0);
					this.set("ITQuantity", 1);
					
					//Сбрасываем видимость колонок ITCount, ITCountInAtr, ITCountinPackEnum, ITWeight и их заголовки
					this.set("CountCaption", "");
					this.set("CountVisible", false);
					this.set("CountInPackCaption", "");
					this.set("CountInPackVisible", false);
					this.set("CountInCaption", "");
					this.set("CountInVisible", false);
					this.set("MonthCaption", "");
					this.set("MonthVisible", false);
					this.set("MonthEnumCaption", "");
					this.set("MonthEnumVisible", false);
					this.set("SumCaption", "");
					this.set("SumVisible", false);
					this.set("SumEnumCaption", "");
					this.set("SumEnumVisible", false);
					this.set("WeightCaption", "");
					this.set("WeightVisible", false);
					this.packCode = "";
					this.IsaviaCode = "";
					this.getProductCode();
				},
				
				//Метод для расчета полей Итого при изменении поля "Цена за единицу"
				countPerUnit: function() {
					var priceUnit = this.get("ITPricePerUnit"), count = this.get("ITQuantity"), nds = 1.18, price = 0, priceNds = 0;
					price = priceUnit * count;
					priceNds = priceUnit * count * nds;
					this.currentAmount = priceUnit;
					this.set("ITAmount", price);
					this.set("ITAmountNds", priceNds);
				},
				
				//Метод подсчитывает колонку Итого С НДС для услуг вне тарифа
				countAmounts: function() {
					var count = this.get("ITQuantity"), amount = this.get("ITAmount"), price = 0, priceNds = 0, nds = 1.18;
					if (count === this.currentQuantity && (amount / count) !== this.currentAmount) {
						this.currentAmount = amount / count;
						price = amount;
						priceNds = amount * nds;
						this.set("ITPricePerUnit", amount / count);
						this.set("ITAmount", price);
						this.set("ITAmountNds", priceNds);
					} else if (count !== this.currentQuantity) {
						price = this.currentAmount * count;
						priceNds = this.currentAmount * count * nds;
						this.set("ITAmount", price);
						this.set("ITAmountNds", priceNds);
						this.currentQuantity = count;
					}
				},
				
				//Адрес сервера тарификации по умолчанию
				tarifficatorServer: "http://tariff.russianpost.ru/tariff",
				
				//Адрес сервеса тарификации, устанавливаемый из системной настройки
				updateTarifficatorServer: function() {
					this.Terrasoft.SysSettings.querySysSettingsItem("TarifficatorServer",
						function(value) {
							this.tarifficatorServer = value;
						},
					this);
				},
				
				//Метод срабатывает при выборе услуги и получает параметры и доп.услуги выбранной услуги
				parseMethod: function() {
					var type = this.get("Product");
					//обнуляем значения всех колонок
					if (type) {
						this.set("ITFrom", null);
						this.set("ITTo", null);
						this.set("ITCountry", null);
						this.set("ITRegion", null);
						this.set("ITWeight", null);
						this.set("ITSumoc", null);
						this.set("ITSumnp", null);
						this.set("ITSumgs", null);
						this.set("ITSum", null);
						this.set("ITMonth", null);
						this.set("ITPack", null);
						this.set("ITCount", null);
						this.set("ITCountinPackEnum", null);
						this.set("ITIsavia", null);
						this.set("ITCodeProduct", null);
						this.set("P1Enum", null);
						this.set("P2Enum", null);
						this.set("P3Enum", null);
						this.set("P4Enum", null);
						this.set("P5Enum", null);
						this.set("ITAnswerFor", null);
						this.set("ITAmount", null);
						this.set("ITAmountNds", null);
						
						//Сбрасываем значения всех атрибутов доп.услуг (caption, visible, value)
						for (var i = 0; i < 14; i++) {		//14 здесь - это количество атрибутов доп. услуг
							this.set(("ITServiceAtr" + i), false);
							this.set(("SerVis" + i), false);
							this.set("Serv" + i, "");
						}
						
						//Сбрасываем видимость атрибутов Дополнительных параметров и их заголовки
						for (i = 1; i < 6; i++) {
							var capt = "P" + i + "EnumCaption", vis = "P" + i + "EnumVisible";
							this.set(capt, "");
							this.set(vis, false);
						}
						
						//Сбрасываем видимость колонок ITCount, ITCountinPackEnum, ITWeight и их заголовки
	/*					this.set("CountCaption", "");
						this.set("CountVisible", false);
						this.set("CountInCaption", "");
						this.set("CountInVisible", false);
						this.set("WeightCaption", "");
						this.set("WeightVisible", false);*/
						
						//Непосредственно отправка запроса на получение инфы по объекту тарификации
						var firstRequest = new XMLHttpRequest();
						var parseText = this.tarifficatorServer + "/v1/dictionary?json&object=";
						parseText += this.productCode;
						firstRequest.open("GET", parseText, false);
						firstRequest.send();
						if (firstRequest.status === 200) {
		//				this.showInformationDialog("Все ок! " + firstRequest.statusText);
							var txt = firstRequest.responseText;
							var obj = JSON.parse(txt);
							var itemParam = [], itemService = [], itemP1 = [], itemP2 = [], itemP3 = [],
							itemP4 = [], itemP5 = [], itemCountIn = [], itemMonth = [], itemSum = [];
							try {
								//Записываем все параметры param по данной услуге
								obj.object[0].params.forEach(function(item, i, arr) {
									itemParam.push(item.param);
									//Если есть допюпараметры, то мы набиваем атрибут P1Enum их значениями
									if (item.param === "client") {
										this.set("P1EnumCaption", item.name);
										item.list.forEach(function(item1, z, arr1) {
											itemP1.push({id: item1.id, name: item1.name});
										});
										this.currentP1 = itemP1;
										this.p1ForMassive = "&client=";
										this.set("Group2Visible", true);
										this.set("P1EnumVisible", true);
									} else if (item.param === "p1") {
										this.set("P1EnumCaption", item.name);
										item.list.forEach(function(item1, z, arr1) {
											itemP1.push({id: item1.id, name: item1.name});
										});
										this.currentP1 = itemP1;
										this.p1ForMassive = "&p1=";
										this.set("Group2Visible", true);
										this.set("P1EnumVisible", true);
									} else if (item.param === "p2") {
										this.set("P2EnumCaption", item.name);
										item.list.forEach(function(item1, z, arr1) {
											itemP2.push({id: item1.id, name: item1.name});
										});
										this.currentP2 = itemP2;
										this.set("Group2Visible", true);
										this.set("P2EnumVisible", true);
									} else if (item.param === "p3") {
										this.set("P3EnumCaption", item.name);
										item.list.forEach(function(item1, z, arr1) {
											itemP3.push({id: item1.id, name: item1.name});
										});
										this.currentP3 = itemP3;
										this.set("Group2Visible", true);
										this.set("P3EnumVisible", true);
									} else if (item.param === "p4") {
										this.set("P4EnumCaption", item.name);
										item.list.forEach(function(item1, z, arr1) {
											itemP4.push({id: item1.id, name: item1.name});
										});
										this.currentP4 = itemP4;
										this.set("Group2Visible", true);
										this.set("P4EnumVisible", true);
									} else if (item.param === "p5") {
										this.set("P5EnumCaption", item.name);
										item.list.forEach(function(item1, z, arr1) {
											itemP5.push({id: item1.id, name: item1.name});
										});
										this.currentP5 = itemP5;
										this.set("Group2Visible", true);
										this.set("P5EnumVisible", true);
									} else if (item.param === "month" && !item.list) {
										this.set("MonthCaption", item.name);
										this.set("MonthVisible", true);
									} else if (item.param === "month" && item.list) {
										this.set("MonthEnumCaption", item.name);
										this.set("MonthEnumVisible", true);
										item.list.forEach(function(item1, z, arr1) {
											itemMonth.push({id: item1.id, name: item1.name});
										});
										this.currentMonth = itemMonth;
									} else if (item.param === "sum" && !item.list) {
										this.set("SumCaption", item.name);
										this.set("SumVisible", true);
									} else if (item.param === "sum" && item.list) {
										this.set("SumEnumCaption", item.name);
										this.set("SumEnumVisible", true);
										item.list.forEach(function(item1, z, arr1) {
											itemSum.push({id: item1.id, name: item1.name});
										});
										this.currentSum = itemSum;
									} else if (item.param === "count") {
										this.set("CountCaption", item.name);
										this.set("CountVisible", true);
									} else if (item.param === "countinpack" && !item.list) {
										this.set("CountInPackCaption", item.name);
										this.set("CountInPackVisible", true);
									} else if (item.param === "countinpack" && item.list) {
										this.set("CountInCaption", item.name);
										this.set("CountInVisible", true);
										item.list.forEach(function(item1, z, arr1) {
											itemCountIn.push({id: item1.id, name: item1.name});
										});
										this.currentCount = itemCountIn;
									} else if (item.param === "weight") {
										this.set("WeightCaption", item.name);
										this.set("WeightVisible", true);
									} else if (item.param === "delivery") {
										this.currentDelivery = "1";
										this.set("DeliveryCaption", item.name);
										this.set("DeliveryVisible", true);
									}
								}, this);
								this.set("ITAnswerFor", itemParam.toString());
								
								//Для проверки на наличие доп.услуг и запуска метода отображения полей доп.услуг
								if (obj.object[0].service) {
									obj.object[0].service.forEach(function(item, i, arr) {
										itemService.push({id: item.id, caption: item.name, avia: item.isavia, off: item.serviceoff});
									}, this);
									this.activeServices = itemService;
									this.trololo(itemService);
								}
							}
							catch (e) {}
						} else {
							this.set("ITAnswerFor", "");
							this.showInformationDialog("Ошибка ответа от тарификатора - " + firstRequest.status + ": " +
							firstRequest.statusText);
						}
					} else {
						this.set("ITAnswerFor", "");
		//				this.showInformationDialog("Поле \"Услуги\" не заполнено!");
					}
				}
			},
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "remove",
					"name": "OpportunityProductPageGeneralTabContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.CONTAINER,
						"items": []
					}
				},
	//			Кнопка расчета тарифа
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "RunTarification",
					"values": {
						"visible": {bindTo: "BtnVisible"},
						"style": Terrasoft.controls.ButtonEnums.style.GREEN,
						itemType: Terrasoft.ViewItemType.BUTTON,
						caption: {bindTo: "Resources.Strings.BtnCalculate"},
						click: {bindTo: "setServices"},
						"layout": {"column": 10, "row": 18, "colSpan": 2, "layoutName": "Header"}
					}
				},
				
				{
					"operation": "remove",
					"name": "TsSenderIndex"
				},
				{
					"operation": "remove",
					"name": "TsPointOfDeparture"
				},
				{
					"operation": "remove",
					"name": "TsRecipientIndex"
				},
				{
					"operation": "remove",
					"name": "TsPointOfDestination"
				},
				{
					"operation": "remove",
					"name": "TsWeight"
				},
				{
					"operation": "remove",
					"name": "TsDeliveryDate"
				},
				{
					"operation": "remove",
					"name": "OfferResult"
				},
				{
					"operation": "remove",
					"name": "Comment"
				},
				{
					"operation": "remove",
					"name": "Price"
				},
				{
					"operation": "remove",
					"name": "Quantity"
				},
				{
					"operation": "remove",
					"name": "Amount"
				},
				{
					"operation": "move",
					"name": "Opportunity",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"readonly": true,
						"bindTo": "Opportunity",
						"layout": {"column": 0, "row": 0, "colSpan": 12},
						"enabled": false
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITPricePerUnit",
					"values": {
						"caption": "Цена за единицу",
						"visible": {bindTo: "PricePerUnitVisible"},
						"bindTo": "ITPricePerUnit",
						"layout": {"column": 12, "row": 0, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "Product",
					"values": {
						"bindTo": "Product",
						"layout": {"column": 0, "row": 1, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"name": "ITQuantity",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"caption": "Количество",
						"visible": true,
						"bindTo": "ITQuantity",
						"layout": {"column": 12, "row": 1, "colSpan": 12}
					}
				},
				{
					"operation": "move",
					"name": "OfferDate",
					"parentName": "Header",
					"propertyName": "items",
					"values": {
						"layout": {"column": 0, "row": 2, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITWeight",
					"values": {
						"caption": {bindTo: "WeightCaption"},
						"visible": {bindTo: "WeightVisible"},
						"bindTo": "ITWeight",
						"layout": {"column": 12, "row": 2, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITFrom",
					"values": {
						"bindTo": "ITFrom",
						"visible": {bindTo: "FromVisible"},
						"layout": {"column": 0, "row": 3, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITTo",
					"values": {
						"bindTo": "ITTo",
						"visible": {bindTo: "ToVisible"},
						"layout": {"column": 12, "row": 3, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITCountry",
					"values": {
						"bindTo": "ITCountry",
						"visible": {bindTo: "CountryVisible"},
						"layout": {"column": 0, "row": 4, "colSpan": 12},
						"tip": {
							// Текст подсказки.
							"content": { "bindTo": "Resources.Strings.TipCountry" },
							"displayMode": Terrasoft.controls.TipEnums.displayMode.WIDE
						}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITRegion",
					"values": {
						"bindTo": "ITRegion",
						"visible": {bindTo: "RegionVisible"},
						"layout": {"column": 12, "row": 4, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITIsavia",
					"values": {
						"bindTo": "ITIsavia",
						"visible": {bindTo: "IsaviaVisible"},
						"layout": {"column": 0, "row": 5, "colSpan": 12}
						
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITSumoc",
					"values": {
						"bindTo": "ITSumoc",
						"visible": {bindTo: "SumocVisible"},
						"layout": {"column": 12, "row": 5, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITSumnp",
					"values": {
						"visible" : {bindTo: "SumnpVisible"},
						"bindTo": "ITSumnp",
						"layout": {"column": 0, "row": 6, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITSumgs",
					"values": {
						"visible": {bindTo: "SumgsVisible"},
						"bindTo": "ITSumgs",
						"layout": {"column": 12, "row": 6, "colSpan": 12}
					}
				},
				//sum обычное поле
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITSum",
					"values": {
						"caption": {bindTo: "SumCaption"},
						"visible": {bindTo: "SumVisible"},
						"bindTo": "ITSum",
						"layout": {"column": 0, "row": 7, "colSpan": 12}
					}
				},
				//sum справочное поле
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITSumEnum",
					"values": {
						"layout": {"column": 12, "row": 7, "colSpan": 12, "rowSpan": 1},
						"controlConfig": {
							"className": "Terrasoft.ComboBoxEdit",
							"list": {
								"bindTo": "SumCollection"
							},
							"change": {
								"bindTo": "SumChange"
							},
							"prepareList": {
								"bindTo": "SumList"
							}
						},
						"labelConfig": {},
						"visible" : {bindTo: "SumEnumVisible"},
						"enabled": true,
						"bindTo": "SumEnum"
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITMonthEnum",
					"values": {
						"layout": {"column": 0, "row": 8, "colSpan": 12, "rowSpan": 1},
						"controlConfig": {
							"className": "Terrasoft.ComboBoxEdit",
							"list": {
								"bindTo": "MonthCollection"
							},
							"change": {
								"bindTo": "MonthChange"
							},
							"prepareList": {
								"bindTo": "MonthList"
							}
						},
						"labelConfig": {},
						"visible" : {bindTo: "MonthEnumVisible"},
						"enabled": true,
						"bindTo": "MonthEnum"
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITMonth",
					"values": {
						"caption": {bindTo: "MonthCaption"},
						"visible": {bindTo: "MonthVisible"},
						"bindTo": "ITMonth",
						"layout": {"column": 12, "row": 8, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITPack",
					"values": {
						"bindTo": "ITPack",
						"visible": {bindTo: "PackVisible"},
						"layout": {"column": 0, "row": 9, "colSpan": 12}
						
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITCount",
					"values": {
						"caption": {bindTo: "CountCaption"},
						"visible": {bindTo: "CountVisible"},
						"bindTo": "ITCount",
						"layout": {"column": 12, "row": 9, "colSpan": 12}
					}
				},
				//countinpack справочник
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITCountinPackEnum",
					"values": {
						"layout": {"column": 0, "row": 10, "colSpan": 12, "rowSpan": 1},
						"controlConfig": {
							"className": "Terrasoft.ComboBoxEdit",
							"list": {
								"bindTo": "CountInCollection"
							},
							"change": {
								"bindTo": "CountInChange"
							},
							"prepareList": {
								"bindTo": "CountInList"
							}
						},
						"labelConfig": {},
						"visible" : {bindTo: "CountInVisible"},
						"enabled": true,
						"bindTo": "CountInEnum"
					}
				},
				//countinpack обычное поле
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITCountinPack",
					"values": {
						"caption": {bindTo: "CountInPackCaption"},
						"visible": {bindTo: "CountInPackVisible"},
						"bindTo": "ITCountinPack",
						"layout": {"column": 12, "row": 10, "colSpan": 12, "rowSpan": 1}
					}
				},
				//Текстовое поле со сроком доставки
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITDeliveryTxt",
					"values": {
						"bindTo": "ITEstimatedDeliveryTime",
						"layout": {"column": 0, "row": 29, "colSpan": 12},
						"visible": {bindTo: "DeliveryTxtVisible"},
						"enabled": false
					}
				},
				//Поле отображает Итого без НДС
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITAmount",
					"values": {
						"bindTo": "ITAmount",
						"layout": {"column": 0, "row": 30, "colSpan": 12}
					}
				},
				//Поле отображает Итого с НДС
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITAmountNds",
					"values": {
						"caption": "Итого сумма с НДС (18%)",
						"enabled": false,
						"bindTo": "ITAmountNds",
						"layout": {"column": 0, "row": 31, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITAnswerFor",
					"values": {
						"bindTo": "ITAnswerFor",
						"layout": {"column": 0, "row": 40, "colSpan": 12},
						"visible" : "false"
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITCodeProduct",
					"values": {
						"bindTo": "ITCodeProduct",
						"layout": {"column": 24, "row": 41, "colSpan": 0},
						"visible" : "false"
					}
				},
				
				//Атрибут выбора расчета "Срок доставки"
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITDelivery",
					"values": {
						"bindTo": "DeliveryAtr",
						"layout": {"column": 0, "row": 11, "colSpan": 12},
						"visible" : {bindTo: "DeliveryVisible"}
					}
				},
				
//				Группа полей "Дополнительные услуги"
				//Группа контроллов
				{
					"operation": "insert",
					"name": "ServicesGroup",
					"values": {
						"layout": {"column": 0, "row": 15, "colSpan": 24},
						"caption": "Дополнительные услуги",//{"bindTo": "Resources.Strings.ServicesGroupCaption"},
						"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
						"markerValue": "added-group",
						"items": [],
						"visible" : {bindTo: "Group1Visible"}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 0
				},
				//Непосредственно контейнер для полей
				{
					"operation": "insert",
					"name": "ServicesGroupContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
						"markerValue": "added-group",
						"items": []
					},
					"parentName": "ServicesGroup",
					"propertyName": "items",
					"index": 0
				},
				//Непосредственно поля Группы полей
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService0",
					"values": {
						"enabled": {bindTo: "ServEnabled0"},
						"bindTo": "ITServiceAtr0",
						"layout": {"column": 0, "row": 0, "colSpan": 12},
						"visible" : {bindTo: "SerVis0"}
					},
					"index": 0
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService1",
					"values": {
						"enabled": {bindTo: "ServEnabled1"},
						"bindTo": "ITServiceAtr1",
						"layout": {"column": 12, "row": 0, "colSpan": 12},
						"visible" : {bindTo: "SerVis1"}
					},
					"index": 1
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService2",
					"values": {
						"enabled": {bindTo: "ServEnabled2"},
						"bindTo": "ITServiceAtr2",
						"layout": {"column": 0, "row": 1, "colSpan": 12},
						"visible" : {bindTo: "SerVis2"}
					},
					"index": 2
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService3",
					"values": {
						"enabled": {bindTo: "ServEnabled3"},
						"bindTo": "ITServiceAtr3",
						"layout": {"column": 12, "row": 1, "colSpan": 12},
						"visible" : {bindTo: "SerVis3"}
					},
					"index": 3
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService4",
					"values": {
						"enabled": {bindTo: "ServEnabled4"},
						"bindTo": "ITServiceAtr4",
						"layout": {"column": 0, "row": 2, "colSpan": 12},
						"visible" : {bindTo: "SerVis4"}
					},
					"index": 4
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService5",
					"values": {
						"enabled": {bindTo: "ServEnabled5"},
						"bindTo": "ITServiceAtr5",
						"layout": {"column": 12, "row": 2, "colSpan": 12},
						"visible" : {bindTo: "SerVis5"}
					},
					"index": 5
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService6",
					"values": {
						"enabled": {bindTo: "ServEnabled6"},
						"bindTo": "ITServiceAtr6",
						"layout": {"column": 0, "row": 3, "colSpan": 12},
						"visible" : {bindTo: "SerVis6"}
					},
					"index": 6
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService7",
					"values": {
						"enabled": {bindTo: "ServEnabled7"},
						"bindTo": "ITServiceAtr7",
						"layout": {"column": 12, "row": 3, "colSpan": 12},
						"visible" : {bindTo: "SerVis7"}
					},
					"index": 7
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService8",
					"values": {
						"enabled": {bindTo: "ServEnabled8"},
						"bindTo": "ITServiceAtr8",
						"layout": {"column": 0, "row": 4, "colSpan": 12},
						"visible" : {bindTo: "SerVis8"}
					},
					"index": 8
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService9",
					"values": {
						"enabled": {bindTo: "ServEnabled9"},
						"bindTo": "ITServiceAtr9",
						"layout": {"column": 12, "row": 4, "colSpan": 12},
						"visible" : {bindTo: "SerVis9"}
					},
					"index": 9
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService10",
					"values": {
						"enabled": {bindTo: "ServEnabled10"},
						"bindTo": "ITServiceAtr10",
						"layout": {"column": 0, "row": 5, "colSpan": 12},
						"visible" : {bindTo: "SerVis10"}
					},
					"index": 10
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService11",
					"values": {
						"enabled": {bindTo: "ServEnabled11"},
						"bindTo": "ITServiceAtr11",
						"layout": {"column": 12, "row": 5, "colSpan": 12},
						"visible" : {bindTo: "SerVis11"}
					},
					"index": 11
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService12",
					"values": {
						"enabled": {bindTo: "ServEnabled12"},
						"bindTo": "ITServiceAtr12",
						"layout": {"column": 0, "row": 6, "colSpan": 12},
						"visible" : {bindTo: "SerVis12"}
					},
					"index": 12
				},
				{
					"operation": "insert",
					"parentName": "ServicesGroupContainer",
					"propertyName": "items",
					"name": "ITService13",
					"values": {
						"enabled": {bindTo: "ServEnabled14"},
						"bindTo": "ITServiceAtr13",
						"layout": {"column": 12, "row": 6, "colSpan": 12},
						"visible" : {bindTo: "SerVis13"}
					},
					"index": 13
				},
				
//				Группа полей "Дополнительные параметры"
				//Группа контроллов
				{
					"operation": "insert",
					"name": "ExtraParams",
					"values": {
						"layout": {"column": 0, "row": 14, "colSpan": 24},
						"caption": "Дополнительные параметры",//{"bindTo": "Resources.Strings.ExtraParamsCaption"},
						"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
						"markerValue": "added-group",
						"items": [],
						"visible" : {bindTo: "Group2Visible"}
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 0
				},
				//Непосредственно контейнер для полей
				{
					"operation": "insert",
					"name": "ExtraParamsContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
						"markerValue": "added-group",
						"items": []
					},
					"parentName": "ExtraParams",
					"propertyName": "items",
					"index": 0
				},
				//Непосредственно поля Группы полей
				{
					"operation": "insert",
					"parentName": "ExtraParamsContainer",
					"propertyName": "items",
					"name": "ITP1",
					"values": {
						"layout": {"column": 0, "row": 0, "colSpan": 12, "rowSpan": 1},
						"controlConfig": {
							"className": "Terrasoft.ComboBoxEdit",
							"list": {
								"bindTo": "P1EnumCollection"
							},
							"change": {
								"bindTo": "P1ValueChange"
							},
							"prepareList": {
								"bindTo": "P1prepareList"
							}
						},
						"labelConfig": {},
						"visible" : {bindTo: "P1EnumVisible"},
						"enabled": true,
						"bindTo": "P1Enum"
					}
				},
				{
					"operation": "insert",
					"parentName": "ExtraParamsContainer",
					"propertyName": "items",
					"name": "ITP2",
					"values": {
						"layout": {"column": 12, "row": 0, "colSpan": 12, "rowSpan": 1},
						"controlConfig": {
							"className": "Terrasoft.ComboBoxEdit",
							"list": {
								"bindTo": "P2EnumCollection"
							},
							"change": {
								"bindTo": "P2ValueChange"
							},
							"prepareList": {
								"bindTo": "P2prepareList"
							}
						},
						"labelConfig": {},
						"visible" : {bindTo: "P2EnumVisible"},
						"enabled": true,
						"bindTo": "P2Enum"
					}
				},
				{
					"operation": "insert",
					"parentName": "ExtraParamsContainer",
					"propertyName": "items",
					"name": "ITP3",
					"values": {
						"layout": {"column": 0, "row": 1, "colSpan": 12, "rowSpan": 1},
						"controlConfig": {
							"className": "Terrasoft.ComboBoxEdit",
							"list": {
								"bindTo": "P3EnumCollection"
							},
							"change": {
								"bindTo": "P3ValueChange"
							},
							"prepareList": {
								"bindTo": "P3prepareList"
							}
						},
						"labelConfig": {},
						"visible" : {bindTo: "P3EnumVisible"},
						"enabled": true,
						"bindTo": "P3Enum"
					}
				},
				{
					"operation": "insert",
					"parentName": "ExtraParamsContainer",
					"propertyName": "items",
					"name": "ITP4",
					"values": {
						"layout": {"column": 12, "row": 1, "colSpan": 12, "rowSpan": 1},
						"controlConfig": {
							"className": "Terrasoft.ComboBoxEdit",
							"list": {
								"bindTo": "P4EnumCollection"
							},
							"change": {
								"bindTo": "P4ValueChange"
							},
							"prepareList": {
								"bindTo": "P4prepareList"
							}
						},
						"labelConfig": {},
						"visible" : {bindTo: "P4EnumVisible"},
						"enabled": true,
						"bindTo": "P4Enum"
					}
				},
				{
					"operation": "insert",
					"parentName": "ExtraParamsContainer",
					"propertyName": "items",
					"name": "ITP5",
					"values": {
						"layout": {"column": 0, "row": 2, "colSpan": 12, "rowSpan": 1},
						"controlConfig": {
							"className": "Terrasoft.ComboBoxEdit",
							"list": {
								"bindTo": "P5EnumCollection"
							},
							"change": {
								"bindTo": "P5ValueChange"
							},
							"prepareList": {
								"bindTo": "P5prepareList"
							}
						},
						"labelConfig": {},
						"visible" : {bindTo: "P5EnumVisible"},
						"enabled": true,
						"bindTo": "P5Enum"
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});