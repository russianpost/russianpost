define("AccountProfileSchema", ["AccountProfileSchemaResources"],
		function(resources) {
			return {
				entitySchemaName: "Account",

				methods: {},

				diff: /**SCHEMA_DIFF*/[

					{
						"operation": "merge",
						"name": "Web",
						"values": {
							"caption": resources.localizableStrings.WebCaption
						}
					}
				]/**SCHEMA_DIFF*/
			};
		}
);
