define("ContactPageV2", ["ConfigurationConstants", "ITJsConsts", "BusinessRuleModule", "ContactPageV2Resources"],
function(ConfigurationConstants, ITJsConsts, BusinessRuleModule, resources) {
	return {
		entitySchemaName: "Contact",
		
		attributes: {
			"ITMacroregion": {
				"dataValueType": Terrasoft.DataValueType.LOOKUP,
				"lookupListConfig": {
					"filters": [
						function() {
							var filterGroup = this.Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("ITMacroregionByOrgStructure",
							this.Terrasoft.createColumnInFilterWithParameters("ITOrgStructure",
								[ITJsConsts.OrgStructure.MRC.toLowerCase(), ITJsConsts.OrgStructure.AUP.toLowerCase()]
							));
							return filterGroup;
						}
					]
				}
			},
			"OwnerCaption": {
				dataValueType: this.Terrasoft.DataValueType.TEXT,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			"ContactLeadTypeVisible": {
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				calue: true
			},
			//Фильтрация поля "Тип" в зависимости от того, является ли текущий пользователь Администратором
			"Type": {
				"isRequired": true,
				"dataValueType": Terrasoft.DataValueType.LOOKUP,
				"lookupListConfig": {
					"filters": [
						function() {
							var filterGroup = this.Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("TypeByIsAdministrator",
							this.Terrasoft.createColumnInFilterWithParameters("Id", this.array
							));
							return filterGroup;
						}
					]
				}
			},
			// Атрибут определяет доступность поля "Не использовать Email"
			"DoNotUseEmailEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			// Атрибут определяет доступность поля "Не использовать телефон"
			"DoNotUseCallEnabled": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			
			"DoNotUseCall": {
				"onChange": "onDoNotUseCall"
			},
			
			"DoNotUseEmail": {
				"onChange": "onDoNotUseEmail"
			}
		},
		
		rules: {
			//Правила для поля "Филиал"
			"TsAccount": {
				//Обязательность заполнения - удалено
				"RequireIfContactIsEmployee": {
					"ruleType": 999
				},
				
				//фильтруется по Родительскому нотрагенту Филиала
				"FiltrationByUFPS": {
					"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
					"autocomplete": false,
					"baseAttributePatch": "Parent",
					"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
					"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
					"attribute": "ITMacroregion"
				},
				//фильтруется по "Тип" = "Наша компания" - удалено
				"FiltrationByOurCompanyType": {
					"ruleType": 999
				}
			},
			
			//Правила для поля "Макрорегион"
			"ITMacroregion": {
				//Обязательность заполнения "Макрорегион"
				"RequireIfContactIsEmployee": {
					ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
					property: BusinessRuleModule.enums.Property.REQUIRED,
					conditions: [
						{
							"leftExpression": {
								"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								"attribute": "Type"
							},
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"rightExpression": {
								"type": BusinessRuleModule.enums.ValueType.CONSTANT,
								"value": ConfigurationConstants.ContactType.Employee
							}
						}
					]
				},
				//Видимость "Макрорегион"
				"VisibleIfContactIsEmployee": {
					ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
					property: BusinessRuleModule.enums.Property.VISIBLE,
					conditions: [
						{
							"leftExpression": {
								"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								"attribute": "Type"
							},
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"rightExpression": {
								"type": BusinessRuleModule.enums.ValueType.CONSTANT,
								"value": ConfigurationConstants.ContactType.Employee
							}
						}
					]
				}
				//Справочник фильтруется по "Наша компания"
	/*			"FiltrationByOurCompanyType": {
					"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
					"autocomplete": false,
					"baseAttributePatch": "Type",
					"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
					"type": BusinessRuleModule.enums.ValueType.CONSTANT,
					"value": ConfigurationConstants.AccountType.OurCompany.toLowerCase()
				},*/
				//Справочник фильтруется по "Оргструктура" = МРЦ
	/*			"FiltrationByMRC": {
					"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
					"autocomplete": false,
					"baseAttributePatch": "ITOrgStructure",
					"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
					"type": BusinessRuleModule.enums.ValueType.CONSTANT,
					"value": ITJsConsts.OrgStructure.MRC.toLowerCase()
				}*/
			}
		},
		
		//Задаем массив для фильтрации поля "Тип"
		array: [],
		
		methods: {
			
			meetingsFilter: function() {
				var filterGroup = new this.Terrasoft.createFilterGroup();
				filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "42C74C49-58E6-DF11-971B-001D60E938C6"));
				// добавляете фильтр по типу активности
				filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Contact", this.get("Id")));
				return filterGroup;
			},



			
			callsActivityFilter: function() {
				var filterGroup = new this.Terrasoft.createFilterGroup();
				filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "03DF85BF-6B19-4DEA-8463-D5D49B80BB28"));
				// добавляете фильтр по типу активности
				filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Contact", this.get("Id")));
				return filterGroup;
			},



			
			tasksActivityFilter: function() {
				var filterGroup = new this.Terrasoft.createFilterGroup();
				filterGroup.add("ActivityTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ActivityCategory", "F51C4643-58E6-DF11-971B-001D60E938C6"));
				// добавляете фильтр по типу активности
				filterGroup.add("AccountFilter", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "Contact", this.get("Id")));
				return filterGroup;
			},

			
			onEntityInitialized: function() {
				this.getUserSaveRights();
				this.callParent(arguments);
			},

			//Переопределяем метод Ts, чтобы избавиться от метода includes, который не поддерживается IE11-
			actualizeAttributes: function(columnNames) {
				if (Ext.isEmpty(columnNames) || columnNames === "Type") {
					this.set("IsContactEmployee", this.get("Type") &&
						ConfigurationConstants.ContactType.Employee === this.get("Type").value);
				}
				if (Ext.isEmpty(columnNames) || columnNames === "TsAccountOldValue") {
					this.set("TsAccountOldValue", this.get("TsAccount"));
				}
			},

			//Переопределен, чтобы в actualizeAttributes передавать значение, а не массив
			onTypeChanged: function() {
				this.actualizeAttributes("Type");
			},
			
			//Переопределен, чтобы в actualizeAttributes передавать значение, а не массив
			onTsAccountChanged: function() {
				var account = this.get("Account");
				var tsAccountOldValue = this.get("TsAccountOldValue");
				var tsAccount = this.get("TsAccount");

				if (account) {
					if (tsAccountOldValue && tsAccountOldValue.value === account.value) {
						this.set("Account", tsAccount);
					}
				} else {
					if (Ext.isEmpty(tsAccountOldValue)) {
						this.set("Account", tsAccount);
					}
				}
				this.actualizeAttributes("TsAccountOldValue");
			},
			
			//Метод для проверки является ли пользователь Администратором
			getUserSaveRights: function() {
				var employee = ITJsConsts.ContactType.Employee.value, currentUser = Terrasoft.SysValue.CURRENT_USER.value,
				sysAdmins = ConfigurationConstants.SysAdminUnit.Id.SysAdministrators, massive = [];
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "SysUserInRole"
				});
				esq.addColumn("SysRole");
				esq.addColumn("SysUser");
				esq.filters.add("SysUser", Terrasoft.createColumnFilterWithParameter(
					Terrasoft.ComparisonType.EQUAL, "SysUser", currentUser));
				esq.filters.add("SysRole", Terrasoft.createColumnFilterWithParameter(
					Terrasoft.ComparisonType.EQUAL, "SysRole", sysAdmins));
				esq.getEntityCollection(function(response) {
					if (response && response.success) {
						var result = response.collection;
						var isSysAdmin = (result.collection.length !== 0);
						
						//Записываем в массив Id всех строк справочника, за исключением "Сотрудника"
						var lookup = Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "ContactType"
						});
						lookup.addColumn("Id");
						lookup.filters.add("ById", Terrasoft.createColumnFilterWithParameter(
							Terrasoft.ComparisonType.NOT_EQUAL, "Id", employee));
						lookup.getEntityCollection(function(respon) {
							respon.collection.each(function(item) {
								massive.push(item.get("Id"));
							});
							//Если пользователь является Администратором, то в массив добавляется его Id
							if (isSysAdmin) {
								massive.push(employee);
								this.array = massive;
							} else {
								this.array = massive;
							}
						}, this);
					}
				}, this);
			},
			
			getOwnerCaption: function() {
				var type = this.get("Type");
				if (type && type.value) {
					if (type.value === ConfigurationConstants.ContactType.Employee.toLowerCase()) {
						this.set("OwnerCaption", this.get("Resources.Strings.OwnerForEmployeeCaption"));
						this.set("ContactLeadTypeVisible", true);
					} else {
						this.set("OwnerCaption", this.get("Resources.Strings.OwnerCaption"));
						this.set("ContactLeadTypeVisible", false);
					}
				} else {
					this.set("OwnerCaption", this.get("Resources.Strings.OwnerCaption"));
					this.set("ContactLeadTypeVisible", false);
				}
				return this.get("OwnerCaption");
			},
			//
			onDoNotUseCall: function() {
				var isTrue = this.get("DoNotUseCall");
				if (isTrue) {
					this.set("DoNotUseEmailEnabled", false);
					this.set("DoNotUseEmail", false);
				} else {
					this.set("DoNotUseEmailEnabled", true);
				}
			},
			
			onDoNotUseEmail: function() {
				var isTrue2 = this.get("DoNotUseEmail");
				if (isTrue2) {
					this.set("DoNotUseCallEnabled", false);
					this.set("DoNotUseCall", false);
				} else {
					this.set("DoNotUseCallEnabled", true);
				}
			}
		},
		
		details: /**SCHEMA_DETAILS*/{
			// Настройка детали [Акты выполненных работ].
			"ITContactLeadTypeDetail": {
				// Название схемы детали.
				"schemaName": "ITContactLeadTypeDetail",
				// Название схемы объекта детали.
				"entitySchemaName": "ITContactLeadType",
				// Фильтрация отображения контактов только для текущего заказа.
				"filter": {
					// Колонка схемы объекта детали.
					"detailColumn": "ITContact",
					// Колонка схемы объекта раздела.
					"masterColumn": "Id"
				}
			},
			"ITMeetingsDetail": {
				"schemaName": "ITMeetingsDetailV2",
				"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				},
				filterMethod: "meetingsFilter"
			},
			"ITTasksDetailV2": {
				"schemaName": "ITTasksDetailV2",
				//"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				},
				filterMethod: "tasksActivityFilter"
			},
			"ITCallsDetail": {
				"schemaName": "ITCallsDetailV2",
				//"entitySchemaName": "Activity",
				"filter": {
					"detailColumn": "Account",
					"masterColumn": "Id"
				},
				filterMethod: "callsActivityFilter"
			}
		}/**SCHEMA_DETAILS*/,
		
		diff: /**SCHEMA_DIFF*/[
			//Удаление детали "Заказы"
			{
				"operation": "remove",
				"name": "Order"
			},
			//Добавление поля Макрорегион
			{
				"operation": "insert",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "ITMacroregion",
				"values": {
					"bindTo": "ITMacroregion",
	//				"visible": {bindTo: "IsContactEmployee"},
					"contentType": this.Terrasoft.ContentType.LOOKUP
				},
				"index": 3
			},
			{
				"operation": "move",
				"name": "TsAccount",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "move",
				"name": "JobTitleProfile",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "move",
				"name": "AccountMobilePhone",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "move",
				"name": "AccountPhone",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "move",
				"name": "AccountEmail",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "merge",
				"name": "PhotoTimeZoneContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "AccountName",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "ProfileFlexibleContainer",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 3
					}
				}
			},
			{
				"operation": "merge",
				"name": "Owner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					},
					"caption": {
						"bindTo": "getOwnerCaption"
					}
				}
			},
			{
				"operation": "merge",
				"name": "Dear",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "move",
				"name": "Dear",
				"parentName": "ContactGeneralInfoBlock",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "remove",
				"name": "Dear"
			},
			{
				"operation": "merge",
				"name": "Gender",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Job",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "JobTitle",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Department",
				"values": {
					"caption": resources.localizableStrings.DepartmentCaption,
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "remove",
				"name": "Department"
			},
			{
				"operation": "merge",
				"name": "DecisionRole",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "DoNotUseEmail",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "DoNotUseSms",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "move",
				"name": "DoNotUseSms",
				"parentName": "CommunicationChannelsContainer",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "merge",
				"name": "DoNotUseCall",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "DoNotUseMail",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "remove",
				"name": "SalutationType"
			},
			{
				"operation": "remove",
				"name": "TsResponsibilityField"
			},
			{
				"operation": "remove",
				"name": "SiteEventDetail"
			},
			{
				"operation": "remove",
				"name": "Cases"
			},
			{
				"operation": "remove",
				"name": "Project"
			},
			{
				"operation": "remove",
				"name": "EventDetail"
			},
			{
				"operation": "remove",
				"name": "BulkEmailDetail"
			},
			{
				"operation": "remove",
				"name": "BulkEmailSubscriptionDetail"
			},
			{
				"operation": "insert",
				// Название детали.
				"name": "ITContactLeadTypeDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail",
					visible: {bindTo: "ContactLeadTypeVisible"}
				},
				// Контейнеры, в которых размещена деталь.
				"parentName": "GeneralInfoTab",
				"propertyName": "items"
				// Индекс в списке добавляемых элементов.
				//"index": 30
			},
			{
				"operation": "remove",
				"name": "TsLeadType"
			},
			{
				"operation": "merge",
				"name": "DoNotUseEmail",
				"values": {
					"enabled": {bindTo: "DoNotUseEmailEnabled"}
				}
			},
			{
				"operation": "merge",
				"name": "DoNotUseCall",
				"values": {
					"enabled": {bindTo: "DoNotUseCallEnabled"}
				}
			},
			{
				"operation": "merge",
				"name": "DoNotUseSms",
				"values": {
					"enabled": true
				}
			},
			{
				"operation": "merge",
				"name": "DoNotUseMail",
				"values": {
					"enabled": true
				}
			},
			{
				"operation": "merge",
				"name": "InternalRate",
				"values": {
					"visible": false
				}
			},
			{
				"operation": "merge",
				"name": "ExternalRate",
				"values": {
					"visible": false
				}
			},
			// скрытие детали "Счета".
			{
				"operation": "merge",
				"name": "Invoice",
				"values": {
					"visible": false
				}
			},
			
			//Замена Детали Активности
			{
				"operation": "remove",
				"name": "Documents"
			},
			{
				"operation": "remove",
				"name": "Activities"
			},
			{
				"operation": "remove",
				"name": "Calls"
			},
			{
				"operation": "insert",
				"name": "ITMeetingsDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTab",
				"propertyName": "items",
				"index": 10
			},
			{
				"operation": "insert",
				"name": "ITTasksDetailV2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTab",
				"propertyName": "items",
				"index": 11
			},
			{
				"operation": "insert",
				"name": "ITCallsDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "HistoryTab",
				"propertyName": "items",
				"index": 12
			}
			//Замена Детали Активности
		]/**SCHEMA_DIFF*/
	};
});