define("AccountContactsDetailV2", ["BaseGridDetailV2Resources", "ConfigurationEnums",
			"GridUtilitiesV2", "WizardUtilities", "QuickFilterModuleV2"],
	function(resources, enums) {
	return {
		entitySchemaName: "Contact",
		details: {},
		methods: {
			init: function() {
				this.callParent(arguments);
				var isNotOurCompanyType = this.sandbox.publish("GetIsNotOurCompanyTypeForDetail", null, null);
				if (isNotOurCompanyType) {
					this.set("Caption", this.get("Resources.Strings.Caption"));
				} else {
					this.set("Caption", "Контакты клиента");
				}
				this.sandbox.subscribe("SetIsNotOurCompanyTypeForDetail", function(isNotOurCompanyTypeArg) {
					if (isNotOurCompanyTypeArg) {
						this.set("Caption", this.get("Resources.Strings.Caption"));
					} else {
						this.set("Caption", this.get("Resources.Strings.AlternativeCaption"));
					}
				}, this, null);
			},
			openContactlookup: function() {
				var config = {
					entitySchemaName: "Contact",
					multiSelect: false,
					columns: ["Name"]
				};
				var esqContact = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "Contact"
				});
				esqContact.addColumn("Name");
				esqContact.addColumn("Account.Name", "AccountName");
				var esqFilter = esqContact.createColumnIsNullFilter("Account");
				config.filters = esqFilter;
				this.openLookup(config, this.addCallBack, this);
			},
			addCallBack: function(args) {
				var bq = this.Ext.create("Terrasoft.BatchQuery");
				var AccountId = this.get("MasterRecordId");
				this.selectedRows = args.selectedRows.getItems();
				this.selectedItems = [];
				this.selectedRows.forEach(function(item) {
					item.ITAccountId = AccountId;
					item.ITContactId = item.value;
					bq.add(this.getContactInsertQuery(item));
					this.selectedItems.push(item.value);
				}, this);
				if (bq.queries.length) {
					this.showBodyMask.call(this);
					bq.execute(this.onContactInsert, this);
				}
				var primaryColumnValue = this.selectedRows[0].Id;
				var config = {
					primaryColumnValue: primaryColumnValue
				};
				this.updateDetail(config);
			},
			getContactInsertQuery: function(item) {
				var insert = Ext.create("Terrasoft.InsertQuery", {
					rootSchemaName: "ContactCareer"
				});
				insert.setParameterValue("Account", item.ITAccountId, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("Contact", item.ITContactId, this.Terrasoft.DataValueType.GUID);
				return insert;
			},
			onContactInsert: function(response) {
				this.hideBodyMask.call(this);
				this.beforeLoadGridData();
				var filterCollection = [];
				response.queryResults.forEach(function(item) {
					filterCollection.push(item.id);
				});
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: this.entitySchemaName
				});
				this.initQueryColumns(esq);
				esq.filters.add("recordId", Terrasoft.createColumnInFilterWithParameters("Id", filterCollection));
				esq.getEntityCollection(function(response) {
					this.afterLoadGridData();
					if (response.success) {
						var responseCollection = response.collection;
						this.prepareResponseCollection(responseCollection);
						this.getGridData().loadAll(responseCollection);
					}
				}, this);
			}
		},
		messages: {
			"GetIsNotOurCompanyTypeForDetail": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.PUBLISH
			},
			"SetIsNotOurCompanyTypeForDetail": {
				mode: this.Terrasoft.MessageMode.BROADCAST,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		attributes: {},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "AddRecordButton",
				"parentName": "Detail",
				"propertyName": "tools",
				"values": {
					"click": false,
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"menu": {
						"items": [
							{
								"caption": "Создание нового контакта",
								"click": {"bindTo": "addRecord"}
							},
							{
								"caption": "Добавление существующего контакта",
								"click": {"bindTo": "openContactlookup"}
							}
						]
					},
					"visible": true,
					"enabled": true,
					"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
					"imageConfig": {"bindTo": "Resources.Images.AddButtonImage"}
				}
			}
		]/**SCHEMA_DIFF*/
	};
});