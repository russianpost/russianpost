using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Text;
using System.Xml;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using System.Linq;
using System.IO;
using System.DirectoryServices;
using System.Security.Principal;
using Terrasoft.Core.LDAP;

namespace Terrasoft.Configuration.ITBase
{
	public class AdUser
    {
        /// <summary>
        /// FIO
        /// </summary>
        public string Cn { get; set; }
        /// <summary>
        /// DN
        /// </summary>
        public string DistinguishedName { get; set; }
        /// <summary>
        /// Sid
        /// </summary>
        public string LDAPEntryId { get; set; }
        /// <summary>
        /// Login
        /// </summary>
        public string SAMAccountName { get; set; }
        /// <summary>
        /// userPrincipalName
        /// </summary>
        public string UserPrincipalName { get; set; }
        /// <summary>
        /// mail
        /// </summary>
        public string Email { get; set; }

        public AdUser(string cn, string distinguishedName, string objectSid,
            string sAMAccountName, string userPrincipalName, string email)
        {
            Cn = cn;
            DistinguishedName = distinguishedName;
            LDAPEntryId = objectSid;
            SAMAccountName = sAMAccountName;
            UserPrincipalName = userPrincipalName;
            Email = email;
        }
    }

    public class SysAdminUnitItem
    {
        public Guid Id { get; set; }
        public string LDAPEntry { get; set; }
        public string LDAPEntryId { get; set; }
        public string LDAPEntryDN { get; set; }
        public Guid? LDAPElementId { get; set; }

        public SysAdminUnitItem(Guid id, string lDAPEntry, string lDAPEntryId,
            string lDAPEntryDN, Guid? lDAPElementId)
        {
            this.Id = id;
            this.LDAPEntry = lDAPEntry;
            this.LDAPEntryId = lDAPEntryId;
            this.LDAPEntryDN = lDAPEntryDN;
            this.LDAPElementId = lDAPElementId;
        }
    }

    public class ITEmsAdImportLogic : ITDBBase
    {
        public ITEmsAdImportLogic(UserConnection userConnection) : base(userConnection)
        {

        }

        public void DoAdImport(UserConnection userConnection)
        {
            List<string> existedLDAPEntryDb = GetLDAPEntryDbUsers(userConnection);
            List<SysAdminUnitItem> sysAdminUnits = GetMainUnits(userConnection);

            if(sysAdminUnits.Count > 0)
            {
                for (int i = 0; i < sysAdminUnits.Count; i++)
                {
                    var sysAdminUnitItem = sysAdminUnits[i];
                    List<AdUser> adUsers = GetAdUsers(sysAdminUnitItem.LDAPEntryDN, userConnection);
                    if(adUsers.Count > 0)
                    {
                        for(int j = 0; j < adUsers.Count; j++)
                        {
                            AdUser adUser = adUsers[j];
                            var existedUser = existedLDAPEntryDb.Where(l => l == adUser.LDAPEntryId).FirstOrDefault();
                            bool isUserExistInDB = existedUser != null ? true : false;
                            if(isUserExistInDB == false)
                            {
                                InsertNewAdUserInDb(adUser, sysAdminUnitItem.Id, userConnection);
                            }
                        }
                    }
                }
            }
        }

        private List<SysAdminUnitItem> GetMainUnits(UserConnection userConnection)
        {
            List<SysAdminUnitItem> result = new List<SysAdminUnitItem>();
            var selectSysAdminUnit = new Select(userConnection).From("SysAdminUnit")
                .Column("Id")
                .Column("LDAPEntry")
                .Column("LDAPEntryId")
                .Column("LDAPEntryDN")
                .Column("LDAPElementId")
                ;
            using (DBExecutor executor = userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = selectSysAdminUnit.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        var id = reader.GetColumnValue<Guid>("Id");
                        var lDAPEntry = reader.GetColumnValue<string>("LDAPEntry");
                        var lDAPEntryId = reader.GetColumnValue<string>("LDAPEntryId");
                        var lDAPEntryDN = reader.GetColumnValue<string>("LDAPEntryDN");
                        var lDAPElementId = reader.GetColumnValue<Guid>("LDAPElementId");
                        if(!string.IsNullOrEmpty(lDAPEntry)
                            && !string.IsNullOrEmpty(lDAPEntryId)
                            && !string.IsNullOrEmpty(lDAPEntryDN)
                            && lDAPElementId != null)
                        {
                            SysAdminUnitItem saunititem = new SysAdminUnitItem(id, lDAPEntry,
                                lDAPEntryId, lDAPEntryDN, lDAPElementId);
                            result.Add(saunititem);
                        }
                    }
                }
            }
            return result;
        }

        private Guid? GetSysRoleIdFromSysAdminUnitTable(string rUnit, UserConnection userConnection)
        {
            try
            {
                string lDAPEntryDN = "OU=" + rUnit + ",OU=FGUP,DC=main,DC=russianpost,DC=ru";
                Guid? sysRoleId = null;
                var selectSysAdminUnit = new Select(userConnection).From("SysAdminUnit")
                    .Column("Id").Where("LDAPEntryDN")
                    .IsEqual(Column.Parameter(lDAPEntryDN)) as Select;
                using (DBExecutor executor = userConnection.EnsureDBConnection())
                {
                    using (IDataReader reader = selectSysAdminUnit.ExecuteReader(executor))
                    {
                        while (reader.Read())
                        {
                            sysRoleId = reader.GetColumnValue<Guid>("Id");
                        }
                    }
                }
                return sysRoleId;
            }
            catch (Exception ex)
            {
                throw new Exception("GetSysRoleIdFromSysAdminUnitTable(string rUnit, UserConnection userConnection)");
            }
        }

        private void InsertNewAdUserInDb(AdUser adUser, Guid sysRoleId, UserConnection userConnection)
        {
            //Create Contact
            Guid contactId = Guid.NewGuid();
            try
            {
                InsertNewContactInDB(contactId, adUser, userConnection);
            }
            catch (Exception ex)
            {
                throw new Exception("InsertNewAdUserInDb(AdUser adUser, Guid sysRoleId, UserConnection userConnection)");
            }
            //Create LDAPElement
            Guid lDAPElementId = Guid.NewGuid();
            try
            {
                InsertNewLDAPElement(lDAPElementId, adUser, userConnection);
            }
            catch (Exception ex)
            {
                throw new Exception("InsertNewLDAPElement(lDAPElementId, adUser, userConnection);");
            }
            //Create SysAdminUnit
            Guid sysAdminUnitId = Guid.NewGuid();
            try
            {
                InsertSysAdminUnit(ref sysAdminUnitId, contactId, lDAPElementId, adUser, userConnection);
            }
            catch (Exception ex)
            {
                throw new Exception("InsertSysAdminUnit(ref sysAdminUnitId, contactId, lDAPElementId, adUser, userConnection);");
            }
            //Create SysUserInRole
            Guid sysUserInRoleId = Guid.NewGuid();
            try
            {
                InsertSysUserInRole(sysUserInRoleId, sysAdminUnitId, sysRoleId, userConnection);
            }
            catch (Exception ex)
            {
                throw new Exception("InsertSysUserInRole(sysUserInRoleId, sysAdminUnitId, sysRoleId, userConnection);");
            }
        }

        private void InsertSysUserInRole(Guid sysUserInRoleId, Guid sysAdminUnitId, Guid sysRoleId, UserConnection userConnection)
        {
            var insertSysUserInRole = new Insert(userConnection).Into("SysUserInRole")
                .Set("Id", Column.Parameter(sysUserInRoleId))
                .Set("SysUserId", Column.Parameter(sysAdminUnitId))
                .Set("SysRoleId", Column.Parameter(sysRoleId))
                .Set("ProcessListeners", Column.Parameter(0));
            insertSysUserInRole.Execute();
        }

        private void InsertSysAdminUnit(ref Guid sysAdminUnit, Guid contactId, Guid lDAPElementId, AdUser adUser, UserConnection userConnection)
        {
            //try{
            Guid? existedSysAdminUnitItemId = null;
            var SysCultureId = new Guid("1A778E3F-0A8E-E111-84A3-00155D054C03");
            
            var selectSysAdminUnit = new Select(userConnection).From("SysAdminUnit")
                .Column("Id").Where("Name")
                .IsEqual(Column.Parameter(adUser.SAMAccountName)) as Select;
            using (DBExecutor executor = userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = selectSysAdminUnit.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        existedSysAdminUnitItemId = reader.GetColumnValue<Guid>("Id");
                    }
                }
            }

            bool isExistSysAdminUnit = existedSysAdminUnitItemId != null ? true : false;

            if (isExistSysAdminUnit == true)
            {
                sysAdminUnit = (Guid)existedSysAdminUnitItemId;
                var updateSysAdminUnit = new Update(userConnection, "SysAdminUnit")
                    .Set("SynchronizeWithLDAP", Column.Parameter(true))
                    .Set("LDAPEntry", Column.Parameter(adUser.SAMAccountName))
                    .Set("LDAPEntryId", Column.Parameter(adUser.LDAPEntryId))
                    .Set("LDAPElementId", Column.Parameter(lDAPElementId))
                    .Set("SysCultureId", Column.Parameter(SysCultureId))
                    .Where("Id")
                    .IsEqual(Column.Parameter((Guid)existedSysAdminUnitItemId))
                    ;
                updateSysAdminUnit.Execute();
            }
            else
            {
                var insertSysAdminUnit = new Insert(userConnection).Into("SysAdminUnit")
                .Set("Id", Column.Parameter(sysAdminUnit))
                .Set("Name", Column.Parameter(adUser.SAMAccountName))
                .Set("Description", Column.Parameter("ITAutoLDAPUsersLoading"))
                .Set("ContactId", Column.Parameter(contactId))
                .Set("TimeZoneId", Column.Parameter("Russian Standard Time"))
                .Set("UserPassword", Column.Parameter(""))
                .Set("SysAdminUnitTypeValue", Column.Parameter(4))
                .Set("Active", Column.Parameter(true))
                .Set("LoggedIn", Column.Parameter(0))
                .Set("SynchronizeWithLDAP", Column.Parameter(true))
                .Set("LDAPEntry", Column.Parameter(adUser.SAMAccountName))
                .Set("LDAPEntryId", Column.Parameter(adUser.LDAPEntryId))
                .Set("LDAPEntryDN", Column.Parameter(""))
                .Set("IsDirectoryEntry", Column.Parameter(0))
                .Set("ProcessListeners", Column.Parameter(0))
                .Set("LoginAttemptCount", Column.Parameter(0))
                .Set("SourceControlLogin", Column.Parameter(""))
                .Set("SourceControlPassword", Column.Parameter(""))
                .Set("ConnectionType", Column.Parameter(0))
                .Set("ForceChangePassword", Column.Parameter(0))
                .Set("LDAPElementId", Column.Parameter(lDAPElementId))
                .Set("SysCultureId", Column.Parameter(SysCultureId))
                ;
                insertSysAdminUnit.Execute();
            }
            //}
            //catch(Exception ex)
            //{
            //	throw new Exception("error in InsertSysAdminUnit: " + ex.Message);
            //}
        }

        private void InsertNewLDAPElement(Guid lDAPElementId, AdUser adUser, UserConnection userConnection)
        {
            var insertLDAPElement = new Insert(userConnection).Into("LDAPElement")
                .Set("Id", Column.Parameter(lDAPElementId))
                .Set("Name", Column.Parameter(adUser.Cn))
                .Set("Description", Column.Parameter("ITAutoLDAPUsersLoading"))
                .Set("ProcessListeners", Column.Parameter(0))
                .Set("LDAPEntryId", Column.Parameter(adUser.LDAPEntryId))
                .Set("LDAPEntryDN", Column.Parameter(""))
                .Set("Type", Column.Parameter(4))
                .Set("FullName", Column.Parameter(adUser.SAMAccountName))
                .Set("Company", Column.Parameter(""))
                .Set("Email", Column.Parameter(""))
                .Set("Phone", Column.Parameter(""))
                .Set("JobTitle", Column.Parameter(""))
                .Set("IsActive", Column.Parameter(true))
                ;
            insertLDAPElement.Execute();
        }

        private void InsertNewContactInDB(Guid contactId, AdUser adUser, UserConnection userConnection)
        {

            //int rId = GetMaxRIdFromContact(userConnection);

            string surname = "";
            string givenname = "";
            string middlename = "";

            FillFIOFromCN(adUser, ref surname, ref givenname, ref middlename);
            var insertContact = new Insert(userConnection).Into("Contact")
                .Set("Id", Column.Parameter(contactId))
                .Set("Name", Column.Parameter(adUser.Cn))
                .Set("Description", Column.Parameter("ITAutoLDAPUsersLoading"))
                .Set("Dear", Column.Parameter(""))
                .Set("JobTitle", Column.Parameter(""))
                .Set("Phone", Column.Parameter(""))
                .Set("MobilePhone", Column.Parameter(""))
                .Set("HomePhone", Column.Parameter(""))
                .Set("Skype", Column.Parameter(""))
                .Set("Email", Column.Parameter(adUser.Email))
                .Set("Address", Column.Parameter(""))
                .Set("Zip", Column.Parameter(""))
                .Set("DoNotUseEmail", Column.Parameter(false))
                .Set("DoNotUseCall", Column.Parameter(false))
                .Set("DoNotUseFax", Column.Parameter(false))
                .Set("DoNotUseSms", Column.Parameter(false))
                .Set("Notes", Column.Parameter(""))
                .Set("Facebook", Column.Parameter(""))
                .Set("LinkedIn", Column.Parameter(""))
                .Set("Twitter", Column.Parameter(""))
                .Set("FacebookId", Column.Parameter(""))
                .Set("LinkedInId", Column.Parameter(""))
                .Set("TwitterId", Column.Parameter(""))
                .Set("ProcessListeners", Column.Parameter(0))
                .Set("GPSN", Column.Parameter(""))
                .Set("GPSE", Column.Parameter(""))
                .Set("Surname", Column.Parameter(surname))
                .Set("GivenName", Column.Parameter(givenname))
                .Set("MiddleName", Column.Parameter(middlename))
                .Set("Confirmed", Column.Parameter(true))
                .Set("IsNonActualEmail", Column.Parameter(false))
                //.Set("RId", Column.Parameter(rId))
                .Set("Completeness", Column.Parameter(0))
                ;
            insertContact.Execute();
        }

        private int GetMaxRIdFromContact(UserConnection userConnection)
        {
            int result = 1;
            bool filled = false;
            var selectContact = new Select(userConnection).From("Contact").Column("RId")
                .OrderByDesc("RId") as Select;
            using (DBExecutor executor = userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = selectContact.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        if (filled == false)
                        {
                            result = reader.GetColumnValue<int>("RId");
                            ++result;
                            filled = true;
                        }
                    }
                }
            }
            return result;
        }

        private void FillFIOFromCN(AdUser adUser, ref string surname, ref string givenname, ref string middlename)
        {
            try
            {
                var split = adUser.Cn.Split(' ');
                surname = split[0];
                givenname = split[1];
                middlename = split[2];
            }
            catch { }
        }

        private List<string> GetLDAPEntryDbUsers(UserConnection userConnection)
        {
            List<string> result = new List<string>();
            var selectSysAdminUnits = new Select(userConnection).From("SysAdminUnit")
                .Column("LDAPEntryId").Where("SysAdminUnitTypeValue")
                .IsEqual(Column.Parameter(4)) as Select;
            using (DBExecutor executor = userConnection.EnsureDBConnection())
            {
                using (IDataReader reader = selectSysAdminUnits.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        var lDAPEntryId = reader.GetColumnValue<string>("LDAPEntryId");
                        if (!string.IsNullOrEmpty(lDAPEntryId))
                            result.Add(lDAPEntryId);
                    }
                }
            }
            return result;
        }

        private string GetUnitName(int i)
        {
            if (i < 10)
            {
                return "R0" + i;
            }
            else
            {
                return "R" + i;
            }
        }

        private List<AdUser> GetAdUsers(string oUnit, UserConnection userConnection)
        {
            List<AdUser> users = new List<AdUser>();
            
            if(oUnit.StartsWith("OU=АйТи,OU=Subcontractors,OU=R00,OU=FGUP,DC=main,DC=russianpost,DC=ru"))
            {
                DirectoryEntry searchRoot = CreateDirectoryEntry(oUnit);
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                search.Filter = "(&(memberOf=CN=R00-D01SFA_admins,OU=Servers_admins,OU=Groups,OU=R00,OU=FGUP,DC=main,DC=russianpost,DC=ru)(objectClass=person))";
                search.PropertiesToLoad.Add("cn");
                search.PropertiesToLoad.Add("userPrincipalName");
                search.PropertiesToLoad.Add("objectSid");
                search.PropertiesToLoad.Add("sAMAccountName");
                search.PropertiesToLoad.Add("distinguishedName");
                search.PropertiesToLoad.Add("mail");

                SearchResult result;
                SearchResultCollection resultCol = search.FindAll();

                if (resultCol != null)
                {
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                        result = resultCol[counter];
                        if (result.Properties.Contains("cn")
                            && result.Properties.Contains("userPrincipalName")
                            && result.Properties.Contains("objectSid")
                            && result.Properties.Contains("sAMAccountName")
                            && result.Properties.Contains("distinguishedName")
                            && result.Properties.Contains("mail"))
                        {
                            var sidInBytes = (byte[])result.Properties["objectSid"][0];
                            LdapUtilities ldapUtils = new LdapUtilities(userConnection);
                            DirectoryEntry user = result.GetDirectoryEntry();

                            if (user != null)
                            {
                                try
                                {
                                    string objectSid = ldapUtils.IdentityAttributeToString(sidInBytes);
                                    string userPrincipalName = (string)result.Properties["userPrincipalName"][0];
                                    string cn = (string)result.Properties["cn"][0];
                                    string sAMAccountName = (string)result.Properties["sAMAccountName"][0];
                                    string email = (string)result.Properties["mail"][0];
                                    string distinguishedName = user.Properties["distinguishedName"].Value.ToString();
                                    AdUser adUser = new AdUser(cn, distinguishedName, objectSid,
                                        sAMAccountName, userPrincipalName, email);
                                    users.Add(adUser);
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("new AdUser(cn, distinguishedName, objectSid, sAMAccountName, userPrincipalName, email);");
                                }
                            }

                        }
                    }
                }
            }
            else
            {
                DirectoryEntry searchRoot = CreateDirectoryEntry(oUnit);
                DirectorySearcher search = new DirectorySearcher(searchRoot);
                search.Filter = "(&(memberOf=CN=R00-CRM_users,OU=CRM_Users,OU=Groups,OU=R00,OU=FGUP,DC=main,DC=russianpost,DC=ru)(objectClass=person))";
                search.PropertiesToLoad.Add("cn");
                search.PropertiesToLoad.Add("userPrincipalName");
                search.PropertiesToLoad.Add("objectSid");
                search.PropertiesToLoad.Add("sAMAccountName");
                search.PropertiesToLoad.Add("distinguishedName");
                search.PropertiesToLoad.Add("mail");

                SearchResult result;
                SearchResultCollection resultCol = search.FindAll();

                if (resultCol != null)
                {
                    for (int counter = 0; counter < resultCol.Count; counter++)
                    {
                        result = resultCol[counter];
                        if (result.Properties.Contains("cn")
                            && result.Properties.Contains("userPrincipalName")
                            && result.Properties.Contains("objectSid")
                            && result.Properties.Contains("sAMAccountName")
                            && result.Properties.Contains("distinguishedName")
                            && result.Properties.Contains("mail"))
                        {
                            var sidInBytes = (byte[])result.Properties["objectSid"][0];
                            LdapUtilities ldapUtils = new LdapUtilities(userConnection);
                            DirectoryEntry user = result.GetDirectoryEntry();

                            if (user != null)
                            {
                                try
                                {
                                    string objectSid = ldapUtils.IdentityAttributeToString(sidInBytes);
                                    string userPrincipalName = (string)result.Properties["userPrincipalName"][0];
                                    string cn = (string)result.Properties["cn"][0];
                                    string sAMAccountName = (string)result.Properties["sAMAccountName"][0];
                                    string email = (string)result.Properties["mail"][0];
                                    string distinguishedName = user.Properties["distinguishedName"].Value.ToString();
                                    AdUser adUser = new AdUser(cn, distinguishedName, objectSid,
                                        sAMAccountName, userPrincipalName, email);
                                    users.Add(adUser);
                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("new AdUser(cn, distinguishedName, objectSid, sAMAccountName, userPrincipalName, email);");
                                }
                            }

                        }
                    }
                }
            }
            
            return users;
        }

        private DirectoryEntry CreateDirectoryEntry(string oUnit)
        {
            DirectoryEntry ldapConnection = new DirectoryEntry();
            ldapConnection.Username = "CRM-ldap-R00";
            ldapConnection.Password = "IvuCE3GxJh";
            //ldapConnection.Path = "LDAP://OU=" + rUnit + ",OU=FGUP,DC=main,DC=russianpost,DC=ru";
            ldapConnection.Path = "LDAP://" + oUnit;
            ldapConnection.AuthenticationType = AuthenticationTypes.Secure;
            return ldapConnection;
        }
    }
}