// Определение схемы и установка ее зависимостей от других модулей.
define("ITContactLeadTypeDetail", ["ConfigurationEnums"],
	function(configurationEnums) {
		return {
			// Название схемы объекта детали.
			entitySchemaName: "ITContactLeadType",
			// Методы схемы детали.
			methods: {
				//Возвращает колонки, которые выбираются запросом.
				getGridDataColumns: function() {
					return {
						"Id": {path: "Id"},
						"ITLeadType": {path: "ITLeadType"}
					};
				},
				//Конфигурирует и отображает модальное окно справочника.
				openDocumentLookup: function() {
					//Конфигурационный объект
					var config = {
						// Название схемы объекта, записи которого будут отображены в справочнике.
						entitySchemaName: "LeadType",
						// Возможность множественного выбора.
						multiSelect: true,
						// Колонки, которые будут отображены в справочнике 
						columns: ["Name"]
					};
					var ContactId = this.get("MasterRecordId");
					if (this.Ext.isEmpty(ContactId)) {
						return;
					}
					// Экземпляр класса [EntitySchemaQuery].
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						// Установка корневой схемы.
						rootSchemaName: this.entitySchemaName
					});
					// Добавление колонки [Id].
					esq.addColumn("Id");
					// Добавление колонки [Id] из схемы [Document].
					esq.addColumn("ITLeadType.Id", "ITLeadTypeId");
					// Создание и добавление фильтров в коллекцию запроса.
					esq.filters.add("filterOrder", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "ITContact", ContactId));
					// Получение всей коллекции записей и отображение ее в модальном окне справочника.
					esq.getEntityCollection(function(result) {
						var existsDocumentsCollection = [];
						if (result.success) {
							result.collection.each(function(item) {
								existsDocumentsCollection.push(item.get("ITLeadTypeId"));
							});
						}
						// Добавление фильтра в конфигурационный объект.
						if (existsDocumentsCollection.length > 0) {
							var existsFilter = this.Terrasoft.createColumnInFilterWithParameters("Id",
								existsDocumentsCollection);
							existsFilter.comparisonType = this.Terrasoft.ComparisonType.NOT_EQUAL;
							existsFilter.Name = "existsFilter";
							config.filters = existsFilter;
						}
						// Вызов модального окна справочника
						this.openLookup(config, this.addCallBack, this);
					}, this);
				},

				// Обработчик события сохранения страницы редактирования.
				onCardSaved: function() {
					this.openDocumentLookup();
				},

				//Открывает справочник документов в случае если страница редактирования заказа была ранее сохранена.
				addRecord: function() {
					var masterCardState = this.sandbox.publish("GetCardState", null, [this.sandbox.id]);
					var isNewRecord = (masterCardState.state === configurationEnums.CardStateV2.ADD ||
					masterCardState.state === configurationEnums.CardStateV2.COPY);
					if (isNewRecord === true) {
						var args = {
							isSilent: true,
							messageTags: [this.sandbox.id]
						};
						this.sandbox.publish("SaveRecord", args, [this.sandbox.id]);
						return;
					}
					this.openDocumentLookup();
				},

				// Добавление выбранных продуктов.
				addCallBack: function(args) {
					// Экземпляр класса пакетного запроса BatchQuery.
					var bq = this.Ext.create("Terrasoft.BatchQuery");
					var ContactId = this.get("MasterRecordId");
					// Коллекция выбранных в справочнике документов.
					this.selectedRows = args.selectedRows.getItems();
					// Коллекция, передаваемая в запрос.
					this.selectedItems = [];
					// Копирование необходимых данных.
					this.selectedRows.forEach(function(item) {
						item.ITContactId = ContactId;
						item.ITLeadTypeId = item.value;
						bq.add(this.getDocumentInsertQuery(item));
						this.selectedItems.push(item.value);
					}, this);
					// Выполнение пакетного запроса, если он не пустой.
					if (bq.queries.length) {
						this.showBodyMask.call(this);
						bq.execute(this.onDocumentInsert, this);
					}
				},

				//Возвращает запрос на добавление текущего объекта.
				getDocumentInsertQuery: function(item) {
					var insert = Ext.create("Terrasoft.InsertQuery", {
						rootSchemaName: this.entitySchemaName
					});
					insert.setParameterValue("ITContact", item.ITContactId, this.Terrasoft.DataValueType.GUID);
					insert.setParameterValue("ITLeadType", item.ITLeadTypeId, this.Terrasoft.DataValueType.GUID);
					return insert;
				},

				//Метод, вызываемый при добавлении записей в реестр детали.
				onDocumentInsert: function(response) {
					this.hideBodyMask.call(this);
					this.beforeLoadGridData();
					var filterCollection = [];
					response.queryResults.forEach(function(item) {
						filterCollection.push(item.id);
					});
					var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: this.entitySchemaName
					});
					this.initQueryColumns(esq);
					esq.filters.add("recordId", Terrasoft.createColumnInFilterWithParameters("Id", filterCollection));
					esq.getEntityCollection(function(response) {
						this.afterLoadGridData();
						if (response.success) {
							var responseCollection = response.collection;
							this.prepareResponseCollection(responseCollection);
							this.getGridData().loadAll(responseCollection);
						}
					}, this);
				},

				// Метод, вызываемый при удалении выбранных записей детали.
				deleteRecords: function() {
					var selectedRows = this.getSelectedItems();
					if (selectedRows.length > 0) {
						this.set("SelectedRows", selectedRows);
						this.callParent(arguments);
					}
				},

				// Скрыть пункт меню [Копировать].
				getCopyRecordMenuItem: Terrasoft.emptyFn,
				// Скрыть пункт меню [Изменить].
				getEditRecordMenuItem: Terrasoft.emptyFn,
				// Возвращает имя колонки по умолчанию для фильтра.
				getFilterDefaultColumnName: function() {
					return "Document";
				}
			},
			// Массив модификаций.
			diff: /**SCHEMA_DIFF*/[
				{
					// Тип операции — слияние.
					"operation": "merge",
					// Название элемента схемы, над которым производится действие.
					"name": "DataGrid",
					// Объект, свойства которого будут объединены со свойствами элемента схемы.
					"values": {
						"rowDataItemMarkerColumnName": "Document"
					}
				},
				{
					// Тип операции — слияние.
					"operation": "merge",
					// Название элемента схемы, над которым производится действие.
					"name": "AddRecordButton",
					// Объект, свойства которого будут объединены со свойствами элемента схемы.
					"values": {
						"visible": {"bindTo": "getToolsVisible"}
					}
				}
			]/**SCHEMA_DIFF*/
		};
	}
);