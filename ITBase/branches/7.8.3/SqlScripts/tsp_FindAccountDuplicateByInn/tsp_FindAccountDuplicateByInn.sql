IF NOT OBJECT_ID('[dbo].[tsp_FindAccountDuplicateByInn]') IS NULL
BEGIN
DROP PROCEDURE [dbo].[tsp_FindAccountDuplicateByInn];
END;
GO
CREATE PROCEDURE [dbo].[tsp_FindAccountDuplicateByInn] (
@parsedConfig CreatingObjectInfo READONLY,
@sysAdminUnit UNIQUEIDENTIFIER,
@ruleId UNIQUEIDENTIFIER
)
AS
BEGIN
	DECLARE @parsedConfigRowsCount INT = (SELECT COUNT(*) FROM @parsedConfig);
	IF OBJECT_ID('tempdb..#searchAccountInn') IS NOT NULL
	BEGIN
		DROP TABLE #searchAccountInn
	END
	CREATE TABLE #searchAccountInn (
		[TsINN] NVARCHAR(50) COLLATE database_default,
		[SortDate] DATETIME
	);
	
	
	
	
	IF @parsedConfigRowsCount = 0
	BEGIN
		INSERT INTO #searchAccountInn ([TsINN], [SortDate])
		SELECT
			[TsINN],
			MAX([ModifiedOn])
		FROM [Account]
		GROUP BY [TsINN]
		HAVING COUNT(*) > 1;
	END;
	INSERT INTO [AccountDuplicateSearchResult] ([AccountId], [GroupId], [RuleId],
		[SysAdminUnitId])
		SELECT
			[vr].[Id],
			DENSE_RANK() OVER (ORDER BY [vr].[SortDate] DESC, [vr].[TsINN]),
			@ruleId RuleId,
			@sysAdminUnit
		FROM (
			SELECT
				[v].[Id],
				[v].[TsINN],
				[r].[SortDate]
			FROM [Account] [v], #searchAccountInn r
				WHERE [v].[TsINN] = [r].[TsINN]
				GROUP BY [v].[TsINN], [r].[SortDate], [v].[Id]
		) [vr];
	END;
GO