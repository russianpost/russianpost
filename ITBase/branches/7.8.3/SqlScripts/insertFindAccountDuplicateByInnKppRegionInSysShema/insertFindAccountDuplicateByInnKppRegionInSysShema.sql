DECLARE @AccountUId UNIQUEIDENTIFIER;

Set @AccountUId = (SELECT TOP 1 SysSchema.UId FROM SysSchema
WHERE SysSchema.Name = 'Account' AND SysSchema.ExtendParent = 0);

INSERT INTO DuplicatesRule ([IsActive], [ObjectId], [ProcedureName], [Name]) VALUES
  (1, @AccountUId, 'tsp_FindAccountDuplicateByKppInnRegion', 'Дубли контрагентов. ИНН, КПП, регион');