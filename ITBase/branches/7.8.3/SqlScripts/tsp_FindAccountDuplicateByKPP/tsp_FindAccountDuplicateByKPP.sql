IF NOT OBJECT_ID('[dbo].[tsp_FindAccountDuplicateByKpp]') IS NULL
BEGIN
DROP PROCEDURE [dbo].[tsp_FindAccountDuplicateByKpp];
END;
GO
CREATE PROCEDURE [dbo].[tsp_FindAccountDuplicateByKpp] (
@parsedConfig CreatingObjectInfo READONLY,
@sysAdminUnit UNIQUEIDENTIFIER,
@ruleId UNIQUEIDENTIFIER
)
AS
BEGIN
	DECLARE @parsedConfigRowsCount INT = (SELECT COUNT(*) FROM @parsedConfig);
	IF OBJECT_ID('tempdb..#searchAccountKpp') IS NOT NULL
	BEGIN
		DROP TABLE #searchAccountKpp
	END
	CREATE TABLE #searchAccountKpp (
		[TsKPP] int,
		[SortDate] DATETIME
	);
	
	
	
	
	IF @parsedConfigRowsCount = 0
	BEGIN
		INSERT INTO #searchAccountKpp ([TsKPP], [SortDate])
		SELECT
			[TsKPP],
			MAX([ModifiedOn])
		FROM [Account]
		GROUP BY [TsKPP]
		HAVING COUNT(*) > 1;
	END;
	INSERT INTO [AccountDuplicateSearchResult] ([AccountId], [GroupId], [RuleId],
		[SysAdminUnitId])
		SELECT
			[vr].[Id],
			DENSE_RANK() OVER (ORDER BY [vr].[SortDate] DESC, [vr].[TsKPP]),
			@ruleId RuleId,
			@sysAdminUnit
		FROM (
			SELECT
				[v].[Id],
				[v].[TsKPP],
				[r].[SortDate]
			FROM [Account] [v], #searchAccountKpp r
				WHERE [v].[TsKPP] = [r].[TsKPP]
				GROUP BY [v].[TsKPP], [r].[SortDate], [v].[Id]
		) [vr];
	END;
GO