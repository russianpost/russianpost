GO
IF OBJECT_ID ('dbo.ITVwLeadDataToDashboard') IS NOT NULL
    DROP VIEW dbo.ITVwLeadDataToDashboard;
GO
GO
CREATE VIEW ITVwLeadDataToDashboard AS
SELECT     NULL AS Id,  NULL AS CreatedOn, NULL AS CreatedById, NULL AS ModifiedOn, NULL AS ModifiedById, NULL AS ProcessListeners, 
					  lead.Id AS ITLeadId,
                      Qualified * 100 AS ITQualification, SaleParticipant * 100 AS ITSale,
                          (CASE WHEN opp.StageId = '60D5310C-5BE6-DF11-971B-001D60E938C6' THEN 100
						  ELSE 0
						  END) as ITVictory
FROM         dbo.Lead AS lead
LEFT JOIN dbo.Opportunity as opp ON lead.OpportunityId = opp.Id
WHERE     (lead.CreatedOn > DATEADD(day, - DAY(GETDATE()), GETDATE()))
GO