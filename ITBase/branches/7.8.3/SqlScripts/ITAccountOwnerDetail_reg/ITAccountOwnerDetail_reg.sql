DECLARE 
    -- Название схемы представления создаваемой миникарточки.
    @ClientUnitSchemaName NVARCHAR(100) = 'ITAccountOwnerDetail',
    -- Название схемы объекта, к которому привязывается миникарточка.
    @EntitySchemaName NVARCHAR(100) = 'ITVwAccountOwner',
    -- Название детали.
    @DetailCaption NVARCHAR(100) = 'Ответственные в направлениях бизнеса'

INSERT INTO SysDetail(Caption, DetailSchemaUId, EntitySchemaUId)
VALUES(@DetailCaption,
     (SELECT TOP 1 UId
      from SysSchema
      WHERE Name = @ClientUnitSchemaName),
      (SELECT TOP 1 UId
      from SysSchema
      WHERE Name = @EntitySchemaName))