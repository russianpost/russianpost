-- RPCRM-968 Автор: Менякин Алексей. Изменение названия колонки, в которую записывается значение,
-- отражающее степень заполненности карточки договора.
update Completeness
SET ResultColumnName = 'ITCompleteness'
WHERE EntitySchemaName = 'Contract'