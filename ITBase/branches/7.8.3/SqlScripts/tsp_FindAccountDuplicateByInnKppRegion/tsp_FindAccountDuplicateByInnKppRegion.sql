IF NOT OBJECT_ID('[dbo].[tsp_FindAccountDuplicateByKppInnRegion]') IS NULL
BEGIN
DROP PROCEDURE [dbo].[tsp_FindAccountDuplicateByKppInnRegion];
END;
GO
CREATE PROCEDURE [dbo].[tsp_FindAccountDuplicateByKppInnRegion] (
@parsedConfig CreatingObjectInfo READONLY,
@sysAdminUnit UNIQUEIDENTIFIER,
@ruleId UNIQUEIDENTIFIER
)
AS
BEGIN
DECLARE @parsedConfigRowsCount INT = (SELECT COUNT(*) FROM @parsedConfig);
	IF OBJECT_ID('tempdb..#searchAccount') IS NOT NULL
	BEGIN
		DROP TABLE #searchAccount
	END
	CREATE TABLE #searchAccount (
		[TsINN] NVARCHAR(128) COLLATE database_default,
		[TsKpp] NVARCHAR(128) COLLATE database_default,
		[TsTarifficatorRegionId] UNIQUEIDENTIFIER,
		[OwnershipId] UNIQUEIDENTIFIER,
		[SortDate] DATETIME
	);


	IF @parsedConfigRowsCount = 0
	BEGIN
		INSERT INTO #searchAccount ([TsINN], [TsKpp], [TsTarifficatorRegionId], [OwnershipId], [SortDate])
		SELECT 
			[TsINN],
			[TsKpp],
			[TsTarifficatorRegionId],
			[OwnershipId],
			MAX([ModifiedOn])
		 FROM
				[Account]
		WHERE 
		[TsINN] != '' AND
		[OwnershipId] IS NOT NULL AND
		(([TsKpp] != 0 AND [OwnershipId] != '350469C4-40F9-49C5-8298-7F8A38548050') OR
		([OwnershipId] = '350469C4-40F9-49C5-8298-7F8A38548050'))		
		GROUP BY [TsINN], [TsKpp], [TsTarifficatorRegionId], [OwnershipId]
		HAVING COUNT(*) > 1;		
	END;

	INSERT INTO [AccountDuplicateSearchResult] ([AccountId], [GroupId], [RuleId], [SysAdminUnitId])
    SELECT   
		[vr].Id,
        DENSE_RANK() OVER (ORDER BY [vr].[SortDate] DESC, [vr].[TsTarifficatorRegionId], [vr].[TsINN]),
        @ruleId RuleId,
        @sysAdminUnit
	FROM 
	(SELECT
	[v].[Id],
    [v].[TsINN],
    [r].[SortDate],
	[v].[TsKpp],
	[v].[TsTarifficatorRegionId]
        FROM [Account] [v], #searchAccount r
        WHERE 
		[v].[TsINN] = [r].[TsINN] AND
		[v].[TsTarifficatorRegionId] = [r].[TsTarifficatorRegionId] AND 
		[v].[TsKpp] = [r].[TsKpp] AND
		[v].[OwnershipId] = [r].[OwnershipId]
    GROUP BY [v].[TsINN], [r].[SortDate], [v].[Id], [v].[TsKpp], [v].[TsTarifficatorRegionId]
		) [vr];
	END;