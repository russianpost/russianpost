GO

  UPDATE [dbo].[DuplicatesRule]
  SET [IsActive] = 0
  WHERE ProcedureName = 'tsp_FindAccountDuplicateByName' OR 
  ProcedureName = 'tsp_FindAccountDuplicateByInnKppBik'OR
  ProcedureName = 'tsp_FindAccountDuplicateByKpp'OR
  ProcedureName = 'tsp_FindAccountDuplicateByNamePhoneSmsAndCity'OR
  ProcedureName = 'tsp_FindAccountDuplicateByNameEmailAndCity'OR
  ProcedureName = 'tsp_FindAccountDuplicateByWeb'OR
  ProcedureName = 'tsp_FindAccountDuplicateByNamePhoneAndSms'

  UPDATE [dbo].[DuplicatesRule]
  SET [IsActive] = 1
  WHERE ProcedureName = 'tsp_FindAccountDuplicateByKppInnRegion'
GO