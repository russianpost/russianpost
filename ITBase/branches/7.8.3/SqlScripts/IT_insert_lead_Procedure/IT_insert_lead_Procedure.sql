IF OBJECT_ID ( 'insert_lead', 'P' ) IS NOT NULL   
    DROP PROCEDURE insert_lead ;  
GO
Create Procedure insert_lead
(
@insertOwnerId uniqueidentifier,
@insertAccountId uniqueidentifier
)
as 
declare @newLeadId as uniqueidentifier
declare @leadName as nvarchar(250)
set @leadName = (select name from Account where id=@insertAccountId) +'. Направление доставки письменной корреспонденции'
if @newLeadId is null set @newLeadId=newid()
insert into Lead (
	id, 
	Notes,
	LeadName,
	Account,
	Contact, 
	FullJobTitle,
	BusinesPhone,
	MobilePhone,
	Email,
	Fax,
	Website,
	Zip,
	Address,
	DoNotUseEmail,
	DoNotUsePhone,
	DoNotUseFax,
	DoNotUseSMS,
	DoNotUseMail,
	Commentary,
	ProcessListeners,
	Dear,
	RemindToOwner,
	Budget,
	ShowDistributionPage,
	CountryStr,
	RegionStr,
	CityStr,
	SearchMobileNumber,
	SearchBusinessNumber,
	IdentificationPassed,
	StartLeadManagementProcess,
	BpmHref,
	BpmRef,
	Qualified,
	SaleParticipant,
	QualifiedPercent,
	SalePercent,
	SaleType,
	Score,
	QualificationPassed,
	ITLeadDescription,
	ITBankBookCount,
	ITLeadName,
	ITDisqualifyReasonDescription,
	--Принимаемые параметры
	OwnerId, 
	QualifiedAccountId,
	--Автоматические параметры
	QualifyStatusId, --Шаг Лида
	ITPriorityId, --Приоритет
	ITPotentialId, --Потенциал
	RegisterMethodId --Тип создания
)
values (
	@newLeadId,
	'',
	@leadName,
	'',--Account nvarchar
	'', --Contact nvarchar
	'', --FullJobTitle nvarchar
	'', --BusinesPhone
	'', --MobilePhone
	'', --Email
	'', --Fax
	'', --Website
	'', --Zip 
	'', --Address nvarchar
	0, --DoNotUseEmail
	0, --DoNotUsePhone
	0, --DoNotUseFax
	0, --DoNotUseSMS
	0, --DoNotUseMail
	'', --Commentary
	0, --ProcessListeners
	'', --Dear
	0, --RemindToOwner
	0, --Budget
	0, --ShowDistributionPage
	'', --CountryStr
	'', --RegionStr
	'', --CityStr
	'', --SearchMobileNumber
	'', --SearchBusinessNumber
	0, --IdentificationPassed
	0, --StartLeadManagementProcess
	'', --BpmHref
	'', --BpmRef
	'', --Qualified
	'', --SaleParticipant
	0, --QualifiedPercent,
	0, --SalePercent,
	'', --SaleType,
	0, --Score,
	0, --QualificationPassed,
	'', --ITLeadDescription,
	0, --ITBankBookCount,
	@leadName, --ITLeadName,
	'', --ITDisqualifyReasonDescription
	@insertOwnerId,
	@insertAccountId,
	'D790A45D-03FF-4DDB-9DEA-8087722C582C', --Квалификация
	'9268A76E-9ABF-4F9E-86F8-975337A62B91', --Приоритет: высокий
	'3B1D7F25-DBF4-4A1C-A0CA-3E5B69C6ABAD', --Потенциал: высокий
	'2F65913C-FF62-40FB-9D01-1A3E2E893E0E' --Тип создания: автоматически
);
insert into LeadProduct (
	id,
	ProcessListeners,
	LeadId,
	ProductId
) values (
	newid(),
	0,
	@newLeadId,
	'99ABD082-1CD4-4EAA-8D6F-5B8A48D0F470' --Нанесение оттиска франкировальной машины
);
insert into LeadProduct (
	id,
	ProcessListeners,
	LeadId,
	ProductId
) values (
	newid(),
	0,
	@newLeadId,
	'44A65A85-9B70-4794-93F4-E9DB1BEEF408' --Забор, выдача почты от клиента и ГСП
)
insert into LeadProduct (
	id,
	ProcessListeners,
	LeadId,
	ProductId
) values (
	newid(),
	0,
	@newLeadId,
	'67B4605E-AB01-44DB-932D-11686FCE069C' --Конвертование
)
insert into LeadProduct (
	id,
	ProcessListeners,
	LeadId,
	ProductId
) values (
	newid(),
	0,
	@newLeadId,
	'FFA643AC-7A3C-4B8B-8432-3BE9781B084C' --Составление списка ф. 103 на партионные почтовые отправления
)
insert into LeadProduct (
	id,
	ProcessListeners,
	LeadId,
	ProductId
) values (
	newid(),
	0,
	@newLeadId,
	'BA944C4A-97AC-4A8C-91BC-DFB058A648A4' --Прочие типографские работы
)
insert into LeadProduct (
	id,
	ProcessListeners,
	LeadId,
	ProductId
) values (
	newid(),
	0,
	@newLeadId,
	'9AD7CC07-D774-4704-9EDC-6F3591D2F908' --Доставка неконвертованных счетов
)
insert into LeadProduct (
	id,
	ProcessListeners,
	LeadId,
	ProductId
) values (
	newid(),
	0,
	@newLeadId,
	'B77676D5-A5D2-485F-AEED-83097A8BE77B' --копировально-множительные услуги
)
--Права на АУП ПочтББ
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
    values (@newLeadId, '98192BB9-7DAD-46A9-9DA7-A5B0B9AC846C', '0', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
    values (@newLeadId,  '98192BB9-7DAD-46A9-9DA7-A5B0B9AC846C', '1', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
    values (@newLeadId,'98192BB9-7DAD-46A9-9DA7-A5B0B9AC846C', '2', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
	--Права на АУП ПочтББ. Группа руководителей
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
    values (@newLeadId, 'C75BC05A-7724-4E68-9951-63CD4B063CFF', '0', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
    values (@newLeadId,  'C75BC05A-7724-4E68-9951-63CD4B063CFF', '1', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
    values (@newLeadId,'C75BC05A-7724-4E68-9951-63CD4B063CFF', '2', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
--Права на Руководители МРЦ в почтовом блоке
declare @mrzHeadId as uniqueidentifier
set @mrzHeadId = (select top 1 SysAdminUnitId from SysAccountRight SAR
						inner join SysAdminUnit SA on SA.Id=SAR.SysAdminUnitId
						where SA.name like '%ПочтББ. Группа руководителей' and SA.Name NOT like 'УФПС%' and SAR.RecordId=@insertAccountId)
if @mrzHeadId is not null 
Begin
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
	values (@newLeadId, @mrzHeadId, '0', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
	values (@newLeadId,  @mrzHeadId, '1', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
values (@newLeadId, @mrzHeadId, '2', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
end
--Права на Руководители УФПС в почтовом блоке
declare @ufpsHeadId as uniqueidentifier
set @ufpsHeadId = (select top 1 SysAdminUnitId from SysAccountRight SAR
						inner join SysAdminUnit SA on SA.Id=SAR.SysAdminUnitId
						where SA.name like '%ПочтББ. Группа руководителей' and SA.Name like 'УФПС%' and SAR.RecordId=@insertAccountId)
if @ufpsHeadId is not null 
Begin
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
	values (@newLeadId, @ufpsHeadId, '0', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
	values (@newLeadId,  @ufpsHeadId, '1', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
insert into SysLeadRight (RecordId, SysAdminUnitId, Operation, RightLevel, Position, SourceId)
values (@newLeadId, @ufpsHeadId, '2', '1', '0', 'F41E0268-E324-4228-9E9E-5CB7CC906398');
end