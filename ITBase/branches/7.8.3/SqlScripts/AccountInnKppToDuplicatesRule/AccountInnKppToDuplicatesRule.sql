-- Переменная, хранящая значение колонки UId схемы Account.
DECLARE @AccountUId UNIQUEIDENTIFIER;
-- Получает значение колонки UId схемы Account.
Set @AccountUId = (SELECT TOP 1 SysSchema.UId FROM SysSchema
WHERE SysSchema.Name = 'Account' AND SysSchema.ExtendParent = 0);

-- Добавляет новое правило ИНН в систему.
if NOT EXISTS (select ProcedureName from DuplicatesRule where ProcedureName like '%tsp_FindAccountDuplicateByInn%')
BEGIN
INSERT INTO DuplicatesRule ([IsActive], [ObjectId], [ProcedureName], [Name]) VALUES
(1, @AccountUId, 'tsp_FindAccountDuplicateByInn', 'Дубли контрагентов. ИНН');
END

-- Добавляет новое правило КПП в систему.
if NOT EXISTS (select ProcedureName from DuplicatesRule where ProcedureName like '%tsp_FindAccountDuplicateByKpp%')
BEGIN
INSERT INTO DuplicatesRule ([IsActive], [ObjectId], [ProcedureName], [Name]) VALUES
(1, @AccountUId, 'tsp_FindAccountDuplicateByKpp', 'Дубли контрагентов. КПП');
END