IF NOT OBJECT_ID('[dbo].[tsp_FindNewAccountDuplicateDelete]') IS NULL
BEGIN
DROP PROCEDURE [dbo].[tsp_FindNewAccountDuplicateDelete];
END;
GO
CREATE PROCEDURE [dbo].[tsp_FindNewAccountDuplicateDelete] (
@currentContactId NVARCHAR(128)
)
AS
BEGIN
	DELETE ITAccountNew
	WHERE ITCurrentContactIdId = @currentContactId
	DELETE ITSearchNewAccount
	WHERE ITCurrentContactId = @currentContactId
END;