Declare
@EntitySchemaName NCHAR(100) = 'OpportunityInStage'
INSERT INTO SysDetail(
    ProcessListeners,
    Caption,
    DetailSchemaUId,
    EntitySchemaUId
)
VALUES (
    0,
   'Стадии',
    '4042D07F-83C4-40B5-91A2-1954847BDC1C',
    (SELECT TOP 1 UId
    FROM SysSchema
    WHERE name = @EntitySchemaName)
)