IF NOT OBJECT_ID('[dbo].[tsp_FindNewAccountDuplicate]') IS NULL
BEGIN
DROP PROCEDURE [dbo].[tsp_FindNewAccountDuplicate];
END;
GO
CREATE PROCEDURE [dbo].[tsp_FindNewAccountDuplicate] (
@accountName NVARCHAR(128),
@primaryContact NVARCHAR(128),
@phone NVARCHAR(128),
@web NVARCHAR(128),
@ownership NVARCHAR(128),
@inn NVARCHAR(128),
@kpp INT,
@currentContactId NVARCHAR(128)
)
AS
BEGIN
DELETE ITSearchNewAccount
DECLARE @i int,
		@j int,
		@s NVARCHAR(128),
		@text NVARCHAR(128),
		@region UNIQUEIDENTIFIER
SET @region = (SELECT TsTarifficatorRegionId FROM Contact WHERE id = @currentContactId);
SET @i = 1;
SET @text = @accountName;
WHILE (@i > 0)
	BEGIN
	SET @j=PATINDEX('%[-1234567890_-+ ") (]%',@text)
	IF (@j > 0)
		BEGIN
		SET @s=SUBSTRING(@text,@i,@j-@i)
		SET @text=SUBSTRING(@text,@j+1, LEN(@text)-@i)
		END
	ELSE
		BEGIN
		SET @s = @text
		SET @i = 0 
		END
	IF(@s !='')
		BEGIN
		DECLARE @str VARCHAR (64);
		SET @str = '%' + RTRIM(@s) + '%'; 
		INSERT INTO ITSearchNewAccount ([ITAccountId], [ITName], [ITInn], [ITKpp], [ITCurrentContactId])
			SELECT id,
				Name,
				TsINN,
				TsKPP,
				@currentContactId
			FROM Account
			WHERE Name LIKE @str AND Id NOT IN (SELECT id FROM ITSearchNewAccount) AND
			TsTarifficatorRegionId = @region
		END
	END
IF(@inn != ' ')
	BEGIN
		DELETE ITSearchNewAccount
		WHERE ITInn != @inn
	END
IF(@kpp != 0)
	BEGIN
		DELETE ITSearchNewAccount
		WHERE ITKpp != @kpp
	END

-- Далее о заполнении таблицы нового контакта

IF(@currentContactId IN (SELECT ITCurrentContactIdId FROM ITAccountNew))
	BEGIN
	DELETE ITAccountNew WHERE ITCurrentContactIdId = @currentContactId
	END

INSERT INTO ITAccountNew (ITName, ITPhone, ITWeb, ITInn, ITKpp, ITCurrentContactIdId)
SELECT @accountName,
		@phone,
		@web,
		@inn,
		@kpp,
		@currentContactId
IF(@ownership != '00000000-0000-0000-0000-000000000000')
	BEGIN
		UPDATE ITAccountNew
		SET ITOwnershipId = @ownership
		WHERE ITCurrentContactIdId = @currentContactId
	END
IF(@primaryContact != '00000000-0000-0000-0000-000000000000')
	BEGIN
		UPDATE ITAccountNew
		SET ITPrimaryContactId = @primaryContact
		WHERE ITCurrentContactIdId = @currentContactId
	END
END;