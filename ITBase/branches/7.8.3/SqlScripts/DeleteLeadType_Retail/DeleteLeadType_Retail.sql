declare @LeadTypeId uniqueidentifier
set @LeadTypeId = 'FFED6C57-6DDC-47C0-9904-A01D31E13B5B'

delete from TsLeadStageHistory where TsLeadId in (select id from Lead where LeadTypeId = @LeadTypeId)
delete from Lead where LeadTypeId = @LeadTypeId
delete from Activity where OpportunityId in (select id from Opportunity where LeadTypeId = @LeadTypeId)
delete from Opportunity where LeadTypeId = @LeadTypeId
delete from TsOppStageInLeadType where TsLeadTypeId = @LeadTypeId
delete from ITContactLeadType where ITLeadTypeId = @LeadTypeId 
delete from ITAccountCateg where ITLeadTypeId = @LeadTypeId 
delete from TsAccountExecutiveRole where TsLeadTypeId = @LeadTypeId
delete from LeadType where id = @LeadTypeId 