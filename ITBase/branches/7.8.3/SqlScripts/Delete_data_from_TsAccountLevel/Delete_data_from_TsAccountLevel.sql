update [Account]
set [TsAccountLevelId] = Null
where [TsAccountLevelId] = (SELECT id FROM TsAccountLevel WHERE Name = 'Lapsed (Прекратившийся)' AND Description = '')
update [ITAccountCateg]
set [ITAccountLevelId] = Null
where [ITAccountLevelId] = (SELECT id FROM TsAccountLevel WHERE Name = 'Lapsed (Прекратившийся)' AND Description = '')
delete from [SysTsAccountLevelLcz]
where [Name] = 'Lapsed (Прекратившийся)' and [Description] = ''
delete from [TsAccountLevel]
where [Name] = 'Lapsed (Прекратившийся)' and [Description] = ''