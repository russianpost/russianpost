If exists (Select 1 from leadtype where (name ='Доставка счетов' or name = 'Блок печатного сервиса' or name = 'Блок печатных сервисов'))
Begin
	Update Lead
	Set LeadTypeId=(Select Id from leadtype where name ='Блок почтового бизнеса')
	where LeadTypeId in (Select Id from leadtype where (name ='Доставка счетов' or name = 'Блок печатного сервиса' or name = 'Блок печатных сервисов'))
	
	

	Update Opportunity
	Set LeadTypeId=(Select Id from leadtype where name ='Блок почтового бизнеса')
	where LeadTypeId in (Select Id from leadtype where (name ='Доставка счетов' or name = 'Блок печатного сервиса' or name = 'Блок печатных сервисов'))
	
	Update Contract
	Set TsLeadTypeId=(Select Id from leadtype where name ='Блок почтового бизнеса')
	where TsLeadTypeId in (Select Id from leadtype where (name ='Доставка счетов' or name = 'Блок печатного сервиса' or name = 'Блок печатных сервисов'))
	

	Update ITContactLeadType
	set ITLeadTypeId =( select id from LeadType where name = 'Блок почтового бизнеса')
	where ITLeadTypeId in ( select id from LeadType where (name = 'Доставка счетов' or name = 'Блок печатного сервиса' or name = 'Блок печатных сервисов'))
	
	update Contact 
	set TsLeadTypeId= null
	where TsLeadTypeId in ( select id from LeadType where (name = 'Доставка счетов' or name = 'Блок печатного сервиса' or name = 'Блок печатных сервисов'))

	delete from ITAccountCateg  where ITLeadTypeId in ( select id from LeadType where (name = 'Доставка счетов' or name = 'Блок печатного сервиса' or name = 'Блок печатных сервисов'))
	delete from TsOppStageInLeadType  where TsLeadTypeId in ( select id from LeadType where (name = 'Доставка счетов' or name = 'Блок печатного сервиса' or name = 'Блок печатных сервисов'))
	delete from TsAccountExecutiveRole  where TsLeadTypeId in ( select id from LeadType where (name = 'Доставка счетов' or name = 'Блок печатного сервиса' or name = 'Блок печатных сервисов'))
	delete from LeadType where (name = 'Доставка счетов' or name = 'Блок печатного сервиса' or name = 'Блок печатных сервисов')
	
	;with a as
(
select row_number() over (partition by ItContactId, ITLeadTypeId order by ItContactId) rn
from ITContactLeadType
)
delete from a where rn > 1
End