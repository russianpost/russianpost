define("ITJsGeneralClientInfo", [], function() {
	var typePTM = {
		PTM3: "bdfb445d-07b5-441d-a0a5-6a58c0588f89",
		PTM43: "7094c541-eadd-4e13-af3a-bc9d2223836a",
		PTM50: "4e7c52e1-8fa6-4a4e-a274-578977fb5f0a"
	};
	var clientProgram = {
		PrivateOffice: "922204de-0a4d-4621-9377-798368ea6d6b",
		API: "d4fd7929-4a24-4322-a702-be9c59a29da7",
		PartyMail: "94de8f69-c6a0-417d-a507-7c3d411826ed",
		OwnPO: "958e7e94-a797-4a1a-a5be-3b0a707a66bf",
		Other: "e138f177-5e8a-4c3a-8fcc-8581de72c6f4"
	};
	return {
		TypePTM: typePTM,
		ClientProgram: clientProgram
	};
});
