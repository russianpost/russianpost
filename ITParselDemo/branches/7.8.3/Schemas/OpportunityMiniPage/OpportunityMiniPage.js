define("OpportunityMiniPage", ["ProcessModuleUtilities"],
		function(ProcessModuleUtilities) {
			return {
				entitySchemaName: "Opportunity",

				attributes: {},
				messages: {},
				rules: {},

				methods: {
					onSaved: function() {
						this.callParent(arguments);
						var OppStage = this.get("Stage");
						if (OppStage) {
							var IsFinalStage = OppStage.End;
							if (this.isAddMode() && !IsFinalStage) {
								this.showConfirmationDialog(this.get("Resources.Strings.ConfirmRunOpportunityProcessMessage"),
								function(result) {
									if (result === Terrasoft.MessageBoxButtons.YES.returnCode) {
										ProcessModuleUtilities.executeProcess({
											sysProcessName: "ITITTsOpportunityManagement32",
											parameters: {
												CurrentOpportunity: this.get("Id")
											}
										});
									}
								}, ["yes", "no"]);
							}
						}
					}
				},

				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,

				diff: []
			};
		});
