/**Создано в рамках задачи https://it-bpm.atlassian.net/browse/RPCRM-1080**/

define("ITErrorsInfoPrePage", ["CustomProcessPageV2Utilities", "BusinessRuleModule"],
	function(CustomProcessPageV2Utilities, BusinessRuleModule) {
		return {
			entitySchemaName: "Opportunity",

			mixins: {
				BaseProcessViewModel: "Terrasoft.CustomProcessPageV2Utilities"
			},

			attributes: {
				"ITFormSize": {
					"onChange": "on7pChanged"
				},

				"ITNotAllFieldsFilled7": {
					"onChange": "on7pChanged"
				},

				"ITWithoutErrors7": {
					"onChange": "onITWithoutErrors7Changed"
				},

				"ITIncorrectPTM103p": {
					"onChange": "on103pChanged"
				},

				"ITWrongCalculating103p": {
					"onChange": "on103pChanged"
				},

				"ITWithoutErrors103p": {
					"onChange": "onITWithoutErrors103pChanged"
				},

				"ITIncorrectPTM103e": {
					"onChange": "on103eChanged"
				},

				"ITExtraSimbols": {
					"onChange": "on103eChanged"
				},

				"ITNotAllFieldsFilled103e": {
					"onChange": "on103eChanged"
				},

				"ITWrongCalculating103e": {
					"onChange": "on103eChanged"
				},

				"ITWithoutErrors103e": {
					"onChange": "onITWithoutErrors103eChanged"
				}
			},

			rules: {},

			methods: {
				//Возвращаем заголовок преднастроенной страницы
				getHeader: function() {
					return this.get("Resources.Strings.ITErrorsInfoPrePageCaption");
				},

				//Переопределение метода для прорисовки caption страницы
				getPageHeaderCaption: function() {
					var pageName = this.name;
					var header = pageName === "ITErrorsInfoPrePage" ? this.getHeader() : "";
					return header;
				},

				initHeaderCaption: Ext.emptyFn,

				initPrintButtonMenu: Ext.emptyFn,

				loadVocabulary: function(args, tag) {
					var column = this.getColumnByName(tag);
					args.schemaName = column.referenceSchemaName;
					this.callParent(arguments);
				},

				getLookupPageConfig: function(args, columnName) {
					var config = {
						entitySchemaName: args.schemaName,
						multiSelect: false,
						columnName: columnName,
						columnValue: this.get(columnName),
						searchValue: args.searchValue,
						filters: this.getLookupQueryFilters(columnName)
					};
					this.Ext.apply(config, this.getLookupListConfig(columnName));
					return config;
				},

				onEntityInitialized: function() {
					this.callParent(arguments);
					this.setDataOnPageInit();
				},

				//Заполнение формы из БД, если данные там уже были
				setDataOnPageInit: function() {
					var recordId = this.get("ITOpportunityId");
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Opportunity"
					});
					esq.addColumn("ITPackageMeetsRequirements");
					esq.addColumn("ITDepartmentFault");
					esq.addColumn("ITFormSize");
					esq.addColumn("ITNotAllFieldsFilled7");
					esq.addColumn("ITWithoutErrors7");
					esq.addColumn("ITIncorrectPTM103p");
					esq.addColumn("ITWrongCalculating103p");
					esq.addColumn("ITWithoutErrors103p");
					esq.addColumn("ITIncorrectPTM103e");
					esq.addColumn("ITExtraSimbols");
					esq.addColumn("ITNotAllFieldsFilled103e");
					esq.addColumn("ITWrongCalculating103e");
					esq.addColumn("ITWithoutErrors103e");
					esq.getEntity(recordId, function(response) {
						if (response.success && response.entity) {
							for (var key in response.entity.columns) {
								if (response.entity.columns[key] && key !== "Id") {
									this.set(key, response.entity.get(key));
								}
							}
						}
					}, this);
				},

				//Закрытие страницы с возвратом процесса к началу текущего этапа
				onCloseCardButtonClick: function() {
					this.saveColumnValues();
					this.sandbox.publish("BackHistoryState");
				},
				
				onNextButtonClick: function() {
					this.saveColumnValues();
					this.acceptProcessElement("NextButton");
					
				},

				//Сохранение введенных данных
				saveColumnValues: function() {
					var update = this.Ext.create("Terrasoft.UpdateQuery", {
						rootSchemaName: "Opportunity"
					});
					update.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Id", this.get("ITOpportunityId")));
					var array = ["ITPackageMeetsRequirements", "ITDepartmentFault", "ITFormSize",
								"ITNotAllFieldsFilled7", "ITWithoutErrors7", "ITIncorrectPTM103p",
								"ITWrongCalculating103p", "ITWithoutErrors103p", "ITIncorrectPTM103e",
								"ITExtraSimbols", "ITNotAllFieldsFilled103e", "ITWrongCalculating103e",
								"ITWithoutErrors103e"];
					
					array.forEach(function(item, i, array) {
						update.setParameterValue(item, this.get(item), this.Terrasoft.DataValueType.BOOLEAN);
					}, this);
					update.execute();
				},

				on7pChanged: function() {
					if ((this.get("ITFormSize") || this.get("ITNotAllFieldsFilled7")) &&
						this.get("ITWithoutErrors7")) {
						this.set("ITWithoutErrors7", false);
					}
				},

				onITWithoutErrors7Changed: function() {
					if (this.get("ITWithoutErrors7")) {
						if (this.get("ITFormSize")) {this.set("ITFormSize", false); }
						if (this.get("ITNotAllFieldsFilled7")) {this.set("ITNotAllFieldsFilled7", false); }
					}
				},

				on103pChanged: function() {
					if ((this.get("ITIncorrectPTM103p") || this.get("ITWrongCalculating103p")) &&
						this.get("ITWithoutErrors103p")) {
						this.set("ITWithoutErrors103p", false);
					}
				},

				onITWithoutErrors103pChanged: function() {
					if (this.get("ITWithoutErrors103p")) {
						if (this.get("ITIncorrectPTM103p")) {this.set("ITIncorrectPTM103p", false); }
						if (this.get("ITWrongCalculating103p")) {this.set("ITWrongCalculating103p", false); }
					}
				},

				on103eChanged: function() {
					if ((this.get("ITIncorrectPTM103e") || this.get("ITExtraSimbols") ||
						this.get("ITNotAllFieldsFilled103e") || this.get("ITWrongCalculating103e")) &&
						this.get("ITWithoutErrors103e")) {
						this.set("ITWithoutErrors103e", false);
					}
				},

				onITWithoutErrors103eChanged: function() {
					if (this.get("ITWithoutErrors103e")) {
						if (this.get("ITIncorrectPTM103e")) {this.set("ITIncorrectPTM103e", false); }
						if (this.get("ITExtraSimbols")) {this.set("ITExtraSimbols", false); }
						if (this.get("ITNotAllFieldsFilled103e")) {this.set("ITNotAllFieldsFilled103e", false); }
						if (this.get("ITWrongCalculating103e")) {this.set("ITWrongCalculating103e", false); }
					}
				}
			},

			diff: /**SCHEMA_DIFF*/[
				// Удалить лишние кнопки
	/*			{
					"operation": "remove",
					"name": "ActionButtonsContainer"
				},*/
				{
					"operation": "remove",
					"name": "DiscardChangesButton"
				},
	/*			{
					"operation": "remove",
					"name": "Tabs"
				},*/
				{
					"operation": "remove",
					"name": "DelayExecutionButton"
				},
				{
					"operation": "remove",
					"name": "ViewOptionsButton"
				},
				{
					"operation": "remove",
					"name": "SaveButton"
				},
				{
					"operation": "remove",
					"name": "actions"
				},

				{
					"operation": "merge",
					"name": "CloseButton",
					"values": {
						"style": Terrasoft.controls.ButtonEnums.style.RED,
						"click": {"bindTo": "onCloseCardButtonClick"},
						"visible": true
					}
				},

				{
					"operation": "insert",
					"parentName": "LeftContainer",
					"propertyName": "items",
					"name": "NextButton",
					"values": {
						"caption": {"bindTo": "Resources.Strings.SaveButtonCaption"},
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"classes": {textClass: "actions-button-margin-right"},
						"style": Terrasoft.controls.ButtonEnums.style.GREEN,
						"click": {bindTo: "onNextButtonClick"}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITPackageMeetsRequirements",
					"values": {
						"bindTo": "ITPackageMeetsRequirements",
						"layout": {column: 0, row: 0, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITDepartmentFault",
					"values": {
						"bindTo": "ITDepartmentFault",
						"layout": {column: 0, row: 1, colSpan: 8}
					}
				},

				//CONTROL_GROUP_form7p
				{
					"operation": "insert",
					"name": "form7p",
					"values": {
						"layout": {"column": 0, "row": 2, "colSpan": 24},
						"caption": {"bindTo": "Resources.Strings.form7pCaption"},
						"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
						"markerValue": "added-group",
						"items": []
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 0
				},

				//GRID_LAYOUT_form7p
				{
					"operation": "insert",
					"name": "form7pContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
						"markerValue": "added-group",
						"items": []
					},
					"parentName": "form7p",
					"propertyName": "items",
					"index": 0
				},

				//COLUMNS_form7p
				{
					"operation": "insert",
					"parentName": "form7pContainer",
					"propertyName": "items",
					"name": "ITFormSize",
					"values": {
						"bindTo": "ITFormSize",
						"layout": {column: 0, row: 0, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "form7pContainer",
					"propertyName": "items",
					"name": "ITNotAllFieldsFilled7",
					"values": {
						"bindTo": "ITNotAllFieldsFilled7",
						"layout": {column: 0, row: 1, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "form7pContainer",
					"propertyName": "items",
					"name": "ITWithoutErrors7",
					"values": {
						"bindTo": "ITWithoutErrors7",
						"layout": {column: 0, row: 2, colSpan: 8}
					}
				},

				//CONTROL_GROUP_form103p
				{
					"operation": "insert",
					"name": "form103p",
					"values": {
						"layout": {"column": 0, "row": 3, "colSpan": 24},
						"caption": {"bindTo": "Resources.Strings.form103pCaption"},
						"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
						"markerValue": "added-group",
						"items": []
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 1
				},

				//GRID_LAYOUT_form103p
				{
					"operation": "insert",
					"name": "form103pContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
						"markerValue": "added-group",
						"items": []
					},
					"parentName": "form103p",
					"propertyName": "items",
					"index": 0
				},

				//COLUMNS_form103p
				{
					"operation": "insert",
					"parentName": "form103pContainer",
					"propertyName": "items",
					"name": "ITIncorrectPTM103p",
					"values": {
						"bindTo": "ITIncorrectPTM103p",
						"layout": {column: 0, row: 0, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "form103pContainer",
					"propertyName": "items",
					"name": "ITWrongCalculating103p",
					"values": {
						"bindTo": "ITWrongCalculating103p",
						"layout": {column: 0, row: 1, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "form103pContainer",
					"propertyName": "items",
					"name": "ITWithoutErrors103p",
					"values": {
						"bindTo": "ITWithoutErrors103p",
						"layout": {column: 0, row: 2, colSpan: 8}
					}
				},

				//CONTROL_GROUP_form103e
				{
					"operation": "insert",
					"name": "form103e",
					"values": {
						"layout": {"column": 0, "row": 4, "colSpan": 24},
						"caption": {"bindTo": "Resources.Strings.form103eCaption"},
						"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
						"markerValue": "added-group",
						"items": []
					},
					"parentName": "Header",
					"propertyName": "items",
					"index": 2
				},

				//GRID_LAYOUT_form103e
				{
					"operation": "insert",
					"name": "form103eContainer",
					"values": {
						"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
						"markerValue": "added-group",
						"items": []
					},
					"parentName": "form103e",
					"propertyName": "items",
					"index": 0
				},

				//COLUMNS_form103e
				{
					"operation": "insert",
					"parentName": "form103eContainer",
					"propertyName": "items",
					"name": "ITIncorrectPTM103e",
					"values": {
						"bindTo": "ITIncorrectPTM103e",
						"layout": {column: 0, row: 0, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "form103eContainer",
					"propertyName": "items",
					"name": "ITExtraSimbols",
					"values": {
						"bindTo": "ITExtraSimbols",
						"layout": {column: 0, row: 1, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "form103eContainer",
					"propertyName": "items",
					"name": "ITNotAllFieldsFilled103e",
					"values": {
						"bindTo": "ITNotAllFieldsFilled103e",
						"layout": {column: 0, row: 2, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "form103eContainer",
					"propertyName": "items",
					"name": "ITWrongCalculating103e",
					"values": {
						"bindTo": "ITWrongCalculating103e",
						"layout": {column: 0, row: 3, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "form103eContainer",
					"propertyName": "items",
					"name": "ITWithoutErrors103e",
					"values": {
						"bindTo": "ITWithoutErrors103e",
						"layout": {column: 0, row: 4, colSpan: 8}
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});