/**Создано в рамках задачи https://it-bpm.atlassian.net/browse/RPCRM-1080**/

define("ITGeneralClientInfoPrePage", ["CustomProcessPageV2Utilities", "BusinessRuleModule", "ITJsGeneralClientInfo"],
	function(CustomProcessPageV2Utilities, BusinessRuleModule, ITJsGeneralClientInfo) {
		return {
			entitySchemaName: "Opportunity",

			mixins: {
				BaseProcessViewModel: "Terrasoft.CustomProcessPageV2Utilities"
			},

			attributes: {
				"ITClientProgramLookup": {
					dataValueType: Terrasoft.DataValueType.ENUM,
					lookupListConfig: {
						columns: ["ITOrderNumber"],
						orders: [
							{
								columnPath: "ITOrderNumber",
								orderDirection: Terrasoft.OrderDirection.ASC
							}
						]
					}
				},

				"ITPTMLookup": {
					dataValueType: Terrasoft.DataValueType.ENUM,
					lookupListConfig: {
						columns: ["ITOrderNumber"],
						orders: [
							{
								columnPath: "ITOrderNumber",
								orderDirection: Terrasoft.OrderDirection.ASC
							}
						]
					}
				}
			},

			rules: {
				"ITClientProgramLookup": {
					"ITClientProgramLookupRequire": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.REQUIRED,
						conditions: [{
							leftExpression: {
								type: BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								attribute: "ITClientProgramLookup"
							},
							comparisonType: this.Terrasoft.ComparisonType.IS_NULL
						}]
					}
				},

				"ITESPP": {
					"ITESPPRequire": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.REQUIRED,
						conditions: [{
							leftExpression: {
								type: BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								attribute: "ITESPP"
							},
							comparisonType: this.Terrasoft.ComparisonType.IS_NULL
						}]
					}
				},

				"ITDate": {
					"ITDateRequire": {
						ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
						property: BusinessRuleModule.enums.Property.REQUIRED,
						conditions: [{
							leftExpression: {
								type: BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								attribute: "ITDate"
							},
							comparisonType: this.Terrasoft.ComparisonType.IS_NULL
						}]
					}
				}
			},

			methods: {
				//Возвращаем заголовок преднастроенной страницы
				getHeader: function() {
					return this.get("Resources.Strings.ITGeneralClientInfoPrePageCaption");
				},

				//Переопределение метода для прорисовки caption страницы
				getPageHeaderCaption: function() {
					var pageName = this.name;
					var header = pageName === "ITGeneralClientInfoPrePage" ? this.getHeader() : "";
					return header;
				},

				initHeaderCaption: Ext.emptyFn,

				initPrintButtonMenu: Ext.emptyFn,

				loadVocabulary: function(args, tag) {
					var column = this.getColumnByName(tag);
					args.schemaName = column.referenceSchemaName;
					this.callParent(arguments);
				},

				getLookupPageConfig: function(args, columnName) {
					var config = {
						entitySchemaName: args.schemaName,
						multiSelect: false,
						columnName: columnName,
						columnValue: this.get(columnName),
						searchValue: args.searchValue,
						filters: this.getLookupQueryFilters(columnName)
					};
					this.Ext.apply(config, this.getLookupListConfig(columnName));
					return config;
				},

				onEntityInitialized: function() {
					this.callParent(arguments);
					this.setDataOnPageInit();
				},

				//Заполнение формы из БД, если данные там уже были
				setDataOnPageInit: function() {
					var recordId = this.get("ITOpportunityId");
					var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Opportunity"
					});
					esq.addColumn("ITClientProgramLookup");
					esq.addColumn("ITPTMLookup");
					esq.addColumn("ITComment");
					esq.addColumn("ITESPP");
					esq.addColumn("ITIsFormTested");
					esq.addColumn("ITDate");
					esq.getEntity(recordId, function(response) {
						if (response.success && response.entity) {
							for (var key in response.entity.columns) {
								if (response.entity.columns[key] && key !== "Id") {
									if (response.entity.get(key)) {
										this.set(key, response.entity.get(key));
									}
								}
							}
						}
					}, this);
				},

				//Закрытие страницы с возвратом процесса к началу текущего этапа
				onCloseCardButtonClick: function() {
					this.sandbox.publish("BackHistoryState");
				},
				
				onNextButtonClick: function() {
					var error = this.get("Resources.Strings.ErrorOnSave");
					if (this.allFieldsAreFilled()) {
						this.saveColumnValues();
						this.acceptProcessElement("NextButton");
					} else {
						this.showConfirmationDialog(error,
							function(returnCode) {
								if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
									this.saveColumnValues();
									this.onCloseCardButtonClick();
								} else if (returnCode === Terrasoft.MessageBoxButtons.CANCEL.returnCode) {
									return;
								}
							}, ["yes", "cancel"]);
					}
					
				},

				//Возвращает признак заполненности всех полей на странице
				allFieldsAreFilled: function() {
					var array = ["ITClientProgramLookup", "ITESPP", "ITDate"],
						isFlagTrue = true;
					array.forEach(function(item, i, array) {
						if (!this.get(item)) {
							isFlagTrue = false;
							return;
						}
					}, this);
					if (!isFlagTrue) {return false; }
					if ((this.isCommentVisible() && !this.get("ITComment")) ||
						(this.isPTMVisible() && !this.get("ITPTMLookup"))) {
						return false;
					} else {
						return true;
					}
				},

				//Сохранение введенных данных
				saveColumnValues: function() {
					var program = this.get("ITClientProgramLookup"),
						programValue = program && program.value ? program.value : null,
						ptm = this.get("ITPTMLookup"),
						ptmValue = ptm && ptm.value ? ptm.value : null,
						comment = this.get("ITComment"),
						espp = this.get("ITESPP"),
						formTested = this.get("ITIsFormTested"),
						date = this.get("ITDate");
					var update = this.Ext.create("Terrasoft.UpdateQuery", {
						rootSchemaName: "Opportunity"
					});
					update.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Id", this.get("ITOpportunityId")));
					update.setParameterValue("ITClientProgramLookup", programValue, this.Terrasoft.DataValueType.GUID);
					update.setParameterValue("ITPTMLookup", ptmValue, this.Terrasoft.DataValueType.GUID);
					update.setParameterValue("ITComment", comment, this.Terrasoft.DataValueType.TEXT);
					update.setParameterValue("ITESPP", espp, this.Terrasoft.DataValueType.TEXT);
					update.setParameterValue("ITIsFormTested", formTested, this.Terrasoft.DataValueType.BOOLEAN);
					update.setParameterValue("ITDate", date, this.Terrasoft.DataValueType.DATE);
					update.execute();
				},

				//Удаляем создание записи из всплывающей
				//подсказки справочных полей
				getNewListItemConfig: function() {
					return;
				},

				//Видимость поля "Комментарий"
				isCommentVisible: function() {
					var other = ITJsGeneralClientInfo.ClientProgram.Other,
						program = this.get("ITClientProgramLookup");
					if (program && program.value) {
						return program.value === other ? true : false;
					} else {
						return false;
					}
				},

				//Видимость поля "Какой PTM"
				isPTMVisible: function() {
					var other = ITJsGeneralClientInfo.ClientProgram.Other,
						partyMail = ITJsGeneralClientInfo.ClientProgram.PartyMail,
						ownPO = ITJsGeneralClientInfo.ClientProgram.OwnPO,
						program = this.get("ITClientProgramLookup"),
						array = [other, partyMail, ownPO];
					if (program && program.value) {
						return array.indexOf(program.value) !== -1 ? true : false;
					} else {
						return false;
					}
				}
			},

			diff: /**SCHEMA_DIFF*/[
				// Удалить лишние кнопки
	/*			{
					"operation": "remove",
					"name": "ActionButtonsContainer"
				},*/
				{
					"operation": "remove",
					"name": "DiscardChangesButton"
				},
	/*			{
					"operation": "remove",
					"name": "Tabs"
				},*/
				{
					"operation": "remove",
					"name": "DelayExecutionButton"
				},
				{
					"operation": "remove",
					"name": "ViewOptionsButton"
				},
				{
					"operation": "remove",
					"name": "SaveButton"
				},
				{
					"operation": "remove",
					"name": "actions"
				},

				{
					"operation": "merge",
					"name": "CloseButton",
					"values": {
						"style": Terrasoft.controls.ButtonEnums.style.RED,
						"click": {"bindTo": "onCloseCardButtonClick"},
						"visible": true
					}
				},

				{
					"operation": "insert",
					"parentName": "LeftContainer",
					"propertyName": "items",
					"name": "NextButton",
					"values": {
						"caption": {"bindTo": "Resources.Strings.SaveButtonCaption"},
						"itemType": Terrasoft.ViewItemType.BUTTON,
						"classes": {textClass: "actions-button-margin-right"},
						"style": Terrasoft.controls.ButtonEnums.style.GREEN,
						"click": {bindTo: "onNextButtonClick"}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITClientProgramLookup",
					"values": {
						"bindTo": "ITClientProgramLookup",
						"layout": {column: 0, row: 0, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITComment",
					"values": {
						"bindTo": "ITComment",
						"layout": {column: 0, row: 1, colSpan: 12},
						"isRequired": true,
						"visible": {bindTo: "isCommentVisible"},
						"contentType": Terrasoft.ContentType.LONG_TEXT
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITPTMLookup",
					"values": {
						"contentType": this.Terrasoft.ContentType.ENUM,
						"bindTo": "ITPTMLookup",
						"layout": {column: 0, row: 2, colSpan: 8},
						"isRequired": true,
						"visible": {bindTo: "isPTMVisible"}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITESPP",
					"values": {
						"bindTo": "ITESPP",
						"layout": {column: 0, row: 3, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITIsFormTested",
					"values": {
						"bindTo": "ITIsFormTested",
						"layout": {column: 0, row: 4, colSpan: 8}
					}
				},

				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "ITDate",
					"values": {
						"bindTo": "ITDate",
						"layout": {column: 0, row: 5, colSpan: 8}
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});