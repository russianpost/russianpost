-- Создано в рамках задачи: https://it-bpm.atlassian.net/browse/RPCRM-558
-- Данные были привязаны к пакету на вкладке "Данные", данный скрипт служил для
-- первичного создания раздела на стенде разработки
--UID ITDeliveryPoint 'EB16B2D3-D502-40F9-9750-06BB9E5EC74C'
/*declare @objectUid UNIQUEIDENTIFIER, @moduleEntityId UNIQUEIDENTIFIER, @sectionUid UNIQUEIDENTIFIER,
		@sectionCaption varchar(75), @objectName varchar(75), @sectionModule UNIQUEIDENTIFIER,
		@image UNIQUEIDENTIFIER, @pageUid UNIQUEIDENTIFIER;
set @objectUid = (select UId from SysSchema where Name = 'ITDeliveryPoint');
set @sectionUid = (select UId from SysSchema where Name = 'ITDeliveryPointSection');
set @pageUid = (select UId from SysSchema where Name = 'ITDeliveryPointPage');
set @sectionModule = (select UId from SysSchema where Name = 'SectionModuleV2');
set @image = '1C4588E9-73C2-45B3-9C0C-E2B5411C394B';

--SysModuleEntity (объект раздела)
if not exists (select 1 from SysModuleEntity where SysEntitySchemaUId = @objectUid)
BEGIN
insert into SysModuleEntity (SysEntitySchemaUId) values (@objectUid)
END;

--SysModuleEdit (страница редактирования раздела)
set @moduleEntityId = (select id from SysModuleEntity where SysEntitySchemaUId = @objectUid);
if not exists (select 1 from SysModuleEdit where SysModuleEntityId = @moduleEntityId)
BEGIN
insert into SysModuleEdit (SysModuleEntityId, UseModuleDetails, CardSchemaUId, ActionKindCaption,
							ActionKindName, PageCaption)
values (@moduleEntityId, 1, @pageUid, 'Добавить точку сдачи',
		'Delivery Point', 'Точка сдачи')
END;

--SysModule (схема раздела)
set @moduleEntityId = (select id from SysModuleEntity where SysEntitySchemaUId = @objectUid);
set @sectionCaption = (select Caption from SysSchema where UId = @objectUid);
set @objectName = (select Name from SysSchema where UId = @objectUid);
if not exists (select 1 from SysModule where SysModuleEntityId = @moduleEntityId)
BEGIN
insert into SysModule (Caption, SysModuleEntityId, FolderModeId, GlobalSearchAvailable,
						Code, SectionModuleSchemaUId, SectionSchemaUId)
values (@sectionCaption, @moduleEntityId, 'B659D704-3955-E011-981F-00155D043204', '1',
		@objectName, @sectionModule, @sectionUid)
END;*/