//Создано для задачи https://it-bpm.atlassian.net/browse/RPCRM-1097
define("ITDeliveryPointPage", ["BusinessRuleModule", "ConfigurationConstants", "EmailHelper",
							"ITJsConsts"],
function(BusinessRuleModule, ConfigurationConstants, EmailHelper,
		ITJsConsts) {
	return {
		entitySchemaName: "ITDeliveryPoint",

		messages: {},

		attributes: {
			"ITIndexNumber": {
				dependencies: [{
					columns: ["ITIndexNumber"],
					methodName: "onITIndexNumberChanged"
				}]
			},

			"ITIndex": {
				dependencies: [{
					columns: ["ITIndex"],
					methodName: "onITIndexChanged"
				}]
			},

			"IndexExists": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			}
		},

		rules: {
			"ITUFPS": {
				"FiltrationByUFPS": {
					"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
					"autocomplete": false,
					"baseAttributePatch": "ITOrgStructure",
					"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
					"type": BusinessRuleModule.enums.ValueType.CONSTANT,
					"value": ITJsConsts.OrgStructure.UFPS.toLowerCase()
				}
			}
		},

		deliveryRecordId: this.Ext.emptyString,

		methods: {
			onITIndexChanged: function() {
				var index = this.get("ITIndex"),
					array = [],
					recordId;
				var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "ITDeliveryPoint"
					});
				esq.addColumn("ITIndex");
				esq.filters.add("esqExistFilter", esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL,
						"ITIndex", index));
				esq.getEntityCollection(function(result) {
					if (result.success && result.collection.collection.items.length !== 0) {
						array = result.collection.collection.items;
						recordId = array[0].values.Id;
						this.deliveryRecordId = recordId;
						this.set("IndexExists", true);
						this.showIndexExistsMessage(recordId);
					} else {
						this.set("IndexExists", false);
					}
				}, this);
			},

			save: function() {
				var indexExists = this.get("IndexExists");
				if (indexExists) {
					this.showIndexExistsMessage(this.deliveryRecordId);
					return;
				} else {
					this.callParent();
				}
			},

			//Выводит сообщение о существующей Точки сдачи
			showIndexExistsMessage: function(recordId) {
				var msg = this.get("Resources.Strings.IndexExistsMessage"),
					url = "CardModuleV2/ITDeliveryPointPage/edit/" + recordId;
				this.showConfirmationDialog(msg,
					function(returnCode) {
						if (returnCode === Terrasoft.MessageBoxButtons.YES.returnCode) {
							this.sandbox.publish("PushHistoryState", {
								hash: url,
								silent: false
							});
						}
					}, ["yes", "cancel"]);
			},

			//Формирует название Точки сдачи из индекса
			onITIndexNumberChanged: function() {
				var index = this.get("ITIndexNumber"),
					length,
					string = "",
					stringIndex = "";
				if (!index) {return; }
				string = index.toString();
				if (string.length >= 4) {
					length = string.length;
					
					for (var i = 0; i < length; i++) {
						stringIndex += string[i];
						if (i === 2) {stringIndex += " "; }
					}
					this.set("ITIndex", stringIndex);
				} else {
					this.set("ITIndex", string);
				}
			},

			setValidationConfig: function() {
				this.callParent(arguments);
				this.addColumnValidator("ITIndexNumber", this.zipValidator);
				this.addColumnValidator("ITEmail", EmailHelper.getEmailValidator);
			},

			zipValidator: function() {
				var invalidMessage = "",
					index = this.get("ITIndexNumber");
				if (index && index.toString().length) {
					invalidMessage = index.toString().length === 6 ?
						invalidMessage : this.get("Resources.Strings.zipValidatorMessage");
				}
				return {
					fullInvalidMessage: invalidMessage,
					invalidMessage: invalidMessage
				};
			}
		},

		details: /**SCHEMA_DETAILS*/{
			"ITDeliveryPointClientsDetail": {
				"schemaName": "ITDeliveryPointClientsDetail",
				"entitySchemaName": "ITDeliveryPointClients",
				"filter": {
					"detailColumn": "ITDeliveryPoint",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,

		diff: /**SCHEMA_DIFF*/[
			//Техническое поле, в которое заполняется название Точки сдачи
			/*{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ITIndex",
				"values": {
					bindTo: "ITIndex",
					layout: { column: 0, row: 30, colSpan: 10 },
					visible: false
				}
			}*/
			{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ITUFPS",
				"values": {
					bindTo: "ITUFPS",
					layout: { column: 0, row: 0, colSpan: 10 }
				}
			},

			{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ITIndexNumber",
				"values": {
					bindTo: "ITIndexNumber",
					layout: { column: 0, row: 1, colSpan: 10 },
					"tip": {
						"content": {"bindTo": "Resources.Strings.zipValidatorMessage"}
					}
				}
			},

			{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ITAddress",
				"values": {
					bindTo: "ITAddress",
					layout: { column: 0, row: 2, colSpan: 10 }
				}
			},

			{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ITContactName",
				"values": {
					bindTo: "ITContactName",
					layout: { column: 0, row: 3, colSpan: 10 }
				}
			},

			{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ITPhone",
				"values": {
					bindTo: "ITPhone",
					layout: { column: 0, row: 4, colSpan: 10 }
				}
			},

			{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ITEmail",
				"values": {
					bindTo: "ITEmail",
					layout: { column: 0, row: 5, colSpan: 10 }
				}
			},

			{
				"operation": "insert",
				"name": "GeneralInfoTab",
				"parentName": "Tabs",
				"propertyName": "tabs",
				"values": {
					"caption": "Клиенты",
					"items": []
				}
			},

			{
				"operation": "insert",
				"name": "ITDeliveryPointClientsDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 0
			},

			//Удалена кнопка Действия
			{
				"operation": "remove",
				"name": "actions"
			}
		]/**SCHEMA_DIFF*/
	};
});