//Реализовано для задачи https://it-bpm.atlassian.net/browse/RPCRM-1097
define("ITDeliveryPointDetail", ["ConfigurationEnums", "ConfigurationGrid", "ConfigurationGridGenerator",
								"ConfigurationGridUtilities"],
function(ConfigurationEnums) {
	return {

		entitySchemaName: "ITDeliveryPointClients",

		messages: {
			"GetColumnsValuesFromContract": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.PUBLISH
			}
		},

		attributes: {
			"IsEditable": {
				dataValueType: Terrasoft.DataValueType.BOOLEAN,
				type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				value: true
			}
		},

		mixins: {
			ConfigurationGridUtilites: "Terrasoft.ConfigurationGridUtilities"
		},

		methods: {
			addToolsButtonMenuItems: function(toolsButtonMenu) {
				this.addRecordOperationsMenuItems(toolsButtonMenu);
				this.addGridOperationsMenuItems(toolsButtonMenu);
	//			Настройку детали отключаем, поскольку не зарегистрирована
	//			this.addDetailWizardMenuItems(toolsButtonMenu);
			},

			//Получает значение Клиента со страницы Договора
			getValuesFromContract: function(column) {
				return this.sandbox.publish("GetColumnsValuesFromContract", column, ["DeliveryPointDetail_key"]);
			},

			//Возвращает колонки, которые выбираются запросом.
			getGridDataColumns: function() {
				return {
					"Id": {path: "Id"},
					"ITDeliveryPoint": {path: "ITDeliveryPoint"}
				};
			},

			//Конфигурирует и отображает модальное окно справочника.
			openDocumentLookup: function() {
				//Конфигурационный объект
				var config = {
					// Название схемы объекта, записи которого будут отображены в справочнике.
					entitySchemaName: "ITDeliveryPoint",
					// Возможность множественного выбора.
					multiSelect: true,
					// Колонки, которые будут отображены в справочнике 
					columns: ["ITIndex", "ITUFPS", "ITAddress", "ITPhone",
							"ITContactName", "ITEmail"]
				};
				var contractId = this.get("MasterRecordId");
				if (this.Ext.isEmpty(contractId)) {
					return;
				}
				
				/**Уберем те Точки сдачи, которые уже добавлены в деталь**/
				var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: this.entitySchemaName
				});
				esq.addColumn("ITDeliveryPoint.Id", "ITDeliveryPoint");
				// Создание и добавление фильтров в коллекцию запроса.
				esq.filters.add("filterByContract", this.Terrasoft.createColumnFilterWithParameter(
					this.Terrasoft.ComparisonType.EQUAL, "ITContract", contractId));
				// Получение всей коллекции записей и отображение ее в модальном окне справочника.
				esq.getEntityCollection(function(result) {
					var existsDeliveryPointsCollection = [];
					if (result.success) {
						result.collection.each(function(item) {
							existsDeliveryPointsCollection.push(item.get("ITDeliveryPoint"));
						});
					}
					// Добавление фильтра в конфигурационный объект.
					if (existsDeliveryPointsCollection.length > 0) {
						var existsFilter = this.Terrasoft.createColumnInFilterWithParameters("Id",
							existsDeliveryPointsCollection);
						existsFilter.comparisonType = this.Terrasoft.ComparisonType.NOT_EQUAL;
						existsFilter.Name = "existsFilter";
						config.filters = existsFilter;
					}
					// Вызов модального окна справочника
					this.openLookup(config, this.addCallBack, this);
				}, this);
			},

			// Обработчик события сохранения страницы редактирования.
			//Справочник детали открывается после сохранения страницы, на которой находится
			onCardSaved: function() {
				this.openDocumentLookup();
			},

			//Открывает справочник точек сдачи в случае если страница редактирования заказа была ранее сохранена.
			addRecord: function() {
				var masterCardState = this.sandbox.publish("GetCardState", null, [this.sandbox.id]);
				var isNewRecord = (masterCardState.state === ConfigurationEnums.CardStateV2.ADD ||
				masterCardState.state === ConfigurationEnums.CardStateV2.COPY);
				if (isNewRecord === true) {
					var args = {
						isSilent: true,
						messageTags: [this.sandbox.id]
					};
					this.sandbox.publish("SaveRecord", args, [this.sandbox.id]);
					return;
				}
				if (!this.getValuesFromContract("isAccountExists")) {return; }
				this.openDocumentLookup();
			},

			// Добавление выбранных продуктов.
			addCallBack: function(args) {
				// Экземпляр класса пакетного запроса BatchQuery.
				var bq = this.Ext.create("Terrasoft.BatchQuery");
				var contractId = this.get("MasterRecordId");
				// Коллекция выбранных в справочнике документов.
				this.selectedRows = args.selectedRows.getItems();
				// Коллекция, передаваемая в запрос.
				this.selectedItems = [];
				// Копирование необходимых данных.
				this.selectedRows.forEach(function(item) {
					item.contractId = contractId;
					item.DocumentId = item.value;
					bq.add(this.getDocumentInsertQuery(item));
					this.selectedItems.push(item.value);
				}, this);
				// Выполнение пакетного запроса, если он не пустой.
				if (bq.queries.length) {
					this.showBodyMask.call(this);
					bq.execute(this.onDocumentInsert, this);
				}
			},

			//Возвращает запрос на добавление текущего объекта.
			getDocumentInsertQuery: function(item) {
				var insert = Ext.create("Terrasoft.InsertQuery", {
					rootSchemaName: this.entitySchemaName
				});
				insert.setParameterValue("ITContract", item.contractId, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("ITDeliveryPoint", item.Id, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("ITAccount", this.getValuesFromContract("Account"), this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("ITUFPS", item.ITUFPS.value, this.Terrasoft.DataValueType.GUID);
				insert.setParameterValue("ITAddress", item.ITAddress, this.Terrasoft.DataValueType.TEXT);
				insert.setParameterValue("ITPhone", item.ITPhone, this.Terrasoft.DataValueType.TEXT);
				insert.setParameterValue("ITEmail", item.ITEmail, this.Terrasoft.DataValueType.TEXT);
				insert.setParameterValue("ITContactName", item.ITContactName, this.Terrasoft.DataValueType.TEXT);
				return insert;
			},

			//Метод, вызываемый при добавлении записей в реестр детали.
			onDocumentInsert: function(response) {
				this.hideBodyMask.call(this);
				this.beforeLoadGridData();
				var filterCollection = [];
				response.queryResults.forEach(function(item) {
					filterCollection.push(item.id);
				});
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: this.entitySchemaName
				});
				this.initQueryColumns(esq);
				esq.filters.add("recordId", Terrasoft.createColumnInFilterWithParameters("Id", filterCollection));
				esq.getEntityCollection(function(response) {
					this.afterLoadGridData();
					if (response.success) {
						var responseCollection = response.collection;
						this.prepareResponseCollection(responseCollection);
						this.getGridData().loadAll(responseCollection);
					}
				}, this);
			},

			// Метод, вызываемый при удалении выбранных записей детали.
			deleteRecords: function() {
				var selectedRows = this.getSelectedItems();
				if (selectedRows.length > 0) {
					this.set("SelectedRows", selectedRows);
					this.callParent(arguments);
				}
			},

			// Скрыть пункт меню [Копировать].
			getCopyRecordMenuItem: Terrasoft.emptyFn,

			// Скрыть пункт меню [Изменить].
			getEditRecordMenuItem: Terrasoft.emptyFn,

			// Возвращает имя колонки по умолчанию для фильтра.
			getFilterDefaultColumnName: function() {
				return "ITDeliveryPoint";
			}
		},

		diff: /**SCHEMA_DIFF*/[

			{
				"operation": "merge",
				"name": "DataGrid",
				// Объект, свойства которого будут объединены со свойствами элемента схемы.
				"values": {
					"rowDataItemMarkerColumnName": "ITDeliveryPoint"
				}
			},

			{
				"operation": "merge",
				"name": "AddRecordButton",
				// Объект, свойства которого будут объединены со свойствами элемента схемы.
				"values": {
					"visible": {"bindTo": "getToolsVisible"}
				}
			}
		]/**SCHEMA_DIFF*/
	};
});