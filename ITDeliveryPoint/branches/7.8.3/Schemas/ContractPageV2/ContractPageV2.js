/**Для задачи https://it-bpm.atlassian.net/browse/RPCRM-1097**/
define("ContractPageV2", [],
function() {
	return {
		messages: {
			"GetColumnsValuesFromContract": {
				mode: Terrasoft.MessageMode.PTP,
				direction: Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},

		methods: {
			subscribeSandboxEvents: function() {
				this.callParent(arguments);
				//Для задачи https://it-bpm.atlassian.net/browse/RPCRM-1097
				this.sandbox.subscribe("GetColumnsValuesFromContract",
									this.getColumnValueFromContract, this, ["DeliveryPointDetail_key"]);
			},

			getColumnValueFromContract: function(columnName) {
				var column = this.get(columnName);
				if (columnName === "isAccountExists") {
					if (!this.get("Account")) {
						this.showInformationDialog(this.get("Resources.Strings.AccountNotExists"));
						return false;
					} else {return true; }
				} else {
					if (column && column.value) {
						return column.value;
					} else {return null; }
				}
			}
		},

		details: {
			//Для задачи https://it-bpm.atlassian.net/browse/RPCRM-1097
			"ITDeliveryPointDetail": {
				"schemaName": "ITDeliveryPointDetail",
				"entitySchemaName": "ITDeliveryPointClients",
				"filter": {
					"detailColumn": "ITContract",
					"masterColumn": "Id"
				}
			}
		},

		diff: /**SCHEMA_DIFF*/[
			//Для задачи https://it-bpm.atlassian.net/browse/RPCRM-1097
			{
				"operation": "insert",
				"name": "ITDeliveryPointDetail",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "GeneralInfoTab",
				"propertyName": "items",
				"index": 5
			}
		]/**SCHEMA_DIFF*/
	};
});