//Реализовано для задачи https://it-bpm.atlassian.net/browse/RPCRM-1097
define("ITDeliveryPointClientsDetail", [], function() {
	return {

		entitySchemaName: "ITDeliveryPointClients",

		methods: {
			addToolsButtonMenuItems: function(toolsButtonMenu) {
	//			Удаляем возможность манипуляции с записями, деталь для чтения
	//			this.addRecordOperationsMenuItems(toolsButtonMenu);
				this.addGridOperationsMenuItems(toolsButtonMenu);
	//			Настройку детали отключаем, поскольку не зарегистрирована
	//			this.addDetailWizardMenuItems(toolsButtonMenu);
			}
		},

		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
	};
});