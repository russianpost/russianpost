-- Создано в рамках задачи: https://it-bpm.atlassian.net/browse/RPCRM-558
-- Данные были привязаны к пакету на вкладке "Данные", данный скрипт служил для
-- первичного создания раздела на стенде разработки
--UID ITTarificatorObject '4C735F76-9229-491F-91BF-8B38CF031F4E'
/*declare @objectUid UNIQUEIDENTIFIER, @moduleEntityId UNIQUEIDENTIFIER, @sectionUid UNIQUEIDENTIFIER,
		@sectionCaption varchar(75), @objectName varchar(75), @sectionModule UNIQUEIDENTIFIER,
		@image UNIQUEIDENTIFIER;
set @objectUid = (select UId from SysSchema where Name = 'ITTarificator');
set @sectionUid = (select UId from SysSchema where Name = 'ITTarificatorPage');
set @sectionModule = (select UId from SysSchema where Name = 'SectionModuleV2');
set @image = '1C4588E9-73C2-45B3-9C0C-E2B5411C394B';

--SysModuleEntity
if not exists (select 1 from SysModuleEntity where SysEntitySchemaUId = @objectUid)
BEGIN
insert into SysModuleEntity (SysEntitySchemaUId) values (@objectUid)
END;*/

--SysModuleEdit (страница редактирования раздела)
/*set @moduleEntityId = (select id from SysModuleEntity where SysEntitySchemaUId = @objectUid);
if not exists (select 1 from SysModuleEdit where SysModuleEntityId = @moduleEntityId)
BEGIN
insert into SysModuleEdit (SysModuleEntityId, UseModuleDetails, CardSchemaUId, ActionKindCaption,
							ActionKindName, PageCaption)
values (@moduleEntityId, 1, @pageUid, 'Добавить',
		'ITTarificatorObjectPage', 'Расчет стоимости доставки')
END;*/
/*
--SysModule (страница раздела)
set @moduleEntityId = (select id from SysModuleEntity where SysEntitySchemaUId = @objectUid);
set @sectionCaption = (select Caption from SysSchema where UId = @objectUid);
set @objectName = (select Name from SysSchema where UId = @objectUid);
if not exists (select 1 from SysModule where SysModuleEntityId = @moduleEntityId)
BEGIN
insert into SysModule (Caption, SysModuleEntityId, FolderModeId, GlobalSearchAvailable,
						Code, SectionModuleSchemaUId, SectionSchemaUId, Image32Id)
values (@sectionCaption, @moduleEntityId, 'B659D704-3955-E011-981F-00155D043204', '1',
		@objectName, @sectionModule, @sectionUid, @image)
END;*/