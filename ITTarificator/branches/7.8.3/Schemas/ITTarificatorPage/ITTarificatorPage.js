define("ITTarificatorPage", ["BusinessRuleModule", "ITTarificatorPageResources", "ServiceHelper"],
function(BusinessRuleModule, resources, ServiceHelper) {
	return {
		entitySchemaName: "ITTarificator",
		
		messages: {
			"getCardInfo": {
				"direction": Terrasoft.MessageDirectionType.PUBLISH,
				"mode": Terrasoft.MessageMode.PTP
			}
		},

		attributes: {
			//Признак НЕтарифицируемости выбранного продукта
			"IsNotTarificatorProduct": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			//Признак тарифицируемости выбранного продукта
			"IsTarificatorProduct": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			"ITProduct": {
				"onChange": "onProductChanged",
				"lookupListConfig": {
					"columns": ["ITTariffCode"]
				}
			},

			"ITcountry": {
				"lookupListConfig": {
					"columns": ["ITCode"]
				}
			},

			"ITregion": {
				"lookupListConfig": {
					"columns": ["TsCode"]
				}
			},

			"ITAmount": {
				"onChange": "onAmountChanged"
			},

			"ITPricePerUnit": {
				"onChange": "countAmount"
			},

			"ITNumber": {
				"onChange": "countAmount"
			},

			/***************Атрибуты_Коллекции_Start*******************/
			"ObjectCollection": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"AmountObjectCollection": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"serviceCollection": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"isaviaCollection": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			/***************Атрибуты_Коллекции_END*******************/

			/***************Видимость_основных_полей_START***************/
			
			"ITweightVisibility": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			"ITweightallVisibility": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			"ITfromVisibility": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			"ITtoVisibility": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			"ITcountryVisibility": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			"ITregionVisibility": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			"ITdeliveryVisibility": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			"ITmonthVisibility": {
				"dataValueType": Terrasoft.DataValueType.COLLECTION,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},
			/***************Видимость_основных_полей_END***************/

			"serviceVisibility": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"value": false
			},

			/***************Атрибуты_SERVICE_START********************/
			"service0": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service1": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service2": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service3": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service4": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service5": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service6": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service7": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service8": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service9": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service10": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service11": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service12": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service13": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			"service14": {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			/***************Атрибуты_SERVICE_END********************/

			/***************Видимость_SERVICE_START********************/
			service0Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service1Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service2Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service3Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service4Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service5Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service6Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service7Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service8Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service9Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service10Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service11Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service12Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service13Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			service14Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},
			/***************Видимость_SERVICE_END********************/

			/***************Динамические_справочники_START***************/
			//Справочная колонка вариант пересылки
			ITisavia: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			ITisaviaVisibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"visible": false
			},

			ITisaviaCollection: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"isCollection": true,
				"value": this.Ext.create("Terrasoft.Collection")
			},

			ITpack: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			ITpackVisibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"visible": false
			},

			ITpackCollection: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"isCollection": true,
				"value": this.Ext.create("Terrasoft.Collection")
			},

			p1: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			p1Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"visible": false
			},

			p1Collection: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"isCollection": true,
				"value": this.Ext.create("Terrasoft.Collection")
			},

			p2: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			p2Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"visible": false
			},

			p2Collection: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"isCollection": true,
				"value": this.Ext.create("Terrasoft.Collection")
			},

			p3: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			p3Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"visible": false
			},

			p3Collection: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"isCollection": true,
				"value": this.Ext.create("Terrasoft.Collection")
			},

			p4: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			p4Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"visible": false
			},

			p4Collection: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"isCollection": true,
				"value": this.Ext.create("Terrasoft.Collection")
			},

			p5: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN
			},

			p5Visibility: {
				"dataValueType": Terrasoft.DataValueType.BOOLEAN,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"visible": false
			},

			p5Collection: {
				"dataValueType": Terrasoft.DataValueType.ENUM,
				"type": Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				"isCollection": true,
				"value": this.Ext.create("Terrasoft.Collection")
			}
			/***************Динамические_справочники_END***************/
		},

		rules: {
			"ITregion": {
				"FiltrationRegionWithCode": {
					"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
					"autocomplete": true,
					"autoClean": true,
					"baseAttributePatch": "TsCode",
					"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
					"type": BusinessRuleModule.enums.ValueType.CONSTANT,
					"value": 0
				}
			}
		},

		methods: {
			onEntityInitialized: function() {
				this.onServicesChange();
				this.callParent(arguments);
				this.setDataOnInit();
			},

			//Возвращаем заголовок преднастроенной страницы
			getHeader: function() {
				return this.get("Resources.Strings.TarificatorCaption");
			},

			//Переопределение метода для прорисовки caption страницы
			getPageHeaderCaption: function() {
				var pageName = this.name;
				var header = pageName === "ITTarificatorPage" ? this.getHeader() : "";
				return header;
			},

			//Переопределение базового метода, чтобы не
			//вылазило сообщение о несохраненных данных
			setNotBeDestroyedConfig: function() {
				return;
			},

			//Установка значений по умолчанию
			setDataOnInit: function() {
				//Вспомогательный объект для расчета сумм нетарифных продуктов 
				this.productObject = {};
				this.productObject.number = 1;
				this.productObject.pricePerUnit = 0;
				this.productObject.amount = 0;
				this.productObject.amountNDS = 0;
				this.set("ITNumber", 1);
				this.set("ITPricePerUnit", 0);
				this.set("ITdate", new Date());
				this.set("ITweight", 0);
				this.set("ITAmount", 0);
				this.set("ITAmountNDS", 0);
			},

			//Сбрасывает видимость колонок для тарификации
			columnsVisibilityFalse: function() {
				var array = ["ITcountry", "ITregion", "ITfrom", "ITto",
							"ITweight", "ITweightall", "ITsumoc", "ITsumnp", "ITsumgs",
							"ITsum", "ITmonth", "ITsize", "ITcount", "ITpack",
							"ITcountinpack", "ITdelivery", "ITdeliveryText", "ITisavia",
							"p1", "p2", "p3", "p4", "p5"];
				array.forEach(function(item, i, array) {
					this.set((item + "Visibility"), false);
				}, this);
			},

			//Сбрасывает параметры дополнительных услуг
			setServices: function() {
				this.set("serviceCollection", null);
				this.set("isaviaCollection", null);
				this.set("serviceVisibility", false);
				for (var i = 0; i < 15; i++) {
					this.set(("service" + i + "Enabled"), true);
					this.set(("service" + i + "Visibility"), false);
					this.set(("service" + i), false);
					this.set(("service" + i + "Caption"), "");
				}
			},

			//Сбрасывает видимость и названия параметров
			cleanParameters: function() {
				var pArray = ["p1", "p2", "p3", "p4", "p5"];
				pArray.forEach(function(item, i, pArray) {
					this.set("parametersVisibility", false);
					this.set((item + "Visibility"), false);
					this.set((item + "Caption"), "");
				}, this);
			},

			//Обнуляем значения колонок
			cleanColumns: function() {
				var array = ["ITcountry", "ITregion", "ITfrom", "ITto",
							"ITsumoc", "ITsumnp", "ITsumgs",
							"ITsum", "ITmonth", "ITsize", "ITcount", "ITpack",
							"ITcountinpack", "ITisavia", "ITdeliveryText",
							"p1", "p2", "p3", "p4", "p5"];
				array.forEach(function(item, i, array) {
					this.set(item, null);
				}, this);
				this.set("ITdelivery", false);
				this.set("ITweight", 0);
				this.set("ITweightall", 0);
			},

			cleanCollections: function() {
				this.set("ObjectCollection", null);
				this.set("AmountObjectCollection", null);
			},

			//Срабатывает при изменении значений атрибутов доп.услуг
			onServicesChange: function() {
				var leng = 15;
				for (var i = 0; i < leng; i++) {
					var atrName = "service" + i;
					this.on("change:" + atrName, this.onServicesChangeFunction, this);
				}
			},

			//Метод обработчик onServicesChange
			onServicesChangeFunction: function(model) {
				var i,
					services = this.get("serviceCollection"),
					arrOf = [];
				if (!services) {return; }
				services.forEach(function(item0, y, services) {
					var txt = "service" + y;
					for (var key in model.changed) {
						if (model && model.changed[key]) {
							if (key === txt) {
								i = y;
								if (services[i].off) {
									arrOf = services[i].off;
									arrOf.forEach(function(item, j, arrOf) {
										services.forEach(function(item1, z, arr) {
											if (item1.id === item) {
												this.set(("service" + z), false);
											}
										}, this);
									}, this);
								}
							}
						} else {return; }
					}
				}, this);
				/*for (var y = 0; y < services.length; y++) {
					var txt = "service" + y;
					for (var key in model.changed) {
						if (model && model.changed[key]) {
							if (key === txt) {
								i = y;
								if (services[i].off) {
									arrOf = services[i].off;
									arrOf.forEach(function(item, j, arrOf) {
										services.forEach(function(item1, z, arr) {
											if (item1.id === item) {
												this.set(("service" + z), false);
											}
										}, this);
									}, this);
								}
							}
						} else {return; }
					}
				}*/
			},

			onProductChanged: function() {
				var product = this.get("ITProduct");
				if (product) {
					//При смене услуги (и если поле не пустое) очистка полей
					this.onProductChangedClear();
					if (product.ITTariffCode) {
						this.onTarificatorProduct(product.ITTariffCode);
					} else {
						//Методы для расчета нетарифных услуг
						this.onNotTarificatorProduct();
					}
				} else {
					//Методы обнуления полей
					this.console.log("Услуга не выбрана");
				}
			},

			onProductChangedClear: function() {
				this.setDataOnInit();
				this.columnsVisibilityFalse();
				this.setServices();
				this.cleanParameters();
				this.cleanColumns();
				this.cleanCollections();
			},

			//Если выбранная услуга тарифицируется
			onTarificatorProduct: function(productCode) {
				this.set("IsTarificatorProduct", true);
				this.set("IsNotTarificatorProduct", false);
				this.getObjectFromService(productCode);
			},

			//Получение сведений по объекту тарификации из сервиса ITTarificatorService
			getObjectFromService: function(productCode) {
				var serviceData = {
					tarifObject: productCode
				};
				ServiceHelper.callService("ITTarificatorService", "GetObjectFromService",
					function(response) {
						var result = response.GetObjectFromServiceResult;
						if (result === "") {
							this.showInformationDialog(this.get("Resources.Strings.TarificatorError"));
						} else {
							this.set("ObjectCollection", JSON.parse(result));
							this.getObjectParams();
						}
					}, serviceData, this);
			},

			getDateParam: function() {
				var date = this.get("ITdate"),
					dateString = "&date=",
					month = "",
					day = "";
				if (!date) {
					date = new Date();
					month += date.getMonth() + 1;
					month = month.length === 1 ? ("0" + month) : month;
					day += date.getDate();
					day = day.length === 1 ? ("0" + day) : day;
					dateString += date.getFullYear() + month + day;
					return ("&date=" + dateString);
				} else {
					month += date.getMonth() + 1;
					month = month.length === 1 ? ("0" + month) : month;
					day += date.getDate();
					day = day.length === 1 ? ("0" + day) : day;
					dateString += date.getFullYear() + month + day;
					return ("&date=" + dateString);
				}
			},

			getServiceParam: function() {
				var service = "&service=",
					array = this.get("serviceCollection");
				if (array) {
					array.forEach(function(item, i, array) {
						if (this.get("service" + i)) {
							if (array.length - 1 === i) {
								service += item.id;
							} else {
								service += item.id + ",";
							}
						}
					}, this);
					return service === "&service=" ? "" : service;
				} else {
					return "";
				}
			},

			getParametersParam: function() {
				var obj = this.get("ObjectCollection"),
					arr = obj.object[0].params,
					pArray = ["p1", "p2", "p3", "p4", "p5"],
					string = "";
				arr.forEach(function(item, i, arr) {
					if (pArray.indexOf(item.param) !== -1) {
						if (this.get(item.param)) {
							string += "&" + item.param + "=" + this.get(item.param).value;
						}
					}
				}, this);
				return string;
			},

			//Подготавливаются параметры для передачи и дальнейшего расчета стоимости по тарифу
			prepareObjectParameters: function() {
				var object, date, weight, country, region, from, to,
					sumoc, sumnp, sumgs, sum, month, size, count, pack,
					countinpack, delivery, isavia, weightall, service, params,
					paramString = "",
					paramArray = [
						object = "&object=" + (this.get("ITProduct").ITTariffCode || ""),
						date = this.getDateParam(),
						weight = this.get("ITweight") ? "&weight=" + this.get("ITweight") : "",
						country = this.get("ITcountry") ? "&country=" + this.get("ITcountry").ITCode : "",
						region = this.get("ITregion") ? "&region=" + this.get("ITregion").TsCode : "",
						from = this.get("ITfrom") ? "&from=" + this.get("ITfrom") : "",
						to = this.get("ITto") ? "&to=" + this.get("ITto") : "",
						sumoc = this.get("ITsumoc") ? "&sumoc=" + this.get("ITsumoc") : "",
						sumnp = this.get("ITsumnp") ? "&sumnp=" + this.get("ITsumnp") : "",
						sumgs = this.get("ITsumgs") ? "&sumgs=" + this.get("ITsumgs") : "",
						sum = this.get("ITsum") ? "&sum=" + this.get("ITsum") : "",
						month = this.get("ITmonth") ? "&month=" + this.get("ITmonth") : "",
						size = this.get("ITsize") ? "&size=" + this.get("ITsize") : "",
						count = this.get("ITcount") ? "&count=" + this.get("ITcount") : "",
						pack = this.get("ITpack") ? "&pack=" + this.get("ITpack").value : "",
						countinpack = this.get("ITcountinpack") ? "&countinpack=" + this.get("ITcountinpack") : "",
						delivery = this.get("ITdelivery") ? "&delivery=" + this.get("ITdelivery") : "",
						isavia = this.get("ITisavia") ? "&isavia=" + this.get("ITisavia").value : "",
						weightall = this.get("ITweightall") ? "&weightall=" + this.get("ITweightall") : "",
						service = this.getServiceParam(),
						params = this.getParametersParam()
					];
				paramArray.forEach(function(item, i, paramArray) {
					paramString += item;
				}, this);
				this.getAmountFromService(paramString);
			},

			//Получение расчета стоимости по объекту тарификации из сервиса ITTarificatorService
			getAmountFromService: function(parameters) {
				var serviceData = {
					parameters: parameters
				};
				ServiceHelper.callService("ITTarificatorService", "GetAmountFromService",
					function(response) {
						var result = response.GetAmountFromServiceResult;
						this.set("AmountObjectCollection", JSON.parse(result));
						this.setAmounts();
					}, serviceData, this);
			},

			//Установка полей "Итого" значениями из ответа тарификатора
			setAmounts: function() {
				var obj = this.get("AmountObjectCollection");
				if (!obj) {return; }
				if (obj.delivery) {
					var text = "от " + obj.delivery.min + " до " + obj.delivery.max + " дней";
					this.set("ITdeliveryText", text);
				}
				if (obj.error) {
					this.showInformationDialog("Ошибка: " + obj.error);
				} else if (obj.pay && obj.paynds) {
					this.set("ITAmount", obj.pay / 100 * this.get("ITNumber"));
					this.set("ITAmountNDS", obj.paynds / 100 * this.get("ITNumber"));
				}
			},

			//Если выбранная услуга НЕ тарифицируется
			onNotTarificatorProduct: function() {
				this.set("IsTarificatorProduct", false);
				this.set("IsNotTarificatorProduct", true);
			},

			//Валидация полей
			setValidationConfig: function() {
				this.addColumnValidator("ITNumber", this.lessThanOneVadlidator);
				this.addColumnValidator("ITPricePerUnit", this.lessThanNullVadlidator);
				this.addColumnValidator("ITweight", this.lessThanNullVadlidator);
				this.addColumnValidator("ITAmount", this.lessThanNullVadlidator);
				this.addColumnValidator("ITfrom", this.zipValidator);
				this.addColumnValidator("ITto", this.zipValidator);
			},

			//Поле не может быть меньше 1
			lessThanOneVadlidator: function() {
				var invalidMessage = "",
					column = this.get(arguments[1].name);
				invalidMessage = (this.Ext.isEmpty(column) || column < 1) ?
					this.get("Resources.Strings.LessThanOneVadlidator") : invalidMessage;
				return {
					fullInvalidMessage: invalidMessage,
					invalidMessage: invalidMessage
				};
			},

			//Поле не может быть меньше 0
			lessThanNullVadlidator: function() {
				var invalidMessage = "",
					column = this.get(arguments[1].name);
				invalidMessage = (this.Ext.isEmpty(column) || column < 0) ?
					this.get("Resources.Strings.LessThanNullValidator") : invalidMessage;
				return {
					fullInvalidMessage: invalidMessage,
					invalidMessage: invalidMessage
				};
			},

			//Индекс должен содержать 6 символов
			zipValidator: function() {
				var invalidMessage = "",
					column = this.get(arguments[1].name);
				if (!column && column !== 0) {
					return {
						fullInvalidMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
				}
				column = column.toString();
				invalidMessage = (column === "" || column.length === 6) ? invalidMessage :
					this.get("Resources.Strings.zipValidatorMessage");
				return {
					fullInvalidMessage: invalidMessage,
					invalidMessage: invalidMessage
				};
			},

			//Расчет поля Итого при изменении поля "Цена за единицу"
			countAmount: function() {
				var priceUnit = this.get("ITPricePerUnit"),
					number = this.get("ITNumber"),
					amount;
				if (priceUnit < 0 || number < 1) {return; }
				amount = priceUnit * number;
				this.currentAmount = priceUnit;
				this.set("ITAmount", amount);
				this.productObject.number = number;
				this.productObject.pricePerUnit = priceUnit;
				this.productObject.amount = amount;
			},

			//При изменении цены БЕЗ НДС
			onAmountChanged: function() {
				var amount = this.get("ITAmount"),
					amountNDS,
					number = this.get("ITNumber");
				if (this.get("IsNotTarificatorProduct")) {
					if (this.productObject.pricePerUnit === (amount / number)) {
						if (!this.Ext.isEmpty(amount)) {
							amountNDS = amount * 1.18;
							this.set("ITAmountNDS", amountNDS);
						} else {
							this.set("ITAmountNDS", 0);
						}
					} else {
						if (!this.Ext.isEmpty(amount)) {
							amountNDS = amount * 1.18;
							this.set("ITAmountNDS", amountNDS);
							this.set("ITPricePerUnit", amount / number);
						} else {
							this.set("ITAmountNDS", 0);
						}
					}
				} else {
					if (!this.Ext.isEmpty(amount)) {
						amountNDS = amount * 1.18;
						this.set("ITAmountNDS", amountNDS);
					} else {
						this.set("ITAmountNDS", 0);
					}
				}
				
			},

			//Получаем данные об объекте тарификации (услуги, доп. услуги, параметры)
			getObjectParams: function() {
				var obj = this.get("ObjectCollection"),
					it = "IT",
					services = [],
					isavia = [],
					servicesId = [],
					pArray = ["p1", "p2", "p3", "p4", "p5"];
				
	//			debugger;
				
				obj.object[0].params.forEach(function(item, i, arr) {
					if (item.param !== "service" && item.param !== "date" &&
							item.param !== "weight") {
						this.set((it + item.param + "Visibility"), true);
						this.set((it + item.param + "Caption"), item.name);
					}
					if (item.param === "service") {
						services = item.list;
						this.set("serviceVisibility", true);
						services.forEach(function(item1, j, services) {
							this.set(("service" + j + "Caption"), item1.name);
							this.set(("service" + j + "Visibility"), true);
							if (item1.serviceoff && item1.serviceoff.length > 0) {
								servicesId.push({id: item1.id, off: item1.serviceoff});
							} else {
								servicesId.push({id: item1.id});
							}
							if (item1.isavia && item1.isavia.length > 0) {
								isavia.push({id: item1.id, isavia: item1.isavia});
							}
						}, this);
						this.set("serviceCollection", servicesId);
						this.set("isaviaCollection", isavia);
					}
					if (pArray.indexOf(item.param) !== -1) {
						this.set("parametersVisibility", true);
						this.set((item.param + "Caption"), item.name);
						this.set((item.param + "Visibility"), true);
					}
					if (item.param === "weight") {
						this.set((it + item.param + "Visibility"), true);
						this.set((it + item.param + "Caption"), item.name + " (" + item.unit[0] + ")");
					}
				}, this);
				if (!this.Ext.isEmpty(isavia)) {
					//По умолчанию устанавливается значение "неземным путем"
					this.ITisaviaChange({value: 0, displayValue: "наземным путём"});
				}
			},

			/******************Наполнение_динамических справочников_START********************/

			//Метод определяет доступность доп. услуг в зависимости от варианта пересылки
			//Срабатывает с помощью свойства контролла
			ITisaviaChange: function(arg) {
				var isaviaArray = this.get("isaviaCollection"),
					services = this.get("serviceCollection");
				if (arg && arg.displayValue) {
					if (isaviaArray && services) {
						isaviaArray.forEach(function(item, i, isaviaArray) {
							if (item.isavia.indexOf(arg.value) === -1) {
								services.forEach(function(item1, j, services) {
									if (item1.id === item.id) {
										this.set(("service" + j + "Enabled"), false);
										this.set(("service" + j), false);
									}
								}, this);
							} else {
								services.forEach(function(item1, j, services) {
									if (item1.id === item.id) {
										this.set(("service" + j + "Enabled"), true);
									}
								}, this);
							}
						}, this);
						
					}
				}
			},

			ITisaviaPrepareList: function(filter, list) {
				var arr = [],
					arr1 = [],
					columns = {},
					obj = this.get("ObjectCollection");
				if (list === null) {
					return;
				}
				list.clear();
				
				arr = obj.object[0].params;
				arr.forEach(function(item, i, arr) {
					if (item.param === "isavia") {
						arr1 = item.list;
						arr1.forEach(function(item1, j, arr1) {
							columns[0 + j] = {
								value: item1.id,
								displayValue: item1.name
							};
						});
					} else {
						return;
					}
				});
				list.loadAll(columns);
			},

			ITpackChange: function() {
				return;
			},

			ITpackPrepareList: function(filter, list) {
				var arr = [],
					arr1 = [],
					columns = {},
					obj = this.get("ObjectCollection");
				if (list === null) {
					return;
				}
				list.clear();
				
				arr = obj.object[0].params;
				arr.forEach(function(item, i, arr) {
					if (item.param === "pack") {
						arr1 = item.list;
						arr1.forEach(function(item1, j, arr1) {
							columns[0 + j] = {
								value: item1.id,
								displayValue: item1.name
							};
						});
					} else {
						return;
					}
				});
				list.loadAll(columns);
			},

			p1Change: function() {
				return;
			},

			p2Change: function() {
				return;
			},

			p3Change: function() {
				return;
			},

			p4Change: function() {
				return;
			},

			p5Change: function() {
				return;
			},

			p1PrepareList: function(filter, list) {
				var arr = [],
					arr1 = [],
					columns = {},
					obj = this.get("ObjectCollection");
				if (list === null) {
					return;
				}
				list.clear();
				
				arr = obj.object[0].params;
				arr.forEach(function(item, i, arr) {
					if (item.param === "p1") {
						arr1 = item.list;
						arr1.forEach(function(item1, j, arr1) {
							columns[j] = {
								value: item1.id,
								displayValue: item1.name
							};
						});
					} else {
						return;
					}
				});
				list.loadAll(columns);
			},

			p2PrepareList: function(filter, list) {
				var arr = [],
					arr1 = [],
					columns = {},
					obj = this.get("ObjectCollection");
				if (list === null) {
					return;
				}
				list.clear();
				
				arr = obj.object[0].params;
				arr.forEach(function(item, i, arr) {
					if (item.param === "p2") {
						arr1 = item.list;
						arr1.forEach(function(item1, j, arr1) {
							columns[j] = {
								value: item1.id,
								displayValue: item1.name
							};
						});
					} else {
						return;
					}
				});
				list.loadAll(columns);
			},

			p3PrepareList: function(filter, list) {
				var arr = [],
					arr1 = [],
					columns = {},
					obj = this.get("ObjectCollection");
				if (list === null) {
					return;
				}
				list.clear();
				
				arr = obj.object[0].params;
				arr.forEach(function(item, i, arr) {
					if (item.param === "p3") {
						arr1 = item.list;
						arr1.forEach(function(item1, j, arr1) {
							columns[j] = {
								value: item1.id,
								displayValue: item1.name
							};
						});
					} else {
						return;
					}
				});
				list.loadAll(columns);
			},

			p4PrepareList: function(filter, list) {
				var arr = [],
					arr1 = [],
					columns = {},
					obj = this.get("ObjectCollection");
				if (list === null) {
					return;
				}
				list.clear();
				
				arr = obj.object[0].params;
				arr.forEach(function(item, i, arr) {
					if (item.param === "p4") {
						arr1 = item.list;
						arr1.forEach(function(item1, j, arr1) {
							columns[j] = {
								value: item1.id,
								displayValue: item1.name
							};
						});
					} else {
						return;
					}
				});
				list.loadAll(columns);
			},

			p5PrepareList: function(filter, list) {
				var arr = [],
					arr1 = [],
					columns = {},
					obj = this.get("ObjectCollection");
				if (list === null) {
					return;
				}
				list.clear();
				
				arr = obj.object[0].params;
				arr.forEach(function(item, i, arr) {
					if (item.param === "p5") {
						arr1 = item.list;
						arr1.forEach(function(item1, j, arr1) {
							columns[j] = {
								value: item1.id,
								displayValue: item1.name
							};
						});
					} else {
						return;
					}
				});
				list.loadAll(columns);
			},
			/******************Наполнение_динамических_справочников_END********************/

			getAmountEnabled: function() {
				var product = this.get("ITProduct");
				if (product && product.ITTariffCode) {
					return true;
				} else {
					return false;
				}
			},

			getDeliveryTextVisibility: function() {
				var delivery = this.get("ITdelivery");
				if (delivery) {
					return true;
				} else {
					this.set("ITdeliveryText", null);
				}
			}
			/******************Наполнение_динамических справочников_START********************/
		},

		diff: /**SCHEMA_DIFF*/[
			/*********TarificatorFields_Start*********/
			{
				"operation": "insert",
				"name": "ITProduct",
				"values": {
					"bindTo": "ITProduct",
					"layout": {"colSpan": 12, "column": 0, "row": 1},
					"isRequired": true
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITNumber",
				"values": {
					"bindTo": "ITNumber",
					"layout": {"colSpan": 5, "column": 12, "row": 1},
					"isRequired": true
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITPricePerUnit",
				"values": {
					"bindTo": "ITPricePerUnit",
					"layout": {"colSpan": 6, "column": 17, "row": 1},
					"isRequired": true,
					"visible": {bindTo: "IsNotTarificatorProduct"}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITdate",
				"values": {
					"bindTo": "ITdate",
					"layout": {"colSpan": 12, "column": 0, "row": 2},
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITdateTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITweight",
				"values": {
					"bindTo": "ITweight",
					"layout": {"colSpan": 12, "column": 0, "row": 3},
					"visible": {bindTo: "ITweightVisibility"},
					"caption": {bindTo: "ITweightCaption"},
					"isRequired": true
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITweightall",
				"values": {
					"bindTo": "ITweightall",
					"layout": {"colSpan": 12, "column": 12, "row": 3},
					"visible": {bindTo: "ITweightallVisibility"},
					"caption": {bindTo: "ITweightallCaption"},
					"isRequired": true
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITfrom",
				"values": {
					"bindTo": "ITfrom",
					"layout": {"colSpan": 12, "column": 0, "row": 4},
					"visible": {bindTo: "ITfromVisibility"},
					"caption": {bindTo: "ITfromCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITfromTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITto",
				"values": {
					"bindTo": "ITto",
					"layout": {"colSpan": 12, "column": 12, "row": 4},
					"visible": {bindTo: "ITtoVisibility"},
					"caption": {bindTo: "ITtoCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITtoTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ITisavia",
				"values": {
					"layout": {"column": 0, "colSpan": 12, "row": 5},
					"controlConfig": {
						"className": "Terrasoft.ComboBoxEdit",
						"list": {
							"bindTo": "ITisaviaCollection"
						},
						"change": {
							"bindTo": "ITisaviaChange"
						},
						"prepareList": {
							"bindTo": "ITisaviaPrepareList"
						}
					},
					"labelConfig": {},
					"visible" : {bindTo: "ITisaviaVisibility"},
					"caption": {bindTo: "ITisaviaCaption"},
					"enabled": true,
					"isRequired": true,
					"bindTo": "ITisavia",
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITisaviaTip"}
					}
				}
			},

			{
				"operation": "insert",
				"name": "ITcountry",
				"values": {
					"bindTo": "ITcountry",
					"layout": {"colSpan": 12, "column": 0, "row": 6},
					"visible": {bindTo: "ITcountryVisibility"},
					"caption": {bindTo: "ITcountryCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITcountryTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITregion",
				"values": {
					"bindTo": "ITregion",
					"layout": {"colSpan": 12, "column": 0, "row": 7},
					"visible": {bindTo: "ITregionVisibility"},
					"caption": {bindTo: "ITregionCaption"},
					"isRequired": true
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITmonth",
				"values": {
					"bindTo": "ITmonth",
					"layout": {"colSpan": 12, "column": 0, "row": 8},
					"visible": {bindTo: "ITmonthVisibility"},
					"caption": {bindTo: "ITmonthCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITmonthTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITsumoc",
				"values": {
					"bindTo": "ITsumoc",
					"layout": {"colSpan": 12, "column": 0, "row": 9},
					"visible": {bindTo: "ITsumocVisibility"},
					"caption": {bindTo: "ITsumocCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITsumocTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITsumnp",
				"values": {
					"bindTo": "ITsumnp",
					"layout": {"colSpan": 12, "column": 12, "row": 9},
					"visible": {bindTo: "ITsumnpVisibility"},
					"caption": {bindTo: "ITsumnpCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITsumnpTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITsumgs",
				"values": {
					"bindTo": "ITsumgs",
					"layout": {"colSpan": 12, "column": 0, "row": 10},
					"visible": {bindTo: "ITsumgsVisibility"},
					"caption": {bindTo: "ITsumgsCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITsumgsTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITsum",
				"values": {
					"bindTo": "ITsum",
					"layout": {"colSpan": 12, "column": 12, "row": 10},
					"visible": {bindTo: "ITsumVisibility"},
					"caption": {bindTo: "ITsumCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITsumTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITsize",
				"values": {
					"bindTo": "ITsize",
					"layout": {"colSpan": 12, "column": 0, "row": 11},
					"visible": {bindTo: "ITsizeVisibility"},
					"caption": {bindTo: "ITsizeCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITsizeTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "ITpack",
				"values": {
					"bindTo": "ITpack",
					"layout": {"colSpan": 12, "column": 0, "row": 12},
					"controlConfig": {
						"className": "Terrasoft.ComboBoxEdit",
						"list": {
							"bindTo": "ITpackCollection"
						},
						"change": {
							"bindTo": "ITpackChange"
						},
						"prepareList": {
							"bindTo": "ITpackPrepareList"
						}
					},
					"labelConfig": {},
					"visible" : {bindTo: "ITpackVisibility"},
					"caption": {bindTo: "ITpackCaption"},
					"enabled": true,
					"isRequired": true
				}
			},

			{
				"operation": "insert",
				"name": "ITcount",
				"values": {
					"bindTo": "ITcount",
					"layout": {"colSpan": 12, "column": 0, "row": 13},
					"visible": {bindTo: "ITcountVisibility"},
					"caption": {bindTo: "ITcountCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITcountTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITcountinpack",
				"values": {
					"bindTo": "ITcountinpack",
					"layout": {"colSpan": 12, "column": 0, "row": 14},
					"visible": {bindTo: "ITcountinpackVisibility"},
					"caption": {bindTo: "ITcountinpackCaption"},
					"isRequired": true,
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITcountinpackTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITdelivery",
				"values": {
					"bindTo": "ITdelivery",
					"layout": {"colSpan": 12, "column": 0, "row": 15},
					"visible": {bindTo: "ITdeliveryVisibility"},
					"caption": {bindTo: "ITdeliveryCaption"},
					"tip": {
						"content": {"bindTo": "Resources.Strings.ITdeliveryTip"}
					}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			/************************SERVICES_START**************************/

			//CONTROL_GROUP
			{
				"operation": "insert",
				"name": "service",
				"values": {
					"layout": {"column": 0, "row": 25, "colSpan": 24},
					"caption": {"bindTo": "Resources.Strings.serviceCaption"},
					"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
					"markerValue": "added-group",
					"items": [],
					"visible" : {bindTo: "serviceVisibility"}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},

			//GRID_LAYOUT
			{
				"operation": "insert",
				"name": "serviceContainer",
				"values": {
					"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "service",
				"propertyName": "items",
				"index": 0
			},

			//COLUMNS
			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service0",
				"values": {
					"enabled": {bindTo: "service0Enabled"},
					"bindTo": "service0",
					"layout": {"colSpan": 12, "column": 0, "row": 0},
					"visible" : {bindTo: "service0Visibility"},
					"caption": {bindTo: "service0Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service1",
				"values": {
					"enabled": {bindTo: "service1Enabled"},
					"bindTo": "service1",
					"layout": {"colSpan": 12, "column": 12, "row": 0},
					"visible" : {bindTo: "service1Visibility"},
					"caption": {bindTo: "service1Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service2",
				"values": {
					"enabled": {bindTo: "service2Enabled"},
					"bindTo": "service2",
					"layout": {"colSpan": 12, "column": 0, "row": 1},
					"visible" : {bindTo: "service2Visibility"},
					"caption": {bindTo: "service2Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service3",
				"values": {
					"enabled": {bindTo: "service3Enabled"},
					"bindTo": "service3",
					"layout": {"colSpan": 12, "column": 12, "row": 1},
					"visible" : {bindTo: "service3Visibility"},
					"caption": {bindTo: "service3Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service4",
				"values": {
					"enabled": {bindTo: "service4Enabled"},
					"bindTo": "service4",
					"layout": {"colSpan": 12, "column": 0, "row": 2},
					"visible" : {bindTo: "service4Visibility"},
					"caption": {bindTo: "service4Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service5",
				"values": {
					"enabled": {bindTo: "service5Enabled"},
					"bindTo": "service5",
					"layout": {"colSpan": 12, "column": 12, "row": 2},
					"visible" : {bindTo: "service5Visibility"},
					"caption": {bindTo: "service5Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service6",
				"values": {
					"enabled": {bindTo: "service6Enabled"},
					"bindTo": "service6",
					"layout": {"colSpan": 12, "column": 0, "row": 3},
					"visible" : {bindTo: "service6Visibility"},
					"caption": {bindTo: "service6Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service7",
				"values": {
					"enabled": {bindTo: "service7Enabled"},
					"bindTo": "service7",
					"layout": {"colSpan": 12, "column": 12, "row": 3},
					"visible" : {bindTo: "service7Visibility"},
					"caption": {bindTo: "service7Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service8",
				"values": {
					"enabled": {bindTo: "service8Enabled"},
					"bindTo": "service8",
					"layout": {"colSpan": 12, "column": 0, "row": 4},
					"visible" : {bindTo: "service8Visibility"},
					"caption": {bindTo: "service8Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service9",
				"values": {
					"enabled": {bindTo: "service9Enabled"},
					"bindTo": "service9",
					"layout": {"colSpan": 12, "column": 12, "row": 4},
					"visible" : {bindTo: "service9Visibility"},
					"caption": {bindTo: "service9Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service10",
				"values": {
					"enabled": {bindTo: "service10Enabled"},
					"bindTo": "service10",
					"layout": {"colSpan": 12, "column": 0, "row": 5},
					"visible" : {bindTo: "service10Visibility"},
					"caption": {bindTo: "service10Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service11",
				"values": {
					"enabled": {bindTo: "service11Enabled"},
					"bindTo": "service11",
					"layout": {"colSpan": 12, "column": 12, "row": 5},
					"visible" : {bindTo: "service11Visibility"},
					"caption": {bindTo: "service11Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service12",
				"values": {
					"enabled": {bindTo: "service12Enabled"},
					"bindTo": "service12",
					"layout": {"colSpan": 12, "column": 0, "row": 6},
					"visible" : {bindTo: "service121Visibility"},
					"caption": {bindTo: "service12Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service13",
				"values": {
					"enabled": {bindTo: "service13Enabled"},
					"bindTo": "service13",
					"layout": {"colSpan": 12, "column": 12, "row": 6},
					"visible" : {bindTo: "service131Visibility"},
					"caption": {bindTo: "service13Caption"}
				}
			},

			{
				"operation": "insert",
				"parentName": "serviceContainer",
				"propertyName": "items",
				"name": "service14",
				"values": {
					"enabled": {bindTo: "service14Enabled"},
					"bindTo": "service14",
					"layout": {"colSpan": 12, "column": 0, "row": 7},
					"visible" : {bindTo: "service141Visibility"},
					"caption": {bindTo: "service14Caption"}
				}
			},
			/************************SERVICES_END**************************/

			/************************PARAMETERS_START**********************/

			//CONTROL_GROUP
			{
				"operation": "insert",
				"name": "parameters",
				"values": {
					"layout": {"column": 0, "row": 26, "colSpan": 24},
					"caption": {"bindTo": "Resources.Strings.parametersCaption"},
					"itemType": Terrasoft.ViewItemType.CONTROL_GROUP,
					"markerValue": "added-group",
					"items": [],
					"visible" : {bindTo: "parametersVisibility"}
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},

			//GRID_LAYOUT
			{
				"operation": "insert",
				"name": "parametersContainer",
				"values": {
					"itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
					"markerValue": "added-group",
					"items": []
				},
				"parentName": "parameters",
				"propertyName": "items",
				"index": 0
			},

			//COLUMNS
			{
				"operation": "insert",
				"parentName": "parametersContainer",
				"propertyName": "items",
				"name": "p1",
				"values": {
					"bindTo": "p1",
					"layout": {"colSpan": 12, "column": 0, "row": 0},
					"controlConfig": {
						"className": "Terrasoft.ComboBoxEdit",
						"list": {
							"bindTo": "p1Collection"
						},
						"change": {
							"bindTo": "p1Change"
						},
						"prepareList": {
							"bindTo": "p1PrepareList"
						}
					},
					"labelConfig": {},
					"visible" : {bindTo: "p1Visibility"},
					"caption": {bindTo: "p1Caption"},
					"enabled": true,
					"isRequired": true
				}
			},

			{
				"operation": "insert",
				"parentName": "parametersContainer",
				"propertyName": "items",
				"name": "p2",
				"values": {
					"bindTo": "p2",
					"layout": {"colSpan": 12, "column": 12, "row": 0},
					"controlConfig": {
						"className": "Terrasoft.ComboBoxEdit",
						"list": {
							"bindTo": "p2Collection"
						},
						"change": {
							"bindTo": "p2Change"
						},
						"prepareList": {
							"bindTo": "p2PrepareList"
						}
					},
					"labelConfig": {},
					"visible" : {bindTo: "p2Visibility"},
					"caption": {bindTo: "p2Caption"},
					"enabled": true,
					"isRequired": true
				}
			},

			{
				"operation": "insert",
				"parentName": "parametersContainer",
				"propertyName": "items",
				"name": "p3",
				"values": {
					"bindTo": "p3",
					"layout": {"colSpan": 12, "column": 0, "row": 1},
					"controlConfig": {
						"className": "Terrasoft.ComboBoxEdit",
						"list": {
							"bindTo": "p3Collection"
						},
						"change": {
							"bindTo": "p3Change"
						},
						"prepareList": {
							"bindTo": "p3PrepareList"
						}
					},
					"labelConfig": {},
					"visible" : {bindTo: "p3Visibility"},
					"caption": {bindTo: "p3Caption"},
					"enabled": true,
					"isRequired": true
				}
			},

			{
				"operation": "insert",
				"parentName": "parametersContainer",
				"propertyName": "items",
				"name": "p4",
				"values": {
					"bindTo": "p4",
					"layout": {"colSpan": 12, "column": 12, "row": 1},
					"controlConfig": {
						"className": "Terrasoft.ComboBoxEdit",
						"list": {
							"bindTo": "p4Collection"
						},
						"change": {
							"bindTo": "p4Change"
						},
						"prepareList": {
							"bindTo": "p4PrepareList"
						}
					},
					"labelConfig": {},
					"visible" : {bindTo: "p4Visibility"},
					"caption": {bindTo: "p4Caption"},
					"enabled": true,
					"isRequired": true
				}
			},

			{
				"operation": "insert",
				"parentName": "parametersContainer",
				"propertyName": "items",
				"name": "p5",
				"values": {
					"bindTo": "p5",
					"layout": {"colSpan": 12, "column": 0, "row": 2},
					"controlConfig": {
						"className": "Terrasoft.ComboBoxEdit",
						"list": {
							"bindTo": "p5Collection"
						},
						"change": {
							"bindTo": "p5Change"
						},
						"prepareList": {
							"bindTo": "p5PrepareList"
						}
					},
					"labelConfig": {},
					"visible" : {bindTo: "p5Visibility"},
					"caption": {bindTo: "p5Caption"},
					"enabled": true,
					"isRequired": true
				}
			},
			/************************PARAMETERS_END**************************/

			{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "GetAmount",
				"values": {
					"visible": {bindTo: "IsTarificatorProduct"},
					"style": Terrasoft.controls.ButtonEnums.style.GREEN,
					"itemType": Terrasoft.ViewItemType.BUTTON,
					"caption": {bindTo: "Resources.Strings.GetAmountCaption"},
					"click": {bindTo: "prepareObjectParameters"},
					"enabled": {bindTo: "getAmountEnabled"},
					"layout": {"colSpan": 2, "column": 10, "row": 40}
				}
			},

			{
				"operation": "insert",
				"name": "ITdeliveryText",
				"values": {
					"bindTo": "ITdeliveryText",
					"layout": {"colSpan": 10, "column": 0, "row": 41},
					"visible": {bindTo: "getDeliveryTextVisibility"},
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITAmount",
				"values": {
					"bindTo": "ITAmount",
					"layout": {"colSpan": 10, "column": 0, "row": 42}
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			{
				"operation": "insert",
				"name": "ITAmountNDS",
				"values": {
					"bindTo": "ITAmountNDS",
					"layout": {"colSpan": 10, "column": 0, "row": 43},
					"enabled": false
				},
				"parentName": "Header",
				"propertyName": "items"
			},

			/********TarificatorFields_End*********/

			{
				operation: "remove",
				name: "SaveButton"
			},

			{
				operation: "merge",
				name: "CloseButton",
				values: {
					"visible": true
				}
			},

			{
				operation: "remove",
				name: "DiscardChangesButton"
			},

			{
				operation: "remove",
				name: "ViewOptionsButton"
			},

			{
				operation: "remove",
				name: "RunProcessContainer"
			},

			{
				operation: "remove",
				name: "RightContainer"
			},

			/*{
				operation: "remove",
				name: "ActionButtonsContainer"
			},*/

			{
				operation: "remove",
				name: "actions"
			}
		]/**SCHEMA_DIFF*/
	};
});