namespace Terrasoft.Configuration.ITTarificatorServiceSpace
{
	using System;
	using System.ServiceModel;
	using System.ServiceModel.Web;
	using System.ServiceModel.Activation;
	using System.IO;
	using System.Web;
	using System.Net;
	using System.Linq;

	//Класс сервиса помечен обязательными атрибутами [ServiceContract] и 
	//[AspNetCompatibilityRequirements] с параметрами.
	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]

	public class ITTarificatorService
	{
		//Атрибуты для корректной работы запроса
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
		ResponseFormat = WebMessageFormat.Json)]

		public string GetObjectFromService(string tarifObject)
		{
			string result = "";
			string url = "http://sfa.tariff.russianpost.ru/tariff/v1/dictionary?json&object=";

			if (!IsExistsInArray())
			{
				url = "http://tariff.russianpost.ru/tariff/v1/dictionary?json&object=";
			}

			try
			{
				url += tarifObject;
				WebRequest wrq = WebRequest.Create(url);
	/*			WebProxy wp = new WebProxy("proxy.it.ru", 80);
				wp.Credentials = new NetworkCredential("", "");
				wrq.Proxy = wp;*/

				using (Stream GetObjectParams = wrq.GetResponse().GetResponseStream())
				{
					using (StreamReader ObjectReader = new StreamReader(GetObjectParams))
					{
						result = ObjectReader.ReadLine().ToString();
					}
				}
			}
			catch(Exception ex)
			{
				
			}
			return result;
		}

		//Атрибуты для корректной работы запроса
		[OperationContract]
		[WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
		ResponseFormat = WebMessageFormat.Json)]

		public string GetAmountFromService(string parameters)
		{
			string result = "";
			string url = "http://sfa.tariff.russianpost.ru/tariff/v1/calculate?json&errorcode=0";

			if (!IsExistsInArray())
			{
				url = "http://tariff.russianpost.ru/tariff/v1/calculate?json&errorcode=0";
			}
			try
			{
				url += parameters;
				WebRequest wrq = WebRequest.Create(url);
	/*			WebProxy wp = new WebProxy("proxy.it.ru", 80);
				wp.Credentials = new NetworkCredential("", "");
				wrq.Proxy = wp;*/
	
				using (Stream GetObjectAmount = wrq.GetResponse().GetResponseStream())
				{
					using (StreamReader reader = new StreamReader(GetObjectAmount))
					{
						result = reader.ReadToEnd().ToString();
					}
				}
			}
			catch(Exception ex)
			{
				
			}
			return result;
		}

		public bool IsExistsInArray()
		{
			string host = System.Net.Dns.GetHostName();
			string[] array = { "D01SFAAPP01", "D01SFAAPP02", "D01SFAAPP03", "D01SFAAPP04", "D01SFAAPP05" };
			return array.Contains(host);
		}
	}
	
}