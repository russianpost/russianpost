define("BasePageV2", [],
	function() {
		return {
			messages: {
				"GetMasterPageInfo": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.SUBSCRIBE
				}
			},
			methods: {
				subscribeDetailEvents: function(detailConfig, detailName) {
					this.callParent(arguments);
					var detailId = this.getDetailId(detailName);
					var detail = this.Terrasoft.deepClone(detailConfig);
					var sandbox = this.sandbox;
					sandbox.subscribe("GetMasterPageInfo", function() {
						return this.getMasterPageInfo(detail);
					}, this, [detailId]);
				},

				getMasterPageInfo: function(detail) {
					var masterColumnName = detail.filter && detail.filter.masterColumn;
					return Ext.apply({}, {
						"masterEntitySchema": this.entitySchema,
						"masterColumnName": masterColumnName
					}, detail);
				}
			}
		};
	}
);