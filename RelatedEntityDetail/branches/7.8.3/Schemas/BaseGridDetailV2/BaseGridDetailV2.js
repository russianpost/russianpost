define("BaseGridDetailV2", [],
	function() {
		return {
			messages: {
				"GetMasterPageInfo": {
					mode: Terrasoft.MessageMode.PTP,
					direction: Terrasoft.MessageDirectionType.PUBLISH
				}
			},
			methods: {
				addRecord: function() {
					var masterRecordId = this.get("MasterRecordId");
					if (!this.Ext.isEmpty(masterRecordId)) {
						this.callParent(arguments);
						return;
					}
					var masterPageInfo = this.sandbox.publish("GetMasterPageInfo", null, [this.sandbox.id]) || {};
					var message = this.get("Resources.Strings.DefaultMasterRecordIdEmptyErrorMessage");
					var masterEntitySchema = masterPageInfo.masterEntitySchema;
					var masterColumnName = masterPageInfo.masterColumnName;
					if (!this.Ext.isEmpty(masterEntitySchema) && !this.Ext.isEmpty(masterColumnName)) {
						var masterColumnCaption = masterEntitySchema.columns[masterColumnName].caption;
						if (!this.Ext.isEmpty(masterColumnCaption)) {
							var template = this.get("Resources.Strings.ParameterizedMasterRecordIdEmptyErrorMessage");
							message = this.Terrasoft.getFormattedString(template, masterColumnCaption);
						}
					}
					this.showInformationDialog(message);
				}
			}
		};
	}
);