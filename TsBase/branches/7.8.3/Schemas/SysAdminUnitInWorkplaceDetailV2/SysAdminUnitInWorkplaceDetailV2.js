define("SysAdminUnitInWorkplaceDetailV2", ["ConfigurationConstants"],
		function(ConfigurationConstants) {
			return {
				entitySchemaName: "SysAdminUnitInWorkplace",
				methods: {
					openSysAdminLookup: function() {
						var workplaceId = this.get("MasterRecordId");
						var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: "SysAdminUnitInWorkplace"
						});
						esq.addColumn("SysAdminUnit.Id", "SysAdminUnitId");
						esq.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
								this.Terrasoft.ComparisonType.EQUAL, "SysWorkplace", workplaceId));
						esq.getEntityCollection(function(result) {
							var existsCollection = [];
							if (result.success) {
								result.collection.each(function(item) {
									existsCollection.push(item.get("SysAdminUnitId"));
								}, this);
							}
							var config = {
								entitySchemaName: "SysAdminUnit",
								multiSelect: true,
								hideActions: true
							};
							var filterGroup = this.Terrasoft.createFilterGroup();
							if (existsCollection.length > 0) {
								var existsFilter =
										this.Terrasoft.createColumnInFilterWithParameters("Id", existsCollection);
								existsFilter.comparisonType = this.Terrasoft.ComparisonType.NOT_EQUAL;
								filterGroup.addItem(existsFilter);
							}
							var rolesFilter = this.Terrasoft.createColumnFilterWithParameter(
									this.Terrasoft.ComparisonType.EQUAL, "SysAdminUnitTypeValue", ConfigurationConstants.SysAdminUnit.Type.FuncRole);
							filterGroup.addItem(rolesFilter);
							config.filters = filterGroup;
							this.openLookup(config, this.addCallBack, this);
						}, this);
					}
				},
				diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
			};
		}
);
