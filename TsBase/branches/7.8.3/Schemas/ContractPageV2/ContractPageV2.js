define("ContractPageV2", ["ConfigurationConstants", "BusinessRuleModule", "LookupUtilities", "GeneralDetails"],
		function(ConfigurationConstants, BusinessRuleModule, LookupUtilities) {
			return {
				entitySchemaName: "Contract",
				attributes: {},
				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "insert",
						"name": "TsTarifficatorRegion",
						"values": {
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 0,
								"row": 4
							},
							"bindTo": "TsTarifficatorRegion"
						},
						"parentName": "Header",
						"propertyName": "items",
						"index": 4
					},
					{
						"operation": "insert",
						"name": "TsSupportEmployee",
						"values": {
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 0,
								"row": 3,
								"layoutName": "Header"
							},
							"labelConfig": {},
							"enabled": true,
							"contentType": 5,
							"bindTo": "TsSupportEmployee"
						},
						"parentName": "Header",
						"propertyName": "items",
						"index": 6
					},
					{
						"operation": "merge",
						"name": "CustomerBillingInfo",
						"values": {
							"contentType": 5,
							"labelConfig": {},
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 0,
								"row": 2
							}
						}
					},
					{
						"operation": "merge",
						"name": "Contact",
						"values": {
							"contentType": 5,
							"labelConfig": {},
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 0,
								"row": 1
							}
						}
					},
					{
						"operation": "merge",
						"name": "SupplierBillingInfo",
						"values": {
							"contentType": 5,
							"labelConfig": {},
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 12,
								"row": 2
							}
						}
					},
					{
						"operation": "insert",
						"propertyName": "items",
						"name": "TsLeadType",
						"parentName": "group_gridLayout",
						"values": {
							"contentType": Terrasoft.ContentType.ENUM,
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 12,
								"row": 1
							}
						}
					},

					{
						"operation": "merge",
						"name": "Amount",
						"values": {
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 0,
								"row": 0
							}
						}
					},
					{
						"operation": "merge",
						"name": "Parent",
						"values": {
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 0,
								"row": 0
							}
						}
					},
					{
						"operation": "move",
						"name": "Parent",
						"parentName": "ContractConnectionsBlock",
						"propertyName": "items",
						"index": 0
					},
					{
						"operation": "merge",
						"name": "Order",
						"values": {
							"layout": {
								"colSpan": 12,
								"rowSpan": 1,
								"column": 12,
								"row": 0
							}
						}
					},
					{
						"operation": "insert",
						"name": "TsLinkOnSEDContract",
						"values": {
							"layout": {
								"colSpan": 11,
								"rowSpan": 1,
								"column": 0,
								"row": 1,
								"layoutName": "ContractConnectionsBlock"
							},
							"labelConfig": {},
							"enabled": true,
							"bindTo": "TsLinkOnSEDContract"
						},
						"parentName": "ContractConnectionsBlock",
						"propertyName": "items",
						"index": 2
					},
					{
						"operation": "insert",
						"name": "TsLinkOnSEDContractButton",
						"parentName": "ContractConnectionsBlock",
						"propertyName": "items",
						"values": {
							"itemType": Terrasoft.ViewItemType.BUTTON,
							"layout": {
								"column": 11,
								"row": 1,
								"colSpan": 1,
								"rowSpan": 1
							},
							"tag": "TsLinkOnSEDContractButton",
							"imageConfig": {"bindTo": "Resources.Images.WebIcon"},
							"style": Terrasoft.controls.ButtonEnums.style.TRANSPARENT,
							"visible": true,
							"classes": {
								wrapperClass: ["detail-tools-button-wrapper"],
								menuClass: ["detail-tools-button-menu"]
							},
							"click": {"bindTo": "openSEDContractLink"}
						}
					},
					{
						"operation": "move",
						"name": "Type",
						"parentName": "Header",
						"propertyName": "items",
						"index": 2
					},
					{
						"operation": "move",
						"name": "State",
						"parentName": "Header",
						"propertyName": "items",
						"index": 3
					}
				]/**SCHEMA_DIFF*/,
				methods: {
					loadVocabulary: function(args, columnName) {
						var multiLookupColumns = this.getMultiLookupColumns(columnName);
						var config = (Ext.isEmpty(multiLookupColumns)) ?
								this.getLookupPageConfig(args, columnName) :
								this.getMultiLookupPageConfig(args, columnName);
						config.valuePairs = config.valuePairs || [];

						if (columnName === "Parent") {
							var select = this.Ext.create("Terrasoft.EntitySchemaQuery", {
								rootSchemaName: this.entitySchemaName
							});
							var filterGroup = new this.Terrasoft.createFilterGroup();
							filterGroup.logicalOperation = this.Terrasoft.LogicalOperatorType.OR;
							filterGroup.addItem(select.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.NOT_EQUAL,
									"Parent", this.get("Id")));
							filterGroup.addItem(this.Terrasoft.createColumnIsNullFilter("Parent"));
							config.filters.addItem(filterGroup);
							config.filters.addItem(this.Terrasoft.createColumnFilterWithParameter(
									this.Terrasoft.ComparisonType.EQUAL, "Type.IsSlave", false));
						} else if (columnName === "CustomerBillingInfo") {
							var accountId = this.get("Account") && this.get("Account").value;
							if (accountId) {
								config.valuePairs.push({
									name: "Account",
									value: accountId
								});
							}
						} else if (columnName === "SupplierBillingInfo") {
							var ourCompanyId = this.get("OurCompany") && this.get("OurCompany").value;
							if (ourCompanyId) {
								config.valuePairs.push({
									name: "Account",
									value: ourCompanyId
								});
							}
						}
						LookupUtilities.Open(this.sandbox, config, this.onLookupResult, this, null, false, false);
					},
					openSEDContractLink: function(tag) {
						var link = this.get("TsLinkOnSEDContract");
						if (!Ext.isEmpty(link)) {
							window.open(link);
						}
					}
				},
				rules: {
					"TsSupportEmployee": {
						"FiltrationByUserType": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"autocomplete": false,
							"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"type": BusinessRuleModule.enums.ValueType.CONSTANT,
							"value": ConfigurationConstants.SysAdminUnit.Type.User
						}
					}
				}
			};
		});
