define("TsAccountExecHistoryDetailV2", [], function() {
	return {
		entitySchemaName: "TsAccountExecHistory",
		attributes: {},
		mixins: {},
		methods: {
			getAddRecordButtonVisible: function() {
				return false;
			},
			getAddTypedRecordButtonVisible: function() {
				return false;
			},
			getEditRecordMenuItem: this.Terrasoft.emptyFn,
			getDeleteRecordMenuItem: this.Terrasoft.emptyFn,
			getCopyRecordMenuItem: this.Terrasoft.emptyFn
		},
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
	};
});