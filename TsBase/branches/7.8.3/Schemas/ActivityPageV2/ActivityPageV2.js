define("ActivityPageV2", ["TsChoosingModule", "BaseFiltersGenerateModule", "MaskHelper"],
	function(TsChoosingModule, BaseFiltersGenerateModule, MaskHelper) {
		return {
			entitySchemaName: "Activity",
			attributes: {
				"NewOwner": {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					referenceSchemaName: "Contact",
					lookupListConfig: {
						hideActions: true,
						filter: BaseFiltersGenerateModule.OwnerFilter
					}
				},
				"DetailedResultBody": {
					dataValueType: this.Terrasoft.DataValueType.TEXT,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					caption: {
						"bindTo": "Resources.Strings.DetailedResultCaption"
					}
				}
			},
			methods: {
				onDelegateActivityClick: function() {
					var choosingModule = this.Ext.create(TsChoosingModule);
					var config = [
						{
							"name": "NewOwner",
							"bindTo": "NewOwner",
							"caption": this.get("Resources.Strings.OwnerCaption"),
							"contentType": this.Terrasoft.ContentType.LOOKUP,
							"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
							"layout": {
								row: 0,
								rowSpan: 1,
								column: 0,
								colSpan: 24
							}
						},
						{
							"name": "DetailedResultBody",
							"bindTo": "DetailedResultBody",
							"caption": this.get("Resources.Strings.DetailedResultCaption"),
							"contentType": this.Terrasoft.ContentType.LONG_TEXT,
							"dataValueType": this.Terrasoft.DataValueType.TEXT,
							"layout": {
								row: 1,
								rowSpan: 4,
								column: 0,
								colSpan: 24
							}
						}

					];
					choosingModule.open({
						viewModel: this,
						sandbox: this.sandbox,
						controlsConfig: config,
						buttons: {"ok": {"tag": "ok"}, "cancel": {"tag": "cancel"}},
						onButtonClick: "onDelegateActivityAction",
						caption: this.get("Resources.Strings.DelegateActivityCaption"),
						windowHeight: 300,
						windowWidth: 500,
						doNotAddRowBeforeButtons: true
					});

				},
				onDelegateActivityAction: function(tag) {
					if (tag === "ok") {
						var owner = this.get("NewOwner");
						if (!(!this.Ext.isEmpty(this.get("DetailedResultBody")) && owner && owner.value)) {
							this.onDelegateActivityClick();
							this.showInformationDialog(this.get("Resources.Strings.FillRequiredFieldsMessage"));
							return;
						}
						this.save({
							isSilent: true,
							isDelegateActivityAction: true
						});
					} else {
						this.set("NewOwner", null);
						this.set("DetailedResultBody", null);
					}
				},

				callDelegateActivityAction: function() {
					MaskHelper.ShowBodyMask();
					var owner = this.get("NewOwner");
					this.callService({
						serviceName: "TsWebService",
						methodName: "DelegateActivity",
						data: {
							activityId: this.get("Id"),
							newOwnerId: this.get("NewOwner").value,
							delegateReason: this.get("DetailedResultBody")
						}
					}, function(responseObject) {
						if (responseObject && responseObject.DelegateActivityResult) {
							this.set("Owner", owner);
							this.set("NewOwner", null);
							this.set("DetailedResultBody", null);
							MaskHelper.HideBodyMask();
							this.showInformationDialog(this.get("Resources.Strings.ActionSuccessfullyFinished"));
						}
						else {
							MaskHelper.HideBodyMask();
							this.showInformationDialog(this.get("Resources.Strings.ActionFinishedWithError"));
						}
					}, this);
				},

				onSaved: function(response, config) {
					var isDelegateActivityAction = (config && config.isSilent && config.isDelegateActivityAction);
					if (isDelegateActivityAction) {
						config.isDelegateActivityAction = false;
					}
					this.callParent(arguments);
					if (isDelegateActivityAction) {
						this.callDelegateActivityAction();
					}
				}
			},
			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "insert",
					"name": "TsBusinessTypes",
					"values": {
						"layout": {
							"colSpan": 12,
							"column": 0,
							"row": 5
						},
						"bindTo": "TsBusinessTypes"
					},
					"parentName": "Header",
					"propertyName": "items"
				},
				{
					"operation": "merge",
					"parentName": "Header",
					"propertyName": "items",
					"name": "Owner",
					"values": {
						"bindTo": "Owner",
						"layout": {"column": 12, "row": 1, "colSpan": 12},
						"tip": {
							"content": {"bindTo": "Resources.Strings.OwnerTip"}
						},
						"enabled": false
					}
				},
				{
					"operation": "insert",
					"name": "DelegateActivityButton",
					"propertyName": "items",
					"parentName": "LeftContainer",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.BUTTON,
						"style": this.Terrasoft.controls.ButtonEnums.style.BLUE,
						"visible": true,
						"click": {
							"bindTo": "onDelegateActivityClick"
						},
						"classes": {
							"textClass": ["actions-button-margin-right"]
						},
						"caption": {"bindTo": "Resources.Strings.DelegateActivityCaption"}
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});
