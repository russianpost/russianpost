using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Security;
using System.Text;
using Terrasoft.Configuration;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Factories;
using Terrasoft.Core.Process;
using Terrasoft.Core.Configuration;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;


namespace Terrasoft.Configuration.TsBase {
	public class TsActivityUtils
	{
		#region Public: Properties
		public UserConnection UserConnection {
			get;
			private set;
		}
		#endregion
		
		#region Public: Constructors
		public TsActivityUtils(UserConnection userConnection)
		{
			UserConnection = userConnection;
		}
		#endregion
		
		
		#region Public: Methods
		public virtual bool DelegateActivity(Guid activityId, Guid ownerId, string delegateReason) {
			var currentActivity = GetActivityEntity(activityId);
			if (currentActivity == null) {
				return false;
			}
			CreateCanceledActivity(currentActivity, delegateReason, TsCsConstants.Activity.ActivityResult.Canceled);
			UpdateCurrentActivityOwner(currentActivity, ownerId);
			return true;
		}
		public virtual void ChangeActivitiesOwnerAsync(Guid currentOwnerId, Guid newOwnerId, Guid activityResultId) {
			CultureInfo culture = Thread.CurrentThread.CurrentCulture;
			Task.Factory.StartNew(() => {
				Thread.CurrentThread.CurrentCulture = culture;
				var resourceStorage = UserConnection.Workspace.ResourceStorage;
				string message = string.Empty;
				try {
					var result = ChangeActivitiesOwner(currentOwnerId, newOwnerId, activityResultId);
					if (result) {
						message = new LocalizableString(resourceStorage, "TsActivityUtils",
					"LocalizableStrings.ChangeOwnerSuccessMessage.Value");
					} else {
						message = new LocalizableString(resourceStorage, "TsActivityUtils",
					"LocalizableStrings.ActivityNotFoundMessage.Value");
					}
				}
				catch (Exception ex) {
					message = new LocalizableString(resourceStorage, "TsActivityUtils",
					"LocalizableStrings.ChangeOwnerErrorMessage.Value");
				}
				finally {
					CreateReminding(message, string.Empty, TsCsConstants.Activity.SchemaUId);
				}
			});
		}
		
		public virtual Guid GetRenindingSubjectId() {
			return new Guid("F5048556-3B7D-4745-9197-89CA58982778");
		}
		#endregion
		#region Protected: Methods
		protected virtual bool ChangeActivitiesOwner(Guid currentOwnerId, Guid newOwnerId, Guid activityResultId) {
			var activitiesList = GetActivitiesListByOwner(currentOwnerId);
			if (activitiesList.Count < 1) {
				return false;
			}
			foreach (Guid activityId in activitiesList) {
				var activityEntity = GetActivityEntity(activityId);
				CreateCanceledActivity(activityEntity, string.Empty, activityResultId);
				UpdateCurrentActivityOwner(activityEntity, newOwnerId);
			}
			return true;
		}
		protected void CreateCanceledActivity(Entity activity, string delegateReason, Guid activityResultId) {
			var canceledActivity = activity.Clone() as Terrasoft.Core.Entities.Entity;
			canceledActivity.SetDefColumnValues();
			canceledActivity.SetColumnValue("CreatedOn", activity.GetTypedColumnValue<DateTime>("CreatedOn"));
			if (!String.IsNullOrWhiteSpace(delegateReason)) {
				canceledActivity.SetColumnValue("DetailedResult", delegateReason);
			}
			canceledActivity.SetColumnValue("StatusId", TsCsConstants.Activity.ActivityStatus.Canceled);
			canceledActivity.SetColumnValue("ResultId", activityResultId);
			canceledActivity.Save(false);
		}
		
		protected void UpdateCurrentActivityOwner(Entity activity, Guid ownerId) {
			activity.SetColumnValue("OwnerId", ownerId);
			activity.SetColumnValue("CreatedOn",DateTime.Now);
			activity.Save(false);
		}
		protected List<Guid> GetActivitiesListByOwner(Guid currentOwnerId) {
			List<Guid> resultList = new List<Guid>();
			Select selectActivities = new Select(UserConnection)
					.Column("Activity", "Id")
					.From("Activity")
					.InnerJoin("ActivityStatus")
					.On("Activity", "StatusId").IsEqual("ActivityStatus", "Id")
					.Where("ActivityStatus", "Finish").IsEqual(Column.Parameter(false))
					.And("Activity", "OwnerId").IsEqual(Column.Parameter(currentOwnerId)) as Select;

			using (DBExecutor dbExecutor = UserConnection.EnsureDBConnection())
			{
				using (var reader = dbExecutor.ExecuteReader(selectActivities.GetSqlText(), selectActivities.Parameters))
				{
					while (reader.Read())
					{
						if (!reader.GetColumnValue<Guid>("Id").Equals(Guid.Empty))
						{
							resultList.Add(reader.GetColumnValue<Guid>("Id"));
						}
					}
					reader.Close();
				}
			}
			return resultList;
		}
		protected virtual void CreateReminding(string subject, string description, Guid schemaId, bool overrideReminding = false) {
			Entity remindingEntity = UserConnection.EntitySchemaManager.GetInstanceByName("Reminding")
				.CreateEntity(UserConnection);
			var condition = new Dictionary<string, object> {
				{
					"Author", UserConnection.CurrentUser.ContactId
				}, {
					"Contact", UserConnection.CurrentUser.ContactId
				}, {
					"Source", RemindingConsts.RemindingSourceAuthorId
				}, {
					"SubjectCaption", subject
				}, {
					"SysEntitySchema", schemaId
				},
			};
			string hash = GetRemindingHash(condition);
			if (!string.IsNullOrEmpty(description)) {
				subject = string.Concat(subject, " ", description);
			}
			if (!overrideReminding ||
					!remindingEntity.FetchFromDB(new Dictionary<string, object> { { "Hash", hash } }, false)) {
				remindingEntity.SetDefColumnValues();
			}
			remindingEntity.SetColumnValue("AuthorId", UserConnection.CurrentUser.ContactId);
			remindingEntity.SetColumnValue("ContactId", UserConnection.CurrentUser.ContactId);
			remindingEntity.SetColumnValue("SourceId", RemindingConsts.RemindingSourceAuthorId);
			remindingEntity.SetColumnValue("RemindTime", UserConnection.CurrentUser.GetCurrentDateTime());
			remindingEntity.SetColumnValue("Description", description);
			remindingEntity.SetColumnValue("SubjectCaption", subject);
			remindingEntity.SetColumnValue("SysEntitySchemaId", schemaId);
			remindingEntity.SetColumnValue("SubjectId", GetRenindingSubjectId());
			remindingEntity.SetColumnValue("Hash", hash);
			remindingEntity.Save();
		}
		#endregion
		#region Private: Methods
		private Entity GetActivityEntity(Guid activityId) {
			var activityEsq = new EntitySchemaQuery(UserConnection.EntitySchemaManager, "Activity");
			activityEsq.AddAllSchemaColumns();
			return activityEsq.GetEntity(UserConnection, activityId);
		}
		private string GetRemindingHash(Dictionary<string, object> dictionary) {
			var str = new StringBuilder();
			foreach (object value in dictionary.Values) {
				str.Append(value);
			}
			return FileUtilities.GetMD5Hash(Encoding.Unicode.GetBytes(str.ToString()));
		}
			
		#endregion
	}
}



