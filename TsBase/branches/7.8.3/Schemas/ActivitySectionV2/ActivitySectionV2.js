define("ActivitySectionV2", ["TsChoosingModule", "TsJsConsts"],
	function(TsChoosingModule, TsJsConsts) {
		return {

			entitySchemaName: "Activity",
			attributes: {
				"TsCurrentOwner" : {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					referenceSchemaName: "Contact",
					lookupListConfig: {
						hideActions: true,
						filter: function() {
							var filterGroup = this.Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("DefaultFilter", this.Terrasoft.createColumnIsNotNullFilter("[SysAdminUnit:Contact].Id"));
							filterGroup.add("ContactOwnerFilter",
								Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL,
									"Owner",
									this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value));
							return filterGroup;
						}

					}
				},
				"TsNewOwner": {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					referenceSchemaName: "Contact",
					lookupListConfig: {
						hideActions: true,
						filter: function() {
							var oldOwner = this.get("TsCurrentOwner");
							var filterGroup = this.Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("DefaultFilter", this.Terrasoft.createColumnIsNotNullFilter("[SysAdminUnit:Contact].Id"));
							filterGroup.add("ContactOwnerFilter",
								Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL,
									"Owner",
									this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value));
							if (oldOwner && !this.Ext.isEmpty(oldOwner.value)) {
								filterGroup.add("OwnerFilter", Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.NOT_EQUAL, "Id", oldOwner.value));
							}
							return filterGroup;
						}
					}
				},
				"TsActivityResult": {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					referenceSchemaName: "ActivityResult",
					lookupListConfig: {
						hideActions: true,
						filter: function() {
							var filterGroup = Ext.create("Terrasoft.FilterGroup");
							filterGroup.add("ActivityCategory",
								Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL,
									"[ActivityCategoryResultEntry:ActivityResult].ActivityCategory.ActivityType",
									TsJsConsts.ActivityType.Task.value));
							filterGroup.add("ActivityResultCategory",
								Terrasoft.createColumnFilterWithParameter(
									Terrasoft.ComparisonType.EQUAL,
									"Category",
									TsJsConsts.ActivityResultCategory.Negative.value));
							return filterGroup;
						}
					}
				}
			},
			messages: {},
			methods: {
				changeOwnerAction: function() {
					var choosingModule = this.Ext.create(TsChoosingModule);
					var config = [
						{
							"name": "TsCurrentOwner",
							"bindTo": "TsCurrentOwner",
							"caption": this.get("Resources.Strings.TsCurrentOwnerCaption"),
							"contentType": this.Terrasoft.ContentType.LOOKUP,
							"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
							"layout": {
								row: 0,
								rowSpan: 1,
								column: 0,
								colSpan: 24
							}
						},
						{
							"name": "TsNewOwner",
							"bindTo": "TsNewOwner",
							"caption": this.get("Resources.Strings.TsNewOwnerCaption"),
							"contentType": this.Terrasoft.ContentType.LOOKUP,
							"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
							"layout": {
								row: 1,
								rowSpan: 1,
								column: 0,
								colSpan: 24
							}
						},
						{
							"name": "TsActivityResult",
							"bindTo": "TsActivityResult",
							"caption": this.get("Resources.Strings.TsActivityResultCaption"),
							"contentType": this.Terrasoft.ContentType.LOOKUP,
							"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
							"layout": {
								row: 2,
								rowSpan: 1,
								column: 0,
								colSpan: 24
							}
						}

					];
					choosingModule.open({
						viewModel: this,
						sandbox: this.sandbox,
						controlsConfig: config,
						buttons: {"ok": {"tag": "ok"}, "cancel": {"tag": "cancel"}},
						onButtonClick: "onChangeActivitiesOwnerClick",
						caption: this.get("Resources.Strings.ChangeOwnerActionCaption"),
						windowHeight: 235,
						windowWidth: 500,
						doNotAddRowBeforeButtons: true
					});
				},
				onChangeActivitiesOwnerClick: function(tag) {
					if (tag === "ok") {
						var currentOwner = this.get("TsCurrentOwner");
						var newOwner = this.get("TsNewOwner");
						var activityResult = this.get("TsActivityResult");
						if (!(currentOwner && newOwner && activityResult)) {
							this.changeOwnerAction();
							this.showInformationDialog(this.get("Resources.Strings.FillRequiredFieldsMessage"));
							return;
						}
						this.callService({
							serviceName: "TsWebService",
							methodName: "ChangeActivitiesOwner",
							data: {
								currentOwnerId: currentOwner.value,
								newOwnerId: newOwner.value,
								activityResultId: activityResult.value
							}
						}, null, this);
						this.showInformationDialog(this.get("Resources.Strings.OnChangeActivitiesOwnerActionMessage"));
						this.cleanChangeActivitiesOwnerActionFields();
					} else {
						this.cleanChangeActivitiesOwnerActionFields();
					}
				},

				cleanChangeActivitiesOwnerActionFields: function() {
					this.set("TsCurrentOwner", null);
					this.set("TsNewOwner", null);
					this.set("TsActivityResult", null);
				}
			},
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "insert",
					"parentName": "CombinedModeActionButtonsCardLeftContainer",
					"propertyName": "items",
					"name": "DelegateActivityButton",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.BUTTON,
						"style": this.Terrasoft.controls.ButtonEnums.style.BLUE,
						"click": {"bindTo": "onCardAction"},
						"classes": {
							"textClass": ["actions-button-margin-right"]
						},
						"caption": {"bindTo": "Resources.Strings.DelegateActivityCaption"},
						"tag": "onDelegateActivityClick"
					}
				},
				{
					"operation": "insert",
					"name": "ChangeActivitiesOwner",
					"parentName": "SeparateModeActionButtonsLeftContainer",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.BUTTON,
						"style": this.Terrasoft.controls.ButtonEnums.style.GREEN,
						"caption": {"bindTo": "Resources.Strings.ChangeActivitiesOwnerCaption"},
						"click": {"bindTo": "changeOwnerAction"},
						"classes": {
							"textClass": ["actions-button-margin-right"],
							"wrapperClass": ["actions-button-margin-right"]
						}
					}
				}

			]/**SCHEMA_DIFF*/
		};
	}
);
