define("AccountSectionV2", ["TsChoosingModule"],
	function (TsChoosingModule) {
		return {
			entitySchemaName: "Account",
			attributes: {
				"NewExecutive": {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,

					referenceSchemaName: "Contact",
					lookupListConfig: {
						hideActions: true,
						filter: function() {
							return this.createExecutiveChangeFilter()
						}
					}
				},
				"OldExecutive": {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,

					referenceSchemaName: "Contact",
					lookupListConfig: {
						hideActions: true,
						filter: function() {
							return this.createExecutiveChangeFilter()
						}
					}
				},
				"ExecutiveRole": {
					dataValueType: this.Terrasoft.DataValueType.LOOKUP,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,

					referenceSchemaName: "TsAccountExecutiveRole",
					lookupListConfig: {
						hideActions: true
					}
				},
				"ChosingModuleInstance": {
					dataValueType: this.Terrasoft.DataValueType.CUSTOM_OBJECT,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: null
				},
				"ChosingModuleConfig": {
					dataValueType: this.Terrasoft.DataValueType.CUSTOM_OBJECT,
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					value: null
				}

			},
			messages: {},
			methods: {
				createExecutiveChangeFilter: function() {
					var filterGroup = this.Terrasoft.createFilterGroup();
					filterGroup.add("IsBPMUserFilter", this.Terrasoft.createColumnIsNotNullFilter("[SysAdminUnit:Contact].Id"));
					filterGroup.addItem(this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
						"Owner", this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value));
					return filterGroup;
				},
				getSectionActions: function() {
					var actionMenuItems = this.callParent(arguments);
					actionMenuItems.addItem(this.getButtonMenuItem({
						Type: "Terrasoft.MenuSeparator",
						Caption: ""
					}));
					actionMenuItems.addItem(this.getButtonMenuItem({
						"Click": {
							"bindTo": "onChangeAccountExecutiveActionClick"
						},
						"Caption": {
							"bindTo": "Resources.Strings.ChangeAccountExecutiveCaption"
						}
					}));
					return actionMenuItems;
				},

				getControlsConfig: function() {
					return [{
							"name": "NewExecutive",
							"bindTo": "NewExecutive",
							"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
							"contentType": this.Terrasoft.ContentType.LOOKUP,
							caption: this.get("Resources.Strings.NewAccountExecutiveCaption"),
							"layout": {
								row: 1,
								rowSpan: 1,
								column: 0,
								colSpan: 24
							}
						},
						{
							"name": "OldExecutive",
							"bindTo": "OldExecutive",
							"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
							"contentType": this.Terrasoft.ContentType.LOOKUP,
							caption: this.get("Resources.Strings.OldAccountExecutiveCaption"),
							"layout": {
								row: 0,
								rowSpan: 1,
								column: 0,
								colSpan: 24
							}
						},
						{
							"name": "ExecutiveRole",
							"bindTo": "ExecutiveRole",
							"dataValueType": this.Terrasoft.DataValueType.LOOKUP,
							"contentType": this.Terrasoft.ContentType.LOOKUP,
							caption: this.get("Resources.Strings.ExecutiveRoleCaption"),
							"tip": {
								content: this.get("Resources.Strings.ChangeExecutiveRoleTip")
							},
							"layout": {
								row: 2,
								rowSpan: 1,
								column: 0,
								colSpan: 24
							}
						}
					];
				},

				getChoosingModuleConfig: function() {
					return {
							viewModel: this,
							sandbox: this.sandbox,
							controlsConfig: this.getControlsConfig(),
							buttons: {
								"ok": {
									"tag": "ok"
								},
								"cancel": {
									"tag": "cancel"
								}
							},
							onButtonClick: "onChangeAccountExecutiveModalResult",
							caption: this.get("Resources.Strings.ChangeAccountExecutiveCaption"),
							windowHeight: 300,
							windowWidth: 400,
							doNotAddRowBeforeButtons: true
						};
				},

				getChoosingModule: function() {
					var choosingModule = this.get("ChosingModuleInstance");
					if (Ext.isEmpty(choosingModule)) {
						choosingModule = this.Ext.create(TsChoosingModule);
						this.set("ChosingModuleInstance", choosingModule);
					}
					return choosingModule;
				},

				openChoosingModule: function() {
					var choosingModule = this.getChoosingModule();
					var config = this.getChoosingModuleConfig();
					choosingModule.open(config);
				},

				onChangeAccountExecutiveActionClick: function() {
					this.openChoosingModule();
				},

				onChangeAccountExecutiveModalResult: function(tag) {
					if (tag === "ok") {
						var newExecutive = this.get("NewExecutive");
						var oldExecutive = this.get("OldExecutive");
						var executiveRole = this.get("ExecutiveRole");
						if (Ext.isEmpty(newExecutive) || Ext.isEmpty(oldExecutive)) {
							this.Terrasoft.showInformation(this.get("Resources.Strings.FillOldExecutiveOrNewExecutive"), function() {
								this.openChoosingModule();
							}.bind(this));
						} else {
							this.Terrasoft.showInformation(this.get("Resources.Strings.ChangeAccountExecutiveIsRun"));
							this.callService({
								serviceName: "TsWebService",
								methodName: "ChangeAccountAndRelatedEntitiesExecutive",
								data: {
									newExecutiveId: newExecutive.value,
									oldExecutiveId: oldExecutive.value,
									accountExecutiveRoleId: (executiveRole && executiveRole.value) || Terrasoft.EMPTY_GUID  
								}
							}, function(responce) {
								this.set("NewExecutive", null);
								this.set("OldExecutive", null);
								this.set("ExecutiveRole", null);
								this.set("ChosingModuleInstance", null);
							}.bind(this));
						}
					} else {
						this.set("NewExecutive", null);
						this.set("OldExecutive", null);
						this.set("ExecutiveRole", null);
						this.set("ChosingModuleInstance", null);
					}
				}
			},
			diff: /**SCHEMA_DIFF*/ [] /**SCHEMA_DIFF*/
		};
	});