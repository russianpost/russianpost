using System.Web;
using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ServiceModel.Activation;
using Terrasoft.Core;
using Terrasoft.Core.Factories;

namespace Terrasoft.Configuration.TsBase
{	
	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class TsWebService
	{
		#region Properties: Protected

		private UserConnection _userConnection;
		protected UserConnection UserConnection {
			get {
				if (_userConnection == null) {
					_userConnection = (UserConnection)HttpContext.Current.Session["UserConnection"];
				}
				return _userConnection;
			}
		}

		#endregion

		[OperationContract]
		[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		public bool DelegateActivity(Guid activityId, Guid newOwnerId, string delegateReason)
		{
			if (newOwnerId == Guid.Empty || string.IsNullOrWhiteSpace(delegateReason)) {
				return false;
			}

			var activityUtils = ClassFactory.Get<TsActivityUtils>(new ConstructorArgument("userConnection", UserConnection));
			return activityUtils.DelegateActivity(activityId, newOwnerId, delegateReason);
		}
		
		[OperationContract]
		[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		public void ChangeActivitiesOwner(Guid currentOwnerId, Guid newOwnerId, Guid activityResultId)
		{
			if (currentOwnerId == Guid.Empty || newOwnerId == Guid.Empty || activityResultId == Guid.Empty) {
				return;
			}

			var activityUtils = ClassFactory.Get<TsActivityUtils>(new ConstructorArgument("userConnection", UserConnection));
			activityUtils.ChangeActivitiesOwnerAsync(currentOwnerId, newOwnerId, activityResultId);
		}

		[OperationContract]
		[WebInvoke(Method = "POST", UriTemplate = "ChangeAccountAndRelatedEntitiesExecutive", BodyStyle = WebMessageBodyStyle.Wrapped,
			RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		public bool ChangeAccountAndRelatedEntitiesExecutive(Guid oldExecutiveId, Guid newExecutiveId, Guid accountExecutiveRoleId) {

			if (oldExecutiveId == Guid.Empty) {
				throw new ArgumentException("oldExecutiveId mustn't be null");
			}
			if (newExecutiveId == Guid.Empty) {
				throw new ArgumentException("newExecutiveId mustn't be null");
			}

			var accountUtils = ClassFactory.Get<TsAccountUtils>(new ConstructorArgument("userConnection", UserConnection));
			accountUtils.ScheduleChangeAccountExecutive(oldExecutiveId, newExecutiveId, accountExecutiveRoleId);
			return true;
		}		
	}
}

