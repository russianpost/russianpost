define("SectionDashboardsModule", ["DashboardsModule", "SectionDashboardsViewModel", "SectionDashboardBuilder",
			"HistoryStateUtilities"],
		function() {

			return Ext.define("Terrasoft.configuration.SectionDashboardsModule", {
				extend: "Terrasoft.DashboardsModule",
				alternateClassName: "Terrasoft.SectionDashboardsModule",
				viewModelClassName: "Terrasoft.SectionDashboardsViewModel",
				builderClassName: "Terrasoft.SectionDashboardBuilder",


				mixins: {

					HistoryStateUtilities: "Terrasoft.HistoryStateUtilities"
				},


				initHistoryState: Terrasoft.emptyFn,


				render: function() {
					this.callParent(arguments);
					this.sandbox.publish("NeedHeaderCaption");
				},


				generateSchemaStructure: function(callback, scope) {
					var builder = this.Ext.create(this.builderClassName, {
						viewModelClass: this.viewModelClassName,
						viewConfigClass: this.viewConfigClass
					});
					var sectionInfo = this.getSectionInfo();
					var config = {
						sectionId: sectionInfo.moduleId || Terrasoft.GUID_EMPTY
					};
					builder.build(config, function(viewModelClass, view) {
						callback.call(scope, viewModelClass, view);
					}, this);
				}

			});

		});
