define("LeadPageV2", [], function() {
	return {
		entitySchemaName: "Lead",
		messages: {
			"GetLeadAccountInfo": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		attributes: {
			"QualifiedAccount": {
				lookupListConfig: {
					columns: ["Industry", "TsAccountBusinessType"]
				}
			}
		},
		methods: {
			subscribeSandboxEvents: function() {
				this.callParent(arguments);

				this.sandbox.subscribe("GetLeadAccountInfo", function() {
					var account = this.get("QualifiedAccount");
					return {
						Industry: account.Industry,
						BusinessType: account.TsAccountBusinessType
					};
				}, this, [this.getDetailId("LeadProduct")]);
			}
		},
		details: /**SCHEMA_DETAILS*/{
			TsLeadStageHistoryDetailV2: {
				schemaName: "TsLeadStageHistoryDetailV2",
				filter: {
					masterColumn: "Id",
					detailColumn: "TsLead"
				}
			}
		}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "TsTarifficatorRegion",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3
					},
					"bindTo": "TsTarifficatorRegion"
				},
				"parentName": "LeadPageRegisterInfoBlock",
				"propertyName": "items"
			},
			{
				"operation": "insert",
				"parentName": "LeadHistoryTabContainer",
				"propertyName": "items",
				"name": "TsLeadStageHistoryDetailV2",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.DETAIL
				}
			}
		]/**SCHEMA_DIFF*/
	};
});