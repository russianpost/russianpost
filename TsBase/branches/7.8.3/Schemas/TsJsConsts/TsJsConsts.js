define("TsJsConsts", [], function() {
	var accountOwnership = {
		IndividualEntrepreneur: {
			value: "350469c4-40f9-49c5-8298-7f8a38548050"
		}
	};

	var activityType = {
		Task: {
			value: "fbe0acdc-cfc0-df11-b00f-001d60e938c6"
		}
	};

	var activityResultCategory = {
		Negative: {
			value: "4db33cbc-ba7c-4103-9887-6b39b8d36b77"
		}
	};

	var leadType = {
		Advert: {
			"value": "3998457d-79e0-4af4-8377-95979e87f300",
			"displayValue": "Блок директ-маркетинга"
		},
		Printing: {
			"value": "68e280d1-186c-4cb9-9429-e066d201f9b6",
			"displayValue": "Блок печатных сервисов"
		},
		Subscribe: {
			"value": "7ce973b9-7a99-46da-b14e-430d9c7ee66b",
			"displayValue": "Блок подписных сервисов"
		},
		Retail: {
			"value": "fed6c57-6ddc-47c0-9904-a01d31e13b5b",
			"displayValue": "Блок розничной торговли"
		},
		Parcel: {
			"value": "9513105a-63c4-4952-96bd-3700d05c2f23",
			"displayValue": "Посылочный блок"
		},
		Fin: {
			"value": "e98c7297-50a5-4c2b-952f-7a06df400f45",
			"displayValue": "Финансовый блок"
		}
	};

	var contactType = {
		Employee: {
			value: "60733efc-f36b-1410-a883-16d83cab0980"
		}
	};
	return {
		AccountOwnership: accountOwnership,
		ActivityResultCategory: activityResultCategory,
		ActivityType: activityType,
		ContactType: contactType,
		LeadType: leadType
	};
});