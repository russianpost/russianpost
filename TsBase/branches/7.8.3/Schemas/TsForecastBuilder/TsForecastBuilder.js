define("TsForecastBuilder", ["TsForecastBuilderResources", "ForecastBuilder"],
	function(Resources) {
		Ext.define("Terrasoft.configuration.TsForecastsViewConfig", {
			extend: "Terrasoft.ForecastsViewConfig",
			alternateClassName: "Terrasoft.TsForecastsViewConfig",

			generate: function() {
				var viewConfig = this.callParent(arguments);
				var tabs;
				viewConfig.forEach(function(element) {
					if(Ext.isEmpty(tabs) && element.name === "Tabs") {
						tabs = element;
					}
				});
				var settingsButton = tabs.controlConfig.items[0];
				var settingsButtonMenuItems = settingsButton.menu.items;
				settingsButtonMenuItems.push({
					"caption": "",
					"className": "Terrasoft.MenuSeparator"
				});
				settingsButtonMenuItems.push({
					"caption": {"bindTo": "Resources.Strings.LeadTypeCaption"},
					"markerValue": {"bindTo": "Resources.Strings.LeadTypeCaption"},
					"menu": {
						"items": {"bindTo": "LeadTypeMenuItemsCollection"}
					}
				});
				return viewConfig;
			}
		});

		Ext.define("Terrasoft.configuration.TsBaseForecastsViewModel", {
			extend: "Terrasoft.BaseForecastsViewModel",
			alternateClassName: "Terrasoft.TsBaseForecastsViewModel",

			getButtonMenuItem: function(config) {
				return Ext.create("Terrasoft.BaseViewModel", {
					values: Ext.apply({}, {
						Id: config.id,
						Caption: config.name,
						Click: {"bindTo": "onLeadTypeChangeClick"},
						Tag: config.id,
						MarkerValue: config.name
					})
				});
			},

			initLeadTypeMenuItems: function() {
				var leadTypeMenuItemsCollection = Ext.create("Terrasoft.BaseViewModelCollection");
				leadTypeMenuItemsCollection.addItem(this.getButtonMenuItem({
					"id": Terrasoft.GUID_EMPTY,
					"name": {"bindTo": "Resources.Strings.EmptyLeadTypeCaption"}
				}));
				var leadTypeEsq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "LeadType"
				});
				leadTypeEsq.addColumn("Id");
				leadTypeEsq.addColumn("Name");
				leadTypeEsq.getEntityCollection(function(response) {
					if (!response || !response.success) {
						return;
					}
					response.collection.each(function(leadType) {
						var leadTypeId = leadType.get("Id");
						var leadTypeName = leadType.get("Name");
						leadTypeMenuItemsCollection.addItem(this.getButtonMenuItem({
							"id": leadTypeId,
							"name": leadTypeName
						}));
					}, this);
					this.set("LeadTypeMenuItemsCollection", leadTypeMenuItemsCollection);
				}, this);
			},

			constructor: function() {
				this.callParent(arguments);
				this.initResourcesValues(Resources);
				this.initLeadTypeMenuItems();
			},

			getForecasts: function(leadTypeId, callback, scope) {
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "Forecast"
				});
				esq.addColumn("Id");
				var nameColumn = esq.addColumn("Name");
				nameColumn.orderPosition = 1;
				nameColumn.orderDirection = Terrasoft.OrderDirection.ASC;
				if (!Ext.isEmpty(leadTypeId) && leadTypeId === Terrasoft.GUID_EMPTY) {
					esq.filters.add("TsLeadTypeFilter", Terrasoft.createIsNullFilter(
						Ext.create("Terrasoft.ColumnExpression", {columnPath: "TsLeadType"})));
				} else {
					esq.filters.add("TsLeadTypeFilter", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "TsLeadType", leadTypeId));
				}
				esq.getEntityCollection(function(response) {
					if (response && response.success) {
						callback.call(scope, response.collection);
					}
				}, this);
			},

			setActiveTab: function(tabName) {
				var activeTabName = this.get("ActiveTabName");
				if (activeTabName === tabName) {
					return;
				}
				this.callParent(arguments);
			},

			reloadTabs: function() {
				var forecasts = this.get("Forecasts");
				var tabsValues = [];
				forecasts.each(function(forecast) {
					tabsValues.push({
						Caption: forecast.get("Name"),
						Name: forecast.get("Id")
					});
				}, this);
				var newTabsCollection = Ext.create("Terrasoft.BaseViewModelCollection", {
					entitySchema: Ext.create("Terrasoft.BaseEntitySchema", {
						columns: {},
						primaryColumnName: "Name"
					})
				});
				newTabsCollection.loadFromColumnValues(tabsValues);
				var tabsCollection = this.get("TabsCollection");
				tabsCollection.clear();
				tabsCollection.loadAll(newTabsCollection);
				var defaultTabName = this.getDefaultTabName();
				if (!defaultTabName) {
					this.set("ActiveTabName", defaultTabName);
					this.reloadRecordRights();
					var forecastModuleId = this.getForecastModuleId();
					this.sandbox.unloadModule(forecastModuleId);
					this.setIsTabsEmpty();
					return;
				}
				this.setActiveTab(defaultTabName);
				this.reloadRecordRights();
				this.setIsTabsEmpty();
			},

			onLeadTypeChangeClick: function(tag) {
				this.getForecasts(tag, function(forecasts) {
					this.set("Forecasts", forecasts);
					this.reloadTabs();
				}, this);
			}
		});

		return Ext.define("Terrasoft.configuration.TsForecastBuilder", {
			extend: "Terrasoft.ForecastBuilder",
			alternateClassName: "Terrasoft.TsForecastBuilder",

			viewModelClass: "Terrasoft.TsBaseForecastsViewModel",

			getForecasts: function(leadTypeId, callback, scope) {
				var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "Forecast"
				});
				esq.addColumn("Id");
				var nameColumn = esq.addColumn("Name");
				nameColumn.orderPosition = 1;
				nameColumn.orderDirection = Terrasoft.OrderDirection.ASC;
				if (!Ext.isEmpty(leadTypeId)) {
					esq.filters.add("TsLeadTypeFilter", Terrasoft.createColumnFilterWithParameter(
						Terrasoft.ComparisonType.EQUAL, "TsLeadType", leadTypeId));
				}
				esq.getEntityCollection(function(response) {
					if (response && response.success) {
						callback.call(scope, response.collection);
					}
				}, this);
			},

			requireForecasts: function(callback, scope) {
				this.getUserKindOfBusiness(function(userKindOfBusinessId) {
					this.getForecasts(userKindOfBusinessId, callback, scope);
				}, this);
			},

			getUserKindOfBusiness: function(callback, scope) {
				var businessKindEsq = Ext.create("Terrasoft.EntitySchemaQuery", {
					rootSchemaName: "Contact"
				});
				businessKindEsq.addColumn("TsLeadType");
				businessKindEsq.filters.add("IdFilter", Terrasoft.createColumnFilterWithParameter(
					Terrasoft.ComparisonType.EQUAL, "Id", Terrasoft.SysValue.CURRENT_USER_CONTACT.value));
				businessKindEsq.getEntityCollection(function(response) {
					var userKindOfBusiness;
					if (response.success) {
						var collection = response.collection;
						if (collection.getCount() > 0) {
							userKindOfBusiness = collection.getByIndex(0).get("TsLeadType").value;
						}
					}
					callback.call(scope, userKindOfBusiness);
				});
			}
		});
	});
