define("ForecastPage", ["ForecastPageResources"],
		function(resources) {
			return {
				entitySchemaName: "Forecast",
				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "insert",
						"name": "TsLeadType",
						"values": {
							"layout": {
								"column": 0,
								"row": 3,
								"colSpan": 12
							},
							"contentType": this.Terrasoft.ContentType.ENUM
						},
						"parentName": "Header",
						"propertyName": "items"
					}
				]/**SCHEMA_DIFF*/,
				attributes: {},
				messages: {},
				methods: {},
				rules: {},
				userCode: {}
			};
		});
