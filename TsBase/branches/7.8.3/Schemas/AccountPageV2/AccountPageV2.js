define("AccountPageV2", ["ConfigurationConstants", "BusinessRuleModule", "TsJsConsts", "TsJsConfigurationUtilities",
		"css!TsAccountPageV2CSS"],
	function(ConfigurationConstants, BusinessRuleModule, TsJsConsts) {
		return {
			entitySchemaName: "Account",
			mixins: {
				TsJsConfigurationUtilities: "Terrasoft.TsJsConfigurationUtilities"
			},
			attributes: {
				"IsKPPVisible": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: true
				},
				"TypeHook": {
					"dependencies": [
						{
							"columns": ["Type"],
							"methodName": "onTypeChanged"
						}
					]
				},
				"Ownership": {
					"dependencies": [
						{
							"columns": ["Ownership", "TsINNLength"],
							"methodName": "onOwnershipChanged"
						}
					],
					lookupListConfig: {
						columns: ["TsINNLength"]
					}

				},
				"KindOfBusinessCollection": {
					dataValueType: this.Terrasoft.DataValueType.COLLECTION,
					value: this.Ext.create("Terrasoft.BaseViewModelCollection")
				},
				"ActiveKindOfBusinessRow": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.GUID,
					"dependencies": [
						{
							"columns": ["ActiveKindOfBusinessRow"],
							"methodName": "onActiveKindOfBusinessRowChanged"
						}
					]
				},
				"IsFinancialLeadTypeSelected": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: false
				},
				"IsAdvertLeadTypeSelected": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: false
				},
				"IsParcelLeadTypeSelected": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: false
				},
				"IsSubscribeLeadTypeSelected": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: false
				},
				"IsRetailLeadTypeSelected": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: false
				},
				"IsPrintingLeadTypeSelected": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
					value: false
				}
			},
			details: /**SCHEMA_DETAILS*/{
				"TsDiscountInAccountDetailV2": {
					"schemaName": "TsDiscountInAccountDetailV2",
					"filter": {
						"masterColumn": "Id",
						"detailColumn": "TsAccount"
					}
				},
				"TsAccountExecHistoryDetailV2": {
					"schemaName": "TsAccountExecHistoryDetailV2",
					"filter": {
						"masterColumn": "Id",
						"detailColumn": "TsAccount"
					}
				}
			}/**SCHEMA_DETAILS*/,
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "merge",
					"name": "AccountName",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						}
					}
				},
				{
					"operation": "merge",
					"name": "AccountType",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						}
					}
				},
				{
					"operation": "merge",
					"name": "AccountOwner",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 2
						}
					}
				},
				{
					"operation": "merge",
					"name": "AccountWeb",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 3
						}
					}
				},
				{
					"operation": "merge",
					"name": "AccountPhone",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 4
						}
					}
				},
				{
					"operation": "merge",
					"name": "NewAccountCategory",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 5
						}
					}
				},
				{
					"operation": "merge",
					"name": "AccountIndustry",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 6
						}
					}
				},
				{
					"operation": "insert",
					"name": "TsAccountBusinessType",
					"values": {
						"bindTo": "TsAccountBusinessType",
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 7
						},
						"contentType": 3
					},
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"index": 7
				},
				{
					"operation": "merge",
					"name": "AccountCompletenessContainer",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 8
						}
					}
				},
				{
					"operation": "merge",
					"name": "AlternativeName",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						}
					}
				},
				{
					"operation": "merge",
					"name": "Code",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0
						}
					}
				},
				{
					"operation": "insert",
					"name": "TsFullName",
					"values": {
						"bindTo": "TsFullName",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						}
					},
					"parentName": "AccountPageGeneralInfoBlock",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsBrandName",
					"values": {
						"bindTo": "TsBrandName",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1
						}
					},
					"parentName": "AccountPageGeneralInfoBlock",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsTarifficatorRegion",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "AccountPageGeneralInfoBlock"
						},
						"bindTo": "TsTarifficatorRegion"
					},
					"parentName": "AccountPageGeneralInfoBlock",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsINN",
					"values": {
						"layout": {
							"colSpan": 6,
							"rowSpan": 1,
							"column": 12,
							"row": 0
						}
					},
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsKPP",
					"values": {
						"layout": {
							"colSpan": 6,
							"rowSpan": 1,
							"column": 18,
							"row": 0
						},
						"visible": {
							"bindTo": "IsKPPVisible"
						}
					},
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "merge",
					"name": "Ownership",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						}
					}
				},
				{
					"operation": "merge",
					"name": "AnnualRevenue",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						}
					}
				},
				{
					"operation": "insert",
					"name": "TsAccountLevel",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1,
							"layoutName": "CategoriesControlGroupContainer"
						},
						"bindTo": "TsAccountLevel"
					},
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsAccountRegionType",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesControlGroupContainer"
						},
						"contentType": 3,
						"bindTo": "TsAccountRegionType"
					},
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "merge",
					"name": "EmployeesNumber",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 2
						}
					}
				},
				{
					"operation": "move",
					"name": "EmployeesNumber",
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items",
					"index": 6
				},
				//#region KindOfBusinessCategorizationGridContainer

				{
					"operation": "insert",
					"parentName": "Tabs",
					"propertyName": "tabs",
					"name": "KindOfBusinessTab",
					"values": {
						"caption": {"bindTo": "Resources.Strings.KindOfBusinessTabCaption"},
						"items": []
					},
					"index": 2
				},
				{
					"operation": "insert",
					"name": "KindOfBusinessGridContainer",
					"parentName": "KindOfBusinessTab",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.CONTAINER,
						"classes": {"wrapClassName": ["detail-container"]},
						"items": []
					}
				},

				{
					"operation": "insert",
					"name": "KindOfBusinessContainer",
					"parentName": "KindOfBusinessGridContainer",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.CONTAINER,
						"classes": {"wrapClassName": ["detail-part-left"]},
						"items": []
					}
				},
				{
					"operation": "insert",
					"name": "KindOfBusinessGrid",
					"parentName": "KindOfBusinessContainer",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.GRID,
						"listedZebra": true,
						"collection": {"bindTo": "KindOfBusinessCollection"},
						"primaryColumnName": "Id",
						"activeRow": {"bindTo": "ActiveKindOfBusinessRow"},
						"type": "listed",
						"columnsConfig": [
							{
								"cols": 24,
								"key": [{
									"name": {"bindTo": "Caption"},
									"type": "text"
								}]
							}
						],
						"captionsConfig": [
							{
								"cols": 24,
								"name": "Тип бизнеса"
							}
						]
					}
				},
				{
					"operation": "insert",
					"name": "KindOfBusinessCategorizationGridContainer",
					"parentName": "KindOfBusinessGridContainer",
					"propertyName": "items",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.CONTAINER,
						"classes": {"wrapClassName": ["detail-part-right"]},
						"items": []
					}
				},

				{
					"operation": "insert",
					"name": "CategoriesSubscribeServicesControlGroupContainer",
					"values": {
						"visible": {bindTo: "IsSubscribeLeadTypeSelected"},
						"itemType": 0,
						"items": []
					},
					"parentName": "KindOfBusinessCategorizationGridContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2BSubscribeServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "CategoriesSubscribeServicesControlGroupContainer"
						},
						"bindTo": "TsB2BSubscribeServices",
						"caption": {"bindTo": "Resources.Strings.TsB2BCaption"}
					},
					"parentName": "CategoriesSubscribeServicesControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2CSubscribeServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesSubscribeServicesControlGroupContainer"
						},
						"bindTo": "TsB2CSubscribeServices",
						"caption": {"bindTo": "Resources.Strings.TsB2CCaption"}
					},
					"parentName": "CategoriesSubscribeServicesControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "TsG2BSubscribeServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesSubscribeServicesControlGroupContainer"
						},
						"bindTo": "TsG2BSubscribeServices",
						"caption": {"bindTo": "Resources.Strings.TsG2BCaption"}
					},
					"parentName": "CategoriesSubscribeServicesControlGroupContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsVipSubscribeServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 3,
							"layoutName": "CategoriesSubscribeServicesControlGroupContainer"
						},
						"bindTo": "TsVipSubscribeServices",
						"caption": {"bindTo": "Resources.Strings.TsVipCation"}
					},
					"parentName": "CategoriesSubscribeServicesControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccCatInSubscribeServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 4,
							"layoutName": "CategoriesSubscribeServicesControlGroupContainer"
						},
						"contentType": 3,
						"bindTo": "TsAccCatInSubscribeServices",
						"caption": {"bindTo": "Resources.Strings.TsStatusCaption"}
					},
					"parentName": "CategoriesSubscribeServicesControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsExecutiveSubscribeServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 5,
							"layoutName": "CategoriesSubscribeServicesControlGroupContainer"
						},
						"bindTo": "TsExecutiveSubscribeServices",
						"labelConfig": {},
						"enabled": true,
						"contentType": 5,
						"caption": {"bindTo": "Resources.Strings.TsExecutiveCaption"}
					},
					"parentName": "CategoriesSubscribeServicesControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsExecSuppSubscribeServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 6,
							"layoutName": "CategoriesSubscribeServicesControlGroupContainer"
						},
						"bindTo": "TsExecSuppSubscribeServices",
						"caption": {"bindTo": "Resources.Strings.TsExecSuppCaption"}
					},
					"parentName": "CategoriesSubscribeServicesControlGroupContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "insert",
					"name": "CategoriesParcelBusinessControlGroupContainer",
					"values": {
						"visible": {bindTo: "IsParcelLeadTypeSelected"},
						"itemType": 0,
						"items": []
					},
					"parentName": "KindOfBusinessCategorizationGridContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2BParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsB2BParcelBusiness",
						"caption": {"bindTo": "Resources.Strings.TsB2BCaption"}
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2CParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsB2CParcelBusiness",
						"caption": {"bindTo": "Resources.Strings.TsB2CCaption"}
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "TsG2BParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsG2BParcelBusiness",
						"caption": {"bindTo": "Resources.Strings.TsG2BCaption"}
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsVipParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 3,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsVipParcelBusiness",
						"caption": {"bindTo": "Resources.Strings.TsVipCation"}
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccountParcelCategory",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 4,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsAccountParcelCategory",
						"labelConfig": {},
						"enabled": true,
						"contentType": 3,
						"caption": {"bindTo": "Resources.Strings.TsStatusCaption"}
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsExecutiveParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 5,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsExecutiveParcelBusiness",
						"labelConfig": {},
						"enabled": true,
						"contentType": 5,
						"caption": {"bindTo": "Resources.Strings.TsExecutiveCaption"}
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsExecSuppParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 6,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsExecSuppParcelBusiness",
						"caption": {"bindTo": "Resources.Strings.TsExecSuppCaption"}
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "insert",
					"name": "CategoriesFinancialBusinessControlGroupContainer",
					"values": {
						"visible": {bindTo: "IsFinancialLeadTypeSelected"},
						"itemType": 0,
						"items": []
					},
					"parentName": "KindOfBusinessCategorizationGridContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2BFinancialBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsB2BFinancialBusiness",
						"caption": {"bindTo": "Resources.Strings.TsB2BCaption"}
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2CFinancialBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsB2CFinancialBusiness",
						"caption": {"bindTo": "Resources.Strings.TsB2CCaption"}
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "TsG2BFinancialBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsG2BFinancialBusiness",
						"caption": {"bindTo": "Resources.Strings.TsG2BCaption"}
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsVipFinancialBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 3,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsVipFinancialBusiness",
						"caption": {"bindTo": "Resources.Strings.TsVipCation"}
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccountFinancialCategory",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 4,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsAccountFinancialCategory",
						"contentType": 3,
						"caption": {"bindTo": "Resources.Strings.TsStatusCaption"}
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsExecutiveFinancialBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 5,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsExecutiveFinancialBusiness",
						"labelConfig": {},
						"enabled": true,
						"contentType": 5,
						"caption": {"bindTo": "Resources.Strings.TsExecutiveCaption"}
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsExecSuppFinBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 6,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsExecSuppFinBusiness",
						"caption": {"bindTo": "Resources.Strings.TsExecSuppCaption"}
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "insert",
					"name": "CategoriesAdvertBusinessControlGroupContainer",
					"values": {
						"visible": {bindTo: "IsAdvertLeadTypeSelected"},
						"itemType": 0,
						"items": []
					},
					"parentName": "KindOfBusinessCategorizationGridContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2BAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsB2BAdvertBusiness",
						"caption": {"bindTo": "Resources.Strings.TsB2BCaption"}
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2CAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsB2CAdvertBusiness",
						"caption": {"bindTo": "Resources.Strings.TsB2CCaption"}
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "TsG2BAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsG2BAdvertBusiness",
						"caption": {"bindTo": "Resources.Strings.TsG2BCaption"}
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsVipAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 3,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsVipAdvertBusiness",
						"caption": {"bindTo": "Resources.Strings.TsVipCation"}
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccountAdvertCategory",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 4,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"contentType": 3,
						"bindTo": "TsAccountAdvertCategory",
						"caption": {"bindTo": "Resources.Strings.TsStatusCaption"}
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsExecutiveAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 5,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsExecutiveAdvertBusiness",
						"labelConfig": {},
						"enabled": true,
						"contentType": 5,
						"caption": {"bindTo": "Resources.Strings.TsExecutiveCaption"}
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsExecSuppAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 6,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsExecSuppAdvertBusiness",
						"caption": {"bindTo": "Resources.Strings.TsExecSuppCaption"}
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "insert",
					"name": "TsDiscountInAccountDetailV2",
					"values": {
						"itemType": 2
					},
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccountExecHistoryDetailV2",
					"values": {
						"itemType": 2
					},
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "CategoriesPrintingServicesControlGroupContainer",
					"values": {
						"visible": {bindTo: "IsPrintingLeadTypeSelected"},
						"itemType": 0,
						"items": []
					},
					"parentName": "KindOfBusinessCategorizationGridContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2BPrintingServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "CategoriesPrintingServicesControlGroupContainer"
						},
						"bindTo": "TsB2BPrintingServices",
						"caption": {"bindTo": "Resources.Strings.TsB2BCaption"}
					},
					"parentName": "CategoriesPrintingServicesControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2CPrintingServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesPrintingServicesControlGroupContainer"
						},
						"bindTo": "TsB2CPrintingServices",
						"caption": {"bindTo": "Resources.Strings.TsB2CCaption"}
					},
					"parentName": "CategoriesPrintingServicesControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "TsG2BPrintingServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesPrintingServicesControlGroupContainer"
						},
						"bindTo": "TsG2BPrintingServices",
						"caption": {"bindTo": "Resources.Strings.TsG2BCaption"}
					},
					"parentName": "CategoriesPrintingServicesControlGroupContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsVipPrintingServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 3,
							"layoutName": "CategoriesPrintingServicesControlGroupContainer"
						},
						"bindTo": "TsVipPrintingServices",
						"caption": {"bindTo": "Resources.Strings.TsVipCation"}
					},
					"parentName": "CategoriesPrintingServicesControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccCatInPrintingServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 4,
							"layoutName": "CategoriesPrintingServicesControlGroupContainer"
						},
						"contentType": 3,
						"bindTo": "TsAccCatInPrintingServices",
						"caption": {"bindTo": "Resources.Strings.TsStatusCaption"}
					},
					"parentName": "CategoriesPrintingServicesControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsExecutivePrintingServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 5,
							"layoutName": "CategoriesPrintingServicesControlGroupContainer"
						},
						"bindTo": "TsExecutivePrintingServices",
						"labelConfig": {},
						"enabled": true,
						"contentType": 5,
						"caption": {"bindTo": "Resources.Strings.TsExecutiveCaption"}
					},
					"parentName": "CategoriesPrintingServicesControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsExecSuppPrintingServices",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 6,
							"layoutName": "CategoriesPrintingServicesControlGroupContainer"
						},
						"bindTo": "TsExecSuppPrintingServices",
						"caption": {"bindTo": "Resources.Strings.TsExecSuppCaption"}
					},
					"parentName": "CategoriesPrintingServicesControlGroupContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "insert",
					"name": "CategoriesRetailControlGroupContainer",
					"values": {
						"visible": {bindTo: "IsRetailLeadTypeSelected"},
						"itemType": 0,
						"items": []
					},
					"parentName": "KindOfBusinessCategorizationGridContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2BRetail",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "CategoriesRetailControlGroupContainer"
						},
						"bindTo": "TsB2BRetail",
						"caption": {"bindTo": "Resources.Strings.TsB2BCaption"}
					},
					"parentName": "CategoriesRetailControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2CRetail",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesRetailControlGroupContainer"
						},
						"bindTo": "TsB2CRetail",
						"caption": {"bindTo": "Resources.Strings.TsB2CCaption"}
					},
					"parentName": "CategoriesRetailControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "TsG2BRetail",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesRetailControlGroupContainer"
						},
						"bindTo": "TsG2BRetail",
						"caption": {"bindTo": "Resources.Strings.TsG2BCaption"}
					},
					"parentName": "CategoriesRetailControlGroupContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsVipRetail",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 3,
							"layoutName": "CategoriesRetailControlGroupContainer"
						},
						"bindTo": "TsVipRetail",
						"caption": {"bindTo": "Resources.Strings.TsVipCation"}
					},
					"parentName": "CategoriesRetailControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccCatInRetail",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 4,
							"layoutName": "CategoriesRetailControlGroupContainer"
						},
						"contentType": 3,
						"bindTo": "TsAccCatInRetail",
						"caption": {"bindTo": "Resources.Strings.TsStatusCaption"}
					},
					"parentName": "CategoriesRetailControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsExecutiveRetail",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 5,
							"layoutName": "CategoriesRetailControlGroupContainer"
						},
						"bindTo": "TsExecutiveRetail",
						"labelConfig": {},
						"enabled": true,
						"contentType": 5,
						"caption": {"bindTo": "Resources.Strings.TsExecutiveCaption"}
					},
					"parentName": "CategoriesRetailControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsExecSuppRetail",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 6,
							"layoutName": "CategoriesRetailControlGroupContainer"
						},
						"bindTo": "TsExecSuppRetail",
						"caption": {"bindTo": "Resources.Strings.TsExecSuppCaption"}
					},
					"parentName": "CategoriesRetailControlGroupContainer",
					"propertyName": "items",
					"index": 6
				}
				//#endregion
			]/**SCHEMA_DIFF*/,
			methods: {
				onEntityInitialized: function() {
					this.initTabRemoving();
					this.callParent(arguments);
					this.onOwnershipChanged();
					this.onTypeChanged();
					this.initKindOfBusinessCollection();
					this.initDefaultKindOfBusiness();
				},
				onTypeChanged: function() {
					var currentType = this.get("Type");
					if (currentType && currentType.value && currentType.value ===
						ConfigurationConstants.AccountType.OurCompany.toLowerCase()) {
						this.removeTabByKey("KindOfBusinessTab");
					} else {
						this.restoreTabByKey("KindOfBusinessTab");
					}
				},
				initKindOfBusinessCollection: function() {
					var collection = this.get("KindOfBusinessCollection");
					var buf = this.Ext.create("Terrasoft.BaseViewModelCollection");
					this.addKindOfBusinessElements(buf);
					collection.loadAll(buf);
				},

				addKindOfBusinessElements: function(collection) {
					var leadtypes = TsJsConsts.LeadType;
					collection.add(leadtypes.Advert.value, this.generateKindOfBusinessViewModel({
						Id: leadtypes.Advert.value,
						Caption: leadtypes.Advert.displayValue
					}));
					collection.add(leadtypes.Printing.value, this.generateKindOfBusinessViewModel({
						Id: leadtypes.Printing.value,
						Caption: leadtypes.Printing.displayValue
					}));
					collection.add(leadtypes.Subscribe.value, this.generateKindOfBusinessViewModel({
						Id: leadtypes.Subscribe.value,
						Caption: leadtypes.Subscribe.displayValue
					}));
					collection.add(leadtypes.Retail.value, this.generateKindOfBusinessViewModel({
						Id: leadtypes.Retail.value,
						Caption: leadtypes.Retail.displayValue
					}));
					collection.add(leadtypes.Parcel.value, this.generateKindOfBusinessViewModel({
						Id: leadtypes.Parcel.value,
						Caption: leadtypes.Parcel.displayValue
					}));
					collection.add(leadtypes.Fin.value, this.generateKindOfBusinessViewModel({
						Id: leadtypes.Fin.value,
						Caption: leadtypes.Fin.displayValue
					}));
				},

				generateKindOfBusinessViewModel: function(config) {
					return this.Ext.create("Terrasoft.BaseViewModel", {
						primaryColumnName: "Id",
						primaryDisplayColumnName: "Caption",
						columns: {
							Caption: {
								type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
								dataValueType: this.Terrasoft.DataValueType.TEXT
							},
							Id: {
								type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
								dataValueType: this.Terrasoft.DataValueType.GUID
							}
						},
						values: {
							Id: config.Id,
							Caption: config.Caption
						}
					});

				},

				initDefaultKindOfBusiness: function() {
					this.getUserKindOfBusiness(function(userKindOfBusiness) {
						var activeKindOfBusinessRow = !this.Ext.isEmpty(userKindOfBusiness) ? userKindOfBusiness :
							this.getDefaultKindOfBusiness();
						this.set("ActiveKindOfBusinessRow", activeKindOfBusinessRow);
					}, this);

				},

				getUserKindOfBusiness: function(callback, scope) {
					var businessKindEsq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
						rootSchemaName: "Contact"
					});
					businessKindEsq.addColumn("TsLeadType");
					businessKindEsq.filters.add("IdFilter", this.Terrasoft.createColumnFilterWithParameter(
						this.Terrasoft.ComparisonType.EQUAL, "Id", this.Terrasoft.SysValue.CURRENT_USER_CONTACT.value));
					businessKindEsq.getEntityCollection(function(response) {
						var userKindOfBusiness;
						if (response.success) {
							var collection = response.collection;
							if (collection.getCount() > 0) {
								userKindOfBusiness = collection.getByIndex(0).get("TsLeadType").value;
							}
						}
						callback.call(scope, userKindOfBusiness);
					});
				},

				getDefaultKindOfBusiness: function() {
					var collection = this.get("KindOfBusinessCollection");
					var firstItem = collection.getByIndex(0);
					return firstItem.get("Id");
				},

				onActiveKindOfBusinessRowChanged: function() {
					var activeRowId = this.get("ActiveKindOfBusinessRow");
					this.unselectAllLeadTypes();
					switch (activeRowId) {
						case TsJsConsts.LeadType.Advert.value:
							this.set("IsAdvertLeadTypeSelected", true);
							break;
						case TsJsConsts.LeadType.Subscribe.value:
							this.set("IsSubscribeLeadTypeSelected", true);
							break;
						case TsJsConsts.LeadType.Retail.value:
							this.set("IsRetailLeadTypeSelected", true);
							break;
						case TsJsConsts.LeadType.Parcel.value:
							this.set("IsParcelLeadTypeSelected", true);
							break;
						case TsJsConsts.LeadType.Fin.value:
							this.set("IsFinancialLeadTypeSelected", true);
							break;
						case TsJsConsts.LeadType.Printing.value:
							this.set("IsPrintingLeadTypeSelected", true);
							break;
					}
				},

				unselectAllLeadTypes: function() {
					this.set("IsAdvertLeadTypeSelected", false);
					this.set("IsSubscribeLeadTypeSelected", false);
					this.set("IsRetailLeadTypeSelected", false);
					this.set("IsParcelLeadTypeSelected", false);
					this.set("IsFinancialLeadTypeSelected", false);
					this.set("IsPrintingLeadTypeSelected", false);
				},

				onOwnershipChanged: function() {
					var ownership = this.get("Ownership");
					var isKPPVisible = true;
					if (ownership && ownership.value === TsJsConsts.AccountOwnership.IndividualEntrepreneur.value) {
						isKPPVisible = false;
					}
					this.set("IsKPPVisible", isKPPVisible);
				},

				getDataForFindDuplicatesService: function() {
					var communication = this.getCommunications();
					var email = this.get("Email");
					if (!this.Ext.isEmpty(email)) {
						communication.push({
							"Number": email,
							"CommunicationTypeId": ConfigurationConstants.CommunicationTypes.Email
						});
					}
					var data = {
						schemaName: this.entitySchemaName,
						request: {
							Id: this.get("Id"),
							Name: this.get("Name"),
							AlternativeName: this.get("AlternativeName"),
							Inn: this.get("TsINN") ? this.get("TsINN") : "0",
							Kpp: this.get("TsKPP"),
							Communication: communication
						}
					};
					return data;
				},

				findDuplicates: function(callback, scope) {
					var data = this.getDataForFindDuplicatesService();
					var methodName = this.getFindDuplicatesServiceMethodName();
					this.set("DuplicatesDataSend", data);
					this.set("FindDuplicatesMethodName", methodName);
					this.callService({
						data: data,
						serviceName: "TsSearchDuplicatesService",
						methodName: methodName,
						encodeData: false
					}, this.handleResponseFindDuplicatesService.bind(this, callback, scope), this);
				},

				handleResponseFindDuplicatesService: function(callback, scope, response) {
					var result = response[this.get("FindDuplicatesMethodName") + "Result"];
					if (result && result.length > 0) {
						this.set("FindDuplicatesResult", result);
						this.loadLocalDuplicateSearchPage();
						this.hideBodyMask();
					} else {
						var resultObject = {success: true};
						callback.call(scope, resultObject);
					}
				},

				destroy: function() {
					this.restoreAllTabs();
					this.callParent(arguments);
				},
				identificationNumberValidator: function() {
					var invalidMessage = "";
					var ownership = this.get("Ownership");
					var maxINNLength = (ownership && ownership.TsINNLength) ? ownership.TsINNLength : 10;
					if (!this.Ext.isEmpty(this.get("TsINN"))) {
						var innValue = this.get("TsINN");
						var currentINNLength = innValue.toString().length;
						var lengthPattern = new RegExp(".{" + 1 + "," + maxINNLength + "}", "g");
						var digitsPattern = /^\d+$/;
						if (innValue.match(lengthPattern) && innValue.match(lengthPattern).length > 1) {
							invalidMessage = this.Ext.String.format(this.get("Resources.Strings.IndividualCodeLengthExceeded"),
								currentINNLength, maxINNLength);
						}
						if (!innValue.match(digitsPattern)) {
							invalidMessage = this.get("Resources.Strings.DigitsException");
						}
					}

					return {
						fullInvalidMessage: invalidMessage,
						invalidMessage: invalidMessage
					};
				},

				setValidationConfig: function() {
					this.callParent(arguments);

					this.addColumnValidator("TsINN", this.identificationNumberValidator);
				}
			},
			rules: {
				"TsExecutiveAdvertBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecutiveFinancialBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecutiveParcelBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecutiveSubscribeServices": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecSuppAdvertBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecSuppSubscribeServices": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecSuppFinBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecSuppParcelBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecutivePrintingServices": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecSuppPrintingServices": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecutiveRetail": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecSuppRetail": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "[SysAdminUnit:Contact:Id].SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				}
			}
		};
	});
