define("ContactPageV2", ["BusinessRuleModule", "ConfigurationConstants"], function(BusinessRuleModule,
		ConfigurationConstants) {
	return {
		entitySchemaName: "Contact",
		attributes: {
			"IsContactEmployee": {
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
				dependencies: [
					{
						columns: ["Type"],
						methodName: "onTypeChanged"
					}
				],
				value: false
			},
			"TsAccountOldValue": {
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				dataValueType: this.Terrasoft.DataValueType.LOOKUP,
				dependencies: [
					{
						columns: ["TsAccount"],
						methodName: "onTsAccountChanged"
					}
				]
			}
		},
		methods: {
			onEntityInitialized: function() {
				this.callParent(arguments);
				this.actualizeAttributes();
			},
			actualizeAttributes: function(columnNames) {
				if (Ext.isEmpty(columnNames) || columnNames.includes("Type")) {
					this.set("IsContactEmployee", this.get("Type") && ConfigurationConstants.ContactType.Employee === this.get("Type").value);
				}
				if (Ext.isEmpty(columnNames)|| columnNames.includes("TsAccountOldValue")) {
					this.set("TsAccountOldValue", this.get("TsAccount"));
				}
			},
			onTypeChanged: function() {
				this.actualizeAttributes(["Type"]);
			},
			onTsAccountChanged: function() {
				var account = this.get("Account");
				var tsAccountOldValue = this.get("TsAccountOldValue");
				var tsAccount = this.get("TsAccount");

				if (account) {
					if (tsAccountOldValue && tsAccountOldValue.value === account.value) {
						this.set("Account", tsAccount);
					}
				} else {
					if (Ext.isEmpty(tsAccountOldValue)) {
						this.set("Account", tsAccount);
					}
				}
				this.actualizeAttributes(["TsAccountOldValue"]);
			}
		},
		rules: {
			"TsLeadType": {
				"RequireIfContactIsEmployee": {
					ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
					property: BusinessRuleModule.enums.Property.REQUIRED,
					conditions: [
						{
							"leftExpression": {
								"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								"attribute": "Type"
							},
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"rightExpression": {
								"type": BusinessRuleModule.enums.ValueType.CONSTANT,
								"value": ConfigurationConstants.ContactType.Employee
							}
						}
					]
				}
			},
			"TsTarifficatorRegion": {
				"RequireIfContactIsEmployee": {
					ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
					property: BusinessRuleModule.enums.Property.REQUIRED,
					conditions: [
						{
							"leftExpression": {
								"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								"attribute": "Type"
							},
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"rightExpression": {
								"type": BusinessRuleModule.enums.ValueType.CONSTANT,
								"value": ConfigurationConstants.ContactType.Employee
							}
						}
					]
				}
			},
			"TsAccount": {
				"RequireIfContactIsEmployee": {
					ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
					property: BusinessRuleModule.enums.Property.REQUIRED,
					conditions: [
						{
							"leftExpression": {
								"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								"attribute": "Type"
							},
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"rightExpression": {
								"type": BusinessRuleModule.enums.ValueType.CONSTANT,
								"value": ConfigurationConstants.ContactType.Employee
							}
						}
					]
				},
				"FiltrationByOurCompanyType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "Type",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.AccountType.OurCompany.toLowerCase()
					}
			}
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "JobInformationBlock",
				"propertyName": "items",
				"name": "TsResponsibilityField",
				"values": {
					"layout": {"column": 0, "row": 2, "colSpan": 12},
					"contentType": this.Terrasoft.ContentType.ENUM
				}
			},
			{
				"operation": "insert",
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"name": "ProfileFlexibleContainer",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.CONTAINER,
					"layout": {
						"column": 0,
						"row": 3,
						"colSpan": 24
					},
					"items": []
				}
			},
			{
				"operation": "move",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "Type",
				"index": 0
			},
			{
				"operation": "insert",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "TsLeadType",
				"values": {
					"visible": {bindTo: "IsContactEmployee"},
					"contentType": this.Terrasoft.ContentType.ENUM
				},
				"index": 1
			},
			{
				"operation": "insert",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "TsTarifficatorRegion",
				"values": {
					"visible": {bindTo: "IsContactEmployee"},
					"contentType": this.Terrasoft.ContentType.LOOKUP
				},
				"index": 2
			},
			{
				"operation": "insert",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "TsAccount",
				"values": {
					"visible": {bindTo: "IsContactEmployee"},
					"contentType": this.Terrasoft.ContentType.LOOKUP
				},
				"index": 3
			},
			{
				"operation": "move",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "JobTitleProfile",
				"index": 4
			},
			{
				"operation": "move",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "AccountMobilePhone",
				"index": 5
			},
			{
				"operation": "move",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "AccountPhone",
				"index": 6
			},
			{
				"operation": "move",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "AccountEmail",
				"index": 7
			}
		]/**SCHEMA_DIFF*/
	};
});
