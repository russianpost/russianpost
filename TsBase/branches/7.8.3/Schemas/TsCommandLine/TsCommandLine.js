define("TsCommandLine", [], function() {
	return Ext.define("Terrasoft.controls.TsCommandLine", {
		override: "Terrasoft.controls.CommandLine",
		changeValue: function(item, isFromSelectList) {
			var value = this.value;
			var isChanged;
			if (value !== null && item !== null) {
				isChanged = (value.value !== item.value) || (value.displayValue !== item.displayValue);
			} else {
				isChanged = value !== item;
			}
			if (isChanged) {
				this.value = item;
				this.fireEvent("changeValue", value);
				var returnValue = Terrasoft.deepClone(item);
				this.fireEvent("change", returnValue, this);
				this.typedValue = returnValue ? returnValue.displayValue : "";
				this.fireEvent("changeTypedValue", {
					typedValue: this.typedValue,
					executeCommand: isFromSelectList
				});
			}
			return isChanged;
		},

		onListElementSelected: function(item) {
			this.setValue(item, true);
			this.listView.hide();
		},
		setValue: function(value, isFromSelectList) {
			var isChanged = this.changeValue(value, isFromSelectList);
			this.setSelectedItem(value);
			if (this.rendered && isChanged) {
				var el = this.getEl();
				el.dom.value = value ? value.displayValue : "";
			}
		}
	});
});
