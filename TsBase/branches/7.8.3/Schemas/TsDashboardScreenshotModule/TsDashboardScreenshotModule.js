define("TsDashboardScreenshotModule", ["ModalBox", "BaseSchemaModuleV2","css!TsDashboardScreenshotModuleCSS"],
        function(ModalBox) {
    Ext.define("Terrasoft.configuration.TsDashboardScreenshotModule", {
        extend: "Terrasoft.BaseSchemaModule",
        alternateClassName: "Terrasoft.TsDashboardScreenshotModule",
        /**
         * @inheritDoc Terrasoft.BaseSchemaModule#generateViewContainerId
         * @overridden
         */
        generateViewContainerId: false,
        /**
         * @inheritDoc Terrasoft.BaseSchemaModule#initSchemaName
         * @overridden
         */
        initSchemaName: function() {
            this.schemaName = "TsDashboardScreenshotPage";
        },
        /**
         * @inheritDoc Terrasoft.BaseSchemaModule#initHistoryState
         * @overridden
         */
        initHistoryState: Terrasoft.emptyFn,
		createViewModel: function() {
			var viewModel = this.callParent(arguments);
			var parameters = this.parameters;
			if (parameters) {
				viewModel.set("СurrentDashboardName", parameters.СurrentDashboardName);
			}
			return viewModel;
		}
    });
    return Terrasoft.TsDashboardScreenshotModule;
});