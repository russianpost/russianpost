namespace Terrasoft.Configuration
{
	using System;

	public static class TsCsConstants
	{
		public static class Activity
		{
			public static class ActivityResult {
				public static readonly Guid Canceled = new Guid("DBBF0E10-F46B-1410-6598-00155D043204");
			}
			public static class ActivityStatus {
				public static readonly Guid Canceled = new Guid("201CFBA8-58E6-DF11-971B-001D60E938C6");
			}
			
			public static readonly Guid SchemaUId = new Guid("C449D832-A4CC-4B01-B9D5-8A12C42A9F89");
		}
	}

}

