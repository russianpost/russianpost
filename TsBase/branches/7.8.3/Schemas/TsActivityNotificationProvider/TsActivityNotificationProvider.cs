namespace Terrasoft.Configuration.TsBase
{
	using Terrasoft.Core.DB;
	using Terrasoft.Core;
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using Terrasoft.Core.Factories;

	#region Class: TsActivityNotificationProvider

	public class TsActivityNotificationProvider : BaseNotificationProvider, INotification
	{

		#region Fields: Private

		private const string GROUP_NAME = "Notification";
		private const string NAME = "Activity";

		#endregion

		#region Constructors: Public

		public TsActivityNotificationProvider(UserConnection userConnection)
			: base(userConnection) {
		}

		public TsActivityNotificationProvider(Dictionary<string, object> parameters) 
			: base(parameters) {
		}
		 
		#endregion

		#region Properties: Public

		
		
		
		public string Group {
			get {
				return GROUP_NAME;
			}
		}

		
		
		
		public string Name {
			get {
				return NAME;
			}
		}

		#endregion

		#region Methods: Private

		private string GetBody(Dictionary<string, string> dictionaryColumnValues) {
			var activityTitle = dictionaryColumnValues["Title"];
			var bodyWithoutCustomerTitle = string.Format("{0}", activityTitle);
			var body = bodyWithoutCustomerTitle;
			return body;
		}

		#endregion

		#region Methods: Protected

		protected override void AddColumns(Select select) {
			select
				.Column("Reminding", "Id").As("Id")
				.Column("Reminding", "RemindTime").As("RemindTime")
				.Column("Reminding", "ContactId").As("RemindingContactId")
				.Column("Reminding", "Description").As("Description")
				.Column("Activity", "Id").As("ActivityId")
				.Column("Activity", "Title").As("Title")
				.Column("Activity", "AccountId").As("AccountId")
				.Column("Activity", "AccountId").As("ContactId")
				.Column("Account", "Name").As("AccountName")
				.Column("Contact", "Name").As("ContactName")
				.Column("SysSchema", "Name").As("EntitySchemaName")
				.Column("NotificationsSettings", "SysImageId").As("ImageId");
		}
		
		protected override void JoinTables(Select select) {
			select
				.LeftOuterJoin("SysAdminUnit").On("Reminding", "ContactId").IsEqual("SysAdminUnit", "ContactId")
				.InnerJoin("Activity").On("Reminding", "SubjectId").IsEqual("Activity", "Id")
				.LeftOuterJoin("Account").On("Activity", "AccountId").IsEqual("Account", "Id")
				.LeftOuterJoin("Contact").On("Activity", "ContactId").IsEqual("Contact", "Id")
				.LeftOuterJoin("SysSchema").On("Reminding", "SysEntitySchemaId").IsEqual("SysSchema", "UId")
				.LeftOuterJoin("NotificationsSettings")
					.On("Reminding", "SysEntitySchemaId").IsEqual("NotificationsSettings", "SysEntitySchemaUId");
		}

		protected override void AddConditions(Select select) {
			base.AddConditions(select);
			select
				.And("Reminding", "NotificationTypeId")
					.IsEqual(Column.Parameter(RemindingConsts.NotificationTypeRemindingId))
				.And("Activity", "StatusId").Not().In(
					new Select(UserConnection).Column("ActivityStatus", "Id").From("ActivityStatus")
						.Where("ActivityStatus", "Finish").IsEqual(Column.Parameter(1))
				);
		}

		protected override INotificationInfo GetRecordNotificationInfo(Dictionary<string, string> dictionaryColumnValues) {
			string body = GetBody(dictionaryColumnValues);
			Guid imageId;
			Guid.TryParse(dictionaryColumnValues["ImageId"], out imageId);
			string title = UserConnection.GetLocalizableString("ActivityNotificationProvider", "Title");
			return new NotificationInfo() {
				Title = title,
				Body = body,
				ImageId = imageId,
				EntityId = new Guid(dictionaryColumnValues["ActivityId"]),
				EntitySchemaName = dictionaryColumnValues["EntitySchemaName"],
				MessageId = new Guid(dictionaryColumnValues["Id"]),
				SysAdminUnit = new Guid(dictionaryColumnValues["SysAdminUnitId"]),
				RemindTime = Convert.ToDateTime(dictionaryColumnValues["RemindTime"]),
				GroupName = Group
			};
		}

		#endregion

		#region Methods: Public

		public override void SetColumns(List<string> columns) {
			columns.Add("Id");
			columns.Add("ContactId");
			columns.Add("RemindTime");
			columns.Add("Title");
			columns.Add("ImageId");
		}

		public override string GetRecordResult(Dictionary<string, string> dictionaryColumnValues) {
			var title = UserConnection.GetLocalizableString("ActivityNotificationProvider", "Title");
			var id = dictionaryColumnValues["Id"];
			var remindTime = dictionaryColumnValues["RemindTime"];
			var imageId = dictionaryColumnValues["ImageId"];
			string body = GetBody(dictionaryColumnValues);
			var key = id + "_" + remindTime;
			var popup = new PopupData() {
				Title = title,
				Body = body, 
				ImageId = imageId, 
				EntityId = id,
				EntitySchemaName = "Activity"
			}; 
			var serializePopup = popup.Serialize();
			return string.Format("\"{0}\": {1}", key, serializePopup);
		}

		public override Select GetEntitiesSelect() {
			var activityUtils = Terrasoft.Core.Factories.ClassFactory.Get<TsActivityUtils>(new ConstructorArgument("userConnection", UserConnection));
			Guid subjectId = activityUtils.GetRenindingSubjectId();
			Guid sysAdminUnitId = (Guid)this.parameters["sysAdminUnitId"];
			DateTime date = (DateTime)this.parameters["dueDate"];
			
			var select = new Select(UserConnection)
				.Column("Reminding", "Id").As("Id")
				.Column("Reminding", "RemindTime").As("RemindTime")
				.Column("Reminding", "ContactId").As("ContactId")
				.Column("Reminding", "SubjectCaption").As("Title")
				.Column("NotificationsSettings", "SysImageId").As("ImageId")
				.From("Reminding")
				.LeftOuterJoin("SysAdminUnit").On("Reminding", "ContactId").IsEqual("SysAdminUnit", "ContactId")
				.LeftOuterJoin("NotificationsSettings")
					.On("Reminding", "SysEntitySchemaId").IsEqual("NotificationsSettings", "SysEntitySchemaUId")
				.Where("RemindTime").IsLessOrEqual(Column.Parameter(date.ToUniversalTime()))
				.And("Reminding", "NotificationTypeId")
					.IsEqual(Column.Parameter(RemindingConsts.NotificationTypeNotificationId)) //NotificationTypeRemindingId
				.And("SysAdminUnit", "Id").IsEqual(Column.Parameter(sysAdminUnitId))
				.And("Reminding", "SubjectId").IsEqual(Column.Parameter(subjectId))
				.And("Reminding", "IsRead").IsEqual(Column.Parameter(0)) as Select;
			
			/*
			var select = new Select(UserConnection)
				.Column("Reminding", "Id").As("Id")
				.Column("Reminding", "RemindTime").As("RemindTime")
				.Column("NotificationsSettings", "SysImageId").As("ImageId")
				.Distinct()
				.From("Reminding")
				.LeftOuterJoin("SysAdminUnit").On("Reminding", "ContactId").IsEqual("SysAdminUnit", "ContactId")
				.LeftOuterJoin("NotificationsSettings")
					.On("Reminding", "SysEntitySchemaId").IsEqual("NotificationsSettings", "SysEntitySchemaUId")
				.Where("RemindTime").IsLessOrEqual(Column.Parameter(date.ToUniversalTime()))
				.And("Reminding", "NotificationTypeId")
					.IsEqual(Column.Parameter(RemindingConsts.NotificationTypeNotificationId)) //NotificationTypeRemindingId
				.And("SysAdminUnit", "Id").IsEqual(Column.Parameter(sysAdminUnitId))
				.And("Reminding", "SubjectId").IsEqual(Column.Parameter(subjectId))
				.And("Reminding", "IsRead").IsEqual(Column.Parameter(0)) as Select;*/
			return select;
		}
		
		public IEnumerable<INotificationInfo> GetNotifications() {
			return GetNotificationsInfos();
		}

		#endregion
	}

	#endregion
}

