define("ContactMiniPage", ["BusinessRuleModule", "ConfigurationConstants"],
		function(BusinessRuleModule, ConfigurationConstants) {
			return {
				entitySchemaName: "Contact",
				attributes: {
					"IsContactEmployee": {
						type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
						dataValueType: this.Terrasoft.DataValueType.BOOLEAN,
						dependencies: [
							{
								columns: ["Type"],
								methodName: "onTypeChanged"
							}
						],
						value: false
					},
					"TsAccountOldValue": {
						type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
						dataValueType: this.Terrasoft.DataValueType.LOOKUP,
						dependencies: [
							{
								columns: ["TsAccount"],
								methodName: "onTsAccountChanged"
							}
						]
					}
				},
				methods: {
					onEntityInitialized: function() {
						this.callParent(arguments);
						this.actualizeAttributes();
					},
					actualizeAttributes: function(columnNames) {
						if (Ext.isEmpty(columnNames) || columnNames.includes("Type")) {
							this.set("IsContactEmployee", this.get("Type") && ConfigurationConstants.ContactType.Employee === this.get("Type").value);
						}
						if (Ext.isEmpty(columnNames)|| columnNames.includes("TsAccountOldValue")) {
							this.set("TsAccountOldValue", this.get("TsAccount"));
						}
					},
					onTypeChanged: function() {
						this.actualizeAttributes(["Type"]);
					},
					onTsAccountChanged: function() {
						var account = this.get("Account");
						var tsAccountOldValue = this.get("TsAccountOldValue");
						var tsAccount = this.get("TsAccount");

						if (account) {
							if (tsAccountOldValue && tsAccountOldValue.value === account.value) {
								this.set("Account", tsAccount);
							}
						} else {
							if (Ext.isEmpty(tsAccountOldValue)) {
								this.set("Account", tsAccount);
							}
						}
						this.actualizeAttributes(["TsAccountOldValue"]);
					}
				},
				rules: {
					"TsLeadType": {
						"RequireIfContactIsEmployee": {
							ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
							property: BusinessRuleModule.enums.Property.REQUIRED,
							conditions: [
								{
									"leftExpression": {
										"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
										"attribute": "Type"
									},
									"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
									"rightExpression": {
										"type": BusinessRuleModule.enums.ValueType.CONSTANT,
										"value": ConfigurationConstants.ContactType.Employee
									}
								}
							]
						}
					},
					"TsTarifficatorRegion": {
						"RequireIfContactIsEmployee": {
							ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
							property: BusinessRuleModule.enums.Property.REQUIRED,
							conditions: [
								{
									"leftExpression": {
										"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
										"attribute": "Type"
									},
									"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
									"rightExpression": {
										"type": BusinessRuleModule.enums.ValueType.CONSTANT,
										"value": ConfigurationConstants.ContactType.Employee
									}
								}
							]
						}
					},
					"TsAccount": {
						"RequireIfContactIsEmployee": {
							ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
							property: BusinessRuleModule.enums.Property.REQUIRED,
							conditions: [
								{
									"leftExpression": {
										"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
										"attribute": "Type"
									},
									"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
									"rightExpression": {
										"type": BusinessRuleModule.enums.ValueType.CONSTANT,
										"value": ConfigurationConstants.ContactType.Employee
									}
								}
							]
						},
						"FiltrationByOurCompanyType": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"autocomplete": false,
							"baseAttributePatch": "Type",
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"type": BusinessRuleModule.enums.ValueType.CONSTANT,
							"value": ConfigurationConstants.AccountType.OurCompany.toLowerCase()
						}
					}
				},
				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "insert",
						"parentName": "MiniPage",
						"propertyName": "items",
						"name": "ProfileFlexibleContainer",
						"values": {
							"isMiniPageModelItem": true,
							"itemType": this.Terrasoft.ViewItemType.CONTAINER,
							"visible": {"bindTo": "isNotViewMode"},
							"layout": {
								"column": 0,
								"row": 4,
								"colSpan": 24
							},
							"items": []
						}
					},
					{
						"operation": "insert",
						"parentName": "ProfileFlexibleContainer",
						"propertyName": "items",
						"name": "Job",
						"values": {
							"layout": {
								"column": 0,
								"row": 0,
								"colSpan": 24
							}
						},
						index: 0
					},
					{
						"operation": "insert",
						"parentName": "ProfileFlexibleContainer",
						"propertyName": "items",
						"name": "TsTarifficatorRegion",
						"values": {
							"isMiniPageModelItem": true,
							"visible": {bindTo: "IsContactEmployee"},
							"contentType": this.Terrasoft.ContentType.LOOKUP
						},
						"index": 0
					},
					{
						"operation": "insert",
						"parentName": "ProfileFlexibleContainer",
						"propertyName": "items",
						"name": "TsLeadType",
						"values": {
							"visible": {bindTo: "IsContactEmployee"},
							"contentType": this.Terrasoft.ContentType.ENUM
						},
						"index": 1
					},
					{
						"operation": "insert",
						"parentName": "ProfileFlexibleContainer",
						"propertyName": "items",
						"name": "TsAccount",
						"values": {
							"visible": {bindTo: "IsContactEmployee"},
							"contentType": this.Terrasoft.ContentType.LOOKUP
						},
						"index": 2
					},
					{
						"operation": "move",
						"name": "JobTitle",
						"parentName": "ProfileFlexibleContainer",
						"propertyName": "items",
						"values": {
							"layout": {
								"row": 6,
								"column": 0,
								"colSpan": 24
							}
						}
					},
					{
						"operation": "move",
						"name": "Department",
						"parentName": "ProfileFlexibleContainer",
						"propertyName": "items",
						"values": {
							"layout": {
								"row": 7,
								"column": 0,
								"colSpan": 24
							}
						}
					},
					{
						"operation": "move",
						"name": "MobilePhone",
						"parentName": "ProfileFlexibleContainer",
						"propertyName": "items",
						"values": {
							"layout": {
								"row": 8,
								"column": 0,
								"colSpan": 24
							}
						}
					},
					{
						"operation": "move",
						"name": "Email",
						"parentName": "ProfileFlexibleContainer",
						"propertyName": "items",
						"values": {
							"layout": {
								"row": 9,
								"column": 0,
								"colSpan": 24
							}
						}
					}
					,
					{
						"operation": "move",
						"name": "Account",
						"parentName": "ProfileFlexibleContainer",
						"propertyName": "items",
						"values": {
							"layout": {
								"row": 9,
								"column": 0,
								"colSpan": 24
							}
						}
					}

				]/**SCHEMA_DIFF*/
			};
		});
