using System;
using System.Collections.Generic;
using System.Data;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Scheduler;


namespace Terrasoft.Configuration.TsBase
{
    public class TsAccountUtils
    {
        #region Public: Properties

        public UserConnection UserConnection { get; private set; }

        public UserConnection SystemUserConnection
        {
            get { return this.UserConnection.AppConnection.SystemUserConnection; }
        }

        #endregion

        #region Filed: Protected

        protected List<String> AccountExecutiveColumnNames;

        #endregion

        #region Public: Constructors

        public TsAccountUtils(UserConnection userConnection)
        {
            UserConnection = userConnection;
        }

        #endregion

        #region Public: Methods

        public virtual void ScheduleChangeAccountExecutive(Guid oldExecutiveId, Guid newExecutiveId,
            Guid accountExecutiveRoleId)
        {
            if (oldExecutiveId == Guid.Empty)
            {
                throw new ArgumentException("oldExecutiveId mustn't be null");
            }

            if (newExecutiveId == Guid.Empty)
            {
                throw new ArgumentException("newExecutiveId mustn't be null");
            }

            var shedulerJobName = "TsChangeAccountExecutiveProcessJob_" + Guid.NewGuid();
            const string shedulerJobGroupName = "TsChangeAccountExecutiveGroup";
            const string jobProcessName = "TsChangeAccountExecutiveProcess";

            var processParameters = new Dictionary<string, object>()
            {
                {"OldOwnerId", oldExecutiveId},
                {"NewOwnerId", newExecutiveId},
                {"AccountExecutiveRoleId", accountExecutiveRoleId}
            };

            AppScheduler.ScheduleImmediateProcessJob(shedulerJobName, shedulerJobGroupName,
                jobProcessName, UserConnection.Workspace.Name, UserConnection.CurrentUser.Name, processParameters);
        }

        public List<ChangeExecutiveLog> ChangeAccountAndRelatedEntitiesExecutive(Guid oldExecutiveId,
            Guid newExecutiveId,
            Guid accountExecutiveRoleId)
        {
            if (oldExecutiveId == Guid.Empty)
            {
                throw new ArgumentException("oldExecutiveId mustn't be null");
            }
            if (newExecutiveId == Guid.Empty)
            {
                throw new ArgumentException("newExecutiveId mustn't be null");
            }

            Tuple<string, Guid> accountRoleDetails = GetAccountExecutiveRoleDetails(accountExecutiveRoleId);
            var accountColumnName = accountRoleDetails.Item1;
            var leadTypeId = accountRoleDetails.Item2;
            if (leadTypeId == Guid.Empty || string.IsNullOrEmpty(accountColumnName))
            {
                InitAccountExecutiveColumnNames();
            }

            return ChangeAccountAndRelatedEntitiesExecutive(oldExecutiveId, newExecutiveId, accountColumnName,
                leadTypeId);
            ;
        }

        protected virtual void InitAccountExecutiveColumnNames()
        {
            AccountExecutiveColumnNames = new List<string>();
            var selectAccountExecutiveRoleDetails =
                new Select(SystemUserConnection).From("TsAccountExecutiveRole")
                    .Column("TsAccountColumnName");
            using (DBExecutor executor = SystemUserConnection.EnsureDBConnection())
            {
                using (IDataReader reader = selectAccountExecutiveRoleDetails.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        AccountExecutiveColumnNames.Add(reader.GetColumnValue<string>("TsAccountColumnName"));
                    }
                }
            }
        }

        protected virtual List<ChangeExecutiveLog> ChangeAccountAndRelatedEntitiesExecutive(Guid oldExecutiveId,
            Guid newExecutiveId,
            string accountColumnName, Guid leadTypeId)
        {
            List<ChangeExecutiveLog> changeExecutiveLogList = new List<ChangeExecutiveLog>();

            ChangeExecutiveLog accountLog = ChangeAccountExecutive(oldExecutiveId, newExecutiveId, accountColumnName);
            if (accountLog != null)
            {
                changeExecutiveLogList.Add(accountLog);
            }

            ChangeExecutiveLog activitiesLog = ChangeOwnerInNotFinishedActivities(oldExecutiveId, newExecutiveId,
                leadTypeId);
            if (activitiesLog != null)
            {
                changeExecutiveLogList.Add(activitiesLog);
            }

            ChangeExecutiveLog contractsLog = ChangeOwnerInNotFinishedContracts(oldExecutiveId, newExecutiveId,
                leadTypeId);
            if (contractsLog != null)
            {
                changeExecutiveLogList.Add(contractsLog);
            }

            ChangeExecutiveLog opportunitiesLog = ChangeOwnerInNotFinishedOpportunities(oldExecutiveId, newExecutiveId,
                leadTypeId);
            if (opportunitiesLog != null)
            {
                changeExecutiveLogList.Add(opportunitiesLog);
            }

            ChangeExecutiveLog leadLog = ChangeOwnerInNotFinishedLeads(oldExecutiveId, newExecutiveId, leadTypeId);
            if (leadLog != null)
            {
                changeExecutiveLogList.Add(leadLog);
            }

            return changeExecutiveLogList;
        }

        #endregion

        #region Protected: Methods

        protected virtual Tuple<string, Guid> GetAccountExecutiveRoleDetails(Guid roleId)
        {
            if (roleId == Guid.Empty)
            {
                return new Tuple<string, Guid>(null, Guid.Empty);
            }
            var accountColumnName = string.Empty;
            var leadTypeId = Guid.Empty;
            var selectAccountExecutiveRoleDetails =
                new Select(SystemUserConnection).From("TsAccountExecutiveRole")
                    .Column("TsAccountColumnName")
                    .Column("TsLeadTypeId")
                    .Where("Id")
                    .IsEqual(Column.Parameter(roleId)) as Select;
            using (DBExecutor executor = SystemUserConnection.EnsureDBConnection())
            {
                using (IDataReader reader = selectAccountExecutiveRoleDetails.ExecuteReader(executor))
                {
                    while (reader.Read())
                    {
                        accountColumnName = reader.GetColumnValue<string>("TsAccountColumnName");
                        leadTypeId = reader.GetColumnValue<Guid>("TsLeadTypeId");
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(accountColumnName) || leadTypeId == Guid.Empty)
            {
                throw new ArgumentException("Inconsistency database at TsAccountExecutiveRole." +
                                            " Not filled LeadTypeId or AccountColumnName for Role with Id:" + roleId);
            }
            return new Tuple<string, Guid>(accountColumnName, leadTypeId);
        }

        protected virtual ChangeExecutiveLog ChangeOwnerInNotFinishedEntityTemplate(string entityName,
            Action<EntitySchemaQuery> configureEsq, Action<EntitySchemaQuery> addFiltersToEsq,
            Action<Entity> processEntity, EntityEnum entityEnum)
        {
            var entitySchemaQuery = new EntitySchemaQuery(SystemUserConnection.EntitySchemaManager, entityName);
            configureEsq(entitySchemaQuery);
            addFiltersToEsq(entitySchemaQuery);

            var entityCollection = entitySchemaQuery.GetEntityCollection(SystemUserConnection);
            var processedEntityCount = 0;

            foreach (Entity entity in entityCollection)
            {
                processEntity(entity);
                entity.Save();
                processedEntityCount++;
            }

            return new ChangeExecutiveLog
            {
                Entity = entityEnum,
                ChangedRowCount = processedEntityCount
            };
        }

        protected virtual ChangeExecutiveLog ChangeAccountExecutive(Guid oldExecutiveId, Guid newExecutiveId,
            string accountExecutiveColumnName = null)
        {
            return ChangeOwnerInNotFinishedEntityTemplate("Account",
                configureEsq: (accountsWithOldExecutiveQuery) =>
                {
                    accountsWithOldExecutiveQuery.Filters.LogicalOperation = LogicalOperationStrict.Or;
                    accountsWithOldExecutiveQuery.AddAllSchemaColumns();
                },
                addFiltersToEsq: (accountsWithOldExecutiveQuery) =>
                {
                    if (!string.IsNullOrWhiteSpace(accountExecutiveColumnName))
                    {
                        accountsWithOldExecutiveQuery.Filters.Add(accountsWithOldExecutiveQuery
                            .CreateFilterWithParameters(FilterComparisonType.Equal, accountExecutiveColumnName,
                                oldExecutiveId));
                    }
                    else
                    {
                        AccountExecutiveColumnNames.ForEach(
                            executiveColumnName =>
                                accountsWithOldExecutiveQuery.Filters.Add(accountsWithOldExecutiveQuery
                                    .CreateFilterWithParameters(
                                        FilterComparisonType.Equal, executiveColumnName,
                                        oldExecutiveId)));
                    }
                },
                processEntity: (entity) =>
                {
                    var account = entity as Account;
                    if (account == null) return;
                    if (!string.IsNullOrWhiteSpace(accountExecutiveColumnName))
                    {
                        account.SetColumnValue(
                            ConvertEntityLookupColumnNameToDatabaseColumnName(accountExecutiveColumnName),
                            newExecutiveId);
                    }
                    else
                    {
                        AccountExecutiveColumnNames.ForEach(executiveColumnName =>
                            {
                                var dbExecutiveColumnName =
                                    ConvertEntityLookupColumnNameToDatabaseColumnName(executiveColumnName);
                                if (account.GetTypedColumnValue<Guid>(dbExecutiveColumnName) == oldExecutiveId)
                                {
                                    account.SetColumnValue(dbExecutiveColumnName, newExecutiveId);
                                }
                            }
                        );
                    }
                    account.ModifiedById = UserConnection.CurrentUser.ContactId;
                }, entityEnum: EntityEnum.Account);
        }

        protected virtual ChangeExecutiveLog ChangeOwnerInNotFinishedActivities(Guid oldOwnerId, Guid newOwnerId,
            Guid leadTypeId = default(Guid))
        {
            return ChangeOwnerInNotFinishedEntityTemplate("Activity",
                configureEsq: (notFinishedActivitiesWithOldOwnerQuery) =>
                {
                    notFinishedActivitiesWithOldOwnerQuery.Filters.LogicalOperation = LogicalOperationStrict.And;
                    notFinishedActivitiesWithOldOwnerQuery.AddAllSchemaColumns();
                },
                addFiltersToEsq: (notFinishedActivitiesWithOldOwnerQuery) =>
                {
                    notFinishedActivitiesWithOldOwnerQuery.Filters.Add(notFinishedActivitiesWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "Owner", oldOwnerId));
                    notFinishedActivitiesWithOldOwnerQuery.Filters.Add(notFinishedActivitiesWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "Status.Finish", false));

                    if (leadTypeId == Guid.Empty)
                        return;
                    var filterGroup = new EntitySchemaQueryFilterCollection(notFinishedActivitiesWithOldOwnerQuery);
                    filterGroup.LogicalOperation = LogicalOperationStrict.Or;
                    filterGroup.Add(notFinishedActivitiesWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "Opportunity.LeadType", leadTypeId));
                    filterGroup.Add(notFinishedActivitiesWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "Contract.TsLeadType", leadTypeId));
                    filterGroup.Add(notFinishedActivitiesWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "Lead.LeadType", leadTypeId));
                },
                processEntity: (entity) =>
                {
                    var activity = entity as Activity;
                    if (activity != null)
                    {
                        activity.OwnerId = newOwnerId;
                        activity.ModifiedById = UserConnection.CurrentUser.ContactId;
                    }
                }, entityEnum: EntityEnum.Activity);
        }

        protected virtual ChangeExecutiveLog ChangeOwnerInNotFinishedContracts(Guid oldOwnerId, Guid newOwnerId,
            Guid leadTypeId = default(Guid))
        {
            return ChangeOwnerInNotFinishedEntityTemplate("Contract",
                configureEsq: (notFinishedContractsWithOldOwnerQuery) =>
                {
                    notFinishedContractsWithOldOwnerQuery.Filters.LogicalOperation = LogicalOperationStrict.And;
                    notFinishedContractsWithOldOwnerQuery.AddAllSchemaColumns();
                },
                addFiltersToEsq: (notFinishedContractsWithOldOwnerQuery) =>
                {
                    notFinishedContractsWithOldOwnerQuery.Filters.Add(notFinishedContractsWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "Owner", oldOwnerId));
                    notFinishedContractsWithOldOwnerQuery.Filters.Add(notFinishedContractsWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "State.TsIsFinal", false));

                    if (leadTypeId == Guid.Empty)
                        return;

                    notFinishedContractsWithOldOwnerQuery.Filters.Add(notFinishedContractsWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "TsLeadType", leadTypeId));
                },
                processEntity: (entity) =>
                {
                    var contract = entity as Contract;
                    if (contract != null)
                    {
                        contract.OwnerId = newOwnerId;
                        contract.ModifiedById = UserConnection.CurrentUser.ContactId;
                    }
                }, entityEnum: EntityEnum.Contract);
        }

        protected virtual ChangeExecutiveLog ChangeOwnerInNotFinishedOpportunities(Guid oldOwnerId, Guid newOwnerId,
            Guid leadTypeId = default(Guid))
        {
            return ChangeOwnerInNotFinishedEntityTemplate("Opportunity",
                configureEsq: (notFinishedOpportunitiesWithOldOwnerQuery) =>
                {
                    notFinishedOpportunitiesWithOldOwnerQuery.Filters.LogicalOperation = LogicalOperationStrict.And;
                    notFinishedOpportunitiesWithOldOwnerQuery.AddAllSchemaColumns();
                },
                addFiltersToEsq: (notFinishedOpportunitiesWithOldOwnerQuery) =>
                {
                    notFinishedOpportunitiesWithOldOwnerQuery.Filters.Add(notFinishedOpportunitiesWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "Owner", oldOwnerId));
                    notFinishedOpportunitiesWithOldOwnerQuery.Filters.Add(notFinishedOpportunitiesWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "Stage.End", false));

                    if (leadTypeId == Guid.Empty)
                        return;

                    notFinishedOpportunitiesWithOldOwnerQuery.Filters.Add(notFinishedOpportunitiesWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "LeadType", leadTypeId));
                },
                processEntity: (entity) =>
                {
                    var opportunity = entity as Opportunity;
                    if (opportunity != null)
                    {
                        opportunity.OwnerId = newOwnerId;
                        opportunity.ModifiedById = UserConnection.CurrentUser.ContactId;
                    }
                }, entityEnum: EntityEnum.Opportunity);
        }

        protected virtual ChangeExecutiveLog ChangeOwnerInNotFinishedLeads(Guid oldOwnerId, Guid newOwnerId,
            Guid leadTypeId = default(Guid))
        {
            return ChangeOwnerInNotFinishedEntityTemplate("Lead",
                configureEsq: (notFinishedLeadsWithOldOwnerQuery) =>
                {
                    notFinishedLeadsWithOldOwnerQuery.Filters.LogicalOperation = LogicalOperationStrict.And;
                    notFinishedLeadsWithOldOwnerQuery.AddAllSchemaColumns();
                },
                addFiltersToEsq: (notFinishedLeadsWithOldOwnerQuery) =>
                {
                    notFinishedLeadsWithOldOwnerQuery.Filters.Add(notFinishedLeadsWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "Owner", oldOwnerId));
                    notFinishedLeadsWithOldOwnerQuery.Filters.Add(notFinishedLeadsWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "QualifyStatus.IsFinal", false));

                    if (leadTypeId == Guid.Empty)
                        return;

                    notFinishedLeadsWithOldOwnerQuery.Filters.Add(notFinishedLeadsWithOldOwnerQuery
                        .CreateFilterWithParameters(
                            FilterComparisonType.Equal, "LeadType", leadTypeId));
                },
                processEntity: (entity) =>
                {
                    var lead = entity as Lead;
                    if (lead != null)
                    {
                        lead.OwnerId = newOwnerId;
                        lead.ModifiedById = UserConnection.CurrentUser.ContactId;
                    }
                }, entityEnum: EntityEnum.Lead);
        }

        #endregion

        private string ConvertEntityLookupColumnNameToDatabaseColumnName(string entityColumnName)
        {
            return entityColumnName + "Id";
        }
    }

    public class ChangeExecutiveLog
    {
        public int ChangedRowCount { get; set; }
        public EntityEnum Entity { get; set; }
    }

    public enum EntityEnum
    {
        Account,
        Activity,
        Opportunity,
        Contract,
        Lead
    }
}