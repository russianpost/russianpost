define("TsForecastsModule", ["ext-base", "BaseSchemaModuleV2", "ForecastsModule", "TsForecastBuilder",
		"css!TsForecastsModule"],
	function() {
		Ext.define("Terrasoft.configuration.TsForecastsModule", {
			extend: "Terrasoft.ForecastsModule",
			alternateClassName: "Terrasoft.TsForecastsModule",

			viewModelClassName: "Terrasoft.TsBaseForecastsViewModel",

			builderClassName: "Terrasoft.TsForecastBuilder",

			viewConfigClass: "Terrasoft.TsForecastsViewConfig"
		});
		return Terrasoft.TsForecastsModule;
	});
