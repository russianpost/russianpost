/*Измененные методы:
 Terrasoft.CommandLineModuleViewModel.onChangeTypedValue
 *
 * */
/*jshint ignore:start */
define("CommandLineModule", ["CommandLineModuleResources", "StorageUtilities", "ProcessModuleUtilities",
			"performancecountermanager", "ConfigurationConstants", "TooltipUtilities", "ViewGeneratorV2", "TsCommandLine"],
		function(resources, storageUtilities, ProcessModuleUtilities, performanceCounterManager,
				ConfigurationConstants) {
			Ext.define("Terrasoft.configuration.CommandLineModuleViewModel", {
				extend: "Terrasoft.BaseViewModel",
				alternateClassName: "Terrasoft.CommandLineModuleViewModel",
				mixins: {
					TooltipUtilitiesMixin: "Terrasoft.TooltipUtilities"
				},

				clearData: function() {
					this.set("selectedValue", "");
					this.set("selectedItem", null);
					this.set("commandInsertValue", null);
				},

				valueChanged: function(item) {
					if (Terrasoft.isEmptyObject(item) || !item.value) {
						return;
					}
					var value = item.value;
					var valuePart = value.substr(0, 4);
					var commandList = this.commandList;
					if (valuePart === "comm" && commandList.contains(value)) {
						this.set("selectedItem", commandList.get(value));
						this.set("commandSelectionText", "");
					}
					if (valuePart === "hint" && this.hintList.contains(value)) {
						this.set("commandSelectionText", "");
					}
				},

				onChangeTypedValue: function(config) {
					if (Ext.isObject(config)) {
						var item = config.typedValue;
					}
					else {
						item = config
					}

					this.set("commandSelectionText", "");
					var selectItem = this.get("selectedItem");
					this.set("selectedValue", null);
					if (item && selectItem) {
						if (item.toLowerCase().indexOf(selectItem.Caption.toLowerCase()) === -1) {
							this.set("selectedItem", null);
							selectItem = this.get("selectedItem");
						}
					}
					var filter = item;
					var commandList = this.commandList;
					if (!selectItem) {
						var filteredCommandList = commandList.filter(
								function(item) {
									if (item.Caption.toLowerCase().indexOf(filter.toLowerCase().trim()) === 0 &&
											item.Caption.length === filter.length) {
										return true;
									}
								}
						);
						if (filteredCommandList.getCount() === 1) {
							selectItem = filteredCommandList.getByIndex(0);
							this.set("selectedItem", selectItem);
						}
					}

					if (item && selectItem && selectItem.Code == "search" && config.executeCommand) {
						this.executeCommand(item);
					}
				},

				loadCommandListFromSelectItem: function(selectItem, filter) {
					var obj = {};
					obj["comm" + selectItem.Id] = {
						value: "comm" + selectItem.Id,
						displayValue: selectItem.Caption
					};
					var suggestString = "";
					var newSuggestion = selectItem.Caption;
					if (newSuggestion.toLowerCase().indexOf(filter.toLowerCase()) === 0) {
						suggestString = newSuggestion.substring(filter.length, newSuggestion.length);
					}
					this.set("commandSelectionText", suggestString);
					this.get("commandList").clear();
					this.get("commandList").loadAll(obj);
				}

			});

			function createConstructor(context) {
				var topCount = 10;
				var Ext = context.Ext;
				var sandbox = context.sandbox;
				var Terrasoft = context.Terrasoft;

				function getIsDefaultCommandItem(item, schemaName) {
					return (item.Code === "search" && item.HintType === ConfigurationConstants.CommandLineHintType.Command
					&& item.SubjectName === schemaName);
				}

				function safeAddToList(list, tempList) {
					if (!tempList.isEmpty()) {
						var keys = tempList.getKeys();
						var items = tempList.getItems();
						list.add(keys[0], items[0]);
					}
				}

				function getDefaultCList(commandList, currentSchemaName) {
					var list = new Terrasoft.Collection();
					var tempList = commandList.filter(function(item) {
						return getIsDefaultCommandItem(item, currentSchemaName);
					});
					safeAddToList(list, tempList);
					if (currentSchemaName !== "Contact") {
						tempList = commandList.filter(function(item) {
							return getIsDefaultCommandItem(item, "Contact");
						});
						safeAddToList(list, tempList);
					}
					if (currentSchemaName !== "Account") {
						tempList = commandList.filter(function(item) {
							return getIsDefaultCommandItem(item, "Account");
						});
						safeAddToList(list, tempList);
					}
					return list;
				}

				function tryGetModule(entitySchemaName) {
					var result = null;
					var key = entitySchemaName === "SocialMessage" ? "ESNFeed" : entitySchemaName;
					Terrasoft.each(Terrasoft.configuration.ModuleStructure, function(item) {
						if (item && item.entitySchemaName && item.entitySchemaName === key) {
							result = item;
						}
					}, this);
					return result;
				}

				function tryGetUrl(entitySchemaName, urlType, columnTypeCode, mode) {
					var url = "";
					switch (urlType) {
						case "section":
							url = getSectionUrl({entitySchemaName: entitySchemaName});
							break;
						case "card":
							url = getCardUrl({
								entitySchemaName: entitySchemaName,
								cardType: columnTypeCode,
								mode: mode
							});
							break;
						default :
							break;
					}
					return url;
				}

				function getCardUrl(config) {
					var module = tryGetModule(config.entitySchemaName);
					if (module.cardSchema) {
						var cardSchemaParams = getCardSchemaParams(config, module);
						var cardSchemaName = cardSchemaParams.cardSchema || module.cardSchema;
						var url = Ext.String.format("{0}/{1}/{2}", module.cardModule, cardSchemaName, config.mode);
						var cardType = config.cardType;
						if (cardType && cardType === cardSchemaParams.name) {
							url += Ext.String.format("/{0}/{1}", cardType, cardSchemaParams.UId);
						}
						return url;
					} else {
						return getSectionUrl(config);
					}
				}

				function getCardSchemaParams(config, module) {
					var attribute = module.attribute;
					var cardType = config.cardType;
					if (cardType && attribute) {
						var pages = module.pages;
						for (var i = 0; i < pages.length; i++) {
							var page = pages[i];
							if (page.name === cardType || page.UId === cardType) {
								return page;
							}
						}
					}
					return module;
				}

				function getSectionUrl(config) {
					var module = tryGetModule(config.entitySchemaName);
					var url = module.sectionModule + "/";
					if (module.sectionSchema) {
						url += module.sectionSchema + "/";
					}
					return url;
				}

				function getOpenCardConfig(schemaName, config) {
					var moduleStructure = Terrasoft.configuration.ModuleStructure[schemaName];
					var values = {
						TypeColumnName: "",
						TypeColumnValue: Terrasoft.GUID_EMPTY,
						EditPageName: moduleStructure.cardSchema
					};
					var attribute = moduleStructure.attribute;
					var columnTypeCode = config.columnTypeCode;
					if (columnTypeCode && attribute) {
						var pages = moduleStructure.pages;
						Terrasoft.each(pages, function(page) {
							if (page.name === columnTypeCode) {
								if (page.cardSchema) {
									values.EditPageName = page.cardSchema;
								}
								values.TypeColumnName = attribute;
								values.TypeColumnValue = page.UId;
								return false;
							}
						}, this);
					}
					var primaryDisplayColumnValue = config.value;
					var primaryDisplayColumnName = config.columnName;
					if (primaryDisplayColumnName && primaryDisplayColumnValue) {
						values.PrimaryDisplayColumnName = primaryDisplayColumnName;
						values.PrimaryDisplayColumnValue = primaryDisplayColumnValue;
					}
					return Ext.create("Terrasoft.BaseViewModel", {values: values});
				}

				function executeCommand(command, mainParam, addParam) {
					switch (command) {
						case "goto":
							executeGoToCommand({
								mainParam: mainParam,
								addParam: addParam
							});
							break;
						case "search":
							executeSearchCommand({
								mainParam: mainParam,
								addParam: addParam
							});
							break;
						case "create":
							executeCreateCommand({
								mainParam: mainParam,
								addParam: addParam
							});
							break;
						case "startbp":
							if (addParam.value && addParam.valueId) {
								executeProcess(addParam.valueId);
							}
							break;
						default:
							break;
					}
				}

				function getSectionSessionFiltersKey(entityName) {
					var sessionFiltersKey = entityName + "SectionV2";
					var moduleStructure = Terrasoft.configuration.ModuleStructure[entityName];
					if (moduleStructure && moduleStructure.sectionSchema) {
						sessionFiltersKey = moduleStructure.sectionSchema;
					}
					return sessionFiltersKey;
				}

				function executeGoToCommand(config) {
					var mainParam = config.mainParam;
					var addParam = config.addParam;
					var newState, state, currentState, filterState;
					var url = tryGetUrl(mainParam, "section");
					if (addParam.value && addParam.valueId) {
						var filtersStorage = Terrasoft.configuration.Storage.Filters =
								Terrasoft.configuration.Storage.Filters || {};
						var sessionFiltersKey = getSectionSessionFiltersKey(mainParam);
						var sessionStorageFilters = filtersStorage[sessionFiltersKey] =
								filtersStorage[sessionFiltersKey] || {};
						var filter = Terrasoft.deserialize(addParam.value);
						sessionStorageFilters.FolderFilters = [filter];
						state = sandbox.publish("GetHistoryState");
						currentState = state.state || {};
						newState = Terrasoft.deepClone(currentState);
						newState.filterState = filterState;
						sandbox.publish("PushHistoryState", {
							hash: url,
							stateObj: newState
						});
					} else {
						sandbox.publish("PushHistoryState", {hash: url});
					}
				}

				function executeSearchCommand(config) {
					var mainParam = config.mainParam;
					var addParam = config.addParam;
					var newState, state, currentState, filterState, url;
					if (addParam.valueId) {
						var typeId = addParam.typeId;
						url = tryGetUrl(mainParam, "card", typeId, "edit") + "/" + addParam.valueId;
						sandbox.publish("PushHistoryState", {hash: url});
					} else if (addParam.value) {
						url = tryGetUrl(mainParam, "section");
						filterState = {};
						filterState.ignoreFixedFilters = true;
						filterState.ignoreFolderFilters = true;
						filterState.customFilterState = {};
						filterState.customFilterState[addParam.columnName] = {
							value: addParam.value,
							displayValue: addParam.value
						};
						state = sandbox.publish("GetHistoryState");
						currentState = state.state || {};
						newState = Terrasoft.deepClone(currentState);
						newState.activeTab = "mainView";
						newState.filterState = filterState;
						newState.searchState = true;
						newState.moduleId = "ViewModule_SectionModule";
						var tryFilterCurrentSection = sandbox.publish("FilterCurrentSection", {
							value: addParam.value,
							displayValue: addParam.value,
							schemaName: addParam.noSchema ? "" : mainParam
						});
						if (!tryFilterCurrentSection) {
							var storage = Terrasoft.configuration.Storage.Filters =
									Terrasoft.configuration.Storage.Filters || {};
							var sessionFiltersKey = getSectionSessionFiltersKey(mainParam);
							var sessionFilters = storage[sessionFiltersKey] = storage[sessionFiltersKey] || {};
							sessionFilters.CustomFilters = {
								value: addParam.value,
								displayValue: addParam.value,
								primaryDisplayColumn: true
							};
							sandbox.publish("PushHistoryState", {
								hash: url,
								stateObj: newState
							});
						}
					} else {
						url = tryGetUrl(mainParam, "section");
						sandbox.publish("PushHistoryState", {hash: url});
					}
				}

				function executeCreateCommand(config) {
					var mainParam = config.mainParam;
					var addParam = config.addParam;
					var newState, state, currentState;
					if (mainParam === "Macros") {
						var url = "MacrosPageModule";
						state = sandbox.publish("GetHistoryState");
						currentState = state.state || {};
						newState = Terrasoft.deepClone(currentState);
						var defaultValues = {};
						if (addParam.columnName) {
							defaultValues[addParam.columnName] = addParam.value;
						}
						newState.defaultValues = defaultValues;
						var obj = {
							hash: url,
							stateObj: newState
						};
						sandbox.publish("PushHistoryState", obj);
					} else {
						var addCardConfig = getOpenCardConfig(mainParam, addParam);
						addCardConfig.set("Tag", sandbox.id);
						addCardConfig.set("EntitySchemaName", mainParam);
						require(["SysModuleEditManageModule"], function(module) {
							if (module) {
								module.Run({
									sandbox: sandbox,
									item: addCardConfig
								});
							}
						});
					}
				}

				function generateTip(config) {
					var viewGenerator = Ext.create("Terrasoft.ViewGenerator");
					var tip = viewGenerator.generatePartial(Ext.merge({
						itemType: Terrasoft.ViewItemType.TIP
					}, config), {
						schemaName: "CommandLineModule",
						schema: {},
						viewModelClass: Ext.ClassManager.get("Terrasoft.CommandLineModuleViewModel")
					})[0];
					return tip;
				}

				function createViewConfig() {
					var commandLineTip = generateTip({
						content: resources.localizableStrings.CommandLineTip,
						className: "Terrasoft.ContextTip",
						contextIdList: ["0", "IntroPage"],
						behaviour: {
							displayEvent: Terrasoft.controls.TipEnums.displayEvent.NONE
						}
					});
					return {
						className: "Terrasoft.Container",
						id: "commandLineContainer",
						selectors: {
							el: "#commandLineContainer",
							wrapEl: "#commandLineContainer"
						},
						items: [
							{
								className: "Terrasoft.CommandLine",
								bigSize: true,
								placeholder: resources.localizableStrings.WhatCanIDoForYou,
								value: {bindTo: "selectedValue"},
								list: {bindTo: "commandList"},
								typedValueChanged: {bindTo: "getCommandList"},
								changeTypedValue: {bindTo: "onChangeTypedValue"},
								typedValue: {bindTo: "commandInsertValue"},
								selectionText: {bindTo: "commandSelectionText"},
								commandLineExecute: {bindTo: "executeCommand"},
								change: {bindTo: "valueChanged"},
								markerValue: "command-line",
								minSearchCharsCount: 3,
								searchDelay: 350,
								width: "100%",
								tips: [
									{
										tip: {
											content: resources.localizableStrings.GoButtonHint,
											markerValue: resources.localizableStrings.GoButtonHint,
											style: Terrasoft.controls.TipEnums.style.GREEN,
											displayMode: Terrasoft.controls.TipEnums.displayMode.NARROW
										},
										settings: {
											displayEvent: Terrasoft.controls.TipEnums.displayEvent.HOVER,
											alignEl: "rightIconWrapperEl"
										}
									},
									commandLineTip
								]
							}
						]
					};
				}

				function createViewModel() {
					var viewModel = Ext.create("Terrasoft.CommandLineModuleViewModel", {
						values: {
							selectedValue: null,
							commandInsertValue: null,
							commandSelectionText: "",
							selectedItem: null,
							commandList: new Terrasoft.Collection(),
							hintList: new Terrasoft.Collection()
						}
					});
					return viewModel;
				}

				function callServiceMethod(ajaxProvider, methodName, callback, dataSend) {
					var data = dataSend || {};
					var requestUrl = Terrasoft.workspaceBaseUrl + "/rest/CommandLineService/" + methodName;
					var request = ajaxProvider.request({
						url: requestUrl,
						headers: {
							"Accept": "application/json",
							"Content-Type": "application/json"
						},
						method: "POST",
						jsonData: data,
						callback: function(request, success, response) {
							var responseObject = {};
							if (success) {
								responseObject = Terrasoft.decode(response.responseText);
							}
							callback.call(this, responseObject);
						},
						scope: this
					});
					return request;
				}

				function executeProcess(processName) {
					ProcessModuleUtilities.executeProcess({
						sysProcessName: processName
					});
				}

				function init(callbackList) {
					this.commandList = new Terrasoft.Collection();
					this.commandsCollection = new Terrasoft.Collection();
					this.mainParamCollection = new Terrasoft.Collection();
					this.hintList = new Terrasoft.Collection();
					this.selectedValue = null;
					var commandList = this.commandList;
					var commandsCollection = this.commandsCollection;
					var mainParamCollection = this.mainParamCollection;
					var ajaxProvider = this.ajaxProvider = Terrasoft.AjaxProvider;
					var serviceCallback = function(response) {
						if (!instance || instance.isDestroyed) {
							return;
						}
						var list = commandList;
						var listCommands = commandsCollection;
						var listMainParam = mainParamCollection;
						var responseArray = response.GetCommandListResult;
						if (responseArray) {
							responseArray.forEach(function(item) {
								list.add("comm" + item.Id, item);
								if (item.HintType === ConfigurationConstants.CommandLineHintType.Command
										|| item.HintType === ConfigurationConstants.CommandLineHintType.Synonym) {
									var command = item.CommandCaption;
									var mainParam = item.MainParamCaption;
									if (!listCommands.contains(command)) {
										listCommands.add(command, {
											Code: item.Code,
											Synonym: item.HintType === ConfigurationConstants.CommandLineHintType.Synonym
										});
									}
									if (!listMainParam.contains(mainParam)) {
										listMainParam.add(mainParam, {
											SubjectName: item.SubjectName,
											ColumnName: item.ColumnName,
											ColumnTypeCode: item.ColumnTypeCode
										});
									}
								}
							});
						}
						if (callbackList) {
							callbackList(this.commandList);
						}
					};
					var keyGenerator = function(ajaxProvider, methodName) {
						return {
							groupName: "CommandLineStorage",
							valueKey: methodName
						};
					};
					var requestFunction = function(callback, ajaxProvider, methodName, dataSend) {
						callServiceMethod(ajaxProvider, methodName, callback, dataSend);
					};
					storageUtilities.workRequestWithStorage(keyGenerator, requestFunction, serviceCallback, this, ajaxProvider,
							"GetCommandList", serviceCallback);
				}

				function render(renderTo) {
					var container = this.renderTo = renderTo;
					if (!container.dom) {
						return;
					}
					var ajaxProvider = this.ajaxProvider;
					var commandList = this.commandList;
					var commandsCollection = this.commandsCollection;
					var mainParamCollection = this.mainParamCollection;
					var hintList = this.hintList;
					var currentSchemaName = sandbox.publish("GetSectionSchemaName");
					if (!currentSchemaName) {
						currentSchemaName = sandbox.publish("GetCardSchemaName");
					}
					sandbox.subscribe("ChangeCommandList", function() {
						var callbackList = function(list) {
							commandList = list;
						};
						init(callbackList);
					});
					var viewConfig = createViewConfig();
					var view = Ext.create(viewConfig.className || "Terrasoft.Container", viewConfig);
					var viewModel = createViewModel();
					viewModel.schemaName = currentSchemaName;
					viewModel.commandList = commandList;
					viewModel.hintList = hintList;
					viewModel.getCommandList = function(filter) {
						var filteredCommandListByCaption = function(caption) {
							var canBeCommand = !commandsCollection.filter(function(item, key) {
								return (key.toLowerCase().indexOf(caption.toLowerCase()) === 0) && !item.Synonym;
							}).isEmpty();
							var canBeMainParam = !mainParamCollection.filter(function(item, key) {
								return key.toLowerCase().indexOf(filter.toLowerCase()) === 0;
							}).isEmpty();
							var needSynonyms = !(canBeCommand || canBeMainParam);
							var filteredList = commandList.filter(function(item) {
								if (item.Caption.toLowerCase().indexOf(filter.toLowerCase()) > -1) {
									if (item.HintType === ConfigurationConstants.CommandLineHintType.Macros ||
											item.HintType === ConfigurationConstants.CommandLineHintType.Command) {
										return true;
									}
									if (item.HintType === ConfigurationConstants.CommandLineHintType.Synonym && needSynonyms) {
										return true;
									}
								}
								return false;
							});
							filteredList.sort("MainParamCaption", Terrasoft.OrderDirection.ASC);
							filteredList.sort(null, null, function(obj1) {
								if (obj1.SubjectName === schemaName) {
									return -1;
								} else {
									return 1;
								}
							});
							var newCaption = "";
							if (filteredList.isEmpty()) {
								filteredList = getDefaultCList(commandList, currentSchemaName);
								newCaption = filter;
							}
							filteredList.each(function(item, index) {
								if (index > topCount) {
									return false;
								}
								obj["comm" + item.Id] = {
									value: "comm" + item.Id,
									displayValue: newCaption ? item.Caption + " " + newCaption : item.Caption
								};
							});
							suggestString = "";
							if (!filteredList.isEmpty()) {
								newSuggestion = filteredList.getByIndex(0).Caption;
								if (newSuggestion.toLowerCase().indexOf(filter.toLowerCase()) === 0) {
									suggestString = newSuggestion.substring(filter.length, newSuggestion.length);
								}
							}
							this.set("commandSelectionText", suggestString);
							this.get("commandList").clear();
							this.get("commandList").loadAll(obj);
						}.bind(this);
						var list = this.get("commandList");
						list.clear();
						var lastRequest, suggestString, newSuggestion;
						if (!filter) {
							return;
						}
						var obj = {};
						var selectItem = this.get("selectedItem");
						var schemaName = this.schemaName;
						if (!selectItem) {
							filteredCommandListByCaption(filter);
						} else {
							hintList.clear();
							var viewModel = this;
							var hintText = (filter.substr(selectItem.Caption.length, filter.length - 1)).trim();
							var subjectName = "";
							var hideHint = selectItem.Code === "create" || !Ext.isEmpty(selectItem.AdditionalParamValue);
							if (selectItem.Code === "goto") {
								subjectName = selectItem.SubjectName + "Folder";
							} else if (selectItem.Code === "run") {
								subjectName = "runnableHint";
							} else {
								subjectName = selectItem.SubjectName;
							}
							if (!hideHint) {
								if (lastRequest) {
									ajaxProvider.abort(lastRequest);
								}
								lastRequest = callServiceMethod(ajaxProvider, "GetHintList", function(response) {
									if (!instance || instance.isDestroyed) {
										return;
									}
									var boxList = list;
									var hints = hintList;
									var hintArray = response.GetHintListResult;
									if (Ext.isEmpty(hintArray)) {
										viewModel.loadCommandListFromSelectItem(selectItem, filter);
										return;
									}
									for (var i = 0; i < hintArray.length && i <= topCount; i++) {
										var uniqueKey = Terrasoft.generateGUID();
										var displayValue = selectItem.Caption + " " + hintArray[i].Key;
										var value = Terrasoft.deepClone(selectItem);
										value.AdditionalParamValue = hintArray[i].Key;
										var additionalParamValueId = hintArray[i].Value;
										try {
											var params = Terrasoft.deserialize(additionalParamValueId);
											value.AdditionalParamValueId = params.value;
											value.TypeId = params.typeId;
										} catch (e) {
											value.AdditionalParamValueId = additionalParamValueId;
										}
										hints.add("hint" + uniqueKey, value);
										obj["hint" + uniqueKey] = {
											value: "hint" + uniqueKey,
											displayValue: displayValue
										};
									}
									suggestString = "";
									if (hintArray.length > 0) {
										newSuggestion = selectItem.Caption + " " + hintArray[0].Key;
										if (newSuggestion.toLowerCase().indexOf(filter.toLowerCase()) === 0) {
											suggestString = newSuggestion.substring(filter.length, newSuggestion.length);
										}
									}
									var commandInsertValue = viewModel.get("commandInsertValue");
									if (commandInsertValue !== selectItem.Caption) {
										viewModel.set("commandSelectionText", suggestString);
									}
									boxList.clear();
									boxList.loadAll(obj);
								}, {"subjectName": subjectName, "hintText": hintText});
							} else {
								if (selectItem) {
									viewModel.loadCommandListFromSelectItem(selectItem, filter);
								}
							}
						}
					};
					viewModel.executeCommand = function(input) {
						if (!input) {
							return;
						}
						var suggestion = this.get("commandSelectionText");
						if (suggestion) {
							input += suggestion;
						}
						var inputArray = input.split(" ");
						var command, mainParam, valueId, columnName, tempList, subjectName, columnTypeCode, macros;
						var i = 0;
						var additionValue = "";
						var macrosText = "";
						var predicateFilterEntry = function(item) {
							return item.Caption.toLowerCase().indexOf(macrosText.toLowerCase()) === 0;
						};
						var predicateFilterMatch = function(item) {
							return item.Caption.toLowerCase() === macrosText.toLowerCase();
						};
						while (i < inputArray.length) {
							while (!macros && inputArray.length > i) {
								macrosText += " " + inputArray[i++];
								macrosText = macrosText.trim();
								tempList = commandList.filter(predicateFilterEntry);
								if (tempList.getCount() === 1) {
									var selectedItem = tempList.getByIndex(0);
									if (selectedItem.Caption.toLowerCase() === macrosText.toLowerCase()) {
										macros = tempList.getByIndex(0);
									}
								}
							}
							while (inputArray.length > i) {
								additionValue += inputArray[i++] + " ";
							}
							additionValue = additionValue.trim();
							if (!macros && inputArray.length === i && !tempList.isEmpty()) {
								tempList = commandList.filter(predicateFilterMatch);
								if (tempList.getCount() === 1) {
									macros = tempList.getByIndex(0);
								}
							}
						}
						if (macros) {
							columnName = macros.ColumnName;
							columnTypeCode = macros.ColumnTypeCode;
							command = macros.Code;
							mainParam = macros.SubjectName;
							var addValue;
							if (macros.HintType === ConfigurationConstants.CommandLineHintType.Macros &&
									macros.AdditionalParamValue) {
								switch (command) {
									case "search":
									case "goto":
										addValue = macros.AdditionalParamValue.split(";");
										valueId = addValue[0];
										additionValue = addValue[2] || addValue[1];
										break;
									case "startbp":
										addValue = macros.AdditionalParamValue.split(";");
										valueId = addValue[0];
										additionValue = addValue[1];
										var esq = Ext.create("Terrasoft.EntitySchemaQuery", {
											rootSchemaName: "VwSysProcess"
										});
										esq.addColumn("Id", "Id");
										esq.addColumn("Name", "Name");
										esq.filters.add("recordId", Terrasoft.createColumnFilterWithParameter(
												Terrasoft.ComparisonType.EQUAL, "Id", valueId));
										esq.getEntityCollection(function(result) {
											if (instance.isDestroyed) {
												return;
											}
											if (!result.collection.isEmpty()) {
												var entities = result.collection.getItems();
												valueId = entities[0].values.Name;
												executeCommand(command, macros.SubjectName, {
													value: additionValue,
													valueId: valueId,
													columnName: columnName,
													columnTypeCode: columnTypeCode
												});
											}
										}, this);
										return;
									default:
										additionValue = macros.AdditionalParamValue;
										break;
								}
							} else if (additionValue && command !== "create") {
								tempList = hintList.filter(function(item) {
									return additionValue === item.AdditionalParamValue;
								});
								if (tempList.getCount() === 1) {
									var item = tempList.getByIndex(0);
									valueId = item.AdditionalParamValueId;
									var typeId = item.TypeId;
									executeCommand(command, mainParam, {
										value: additionValue,
										valueId: valueId,
										columnName: columnName,
										columnTypeCode: columnTypeCode,
										typeId: typeId
									});
									this.clearData();
									return;
								} else {
									subjectName = macros.SubjectName;
									if (command === "goto") {
										subjectName = subjectName + "Folder";
									}
									var viewModel = this;
									callServiceMethod(ajaxProvider, "GetHintList", function(response) {
										if (!instance || instance.isDestroyed) {
											return;
										}
										var hintArray = response.GetHintListResult;
										var config = {
											value: additionValue,
											columnName: columnName,
											columnTypeCode: columnTypeCode
										};
										if (hintArray.length === 1) {
											var value = hintArray[0].Value;
											try {
												var params = Terrasoft.deserialize(value);
												config.valueId = params.value;
												config.typeId = params.typeId;
											} catch (e) {
												config.valueId = value;
											}
										}
										executeCommand(command, mainParam, config);
										viewModel.clearData();
									}, {
										"subjectName": subjectName,
										"hintText": additionValue
									});
									return;
								}
							}
							executeCommand(command, mainParam, {
								value: additionValue,
								valueId: valueId,
								columnName: columnName,
								columnTypeCode: columnTypeCode
							});
						} else {
							var searchSchemaName = "Contact";
							var searchColumnName = "Name";
							if (currentSchemaName) {
								var tempCommandList = commandList.filter(function(item) {
									return (item.SubjectName === currentSchemaName);
								});
								if (!tempCommandList.isEmpty()) {
									searchSchemaName = currentSchemaName;
									searchColumnName = tempCommandList.getByIndex(0).ColumnName;
								}
							}
							executeCommand("search", searchSchemaName, {
								value: input.trim(),
								valueId: "",
								columnName: searchColumnName,
								columnTypeCode: "",
								noSchema: true
							});
						}
						this.clearData();
					};
					view.render(container);
					view.bind(viewModel);
					performanceCounterManager.setTimeStamp("loadAdditionalModulesComplete");
				}

				var instance = Ext.define("CommandLineModule", {
					init: init,
					render: render
				});
				return instance;
			}

			return createConstructor;
		});
/*jshint ignore:end */