define("TsDashboardBuilderExtender", ["MaskHelper", "ModalBox", "TsDashboardBuilderExtenderResources", "html2canvas",
			"DashboardBuilder"],
		function(MaskHelper, ModalBox, Resources) {
			return Ext.define("Terrasoft.configuration.TsBaseDashboardsViewModel", {
				override: "Terrasoft.BaseDashboardsViewModel",
				alternateClassName: "Terrasoft.TsBaseDashboardsViewModel",

				subscribeSandboxMessages: function() {
					this.callParent(arguments);
					this.sandbox.subscribe("CreateScreenshotWithContainer", function(config) {
						MaskHelper.ShowBodyMask();
						Terrasoft.convertElementToCanvas(config.ContainerName, null, function(canvas) {
							var currentDashboardName = this.get("ActiveTabName");
							var currentDashboard = Terrasoft.DashboardManager.getItem(currentDashboardName);
							var fileName = currentDashboard.getCaption() + "." +
									currentDashboard.items[config.ContainerName].parameters.caption + ".png";

							if (canvas.msToBlob) {
								window.navigator.msSaveBlob(canvas.msToBlob(), fileName);
							} else {
								Terrasoft.download(canvas.toDataURL(), fileName);
							}
							MaskHelper.HideBodyMask();
						}, this);
					}.bind(this), this, [this.sandbox.id + "_TsDashboardScreenshotModule"]);

				},
				onScreenshotButtonClick: function() {
					MaskHelper.ShowBodyMask();

					this.Terrasoft.showConfirmation(Resources.localizableStrings.ChooseScreenshotTypeMessage, function(returnCode) {
						if (returnCode === "All") {
							Terrasoft.convertElementToCanvas("DashboardModuleContainer", null, function(canvas) {
								var currentDashboardName = this.get("ActiveTabName");
								var currentDashboard = Terrasoft.DashboardManager.getItem(currentDashboardName);
								var fileName = currentDashboard.getCaption() + ".png";

								if (canvas.msToBlob) {
									window.navigator.msSaveBlob(canvas.msToBlob(), fileName);
								} else {
									Terrasoft.download(canvas.toDataURL(), fileName);
								}
								MaskHelper.HideBodyMask();
							}, this);
						}
						else if (returnCode === "selectCustom") {
							var sandbox = this.sandbox;
							var config = {
								boxClasses: ["width-300"]
							};
							var moduleName = "TsDashboardScreenshotModule";
							var moduleId = sandbox.id + "_" + moduleName;
							var renderTo = ModalBox.show(config, function() {
								sandbox.unloadModule(moduleId, renderTo);
								MaskHelper.HideBodyMask()
							});
							var currentDashboardName = this.get("ActiveTabName");
							sandbox.loadModule(moduleName, {
								id: moduleId,
								renderTo: renderTo,
								parameters: {
									СurrentDashboardName: currentDashboardName
								}
							});
						}
					}, [{
						className: "Terrasoft.Button",
						returnCode: "All",
						style: this.Terrasoft.controls.ButtonEnums.style.GREEN,
						caption: Resources.localizableStrings.SelectAllButtonCaption
					}, {
						className: "Terrasoft.Button",
						returnCode: "selectCustom",
						style: this.Terrasoft.controls.ButtonEnums.style.GREEN,
						caption: Resources.localizableStrings.SelectCustomElementButtonCaption
					}], this);
				}
			});
		});
