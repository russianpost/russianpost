define("ProductPageV2", [], function() {
	return {
		entitySchemaName: "Product",
		attributes: {
			TsIncludedInPacket: {
				lookupListConfig: {
					filter: function() {
						return this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
								"TsIsPacketOfServices", true);
					}
				}
			}
		},
		details: /**SCHEMA_DETAILS*/{
			"TsBusinessTypeInProductDetailV2": {
				"schemaName": "TsBusinessTypeInProductDetailV2",
				"entitySchemaName": "TsBusinessTypeInProduct",
				"filter": {
					"detailColumn": "TsProduct",
					"masterColumn": "Id"
				}
			},
			"TsIndustryInProductDetailV2": {
				"schemaName": "TsIndustryInProductDetailV2",
				"entitySchemaName": "TsIndustryInProduct",
				"filter": {
					"detailColumn": "TsProduct",
					"masterColumn": "Id"
				}
			}
		}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "merge",
				"name": "Name",
				"values": {
					"layout": {
						"colSpan": 24,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Code",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "Owner",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1
					}
				}
			},
			{
				"operation": "merge",
				"name": "URL",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2
					}
				}
			},
			{
				"operation": "merge",
				"name": "IsArchive",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3
					}
				}
			},
			{
				"operation": "merge",
				"name": "Category",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "TradeMark",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0
					}
				}
			},
			{
				"operation": "merge",
				"name": "Type",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 1
					}
				}
			},
			{
				"operation": "insert",
				"name": "TsProductActivityType",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 1,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsProductActivityType"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "TsKAU",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 2,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsKAU"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "TsDiaryArticleForm130",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 2,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsDiaryArticleForm130"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 3
			},
			{
				"operation": "insert",
				"name": "TsArticleBDR",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 3,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsArticleBDR"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 5
			},
			{
				"operation": "insert",
				"name": "TsArticleBDRRow",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 3,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsArticleBDRRow"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 6
			},
			{
				"operation": "insert",
				"name": "TsForm65Page",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 4,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsForm65Page"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 7
			},
			{
				"operation": "insert",
				"name": "TsForm65Article",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 4,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsForm65Article"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 8
			},
			{
				"operation": "insert",
				"name": "TsServiceProvisionInOPS",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 5,
						"layoutName": "ProductCategoryBlock"
					}
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items"
			},
			{
				"operation": "insert",
				"name": "TsOwnerDivision",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 5,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsOwnerDivision"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "TsIncludedInPacket",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsIncludedInPacket"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items"
			},
			{
				"operation": "insert",
				"name": "TsIsObjectWithTariffication",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 6,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsIsObjectWithTariffication"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items"
			},
			{
				"operation": "insert",
				"name": "TsIsPacketOfServices",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 7,
						"layoutName": "ProductCategoryBlock"
					},
					"bindTo": "TsIsPacketOfServices"
				},
				"parentName": "ProductCategoryBlock",
				"propertyName": "items"
			},
			{
				"operation": "merge",
				"name": "Unit",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 6,
						"layoutName": "Header"
					},
					"parentName": "Header",
					"propertyName": "items"
				}
			},
			{
				"operation": "remove",
				"name": "Price"
			},
			{
				"operation": "remove",
				"name": "Tax"
			},
			{
				"operation": "remove",
				"name": "PriceControlGroup"
			},
			{
				"operation": "remove",
				"name": "ProductPricesTab"
			},
			{
				"operation": "move",
				"name": "ESNTab",
				"parentName": "Tabs",
				"propertyName": "tabs",
				"index": 4
			},
			{
				"operation": "insert",
				"name": "TsIndustryInProductDetailV2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ProductGeneralInfoTab",
				"propertyName": "items",
				"index": 1
			},
			{
				"operation": "insert",
				"name": "TsBusinessTypeInProductDetailV2",
				"values": {
					"itemType": 2,
					"markerValue": "added-detail"
				},
				"parentName": "ProductGeneralInfoTab",
				"propertyName": "items",
				"index": 2
			}
		]/**SCHEMA_DIFF*/,
		methods: {},
		rules: {}
	};
});