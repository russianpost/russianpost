define("DashboardsModule", ["BaseSchemaModuleV2", "DashboardBuilder",  "TsDashboardBuilderExtender","css!DashboardsModule"],
		function() {


			return Ext.define("Terrasoft.configuration.DashboardsModule", {
				extend: "Terrasoft.BaseSchemaModule",
				alternateClassName: "Terrasoft.DashboardsModule",

				Ext: null,
				sandbox: null,
				Terrasoft: null,

				viewModelClassName: "Terrasoft.BaseDashboardsViewModel",

				builderClassName: "Terrasoft.DashboardBuilder",

				viewConfigClass: "Terrasoft.DashboardsViewConfig",


				getProfileKey: function() {
					return "DashboardId";
				},


				initSchemaName: Terrasoft.emptyFn,


				createViewModel: function(viewModelClass) {
					return this.Ext.create(viewModelClass, {
						Ext: this.Ext,
						sandbox: this.sandbox,
						Terrasoft: this.Terrasoft
					});
				},


				generateSchemaStructure: function(callback, scope) {
					var builder = Ext.create(this.builderClassName, {
						viewModelClass: this.viewModelClassName,
						viewConfigClass: this.viewConfigClass
					});
					var config = {};
					builder.build(config, function(viewModelClass, view) {
						callback.call(scope, viewModelClass, view);
					}, this);
				},


				destroy: function() {
					this.callParent(arguments);
					this.renderContainer = null;
				}

			});

		});
