define("TsDashboardScreenshotPage", ["ModalBox", "TsDashboardScreenshotPageResources"], function(ModalBox, Resources) {
	return {
		attributes: {
			"DashboardElementGridCollection": {
				dataValueType: this.Terrasoft.DataValueType.COLLECTION,
				value: this.Ext.create("Terrasoft.BaseViewModelCollection")
			},
			"ActiveDashboardElement": {
				type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
				dataValueType: this.Terrasoft.DataValueType.GUID,
				"dependencies": [
					{
						"columns": ["ActiveDashboardElement"],
						"methodName": "onActiveDashboardElementChanged"
					}
				]
			}
		},
		messages: {
			"CreateScreenshotWithContainer": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.PUBLISH
			}
		},
		methods: {
			generateDashboardElementViewModel: function(config) {
				return this.Ext.create("Terrasoft.BaseViewModel", {
					primaryColumnName: "ContainerName",
					primaryDisplayColumnName: "Caption",
					columns: {
						Caption: {
							type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
							dataValueType: this.Terrasoft.DataValueType.TEXT
						},
						ContainerName: {
							type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
							dataValueType: this.Terrasoft.DataValueType.TEXT
						}
					},
					values: {
						ContainerName: config.ContainerName,
						Caption: config.Caption
					}
				});
			},

			init: function(callback, scope) {
				this.callParent(arguments);
				var currentDashboardName = this.get("СurrentDashboardName");
				var currentDashboard = Terrasoft.DashboardManager.getItem(currentDashboardName);
				this.fillDashboardElementCollection(currentDashboard);
			},

			fillDashboardElementCollection: function(dashboard) {
				var dashboardItems = dashboard.items;
				var buf = this.Ext.create("Terrasoft.BaseViewModelCollection");

				for (var containerName in dashboardItems) {
					buf.add(containerName, this.generateDashboardElementViewModel({
						ContainerName: containerName,
						Caption: dashboardItems[containerName].parameters.caption
					}));
				}
				var collection = this.get("DashboardElementGridCollection");
				collection.clear();
				collection.loadAll(buf);
			},

			onRender: function() {
			},

			onSelectButtonClick: function() {
				this.sandbox.publish("CreateScreenshotWithContainer", {
					ContainerName: this.get("ActiveDashboardElement"),
					ContainerCaption: ""
				}, [this.sandbox.id]);
				ModalBox.close();
			},
			onCloseButtonClick: function() {
				ModalBox.close();
			}
		},
		diff: [
			{
				"operation": "insert",
				"name": "MainContainer",
				"propertyName": "items",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.CONTAINER,
					"items": [],
					classes: {
						wrapClassName: ["width-290"]
					}
				}
			},
			{
				"operation": "insert",
				"parentName": "MainContainer",
				"propertyName": "items",
				"name": "MainGridLayout",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.GRID_LAYOUT,
					"items": []
				}
			},
			{
				"operation": "insert",
				"name": "DashboardElementGrid",
				"parentName": "MainGridLayout",
				"propertyName": "items",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.GRID,
					"listedZebra": true,
					"collection": {"bindTo": "DashboardElementGridCollection"},
					"primaryColumnName": "ContainerName",
					"activeRow": {"bindTo": "ActiveDashboardElement"},
					"type": "listed",
					"columnsConfig": [
						{
							"cols": 24,
							"key": [{
								"name": {"bindTo": "Caption"},
								"type": "text"
							}]
						}
					],
					"captionsConfig": [
						{
							"cols": 24,
							"name": Resources.localizableStrings.GridColumnHeaderCaption
						}
					],
					"layout": {"column": 0, "row": 0, "colSpan": 24}
				}
			},
			{
				"operation": "insert",
				"parentName": "MainGridLayout",
				"name": "SelectButton",
				"propertyName": "items",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.BUTTON,
					"style": this.Terrasoft.controls.ButtonEnums.style.BLUE,
					"click": {bindTo: "onSelectButtonClick"},
					"markerValue": "CloseButton",
					"caption": {bindTo: "Resources.Strings.SelectButtonCaption"},
					"layout": {"column": 0, "row": 1, "colSpan": 12}
				}
			},
			{
				"operation": "insert",
				"parentName": "MainGridLayout",
				"name": "CloseButton",
				"propertyName": "items",
				"values": {
					"itemType": this.Terrasoft.ViewItemType.BUTTON,
					"style": this.Terrasoft.controls.ButtonEnums.style.BLUE,
					"markerValue": "CloseButton",
					"click": {bindTo: "onCloseButtonClick"},
					"caption": {bindTo: "Resources.Strings.CancelButtonCaption"},
					"layout": {"column": 12, "row": 1, "colSpan": 12}
				}
			}
		]
	};
});