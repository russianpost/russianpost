define("TsJsConfigurationUtilities", [], function() {
	/**
	 * @class Terrasoft.configuration.mixins.TsJsConfigurationUtilities
	 * Миксин, реализующий решение часто возникающих задач
	 */
	var tscUtils = Ext.define("Terrasoft.configuration.mixins.TsJsConfigurationUtilities", {
		alternateClassName: "Terrasoft.TsJsConfigurationUtilities",

		/**
		 * Реализует проверку значения lookup колонки и ее очистку
		 * если значение не попадает под текущий фильтр
		 * @param lookupColumnName название lookup колонки на проверкуe
		 */
		checkLookupColumnCurrentValue: function(lookupColumnName) {
			if (!Boolean(this.get(lookupColumnName))) {
				return;
			}
			var currentValue = this.get(lookupColumnName).value;
			var depQuery = this.getLookupQuery(null, lookupColumnName);
			var idFilter = this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
					"Id", currentValue);
			depQuery.filters.addItem(idFilter);
			depQuery.rowCount = 1;
			depQuery.getEntityCollection(function(result) {
				if (!Boolean(result.success) || result.collection.getCount() !== 1) {
					this.set(lookupColumnName, null);
				}
			}, this);
		},

		setDateIfTrue: function(dateColumnName, boolColumnName, dateValue) {
			if (Boolean(this.get(boolColumnName))) {
				this.set(dateColumnName, dateValue || new Date());
			}
		},

		/**
		 * Реализует получения значения lookup колонки из веб-сервиса 'TscService'
		 * если значение не попадает под текущий фильтр
		 * @param columnRelationName для возможности повторного использования метода и веб-сервиса,
		 * в классе 'TscServiceHelper' в методе 'TryGetColumnValues' необходимо дописать свою реализацию
		 */
		getColumnValuesFromTscService: function(columnRelationName, parametersConfig, callback) {
			var data = {
				columnRelationName: columnRelationName,
				config: []
			};
			for (var prop in parametersConfig) {
				data.config.push({Key: prop, Value: parametersConfig[prop]});
			}
			this.callService({
				serviceName: "TscService",
				methodName: "GetColumnValues",
				data: data
			}, function(responseObject) {
				callback.call(this, responseObject && responseObject.GetColumnValuesResult);
			}, this);
		},

		/**
		 * Реализует вызов метода на сервере
		 * @param methodName для возможности повторного использования метода и веб-сервиса,
		 * в классе 'TscServiceHelper' в методе 'InvokeMethod' необходимо дописать свою реализацию
		 */
		invokeServerMethod: function(methodName, parametersConfig, callback) {
			var data = {
				methodName: methodName,
				config: []
			};
			for (var prop in parametersConfig) {
				var value = parametersConfig[prop];
				data.config.push({Key: prop, Value: value.value || value});
			}
			this.callService({
				serviceName: "TscService",
				methodName: "InvokeMethod",
				data: data
			}, function(responseObject) {
				if (responseObject && responseObject.InvokeMethodResult) {
					callback.call(this, this.Terrasoft.deserialize(responseObject.InvokeMethodResult));
				} else {
					callback.call(this, null);
				}

			}, this);
		},

		/**
		 * Инвертирует булево значение
		 * @param value значение
		 * @returns {boolean}
		 */
		invertBooleanValue: function(value) {
			return !Boolean(value);
		},

		/**
		 * Возвращает первую запись в БД
		 * @param {string} schemaName название объекта
		 * @param {object} filters фильтры в виде: {ColumnName: ColumnValue,Column2Name: Column2Value}
		 * @param {function} callback
		 * @param {string[]} [columns]  колонки для извлечения в виде масива ["ColumnName","Column2Name"],
		 * необязательный (по умолчанию первичные поля)
		 */
		fetchColumnValues: function(schemaName, filters, callback, columns) {
			var select = this.Ext.create("Terrasoft.EntitySchemaQuery", {
				rootSchemaName: schemaName
			});
			select.rowCount = 1;
			if (columns) {
				this.Terrasoft.each(columns, function(column) {
					select.addColumn(column);
				});
			} else {
				select.addMacrosColumn(Terrasoft.QueryMacrosType.PRIMARY_COLUMN, "Id");
			}
			if (filters) {
				for (var filterColumn in filters) {
					var filter = select.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
							filterColumn, filters[filterColumn]);
					select.filters.addItem(filter);
				}
			}
			select.getEntityCollection(function(response) {
				if (response.success && response.collection.getCount() === 1) {
					callback.call(this, response.collection.getByIndex(0));
				} else {
					callback.call(this, null);
				}
			}, this);
		},

		openNewWindow: function(link) {
			if (link) {
				window.open(link);
			}
		},

		addMonths: function(d, number) {
			var tempDatedateMonth = d.getDate();
			d.setMonth(d.getMonth() + number);
			if (tempDatedateMonth > d.getDate()) {
				this.addDays(d, -d.getDate());
			}
			return d;
		},

		addDays: function(d, number) {
			d.setDate(d.getDate() + number);
		},

		/**
		 * Распарсить дату, сериализированую WCF сервисом
		 * @param wcfDate
		 * @returns {date}
		 */
		parseWCFDate: function(wcfDate) {
			return this.Ext.Date.parse(wcfDate, "MS");
		},

		/**
		 * Устонавливает значение новое значение колонки объекта, если текущее значение отсутствует
		 * @param {String} columnName Имя колонки
		 * @param {Object} value Новое значение
		 * @param {Object} scope Контекст
		 */
		setIfEmpty: function(columnName, value, scope) {
			if (!scope) {
				scope = this;
			}
			var oldValue = scope.get(columnName);
			if (Ext.isEmpty(oldValue)) {
				scope.set(columnName, value);
			}
		},

		/**
		 * Возвращает аттрибут value объекта, если такой существует, иначе null
		 * @param object объект
		 * @returns {Guid}
		 */
		getValueOrNull: function(object) {
			return object && object.value ? object.value : null;
		},

		/**
		 * Возвращает имя панели для вкладок
		 * @returns {string} Имя панели
		 */
		getTabPanelName: function() {
			var cardPageName = this.name;
			return cardPageName + "TabsTabPanel";
		},

		/*
		 * Инициализирует работу с табами, следует вызвать после инициализации странцы
		 * */
		initTabRemoving: function() {
			if (this.get("TsJsConfigurationUtil_InitTabsCollection") && !this.get("IsSeparateMode")) {
				return;
			}
			var tabsCollection = this.get("TabsCollection");
			var tabItems = tabsCollection && tabsCollection.getItems();
			if (tabItems) {
				this.set("TsJsConfigurationUtil_InitTabsCollection", tabItems.slice());
			}

		},
		/**
		 * Удаляет таб с именем key
		 * @param key Имя таба для удаления
		 */
		removeTabByKey: function(tabName) {
			var tabsCollection = this.get("TabsCollection");
			if (tabsCollection.contains(tabName)) {
				if (this.get("ActiveTabName") === tabName) {
					var newActiveTab = tabsCollection.collection.find(function(el) {
						return el.values.Name !== tabName;
					});
					this.set("ActiveTabName", newActiveTab.values.Name);
				}
				tabsCollection.removeByKey(tabName);
			}
		},
		/**
		 * Возвращает вкладку по имени.
		 * @param {String} tabName Имя вкладки для удаления.
		 */
		restoreTabByKey: function(tabName) {
			var tabsCollection = this.get("TabsCollection");
			if (!tabsCollection.contains(tabName)) {
				var initTabsCollection = this.get("TsJsConfigurationUtil_InitTabsCollection");
				for (var i = 0; i < initTabsCollection.length; i++) {
					if (initTabsCollection[i].values.Name === tabName) {
						var index = 0;
						for (var j = i - 1; j >= 0; j--) {
							if (tabsCollection.contains(initTabsCollection[j].values.Name)) {
								index = j + 1;
								break;
							}
						}
						tabsCollection.insert(index, tabName, initTabsCollection[i]);
						return;
					}
				}
			}
		},

		restoreAllTabs: function() {
			var initTabsCollection = this.get("TsJsConfigurationUtil_InitTabsCollection");
			var tabsCollection = this.get("TabsCollection");
			var tabsCollectionKeys = tabsCollection.getKeys();
			initTabsCollection.forEach(function(tab) {
				var tabName = tab.get("Name");
				if (!tabsCollectionKeys.includes(tabName)) {
					this.restoreTabByKey(tabName);
				}
			}.bind(this));
		},
		/**
		 * Returns link to the parent class with his original, not
		 * overriden properties and methods.
		 * @returns {Object}
		 */
		getParentClass: function() {
			var superclass;
			return (superclass = this.getParentClass.caller) && (
					(superclass.$previous && superclass.$previous.$owner.superclass) ||
					((superclass = superclass.$owner ? superclass : superclass.caller) &&
					superclass.$owner.superclass));
		},

		getParentMethod: function() {
			var method,
					superMethod = (method = this.getParentMethod.caller) && (method.$previous ||
							((method = method.$owner ? method : method.caller) &&
							method.$owner.superclass[method.$name]));
			return superMethod;
		},

		/*
		 * returns pass to parentSchema after schema with givenUid
		 * parent - this.getParentClass() from module in which is needed get parent
		 * schemaUid - uId of schema which is needed skip
		 */
		skipParentSchemaByUid: function(schemaUid, parent) {
			while (parent.uId !== schemaUid) {
				parent = parent.superclass;
			}
			return parent.superclass;
		},

		/**
		 * Добавляет поле в массив, если оно не заполнено
		 * @param fieldArray Результирующий массив
		 * @param fieldName Поле, которое проверяется
		 */
		addFieldToArrayIfEmpty: function(fieldArray, fieldName) {
			var fieldValue = this.get(fieldName);
			if (typeof fieldValue === "string") {
				fieldValue = fieldValue.trim();
			}
			if (Ext.isEmpty(fieldValue)) {
				fieldArray.push(fieldName);
			}
		},

		/**
		 * Возвращает заголовки всех колонок из массива
		 * @param fieldArray Массив с именами колонок
		 * @returns {string}
		 */
		getFieldsCaptionFromNameArray: function(fieldArray) {
			var fieldsCaptions = [];
			Terrasoft.each(fieldArray, function(fieldName) {
				var column = this.getColumnByName(fieldName);
				fieldsCaptions.push("\"" + this.getColumnCaption(column) + "\"");
			}, this);
			return fieldsCaptions.join(", ");
		}
	});
	return Ext.create(tscUtils);
});