define("LeadProductDetailV2", [], function() {
			return {
				entitySchemaName: "LeadProduct",
				messages: {
					"GetLeadAccountInfo": {
						mode: this.Terrasoft.MessageMode.PTP,
						direction: this.Terrasoft.MessageDirectionType.PUBLISH
					}
				},
				methods: {
					openProductLookup: function() {
						var accountInfo = this.sandbox.publish("GetLeadAccountInfo", null, [this.sandbox.id]);

						var config = {
							entitySchemaName: "Product",
							multiSelect: true,
							columns: ["Name", "Price", "Currency"]
						};
						var leadId = this.get("MasterRecordId");

						if (this.Ext.isEmpty(leadId)) {
							return;
						}
						var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {
							rootSchemaName: this.entitySchemaName
						});
						esq.addColumn("Id");
						esq.addColumn("Product.Id", "ProductId");
						esq.filters.add("filterLead", this.Terrasoft.createColumnFilterWithParameter(
								this.Terrasoft.ComparisonType.EQUAL, "Lead", leadId));
						esq.getEntityCollection(function(result) {
							var existsProductsCollection = [];
							if (result.success) {
								result.collection.each(function(item) {
									existsProductsCollection.push(item.get("ProductId"));
								});
							}
							var filterGroup = this.Terrasoft.createFilterGroup();
							if (existsProductsCollection.length > 0) {
								var existsFilter = this.Terrasoft.createColumnInFilterWithParameters("Id",
										existsProductsCollection);
								existsFilter.comparisonType = this.Terrasoft.ComparisonType.NOT_EQUAL;
								existsFilter.Name = "existsFilter";
								filterGroup.addItem(existsFilter);

							}
							var typesFilters = this.Terrasoft.createFilterGroup();
							typesFilters.logicalOperation = this.Terrasoft.LogicalOperatorType.OR;
							if (accountInfo.BusinessType) {
								var businessTypeFilter = this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
										"[TsBusinessTypeInProduct:TsProduct:Id].TsBusinessTypes", accountInfo.BusinessType.value);
								typesFilters.addItem(businessTypeFilter);
							}
							if (accountInfo.Industry) {
								var industryFilter = this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL,
										"[TsIndustryInProduct:TsProduct:Id].TsAccountIndustry", accountInfo.Industry.value);
								typesFilters.addItem(industryFilter);
							}

							filterGroup.addItem(typesFilters);

							config.filters = filterGroup;
							this.openLookup(config, this.addCallBack, this);
						}, this);
					}
				},
				diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
			};
		}
);
