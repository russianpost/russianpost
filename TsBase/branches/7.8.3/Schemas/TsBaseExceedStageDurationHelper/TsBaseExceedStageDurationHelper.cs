using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Terrasoft.Common;
using Terrasoft.Configuration;
using Terrasoft.Core;
using Terrasoft.Core.DB;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Factories;
using Terrasoft.Mail.Sender;

namespace Terrasoft.Configuration.TsBase {
	public abstract class TsBaseExceedStageDurationHelper {

		#region Properties: Protected

		protected UserConnection UserConnection {
			get;
			set;
		}

		protected EntityCollection DataRows;

		protected abstract string HeadEmailColumnName {
			get;
		}

		protected abstract string DataObjectName {
			get;
		}

		protected abstract string EmailTableView {
			get;
		}

		protected abstract string EmailRowView {
			get;
		}

		protected virtual bool UseHtml {
			get {
				return true;
			}
		}

		protected abstract string Subject {
			get;
		}

		protected virtual string SenderEmail {
			get {
				var senderSelect =
					new Select(UserConnection)
						.Column("mss", "SenderEmailAddress")
						.From("SysSettings").As("ss")
						.InnerJoin("SysSettingsValue").As("ssv").On("ss", "Id").IsEqual("ssv", "SysSettingsId")
						.InnerJoin("MailboxSyncSettings").As("mss").On("ssv", "GuidValue").IsEqual("mss", "Id")
						.Where("ss", "Code").IsEqual(Column.Const("EmailForSystemMessages")) as Select;
				return senderSelect.ExecuteScalar<string>();
			}
		}

		#endregion

		#region Constructors

		protected TsBaseExceedStageDurationHelper(UserConnection userConnection) {
			UserConnection = userConnection;
		}

		#endregion

		#region Methods : Protected

		protected void ReadDataRows() {
			var dataEsq = new EntitySchemaQuery(UserConnection.EntitySchemaManager, DataObjectName);
			dataEsq.AddAllSchemaColumns();
			DataRows = dataEsq.GetEntityCollection(UserConnection);
		}

		protected virtual string GetColumnMacros(string columnName) {
			return "{" + columnName + "}";
		}

		protected string GetEmailDataRow(Entity dataRow) {
			EntitySchemaColumnCollection columns = dataRow.Schema.Columns;
			string emailDataRow = EmailRowView;
			foreach (var column in columns) {
				string columnValueName = column.ColumnValueName;
				emailDataRow = emailDataRow.Replace(GetColumnMacros(columnValueName), dataRow.GetColumnValue(columnValueName).ToString());
			}
			return emailDataRow;
		}

		protected string GetEmailBody(IEnumerable<Entity> dataRows) {
			StringBuilder resultBuidler = new StringBuilder();
			foreach (Entity dataRow in dataRows) {
				resultBuidler.Append(GetEmailDataRow(dataRow));
			}
			return String.Format(EmailTableView, resultBuidler.ToString());
		}

		protected virtual Activity CreateEmailActivity(string senderEmail, string recepientEmail, string body) {
			Activity activity = new Activity(UserConnection);
			activity.SetDefColumnValues();
			activity.Title = Subject;
			activity.Body = body;
			activity.TypeId = ActivityConsts.EmailTypeUId;
			activity.Sender = senderEmail;
			activity.Recepient = recepientEmail;
			activity.IsHtmlBody = UseHtml;
			activity.Save(false);
			return activity;
		}

		protected void SendEmail(string senderEmail, string recipientEmail, string emailBody) {

			Activity activity = CreateEmailActivity(senderEmail, recipientEmail, emailBody);
			var emailClientFactory = ClassFactory.Get<EmailClientFactory>(new ConstructorArgument("userConnection", UserConnection));
			var activityEmailSender = new ActivityEmailSender(emailClientFactory, UserConnection);
			activityEmailSender.Send(activity.Id);
		}

		protected void SendEmails() {
			string senderEmail = SenderEmail;
			IEnumerable<string> headEmails = DataRows.Select(dataRow => dataRow.GetTypedColumnValue<string>(HeadEmailColumnName)).Distinct();
			foreach (string headEmail in headEmails) {
				IEnumerable<Entity> dataRowsForHead =
					DataRows.Where(dataRow => dataRow.GetTypedColumnValue<string>(HeadEmailColumnName) == headEmail);
				string emailBody = GetEmailBody(dataRowsForHead);
				SendEmail(senderEmail, headEmail, emailBody);
			}
		}

		#endregion

		#region Methods : Public

		public void DoCheckAndSendEmails() {
			ReadDataRows();
			SendEmails();
		}

		#endregion
	}
}
