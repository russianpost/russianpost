define("OpportunityContactPageV2", ["terrasoft"], function(Terrasoft) {
	return {
		entitySchemaName: "OpportunityContact",
		attributes: {
			"Contact": {
				lookupListConfig: {
					columns: ["TsResponsibilityField"]
				}
			},
			"TsResponsibilityField": {
				dependencies: [
					{
						columns: ["Contact"],
						methodName: "onContactSelect"
					}
				]
			}
		},
		methods: {
			onContactSelect: function() {
				var contact = this.get("Contact");
				if (!Ext.isEmpty(contact)) {
					this.set("TsResponsibilityField", contact.TsResponsibilityField);
				}
			},
			getActions: function() {
				var emptyCollection = this.Ext.create("Terrasoft.BaseViewModelCollection");
				return emptyCollection;
			}
		},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "Header",
				"propertyName": "items",
				"name": "TsResponsibilityField",
				"values": {
					"layout": {"column": 0, "row": 6, "colSpan": 24},
					"contentType": Terrasoft.ContentType.ENUM
				}
			}
		]/**SCHEMA_DIFF*/
	};
});
