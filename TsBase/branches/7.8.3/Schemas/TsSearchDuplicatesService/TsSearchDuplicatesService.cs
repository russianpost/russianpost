namespace Terrasoft.Configuration.TsBase
{
	using System.CodeDom.Compiler;
	using System.ServiceModel;
	using System.ServiceModel.Web;
	using System.ServiceModel.Activation;
	using System.Web;
	using Terrasoft.Common;
	using Terrasoft.Core;
	using Terrasoft.Core.DB;
	using Terrasoft.Core.Entities;
	using Terrasoft.Core.Store;
	using Terrasoft.Core.Scheduler;
	using System;
	using System.Data;
	using System.Collections.Generic;
	using System.Linq;
	using System.Runtime.Serialization;
	using System.Xml;
	using System.Xml.Linq;
	using DocumentFormat.OpenXml.Wordprocessing;
	using Newtonsoft.Json.Linq;
	using Quartz;
	using Quartz.Impl;
	using Quartz.Impl.Triggers;
	using Terrasoft.Core.Factories;
	using Column = Terrasoft.Core.DB.Column;

	[ServiceContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
	public class TsSearchDuplicatesService {

		#region Fields: Private

		private UserConnection _userConnection;
		private DeduplicationProcessing _deduplicationProcessing;

		#endregion

		#region Properties: Public

		public UserConnection UserConnection {
			get {
				return _userConnection ??
					(_userConnection = (UserConnection)HttpContext.Current.Session["UserConnection"]);
			}
			set {
				_userConnection = value;
			}
		}

		public DeduplicationProcessing DeduplicationProcessing {
			get {
				if (_deduplicationProcessing == null) {
					var deduplicationProcessingArgs = new[] { 
						new ConstructorArgument("userConnection", UserConnection)
					};
					_deduplicationProcessing = ClassFactory.Get<DeduplicationProcessing>(deduplicationProcessingArgs);
				}
				return _deduplicationProcessing;
			}
			set {
				_deduplicationProcessing = value;
			}
		}

		#endregion

		#region Methods: Public
		
		[OperationContract]
		[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.Wrapped,
			RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
		public List<Guid> FindDuplicatesOnSave(string schemaName, TsSingleRequest request) {
			XmlDocument xml = GetPreparedXml(request);
			List<Guid> result = DeduplicationProcessing.FindDuplicates(schemaName, xml);
			return result;
		}
		
		#endregion

		#region DataContract
		
		[DataContract]
		public class TsSingleRequest {
			[DataMember]
			public Guid Id { get; set; }
			[DataMember]
			public string Name { get; set; }
			[DataMember]
			public string AlternativeName { get; set; }
			[DataMember]
			public List<TsRequestCommunication> Communication { get; set; }
			[DataMember]
			public string Inn { get; set; }
			[DataMember]
			public int Kpp { get; set; }
			[DataMember]
			public int Bik { get; set; }
		}
		
		[DataContract]
		public class TsRequestCommunication {
			[DataMember]
			public Guid Id { get; set; }
			[DataMember]
			public string Number { get; set; }
			[DataMember]
			public Guid CommunicationTypeId { get; set; }
		}
		#endregion

		#region Methods: Private

		private XmlDocument GetPreparedXml(TsSingleRequest request) {
			XmlDocument xml = new XmlDocument();
			XmlElement elementRows = xml.CreateElement("rows");
			List<TsRequestCommunication> communicationsList = request.Communication ?? new List<TsRequestCommunication>();
			foreach (TsRequestCommunication communication in communicationsList) {
				XmlElement elementRow = xml.CreateElement("row");
				XmlElement elementCommunicationTypeId = xml.CreateElement("CommunicationTypeId");
				elementCommunicationTypeId.InnerText = communication.CommunicationTypeId.ToString();
				XmlElement elementNumber = xml.CreateElement("Number");
				elementNumber.InnerText = communication.Number;
				elementRow.AppendChild(elementCommunicationTypeId);
				elementRow.AppendChild(elementNumber);
				AddElementsToRow(xml, elementRow, request);
				elementRows.AppendChild(elementRow);
			}
			if (communicationsList.Count < 1) {
				XmlElement elementRow = xml.CreateElement("row");
				AddElementsToRow(xml, elementRow, request);
				elementRows.AppendChild(elementRow);
			}
			xml.AppendChild(elementRows);
			return xml;
		}

		private void AddElementsToRow(XmlDocument xml, XmlElement elementRow, TsSingleRequest request) {
			XmlElement elementName = xml.CreateElement("Name");
			elementName.InnerText = request.Name;
			elementRow.AppendChild(elementName);
			
			if (!string.IsNullOrWhiteSpace(request.Inn))
			{
				XmlElement elementInn = xml.CreateElement("Inn");
				elementInn.InnerText = request.Inn;
				elementRow.AppendChild(elementInn);
			}
			
			if (request.Kpp > 0)
			{
				XmlElement elementKpp = xml.CreateElement("Kpp");
				elementKpp.InnerText = request.Kpp.ToString();
				elementRow.AppendChild(elementKpp);
			}
			
			if (request.Bik > 0)
			{
				XmlElement elementBik = xml.CreateElement("Bik");
				elementBik.InnerText = request.Bik.ToString();
				elementRow.AppendChild(elementBik);
			}
			
			if (request.Id != Guid.Empty) {
				XmlElement elementId = xml.CreateElement("Id");
				elementId.InnerText = request.Id.ToString();
				elementRow.AppendChild(elementId);
			}
		}
		
		#endregion
	}
}
