define("OpportunityActionsDashboard", [], function() {
	return {
		methods: {
			onCardColumnChanged: function(changedColumn) {
				this.callParent(arguments);
				if(changedColumn.columnName === "LeadType") {
					this.initActionsControl();
				}
			},
			addActionsFilters:function(esq){
				this.callParent(arguments);
				var leadType = this.getMasterEntityParameterValue("LeadType");
				if (leadType && leadType.value) {
					esq.filters.add("leadTypeFilter", this.Terrasoft.createColumnFilterWithParameter(
							this.Terrasoft.ComparisonType.EQUAL, "[TsOppStageInLeadType:TsOpportunityStage].TsLeadType", leadType.value));
				}
			}
		},
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
	};
});
