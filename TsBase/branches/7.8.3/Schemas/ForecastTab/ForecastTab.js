define("ForecastTab", ["ConfigurationConstants"], function(ConfigurationConstants) {
	return {
		messages: {},
		mixins: {},
		attributes: {},
		methods: {
			addFiltersToForecastDimensionLookup: function(config) {
				config.filters = this.Terrasoft.createFilterGroup();

				if (config.entitySchemaName === "Contact") {
					config.filters.add("ContactIsUserFilter", this.Terrasoft.createColumnIsNotNullFilter("[SysAdminUnit:Contact].Id"));
				}

				if (config.entitySchemaName === "Account") {
					config.filters.add("AccountIsOurCompanyFilter", this.Terrasoft.createColumnFilterWithParameter(this.Terrasoft.ComparisonType.EQUAL, "Type",
							ConfigurationConstants.AccountType.OurCompany.toLowerCase()));
				}
			},

			onAddButtonClick: function() {
				var config = {
					entitySchemaName: this.get("ForecastDimensionSchemaName"),
					multiSelect: true,
					columnName: "Name",
					hideActions: true
				};

				this.addFiltersToForecastDimensionLookup(config);

				this.openLookup(config, this.onChooseDimension, this);
			}
		},
		diff: []
	};
});
