define("OrderPageV2", ["BusinessRuleModule", "ConfigurationConstants"],
		function(BusinessRuleModule, ConfigurationConstants) {
			return {
				entitySchemaName: "Order",
				attributes: {
					"Client": {
						"multiLookupColumns": ["Account"],
						"isRequired": true
					}
				},
				rules: {
					"Account": {
						"FiltrationClientByNotOurCompany": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"baseAttributePatch": "Type",
							"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
							"type": BusinessRuleModule.enums.ValueType.CONSTANT,
							"value": ConfigurationConstants.AccountType.OurCompany
						},
						"FiltrationOrderByNotCompetitor": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"baseAttributePatch": "Type",
							"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
							"type": BusinessRuleModule.enums.ValueType.CONSTANT,
							"value": ConfigurationConstants.AccountType.Competitor
						}
					}
				},
				methods: {},
				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "merge",
						"name": "Client",
						"values": {
							"layout": {
								column: 0,
								row: 0,
								colSpan: 24
							},
							"tip": null
						}
					},
					{
						"operation": "merge",
						"name": "Amount",
						"values": {
							"layout": {
								column: 12,
								colSpan: 12,
								row: 1
							}
						}
					},
					{
						"operation": "merge",
						"name": "PaymentAmount",
						"values": {
							"layout": {
								column: 12,
								colSpan: 12,
								row: 2
							}
						}
					},
					{
						"operation": "insert",
						"name": "TsLeadType",
						"propertyName": "items",
						"parentName": "Header",
						"values": {
							"contentType": this.Terrasoft.ContentType.ENUM,
							"layout": {
								row: 1,
								column: 0,
								columnSpan: 12
							}
						}
					},
					{
						"operation": "merge",
						"name": "Status",
						"values": {
							"layout": {
								column: 0,
								columnSpan: 12,
								row: 2
							}
						}
					},
					{
						"operation": "remove",
						"name": "OrderDeliveryTab"
					},
					{
						"operation": "remove",
						"name": "OrderReceiverInformationResultsControlBlock"
					},
					{
						"operation": "remove",
						"name": "OrderResultsDeliveryAndPaymentControlBlock"
					}

				]/**SCHEMA_DIFF*/
			};
		});
