define("OpportunityPageV2", ["BusinessRuleModule", "ConfigurationConstants"],
	function(BusinessRuleModule, ConfigurationConstants) {
		return {
			entitySchemaName: "Opportunity",
			attributes: {
				"Client": {
					"multiLookupColumns": ["Account"],
					"isRequired": true
				},
				"Account": {
					dependencies: [
						{
							columns: ["Account"],
							methodName: "onAccountChanged"
						}
					]
				},
				"Owner": {
					"lookupListConfig": {
						"columns": ["TsAccount", "TsTarifficatorRegion"]
					},
					dependencies: [
						{
							columns: ["Owner"],
							methodName: "onOwnerChanged"
						}
					]
				},
				"OwnerTsAccount": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.LOOKUP
				},
				"OwnerTsTarifficatorRegion": {
					type: this.Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: this.Terrasoft.DataValueType.LOOKUP
				},
			},
			modules: {
				"ActionsDashboardModule": {
					"config": {
						"isSchemaConfigInitialized": true,
						"schemaName": "OpportunityActionsDashboard",
						"useHistoryState": false,
						"parameters": {
							"viewModelConfig": {
								"entitySchemaName": "Opportunity",
								"actionsConfig": {
									"schemaName": "OpportunityStage",
									"columnName": "Stage",
									"colorColumnName": "Color",
									"filterColumnName": "ShowInProgressBar",
									"orderColumnName": "[TsOppStageInLeadType:TsOpportunityStage].TsNumber",
									"innerOrderColumnName": "End",
									"decouplingConfig": {
										"name": "OppStageDecoupling",
										"masterColumnName": "CurrentStage",
										"referenceColumnName": "AvailableStage"
									}
								},
								"dashboardConfig": {
									"Activity": {
										"masterColumnName": "Id",
										"referenceColumnName": "Opportunity"
									}
								}
							}
						}
					}
				}
			},
			methods: {
				onSaved: function() {
					this.callParent(arguments);
					if (this.isAddMode()) {
						this.showConfirmationDialog(this.get("Resources.Strings.ConfirmRunOpportunityProcessMessage"), function(result) {
							if (result === Terrasoft.MessageBoxButtons.YES.returnCode) {
								this.getProcessModuleUtils().executeProcess({
									sysProcessName: "TsOpportunityManagement",
									parameters: {
										CurrentOpportunity: this.get("Id")
									}
								});
							}
						}, ["yes", "no"]);
					}
				},

				onEntityInitialized: function() {
					this.callParent(arguments);
					this.onOwnerChanged();
				},

				onAccountChanged: function() {
					this.updateDetail({detail: "Contracts"});
				},

				onOwnerChanged: function() {
					var newOwner = this.get("Owner");

					if (Ext.isEmpty(newOwner)) {
						this.set("OwnerTsAccount", null);
						this.set("OwnerTsTarifficatorRegion", null);
						return;
					}

					this.set("OwnerTsAccount", newOwner.TsAccount);
					this.set("OwnerTsTarifficatorRegion", newOwner.TsTarifficatorRegion);
				}				
			},
			rules: {
				"Account": {
					"FiltrationClientByNotOurCompany": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"baseAttributePatch": "Type",
						"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.AccountType.OurCompany
					},
					"FiltrationOrderByNotCompetitor": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"baseAttributePatch": "Type",
						"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.AccountType.Competitor
					}
				}
			},
			details: /**SCHEMA_DETAILS*/{
				Contracts: {
					schemaName: "ContractDetailV2",
					filter: {
						masterColumn: "Account",
						detailColumn: "Account"
					}
				}
			}/**SCHEMA_DETAILS*/,
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "insert",
					"parentName": "HistoryAccountTab",
					"propertyName": "items",
					"name": "Contracts",
					"values": {
						"itemType": this.Terrasoft.ViewItemType.DETAIL
					}
				},
				{
					"operation": "remove",
					"name": "OpportunityClient",
					"parentName": "ProfileContainer"
				},
				{
					"operation": "merge",
					"name": "LablelMetricsContainer",
					"propertyName": "items",
					"values": {
						"layout": {
							"column": 0,
							"row": 5,
							"colSpan": 24
						}
					}
				},
				{
					"operation": "move",
					"name": "OpportunityLeadType",
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"values": {
						"bindTo": "LeadType",
						"layout": {
							"column": 0,
							"row": 1,
							"colSpan": 24
						},
						"contentType": this.Terrasoft.ContentType.ENUM
					}
				},
				{
					"operation": "insert",
					"name": "OwnerTsTarifficatorRegion",
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"values": {
						"caption": {
							"bindTo": "Resources.Strings.OwnerTsTarifficatorRegionCaption"
						},
						"bindTo": "OwnerTsTarifficatorRegion",
						"layout": {
							"column": 0,
							"row": 2,
							"colSpan": 24
						},
						"enabled": false,
						"contentType": this.Terrasoft.ContentType.ENUM
					}
				},
				{
					"operation": "insert",
					"name": "OwnerTsAccount",
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"values": {
						"caption": {
							"bindTo": "Resources.Strings.OwnerTsAccountCaption"
						},
						"bindTo": "OwnerTsAccount",
						"layout": {
							"column": 0,
							"row": 3,
							"colSpan": 24
						},
						"enabled": false,
						"contentType": this.Terrasoft.ContentType.ENUM
					}
				},
				{
					"operation": "move",
					"name": "OpportunityDueDate",
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"values": {
						"bindTo": "DueDate",
						"dataValueType": this.Terrasoft.DataValueType.DATE,
						"layout": {
							"column": 0,
							"row": 4,
							"colSpan": 24
						},
						"tip": {
							"content": {"bindTo": "Resources.Strings.DueDateTip"}
						}
					}
				},
				{
					"operation": "remove",
					"name": "BantProfile"
				},
				{
					"operation": "remove",
					"name": "Weaknesses"
				},
				{
					"operation": "remove",
					"name": "Strength",
					"parentName": "OpportunityTacticAndCompetitorBlockGridLayout",
					"propertyName": "items",
					"values": {
						"bindTo": "Strength"
					}
				},
				{
					"operation": "insert",
					"name": "TsOurWeakSides",
					"parentName": "OpportunityTacticAndCompetitorBlock",
					"propertyName": "items",
					"values": {
						"bindTo": "TsOurWeakSides",
						"layout": {
							"column": 0,
							"row": 0,
							"colSpan": 12,
							"rowSpan": 1,
							"layoutName": "OpportunityTacticAndCompetitorBlock"
						}
					}
				},
				{
					"operation": "insert",
					"name": "TsOurStrongSides",
					"parentName": "OpportunityTacticAndCompetitorBlock",
					"propertyName": "items",
					"values": {
						"bindTo": "TsOurStrongSides",
						"layout": {
							"column": 11,
							"row": 0,
							"colSpan": 12,
							"rowSpan": 1,
							"layoutName": "OpportunityTacticAndCompetitorBlock"
						}
					}
				},
				{
					"operation": "insert",
					"name": "TsTarifficatorRegion",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 6
						},
						"bindTo": "TsTarifficatorRegion"
					},
					"parentName": "OpportunityPageGeneralBlock",
					"propertyName": "items"
				}

			]/**SCHEMA_DIFF*/
		};
	});
