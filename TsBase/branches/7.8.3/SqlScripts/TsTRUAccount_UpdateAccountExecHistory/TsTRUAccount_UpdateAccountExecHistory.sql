IF ( OBJECT_ID( 'TsTRUAccount_UpdateAccountExecHistory' ) IS NOT NULL )
		DROP TRIGGER TsTRUAccount_UpdateAccountExecHistory
GO

CREATE TRIGGER TsTRUAccount_UpdateAccountExecHistory
		ON Account
AFTER UPDATE, INSERT
AS
		BEGIN
				IF NOT EXISTS( SELECT Id
											 FROM INSERTED )
						RETURN;
				DECLARE @EmptyGUID UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000';
				DECLARE @TempAccExecHistory TABLE(
						TsAccountId              UNIQUEIDENTIFIER,
						TsExecutiveId            UNIQUEIDENTIFIER,
						ModifiedOn               DATETIME2,
						ModifiedById             UNIQUEIDENTIFIER,
						TsAccountExecutiveRoleId UNIQUEIDENTIFIER);
				WITH tab AS (
						SELECT
								i.Id                                   AccountId,
								i.TsExecutiveParcelBusinessId          NewExecutiveId,
								d.TsExecutiveParcelBusinessId          OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'2C1015E5-AFCC-499D-9F60-FABDC1F330A0' BusinessRole -- Ответственный в Посылочном блоке
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecutiveParcelBusinessId, @EmptyGUID ) <>
								ISNULL( d.TsExecutiveParcelBusinessId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecutiveSubscribeServicesId       NewExecutiveId,
								d.TsExecutiveSubscribeServicesId       OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'30CF3475-DA20-4B29-9219-1B7DDD3C9416' BusinessRole --Ответственный в Подписных сервисах
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE

								ISNULL( i.TsExecutiveSubscribeServicesId, @EmptyGUID ) <>
								ISNULL( d.TsExecutiveSubscribeServicesId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecutiveFinancialBusinessId       NewExecutiveId,
								d.TsExecutiveFinancialBusinessId       OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'D57CA098-AEB1-4244-8CEF-07C353616F51' BusinessRole --Ответственный в Финансовом блоке
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecutiveFinancialBusinessId, @EmptyGUID ) <>
								ISNULL( d.TsExecutiveFinancialBusinessId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecutiveAdvertBusinessId          NewExecutiveId,
								d.TsExecutiveAdvertBusinessId          OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'87B486D6-F805-457C-A524-A7D0CA8DA578' BusinessRole --Ответственный в блоке Директ-маркетинг
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecutiveAdvertBusinessId, @EmptyGUID ) <>
								ISNULL( d.TsExecutiveAdvertBusinessId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecSuppParcelBusinessId           NewExecutiveId,
								d.TsExecSuppParcelBusinessId           OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'AAF8DE02-2DEB-4F7F-B87B-BFB0C941C470' BusinessRole --Ответственный поддержки в Посылочном блоке
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecSuppParcelBusinessId, @EmptyGUID ) <>
								ISNULL( d.TsExecSuppParcelBusinessId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecSuppSubscribeServicesId        NewExecutiveId,
								d.TsExecSuppSubscribeServicesId        OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'53D7120D-89C4-44BD-8F31-D3F2C825F061' BusinessRole --Ответственный поддержки в Подписных сервисах
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecSuppSubscribeServicesId, @EmptyGUID ) <>
								ISNULL( d.TsExecSuppSubscribeServicesId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecSuppFinBusinessId              NewExecutiveId,
								d.TsExecSuppFinBusinessId              OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'C3A635A9-0812-4D3D-B9EE-C38507F33BD1' BusinessRole --Ответственный поддержки в Финансовом блоке
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecSuppFinBusinessId, @EmptyGUID ) <>
								ISNULL( d.TsExecSuppFinBusinessId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecSuppAdvertBusinessId           NewExecutiveId,
								d.TsExecSuppAdvertBusinessId           OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'3A8097F6-460F-4FC5-9E02-64BFFE59ECCA' BusinessRole --Ответственный поддержки в блоке Директ-маркетинг
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecSuppAdvertBusinessId, @EmptyGUID ) <>
								ISNULL( d.TsExecSuppAdvertBusinessId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecutivePrintingServicesId        NewExecutiveId,
								d.TsExecutivePrintingServicesId        OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'1A16D8F8-0F62-4107-BF8C-2129CEC70D93' BusinessRole --Ответственный в Печатных сервисах
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecutivePrintingServicesId, @EmptyGUID ) <>
								ISNULL( d.TsExecutivePrintingServicesId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecSuppPrintingServicesId         NewExecutiveId,
								d.TsExecSuppPrintingServicesId         OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'1A71E3F3-E59F-4362-93F8-7215FDCDD1F1' BusinessRole --Ответственный поддержки в Печатных сервисах
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecSuppPrintingServicesId, @EmptyGUID ) <>
								ISNULL( d.TsExecSuppPrintingServicesId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecutiveRetailId                  NewExecutiveId,
								d.TsExecutiveRetailId                  OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'8842E865-1791-4730-9DA5-61FA832B8D16' BusinessRole --Ответственный в Розничной торговле
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecutiveRetailId, @EmptyGUID ) <>
								ISNULL( d.TsExecutiveRetailId, @EmptyGUID )
						UNION ALL
						SELECT
								i.Id                                   AccountId,
								i.TsExecSuppRetailId                   NewExecutiveId,
								d.TsExecSuppRetailId                   OldExecutiveId,
								i.ModifiedOn                           ModifiedOn,
								i.ModifiedById                         ModifiedById,
								'E0B88F4D-9BC8-48C2-AAF1-97BBF7D0723E' BusinessRole --Ответственный поддержки в Розничной торговле
						FROM INSERTED i LEFT JOIN DELETED d
										ON
												i.Id = d.Id
						WHERE
								ISNULL( i.TsExecSuppRetailId, @EmptyGUID ) <>
								ISNULL( d.TsExecSuppRetailId, @EmptyGUID )
				)
				INSERT INTO @TempAccExecHistory
						SELECT
								TsAccountId              AS TsAccountId,
								TsExecutiveId            AS TsExecutiveId,
								ModifiedOn               AS ModifiedOn,
								ModifiedById             AS ModifiedById,
								TsAccountExecutiveRoleId AS TsAccountExecutiveRoleId
						FROM ( MERGE TsAccountExecHistory AS target
						USING tab AS source
						ON ( target.TsAccountId = source.AccountId AND
								 target.TsExecutiveId = ISNULL( source.OldExecutiveId, @EmptyGUID ) AND
								 target.TsAccountExecutiveRoleId = source.BusinessRole AND
								 target.TsIsActualExecutive = 1 )
						WHEN MATCHED
						THEN UPDATE SET
								TsIsActualExecutive = 0,
								ModifiedOn          = source.ModifiedOn,
								ModifiedById        = source.ModifiedById
						WHEN NOT MATCHED THEN INSERT (TsAccountId,
																					TsExecutiveId,
																					ModifiedOn,
																					CreatedOn,
																					ModifiedById,
																					CreatedById,
																					TsAccountExecutiveRoleId,
																					TsIsActualExecutive)
								VALUES (
										source.AccountId,
										source.NewExecutiveId,
										source.ModifiedOn,
										source.ModifiedOn,
										source.ModifiedById,
										source.ModifiedById,
										source.BusinessRole,
										1 )
						OUTPUT $action,
								source.AccountId,
								source.NewExecutiveId,
								source.ModifiedOn,
								source.ModifiedOn,
								source.ModifiedById,
								source.ModifiedById,
								source.BusinessRole )
								AS Updates ( Action,
								 TsAccountId,
								 TsExecutiveId,
								 ModifiedOn,
								 CreatedOn,
								 ModifiedById,
								 CreatedById,
								 TsAccountExecutiveRoleId
								 )
						WHERE Action = 'UPDATE'
				INSERT INTO TsAccountExecHistory (TsAccountId,
																					TsExecutiveId,
																					ModifiedOn,
																					CreatedOn,
																					ModifiedById,
																					CreatedById,
																					TsAccountExecutiveRoleId,
																					TsIsActualExecutive)
						SELECT
								TsAccountId,
								TsExecutiveId,
								ModifiedOn,
								ModifiedOn,
								ModifiedById,
								ModifiedById,
								TsAccountExecutiveRoleId,
								1
						FROM @TempAccExecHistory
						WHERE TsExecutiveId IS NOT NULL
		END