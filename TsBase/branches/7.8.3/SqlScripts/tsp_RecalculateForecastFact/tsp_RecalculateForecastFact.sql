IF ( OBJECT_ID( 'tsp_RecalculateForecastFact' ) IS NOT NULL )
		DROP PROCEDURE tsp_RecalculateForecastFact
GO

CREATE PROCEDURE [dbo].[tsp_RecalculateForecastFact]
				@ForecastId           UNIQUEIDENTIFIER = NULL,
				@CurrentUserContactId UNIQUEIDENTIFIER = NULL
AS

		IF @ForecastId IS NULL
				BEGIN
						RETURN;
				END

		DECLARE @PlanIndicatorId UNIQUEIDENTIFIER
		DECLARE @FactIndicatorId UNIQUEIDENTIFIER
		DECLARE @FactPercentIndicatorId UNIQUEIDENTIFIER
		DECLARE @PotentialIndicatorId UNIQUEIDENTIFIER
		DECLARE @CompletedId UNIQUEIDENTIFIER

		DECLARE @ForecastItemId UNIQUEIDENTIFIER
		DECLARE @DimensionId UNIQUEIDENTIFIER
		DECLARE @DimensionValueId UNIQUEIDENTIFIER
		DECLARE @PeriodId UNIQUEIDENTIFIER
		DECLARE @StartDate DATE
		DECLARE @DueDate DATE
		DECLARE @MaxDueDate DATE
		DECLARE @ColumnName NVARCHAR(100)
		DECLARE @LeadTypeId UNIQUEIDENTIFIER
		DECLARE @SQLText NVARCHAR(MAX)
		DECLARE @LeadTypeWhereClause NVARCHAR(100)
		DECLARE @PlanAmount DECIMAL(18, 2)
		DECLARE @FactAmount DECIMAL(18, 2)
		DECLARE @PotentialAmount DECIMAL(18, 2)
		DECLARE @FactPotentialAmountTable TABLE(PlanAmount DECIMAL(18, 2), FactAmount DECIMAL(18, 2), PotentialAmount DECIMAL(18, 2))
		DECLARE @SelectFactAmount NVARCHAR(MAX)
		DECLARE @SelectPotentialAmount NVARCHAR(MAX)
		DECLARE @TarifficatorRegionDimension UNIQUEIDENTIFIER = '2E3354ED-83B2-4F6E-98B6-55E8BDB55348'
		DECLARE @BranchDimension UNIQUEIDENTIFIER = '474531F0-8F54-4EF7-86D5-A6EB5065135E'
		DECLARE @ProductDimension UNIQUEIDENTIFIER = 'CEA404D6-CDEC-4410-A4EC-32DD6D9848CD'

		SET @PlanIndicatorId = '{CBD311C7-6E1B-4324-BF21-192681349DDF}'
		SET @FactIndicatorId = '{52CAE26F-84F6-42A0-AAEF-97790AF3B8D9}'
		SET @FactPercentIndicatorId = '{E0D66FFB-A3E3-4DA9-BCB7-95D27033286E}'
		SET @PotentialIndicatorId = '{A004FC7A-D63D-4E3C-9356-0AD77B2600F3}'
		SET @CompletedId = '{60D5310C-5BE6-DF11-971B-001D60E938C6}'

		DELETE FROM ForecastItemValue
		WHERE ForecastIndicatorId IN ( @FactIndicatorId, @FactPercentIndicatorId, @PotentialIndicatorId )
					AND EXISTS( SELECT 1
											FROM ForecastItem fi
											WHERE ForecastItemValue.ForecastItemId = fi.Id
														AND fi.ForecastId = @ForecastId
					)

		SET @MaxDueDate = ( SELECT Convert( DATE, MAX( DueDate ), 104 )
												FROM Opportunity o )

		DECLARE Cur CURSOR STATIC LOCAL FOR
				SELECT
						fi.Id               ForecastItemId,
						d.Id                DimensionId,
						fi.DimensionValueId DimensionValueId,
						p.Id                PeriodId,
						p.StartDate         StartDate,
						p.DueDate           DueDate,
						d.[Path] + 'Id',
						f.TsLeadTypeId      LeadTypeId
				FROM ForecastItem fi
						INNER JOIN Forecast f
								ON f.Id = fi.ForecastId
						INNER JOIN ForecastDimension fd
								ON fd.Id = fi.ForecastDimensionId
						INNER JOIN Dimension d
								ON d.Id = fd.DimensionId
						INNER JOIN Period p
								ON p.PeriodTypeId = f.PeriodTypeId
				WHERE f.Id = @ForecastId
							AND ISNULL( d.[Path], '' ) <> ''
							AND p.StartDate <= @MaxDueDate

		SET NOCOUNT ON;

		OPEN Cur
		FETCH NEXT FROM Cur
		INTO @ForecastItemId, @DimensionId, @DimensionValueId, @PeriodId, @StartDate, @DueDate, @ColumnName, @LeadTypeId
		WHILE @@FETCH_STATUS = 0
				BEGIN
						SET @SelectFactAmount = ''
						SET @SelectPotentialAmount = ''
						IF ( @LeadTypeId IS NULL )
								BEGIN
						SET  @LeadTypeWhereClause  = ''
								END
						ELSE
								BEGIN
SET  @LeadTypeWhereClause  =  N' AND o.LeadTypeId =  @P8 '
								END


						IF ( @DimensionId = @TarifficatorRegionDimension )
								BEGIN
										SET @SelectFactAmount = N'SELECT SUM(ISNULL(o.[Amount], 0))
																							FROM [Opportunity] o
																						 INNER JOIN Contact c on c.Id = o.OwnerId
																						INNER JOIN OpportunityStage os
																												ON o.StageId = os.Id
																								WHERE os.[Successful] = 1
																											AND os.[End] = 1
																						AND o.[DueDate] >= @P2
																						AND o.[DueDate] < @P3
																						AND c.TsTarifficatorRegionId = @P4' + @LeadTypeWhereClause;

										SET @SelectPotentialAmount = N'SELECT SUM(ISNULL(o.[Amount], 0) * ISNULL(o.[Probability], 0) / 100)
																										FROM [Opportunity] o
																											INNER JOIN [OpportunityStage] os ON os.[Id] = o.[StageId]
																											INNER JOIN Contact c on c.Id = o.OwnerId
																										WHERE os.[End] = 0
																										AND o.[DueDate] >= @P2
																										AND o.[DueDate] < @P3
																										AND c.TsTarifficatorRegionId = @P4' + @LeadTypeWhereClause;
								END

						IF ( @DimensionId = @BranchDimension )
								BEGIN
										SET @SelectFactAmount = N'SELECT SUM(ISNULL(o.[Amount], 0))
																							FROM [Opportunity] o
																								 INNER JOIN Contact c on c.Id = o.OwnerId
																								INNER JOIN OpportunityStage os
																												ON o.StageId = os.Id
																								WHERE os.[Successful] = 1
																											AND os.[End] = 1
																								AND o.[DueDate] >= @P2
																								AND o.[DueDate] < @P3
																								AND c.TsAccountId = @P4' + @LeadTypeWhereClause;

										SET @SelectPotentialAmount = N'SELECT SUM(ISNULL(o.[Amount], 0) * ISNULL(o.[Probability], 0) / 100)
																										FROM [Opportunity] o
																											INNER JOIN [OpportunityStage] os ON os.[Id] = o.[StageId]
																											INNER JOIN Contact c on c.Id = o.OwnerId
																										WHERE os.[End] = 0
																										AND o.[DueDate] >= @P2
																										AND o.[DueDate] < @P3
																										AND c.TsAccountId = @P4'+ @LeadTypeWhereClause;
								END
						IF ( @DimensionId = @ProductDimension )
								BEGIN
										SET @SelectFactAmount = N'SELECT SUM( ISNULL( opi.Amount, 0 ) )
																								FROM [OpportunityProductInterest] opi
																										INNER JOIN Opportunity o
																												ON opi.OpportunityId = o.Id
																										INNER JOIN OpportunityStage os
																												ON o.StageId = os.Id
																								WHERE os.[Successful] = 1
																											AND os.[End] = 1
																											AND o.[DueDate] >= @P2
																											AND o.[DueDate] < @P3
																											AND opi.[ProductId] = @P4'+ @LeadTypeWhereClause;

										SET @SelectPotentialAmount = N'SELECT SUM(ISNULL(opi.[Amount], 0) * ISNULL(o.[Probability], 0) / 100)
																												FROM OpportunityProductInterest opi
																														INNER JOIN [Opportunity] o on opi.OpportunityId = o.Id
																													INNER JOIN [OpportunityStage] os ON os.[Id] = o.[StageId]
																												WHERE os.[End] = 0
																												AND o.[DueDate] >= @P2
																												AND o.[DueDate] < @P3
																												AND opi.ProductId = @P4' +@LeadTypeWhereClause +
																								 N' GROUP BY  o.Id';
								END

						IF ( @SelectPotentialAmount = '' )
								BEGIN
										SET @SelectPotentialAmount = N'SELECT SUM(ISNULL(o.[Amount], 0) * ISNULL(o.[Probability], 0) / 100)
								FROM [Opportunity] o
									INNER JOIN [OpportunityStage] os ON os.[Id] = o.[StageId]
								WHERE os.[End] = 0
								AND o.[DueDate] >= @P2
								AND o.[DueDate] < @P3
								AND o.' + @ColumnName + N' = @P4' +@LeadTypeWhereClause;
								END

						IF ( @SelectFactAmount = '' )
								BEGIN
										SET @SelectFactAmount = N'SELECT SUM(ISNULL(o.[Amount], 0))
								FROM [Opportunity] o
								INNER JOIN OpportunityStage os
										ON o.StageId = os.Id
								WHERE os.[Successful] = 1
								AND os.[End] = 1
								AND o.[DueDate] >= @P2
								AND o.[DueDate] < @P3
								AND o.' + @ColumnName + N' = @P4' +@LeadTypeWhereClause;
								END

						SET @DueDate = DATEADD( DAY, 1, @DueDate )
						DELETE FROM @FactPotentialAmountTable
						SET @SQLText = N'
		SELECT
		(SELECT SUM(ISNULL(fiv.[Value], 0))
		FROM [ForecastItemValue] fiv
		WHERE fiv.[ForecastItemId] = @P5
		AND fiv.[PeriodId] = @P6
		AND fiv.[ForecastIndicatorId] = @P7
		) PlanAmount,
		(' + @SelectFactAmount + ') FactAmount,
		(' + @SelectPotentialAmount + ') PotentialAmount'
						INSERT INTO @FactPotentialAmountTable EXEC sp_executesql @SQLText,
																																		 N'@P1 UNIQUEIDENTIFIER, @P2 DATE, @P3 DATE, @P4 UNIQUEIDENTIFIER, @P5 UNIQUEIDENTIFIER, @P6 UNIQUEIDENTIFIER, @P7 UNIQUEIDENTIFIER, @P8 UNIQUEIDENTIFIER',
																																		 @P1 = @CompletedId, @P2 = @StartDate,
																																		 @P3 = @DueDate,
																																		 @P4 = @DimensionValueId, @P5 = @ForecastItemId,
																																		 @P6 = @PeriodId, @P7 = @PlanIndicatorId, @P8=@LeadTypeId

						SELECT
								@PlanAmount = PlanAmount,
								@FactAmount = FactAmount,
								@PotentialAmount = PotentialAmount
						FROM @FactPotentialAmountTable

						IF ( @FactAmount <> 0 )
								BEGIN
										INSERT INTO ForecastItemValue
										(
												Id,
												CreatedOn,
												CreatedById,
												ModifiedOn,
												ModifiedById,
												ProcessListeners,
												ForecastIndicatorId,
												[Value],
												PeriodId,
												ForecastItemId
										)
										VALUES
												(
														NEWID( ),
														GETUTCDATE( ),
														@CurrentUserContactId,
														GETUTCDATE( ),
														@CurrentUserContactId,
														0,
														@FactIndicatorId,
														@FactAmount,
														@PeriodId,
														@ForecastItemId
												)
										IF ( @PlanAmount <> 0 )
												BEGIN
														INSERT INTO ForecastItemValue
														(
																Id,
																CreatedOn,
																CreatedById,
																ModifiedOn,
																ModifiedById,
																ProcessListeners,
																ForecastIndicatorId,
																[Value],
																PeriodId,
																ForecastItemId
														)
														VALUES
																(
																		NEWID( ),
																		GETUTCDATE( ),
																		@CurrentUserContactId,
																		GETUTCDATE( ),
																		@CurrentUserContactId,
																		0,
																		@FactPercentIndicatorId,
																		CAST( ( @FactAmount * 100 / @PlanAmount ) AS DECIMAL(18, 2) ),
																		@PeriodId,
																		@ForecastItemId
																)
												END
								END

						IF ( @PotentialAmount <> 0 )
								BEGIN
										INSERT INTO ForecastItemValue
										(
												Id,
												CreatedOn,
												CreatedById,
												ModifiedOn,
												ModifiedById,
												ProcessListeners,
												ForecastIndicatorId,
												[Value],
												PeriodId,
												ForecastItemId
										)
										VALUES
												(
														NEWID( ),
														GETUTCDATE( ),
														@CurrentUserContactId,
														GETUTCDATE( ),
														@CurrentUserContactId,
														0,
														@PotentialIndicatorId,
														@PotentialAmount,
														@PeriodId,
														@ForecastItemId
												)
								END

						FETCH NEXT FROM Cur
						INTO @ForecastItemId, @DimensionId, @DimensionValueId, @PeriodId, @StartDate, @DueDate, @ColumnName, @LeadTypeId
				END
		CLOSE Cur
		DEALLOCATE Cur
GO