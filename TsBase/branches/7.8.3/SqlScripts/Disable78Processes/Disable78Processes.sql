
DECLARE @LeadToOpportunity780BaseSchemaUId UNIQUEIDENTIFIER = 'D3FC7286-DDE1-43EA-B06C-57E80832FE54';
INSERT INTO SysProcessDisabled (SysSchemaId)
	SELECT ss.Id
	FROM SysSchema ss LEFT JOIN SysProcessDisabled spd
			ON spd.SysSchemaId = ss.Id
	WHERE spd.Id IS NULL AND ss.UId = @LeadToOpportunity780BaseSchemaUId

DECLARE @Proposal780BaseSchemaUId UNIQUEIDENTIFIER = 'B56D5E4F-2654-4089-861C-73562EFE590F';
INSERT INTO SysProcessDisabled (SysSchemaId)
	SELECT ss.Id
	FROM SysSchema ss LEFT JOIN SysProcessDisabled spd
			ON spd.SysSchemaId = ss.Id
	WHERE spd.Id IS NULL AND ss.UId = @Proposal780BaseSchemaUId

DECLARE @Contracting780BaseSchemaUId UNIQUEIDENTIFIER = '3809BE02-3949-43DE-96D5-8A2CE750E791';
INSERT INTO SysProcessDisabled (SysSchemaId)
	SELECT ss.Id
	FROM SysSchema ss LEFT JOIN SysProcessDisabled spd
			ON spd.SysSchemaId = ss.Id
	WHERE spd.Id IS NULL AND ss.UId = @Contracting780BaseSchemaUId

DECLARE @Presentation780BaseSchemaUId UNIQUEIDENTIFIER = '6BF791EC-1818-465E-AA06-54135080911C';
INSERT INTO SysProcessDisabled (SysSchemaId)
	SELECT ss.Id
	FROM SysSchema ss LEFT JOIN SysProcessDisabled spd
			ON spd.SysSchemaId = ss.Id
	WHERE spd.Id IS NULL AND ss.UId = @Presentation780BaseSchemaUId