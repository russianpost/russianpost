CREATE TABLE #DefaultAccountOwnership (
	Name        NVARCHAR(250),
	Description NVARCHAR(250),
	CountryId   UNIQUEIDENTIFIER,
	TsINNLength INT
)

INSERT INTO #DefaultAccountOwnership (Name, Description, CountryId, TsINNLength)
	SELECT
		'ТОО',
		'',
		'A570B005-E8BB-DF11-B00F-001D60E938C6',
		12
	UNION ALL SELECT
				  'ИП',
				  'Индивидуальный предприниматель',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  12
	UNION ALL SELECT
				  'ПБОЮЛ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  12
	UNION ALL SELECT
				  'КФХ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  12
	UNION ALL SELECT
				  'ЛОГУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ЛПУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МАОУДО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МАУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МБОУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МБОУДОД',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МБУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МБУЗ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МКУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МНТК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ПК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ПКГО ККОООО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ПМП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ПО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ПОБ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ИМНС',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ККООО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'КТ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'КУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'РайПО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'РОО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ЧОУ ДПО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'СЕЛЬПО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'НПО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'НПФ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ОГУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ОДО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ОиРО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ООО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ПАО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'НКО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'НП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'НПК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГБУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ОАО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ОГБОУ ДО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ОГБУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'АНО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'АО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'АУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'БУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГАУЗ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГАУК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГБОУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГНУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГОУВПО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГОУСПО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГБУЗ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ГКУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ЗАО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МОБУДОД',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МОО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МОУДО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МОУДПОПР',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МУЗ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'МУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'НАО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'НГОУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФГУЗ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФГУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
		UNION ALL SELECT
				  'ОГУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
		UNION ALL SELECT
				  'КГУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
		UNION ALL SELECT
				  'РГУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФДА',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФКП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФКУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'Фонд',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ТСЖ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ТСН',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'УФАС',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'УФК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФБУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФГБНУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'СП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'СПК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'СПССПК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФГБУН',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФГКУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФГОУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФГУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'ФГУДП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  10
	UNION ALL SELECT
				  'СФГ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'СХПК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ТАА',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ТДА',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ФГБОУ ВПО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ЧОУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'Некоммерческая организация',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ЗАТ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ГКУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ЖСК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ГБУ КО',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ВК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ОГКУ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'НУЗ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ААТ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'РУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'СВК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'КУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ППО ГУЧ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ПСК',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ПТ',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ПУП',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0
	UNION ALL SELECT
				  'ПГ ТАА',
				  '',
				  'A570B005-E8BB-DF11-B00F-001D60E938C6',
				  0



DECLARE @CurrentDate DATETIME2 = GETUTCDATE( );

MERGE AccountOwnership AS target
USING #DefaultAccountOwnership AS source
ON ( target.Name = source.Name COLLATE SQL_Latin1_General_CP1_CI_AS)
WHEN NOT MATCHED
THEN
INSERT (Name, Description, CountryId, TsINNLength)
	VALUES ( source.Name, source.Description, source.CountryId, source.TsINNLength )
WHEN MATCHED THEN
UPDATE SET CountryId = source.CountryId,
	TsINNLength      = source.TsINNLength,
	Description      = source.Description COLLATE SQL_Latin1_General_CP1_CI_AS,
	ModifiedOn       = @CurrentDate;

DROP TABLE #DefaultAccountOwnership
