IF (OBJECT_ID('TsTRULead_UpdateLeadStageHistory') is not null)
	DROP TRIGGER TsTRULead_UpdateLeadStageHistory
GO
CREATE TRIGGER TsTRULead_UpdateLeadStageHistory ON Lead
AFTER UPDATE, INSERT
AS
	BEGIN
		IF NOT EXISTS( SELECT Id
					   FROM INSERTED )
			RETURN;

		DECLARE @CurrentDate DATETIME2 = GETUTCDATE( );
		IF NOT EXISTS( SELECT Id
					   FROM DELETED )
			BEGIN
				INSERT INTO TsLeadStageHistory (TsLeadId, TsQualifyStatusId, TsOwnerId, TsStartDate, CreatedById, ModifiedById)
					SELECT
						i.Id,
						i.QualifyStatusId,
						i.OwnerId,
						@CurrentDate,
						i.ModifiedById,
						i.CreatedById
					FROM INSERTED i
				RETURN;
			END;

		IF EXISTS( SELECT i.Id
				   FROM inserted i
					   JOIN deleted d
						   ON i.Id = d.Id
				   WHERE i.QualifyStatusId != d.QualifyStatusId OR i.OwnerId != d.OwnerId )
			BEGIN

				UPDATE TsLeadStageHistory
				SET TsDueDate    = @CurrentDate,
					ModifiedById = i.ModifiedById
				FROM TsLeadStageHistory
					JOIN inserted i
						ON i.Id = TsLeadStageHistory.TsLeadId
					JOIN deleted d
						ON i.Id = d.Id
				WHERE TsLeadStageHistory.TsDueDate IS NULL AND
					  ( i.QualifyStatusId != d.QualifyStatusId OR i.OwnerId != d.OwnerId )


				INSERT INTO TsLeadStageHistory (TsLeadId, TsOwnerId, TsQualifyStatusId, TsStartDate, CreatedById, ModifiedById)
					SELECT
						i.Id,
						i.OwnerId,
						i.QualifyStatusId,
						@CurrentDate,
						i.ModifiedById,
						i.ModifiedById
					FROM inserted i
						JOIN deleted d
							ON i.Id = d.Id
					WHERE i.QualifyStatusId != d.QualifyStatusId OR i.OwnerId != d.OwnerId
			END
	END