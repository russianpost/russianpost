IF object_id(N'TsVwOppExceedStageDuration', 'V') IS NOT NULL
	DROP VIEW [dbo].[TsVwOppExceedStageDuration]
GO

CREATE VIEW [dbo].[TsVwOppExceedStageDuration] AS
SELECT
  os.Name            AS TsOpportunityStageName,
  o.Id               AS TsOpportunityId,
  o.Title            AS TsOpportunityTitle,
  ISNULL(a.Name, '') AS TsAccountName,
  c.Name             AS TsOwnerName,
  HeadEmail.Number   AS TsHeadEmail
FROM Opportunity o
  JOIN OpportunityInStage ois ON ois.OpportunityId = o.Id AND ois.DueDate IS NULL
  JOIN TsOppStageInLeadType osilt
    ON osilt.TsOpportunityStageId = ois.StageId AND osilt.TsLeadTypeId = o.LeadTypeId AND osilt.TsMaxDurationInDays != 0
  JOIN OpportunityStage os ON os.Id = osilt.TsOpportunityStageId AND os.[End] != 1
  JOIN Contact c ON c.Id = ois.OwnerId
  JOIN Contact cHead ON cHead.Id = c.OwnerId
  CROSS APPLY (
                SELECT TOP 1 Number
                FROM ContactCommunication
                WHERE
                  ContactId = cHead.Id
                  AND CommunicationTypeId = 'EE1C85C3-CFCB-DF11-9B2A-001D60E938C6' --Email
                ORDER BY ModifiedOn DESC
              ) AS HeadEmail
  LEFT JOIN Account a ON a.Id = o.AccountId
WHERE
  GETUTCDATE() > DATEADD(DAY, osilt.TsMaxDurationInDays, ois.StartDate)