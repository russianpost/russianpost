IF NOT OBJECT_ID('[dbo].[tsp_FindDuplicate]') IS NULL
BEGIN
	DROP PROCEDURE [dbo].[tsp_FindDuplicate];
END;
GO
CREATE PROCEDURE [dbo].[tsp_FindDuplicate] (
	@schemaName NVARCHAR(MAX),
	@xmlRows NVARCHAR(MAX) = '',
	@sysAdminUnit UNIQUEIDENTIFIER = NULL
)
AS
BEGIN
	DECLARE @xmlRowsConfig XML;
	DECLARE @parsedConfig AS CreatingObjectInfo;
	DECLARE @minimumGroupCount INT = 1;
	DECLARE @execProceduresSql NVARCHAR(MAX);
	DECLARE @processingRowId UNIQUEIDENTIFIER;

	IF @sysAdminUnit IS NULL
	BEGIN
		SET @execProceduresSql = 'DELETE FROM '+ @schemaName +'DuplicateSearchResult';
		EXECUTE sp_executesql @execProceduresSql;
	END;
	ELSE
	BEGIN
		SET @execProceduresSql = 'DELETE FROM '+ @schemaName +'DuplicateSearchResult WHERE SysAdminUnitId = @sysAdminUnit';
		EXECUTE sp_executesql @execProceduresSql, N'@sysAdminUnit UNIQUEIDENTIFIER', @sysAdminUnit;
	END

	IF @xmlRows <> ''
	BEGIN
		SET @minimumGroupCount = 0;
		SET @xmlRowsConfig = CAST(@xmlRows AS XML);
		INSERT INTO @parsedConfig (Id, ObjectModifiedOn, CommunicationTypeId, CityId, Name, Number, SearchNumber, Web)
		SELECT
			NULLIF(b.value('(./Id/text())[1]', 'NVARCHAR(36)'), NEWID()) AS [Id],
			NULLIF(b.value('(./ContactModifiedOn/text())[1]', 'NVARCHAR(128)'), '') AS [ObjectModifiedOn],
			NULLIF(b.value('(./CommunicationTypeId/text())[1]', 'NVARCHAR(36)'), '') AS [CommunicationTypeId],
			NULL AS [CityId],
			[dbo].[fn_NormalizeFullName](NULLIF(b.value('(./Name/text())[1]', 'NVARCHAR(128)'), '')) AS [Name],
			[dbo].[fn_NormalizeString](NULLIF(b.value('(./Number/text())[1]', 'NVARCHAR(250)'), ''), N'0-9a-zа-я@_.') AS [Number],
			[dbo].[fn_ExtractDigitLimitFromNumber](LTRIM(
				[dbo].[fn_GetPhoneNumberSearchForm](NULLIF(b.value('(./Number/text())[1]', 'NVARCHAR(250)'), ''))
			)) AS [SearchNumber],
			NULLIF([dbo].[fn_ExtractDomainFromUrl](b.value('(./Number/text())[1]', 'NVARCHAR(250)')), '') AS [Web]
		FROM @xmlRowsConfig.nodes('/rows/row') as a(b);
		SET @processingRowId = (SELECT TOP 1 Id FROM @parsedConfig);
	END;

	SELECT
		@execProceduresSql = COALESCE(@execProceduresSql + '; ', '')
			+ 'EXEC [dbo].[' + [dr].[ProcedureName] + '] @parsedConfig, @sysAdminUnit, ''' + CAST([dr].[Id] AS NVARCHAR(MAX)) + ''''
	FROM [DuplicatesRule] [dr]
	JOIN [SysSchema] [ss] ON [ss].[UId] = [dr].[ObjectId]
	WHERE [ss].[Name] = @schemaName AND [dr].[IsActive] = 1 AND [dr].[ProcedureName] <> '';
	EXECUTE sp_executesql @execProceduresSql, N'@parsedConfig CreatingObjectInfo READONLY, @sysAdminUnit UNIQUEIDENTIFIER',
		@parsedConfig, @sysAdminUnit;

	SET @execProceduresSql = 'EXEC [dbo].[tsp_Group' + @schemaName + 'DuplicateSearchResult] @minimumGroupCount;';
	EXECUTE sp_executesql @execProceduresSql, N'@minimumGroupCount INT', @minimumGroupCount;
	IF @xmlRows <> ''
	BEGIN
		SET @execProceduresSql = 'DELETE FROM '+ @schemaName +'DuplicateSearchResult WHERE '
			+ @schemaName + 'Id = @processingRowId AND SysAdminUnitId = @sysAdminUnit';
		EXECUTE sp_executesql @execProceduresSql, N'@processingRowId UNIQUEIDENTIFIER, @sysAdminUnit UNIQUEIDENTIFIER',
			@processingRowId, @sysAdminUnit;
	END;
END;
GO
