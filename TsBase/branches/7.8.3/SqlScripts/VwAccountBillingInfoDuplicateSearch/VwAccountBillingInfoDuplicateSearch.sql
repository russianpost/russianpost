IF OBJECT_ID('dbo.VwAccountBillingInfoDuplicateSearch', 'V') IS NOT NULL
BEGIN
	DROP VIEW dbo.[VwAccountBillingInfoDuplicateSearch]
END
GO

CREATE VIEW [dbo].[VwAccountBillingInfoDuplicateSearch] WITH SCHEMABINDING AS 
	SELECT
		[BI].[Id] AS [AccountBillingInfoId],
		[A].[Id] AS [AccountId],
		[A].[ModifiedOn] AS [AccountModifiedOn],
		[A].[TsINN] AS Inn,
		[A].[TsKPP] AS Kpp,
		[BI].[TsKPP] AS BillingInfoKpp,
		[BI].[TsBIK] AS Bik
	FROM 
		[dbo].[Account] A
	JOIN 
		[dbo].[AccountBillingInfo] BI 
	ON 
		BI.AccountId = A.Id
GO

CREATE UNIQUE CLUSTERED INDEX [IVwAccountBillingInfoDuplicateSearch_AccountBillingInfoId] ON [dbo].[VwAccountBillingInfoDuplicateSearch]
(
	[AccountId], [AccountBillingInfoId]
) 

CREATE NONCLUSTERED INDEX [IVwAccountBillingInfoDuplicateSearch_Inn] ON [dbo].[VwAccountBillingInfoDuplicateSearch]
(
	[Inn] ASC
)
INCLUDE ([AccountId]) 

CREATE NONCLUSTERED INDEX [IVwAccountBillingInfoDuplicateSearch_Kpp] ON [dbo].[VwAccountBillingInfoDuplicateSearch]
(
	[Kpp] ASC
)
INCLUDE ([AccountId]) 

CREATE NONCLUSTERED INDEX [IVwAccountBillingInfoDuplicateSearch_BillingInfoKpp] ON [dbo].[VwAccountBillingInfoDuplicateSearch]
(
	[BillingInfoKpp] ASC
)
INCLUDE ([AccountId]) 

CREATE NONCLUSTERED INDEX [IVwAccountBillingInfoDuplicateSearch_Bik] ON [dbo].[VwAccountBillingInfoDuplicateSearch]
(
	[Bik] ASC
)
INCLUDE ([AccountId]) 