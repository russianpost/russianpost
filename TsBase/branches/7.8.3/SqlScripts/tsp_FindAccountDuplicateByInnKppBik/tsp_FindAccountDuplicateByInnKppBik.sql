IF NOT OBJECT_ID('[dbo].[tsp_FindAccountDuplicateByInnKppBik]') IS NULL
BEGIN
	DROP PROCEDURE [dbo].[tsp_FindAccountDuplicateByInnKppBik];
END;
GO
CREATE PROCEDURE [dbo].[tsp_FindAccountDuplicateByInnKppBik] (
	@parsedConfig CreatingObjectInfo READONLY,
	@sysAdminUnit UNIQUEIDENTIFIER,
	@ruleId UNIQUEIDENTIFIER
)
AS
BEGIN
	DECLARE @parsedConfigRowsCount INT = (SELECT COUNT(*) FROM @parsedConfig);
	IF OBJECT_ID('tempdb..#searchAccount') IS NOT NULL
	BEGIN
		DROP TABLE #searchAccount
	END
	CREATE TABLE #searchAccount (
		[Inn] NVARCHAR(128) COLLATE database_default,
		[Kpp] NVARCHAR(128) COLLATE database_default,
		[Bik] NVARCHAR(128) COLLATE database_default,
		[SortDate] DATETIME
	);


	IF @parsedConfigRowsCount = 0
	BEGIN
		INSERT INTO #searchAccount ([Inn], [Kpp], [Bik], [SortDate])
		SELECT 
			[dedup].Inn,
			[dedup].Kpp,
			[dedup].Bik,
			MAX([dedup].[SortDate]) [SortDate]
		 FROM
			(SELECT 
				[Ac].Id, 
				ISNULL([Ac].TsINN, '') AS Inn,
				ISNULL([Ac].TsKPP, '') AS Kpp,
				ISNULL([BI].Bik, '') AS Bik,
				MAX([ModifiedOn]) [SortDate] 
			FROM 
				Account [Ac]
			LEFT JOIN 
				VwAccountBillingInfoDuplicateSearch [BI]
			ON [Ac].Id = [BI].AccountId			
			GROUP BY [Ac].Id, [Ac].TsINN, [Ac].TsKPP, [BI].Bik) AS [dedup]
		GROUP BY [dedup].[Inn], [dedup].Kpp, [dedup].Bik
		HAVING COUNT(*) > 1;		
	END;
	ELSE
	BEGIN
		INSERT INTO #searchAccount ([Inn], [Kpp], [Bik], [SortDate])
		SELECT 
			[AC].Inn, 
			[AC].Kpp, 
			[BI].Bik, 
			GETDATE() AS [SortDate] 
		FROM 
			@parsedConfig [AC]
		LEFT JOIN 
			VwAccountBillingInfoDuplicateSearch [BI] ON [AC].Id = [BI].AccountId
	END;

	INSERT INTO [AccountDuplicateSearchResult] ([AccountId], [GroupId], [RuleId], [SysAdminUnitId])
	SELECT
		resTab.Id,
		DENSE_RANK() OVER (ORDER BY resTab.[SortDate] DESC, resTab.Inn ASC),
		@ruleId [RuleId],
		@sysAdminUnit
	FROM (
	SELECT 
		Id, ModifiedOn, Inn, Kpp, Bik, SortDate
	FROM (
	--ИННы совпадают, а КПП и БИКи пустые
		SELECT 
			Id AS Id, ModifiedOn AS ModifiedOn, src.Inn AS Inn, src.Kpp AS Kpp, src.Bik AS Bik, r.SortDate
		FROM
			(SELECT 
				[AC].Id AS Id, 
				[AC].ModifiedOn AS ModifiedOn, 
				[AC].TsInn AS Inn, 
				[AC].TsKPP AS Kpp,
				[BI].Bik AS Bik
			FROM 
				Account [AC]
			LEFT JOIN 
					VwAccountBillingInfoDuplicateSearch [BI] ON [AC].Id = [BI].AccountId) src, #searchAccount r
		WHERE 
			src.Inn = r.Inn AND 
			(ISNULL(r.Bik, '') = '' OR r.Bik = '0') AND
			ISNULL(r.Kpp, 0) =  0 AND (ISNULL(src.Bik, '') = '' OR src.Bik = '0') AND
			ISNULL(src.Kpp, 0) =  0
		
		UNION ALL
		--КПП совпадают, а ИННы пустые или не совпадают и БИКи пустые
		SELECT 
			Id AS Id, ModifiedOn AS ModifiedOn, src.Inn AS Inn, src.Kpp AS Kpp, src.Bik AS Bik, r.SortDate
		FROM
			(SELECT 
				[AC].Id AS Id, 
				[AC].ModifiedOn AS ModifiedOn, 
				[AC].TsInn AS Inn, 
				[AC].TsKPP AS Kpp,
				[BI].Bik AS Bik
			FROM 
				Account [AC]
			LEFT JOIN 
					VwAccountBillingInfoDuplicateSearch [BI] ON [AC].Id = [BI].AccountId) src, #searchAccount r
			WHERE 
				src.Kpp = r.Kpp AND 
				(ISNULL(r.Bik, '') = '' OR r.Bik = '0') AND 
				(ISNULL(src.Bik, '') = '' OR src.Bik = '0')
			UNION ALL
		--БИКи совпадают
			SELECT 
				Id AS Id, ModifiedOn AS ModifiedOn, src.Inn AS Inn, src.Kpp AS Kpp, src.Bik AS Bik, r.SortDate
			FROM
				(SELECT 
					[AC].Id AS Id, 
					[AC].ModifiedOn AS ModifiedOn, 
					[AC].TsInn AS Inn, 
					[AC].TsKPP AS Kpp,
					[BI].Bik AS Bik
				FROM 
					Account [AC]
				LEFT JOIN 
						VwAccountBillingInfoDuplicateSearch [BI] ON [AC].Id = [BI].AccountId) src, #searchAccount r
				WHERE 
					src.Bik = r.Bik AND ISNULL(r.Bik, '') <> '' AND r.Bik <> '0'
			UNION ALL
		--ИННы и КПП совпадают, а БИКи пустые
			SELECT 
				Id AS Id, ModifiedOn AS ModifiedOn, src.Inn AS Inn, src.Kpp AS Kpp, src.Bik AS Bik, r.SortDate
			FROM
				(SELECT 
					[AC].Id AS Id, 
					[AC].ModifiedOn AS ModifiedOn, 
					[AC].TsInn AS Inn, 
					[AC].TsKPP AS Kpp,
					[BI].Bik AS Bik
				FROM 
					Account [AC]
				LEFT JOIN 
						VwAccountBillingInfoDuplicateSearch [BI] ON [AC].Id = [BI].AccountId) src, #searchAccount r
			WHERE
				src.Inn = r.Inn AND 
				src.Kpp = r.Kpp AND 
				(ISNULL(src.Bik, '') = '' OR src.Bik = '0') AND (ISNULL(r.Bik, '') = '' OR r.Bik = '0') 
			UNION ALL
		--ИННы и БИКи совпадают, а КПП пустые или не совпадают
			SELECT 
				Id AS Id, ModifiedOn AS ModifiedOn, src.Inn AS Inn, src.Kpp AS Kpp, src.Bik AS Bik, r.SortDate
			FROM
				(SELECT 
					[AC].Id AS Id, 
					[AC].ModifiedOn AS ModifiedOn, 
					[AC].TsInn AS Inn, 
					[AC].TsKPP AS Kpp,
					[BI].Bik AS Bik
				FROM 
					Account [AC]
				LEFT JOIN 
						VwAccountBillingInfoDuplicateSearch [BI] ON [AC].Id = [BI].AccountId) src, #searchAccount r
			WHERE
				src.Inn = r.Inn AND 
				src.Bik = r.Bik AND 
				ISNULL(src.Kpp, 0) =  0 AND
				ISNULL(r.Kpp, 0) =  0 
			UNION ALL
		--БИКи и КПП совпадают, а ИННы пустые или не совпадают
			SELECT 
				Id AS Id, ModifiedOn AS ModifiedOn, src.Inn AS Inn, src.Kpp AS Kpp, src.Bik AS Bik, r.SortDate
			FROM
				(SELECT 
					[AC].Id AS Id, 
					[AC].ModifiedOn AS ModifiedOn, 
					[AC].TsInn AS Inn, 
					[AC].TsKPP AS Kpp,
					[BI].Bik AS Bik
				FROM 
					Account [AC]
				LEFT JOIN 
						VwAccountBillingInfoDuplicateSearch [BI] ON [AC].Id = [BI].AccountId) src, #searchAccount r
			WHERE
				src.Kpp = r.Kpp AND 
				src.Bik = r.Bik AND 
				(ISNULL(src.Inn, '') =  '' OR src.Inn = '0') AND
				(ISNULL(r.Inn, '') =  '' OR r.Inn = '0')
			UNION ALL
		--БИКи и КПП и ИННы совпадают
			SELECT 
				Id AS Id, ModifiedOn AS ModifiedOn, src.Inn AS Inn, src.Kpp AS Kpp, src.Bik AS Bik, r.SortDate
			FROM
				(SELECT 
					[AC].Id AS Id, 
					[AC].ModifiedOn AS ModifiedOn, 
					[AC].TsInn AS Inn, 
					[AC].TsKPP AS Kpp,
					[BI].Bik AS Bik
				FROM 
					Account [AC]
				LEFT JOIN 
						VwAccountBillingInfoDuplicateSearch [BI] ON [AC].Id = [BI].AccountId) src, #searchAccount r
			WHERE
				src.Kpp = r.Kpp AND 
				src.Bik = r.Bik AND 
				src.Inn = r.Inn
		) tab
		GROUP BY Id, ModifiedOn, Inn, Kpp, Bik, SortDate) resTab
END;
GO