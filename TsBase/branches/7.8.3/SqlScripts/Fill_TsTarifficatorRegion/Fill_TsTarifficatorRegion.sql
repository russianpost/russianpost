IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '43455237-50AB-4A46-91D2-0443831356E6' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '43455237-50AB-4A46-91D2-0443831356E6', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Адыгея', '', 0, 1 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'EDC4952B-612E-4FC7-8CE0-7620B94B2077' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'EDC4952B-612E-4FC7-8CE0-7620B94B2077', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Алтай', '', 0, 4 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '2932B179-250F-436B-8E61-AD2FF699A8F2' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '2932B179-250F-436B-8E61-AD2FF699A8F2', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Башкортостан', '', 0, 2 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '0551E22E-CEF6-4914-8190-EA099AF9041E' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '0551E22E-CEF6-4914-8190-EA099AF9041E', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Бурятия', '', 0, 3 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'CFCE5CFE-19C7-4FBD-9FFF-9A2AEA47F11A' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'CFCE5CFE-19C7-4FBD-9FFF-9A2AEA47F11A', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Дагестан', '', 0, 5 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'B0CF7F6C-4A19-486D-BAD1-07C8131FE3DD' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'B0CF7F6C-4A19-486D-BAD1-07C8131FE3DD', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Ингушетия', '', 0, 6 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '5B82AAA2-6078-421E-B257-315098671A94' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '5B82AAA2-6078-421E-B257-315098671A94', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Кабардино-Балкарская республика', '', 0, 7 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '7E3950CA-E8F5-4DDF-A174-D2458C80DCCB' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '7E3950CA-E8F5-4DDF-A174-D2458C80DCCB', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Калмыкия', '', 0, 8 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'B1FE4D74-6112-4066-B496-71D7C8D49340' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'B1FE4D74-6112-4066-B496-71D7C8D49340', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Карачаево-Черкесская республика', '', 0, 9 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '252F2597-51D8-43AB-927C-81435D29F37F' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '252F2597-51D8-43AB-927C-81435D29F37F', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Карелия', '', 0, 10 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'AB52F3AE-E7D5-4502-9816-39A117E17772' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'AB52F3AE-E7D5-4502-9816-39A117E17772', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Коми', '', 0, 11 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '1E900C94-2508-4175-A90F-822B3E0A8E5E' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '1E900C94-2508-4175-A90F-822B3E0A8E5E', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Крым', '', 0, 91 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'C163AC20-E8D5-47A0-8779-5A92A9C3A48C' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'C163AC20-E8D5-47A0-8779-5A92A9C3A48C', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Марий Эл', '', 0, 12 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'C0EAFCE0-8864-4B97-85B2-5F2A49AE53C4' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'C0EAFCE0-8864-4B97-85B2-5F2A49AE53C4', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Мордовия', '', 0, 13 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '7E5D909B-FC9E-4E16-8B19-4FA4BE9B0CA3' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '7E5D909B-FC9E-4E16-8B19-4FA4BE9B0CA3', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Саха (Якутия)', '', 0, 14 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '71388758-EA4B-4CC6-AA67-920673D7A5EF' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '71388758-EA4B-4CC6-AA67-920673D7A5EF', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Северная Осетия — Алания', '', 0, 15 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '8C835548-DC40-4034-804A-F201D9A88B27' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '8C835548-DC40-4034-804A-F201D9A88B27', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Татарстан', '', 0, 16 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'C7B1FD10-3368-4C02-90CE-C9F8127C6C09' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'C7B1FD10-3368-4C02-90CE-C9F8127C6C09', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Тыва', '', 0, 17 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'F1157A89-70A8-42E1-82D2-EA170B458E93' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'F1157A89-70A8-42E1-82D2-EA170B458E93', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Удмуртская республика', '', 0, 18 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '75160DF8-15D9-4DC6-B87C-74964B968F93' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '75160DF8-15D9-4DC6-B87C-74964B968F93', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Республика Хакасия', '', 0, 19 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'AF6034E2-3CF8-4952-9521-4DBB78BCFE17' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'AF6034E2-3CF8-4952-9521-4DBB78BCFE17', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Чеченская республика', '', 0, 20 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'FA233253-2D8D-4D2B-82C3-30DCF4DBCCA4' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'FA233253-2D8D-4D2B-82C3-30DCF4DBCCA4', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Чувашская республика', '', 0, 21 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '0F23178D-ED46-4D7E-B5C5-2F6456E26E66' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '0F23178D-ED46-4D7E-B5C5-2F6456E26E66', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Алтайский край', '', 0, 22 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '697FF0B3-ED36-4F7A-AF31-1037326C08D2' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '697FF0B3-ED36-4F7A-AF31-1037326C08D2', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Забайкальский край', '', 0, 75 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'E3327391-1DC5-4342-8B85-148F336D6E79' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'E3327391-1DC5-4342-8B85-148F336D6E79', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Камчатский край', '', 0, 41 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '9A2429AB-DE3D-46EA-83F2-5DE902834180' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '9A2429AB-DE3D-46EA-83F2-5DE902834180', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Краснодарский край', '', 0, 23 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '314F6866-F06E-44E8-9D73-B2A045C64812' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '314F6866-F06E-44E8-9D73-B2A045C64812', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Красноярский край', '', 0, 24 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '57C59DAE-2FD1-4550-A9C2-DC713E9276B0' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '57C59DAE-2FD1-4550-A9C2-DC713E9276B0', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Пермский край', '', 0, 59 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '31C1E239-0765-41A6-A167-145ABF14E63A' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '31C1E239-0765-41A6-A167-145ABF14E63A', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Приморский край', '', 0, 25 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '9D0463D8-84C9-4B37-A82D-C9D859092CE4' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '9D0463D8-84C9-4B37-A82D-C9D859092CE4', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Ставропольский край', '', 0, 26 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'B3F74BE2-43CD-483B-B512-413605B6E26F' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'B3F74BE2-43CD-483B-B512-413605B6E26F', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Хабаровский край', '', 0, 27 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '140EB717-58E2-4AB6-82A0-DF0E092C546F' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '140EB717-58E2-4AB6-82A0-DF0E092C546F', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Амурская область', '', 0, 28 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '765BB41C-1E79-4061-83A5-B345BD79E0A6' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '765BB41C-1E79-4061-83A5-B345BD79E0A6', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Архангельская область', '', 0, 29 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '3B2D8676-F5D3-43A2-BF1F-E82EBC3CD8C4' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '3B2D8676-F5D3-43A2-BF1F-E82EBC3CD8C4', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Астраханская область', '', 0, 30 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'EEE943DA-AFAE-4D49-9A4B-D2B2E3F64D45' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'EEE943DA-AFAE-4D49-9A4B-D2B2E3F64D45', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Белгородская область', '', 0, 31 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '227648BE-2DF1-440E-876E-B85CA41A56AC' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '227648BE-2DF1-440E-876E-B85CA41A56AC', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Брянская область', '', 0, 32 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '452B4EDD-27B7-4CD3-8412-B29BF12199F4' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '452B4EDD-27B7-4CD3-8412-B29BF12199F4', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Владимирская область', '', 0, 33 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '45BBCE32-7036-4464-88FE-66E4BB0DF0D7' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '45BBCE32-7036-4464-88FE-66E4BB0DF0D7', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Волгоградская область', '', 0, 34 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'C97A0BE9-55C2-4852-9B39-C2B4409BB0EA' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'C97A0BE9-55C2-4852-9B39-C2B4409BB0EA', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Вологодская область', '', 0, 35 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '364DADFA-0FAA-49B2-8C4E-3235BBF849B5' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '364DADFA-0FAA-49B2-8C4E-3235BBF849B5', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Воронежская область', '', 0, 36 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '33226750-C233-4975-A72A-6E75FCB66584' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '33226750-C233-4975-A72A-6E75FCB66584', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Ивановская область', '', 0, 37 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '7449336C-C012-4D38-8C62-ACA3792826E7' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '7449336C-C012-4D38-8C62-ACA3792826E7', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Иркутская область', '', 0, 38 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '8817B85F-DE1E-48B7-ABCE-E968E9B67EB4' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '8817B85F-DE1E-48B7-ABCE-E968E9B67EB4', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Калининградская область', '', 0, 39 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '71085529-21CB-45E9-BFBE-71BC6B2A5316' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '71085529-21CB-45E9-BFBE-71BC6B2A5316', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Калужская область', '', 0, 40 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'EAAB87A5-5204-496F-84F6-B195E4A72821' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'EAAB87A5-5204-496F-84F6-B195E4A72821', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Кемеровская область', '', 0, 42 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '5A8038B6-3734-4138-947D-741CF78F1C23' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '5A8038B6-3734-4138-947D-741CF78F1C23', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Кировская область', '', 0, 43 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '20E2527B-73F5-46CA-BC6B-BBE5ED88BC9B' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '20E2527B-73F5-46CA-BC6B-BBE5ED88BC9B', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Костромская область', '', 0, 44 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'D76F5A46-E919-4889-917A-23CDBCAA1807' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'D76F5A46-E919-4889-917A-23CDBCAA1807', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Курганская область', '', 0, 45 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '2237920D-48F5-4F5D-ADA6-FEB2C332E9D6' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '2237920D-48F5-4F5D-ADA6-FEB2C332E9D6', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Курская область', '', 0, 46 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '2EF1D879-5CC4-4E6A-AA02-CC2F6C8E7258' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '2EF1D879-5CC4-4E6A-AA02-CC2F6C8E7258', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Ленинградская область', '', 0, 47 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'B3FE35B3-D0F2-4741-B8DD-0531050058B0' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'B3FE35B3-D0F2-4741-B8DD-0531050058B0', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Липецкая область', '', 0, 48 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'A45ED337-E53C-4E5A-826A-8F3756038727' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'A45ED337-E53C-4E5A-826A-8F3756038727', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Магаданская область', '', 0, 49 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '25691A5B-1CEC-448E-84FC-A8E11B1F5C9C' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '25691A5B-1CEC-448E-84FC-A8E11B1F5C9C', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Московская область', '', 0, 50 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'ECB246AB-DC30-4A98-9CA5-F2E998023F93' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'ECB246AB-DC30-4A98-9CA5-F2E998023F93', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Мурманская область', '', 0, 51 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '57390EF0-6E48-46CF-A252-142DC27DF2EB' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '57390EF0-6E48-46CF-A252-142DC27DF2EB', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Нижегородская область', '', 0, 52 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '5A560FC2-D8A4-445E-937E-A749FDA791A1' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '5A560FC2-D8A4-445E-937E-A749FDA791A1', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Новгородская область', '', 0, 53 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '42DB67A1-7775-4F2B-8550-1914E3EFF8E1' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '42DB67A1-7775-4F2B-8550-1914E3EFF8E1', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Новосибирская область', '', 0, 54 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '4FB28E5D-AB2F-4BF1-9006-C63ABB394782' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '4FB28E5D-AB2F-4BF1-9006-C63ABB394782', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Омская область', '', 0, 55 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'E6655C29-87EB-42E8-BA24-0134503853DF' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'E6655C29-87EB-42E8-BA24-0134503853DF', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Оренбургская область', '', 0, 56 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '049FB833-A07C-454C-8A71-8EC496C6B5BD' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '049FB833-A07C-454C-8A71-8EC496C6B5BD', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Орловская область', '', 0, 57 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '045CD378-C33E-4630-94F3-9CF269426792' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '045CD378-C33E-4630-94F3-9CF269426792', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Пензенская область', '', 0, 58 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '77E1F8C5-7CA5-492C-8EB6-043B2B735686' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '77E1F8C5-7CA5-492C-8EB6-043B2B735686', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Псковская область', '', 0, 60 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '15549D0C-4995-40C9-8D03-0528FBDBA6E3' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '15549D0C-4995-40C9-8D03-0528FBDBA6E3', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Ростовская область', '', 0, 61 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'C8C82197-D69C-4E25-9E33-4175B049EA30' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'C8C82197-D69C-4E25-9E33-4175B049EA30', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Рязанская область', '', 0, 62 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'BDF04C1C-E824-4557-8738-C0C459EA5F32' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'BDF04C1C-E824-4557-8738-C0C459EA5F32', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Самарская область', '', 0, 63 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '30CAE459-05EC-4A46-A660-7E47C595FBF1' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '30CAE459-05EC-4A46-A660-7E47C595FBF1', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Саратовская область', '', 0, 64 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'B2D055E1-63DB-44E3-8181-C6A7699C1C4A' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'B2D055E1-63DB-44E3-8181-C6A7699C1C4A', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Сахалинская область', '', 0, 65 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '8C118478-407D-482B-B489-59147DFB2A6C' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '8C118478-407D-482B-B489-59147DFB2A6C', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Свердловская область', '', 0, 66 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'D7D80EEE-2F4E-4025-9B16-5123FF909100' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'D7D80EEE-2F4E-4025-9B16-5123FF909100', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Смоленская область', '', 0, 67 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '7EEBC50F-71B5-4160-A7F9-E9A86DB5C5C9' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '7EEBC50F-71B5-4160-A7F9-E9A86DB5C5C9', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Тамбовская область', '', 0, 68 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '526BE685-D35E-4CFE-B7CD-770B91A147F0' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '526BE685-D35E-4CFE-B7CD-770B91A147F0', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Тверская область', '', 0, 69 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '1E35524A-A8C3-4D47-9B4D-E9BCAB311179' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '1E35524A-A8C3-4D47-9B4D-E9BCAB311179', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Томская область', '', 0, 70 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '398EAE50-6EC1-4EDE-86CB-098E1ACCA878' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '398EAE50-6EC1-4EDE-86CB-098E1ACCA878', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Тульская область', '', 0, 71 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '481A7315-421E-4F7C-9C23-31471AB45DF2' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '481A7315-421E-4F7C-9C23-31471AB45DF2', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Тюменская область', '', 0, 72 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'C5DA30D7-0D2E-460E-B52F-EC48079967B3' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'C5DA30D7-0D2E-460E-B52F-EC48079967B3', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Ульяновская область', '', 0, 73 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'A4F338E6-856A-4864-99AA-62245ED36447' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'A4F338E6-856A-4864-99AA-62245ED36447', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Челябинская область', '', 0, 74 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'AF92155C-8F44-4B71-8048-3B80EB7A060A' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'AF92155C-8F44-4B71-8048-3B80EB7A060A', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Ярославская область', '', 0, 76 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '9F03664C-F17C-4529-81A1-23999CC025BD' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '9F03664C-F17C-4529-81A1-23999CC025BD', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Москва', '', 0, 77 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'D3827FDA-2A4E-4D7D-A44A-D31396B7933E' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'D3827FDA-2A4E-4D7D-A44A-D31396B7933E', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Санкт-Петербург', '', 0, 78 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '9C1CAF17-BA80-4399-9221-E9742EA6AE2D' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '9C1CAF17-BA80-4399-9221-E9742EA6AE2D', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Севастополь', '', 0, 92 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'AA77FC4B-0A58-405D-ADA0-30B64D372CE9' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'AA77FC4B-0A58-405D-ADA0-30B64D372CE9', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Еврейская автономная область', '', 0, 79 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'CD8F62C5-C93B-4466-83BB-66E577856A43' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'CD8F62C5-C93B-4466-83BB-66E577856A43', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Ненецкий автономный округ', '', 0, 83 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '522E0A37-0C15-452D-A209-C2AB959E5F44' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '522E0A37-0C15-452D-A209-C2AB959E5F44', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Ханты-Мансийский автономный округ - Югра', '', 0, 86 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = '05F65767-0172-4785-9919-C5370A4BED92' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( '05F65767-0172-4785-9919-C5370A4BED92', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Чукотский автономный округ', '', 0, 87 );
IF ( ( SELECT 1
	   FROM TsTarifficatorRegion
	   WHERE Id = 'B18BD323-FFF5-4D79-9523-9F58B75E136A' ) IS NULL )
	INSERT INTO TsTarifficatorRegion (Id, CreatedOn, CreatedById, ModifiedOn, ModifiedById, Name, Description, ProcessListeners, TsCode)
	VALUES ( 'B18BD323-FFF5-4D79-9523-9F58B75E136A', '2016-10-03 17:40:47.6370000', NULL, '2016-10-03 17:40:47.6370000',
			 NULL, 'Ямало-Ненецкий автономный округ', '', 0, 89 );