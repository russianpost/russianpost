define("AccountPageV2", ["ConfigurationConstants", "BusinessRuleModule", "TsJsConsts"],
	function(ConfigurationConstants, BusinessRuleModule, TsJsConsts) {
		return {
			entitySchemaName: "Account",
			attributes: {
				"IsKPPVisible": {
					type: Terrasoft.ViewModelColumnType.VIRTUAL_COLUMN,
					dataValueType: Terrasoft.DataValueType.BOOLEAN,
					value: true
				},
				"Ownership": {
					"dependencies": [
						{
							"columns": ["Ownership"],
							"methodName": "onOwnershipChanged"
						}
					]
				}
			},
			details: /**SCHEMA_DETAILS*/{
				"TsDiscountInAccountDetailV2": {
					"schemaName": "TsDiscountInAccountDetailV2",
					"filter": {
						"masterColumn": "Id",
						"detailColumn": "TsAccount"
					}
				}
			}/**SCHEMA_DETAILS*/,
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "merge",
					"name": "AccountName",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						}
					}
				},
				{
					"operation": "merge",
					"name": "AccountType",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						}
					}
				},
				{
					"operation": "merge",
					"name": "AccountOwner",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 2
						}
					}
				},
				{
					"operation": "merge",
					"name": "AccountWeb",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 3
						}
					}
				},
				{
					"operation": "merge",
					"name": "AccountPhone",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 4
						}
					}
				},
				{
					"operation": "merge",
					"name": "NewAccountCategory",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 5
						}
					}
				},
				{
					"operation": "merge",
					"name": "AccountIndustry",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 6
						}
					}
				},
				{
					"operation": "insert",
					"name": "TsAccountBusinessType",
					"values": {
						"bindTo": "TsAccountBusinessType",
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 7
						},
						"contentType": 3
					},
					"parentName": "ProfileContainer",
					"propertyName": "items",
					"index": 7
				},
				{
					"operation": "merge",
					"name": "AccountCompletenessContainer",
					"values": {
						"layout": {
							"colSpan": 24,
							"rowSpan": 1,
							"column": 0,
							"row": 8
						}
					}
				},
				{
					"operation": "merge",
					"name": "AlternativeName",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						}
					}
				},
				{
					"operation": "merge",
					"name": "Code",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0
						}
					}
				},
				{
					"operation": "insert",
					"name": "TsFullName",
					"values": {
						"bindTo": "TsFullName",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						}
					},
					"parentName": "AccountPageGeneralInfoBlock",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsBrandName",
					"values": {
						"bindTo": "TsBrandName",
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1
						}
					},
					"parentName": "AccountPageGeneralInfoBlock",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsTarifficatorRegion",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "AccountPageGeneralInfoBlock"
						},
						"bindTo": "TsTarifficatorRegion"
					},
					"parentName": "AccountPageGeneralInfoBlock",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "merge",
					"name": "Ownership",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0
						}
					}
				},
				{
					"operation": "insert",
					"name": "TsINN",
					"values": {
						"layout": {
							"colSpan": 6,
							"rowSpan": 1,
							"column": 12,
							"row": 0
						}
					},
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items"
				},
				{
					"operation": "insert",
					"name": "TsKPP",
					"values": {
						"layout": {
							"colSpan": 6,
							"rowSpan": 1,
							"column": 18,
							"row": 0
						},
						"visible": {"bindTo": "IsKPPVisible"}
					},
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items"
				},
				{
					"operation": "merge",
					"name": "AnnualRevenue",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1
						}
					}
				},
				{
					"operation": "insert",
					"name": "TsAccountLevel",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1,
							"layoutName": "CategoriesControlGroupContainer"
						},
						"bindTo": "TsAccountLevel"
					},
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccountRegionType",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesControlGroupContainer"
						},
						"contentType": 3,
						"bindTo": "TsAccountRegionType"
					},
					"parentName": "CategoriesControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "merge",
					"name": "EmployeesNumber",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 2
						}
					}
				},
				{
					"operation": "insert",
					"name": "CategoriesPostBusinessControlGroup",
					"values": {
						"itemType": 15,
						"caption": {
							"bindTo": "Resources.Strings.CategoriesPostBusinessGroupCaption"
						},
						"items": []
					},
					"parentName": "AccountPageGeneralTabContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "insert",
					"name": "CategoriesPostBusinessControlGroupContainer",
					"values": {
						"itemType": 0,
						"items": []
					},
					"parentName": "CategoriesPostBusinessControlGroup",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2BPostBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "CategoriesPostBusinessControlGroupContainer"
						},
						"bindTo": "TsB2BPostBusiness"
					},
					"parentName": "CategoriesPostBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2CPostBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0,
							"layoutName": "CategoriesPostBusinessControlGroupContainer"
						},
						"bindTo": "TsB2CPostBusiness"
					},
					"parentName": "CategoriesPostBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "TsG2BPostBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesPostBusinessControlGroupContainer"
						},
						"bindTo": "TsG2BPostBusiness"
					},
					"parentName": "CategoriesPostBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsVipPostBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1,
							"layoutName": "CategoriesPostBusinessControlGroupContainer"
						},
						"bindTo": "TsVipPostBusiness"
					},
					"parentName": "CategoriesPostBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccountPostCategory",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesPostBusinessControlGroupContainer"
						},
						"contentType": 3,
						"bindTo": "TsAccountPostCategory"
					},
					"parentName": "CategoriesPostBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsExecutivePostBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 2,
							"layoutName": "CategoriesPostBusinessControlGroupContainer"
						},
						"bindTo": "TsExecutivePostBusiness",
						"labelConfig": {},
						"enabled": true,
						"contentType": 5
					},
					"parentName": "CategoriesPostBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsExecSuppPostBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 3,
							"layoutName": "CategoriesPostBusinessControlGroupContainer"
						},
						"bindTo": "TsExecSuppPostBusiness"
					},
					"parentName": "CategoriesPostBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "insert",
					"name": "CategoriesParcelBusinessControlGroup",
					"values": {
						"itemType": 15,
						"caption": {
							"bindTo": "Resources.Strings.CategoriesParcelBusinessGroupCaption"
						},
						"items": []
					},
					"parentName": "AccountPageGeneralTabContainer",
					"propertyName": "items",
					"index": 7
				},
				{
					"operation": "insert",
					"name": "CategoriesParcelBusinessControlGroupContainer",
					"values": {
						"itemType": 0,
						"items": []
					},
					"parentName": "CategoriesParcelBusinessControlGroup",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2BParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsB2BParcelBusiness"
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2CParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsB2CParcelBusiness"
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "TsG2BParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsG2BParcelBusiness"
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsVipParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsVipParcelBusiness"
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccountParcelCategory",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsAccountParcelCategory",
						"labelConfig": {},
						"enabled": true,
						"contentType": 3
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsExecutiveParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 2,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsExecutiveParcelBusiness",
						"labelConfig": {},
						"enabled": true,
						"contentType": 5
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsExecSuppParcelBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 3,
							"layoutName": "CategoriesParcelBusinessControlGroupContainer"
						},
						"bindTo": "TsExecSuppParcelBusiness"
					},
					"parentName": "CategoriesParcelBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "insert",
					"name": "CategoriesFinancialBusinessControlGroup",
					"values": {
						"itemType": 15,
						"caption": {
							"bindTo": "Resources.Strings.CategoriesFinancialBusinessGroupCaption"
						},
						"items": []
					},
					"parentName": "AccountPageGeneralTabContainer",
					"propertyName": "items",
					"index": 8
				},
				{
					"operation": "insert",
					"name": "CategoriesFinancialBusinessControlGroupContainer",
					"values": {
						"itemType": 0,
						"items": []
					},
					"parentName": "CategoriesFinancialBusinessControlGroup",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2BFinancialBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsB2BFinancialBusiness"
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2CFinancialBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsB2CFinancialBusiness"
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "TsG2BFinancialBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsG2BFinancialBusiness"
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsVipFinancialBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsVipFinancialBusiness"
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccountFinancialCategory",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsAccountFinancialCategory",
						"contentType": 3
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsExecutiveFinancialBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 2,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsExecutiveFinancialBusiness",
						"labelConfig": {},
						"enabled": true,
						"contentType": 5
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsExecSuppFinBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 3,
							"layoutName": "CategoriesFinancialBusinessControlGroupContainer"
						},
						"bindTo": "TsExecSuppFinBusiness"
					},
					"parentName": "CategoriesFinancialBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "insert",
					"name": "CategoriesAdvertBusinessControlGroup",
					"values": {
						"itemType": 15,
						"caption": {
							"bindTo": "Resources.Strings.CategoriesAdvertBusinessGroupCaption"
						},
						"items": []
					},
					"parentName": "AccountPageGeneralTabContainer",
					"propertyName": "items",
					"index": 9
				},
				{
					"operation": "insert",
					"name": "CategoriesAdvertBusinessControlGroupContainer",
					"values": {
						"itemType": 0,
						"items": []
					},
					"parentName": "CategoriesAdvertBusinessControlGroup",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2BAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 0,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsB2BAdvertBusiness"
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 0
				},
				{
					"operation": "insert",
					"name": "TsB2CAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 0,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsB2CAdvertBusiness"
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 1
				},
				{
					"operation": "insert",
					"name": "TsG2BAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 1,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsG2BAdvertBusiness"
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 2
				},
				{
					"operation": "insert",
					"name": "TsVipAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 1,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsVipAdvertBusiness"
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 3
				},
				{
					"operation": "insert",
					"name": "TsAccountAdvertCategory",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 0,
							"row": 2,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"contentType": 3,
						"bindTo": "TsAccountAdvertCategory"
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 4
				},
				{
					"operation": "insert",
					"name": "TsExecutiveAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 2,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsExecutiveAdvertBusiness",
						"labelConfig": {},
						"enabled": true,
						"contentType": 5
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 5
				},
				{
					"operation": "insert",
					"name": "TsExecSuppAdvertBusiness",
					"values": {
						"layout": {
							"colSpan": 12,
							"rowSpan": 1,
							"column": 12,
							"row": 3,
							"layoutName": "CategoriesAdvertBusinessControlGroupContainer"
						},
						"bindTo": "TsExecSuppAdvertBusiness"
					},
					"parentName": "CategoriesAdvertBusinessControlGroupContainer",
					"propertyName": "items",
					"index": 6
				},
				{
					"operation": "insert",
					"name": "TsDiscountInAccountDetailV2",
					"values": {
						"itemType": 2
					},
					"parentName": "HistoryTabContainer",
					"propertyName": "items",
					"index": 3
				}
			]/**SCHEMA_DIFF*/,
			methods: {
				onEntityInitialized: function() {
					this.callParent(arguments);
					this.onOwnershipChanged();
				},

				onOwnershipChanged: function() {
					var ownership = this.get("Ownership");
					var isKPPVisible = true;
					if (!this.Ext.isEmpty(ownership) && ownership.value === TsJsConsts.AccountOwnership.IndividualEntrepreneur.value) {
						isKPPVisible = false;
					}
					this.set("IsKPPVisible", isKPPVisible);
				}
			},
			rules: {
				"TsExecutiveAdvertBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecutiveFinancialBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecutiveParcelBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecutivePostBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecSuppAdvertBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecSuppPostBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecSuppFinBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				},
				"TsExecSuppParcelBusiness": {
					"FiltrationByUserType": {
						"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
						"autocomplete": false,
						"baseAttributePatch": "SysAdminUnitTypeValue",
						"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
						"type": BusinessRuleModule.enums.ValueType.CONSTANT,
						"value": ConfigurationConstants.SysAdminUnit.Type.User
					}
				}

			}
		};
	});
