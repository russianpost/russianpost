define("ActivityPageV2", [],
		function() {
			return {
				entitySchemaName: "Activity",
				attributes: {},
				methods: {},
				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "insert",
						"name": "TsBusinessTypes",
						"values": {
							"layout": {
								"colSpan": 12,
								"column": 0,
								"row": 5
							},
							"bindTo": "TsBusinessTypes"
						},
						"parentName": "Header",
						"propertyName": "items"
					}
				]/**SCHEMA_DIFF*/
			};
		});
