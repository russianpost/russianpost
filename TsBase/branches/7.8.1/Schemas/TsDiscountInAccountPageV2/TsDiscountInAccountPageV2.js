define("TsDiscountInAccountPageV2", [], function() {
	return {
		entitySchemaName: "TsDiscountInAccount",
		methods: {
			tsDiscountValidator: function() {
				var discount = this.get("TsDiscount");
				var validationResult = {
					fullInvalidMessage: this.Terrasoft.emptyString,
					invalidMessage: this.Terrasoft.emptyString
				};

				if (discount < 0 || discount > 100) {
					var errorMessage = this.get("Resources.Strings.TsDiscountOutOfRange");
					this.Ext.merge(validationResult, {
						fullInvalidMessage: errorMessage,
						invalidMessage: errorMessage
					});
				}
				return validationResult;
			},
			setValidationConfig: function() {
				this.callParent(arguments);

				this.addColumnValidator("TsDiscount", this.tsDiscountValidator);
			}
		},
		rules: {},
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[]/**SCHEMA_DIFF*/
	};
});
