define("OpportunityProductPageV2", [],
	function() {
		return {
			entitySchemaName: "OpportunityProductInterest",
			attributes: {},
			details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
			methods: {},
			diff: /**SCHEMA_DIFF*/[
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "TsSenderIndex",
					"values": {
						"bindTo": "TsSenderIndex",
						"layout": {"column": 12, "row": 0, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "TsPointOfDeparture",
					"values": {
						"bindTo": "TsPointOfDeparture",
						"layout": {"column": 12, "row": 1, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "TsRecipientIndex",
					"values": {
						"bindTo": "TsRecipientIndex",
						"layout": {"column": 12, "row": 2, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "TsPointOfDestination",
					"values": {
						"bindTo": "TsPointOfDestination",
						"layout": {"column": 12, "row": 3, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "TsWeight",
					"values": {
						"bindTo": "TsWeight",
						"layout": {"column": 12, "row": 4, "colSpan": 12}
					}
				},
				{
					"operation": "insert",
					"parentName": "Header",
					"propertyName": "items",
					"name": "TsDeliveryDate",
					"values": {
						"bindTo": "TsDeliveryDate",
						"layout": {"column": 12, "row": 5, "colSpan": 12}
					}
				}
			]/**SCHEMA_DIFF*/
		};
	});
