define("ContactPageV2", ["BusinessRuleModule", "ConfigurationConstants"], function(BusinessRuleModule,
		ConfigurationConstants) {
	return {
		entitySchemaName: "Contact",
		attributes: {},
		methods: {},
		rules: {
			"TsLeadType": {
				"ShowIfContactIsEmployee": {
					"ruleType": BusinessRuleModule.enums.RuleType.BINDPARAMETER,
					"property": BusinessRuleModule.enums.Property.VISIBLE,
					"conditions": [
						{
							"leftExpression": {
								"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								"attribute": "Type"
							},
							"comparisonType": Terrasoft.ComparisonType.EQUAL,
							"rightExpression": {
								"type": BusinessRuleModule.enums.ValueType.CONSTANT,
								"value": ConfigurationConstants.ContactType.Employee
							}
						}
					]
				},
				"RequireIfContactIsEmployee": {
					ruleType: BusinessRuleModule.enums.RuleType.BINDPARAMETER,
					property: BusinessRuleModule.enums.Property.REQUIRED,
					conditions: [
						{
							"leftExpression": {
								"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
								"attribute": "Type"
							},
							"comparisonType": Terrasoft.ComparisonType.EQUAL,
							"rightExpression": {
								"type": BusinessRuleModule.enums.ValueType.CONSTANT,
								"value": ConfigurationConstants.ContactType.Employee
							}
						}
					]
				}
			}
		},
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"parentName": "JobInformationBlock",
				"propertyName": "items",
				"name": "TsResponsibilityField",
				"values": {
					"layout": {"column": 0, "row": 2, "colSpan": 12},
					"contentType": Terrasoft.ContentType.ENUM
				}
			},
			{
				"operation": "insert",
				"parentName": "ProfileContainer",
				"propertyName": "items",
				"name": "ProfileFlexibleContainer",
				"values": {
					"itemType": Terrasoft.ViewItemType.CONTAINER,
					"layout": {
						"column": 0,
						"row": 3,
						"colSpan": 24
					},
					"items": []
				}
			},
			{
				"operation": "move",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "Type",
				"index": 0
			},
			{
				"operation": "insert",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "TsLeadType",
				"values": {
					"contentType": Terrasoft.ContentType.ENUM
				},
				"index": 1
			},
			{
				"operation": "move",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "JobTitleProfile",
				"index": 2
			},
			{
				"operation": "move",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "AccountMobilePhone",
				"index": 3
			},
			{
				"operation": "move",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "AccountPhone",
				"index": 4
			},
			{
				"operation": "move",
				"parentName": "ProfileFlexibleContainer",
				"propertyName": "items",
				"name": "AccountEmail",
				"index": 5
			}
		]/**SCHEMA_DIFF*/
	};
});
