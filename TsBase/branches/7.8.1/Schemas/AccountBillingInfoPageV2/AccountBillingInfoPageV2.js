define("AccountBillingInfoPageV2", ["TsJsConsts"],
		function(TsJsConsts) {
			return {
				entitySchemaName: "AccountBillingInfo",
				attributes: {
					"AccountIsNotIndividualEntrepreneur": {
						dependencies: [
							{
								columns: ["Account"],
								methodName: "updateAccountOwnership"
							}
						]
					},
					"Account": {
						lookupListConfig: {
							columns: ["Ownership", "Ownership.TsINNLength"]
						}
					},
					"Ownership": {
						lookupListConfig: {
							columns: ["TsINNLength"]
						}
					}
				},
				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
				methods: {
					onEntityInitialized: function() {
						this.callParent(arguments);
						this.updateAccountOwnership();
					},
					updateAccountOwnership: function() {
						var accountOwnership = this.get("Account").Ownership;
						if (accountOwnership && accountOwnership.value) {
							this.set("AccountIsNotIndividualEntrepreneur", TsJsConsts.AccountOwnership.IndividualEntrepreneur.value !==
									accountOwnership.value);
						}
						else {
							this.set("AccountIsNotIndividualEntrepreneur", true);
						}
					},
					identificationNumberValidator: function() {
						var invalidMessage = "";
						var maxINNLength = this.get("Account")["Ownership.TsINNLength"];
						var currentINNLength = this.get("TsIdentificationNumber").toString().length;

						if (maxINNLength && currentINNLength > maxINNLength) {
							invalidMessage = this.Ext.String.format(this.get("Resources.Strings.IndividualCodeLengthExceeded"),
									currentINNLength, maxINNLength);
						}
						return {
							fullInvalidMessage: invalidMessage,
							invalidMessage: invalidMessage
						};
					},

					setValidationConfig: function() {
						this.callParent(arguments);

						this.addColumnValidator("TsIdentificationNumber", this.identificationNumberValidator);
					}
				},
				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "remove",
						"name": "BillingInfo",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "BillingInfo",
							"layout": {
								"column": 12,
								"colSpan": 12,
								"row": 5,
								"rowSpan": 1
							},
							controlConfig: {
								"height": "160px"
							},
							"contentType": Terrasoft.ContentType.LONG_TEXT
						}
					},
					{
						"operation": "insert",
						"name": "TsIdentificationNumber",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "TsIdentificationNumber",
							"layout": {"column": 12, "row": 0, "colSpan": 6}
						}
					},
					{
						"operation": "insert",
						"name": "TsKPP",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "TsKPP",
							"layout": {"column": 18, "row": 0, "colSpan": 6},
							"visible": {bindTo: "AccountIsNotIndividualEntrepreneur"}
						}
					},
					{
						"operation": "insert",
						"name": "TsOKPO",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "TsOKPO",
							"layout": {"column": 12, "row": 1, "colSpan": 12}
						}
					},
					{
						"operation": "insert",
						"name": "TsOKVED",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "TsOKVED",
							"layout": {"column": 12, "row": 2, "colSpan": 12}
						}
					},
					{
						"operation": "insert",
						"name": "TsOGRN",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "TsOGRN",
							"layout": {"column": 12, "row": 3, "colSpan": 12}
						}
					},
					{
						"operation": "insert",
						"name": "TsBIK",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "TsBIK",
							"layout": {"column": 12, "row": 4, "colSpan": 12}
						}
					},
					{
						"operation": "insert",
						"name": "TsBankName",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "TsBankName",
							"layout": {"column": 12, "row": 5, "colSpan": 12}
						}
					},
					{
						"operation": "insert",
						"name": "TsBankAccountNumber",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "TsBankAccountNumber",
							"layout": {"column": 12, "row": 6, "colSpan": 12}
						}
					},
					{
						"operation": "insert",
						"name": "TsBankCorrespondentAccount",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "TsBankCorrespondentAccount",
							"layout": {"column": 12, "row": 7, "colSpan": 12}
						}
					},
					{
						"operation": "move",
						"name": "Name",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "Name",
							"layout": {
								"column": 0,
								"row": 2,
								"colSpan": 12
							}
						}
					},
					{
						"operation": "move",
						"name": "AccountManager",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "AccountManager",
							"layout": {
								"column": 0,
								"row": 3,
								"colSpan": 12
							}
						}
					},
					{
						"operation": "move",
						"name": "ChiefAccountant",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "ChiefAccountant",
							"layout": {
								"column": 0,
								"row": 4,
								"colSpan": 12
							}
						}
					},
					{
						"operation": "move",
						"name": "Country",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"bindTo": "Country",
							"layout": {
								"column": 0,
								"row": 5,
								"colSpan": 12
							}
						}
					},
					{
						"operation": "move",
						"name": "LegalUnit",
						"parentName": "Header",
						"propertyName": "items",
						"values": {
							"layout": {"column": 0, "row": 1, "colSpan": 24}
						}
					}
				]/**SCHEMA_DIFF*/,
				rules: {},
				userCode: {}
			};
		}
);
