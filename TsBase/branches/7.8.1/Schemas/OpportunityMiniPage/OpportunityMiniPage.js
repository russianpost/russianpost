define("OpportunityMiniPage", ["BusinessRuleModule", "ConfigurationConstants", "ProcessModuleUtilities"],
		function(BusinessRuleModule, ConfigurationConstants, ProcessModuleUtilities) {
			return {
				entitySchemaName: "opportunity",
				attributes: {
					"Client": {
						"multiLookupColumns": ["Account"],
						"isRequired": true
					}
				},
				methods: {
					onSaved: function() {
						this.callParent(arguments);
						if (this.isAddMode()) {
							this.showConfirmationDialog(this.get("Resources.Strings.ConfirmRunOpportunityProcessMessage"), function(result) {
								if (result === Terrasoft.MessageBoxButtons.YES.returnCode) {
									ProcessModuleUtilities.executeProcess({
										sysProcessName: "TsOpportunityManagement",
										parameters: {
											CurrentOpportunity: this.get("Id")
										}
									});
								}
							}, ["yes", "no"]);
						}
					}
				},
				rules: {
					"Stage": {
						"FiltrationStageByLeadType": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"autocomplete": true,
							"autoClean": true,
							"baseAttributePatch": "[TsOppStageInLeadType:TsOpportunityStage].TsLeadType",
							"comparisonType": this.Terrasoft.ComparisonType.EQUAL,
							"type": BusinessRuleModule.enums.ValueType.ATTRIBUTE,
							"attribute": "LeadType"
						}
					},
					"Account": {
						"FiltrationClientByNotOurCompany": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"baseAttributePatch": "Type",
							"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
							"type": BusinessRuleModule.enums.ValueType.CONSTANT,
							"value": ConfigurationConstants.AccountType.OurCompany
						},
						"FiltrationOrderByNotCompetitor": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"baseAttributePatch": "Type",
							"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
							"type": BusinessRuleModule.enums.ValueType.CONSTANT,
							"value": ConfigurationConstants.AccountType.Competitor
						}
					}
				},
				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "remove",
						"name": "Budget"
					}
				]/**SCHEMA_DIFF*/
			};
		});
