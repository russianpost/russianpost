using System;
using Terrasoft.Common;
using Terrasoft.Core;
using Terrasoft.Core.Configuration;

namespace Terrasoft.Configuration.TsBase {
	public class TsLeadExceedStageDurationHelper : TsBaseExceedStageDurationHelper {

		#region Properties: Protected

		protected override string HeadEmailColumnName {
			get {
				return GetLocalizableStringValue("HeadEmailColumnName");
			}
		}

		protected override string DataObjectName {
			get {
				return GetLocalizableStringValue("DataObjectName");
			}
		}

		protected override string EmailTableView {
			get {
				return GetLocalizableStringValue("EmailTableView");
			}
		}

		protected override string EmailRowView {
			get {
				string rowView = GetLocalizableStringValue("EmailRowView");
				var baseUrl = (string)Terrasoft.Core.Configuration.SysSettings.GetValue(UserConnection, "WebsiteBaseUrl");
				return rowView.Replace("{baseUrl}", baseUrl);
			}
		}

		protected override string Subject {
			get {
				return GetLocalizableStringValue("Subject");
			}
		}

		#endregion

		#region Constructors

		public TsLeadExceedStageDurationHelper(UserConnection userConnection)
			: base(userConnection) {
		}

		#endregion

		#region Methods : Private

		private string GetLocalizableStringValue(string name) {
			return new LocalizableString(UserConnection.ResourceStorage, "TsLeadExceedStageDurationHelper",
				"LocalizableStrings." + name + ".Value").Value;
		}

		#endregion
	}
}
