define("InvoicePageV2", ["BusinessRuleModule", "ConfigurationConstants"],
		function(BusinessRuleModule, ConfigurationConstants) {
			return {
				entitySchemaName: "Invoice",
				attributes: {
					"Client": {
						"multiLookupColumns": ["Account"],
						"isRequired": true
					}
				},
				rules: {
					"Account": {
						"FiltrationClientByNotOurCompany": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"baseAttributePatch": "Type",
							"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
							"type": BusinessRuleModule.enums.ValueType.CONSTANT,
							"value": ConfigurationConstants.AccountType.OurCompany
						},
						"FiltrationOrderByNotCompetitor": {
							"ruleType": BusinessRuleModule.enums.RuleType.FILTRATION,
							"baseAttributePatch": "Type",
							"comparisonType": this.Terrasoft.ComparisonType.NOT_EQUAL,
							"type": BusinessRuleModule.enums.ValueType.CONSTANT,
							"value": ConfigurationConstants.AccountType.Competitor
						}
					}
				},
				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "merge",
						"parentName": "InvoicePageGeneralBlock",
						"propertyName": "items",
						"name": "Client",
						"values": {
							"tip": null
						}
					},
					{
						"operation": "insert",
						"parentName": "InvoicePagePaymentBlock",
						"name": "TsPaymentCondition",
						"propertyName": "items",
						"values": {
							"bindTo": "TsPaymentCondition",
							"layout": {
								"column": 12,
								"row": 1,
								"colSpan": 12
							},
							"contentType": this.Terrasoft.ContentType.ENUM
						}
					}
				]/**SCHEMA_DIFF*/
			};
		});
