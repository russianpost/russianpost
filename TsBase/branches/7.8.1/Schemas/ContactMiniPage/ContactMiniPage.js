define("ContactMiniPage", [],
		function() {
			return {
				entitySchemaName: "Contact",
				attributes: {},
				methods: {},
				rules: {},
				details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
				diff: /**SCHEMA_DIFF*/[
					{
						"operation": "insert",
						"parentName": "MiniPage",
						"propertyName": "items",
						"name": "Job",
						"values": {
							"isMiniPageModelItem": true,
							"visible": {"bindTo": "isNotViewMode"},
							"layout": {
								"column": 0,
								"row": 4,
								"colSpan": 24
							}
						}
					},
					{
						"operation": "merge",
						"name": "JobTitle",
						"values": {
							"layout": {
								"row": 5,
								"column": 0,
								"colSpan": 24
							}
						}
					},
					{
						"operation": "merge",
						"name": "Department",
						"values": {
							"layout": {
								"row": 6,
								"column": 0,
								"colSpan": 24
							}
						}
					},
					{
						"operation": "merge",
						"name": "MobilePhone",
						"values": {
							"layout": {
								"row": 7,
								"column": 0,
								"colSpan": 24
							}
						}
					},
					{
						"operation": "merge",
						"name": "Email",
						"values": {
							"layout": {
								"row": 8,
								"column": 0,
								"colSpan": 24
							}
						}
					}

				]/**SCHEMA_DIFF*/
			};
		});
