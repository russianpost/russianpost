define("TsBusinessTypeInProductPageV2", [], function() {
	return {
		entitySchemaName: "TsBusinessTypeInProduct",
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "TsProduct87a21957-e66a-4ebb-94c0-346a76b0f31a",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 0,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "TsProduct"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 0
			},
			{
				"operation": "insert",
				"name": "TsBusinessTypes50009c3b-7494-49a6-a42b-65ef348d03d5",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 12,
						"row": 0,
						"layoutName": "Header"
					},
					"bindTo": "TsBusinessTypes"
				},
				"parentName": "Header",
				"propertyName": "items",
				"index": 1
			}
		]/**SCHEMA_DIFF*/,
		methods: {},
		rules: {}
	};
});
