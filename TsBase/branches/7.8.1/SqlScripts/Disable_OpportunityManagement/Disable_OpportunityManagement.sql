DECLARE @OpportunityManagementBaseSchemaUId UNIQUEIDENTIFIER = 'A5F68BDC-2044-42C4-8830-9965E224D704';
INSERT INTO SysProcessDisabled (SysSchemaId)
	SELECT ss.Id
	FROM SysSchema ss LEFT JOIN SysProcessDisabled spd
			ON spd.SysSchemaId = ss.Id
	WHERE spd.Id IS NULL AND ss.UId = @OpportunityManagementBaseSchemaUId