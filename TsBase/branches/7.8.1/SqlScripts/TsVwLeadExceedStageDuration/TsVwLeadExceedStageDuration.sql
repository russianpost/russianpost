IF object_id(N'TsVwLeadExceedStageDuration', 'V') IS NOT NULL
  DROP VIEW [dbo].[TsVwLeadExceedStageDuration]
GO

CREATE VIEW [dbo].[TsVwLeadExceedStageDuration] AS
  SELECT
    qs.Name            AS TsLeadStageName,
    l.Id               AS TsLeadId,
    l.LeadName         AS TsLeadName,
    ISNULL(a.Name, '') AS TsAccountName,
    c.Name             AS TsOwnerName,
    HeadEmail.Number   AS TsHeadEmail
  FROM Lead l
    JOIN TsLeadStageHistory lsh ON lsh.TsLeadId = l.Id AND lsh.TsDueDate IS NULL
    JOIN TsLeadStageLimit lsl ON lsl.TsQualifyStatusId = lsh.TsQualifyStatusId
    JOIN QualifyStatus qs ON qs.Id = lsl.TsQualifyStatusId AND qs.IsFinal != 1
    JOIN Contact c ON c.Id = lsh.TsOwnerId
    JOIN Contact cHead ON cHead.Id = c.OwnerId
    CROSS APPLY (
                  SELECT TOP 1 Number
                  FROM ContactCommunication
                  WHERE
                    ContactId = cHead.Id
                    AND CommunicationTypeId = 'EE1C85C3-CFCB-DF11-9B2A-001D60E938C6' --Email
                  ORDER BY ModifiedOn DESC
                ) AS HeadEmail
    LEFT JOIN Account a ON a.Id = l.QualifiedAccountId
  WHERE
    GETUTCDATE() > DATEADD(DAY, lsl.TsMaxDurationInDays, lsh.TsStartDate)