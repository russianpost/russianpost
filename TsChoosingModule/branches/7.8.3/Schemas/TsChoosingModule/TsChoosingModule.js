define("TsChoosingModule", ["ext-base", "terrasoft", "ModalBox", "LookupUtilities",
        "ViewGeneratorV2", "MaskHelper", "css!TsChoosingModuleStyle"
    ],
    function (Ext, Terrasoft, ModalBox, LookupUtilities) {
        return Ext.define("Terrasoft.configuration.TsChoosingModule", {
            extend: "Terrasoft.BaseObject",
            alternateClassName: "Terrasoft.TsChoosingModule",
            sandbox: null,
            singleton: true,
            view: null,
            viewGenerator: null,
            viewGeneratorClassName: "Terrasoft.ViewGenerator",
            viewModelClass: null,
            viewModelClassName: "Terrasoft.BaseViewModel",
            gridLayoutRowHeight: 42,
            heightConstDif: 40,
            height: 64,
            width: 400,
            /**
             * Контейнер в который будет отрисован LookupPage
             * @private
             * @type {Object}
             */
            modalBoxContainer: null,
            /**
             * Конфигурация открытия модального окна
             * @private
             * @type {Object}
             */
            modalBoxSize: {
                minHeight: "1",
                minWidth: "1",
                maxHeight: "120",
                maxWidth: "120"
            },
            /**
             * Возвращает фиксированный контейнер в модальном окне
             * @protected
             * @returns {Object}
             */
            getFixedContainer: function () {
                return ModalBox.getFixedBox();
            },
            /**
             * Возвращает основной контейнер в модальном окне
             * @protected
             * @returns {Object}
             */
            getPageContainer: function () {
                return this.modalBoxContainer;
            },
            /**
             * Обновляет размеры окна в соответствии с контентом
             */
            UpdateSize: function () {
                ModalBox.updateSizeByContent();
            },

            /**
             * Подготавливает модальное окно для загрузки туда модуля
             * @private
             */
            prepareModalBox: function () {
                ModalBox.show(this.modalBoxSize, function (destroy) {
                    if (destroy && this.view) {
                        this.view.destroy();
                    }
                }, this);
                this.modalBoxContainer = this.getFixedContainer();
                ModalBox.setSize(this.width, this.height);
                //MaskHelper.ShowBodyMask();
            },

            /**
             * Закрывает модальное окно Lookup-а и выгружает модуль
             */
            CloseModalBox: function () {
                if (this.modalBoxContainer) {
                    ModalBox.close(true);
                    this.modalBoxContainer = null;
                }
                //MaskHelper.HideBodyMask();
            },
            /**
             * Генерирует идентификатор для модуля
             * @private
             * @param {Object} sandbox Песоцница модуля, вызывающего открытие модуля выбора
             * @return {string}
             */
            getChoosingPageId: function () {
                return this.sandbox.id + "_TsChoosingPage";
            },

            getViewModelClass: function (columns) {
                if (this.viewModelClass) {
                    return this.viewModelClass;
                }
                var classConfig = {
                    extend: this.viewModelClassName,
                    alternateClassName: "Terrasoft.TsChoosingModuleViewModelConfig",
                    columns: columns
                };
                this.viewModelClass = Ext.define("Terrasoft.configuration.TsChoosingModuleViewModelConfig",
                    classConfig);
                return this.viewModelClass;
            },

            getViewGenerator: function () {
                if (this.viewGenerator) {
                    return this.viewGenerator;
                }
                this.viewGenerator = Ext.create(this.viewGeneratorClassName);
                return this.viewGenerator;
            },

            generateViewConfig: function (viewConfig, columns) {
                var viewGenerator = this.getViewGenerator();
                var viewModelClass = this.getViewModelClass(columns);
                var id = this.getChoosingPageId() + "GridLayout";
                var gridLayout = {
                    "name": id,
                    "itemType": Terrasoft.ViewItemType.GRID_LAYOUT,
                    items: viewConfig
                };
                var gridLayoutContainer = {
                    "name": id + "Container",
                    "itemType": Terrasoft.ViewItemType.CONTAINER
                };
                gridLayoutContainer.items = [gridLayout];
                var config = {
                    schema: {viewConfig: gridLayoutContainer},
                    viewModelClass: viewModelClass
                };
                viewGenerator.init(config);
                var viewConfig = [gridLayoutContainer];
                viewGenerator.customGenerators = [];
                viewGenerator.fillCustomGenerators(viewConfig, viewGenerator.customGenerators);
                return viewGenerator.generateView(viewConfig)[0];
            },
            applyControlsConfig: function (controlConfig, pageButtons) {
                var config = [];
                var labelConfig = this.getMainLabelConfig();
                var buttonsConfig = this.getButtonsConfig(pageButtons, controlConfig);
                config = config.concat(labelConfig, controlConfig, buttonsConfig);
                this.height = this.windowHeight || (buttonsConfig[0].layout.row + 1) * this.gridLayoutRowHeight + this.heightConstDif;
                return config;
            },
            getMainLabelConfig: function () {
                return {
                    "name": "captionLabel",
                    "itemType": Terrasoft.ViewItemType.LABEL,
                    "caption": this.caption,
                    "layout": {
                        row: 0,
                        rowSpan: 1,
                        column: 2,
                        colSpan: 20
                    },
                    "labelClass": ["choosing-box-main-caption"]
                };
            },
            getButtonsConfig: function (pageButtons, controlConfig) {
                var buttons = pageButtons || [];
                var result = [];
                var defButtons = this.getPredefinedButtons();
                Terrasoft.each(buttons, function (buttonConfig, buttonName) {
                    var btn = defButtons[buttonName];
                    if (btn) {
                        if (buttonConfig) {
                            btn = Ext.apply(btn, buttonConfig);
                        }
                        result.push(btn);
                    }
                }, this);
                var lastRowIndex = this.getLastRowIndex(controlConfig);
                var buttonsCount = result.length;
                var lastColumn = 2, colSpan = 20 / buttonsCount,
                    buttonWidth = Math.round(colSpan * 0.8), spaceWidth = Math.round(colSpan * 0.1);
                Terrasoft.each(result, function (button, key) {
                    result[key] = Ext.apply(button, {
                        "layout": {
                            row: lastRowIndex + 1 + (this.doNotAddRowBeforeButtons ? 0 : 1),
                            rowSpan: 1,
                            column: lastColumn + spaceWidth,
                            colSpan: buttonWidth
                        }
                    });
                    lastColumn += buttonWidth + 2 * spaceWidth;
                }, this);
                return result;
            },
            getLastRowIndex: function (controlsConfig) {
                var lastIndex = 1;
                Terrasoft.each(controlsConfig, function (controlConfig) {
                    controlConfig.layout.rowSpan = controlConfig.layout.rowSpan ? controlConfig.layout.rowSpan : 0;
                    if (controlConfig && controlConfig.layout && isFinite((controlConfig.layout.row))) {
                        // if (controlConfig.layout.rowSpan && controlConfig.layout.rowSpan > 0) {
                        //     controlConfig.layout.row += controlConfig.layout.rowSpan;
                        // } else {
                        controlConfig.layout.row += 1;
                        // }
                        if (controlConfig.layout.row + controlConfig.layout.rowSpan > lastIndex) {
                            lastIndex = controlConfig.layout.row + controlConfig.layout.rowSpan;
                        }
                    }
                }, this);
                return lastIndex;
            },
            getPredefinedButtons: function () {
                return {
                    "close": {
                        name: "closeButton",
                        itemType: Terrasoft.ViewItemType.BUTTON,
                        caption: Terrasoft.Resources.Controls.MessageBox.ButtonCaptionClose,
                        "classes": {"textClass": ["button-full-width"]},
                        "click": {"bindTo": "closeModalBox"},
                        "tag": "close"
                    },
                    "ok": {
                        name: "okButton",
                        itemType: Terrasoft.ViewItemType.BUTTON,
                        caption: Terrasoft.Resources.Controls.MessageBox.ButtonCaptionOk,
                        "classes": {"textClass": ["button-full-width"]},
                        "click": {"bindTo": "closeModalBox"},
                        "tag": "ok"
                    },
                    "yes": {
                        name: "yesButton",
                        itemType: Terrasoft.ViewItemType.BUTTON,
                        caption: Terrasoft.Resources.Controls.MessageBox.ButtonCaptionYes,
                        "classes": {"textClass": ["button-full-width"]},
                        "click": {"bindTo": "closeModalBox"},
                        "tag": "yes"
                    },
                    "no": {
                        name: "noButton",
                        itemType: Terrasoft.ViewItemType.BUTTON,
                        caption: Terrasoft.Resources.Controls.MessageBox.ButtonCaptionNo,
                        "classes": {"textClass": ["button-full-width"]},
                        "click": {"bindTo": "closeModalBox"},
                        "tag": "no"
                    },
                    "cancel": {
                        name: "noButton",
                        itemType: Terrasoft.ViewItemType.BUTTON,
                        caption: Terrasoft.Resources.Controls.MessageBox.ButtonCaptionCancel,
                        "classes": {"textClass": ["button-full-width"]},
                        "click": {"bindTo": "closeModalBox"},
                        "tag": "cancel"
                    }
                };
            },

            /**
             * Открывает справочник в модальном окне
             * @protected
             * @param {Object} config Конфигурация справочника
             * @param {Function} callback Функция обратного вызова
             * @param {Object} scope Контекст функции обратного вызова
             */
            openLookup: function (config, callback, scope) {
                LookupUtilities.Open(this.sandbox, config, callback, scope || this, null, false, false);
            },

            onLookUpClick: function (args) {
                var selectedRows = args.selectedRows;
                if (!selectedRows.isEmpty()) {
                    this.viewModel.set(args.columnName, selectedRows.getByIndex(0));
                }
            },

            waitForLookupWindow: function (callback, scope) {
                var me = this;
                var runner = new Ext.util.TaskRunner();
                var task = {
                    run: function () {
                        if (me.getFixedContainer() == null && (me.checkHistoryState())) {
                            runner.stop(task);
                            callback.call(scope);
                        }
                    },
                    interval: 100
                };
                runner.start(task);
            },

            checkHistoryState: function () {
                var startModuleId = this.historyState && this.historyState.state && this.historyState.state.moduleId;
                if (startModuleId) {
                    var currentState = this.sandbox.publish("GetHistoryState");
                    var moduleId = currentState && currentState.state && currentState.state.moduleId;
                    return (moduleId === startModuleId);
                }
                return true;
            },
            getLookupListConfig: function (schemaColumn) {
                var lookupListConfig = schemaColumn.lookupListConfig;
                if (!lookupListConfig) {
                    return null;
                }
                var excludedProperty = ["filters", "filter"];
                var config = {};
                Terrasoft.each(lookupListConfig, function (property, propertyName) {
                    if (excludedProperty.indexOf(propertyName) === -1) {
                        config[propertyName] = property;
                    }
                });
                return config;
            },
            setViewModelFunctionality: function (viewModel, scope) {
                viewModel.closeModalBox = function (item1, item2, item3, tag) {
                    scope.CloseModalBox.apply(scope);
                    if (typeof(scope.onButtonClick) === "function") {
                        scope.onButtonClick.call(this, tag);
                    } else {
                        var callback = this[scope.onButtonClick];
                        if (typeof(callback) === "function") {
                            callback.call(this, tag);
                        }
                    }
                };
                viewModel.loadVocabulary = function (filtration, columnName) {
                    var column = this.getColumnByName(columnName);
                    //scope.columns[columnName];
                    if (column) {
                        var config = {
                            entitySchemaName: column.referenceSchemaName,
                            multiSelect: false,
                            columnName: columnName
                        };
                        var lookupConfig = scope.getLookupListConfig(column);
                        var filters = scope.getColumnFilters(column, viewModel);
                        if (filters) {
                            lookupConfig.filters = filters;
                        }
                        Ext.apply(config, lookupConfig);
                        scope.CloseModalBox();
                        scope.waitForLookupWindow(function () {
                            this.prepareModalBox();
                            this.view = Ext.create("Terrasoft.Container", Terrasoft.deepClone(this.viewConfig));
                            this.view.bind(this.viewModel);
                            this.view.render(this.getPageContainer());
                        }, scope);
                        scope.openLookup(config, scope.onLookUpClick, scope);
                    }
                };
            },
            getColumnFilters: function (column, viewModel) {
                var config = column.lookupListConfig;
                if (config.filter) {
                    return config.filter.call(viewModel);
                }
                return null;
            },
            applyColumns: function (configColumns, vmColumns) {
                Terrasoft.each(configColumns, function (columnData, columnName) {
                    if (vmColumns[columnName]) {
                        configColumns[columnName] = Ext.apply(columnData, vmColumns[columnName]);
                    }
                }, this);
                return Terrasoft.deepClone(configColumns);
            },
            /**
             *Открывает окно выбора
             */
            open: function (config) {
                var viewModel = this.viewModel = config.viewModel;
                var controlsConfig = config.controlsConfig;
                var buttons = config.buttons;
                this.sandbox = config.sandbox;
                this.columns = config.columns ? (this.applyColumns(config.columns, this.viewModel.bindMap.collection.map))
                    : this.viewModel.bindMap.collection.map;
                this.caption = config.caption;
                this.onButtonClick = config.onButtonClick;
                this.doNotAddRowBeforeButtons = config.doNotAddRowBeforeButtons;
                if (config.viewModelClassName) {
                    this.viewModelClassName = config.viewModelClassName;
                }
                if (config.windowWidth) {
                    this.width = config.windowWidth;
                }
                if (config.windowHeight) {
                    this.windowHeight = config.windowHeight;
                }
                var viewConfig = this.applyControlsConfig(controlsConfig, buttons);
                this.viewConfig = this.generateViewConfig(viewConfig, this.columns);
                var view = this.view = Ext.create("Terrasoft.Container", Terrasoft.deepClone(this.viewConfig));
                var scope = this;
                this.historyState = this.sandbox.publish("GetHistoryState");
                this.prepareModalBox();
                this.setViewModelFunctionality(viewModel, this);
                view.bind(viewModel);
                view.render(this.getPageContainer());
            }
        });

    });